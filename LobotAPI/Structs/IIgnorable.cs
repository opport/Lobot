﻿namespace LobotAPI.Structs
{
    /// <summary>
    /// SpawnType that can be disregarded.
    /// This is helpful in handling getting StopMovement and using a timer to reset an object for being a viable target again.
    /// </summary>
    public interface IIgnorable
    {
        bool IsIgnored { get; set; }
    }
}
