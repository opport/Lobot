﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using System.Collections.Generic;

namespace LobotAPI.Structs
{
    /// <summary>
    /// A concise version of the <see cref="LobotAPI.PK2.Pk2ShopData"/> class.
    /// Used in <see cref="Globals.PK2DataGlobal.Shops"/> to map tab numbers to tabs.
    /// </summary>
    public class Shop
    {
        public uint NPCID;
        public Pk2NPC Pk2Entry => PK2DataGlobal.NPCs[NPCID];
        /// <summary>
        /// Item reference IDs mapped to an entry containing their page and slot in an NPC's store window.
        /// </summary>
        public Dictionary<uint, ShopItemEntry> ShopItemEntries = new Dictionary<uint, ShopItemEntry>();

        public Shop(uint nPCID)
        {
            NPCID = nPCID;
        }

        public override bool Equals(object obj)
        {
            if (obj is Shop)
            {
                return NPCID.Equals((obj as Shop).NPCID);
            }
            else
            {
                return base.Equals(obj);
            }
        }

        public override int GetHashCode()
        {
            return NPCID.GetHashCode();
        }

        public override string ToString()
        {
            return PK2DataGlobal.NPCs.TryGetValue(NPCID, out Pk2NPC entry) ? Pk2Entry.ToString() : NPCID.ToString();
        }
    }
}
