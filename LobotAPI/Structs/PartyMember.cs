﻿namespace LobotAPI.Structs
{
    public class PartyMember
    {
        public uint Avatar { get; set; }
        public uint MemberID { get; set; }
        public byte Level { get; set; }
        public uint Mastery1 { get; set; }
        public uint Mastery2 { get; set; }
        public string Name { get; set; }
        public string GuildName { get; set; }
        public byte Race { get; set; } = 0;

        public PartyMember(uint number)
        {
            MemberID = number;
        }

        public override string ToString()
        {
            return string.Format("({0,-3}){1,-13}-{2,-13}", Level, Name, GuildName);
        }
    }
}
