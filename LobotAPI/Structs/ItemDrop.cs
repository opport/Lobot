﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using System.Windows;

namespace LobotAPI.Structs
{
    public class ItemDrop : ISpawnType, IIgnorable
    {
        public uint RefID { get; set; }
        public uint UniqueID { get; set; }
        public string Name { get; set; }
        public uint OwnerID { get; set; }
        public string OwnerIDString { get { return OwnerID.ToString("X2"); } }
        public uint Amount = 0;
        public float X = 0;
        public float Y = 0;
        public Point Position { get; set; }
        public byte Plus = 0;
        public byte Rarity = 0; // Item Type -> 0 = normal , 1 = blue, 2= sox
        public Pk2Item Pk2Item { get { return PK2DataGlobal.Items[RefID]; } }
        public bool IsIgnored { get; set; }
    }
}
