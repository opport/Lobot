﻿using LobotAPI.Scripting;
using System.Collections.Generic;
using System.Windows;

namespace LobotAPI.Structs
{
    public class Town
    {
        public readonly string Name;
        public readonly Point SpawnPoint;
        public readonly List<Point> Polygon;
        public List<AbstractExpression> TownScript { get; private set; }


        public Town(string n, Point s, List<Point> p, List<AbstractExpression> script)
        {
            Name = n;
            SpawnPoint = s;
            Polygon = p;
            TownScript = script;
        }

        public Shop Teleporter { get; set; }
        public Shop Smith { get; set; }
        public Shop Armor { get; set; }
        public Shop Potions { get; set; }
        public Shop Accessories { get; set; }
        public Shop Special { get; set; }
        public Shop Stable { get; set; }

        public override string ToString()
        {
            return $"{Name}({SpawnPoint})";
        }
    }
}
