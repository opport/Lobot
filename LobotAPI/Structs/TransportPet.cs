﻿namespace LobotAPI.Structs
{
    public class TransportPet : Pet
    {
        public TransportPet(Pet pet)
        {
            CustomName = pet.CustomName;
            OwnerName = pet.OwnerName;
            OwnerID = pet.OwnerID;
            RefID = pet.RefID;
            UniqueID = pet.UniqueID;
            Position = pet.Position;
            CurrentHP = pet.CurrentHP;
            MaxHP = pet.MaxHP;
            IsAlive = pet.IsAlive;
            IsKnockedDown = pet.IsKnockedDown;
            Target = pet.Target;
            IsInvisible = pet.IsInvisible;
        }

        /// <summary>
        /// Use to update static pet in Bot for updating  UI
        /// </summary>
        /// <param name="pet"></param>
        public void SetFromPet(Pet pet)
        {
            CustomName = pet.CustomName;
            OwnerName = pet.OwnerName;
            OwnerID = pet.OwnerID;
            RefID = pet.RefID;
            UniqueID = pet.UniqueID;
            Position = pet.Position;
            CurrentHP = pet.CurrentHP;
            MaxHP = pet.MaxHP;
            IsAlive = pet.IsAlive;
            IsKnockedDown = pet.IsKnockedDown;
            Target = pet.Target;
            IsInvisible = pet.IsInvisible;
        }
    }
}