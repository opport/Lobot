﻿using System.Windows;

namespace LobotAPI.Structs
{
    /// <summary>
    /// The interface types to provide both ID types. 
    /// </summary>
    public interface ISpawnType
    {
        /// <summary>
        /// The spawn unit name
        /// </summary>
        string Name { get; }
        /// <summary>
        /// ID to lookup info from PK2 lists
        /// </summary>
        uint RefID { get; set; }
        /// <summary>
        /// ID to lookup in spawn list or perform ingame interactions.
        /// </summary>
        uint UniqueID { get; set; }
        /// <summary>
        /// Position to determine interaction with unit.
        /// </summary>
        Point Position { get; set; }
    }
}
