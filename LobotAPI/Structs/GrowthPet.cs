﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using System.ComponentModel;

namespace LobotAPI.Structs
{
    public class GrowthPet : Pet, INotifyPropertyChanged
    {
        private byte _level = 0;
        private ulong _eXP = 0;
        private ushort _hGP = 0;

        public byte Level { get => _level; set { _level = value; NotifyPropertyChanged(); } }
        public double HPPercent { get => CurrentHP == 0 || MaxHP == 0 ? 0 : (CurrentHP * 100) / MaxHP; }
        public ulong EXP { get => _eXP; set { _eXP = value; NotifyPropertyChanged("EXPPercent"); } }
        public double EXPPercent { get => PK2DataGlobal.LevelData != null && PK2DataGlobal.LevelData.TryGetValue(Level, out ulong lvlEXP) ? (double)(EXP * 100 / (double)lvlEXP) : 0; } // exp rounded up to second decimal place
        public ushort HGP { get => _hGP; set { _hGP = value; NotifyPropertyChanged("HGPPercent"); } }
        public double HGPPercent { get => HGP * 100 / 10000; }
        public byte BadStatus { get; set; }
        public bool AttackStatus { get; set; }

        public GrowthPet() { }

        public GrowthPet(Pet pet)
        {
            SetFromPet(pet);
        }

        /// <summary>
        /// Use to update static pet in Bot for updating  UI
        /// Also this to reuse variables EXP and CurrentHP for Pet that was relocated by server from being stuck.
        /// These variables are only set by the petinfohandler after a normal respawn, not after being relocated.
        /// This can be seen in the stats part of the bot window.
        /// </summary>
        /// <param name="pet"></param>
        public void SetFromPet(Pet pet)
        {
            CustomName = pet.CustomName;
            OwnerName = pet.OwnerName;
            OwnerID = pet.OwnerID;
            RefID = pet.RefID;
            UniqueID = pet.UniqueID;
            Position = pet.Position;
            CurrentHP = pet.CurrentHP;
            MaxHP = pet.MaxHP;
            IsAlive = pet.IsAlive;
            IsKnockedDown = pet.IsKnockedDown;
            Target = pet.Target;
            IsInvisible = pet.IsInvisible;

            if (pet is GrowthPet)
            {
                EXP = (pet as GrowthPet).EXP > 0 ? (pet as GrowthPet).EXP : EXP;
            }

            if (pet.MaxHP > 0)
            {
                MaxHP = pet.MaxHP;
            }
            else if (PK2DataGlobal.NPCs != null && PK2DataGlobal.NPCs.TryGetValue(pet.RefID, out Pk2NPC pk2Pet))
            {
                MaxHP = pk2Pet.HP;
            }
        }
    }
}
