﻿using LobotAPI.Globals.Settings;
using System;

namespace LobotAPI.Structs
{
    public enum PartyType : byte
    {
        // result of 3 bytes -> bool canInvite - bool itemshare - bool expshare
        ClosedFreeFree = 0x00,
        ClosedFreeShare = 0x01,
        ClosedShareFree = 0x02,
        ClosedShareShare = 0x03,
        LTP = 0x04, //OpenFreeFree
        OpenFreeShare = 0x05,
        OpenShareFree = 0x06,
        OpenShareShare = 0x07
    }

    public static class PartyTypeExtensions
    {
        public static string ToFriendlyString(this PartyType type)
        {
            switch (type)
            {
                case PartyType.ClosedFreeFree:
                    return "0 IF - EF";
                //return "Closed Item FFA, Exp FFA";
                case PartyType.ClosedFreeShare:
                    return "0 IF - ES";
                //return "Closed Item FFA, Exp Share";
                case PartyType.ClosedShareFree:
                    return "0 IS - EF";
                //return "Closed Item Share, Exp FFA";
                case PartyType.ClosedShareShare:
                    return "0 IS - ES";
                //return "Closed Item Share, Exp Share";
                case PartyType.LTP:
                    return "LTP";
                //return "Open Item FFA, Exp FFA";
                case PartyType.OpenFreeShare:
                    return "1 IF - ES";
                ///return "Open Item FFA, Exp Share";
                case PartyType.OpenShareFree:
                    return "1 IS - EF";
                //return "Open Item Share, Exp Free";
                case PartyType.OpenShareShare:
                    return "1 IS - ES";
                //return "Open Item Share, Exp Share";
                default:
                    return "Time To Party!";
            }
        }

        public static byte GetMaxMembers(this PartyType type)
        {
            return ((byte)type % 2 == 0) // expshare == 0
             ? (byte)4
             : (byte)8;
        }

        public static bool IsSameTypeOpenOrClosed(this PartyType thisType, PartyType type)
        {
            return type == PartySettings.Type || Math.Abs((byte)PartySettings.Type - (byte)type) == 0;
        }

        /// <summary>
        /// Return the type without the most significant bit that says whether members can invite.
        /// This is used to choose the party type in the GUI.
        /// E.g. OpenShareShare = 0x07 (bits 111) becomes ClosedShareShare = 0x03 (bits 11)
        /// </summary>
        /// <param name="thisType">Long party type</param>
        /// <returns>Party type without MSB</returns>
        /// <see href="https://stackoverflow.com/questions/7790233/how-to-clear-the-most-significant-bit"/>
        public static PartyType GetTypeWithoutMemberCanInvite(this PartyType thisType)
        {
            return (PartyType)((byte)thisType & 3);
        }
    }
}
