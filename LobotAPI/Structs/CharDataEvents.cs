﻿using LobotAPI.Scripting;
using System;
using System.Windows;

namespace LobotAPI.Structs
{
    public partial class CharData
    {
        public event EventHandler ZerkReady;
        public event EventHandler<ObjectStatusChangedArgs> CharStatusChanged;
        public event EventHandler CharNameChanged;
        public event EventHandler CharPositionChanged;

        private static void CalculateOffsetCoordinates(Point p)
        {
            Tuple<int, byte> xOffsetData = ScriptUtils.GetXOffset((int)p.X);
            Tuple<int, byte> yOffsetData = ScriptUtils.GetYOffset((int)p.Y);

            Bot.CharData.XOffset = xOffsetData.Item1;
            Bot.CharData.XSec = xOffsetData.Item2;
            Bot.CharData.YOffset = yOffsetData.Item1;
            Bot.CharData.YSec = yOffsetData.Item2;
        }

        private void OnNameChanged(EventArgs e)
        {
            EventHandler handler = CharNameChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnZerkReady(EventArgs e)
        {
            EventHandler handler = ZerkReady;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnCharStatusChanged(ObjectStatusChangedArgs e)
        {
            EventHandler<ObjectStatusChangedArgs> handler = CharStatusChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void OnPositionChanged(EventArgs e)
        {
            EventHandler handler = CharPositionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
