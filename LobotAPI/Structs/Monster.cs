using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LobotAPI.Structs
{
    /// <summary>
    /// Attribute describing a monster's strength level.
    /// </summary>
    public enum MonsterType : byte
    {
        Normal = 0x00,
        Champion = 0x01,
        Unique = 0x03,
        Giant = 0x04,
        Strong = 0x05, //Titan
        Elite1 = 0x06,
        Elite2 = 0x07,
        Party = 0x10,
        PartyChampion = 0x11,
        PartyUnique = 0x13,
        PartyGiant = 0x14,
        PartyStrong = 0x15, // = 0x00 TBD
        Elite = 0x16, // = 0x00 TBD
        Event, // = 0x00 TBD
        Quest // = 0x00 TBD
    }

    public static class MonsterTypeExtensions
    {
        public static int GetHPMultiplier(this MonsterType type)
        {
            switch (type)
            {
                case MonsterType.Normal:
                    break;
                case MonsterType.Champion:
                    return 2;
                case MonsterType.Unique:
                    break;
                case MonsterType.Giant:
                    return 20;
                case MonsterType.Strong:
                    break;
                case MonsterType.Elite1:
                    break;
                case MonsterType.Elite2:
                    break;
                case MonsterType.Party:
                    return 10;
                case MonsterType.PartyChampion:
                    return 20;
                case MonsterType.PartyGiant:
                    return 200;
                case MonsterType.PartyStrong:
                    break;
                case MonsterType.Elite:
                    break;
                case MonsterType.Event:
                    break;
                case MonsterType.Quest:
                    break;
                default:
                    break;
            }
            return 1;
        }
    }


    public class Monster : ISpawnType, ICombatType, IIgnorable, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Name
        {
            get
            {
                if (PK2DataGlobal.NPCs.TryGetValue(RefID, out PK2.Pk2NPC npc))
                {
                    string typeSuffix = Type != MonsterType.Normal ? $"({Type})" : "";
                    return $"{npc.Name}{typeSuffix}";
                }
                else
                {
                    return RefID.ToString();
                }
            }
        }
        public string LongID
        {
            get
            {
                if (PK2DataGlobal.NPCs.TryGetValue(RefID, out PK2.Pk2NPC npc))
                {
                    return npc.LongId;
                }
                else
                {
                    return RefID.ToString();
                }
            }
        }
        public int Level
        {
            get
            {
                if (PK2DataGlobal.NPCs.TryGetValue(RefID, out PK2.Pk2NPC npc))
                {
                    return npc.Level;
                }
                else
                {
                    return 0;
                }
            }
        }
        public uint RefID { get; set; } // ID for game class
        public uint UniqueID { get; set; } // ID for instance of class
        public MonsterType Type { get; set; }
        private int _maxHP;
        public int MaxHP { get => _maxHP * Type.GetHPMultiplier(); set => _maxHP = value; }
        private uint _currentHP;
        public uint CurrentHP { get => _currentHP; set { _currentHP = value; NotifyPropertyChanged(); } }
        private Point _position;
        public Point Position { get => _position; set { _position = value; NotifyPropertyChanged(); } }
        private bool _isKnockedDown;
        public bool IsKnockedDown { get => _isKnockedDown; set { _isKnockedDown = value; NotifyPropertyChanged(); } }
        private uint _target;
        public uint Target { get => _target; set { _target = value; NotifyPropertyChanged(); } }
        private bool _isAlive;
        public bool IsAlive { get => _isAlive; set { _isAlive = value; NotifyPropertyChanged(); } }
        public bool IsInvisible;
        private bool isIgnored;
        public bool IsIgnored { get => isIgnored; set { isIgnored = value; NotifyPropertyChanged(); } }
        public List<Buff> Buffs { get; set; } = new List<Buff>();
    }
}
