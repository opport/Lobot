﻿namespace LobotAPI.Structs
{
    public enum DebuffStatus : byte
    {
        Normal = 0x00,
        BadStatus = 0x01,
        Debuff = 0x02
    }
}
