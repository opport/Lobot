﻿namespace LobotAPI.Structs
{
    public struct ItemMallInfo
    {
        public readonly uint IPCoins;
        public readonly uint Silk;
        public readonly uint SoulTickets;

        public ItemMallInfo(uint iPCoins, uint silk, uint soulTickets)
        {
            IPCoins = iPCoins;
            Silk = silk;
            SoulTickets = soulTickets;
        }
    }
}
