﻿namespace LobotAPI.Structs
{
    public enum PartyObjective : byte
    {
        Hunting = 0x00,
        Quest = 0x01,
        TradeHunter = 0x02,
        Thief = 0x03
    }
}
