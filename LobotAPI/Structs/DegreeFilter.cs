﻿using Config.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace LobotAPI.Structs
{
    class DegreeFilterParser : ITypeParser
    {
        public IEnumerable<Type> SupportedTypes => new[] { typeof(DegreeFilter) };

        public string ToRawString(object value)
        {
            if (value == null) return null;

            return value.ToString();
        }

        private bool IsTrue(string s)
        {
            return s == "1" || s == "True";
        }

        public bool TryParse(string value, Type t, out object result)
        {
            if (value == null)
            {
                result = null;
                return false;
            }

            if (t == typeof(DegreeFilter))
            {
                string[] parts = ((string)value).Split(new char[] { ',' });

                // badly formed serialized item filter
                if (parts.Length < 13)
                {
                    result = default(DegreeFilter);
                    return false;
                }

                DegreeFilter filter = new DegreeFilter();
                filter.Arr[1] = parts.Length > 1 ? IsTrue(parts[1]) : false;
                filter.Arr[2] = parts.Length > 2 ? IsTrue(parts[2]) : false;
                filter.Arr[3] = parts.Length > 3 ? IsTrue(parts[3]) : false;
                filter.Arr[4] = parts.Length > 4 ? IsTrue(parts[4]) : false;
                filter.Arr[5] = parts.Length > 5 ? IsTrue(parts[5]) : false;
                filter.Arr[6] = parts.Length > 6 ? IsTrue(parts[6]) : false;
                filter.Arr[7] = parts.Length > 7 ? IsTrue(parts[7]) : false;
                filter.Arr[8] = parts.Length > 8 ? IsTrue(parts[8]) : false;
                filter.Arr[9] = parts.Length > 9 ? IsTrue(parts[9]) : false;
                filter.Arr[10] = parts.Length > 10 ? IsTrue(parts[10]) : false;
                filter.Arr[11] = parts.Length > 11 ? IsTrue(parts[11]) : false;
                filter.Arr[12] = parts.Length > 12 ? IsTrue(parts[12]) : false;

                result = filter;
                return true;
            }

            result = null;
            return false;
        }
    }

    public class DegreeFilterConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        private bool IsTrue(string s)
        {
            return s == "1" || s == "True";
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] parts = ((string)value).Split(new char[] { ',' });

                DegreeFilter filter = new DegreeFilter();
                filter.Arr[1] = parts.Length > 0 ? IsTrue(parts[0]) : false;
                filter.Arr[2] = parts.Length > 1 ? IsTrue(parts[1]) : false;
                filter.Arr[3] = parts.Length > 2 ? IsTrue(parts[2]) : false;
                filter.Arr[4] = parts.Length > 3 ? IsTrue(parts[3]) : false;
                filter.Arr[5] = parts.Length > 4 ? IsTrue(parts[4]) : false;
                filter.Arr[6] = parts.Length > 5 ? IsTrue(parts[5]) : false;
                filter.Arr[7] = parts.Length > 6 ? IsTrue(parts[6]) : false;
                filter.Arr[8] = parts.Length > 7 ? IsTrue(parts[7]) : false;
                filter.Arr[9] = parts.Length > 8 ? IsTrue(parts[8]) : false;
                filter.Arr[10] = parts.Length > 9 ? IsTrue(parts[9]) : false;
                filter.Arr[11] = parts.Length > 10 ? IsTrue(parts[10]) : false;
                filter.Arr[12] = parts.Length > 11 ? IsTrue(parts[11]) : false;
                return filter;
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                DegreeFilter filter = value as DegreeFilter;
                return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                    filter.Arr[1], filter.Arr[2], filter.Arr[3], filter.Arr[4], filter.Arr[5], filter.Arr[6],
                    filter.Arr[7], filter.Arr[8], filter.Arr[9], filter.Arr[10], filter.Arr[11], filter.Arr[12]);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    /// <summary>
    /// The type used for degree settings in item filters
    /// </summary>
    [TypeConverter(typeof(DegreeFilterConverter))]
    [SettingsSerializeAs(SettingsSerializeAs.String)]
    public class DegreeFilter : INotifyPropertyChanged
    {
        private bool[] arr;

        public bool[] Arr { get => arr; set { arr = value; } }

        /// <summary>
        /// Empty constructor for settings initialization
        /// </summary>
        public DegreeFilter() { SetupArray(); }

        public DegreeFilter(bool[] array)
        {
            arr = new bool[13];

            if (array.Length < arr.Length)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    arr[i] = array[i];
                }
                for (int i = array.Length; i < arr.Length; i++)
                {
                    arr[i] = array[i];
                }
            }
            else
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = array[i];
                }
            }

            Arr = arr;
        }

        public void SetupArray()
        {
            arr = new bool[13]
            {
                true, // for items without degree -> degree == 0
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                true,
                true,
                true
            };
        }

        public bool this[int index]
        {
            get { return arr[index]; }
            //set { arr[index] = value; NotifyPropertyChanged(); }
        }

        public override string ToString() => string.Join(",", Array.ConvertAll(Arr, i => i.ToString()));

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
