﻿using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using System.Linq;

namespace LobotAPI.Structs
{
    public static class ShoppingList
    {
        /// <summary>
        /// /// Find number of consumables of the same or superior type in the inventory.
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="quantity"></param>
        /// <param name="digits">Number of digits to take into acount at the end of the item's long ID.</param>
        /// <returns></returns>
        private static int GetRemainingTypeSameOrBetter(uint itemID, int quantity, int digits = 2)
        {
            PK2.Pk2Item item = PK2DataGlobal.Items[itemID];
            string itemTypeSignature = item.LongId.Substring(0, item.LongId.Length - digits);
            int itemTypeIndex = int.Parse(item.LongId.Substring(item.LongId.Length - digits));
            // Get sum of all items with the same signature and same/better type index
            int stacksOfSameOrBetter = (int)CharInfoGlobal.Inventory.Where(entry =>
               entry.Value.LongID.Contains(itemTypeSignature)
               && int.Parse(entry.Value.LongID.Substring(entry.Value.LongID.Length - digits)) >= itemTypeIndex)
                .Sum(i => i.Value.StackCount);
            int result = quantity - stacksOfSameOrBetter;

            return result >= 0 ? result : 0;
        }

        /// <summary>
        /// Find number of consumables of the same in the inventory.
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        private static int GetRemainingType(uint itemID, int quantity)
        {
            PK2.Pk2Item item = PK2DataGlobal.Items[itemID];
            int amountInEquipment = (int)CharInfoGlobal.Equipment.Where(entry => entry.Value.LongID.Contains(item.LongId)).Sum(entry => entry.Value.StackCount);
            int amountInInventory = (int)CharInfoGlobal.Inventory.Where(entry => entry.Value.LongID.Contains(item.LongId)).Sum(entry => entry.Value.StackCount);
            int result = quantity - amountInEquipment - amountInInventory;

            return result >= 0 ? result : 0;
        }

        public static int HPPotionQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.HPPotionType, ShoppingSettings.HPPotionQuantity); } }
        public static int MPPotionQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.MPPotionType, ShoppingSettings.MPPotionQuantity); } }
        public static int VigorPotionQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.VigorPotionType, ShoppingSettings.VigorPotionQuantity); } }
        public static int UniversalPillQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.UniversalPillType, ShoppingSettings.UniversalPillQuantity); } }
        public static int PurificationPillQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.PurificationPillType, ShoppingSettings.PurificationPillQuantity); } }

        public static int ProjectileQuantity { get { return GetRemainingType(ShoppingSettings.ProjectileType, ShoppingSettings.ProjectileQuantity); } }
        public static int SpeedPotQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.SpeedPotType, ShoppingSettings.SpeedPotQuantity); } }
        public static int ReturnScrollQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.ReturnScrollType, ShoppingSettings.ReturnScrollQuantity); } }
        public static int TransportQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.TransportType, ShoppingSettings.TransportQuantity, 1); } }
        public static int BeserkPotionQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.BeserkPotionType, ShoppingSettings.BeserkPotionQuantity); } }

        public static int PetHPPotionQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.PetHPPotionType, ShoppingSettings.PetHPPotionQuantity); } }
        public static int PetPillQuantity { get { return GetRemainingTypeSameOrBetter(ShoppingSettings.PetPillType, ShoppingSettings.PetPillQuantity); } }
        public static int HGPPotionQuantity { get { return GetRemainingType(ShoppingSettings.PetHGPID, ShoppingSettings.HGPPotionQuantity); } }
        public static int GrassOfLifeQuantity { get { return GetRemainingType(ShoppingSettings.PetGrassOfLifeID, ShoppingSettings.GrassOfLifeQuantity); } }
    }
}
