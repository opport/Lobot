﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using static LobotAPI.Structs.WhiteAttributes;

namespace LobotAPI.Structs
{
    public class Item : INotifyPropertyChanged, ICloneable
    {
        public class RentItem
        {
            public uint PeriodBeginTime;
            public uint PeriodEndTime;
            public uint CanDelete;
            public uint MeterRateTime;
            public uint CanRecharge;
            public uint PackingTime;
        }

        public class Option
        {
            public uint Slot;
            public uint ID;
            public uint NParam1; //(=> Reference to Socket)
            public uint OptValue;
        }

        /// <summary>
        /// Blue attributes granted by jade tablets.
        /// <seealso href="https://www.elitepvpers.com/forum/sro-pserver-guides-releases/2946309-share-guide-information-about-white-100-common-special-blue-states.html"/>
        /// </summary>
        public enum MagParamType : uint
        {
            Immortal = 0x30, // 30 00 00 00 05 00 00 00 = immortal 5
            Steady = 0x38, //38 00 00 00 06 00 00 00 = steady 6
            Lucky = 0x3B, // 3B 00 00 00 01 00 00 00 = lucky 1
            Str = 0x48, // 30064771144 = Str 7
            Int = 0x49, // 30064771150 = Int 7
            Durability = 0x5A,  // 858993459290 = Durability 200%
            HP = 0x45,  // ??? = Durability ???%
            MP = 0x4B,  // ??? = Durability 200%
            AttackRatio = 0x66, // 257698037862 = Attack Rate 60%
            BlockRatio = 0x72,  // 429496729714 = Blocking Rate 100
            CriticalBlock = 0x7E,  // 429496729726 = Critical Block 100
            ParryRatio = 0x8A,  // 257698037898 = Parry Rate 60%
            Burn = 0xA8, //  85899346088 = Burn 20%
            ElectricShock = 0xAE, // 85899346094 = Electric Shock 20%
            Freeze = 0xB4,  //85899346100 = Freeze 20%
            Poison = 0xBA, // 85899346106 = Poison 20%
            Zombie = 0xC0, // 85899346112 = Zombie 20%
        }

        public class MagParam
        {
            public uint Str { get; set; }
            public uint Int { get; set; }
            public uint Durability { get; set; }
            public uint HP { get; set; }
            public uint MP { get; set; }
            public uint HitRatio { get; set; }
            public uint BlockRatio { get; set; }
            public uint CriticalBlock { get; set; }
            public uint ParryRatio { get; set; }
            public uint Burn { get; set; }
            public uint Shock { get; set; }
            public uint Freeze { get; set; }
            public uint Poison { get; set; }
            public uint Zombie { get; set; }

            public MagParamType MagParamType;
            public uint MagParamValue;

            public MagParam() { }

            public void ChangeValue(MagParamType magParamType, uint magParamValue)
            {
                switch (magParamType)
                {
                    case MagParamType.Str:
                        Str = magParamValue;
                        break;
                    case MagParamType.Int:
                        Int = magParamValue;
                        break;
                    case MagParamType.HP:
                        HP = magParamValue;
                        break;
                    case MagParamType.MP:
                        MP = magParamValue;
                        break;
                    case MagParamType.Durability:
                        Durability = magParamValue;
                        break;
                    case MagParamType.AttackRatio:
                        HitRatio = magParamValue;
                        break;
                    case MagParamType.BlockRatio:
                        BlockRatio = magParamValue;
                        break;
                    case MagParamType.CriticalBlock:
                        CriticalBlock = magParamValue;
                        break;
                    case MagParamType.ParryRatio:
                        ParryRatio = magParamValue;
                        break;
                    case MagParamType.Burn:
                        Burn = magParamValue;
                        break;
                    case MagParamType.ElectricShock:
                        Shock = magParamValue;
                        break;
                    case MagParamType.Freeze:
                        Freeze = magParamValue;
                        break;
                    case MagParamType.Poison:
                        Poison = magParamValue;
                        break;
                    case MagParamType.Zombie:
                        Zombie = magParamValue;
                        break;
                    default:
                        break;
                }
            }
        }

        public Pk2Item Pk2Item { get { return PK2DataGlobal.Items[RefItemID]; } }
        public uint RentType = 0;
        public RentItem Rent = new RentItem();
        public byte Slot { get; set; }
        public uint RefItemID;
        public string LongID { get { return Pk2Item.LongId; } }
        public int Level { get { return Pk2Item.Level; } }
        public int MaxStacks { get { return Pk2Item.MaxStacks; } }
        public int Degree { get { return Pk2Item.Degree; } }
        public Gender Gender { get { return Pk2Item.Gender; } }
        public Race Race { get { return Pk2Item.Race; } }
        public bool IsSOX { get { return Pk2Item.IsSOX; } }
        public bool IsStorable { get { return Pk2Item.IsStorable; } }
        public bool IsMallItem { get { return Pk2Item.IsMallItem; } }

        private uint _stackCount = 1;
        public uint StackCount { get => _stackCount; set { _stackCount = value; NotifyPropertyChanged(); } }
        public uint AttributeAssimilationProbability;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Item(uint refID = 0, uint stackCount = 1)
        {
            RefItemID = refID;
            StackCount = stackCount;
        }

        public Item(Pk2Item pk2Item) : this(pk2Item.Id)
        {
        }

        public Item(Item item)
        {
            RentType = item.RentType;
            Slot = item.Slot;
            RefItemID = item.RefItemID;
            StackCount = item.StackCount;
            AttributeAssimilationProbability = item.AttributeAssimilationProbability;
        }

        /// <summary>
        /// The Name property displayed in the GUI inventory list.
        /// </summary>
        public string ItemName { get { return Pk2Item.Name; } }
        /// <summary>
        /// The Quantity property displayed in the GUI inventory list.
        /// </summary>
        public string Quantity { get { return StackCount.ToString(); } }
        /// <summary>
        /// The Type property displayed in the GUI inventory list.
        /// </summary>
        public Pk2ItemType Type { get { return Pk2Item.Type; } }
        public string SellOrStoreString { get { return Pk2Item.ShouldSell() ? "Sell" : Pk2Item.ShouldStore() ? "Store" : "Keep"; } }
        public override string ToString() => Pk2Item.ToString();

        public virtual object Clone() => new Item(this);
    }

    public class Equipment : Item
    {
        /// <summary>
        /// The number of plus from elixirs 
        /// </summary>
        public byte OptLevel { get; set; }
        public byte EffectiveLevel => (byte)(Level + OptLevel);
        public Option ItemOption;
        /// <summary>
        /// The variance of white attributes on item
        /// </summary>
        public WhiteAttributes Attributes { get; set; }
        public uint Durability;
        public byte MagParamNum;
        public MagParam Blues = new MagParam();

        /// <summary>
        /// String representation for Alchemy tab in GUI
        /// </summary>
        public string AlchemySettingsString { get { return $"(+{OptLevel}){Pk2Item.Name}"; } }

        public Equipment(Item i)
        {
            this.RentType = i.RentType;
            this.Rent = i.Rent;
            this.Slot = i.Slot;
            this.RefItemID = i.RefItemID;
            this.StackCount = i.StackCount;
            this.AttributeAssimilationProbability = i.AttributeAssimilationProbability;
            this.ItemOption = new Option();
        }

        public override object Clone() => new Equipment(this);

        public AttributeType GetAttributeType()
        {
            if (Pk2Item.IsWeapon())
            {
                return AttributeType.Weapon;
            }
            else if (Pk2Item.IsShield())
            {
                return AttributeType.Shield;
            }
            else if (Pk2Item.IsAccessory())
            {
                return AttributeType.Accessory;
            }
            else
            {
                return AttributeType.Equipment;
            }
        }
    }

    public enum StallItemStatus : byte
    {
        InStall = 0x00,
        Reserved = 0x01
    }

    public class StallItem : INotifyPropertyChanged, IEquatable<Item>, IEquatable<StallItem>
    {
        public byte _slot = 0;
        public ushort _stackCount = 1;
        public ulong _price = 0;
        public StallItemStatus _status;

        public byte Slot { get => _slot; set { _slot = value; NotifyPropertyChanged(); } }
        public ushort StackCount { get; }
        public ulong Price { get => _price; set { _price = value; NotifyPropertyChanged(); } }
        public StallItemStatus Status { get => _status; set { _status = value; NotifyPropertyChanged(); } }
        public string Name { get => InventoryItem.ItemName; }

        public Item InventoryItem;

        public StallItem(Item inventoryItem, byte slot = 0, ulong price = 0)
        {
            InventoryItem = inventoryItem;
            Slot = slot;
            StackCount = (ushort)InventoryItem.StackCount;
            Price = price;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override string ToString()
        {
            return InventoryItem is Equipment ? $"[Lvl {(InventoryItem as Equipment).Level}] {Name}(+{(InventoryItem as Equipment).OptLevel})" : Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is StallItem)
            {
                return Equals((obj as StallItem).InventoryItem);
            }
            else if (obj is Item)
            {
                return Equals(obj as Item);
            }
            else if (byte.TryParse(obj as string, out byte result)) // slot number
            {
                return InventoryItem.Slot == result;
            }
            else
            {
                return false;
            }
        }

        public bool Equals(Item obj)
        {
            return InventoryItem.Slot == obj.Slot;
        }

        public bool Equals(StallItem other)
        {
            return other != null &&
                   _slot == other._slot &&
                   EqualityComparer<Item>.Default.Equals(InventoryItem, other.InventoryItem);
        }

        public override int GetHashCode()
        {
            var hashCode = 259477554;
            hashCode = hashCode * -1521134295 + _slot.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<Item>.Default.GetHashCode(InventoryItem);
            return hashCode;
        }
    }

    public enum PetStatus : byte
    {
        Unsummoned = 1,
        Summoned = 2,
        Alive = 3,
        Dead = 4
    }

    public class PetScroll : Item
    {
        public PetStatus Status; //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
        public uint UniqueID;
        public ushort NameLength;
        public string Name;
        public byte unk02;

        public PetScroll(Item i)
        {
            this.RentType = i.RentType;
            this.Rent = i.Rent;
            this.Slot = i.Slot;
            this.RefItemID = i.RefItemID;
            this.StackCount = i.StackCount;
            this.AttributeAssimilationProbability = i.AttributeAssimilationProbability;
        }

        public override object Clone() => new PetScroll(this);
    }

    public class AbilityPet : PetScroll
    {
        public ulong SecondsToRentEndTime;

        public AbilityPet(Item i) : base(i) { }

        public override object Clone() => new AbilityPet(this);
    }

    public class MagicCube : Item
    {
        public ulong StoredItemCount;

        public MagicCube(Item i)
        {
            this.RentType = i.RentType;
            this.Rent = i.Rent;
            this.Slot = i.Slot;
            this.RefItemID = i.RefItemID;
            this.StackCount = i.StackCount;
            this.AttributeAssimilationProbability = i.AttributeAssimilationProbability;
        }

        public override object Clone() => new MagicCube(this);
    }

    public class ItemExchangeCoupon : Item
    {
        public List<ulong> MagParamValues = new List<ulong>();

        public ItemExchangeCoupon(Item i) : base(i) { }

        public override object Clone() => new ItemExchangeCoupon(this);
    }

}
