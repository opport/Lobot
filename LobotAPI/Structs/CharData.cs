﻿using Config.Net;
using LobotAPI.Globals;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;

namespace LobotAPI.Structs
{
    public class AvatarInventory
    {
        public byte Size;
        public List<Item> Items = new List<Item>();
        public byte ItemCount;
    }

    public class BlockedPlayer
    {
        public string TargetName;
        public ushort TargetNameLength;
    }

    public class Buff
    {
        public uint TargetID;
        public uint RefSkillID;
        public uint BuffDuration;
        public uint RefSkillParam;
        public byte IsCreator;
        public uint UniqueID;
        public Pk2Skill Pk2Entry => PK2DataGlobal.Skills[RefSkillID];
        public Pk2SkillType Type => Pk2Entry.Type;

        public Buff(uint refSkillID)
        {
            RefSkillID = refSkillID;
        }

        public override string ToString()
        {
            return Pk2Entry.ToString();
        }
    }

    public class Hotkey
    {
        public byte SlotSeq;
        public byte SlotType; //(37 = COS Command, 70 = InventoryItem, 71 = EquipedItem, 73 = Skill, 74 = Action, 78 = EquipedAvatar)
        public uint Data;
    }

    public class Inventory
    {
        public byte Size;
        public byte ItemCount;
        public List<Item> Items = new List<Item>();
        public AvatarInventory AvatarInventory = new AvatarInventory();
    }

    public class Mastery
    {
        public uint ID;
        public byte Level;
    }

    public class Quest
    {
        public class Objective
        {
            public byte ID;
            public byte Status; //(00 = done, 01 = incomplete)
            public ushort NameLength;
            public string Name;
            public List<uint> Tasks = new List<uint>();
            public byte AchievementCount;
            public byte TaskCount;
        }

        public uint RefID;
        public byte AchievementCount;
        public byte RequiresSharePt;
        public byte Type;
        public byte Status;
        public List<Objective> Objectives = new List<Objective>();
        public byte ObjectiveCount;
        public byte TaskCount;
        public List<uint> TaskRefObjID = new List<uint>(); //(=> NPCs to deliver to, when complete you get reward)
    }

    class SkillParser : ITypeParser
    {
        public IEnumerable<Type> SupportedTypes => new[] { typeof(Skill) };

        public string ToRawString(object value)
        {
            if (value == null || !(value is Skill)) return null;

            return (value as Skill).RefSkillID.ToString();
        }

        public bool TryParse(string value, Type t, out object result)
        {
            if (value == null)
            {
                result = null;
                return false;
            }

            if (t == typeof(Skill))
            {
                bool IsParsed = uint.TryParse(value, out uint refID);
                result = new Skill(refID);
                return IsParsed;
            }

            result = null;
            return false;
        }
    }

    /// <summary>
    /// This class is used to serialize skills by simply using their RefID
    /// <seealso href="https://stackoverflow.com/a/22355712/9597363"/>
    /// </summary>
    public class SkillJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue((value as Skill).RefSkillID);
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(Skill) && uint.TryParse(reader.Value.ToString(), out uint refID))
            {
                return new Skill(refID);
            }
            else
            {
                throw new JsonException($"\"{(string)reader.Value}\" is not a valid uint for a skill.");
            }
        }
    }

    [JsonConverter(typeof(SkillJsonConverter))]
    [Serializable]
    [SettingsSerializeAs(SettingsSerializeAs.String)]
    public class Skill : IEquatable<Skill>
    {
        [JsonConverter(typeof(SkillJsonConverter))]
        public uint RefSkillID;
        [IgnoreDataMemberAttribute]
        public bool IsAutoAttack = false;
        [IgnoreDataMemberAttribute]
        public string Name => Pk2Skill.Name;
        [IgnoreDataMemberAttribute]
        public Pk2SkillType Type => Pk2Skill.Type;
        [IgnoreDataMemberAttribute]
        public int CastTime => Pk2Skill.CastTime;
        [IgnoreDataMemberAttribute]
        public int Cooldown => Pk2Skill.Cooldown;
        [IgnoreDataMemberAttribute]
        public byte SkillEnabled = 1;
        [IgnoreDataMemberAttribute]
        public Pk2Skill Pk2Skill => PK2DataGlobal.Skills[RefSkillID];
        [IgnoreDataMemberAttribute]
        public Pk2ItemType RequiredWeapon => Pk2Skill.RequiredWeapon;
        [IgnoreDataMemberAttribute]
        public EquipmentSlot RequiredWeaponSlot => Pk2Skill.RequiredWeapon.GetRequiredWeaponSlot();

        /// <summary>
        /// Empty constructor for settings initialization
        /// </summary>
        public Skill() { }

        public Skill(uint refID, byte skillEnable = 1, bool isAutoAttack = false)
        {
            RefSkillID = refID;
            SkillEnabled = skillEnable;
            IsAutoAttack = isAutoAttack;
        }

        public override string ToString() => $"{RefSkillID}, {Name}, {Type}, Weapon: {RequiredWeapon}, Enabled: {SkillEnabled}";

        public override bool Equals(object obj)
        {
            return Equals(obj as Skill);
        }

        public bool Equals(Skill other)
        {
            return other != null &&
                   RefSkillID == other.RefSkillID &&
                   Name == other.Name;
        }

        public override int GetHashCode()
        {
            var hashCode = 1479869798;
            hashCode = hashCode * -1521134295 + RefSkillID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            return hashCode;
        }
    }

    public partial class CharData : ISpawnType, ICombatType, INotifyPropertyChanged
    {
        public uint ServerTime;

        private uint _refID;
        public uint RefID { get => _refID; set { _refID = value; NotifyPropertyChanged(); } }
        public byte Scale;

        private byte _level;
        public byte Level { get => _level; set { _level = value; NotifyPropertyChanged(); } } // Property for UI WPF purposes
        public byte MaxLevel;
        private ulong expOffset;
        public ulong ExpOffset { get => expOffset; set { expOffset = value; NotifyPropertyChanged("ExpPercent"); } } // Notify to update UI EXP bar
        public double ExpPercent { get => PK2DataGlobal.LevelData != null && PK2DataGlobal.LevelData.TryGetValue(Level, out ulong lvlEXP) ? (double)(ExpOffset * 100 / (double)lvlEXP) : 100; } // exp rounded up to second decimal place
        public uint SExpOffset;
        public ulong RemainGold;
        private uint remainSkillPoint;
        public uint RemainSkillPoint { get => remainSkillPoint; set { remainSkillPoint = value; NotifyPropertyChanged(); } } // Property for UI WPF purposes
        public ushort RemainStatPoint;

        private byte remainHwanCount = 0;
        public byte RemainHwanCount
        {
            get { return remainHwanCount; }
            set { remainHwanCount = value; if (remainHwanCount == HwanLevel) { OnZerkReady(EventArgs.Empty); NotifyPropertyChanged(); } }
        }
        public uint GatheredExpPoint;
        public int MaxHP { get; set; }
        private uint currentHP;
        private uint currentMP;
        public uint CurrentHP { get => currentHP; set { currentHP = value; NotifyPropertyChanged("CurrentHPPercent"); } } // Notify to update UI HP bar
        public uint CurrentHPPercent { get => currentHP == 0 || Bot.CharInfo.MaxHP == 0 ? 100 : (currentHP * 100) / Bot.CharInfo.MaxHP; }
        public uint CurrentMP { get => currentMP; set { currentMP = value; NotifyPropertyChanged("CurrentMPPercent"); } } // Notify to update UI MP bar
        public uint CurrentMPPercent { get => currentMP == 0 || Bot.CharInfo.MaxMP == 0 ? 100 : (currentMP * 100) / Bot.CharInfo.MaxMP; }

        public byte AutoInverstExp; //(1 = Beginner Icon, 2 = Helpful, 3 = Beginner&Helpful)
        public byte DailyPK;
        public ushort TotalPK;
        public uint PKPenaltyPoint;
        public byte HwanLevel;
        public byte FreePVP; // -> Check for != 0
        public Inventory Inventory = new Inventory();
        public bool CharacterInventoryIsFull
        {
            get
            {
                int count = 0;
                foreach (KeyValuePair<byte, Item> item in CharInfoGlobal.Inventory)
                {
                    if (item.Key >= CharInfoGlobal.EQUIPMENT_SLOTS)
                    {
                        count++;
                    }
                }
                return count == (Bot.CharData.Inventory.Size - 13);
            }
        }
        public byte HasMask; // -> Check for != 0 (MaskFlag?)
        public byte MasteryFlag; //[0 = done, 1 = Mastery]
        public Dictionary<uint, Mastery> Masteries = new Dictionary<uint, Mastery>();
        public byte SkillFlag; //[0 = done, 1 = Skill]
        public List<Skill> Skills = new List<Skill>();
        public ushort CompletedQuestCount;
        public List<uint> CompletedQuest = new List<uint>();
        public byte ActiveQuestCount;
        public List<Quest> ActiveQuests = new List<Quest>();
        public byte QuestAchievementCount;  //(Repetition Amount = Bit && Completetion Amount = Bit)
        public byte QuestRequiresAutoShareParty; // -> Check for != 0
        public byte QuestType; // (8 = , 24 = , 88 = )
        public byte QuestStatus; //(1 = Untouched, 7 = Started, 8 = Complete)
        public byte QuestObjectiveCount;
        public byte unk05; // -> Check for != 0
        public uint CollectionBookCount; // -> Check for != 0

        private uint _uniqueID;
        public uint UniqueID { get => _uniqueID; set { _uniqueID = value; NotifyPropertyChanged(); } }
        public byte CaveXSec; // optional
        public byte CaveYSec; // optional
        public byte XSec = 0; // 135; // (=> XSec; YSec)
        public byte YSec = 0;  // 92;
        public float XOffset = 0;
        public float ZOffset = 0;
        public float YOffset = 0;
        //public uint angle;
        public byte HasDestination;
        public bool IsInCave { get { return (XSec << 8 | YSec) >= ushort.MaxValue; } }
        public byte MovementType;  //(0 = Walking, 1 = Running)
        public byte DestXSec;
        public byte DestYSec;
        public uint DestX;
        public uint DestZ;
        public uint DestY;
        public byte SkyClickFlag; //(1 = Sky-/ArrowKey-walking)
        public ushort Angle;
        public bool IsAlive { get; set; }  //(1 = Alive, 2 = Dead)
        public DebuffStatus DebuffState; // -> Check for != 0
        public byte MotionState; //(0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
        private ObjectStatus _status;
        public ObjectStatus Status
        {
            get => _status;
            set
            {
                ObjectStatus prev = _status;
                _status = value;

                if (prev != value)
                {
                    ObjectStatusChangedArgs e = new ObjectStatusChangedArgs(_status, value);
                    OnCharStatusChanged(e);
                }
            }
        } //(0 = None, 1 = Zerk, 2 = Untouchable, 3 = GMInvincible, 4 = GMInvisible, 5= ??, 6 = Stealth, 7 = Invisible)
        public float CurrentSpeed { get { return MovementType == 0x00 ? WalkSpeed : RunSpeed; } } // For calculating time to travel a distance with current speed
        public float WalkSpeed = 22;
        public float RunSpeed = 55;
        public float HwanSpeed;
        public byte ActiveBuffCount;
        public List<Buff> ActiveBuffs = new List<Buff>();
        private string name = "";
        public string Name { get { return name; } set { name = value; OnNameChanged(EventArgs.Empty); NotifyPropertyChanged(); } } // Property for UI WPF purposes
        public string NameFull { get { return $"{Name} (Lvl {LevelByEquipment()})" + (GuildInfo == "" ? "" : $"[{GuildInfo}]"); } } // Property for UI WPF purposes
        public ushort JobNameLength;
        public string JobName;
        public byte JobType; // (0 = None, 1 = Trader, 2 = Thief, 3 = Hunter)	
        public byte JobLevel;
        public uint JobExp;
        public uint JobContribution;
        public uint JobReward;
        private string guildName = "";
        public string GuildName { get => guildName; set { guildName = value; NotifyPropertyChanged(); } } // Property for UI WPF purposes
        public string GuildGrantName;
        public uint GuildID;
        public uint GuildLastCrest;
        public uint UnionID;
        public uint UnionLastCrest;
        public byte GuildIsFriendly; //0 = Hostile, 1 = Friendly
        public byte GuildSeigeAuthority;
        public string GuildInfo { get { return string.IsNullOrEmpty(GuildGrantName) ? GuildName : GuildName + " - " + GuildGrantName; } }

        public Point Position { get => new Point(ScriptUtils.GetXCoord(XOffset, XSec), ScriptUtils.GetYCoord(YOffset, YSec)); set { CalculateOffsetCoordinates(value); OnPositionChanged(EventArgs.Empty); NotifyPropertyChanged(); } }
        public byte ScrollMode; //0 = None, 1 = Return Scroll, 2 = Bandit Return Scroll
        public byte InteractionMode; // 4 if stall is open //0 = None 2 = P2P, 4 = P2N_TALK, 6 = OPNMKT_DEAL
        public byte PVPState; // -> Check for != 0	(According to Spawn structure => MurderFlag?)
        public byte TransportFlag; // -> Check for != 0	(According to Spawn structure => RideFlag or AttackFlag?)
        public uint TransportUniqueID;
        public byte InCombat; // -> Check for != 0	(According to Spawn structure => EquipmentCountdown?)
        public byte PVPFlag; // Flag(255 = Disable, 34 = Enable)
        public ulong GuideFlag;
        public uint AccountID; // (=> GameAccountID)
        public byte GMFlag;
        public byte ActivationFlag;
        public byte HotkeyCount;
        public List<Hotkey> Hotkeys = new List<Hotkey>();
        public byte HPSlot; // (FValue  10 + Slot)
        public byte HPValue; // (Enabled = 128 + Value)
        public byte MPSlot; //(FValue  10 + Slot)
        public byte MPValue; //(Enabled = 128 + Value)
        public byte UniversalSlot; //(Enabled = 128 + Value)
        public byte UniversalValue; //(Enabled = 128,0 = Disabled)
        public byte PotionDelay;  //(Enabled = 128 + Value)
        //
        public byte BlockedPlayCount;
        public List<BlockedPlayer> BlockedPlayers = new List<BlockedPlayer>();
        //public ushort TargetNameLength;
        public uint unk13;
        public byte unk14;

        public bool IsKnockedDown { get; set; } // TODO check if any of the unknown bytes changes when chardata is loaded for knockeddown player
        public bool IsUsingZerk { get; set; }

        private byte LevelByEquipment()
        {
            int maxItemLevel = Inventory.ItemCount > 0
                ? Inventory.Items.Aggregate((i1, i2) => i1.Level > i2.Level ? i1 : i2).Level
                : 0;
            return (byte)Math.Max(Level, maxItemLevel);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
