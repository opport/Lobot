using System.Windows;

namespace LobotAPI.Structs
{
    public class Portal : ISpawnType
    {
        public string Name
        {
            get
            {
                if (PK2Data.Name != null)
                {
                    return PK2Data.Name;
                }
                else
                {
                    return RefID.ToString();
                }
            }
        }

        public Point Position { get; set; }
        public uint RefID { get; set; } // ID for game class
        public uint UniqueID { get; set; } // ID for instance of class
        public PK2.Pk2Teleporter PK2Data;
    }
}
