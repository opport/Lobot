﻿namespace LobotAPI.Structs
{
    public interface ICombatType
    {
        uint CurrentHP { get; set; }
        int MaxHP { get; set; }
        bool IsAlive { get; set; }
        bool IsKnockedDown { get; set; }
    }
}
