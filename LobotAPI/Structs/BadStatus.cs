﻿namespace LobotAPI.Structs
{
    /// <summary>
    /// List of bad status for units.
    /// 
    //disease     00 80 00 00
    //bleed       00 08 00 00
    //dull        00 01 00 00
    //division    00 00 10 00
    //weaken      00 00 04 00
    //impotent    00 00 08 00
    //decay       00 00 02 00
    //attack-rate
    //parry-rate


    //sleep           40 00 00 00
    //hidden(trap)    00 00 00 01
    //bind

    //stun            00 40 00 00
    //wheel bind(panic)   00 00 20 00
    //manadrain(combustion)
    //Shortsight          00 04 00 00

    //e.g.division and weaken 00 00 14 00

    //division    00 00 10 00
    //weaken      00 00 04 00

    //[S -> C] [3057]
    //65 44 F9 01                                       eD..............
    //00 01                                             ................
    //05                                                ................
    //72 0C 00 00                                       r...............
    //00 00 14 00                                       ................
    //05                                                ................
    //05                                                ................
    /// </summary>
    public enum BadStatus : uint
    {
        Normal = 0x00,
        Fire = 0x01,
        Ice = 0x02,
        Freeze = 0x03,
        Electricity = 0x04,
        Fire2 = 0x08,
        Poison = 0x10,

        Sleep = 0x40,        // 40 00 00 00
        Dull = 0x0100,       // 00 01 00 00
        Shortsight = 0x0400, // 00 04 00 00
        Bleed = 0x0800,      // 00 08 00 00
        Darkness = 0x2000,   // 00 20 00 00
        Stun = 0x4000,       // 00 40 00 00

        Disease = 0x8000,    // 00 80 00 00
        Decay = 0x020000,    // 00 00 02 00
        Weaken = 0x040000,   // 00 00 04 00
        Impotent = 0x080000, // 00 00 08 00
        Division = 0x100000, // 00 00 10 00
        Panic = 0x200000,    // 00 00 20 00 Wheelbind
        //AttackRate, handled as buff
        //ParryRate, handled as buff
        Trap = 0x01000000,   // 00 00 00 01 Hidden
        Bind,
        Combustion, //Mana drain
    }
}
