﻿using System;
using System.Collections.Generic;

namespace LobotAPI.Structs
{
    public class PK2ShopItemRef : IEquatable<PK2ShopItemRef>
    {
        public uint ItemID;
        public string LongID;
        public string TabName;
        public int Price;
        public int Slot;

        public PK2ShopItemRef(string longID, string tabName, string price, int slot)
        {
            LongID = longID ?? throw new ArgumentNullException(nameof(longID));
            TabName = tabName ?? throw new ArgumentNullException(nameof(tabName));
            Price = int.Parse(price);
            Slot = slot;
        }

        public override bool Equals(object obj)
        {
            var @ref = obj as PK2ShopItemRef;
            return @ref != null &&
                   ItemID == @ref.ItemID &&
                   LongID == @ref.LongID;
        }

        public bool Equals(PK2ShopItemRef other)
        {
            return other != null &&
                   ItemID == other.ItemID &&
                   LongID == other.LongID &&
                   TabName == other.TabName &&
                   Price == other.Price &&
                   Slot == other.Slot;
        }

        public override int GetHashCode()
        {
            var hashCode = -547369493;
            hashCode = hashCode * -1521134295 + ItemID.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(LongID);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(TabName);
            hashCode = hashCode * -1521134295 + Price.GetHashCode();
            hashCode = hashCode * -1521134295 + Slot.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4}", TabName, LongID, ItemID, Price, Slot);
        }
    }
}
