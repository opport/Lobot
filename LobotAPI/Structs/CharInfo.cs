﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LobotAPI.Structs
{
    /// <summary>
    /// //Opcode: 0x303D
    //Name: SERVER_AGENT_CHARACTER_STATS
    //Description:
    //Encryption: false
    //Massive: false
    //public const ushort SERVER_AGENT_CHARACTER_STATS = 0x303D;
    //	4	uint	PhyAtkMin
    //	4	uint	PhyAtkMax
    //	4	uint	MagAtkMin
    //	4	uint	MagAtkMax
    //	2	ushort	PhyDef
    //	2	ushort	MagDef
    //	2	ushort	HitRate
    //	2	ushort	ParryRate
    //	4	uint	MaxHP
    //	4	uint	MaxMP
    //	2	ushort	STR
    //	2	ushort	INT
    /// </summary>
    /// </summary>
    public class CharInfo : INotifyPropertyChanged
    {
        private ushort _sTR;
        private ushort _iNT;

        public uint PhyAtkMin { get; set; }
        public uint PhyAtkMax { get; set; }
        public uint MagAtkMin { get; set; }
        public uint MagAtkMax { get; set; }
        public ushort PhyDef { get; set; }
        public ushort MagDef { get; set; }
        public ushort HitRate { get; set; }
        public ushort ParryRate { get; set; }
        public uint MaxHP { get; set; }
        public uint MaxMP { get; set; }
        public ushort STR { get => _sTR; set { _sTR = value; NotifyPropertyChanged(); } }
        public ushort INT { get => _iNT; set { _iNT = value; NotifyPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
