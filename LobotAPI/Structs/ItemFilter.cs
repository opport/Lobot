﻿using Config.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace LobotAPI.Structs
{
    class ItemFilterParser : ITypeParser
    {
        public IEnumerable<Type> SupportedTypes => new[] { typeof(ItemFilter) };

        public string ToRawString(object value)
        {
            if (value == null) return null;

            return value.ToString();
        }

        private bool IsTrue(string s)
        {
            return s == "1" || s == "True";
        }

        public bool TryParse(string value, Type t, out object result)
        {
            if (value == null)
            {
                result = null;
                return false;
            }

            if (t == typeof(ItemFilter))
            {
                string[] parts = ((string)value).Split(new char[] { ',' });

                // badly formed serialized item filter
                if (parts.Length < 4)
                {
                    result = default(ItemFilter);
                    return false;
                }

                ItemFilter filter = new ItemFilter();
                filter.Pick = parts.Length > 0 ? IsTrue(parts[0]) : false;
                filter.Store = parts.Length > 1 ? IsTrue(parts[1]) : false;
                filter.Sell = parts.Length > 2 ? IsTrue(parts[2]) : false;
                filter.Drop = parts.Length > 3 ? IsTrue(parts[3]) : false;

                result = filter;
                return true;
            }

            result = null;
            return false;
        }
    }

    public class ItemFilterConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        private bool IsTrue(string s)
        {
            return s == "1" || s == "True";
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] parts = ((string)value).Split(new char[] { ',' });
                ItemFilter filter = new ItemFilter();
                filter.Pick = parts.Length > 0 ? IsTrue(parts[0]) : false;
                filter.Store = parts.Length > 1 ? IsTrue(parts[1]) : false;
                filter.Sell = parts.Length > 2 ? IsTrue(parts[2]) : false;
                filter.Drop = parts.Length > 3 ? IsTrue(parts[3]) : false;
                return filter;
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                ItemFilter filter = value as ItemFilter;
                return string.Format("{0},{1},{2},{3}", filter.Pick.ToString(), filter.Store.ToString(), filter.Sell.ToString(), filter.Drop.ToString());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    /// <summary>
    /// The type used for filter settings.
    /// </summary>
    [TypeConverter(typeof(ItemFilterConverter))]
    [SettingsSerializeAs(SettingsSerializeAs.String)]
    public class ItemFilter : INotifyPropertyChanged
    {
        public enum FilterProperty
        {
            Pick = 0,
            Store = 1,
            Drop = 2,
            Sell = 3
        }

        private bool _pick;
        private bool _store;
        private bool _sell;
        private bool _drop;

        public bool Pick { get => _pick; set { _pick = value; NotifyPropertyChanged(); } }
        public bool Store { get => _store; set { _store = value; NotifyPropertyChanged(); } }
        public bool Sell { get => _sell; set { _sell = value; NotifyPropertyChanged(); } }
        public bool Drop { get => _drop; set { _drop = value; NotifyPropertyChanged(); } }

        /// <summary>
        /// Empty constructor for settings initialization
        /// </summary>
        public ItemFilter() { }

        public ItemFilter(bool pick = false, bool store = false, bool sell = false, bool drop = false)
        {
            Pick = pick;
            Store = store;
            Sell = sell;
            Drop = drop;
        }

        public override string ToString() => $"{_pick},{_store},{_sell},{_drop}";

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
