﻿using LobotAPI.Globals;

namespace LobotAPI.Structs
{
    public struct ShopItemEntry
    {
        //ShopNPCID + "," + tabNumber + "," + slotNumber++ + "," + item.ItemID + "," + item.Price
        public uint ItemID;
        public byte TabNumber;
        public byte SlotNumber;
        public int Price;
        public int MaxStacks { get { return PK2DataGlobal.Items[ItemID].MaxStacks; } }

        public ShopItemEntry(uint itemID, byte tabNumber, byte slotNumber, int price)
        {
            ItemID = itemID;
            TabNumber = tabNumber;
            SlotNumber = slotNumber;
            Price = price;
        }

        public override string ToString()
        {
            return ItemID + "," + TabNumber + "," + SlotNumber + "," + Price;
        }
    }
}
