﻿using System;
using System.Collections.Generic;

namespace LobotAPI.Structs
{
    public class PartyEntry : IEquatable<PartyEntry>
    {
        public uint PartyNumber { get; set; }
        public byte MemberCount { get; set; }
        public byte MaxMembers { get { return Type.GetMaxMembers(); } }
        public bool IsFull { get { return MemberCount == Type.GetMaxMembers(); } }
        public uint LeaderID { get; set; }
        public string Leader { get; set; } = "";
        public string Title { get; set; } = Globals.Settings.PartySettings.Title;
        public byte Race { get; set; }
        public PartyType Type { get; set; }
        public string TypeString { get { return Type.ToFriendlyString(); } }
        public PartyObjective Objective { get; set; }
        public uint MinLevel { get; set; }
        public uint MaxLevel { get; set; }
        public string MinMax { get { return string.Format("{0,-8}", MinLevel + "~" + MaxLevel); } }
        public string PartyFullness { get { return string.Format("{0}/{1}", MemberCount, Type.GetMaxMembers()); } }
        public List<PartyMember> Members = new List<PartyMember>(8);

        public PartyEntry(uint number)
        {
            PartyNumber = number;
        }

        public override string ToString()
        {
            return string.Format("{0,-8}  {1}\t{2,-13}  {3}  {4}:{5}", PartyNumber, PartyFullness, Type.ToFriendlyString(), MinMax, Leader, Title);
        }

        public override bool Equals(object obj)
        {
            var entry = obj as PartyEntry;
            return entry != null &&
                   PartyNumber == entry.PartyNumber;
        }

        public bool Equals(PartyEntry other)
        {
            return other != null &&
                   PartyNumber == other.PartyNumber;
        }

        public override int GetHashCode()
        {
            return 128635974 + PartyNumber.GetHashCode();
        }
    }
}
