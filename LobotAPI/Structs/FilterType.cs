﻿using System;

namespace Lobot
{
    /// <summary>
    /// Type used to save filter options
    /// </summary>
    [Flags]
    public enum FilterType : byte
    {
        Pick = 8,
        Store = 4,
        Sell = 2,
        Drop = 1
    }
}
