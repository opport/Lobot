﻿using System.Collections.Generic;

namespace LobotAPI.Structs
{
    /// <summary>
    /// A concise version of the <see cref="LobotAPI.PK2.Pk2ShopTabData"/> class.
    /// Used as a value object to map item slot numbers to itemIDs.
    /// </summary>
    public class ShopTab
    {
        public Dictionary<int, uint> Items = new Dictionary<int, uint>();
    }
}
