using LobotAPI.Globals;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LobotAPI
{

    public class Pet : ISpawnType, ICombatType, INotifyPropertyChanged
    {
        public string Name
        {
            get
            {
                if (PK2DataGlobal.NPCs.TryGetValue(RefID, out PK2.Pk2NPC npc))
                {
                    return npc.Name;
                }
                else
                {
                    return RefID.ToString();
                }
            }
        }
        private string _customName = "";
        public string CustomName { get => _customName; set { _customName = value; NotifyPropertyChanged(); } }
        public string OwnerName { get; set; }
        public uint OwnerID { get; set; }
        public uint RefID { get; set; } // ID for game class
        public uint UniqueID { get; set; } // ID for instance of class
        public Point Position { get; set; }
        private uint _currentHP = 0;
        public uint CurrentHP { get => _currentHP; set { _currentHP = value; NotifyPropertyChanged("HPPercent"); } }
        public Pk2NPC Pk2Entry => PK2DataGlobal.NPCs[RefID];
        public int MaxHP { get; set; }
        public bool IsAlive { get; set; }
        public bool IsKnockedDown { get; set; }
        public string Target;
        public bool IsInvisible;
        public byte ScrollSlot { get; set; } = 0;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Commands pet to return to Player. Useful to stop attacking.
        /// </summary>
        public void ReturnToPlayer()
        {
            throw new NotImplementedException();
        }
    }
}
