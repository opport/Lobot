﻿using System;
using System.Collections.Generic;

namespace LobotAPI.Structs
{
    public class PK2ShopTabRef
    {
        public int TabNumber;
        public string TabLongID;
        public List<PK2ShopItemRef> Items = new List<PK2ShopItemRef>();

        public PK2ShopTabRef(string tabLongID, List<PK2ShopItemRef> items)
        {
            TabLongID = tabLongID ?? throw new ArgumentNullException(nameof(tabLongID));
            Items = items ?? throw new ArgumentNullException(nameof(items));
        }
    }
}
