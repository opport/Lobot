﻿using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;

namespace LobotAPI.Globals
{
    public static class SpawnListsGlobal
    {
        public enum BotLists : int
        {
            None = -1,
            Drops = 0,
            MonsterSpawns = 1,
            NPCs = 2,
            Portals = 3,
            Players = 4,
            Pets = 5
        }

        public static Comparison<Tuple<byte, Item>> ItemComparison = new Comparison<Tuple<byte, Item>>((i1, i2) => { return i1.Item1 - i2.Item1; });

        public static Dictionary<uint, BotLists> AllSpawnEntries;
        public static ObservableConcurrentDictionary<uint, ItemDrop> Drops { get; set; }
        public static Dictionary<uint, uint> SpawnBuffs = new Dictionary<uint, uint>();

        // NPCs
        public static ObservableCollection<CharData> PlayerList { get; set; }
        public static ObservableConcurrentDictionary<uint, Monster> MonsterSpawns { get; set; }
        public static ObservableConcurrentDictionary<uint, Monster> NPCSpawns { get; set; }
        public static ObservableConcurrentDictionary<uint, Portal> Portals { get; set; }

        public static ObservableCollection<LobotAPI.Structs.Skill> Skills { get; set; }

        // Lock objects to enable changing observablecollections in threads different from the UI-thread they were created in
        private static object PartyMatchingLock = new object();

        private static object AvatarInventoryLock = new object();
        private static object InventoryLock = new object();
        private static object StorageLock = new object();
        private static object GuildStorageLock = new object();
        private static object DropsLock = new object();

        private static object PlayerListLock = new object();
        private static object NPCListLock = new object();
        private static object SpawnsLock = new object();
        //private static object ShopsLock = new object();
        private static object TeleportersLock = new object();

        private static object SkillsLock = new object();

        static SpawnListsGlobal()
        {
            AllSpawnEntries = new Dictionary<uint, BotLists>();

            Drops = new ObservableConcurrentDictionary<uint, ItemDrop>();
            BindingOperations.EnableCollectionSynchronization(Drops, DropsLock);

            PlayerList = new ObservableCollection<CharData>();
            BindingOperations.EnableCollectionSynchronization(PlayerList, PlayerListLock);

            MonsterSpawns = new ObservableConcurrentDictionary<uint, Monster>();
            BindingOperations.EnableCollectionSynchronization(MonsterSpawns, SpawnsLock);

            NPCSpawns = new ObservableConcurrentDictionary<uint, Monster>();
            BindingOperations.EnableCollectionSynchronization(NPCSpawns, NPCListLock);

            Portals = new ObservableConcurrentDictionary<uint, Portal>();
            BindingOperations.EnableCollectionSynchronization(Portals, TeleportersLock);

            Skills = new ObservableCollection<LobotAPI.Structs.Skill>();
            BindingOperations.EnableCollectionSynchronization(Skills, SkillsLock);

            //BindingOperations.EnableCollectionSynchronization(Shops, ShopsLock);
        }

        public static void RemoveAllSpawnEntries()
        {
            List<uint> keys = new List<uint>(AllSpawnEntries.Keys);

            foreach (uint key in keys)
            {
                RemoveFromBotList(key, AllSpawnEntries[key]);
            }
        }

        private static bool AccessBotList(uint uniqueID, BotLists list, out ISpawnType spawn)
        {
            switch (list)
            {
                case BotLists.None:
                    spawn = null;
                    break;
                case BotLists.Drops:
                    spawn = Drops.FirstOrDefault(t => t.Key == uniqueID).Value;
                    break;
                case BotLists.MonsterSpawns:
                    spawn = MonsterSpawns.FirstOrDefault(t => t.Key == uniqueID).Value;
                    break;
                case BotLists.NPCs:
                    spawn = NPCSpawns.FirstOrDefault(t => t.Key == uniqueID).Value;
                    break;
                case BotLists.Portals:
                    spawn = Portals.FirstOrDefault(t => t.Key == uniqueID).Value;
                    break;
                case BotLists.Players:
                    spawn = PlayerList.FirstOrDefault(t => t.UniqueID == uniqueID);
                    break;
                case BotLists.Pets:
                    //TODO
                    spawn = null;
                    break;
                default:
                    spawn = null;
                    break;
            }

            return spawn != null;
        }

        public static bool TryGetSpawnObject(uint uniqueID, out ISpawnType spawn)
        {
            if (AllSpawnEntries.TryGetValue(uniqueID, out SpawnListsGlobal.BotLists listType)
                && AccessBotList(uniqueID, listType, out ISpawnType resultSpawn))
            {
                spawn = resultSpawn;
                return spawn != null; // sometimes despawn during AccessBotList() can cause a null pointer to return as true
            }
            else
            {
                spawn = null;
                return false;
            }
        }

        public static void RemoveFromBotList(uint objectID, BotLists listNumber)
        {
            switch (listNumber)
            {
                case BotLists.None:
                    return;
                case BotLists.Drops:
                    Drops.Remove(objectID);
                    AllSpawnEntries.Remove(objectID);
                    break;
                case BotLists.MonsterSpawns:
                    MonsterSpawns.Remove(objectID);
                    AllSpawnEntries.Remove(objectID);
                    break;
                case BotLists.NPCs:
                    NPCSpawns.Remove(objectID);
                    AllSpawnEntries.Remove(objectID);
                    break;
                case BotLists.Portals:
                    Portals.Remove(objectID);
                    AllSpawnEntries.Remove(objectID);
                    break;
                case BotLists.Players:
                    PlayerList.RemoveAll(player => player.UniqueID == objectID);
                    AllSpawnEntries.Remove(objectID);
                    break;
                case BotLists.Pets:
                    if (Bot.PickupPet.UniqueID == objectID)
                    {
                        Bot.PickupPet.UniqueID = 0;
                    }
                    else if (Bot.GrowthPet.UniqueID == objectID // Make sure petscroll is set to "Unsummon" before removing pet
                        && CharInfoGlobal.Inventory.TryGetValue(Bot.GrowthPet.ScrollSlot, out Item value)
                        && value is PetScroll
                        && (value as PetScroll).Status != PetStatus.Summoned)
                    {
                        Bot.GrowthPet.SetFromPet(new Pet());
                    }
                    else if (Bot.Transport.UniqueID == objectID)
                    {
                        Bot.Transport.UniqueID = 0;
                    }
                    AllSpawnEntries.Remove(objectID);
                    break;
                default:
                    break;
            }
        }
    }
}
