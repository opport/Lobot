﻿using LobotAPI.Scripting;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Windows;

namespace LobotAPI.Globals
{
    public static class ScriptGlobal
    {
        public const int MAX_GO_DISTANCE = 75;
        public static readonly string WalkScriptFolder = AppDomain.CurrentDomain.BaseDirectory + "Scripts\\";
        public static readonly string TownScriptFolder = AppDomain.CurrentDomain.BaseDirectory + "Townscripts\\";

        public static readonly Dictionary<string, string> TownScripts = new Dictionary<string, string>
        {
            {"jangan", TownScriptFolder + "jangan.txt"},
            { "donwang", TownScriptFolder + "donwang.txt"},
            { "hotan", TownScriptFolder + "hotan.txt"},
            { "samarkand", TownScriptFolder + "samarkand.txt"},
            { "constantinople", TownScriptFolder + "constantinople.txt"},
            { "alexandria north", TownScriptFolder + "alexandria north.txt"},
            { "alexandria south", TownScriptFolder + "alexandria south.txt"},
        };

        public static readonly List<AbstractExpression> TownScriptJangan = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "6427","1082" }),
            new CommandExpression("go", new string[] { "6420","1050" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "6402","1026" }),
            new CommandExpression("go", new string[] { "6377","1020" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "6377","1048" }),
            new CommandExpression("go", new string[] { "6377","1078" }),
            new CommandExpression("go", new string[] { "6382","1093" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "6434","1090" }),
            new CommandExpression("go", new string[] { "6464","1089" }),
            new CommandExpression("go", new string[] { "6497","1085" }),
            new CommandExpression("potions"),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "6464","1089" }),
            new CommandExpression("go", new string[] { "6434","1090" })
        };

        public static readonly List<AbstractExpression> TownScriptDonwang = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "3548","2070" }),
            new CommandExpression("go", new string[] { "3565","2030" }),
            new CommandExpression("go", new string[] { "3567","2005" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "3535","2006" }),
            new CommandExpression("go", new string[] { "3515","2012" }),
            new CommandExpression("special"),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "3545","2032" }),
            new CommandExpression("go", new string[] { "3558","2046" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "3565","2088" }),
            new CommandExpression("go", new string[] { "3583","2088" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "3550","2087" })
        };

        public static readonly List<AbstractExpression> TownScriptHotan = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "103","30" }),
            new CommandExpression("go", new string[] { "95","60" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "80","95" }),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "67","76" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "75","30" }),
            new CommandExpression("go", new string[] { "81","10" }),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "110","10" }),
            new CommandExpression("go", new string[] { "142","8" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "113","24" })
        };

        public static readonly List<AbstractExpression> TownScriptSamarkand = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "-5183","2847" }),
            new CommandExpression("go", new string[] { "-5141","2816" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "-5138","2875" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "-5191","2938" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "-5227","2878" }),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "-5214","2844" }),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "-5183","2847" })
        };

        public static readonly List<AbstractExpression> TownScriptConstantinople = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "-10642","2602" }),
            new CommandExpression("go", new string[] { "-10631","2584" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "-10672","2534" }),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "-10747","2550" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "-10726","2606" }),
            new CommandExpression("go", new string[] { "-10681","2639" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "-10630","2634" }),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "-10642","2602" })
        };

        public static readonly List<AbstractExpression> TownScriptAlexNorth = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "-16127","61" }),
            new CommandExpression("go", new string[] { "-16090","34" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "-16133","58" }),
            new CommandExpression("go", new string[] { "-16184","51" }),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "-16219","38" }),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "-16249","-1" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "-16253","-59" }),
            new CommandExpression("go", new string[] { "-16261","-85" }),
            new CommandExpression("go", new string[] { "-16294","-90" }),
            new CommandExpression("go", new string[] { "-16336","-98" }),
            new CommandExpression("go", new string[] { "-16368","-133" }),
            new CommandExpression("go", new string[] { "-16396","-171" }),
            new CommandExpression("go", new string[] { "-16428","-213" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "-16396","-171" }),
            new CommandExpression("go", new string[] { "-16368","-133" }),
            new CommandExpression("go", new string[] { "-16336","-98" }),
            new CommandExpression("go", new string[] { "-16294","-90" }),
            new CommandExpression("go", new string[] { "-16261","-85" }),
            new CommandExpression("go", new string[] { "-16233","-78" }),
            new CommandExpression("go", new string[] { "-16174","-67" }),
            new CommandExpression("go", new string[] { "-16126","-46" }),
            new CommandExpression("go", new string[] { "-16099","-14" }),
            new CommandExpression("go", new string[] { "-16114","52" })
        };
        public static readonly List<AbstractExpression> TownScriptAlexSouth = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "-16615","-302" }),
            new CommandExpression("go", new string[] { "-16577","-339" }),
            new CommandExpression("go", new string[] { "-16544","-347" }),
            new CommandExpression("go", new string[] { "-16513","-323" }),
            new CommandExpression("go", new string[] { "-16477","-283" }),
            new CommandExpression("store"),
            new CommandExpression("go", new string[] { "-16457","-253" }),
            new CommandExpression("go", new string[] { "-16431","-219" }),
            new CommandExpression("stable"),
            new CommandExpression("go", new string[] { "-16457","-253" }),
            new CommandExpression("go", new string[] { "-16480","-271" }),
            new CommandExpression("go", new string[] { "-16518","-262" }),
            new CommandExpression("go", new string[] { "-16560","-255" }),
            new CommandExpression("special"),
            new CommandExpression("go", new string[] { "-16630","-248" }),
            new CommandExpression("go", new string[] { "-16696","-265" }),
            new CommandExpression("go", new string[] { "-16717","-279" }),
            new CommandExpression("repair"),
            new CommandExpression("go", new string[] { "-16670","-317" }),
            new CommandExpression("go", new string[] { "-16629","-349" }),
            new CommandExpression("potions"),
            new CommandExpression("go", new string[] { "-16614","-302" })
        };

        /// <summary>
        /// Dictionary containing "Town", (Coordinates, radius).
        /// The player must be within the 
        /// </summary>
        public static IReadOnlyDictionary<string, Town> Towns = new Dictionary<string, Town>
        {
            { "jangan", new Town("Jangan", new Point(6434, 1121),
                new List<Point>{new Point(6500,953),new Point(6500,1152),new Point(6360,1151),new Point(6360,950)},
                TownScriptJangan)},
            {"donwang", new Town("Donwang", new Point(3549,2070),
                new List<Point>{new Point(3631,2161),new Point(3464,2162),new Point(3461,1940),new Point(3632,1941)},
                TownScriptDonwang)},
            {"hotan", new Town("Hotan", new Point(113,47),
                new List<Point>{new Point(159,151),new Point(70,150),new Point(10,90),new Point(10,5),
                new Point(71,-55),new Point(157,-53),new Point(216,5),new Point(215,90) },
                TownScriptHotan)},
            {"samarkand", new Town("Samarkand", new Point(-5183,2892),
                new List<Point>{new Point(-5000,2880),new Point(-5001,2921),new Point(-5168,3054),new Point(-5204,3056),
                new Point(-5366,2907),new Point(-5368,2877),new Point(-5194,2704),new Point(-5174,2705)},
                TownScriptSamarkand) },
            {"constantinople", new Town("Constantinople", new Point(-10683,2584),
                new List<Point>{new Point(-10562,2682),new Point(-10745,2675),new Point(-10787,2513),new Point(-10621,2534)},
                TownScriptConstantinople)},
            {"alexandria north", new Town("AlexNorth", new Point(-16166,-4),
                new List<Point>{new Point(-16011,106),new Point(-15990,66),new Point(-16087,17),new Point(-16118,-72),
                new Point(-16223,-100),new Point(-16303,-103),new Point(-16351,-134),new Point(-16406,-274),
                new Point(-16429,-292),new Point(-16483,-241),new Point(-16401,-151),new Point(-16349,-53),
                new Point(-16317,-42),new Point(-16258,40),new Point(-16180,85),new Point(-16168,99),
                new Point(-16121,94),new Point(-16093,71) },
                TownScriptAlexNorth) },
            {"alexandria south", new Town("AlexSouth", new Point(-16580,-292),
                new List<Point> {new Point(-16597,-167),new Point(-16649,-166),new Point(-16751,-277),new Point(-16645,-386),
                new Point(-16591,-346),new Point(-16473,-484),new Point(-16438,-447),new Point(-16519,-352),
                new Point(-16449,-286),new Point(-16480,-253) },
                TownScriptAlexSouth)}
        };
    }
}
