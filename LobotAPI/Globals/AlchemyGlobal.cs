﻿namespace LobotAPI.Globals
{
    public partial class AlchemyGlobal
    {
        public static int WeaponElixirCount;
        public static int ShieldElixirCount;
        public static int AccessoryElixirCount;
        public static int ProtectorElixirCount;
        public static int LuckyCount;

        public static byte PimpIndex = 0;
        public static bool Fusing = false;
    }
}
