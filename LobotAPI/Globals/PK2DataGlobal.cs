﻿using LobotAPI.Globals.Settings;
using LobotAPI.Scripting.NavMesh.Structs;
using LobotAPI.Structs;
using System.Collections.Generic;

namespace LobotAPI.Globals
{
    public static class PK2DataGlobal
    {
        public static string PK2MediaFilePath { get => LoginSettings.LoginClientDirectoryPath + "\\media.pk2"; }
        public static string PK2DataFilePath { get => LoginSettings.LoginClientDirectoryPath + "\\data.pk2"; }

        public static void SetEmpty()
        {

        }

        public static readonly List<string> PetTypesAttack = new List<string>
        {
            "COS_P_BEAR",
            "COS_P_FOX",
            "COS_P_PENGUIN",
            "COS_P_WOLF_WHITE_SMALL",
            "COS_P_WOLF_WHITE",
            "COS_P_WOLF",
            "COS_P_JINN",
            "COS_P_KANGAROO",
            "COS_P_RAVEN"
        };

        public static readonly List<string> PetTypesPickup = new List<string>
        {
            "COS_P_SPOT_RABBIT",
            "COS_P_RABBIT",
            "COS_P_GGLIDER",
            "COS_P_MYOWON",
            "COS_P_SEOWON",
            "COS_P_RACCOONDOG",
            "COS_P_CAT",
            "COS_P_BROWNIE",
            "COS_P_PINKPIG",
            "COS_P_GOLDPIG",
            "COS_P_WINTER_SNOWMAN"
        };

        public static Dictionary<string, string> Names;
        public static Dictionary<uint, PK2.Pk2Item> Items { get; set; }
        public static Dictionary<int, ulong> LevelData { get; set; }
        public static Dictionary<uint, PK2.Pk2NPC> NPCs { get; set; }
        public static Dictionary<int, Shop> Shops { get; set; } // int instead of uint because itemmall numbers are negative (e.g. -268435455)
        public static Dictionary<uint, PK2.Pk2Skill> Skills { get; set; }
        public static Dictionary<uint, PK2.Pk2Teleporter> Teleporters { get; set; }
        public static Dictionary<uint, NavMeshSegment> NavigationSegments { get; set; }
    }
}
