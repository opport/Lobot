﻿using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System.Collections.Generic;
using System.Linq;

namespace LobotAPI.Globals
{
    public static class ConsumablesGlobal
    {
        public static List<KeyValuePair<byte, Item>> HPPots { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.HPPot).ToList(); } }
        public static List<KeyValuePair<byte, Item>> HGrains { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.HPGrain && t.Value.LongID.Contains("HP_SPOTION")).ToList(); } }
        public static List<KeyValuePair<byte, Item>> MPPots { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.MPPot).ToList(); } }
        public static List<KeyValuePair<byte, Item>> MPGrains { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.MPGrain && t.Value.LongID.Contains("MP_SPOTION")).ToList(); } }
        public static List<KeyValuePair<byte, Item>> Vigors { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.VigourPot).ToList(); } }
        public static List<KeyValuePair<byte, Item>> VigorGrains { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.VigourGrain && t.Value.LongID.Contains("ALL_SPOTION")).ToList(); } }
        public static List<KeyValuePair<byte, Item>> UniversalPills { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.UniversalPill).ToList(); } }
        public static List<KeyValuePair<byte, Item>> SpecialPills { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.PurificationPill).ToList(); } }

        public static List<KeyValuePair<byte, Item>> Ammo
        {
            get
            {
                return (ShoppingSettings.ProjectileType == 62)
                    ? CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.Arrow).ToList()
                    : CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.Bolt).ToList();
            }
        }
        public static List<KeyValuePair<byte, Item>> SpeedPots { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.BuffScroll && t.Value.LongID.Contains("POTION_SPEED")).ToList(); } }
        public static List<KeyValuePair<byte, Item>> ReturnScrolls { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.ReturnScroll).ToList(); } }
        public static List<KeyValuePair<byte, Item>> Vehicles { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.Vehicle).ToList(); } }

        public static List<KeyValuePair<byte, Item>> PetHPPotions { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.PetHPPotion).ToList(); } }
        public static List<KeyValuePair<byte, Item>> PetPills { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.PetPill).ToList(); } }
        public static List<KeyValuePair<byte, Item>> HGPs { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.HGPPotion).ToList(); } }
        public static List<KeyValuePair<byte, Item>> GrassOfLife { get { return CharInfoGlobal.Inventory.Where(t => t.Value.Type == Pk2ItemType.GrassOfLife).ToList(); } }
    }
}
