﻿using LobotAPI.PK2;
using LobotAPI.Structs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace LobotAPI.Globals.Settings
{
    public static class SkillSettings
    {
        public static int SkillListIndex { get; set; } = 0;
        public static int BuffListIndex { get; set; } = 0;

        public static Skill Imbue { get => Settings.Instance.Skills.Imbue ?? null; set { Settings.Instance.Skills.Imbue = value; NotifyPropertyChanged(); } }
        public static Skill SpeedBuff { get => Settings.Instance.Skills.SpeedBuff ?? null; set { Settings.Instance.Skills.SpeedBuff = value; NotifyPropertyChanged(); } }
        public static Skill HealSpell { get => Settings.Instance.Skills.HealSpell ?? null; set { Settings.Instance.Skills.HealSpell = value; NotifyPropertyChanged(); } }
        public static Skill ManaSpell { get => Settings.Instance.Skills.ManaSpell ?? null; set { Settings.Instance.Skills.ManaSpell = value; NotifyPropertyChanged(); } }
        public static Skill ResSpell { get => Settings.Instance.Skills.ResSpell ?? null; set { Settings.Instance.Skills.ResSpell = value; NotifyPropertyChanged(); } }

        // Property backing variables
        private static ObservableCollection<ObservableCollection<Skill>> s_initiationSkills;
        private static ObservableCollection<ObservableCollection<Skill>> s_generalSkills;
        private static ObservableCollection<ObservableCollection<Skill>> s_finisherSkills;
        private static ObservableCollection<ObservableCollection<Skill>> s_buffSkills;

        public static bool CycleSkills { get => Settings.Instance.Skills.CycleSkills; set { Settings.Instance.Skills.CycleSkills = value; NotifyPropertyChanged(); } }

        public static bool SpeedBuffOrPot { get => Settings.Instance.Skills.SpeedBuffOrPot; set { Settings.Instance.Skills.SpeedBuffOrPot = value; NotifyPropertyChanged(); } }
        public static bool BuffAllOrMembers { get => Settings.Instance.Skills.BuffAllOrMembers; set { Settings.Instance.Skills.BuffAllOrMembers = value; NotifyPropertyChanged(); } }

        public static bool UseHeal { get => Settings.Instance.Skills.UseHeal; set { Settings.Instance.Skills.UseHeal = value; NotifyPropertyChanged(); } }
        public static int HealPercentage { get => Settings.Instance.Skills.UseHealPercentage; set { Settings.Instance.Skills.UseHealPercentage = value; NotifyPropertyChanged(); } }

        public static bool UseMPSpell { get => Settings.Instance.Skills.UseMPSpell; set { Settings.Instance.Skills.UseMPSpell = value; NotifyPropertyChanged(); } }
        public static int MPPercentage { get => Settings.Instance.Skills.UseMPSpellPercentage; set { Settings.Instance.Skills.UseMPSpellPercentage = value; NotifyPropertyChanged(); } }

        public static bool UseRes { get => Settings.Instance.Skills.UseRes; set { Settings.Instance.Skills.UseRes = value; NotifyPropertyChanged(); } }

        public static ObservableCollection<MonsterType> MonsterTypes { get; set; }
        public static ObservableCollection<ObservableCollection<Skill>> InitiationSkills { get => s_initiationSkills; set => s_initiationSkills = value; }
        public static ObservableCollection<ObservableCollection<Skill>> GeneralSkills { get => s_generalSkills; set => s_generalSkills = value; }
        public static ObservableCollection<ObservableCollection<Skill>> FinisherSkills { get => s_finisherSkills; set => s_finisherSkills = value; }
        public static ObservableCollection<ObservableCollection<Skill>> BuffSkills { get => s_buffSkills; set => s_buffSkills = value; }
        public static ObservableCollection<string> MembersToBuff { get; set; }
        public static ObservableCollection<ObservableCollection<PartyMember>> MemberBuffs { get; set; }
        private static object MonsterTypesLock = new object();
        private static object InitiationSkillsLock = new object();
        private static object GeneralSkillsLock = new object();
        private static object FinisherSkillsLock = new object();
        private static object BuffSkillsLock = new object();
        private static object MembersToBuffLock = new object();
        private static object Memberbuffs = new object();

        // Selected lists based on combobox in GUI
        public static ObservableCollection<Skill> SelectedInitiationSkills { get; set; }
        public static ObservableCollection<Skill> SelectedGeneralSkills { get; set; }
        public static ObservableCollection<Skill> SelectedFinisherSkills { get; set; }
        public static ObservableCollection<Skill> SelectedBuffs { get; set; }

        static SkillSettings()
        {
            MonsterTypes = new ObservableCollection<MonsterType>();
            BindingOperations.EnableCollectionSynchronization(MonsterTypes, MonsterTypesLock);
            InitiationSkills = new ObservableCollection<ObservableCollection<Skill>>();
            BindingOperations.EnableCollectionSynchronization(InitiationSkills, InitiationSkillsLock);
            GeneralSkills = new ObservableCollection<ObservableCollection<Skill>>();
            BindingOperations.EnableCollectionSynchronization(GeneralSkills, GeneralSkillsLock);
            FinisherSkills = new ObservableCollection<ObservableCollection<Skill>>();
            BindingOperations.EnableCollectionSynchronization(FinisherSkills, FinisherSkillsLock);
            BuffSkills = new ObservableCollection<ObservableCollection<Skill>>();
            BindingOperations.EnableCollectionSynchronization(BuffSkills, BuffSkillsLock);
            MembersToBuff = new ObservableCollection<string>();
            BindingOperations.EnableCollectionSynchronization(MembersToBuff, MembersToBuffLock);
            MemberBuffs = new ObservableCollection<ObservableCollection<PartyMember>>();
            BindingOperations.EnableCollectionSynchronization(MemberBuffs, Memberbuffs);

            foreach (MonsterType monsterType in Enum.GetValues(typeof(MonsterType)))
            {
                MonsterTypes.Add(monsterType);
                InitiationSkills.Add(new ObservableCollection<Skill>());
                GeneralSkills.Add(new ObservableCollection<Skill>());
                FinisherSkills.Add(new ObservableCollection<Skill>());
                BuffSkills.Add(new ObservableCollection<Skill>());
            }

            // Initialize Selected skills to prevent null reference exceptions
            SelectedInitiationSkills = InitiationSkills[0];
            SelectedGeneralSkills = GeneralSkills[0];
            SelectedFinisherSkills = FinisherSkills[0];
            SelectedBuffs = BuffSkills[0];
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        private static bool TryGetHighestSkillOfType(Pk2SkillType[] type, out Skill skill)
        {
            skill = null;

            foreach (Skill s in CharInfoGlobal.Skills)
            {
                if (type.Any(t => t == s.Type))
                {
                    // Get first available skill or higher level skill of same type
                    if (skill == null || skill.RefSkillID < s.RefSkillID)
                    {
                        skill = s;
                    }
                }
            }

            return skill == null;
        }

        private static void HandleParsedCharData(object sender, EventArgs e)
        {
            if (TryGetHighestSkillOfType(new Pk2SkillType[] { Pk2SkillType.Imbue }, out Skill imbue))
            {
                Imbue = imbue;
            }
            if (SpeedBuffOrPot && TryGetHighestSkillOfType(new Pk2SkillType[] { Pk2SkillType.Heal, Pk2SkillType.SelfHeal }, out Skill speedbuff))
            {
                SpeedBuff = speedbuff;
            }
            if (TryGetHighestSkillOfType(new Pk2SkillType[] { Pk2SkillType.Heal, Pk2SkillType.SelfHeal }, out Skill heal))
            {
                HealSpell = heal;
            }
            if (TryGetHighestSkillOfType(new Pk2SkillType[] { Pk2SkillType.ManaBuff }, out Skill mana))
            {
                ManaSpell = mana;
            }
            if (TryGetHighestSkillOfType(new Pk2SkillType[] { Pk2SkillType.Resurrection }, out Skill res))
            {
                ResSpell = res;
            }

            Bot.CharData.CharNameChanged -= HandleParsedCharData;
        }

        private static void UpdateListInSettings<T>(Collection<T> list, NotifyCollectionChangedEventArgs e, string collectionName, int listIndex)
        {
            string jsonString = File.Exists(Settings.GetSettingsPath)
                ? File.ReadAllText(Settings.GetSettingsPath)
                : "{}";
            JObject json = JObject.Parse(jsonString);

            JArray listAsArray = JArray.Parse(JsonConvert.SerializeObject(list, Formatting.Indented));
            JProperty listAsProperty = new JProperty(listIndex.ToString(), listAsArray);
            JProperty listAsIndexedProperty = new JProperty(collectionName, new JObject(listAsProperty));

            // Replace new 
            if (!json.TryGetValue("Skills", out JToken section))
            {
                json["Skills"] = new JObject(listAsIndexedProperty);
            }
            else if (!json.TryGetValue($"Skills.{collectionName}", out JToken listName))
            {
                json["Skills"][collectionName] = new JObject(listAsProperty);
            }
            else if (!json.TryGetValue($"Skills.{collectionName}.{listIndex}", out JToken listIndexedName))
            {
                json["Skills"][collectionName][listIndex] = listAsArray;
            }
            else
            {
                json["Skills"][collectionName][listIndex] = listAsArray;
            }

            string output = Newtonsoft.Json.JsonConvert.SerializeObject(json, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(Settings.GetSettingsPath, output);
        }

        private static void UpdateSkillList(NotifyCollectionChangedEventArgs e)
        {
            // Ignore autoattacks
            if (e.Action == NotifyCollectionChangedAction.Add && (e.NewItems[0] as Skill).IsAutoAttack)
            {
                return;
            }

            string jsonString = File.Exists(Settings.GetSettingsPath)
                ? File.ReadAllText(Settings.GetSettingsPath)
                : "{}";
            JObject json = JObject.Parse(jsonString);

            JArray listAsArray = JArray.Parse(JsonConvert.SerializeObject(CharInfoGlobal.Skills, Formatting.Indented));
            JProperty listAsProperty = new JProperty("SkillList", listAsArray);

            // Replace new 
            if (!json.TryGetValue("Skills", out JToken section))
            {
                json["Skills"] = new JObject(listAsProperty);
            }
            else if (!json.TryGetValue($"Skills.SkillList", out JToken listName))
            {
                json["Skills"]["SkillList"] = listAsArray;
            }
            else
            {
                json["Skills"]["SkillList"] = listAsArray;
            }

            string output = JsonConvert.SerializeObject(json, Formatting.Indented);
            File.WriteAllText(Settings.GetSettingsPath, output);
        }

        /// <summary>
        /// Handle updates to Lists in order to save them to the settings file.
        /// This should be called after the <see cref="Settings.PopulateLists()"/> function to prevent the lists' eventhandlers from being reset.
        /// </summary>
        public static void InitializeHandlers()
        {
            // Serialize changes to skill list
            CharInfoGlobal.Skills.CollectionChanged += UpdateSkillList;
            // Reset imbue, speed, heal, mana and res skills
            Bot.CharData.CharNameChanged += HandleParsedCharData;

            // Initialize Selected skills to prevent null reference exceptions
            SelectedInitiationSkills = InitiationSkills[0];
            SelectedGeneralSkills = GeneralSkills[0];
            SelectedFinisherSkills = FinisherSkills[0];
            SelectedBuffs = BuffSkills[0];

            SelectedInitiationSkills.CollectionChanged += UpdateInitiationList;
            SelectedGeneralSkills.CollectionChanged += UpdateGeneralList;
            SelectedFinisherSkills.CollectionChanged += UpdateFinisherList;
            SelectedBuffs.CollectionChanged += UpdateBuffList;
            MemberBuffs.CollectionChanged += UpdateMemberBuffList;
        }

        private static void UpdateSkillList(object sender, NotifyCollectionChangedEventArgs e) => UpdateSkillList(e);

        private static void UpdateInitiationList(object sender, NotifyCollectionChangedEventArgs e) => UpdateListInSettings(SelectedInitiationSkills, e, "Initiation", SkillListIndex);
        private static void UpdateGeneralList(object sender, NotifyCollectionChangedEventArgs e) => UpdateListInSettings(SelectedGeneralSkills, e, "General", SkillListIndex);
        private static void UpdateFinisherList(object sender, NotifyCollectionChangedEventArgs e) => UpdateListInSettings(SelectedFinisherSkills, e, "Finisher", SkillListIndex);
        private static void UpdateBuffList(object sender, NotifyCollectionChangedEventArgs e) => UpdateListInSettings(SelectedBuffs, e, "Buffs", SkillListIndex);
        private static void UpdateMemberBuffList(object sender, NotifyCollectionChangedEventArgs e) => UpdateListInSettings(MemberBuffs[BuffListIndex], e, "MemberBuffs", BuffListIndex);
    }
}
