﻿using LobotAPI.Packets.Commands.Alchemy;
using LobotAPI.Structs;
using System.Collections.ObjectModel;

namespace LobotAPI.Globals.Settings
{
    public static class AlchemySettings
    {
        public static Equipment SelectedItem { get; set; }
        public static ObservableCollection<string> Log { get; set; }
        public static uint PlusGoal { get; set; } = 1;
        public static bool AutoBuyLuckyPowder { get; set; } = false;
        public static bool AutoSwitchToLuckyAvatarDress { get; set; } = false;
        public static bool AutoGetElixirsFromStorage { get; set; } = false;
        public static bool AutoGetMaterialsFromStorage { get; set; } = false;

        public static void HandleReinforce(object sender, AlchemyArgs e)
        {
            string outPut;

            if (e.Success)
            {
                if (e.OptLevel == PlusGoal)
                {
                    outPut = $"Enhancement Goal {e.OptLevel} reached ";
                }
                else
                {
                    outPut = $"Success! {e.ItemString}";
                }
            }
            else
            {
                outPut = $"Failure: {e.ItemString}";
            }

            Log.Add(outPut);
        }
    }
}
