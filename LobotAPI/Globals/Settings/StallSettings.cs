﻿using LobotAPI.Structs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace LobotAPI.Globals.Settings
{
    public static class StallSettings
    {
        public static string DefaultName = "[Lobot]'s store";
        public static string DefaultMessage = "Welcome to [Lobot]'s store";
        private static string s_name = "Lobot's stall.";
        private static string s_stallMessage = "welcome to Lobot's stall.";
        private static int s_stallTime;
        private static bool isOpen = false;

        public const int MAX_STALL_ITEMS = 10;

        public static string StallName { get { return s_name; } set { s_name = value; NotifyPropertyChanged(); } }
        public static string StallMessage { get { return s_stallMessage; } set { s_stallMessage = value; NotifyPropertyChanged(); } }
        public static int StallTime { get { return s_stallTime; } set { s_stallTime = value; NotifyPropertyChanged(); } }

        public static bool IsOpen { get => isOpen; set { isOpen = value; OnToggledAutoAdd(new BoolEventArgs(isOpen)); NotifyPropertyChanged(); } }
        public static bool AutoAdd { get => Settings.Instance.Stall.AutoAdd; set { Settings.Instance.Stall.AutoAdd = value; OnToggledAutoAdd(new BoolEventArgs(Settings.Instance.Stall.AutoAdd)); NotifyPropertyChanged(); } }
        public static bool ResumeTraining { get => Settings.Instance.Stall.ResumeTraining; set { Settings.Instance.Stall.ResumeTraining = value; OnToggledToggledResumeTraining(new BoolEventArgs(Settings.Instance.Stall.ResumeTraining)); NotifyPropertyChanged(); } }

        public static ObservableCollection<StallItem> StallItems { get; set; }
        public static ObservableCollection<string> StallLog { get; set; }

        private static object StallLock = new object();
        private static object ReserveLock = new object();

        public static event EventHandler<BoolEventArgs> ToggledAutoAdd = delegate { };
        public static event EventHandler<BoolEventArgs> ToggledResumeTraining = delegate { };

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        private static void OnToggledAutoAdd(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledAutoAdd;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnToggledToggledResumeTraining(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledResumeTraining;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        static StallSettings()
        {
            StallLog = new ObservableCollection<string>();
            StallItems = new ObservableCollection<StallItem>();
            BindingOperations.EnableCollectionSynchronization(StallItems, StallLock);
        }
    }
}
