﻿using LobotAPI.Scripting;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Timers;
using static LobotAPI.Scripting.Area;

namespace LobotAPI.Globals.Settings
{
    public static class GeneralTrainingSettings
    {
        public static event EventHandler<BoolEventArgs> ToggledCanLogin = delegate { };

        private static void OnsettingChanged(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledCanLogin;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public static bool PrioritizeWeakest { get => Settings.Instance.Training.AttackPrioritizeWeakest; set { Settings.Instance.Training.AttackPrioritizeWeakest = value; NotifyPropertyChanged(); } }
        public static bool SwitchOnAggro { get => Settings.Instance.Training.AttackSwitchIfPlayerAttacks; set { Settings.Instance.Training.AttackSwitchIfPlayerAttacks = value; NotifyPropertyChanged(); } }
        public static bool DefendTeammates { get => Settings.Instance.Training.AttackDefendTeam; set { Settings.Instance.Training.AttackDefendTeam = value; NotifyPropertyChanged(); } }
        public static bool DefendPet { get => Settings.Instance.Training.AttackDefendTeam; set { Settings.Instance.Training.AttackDefendTeam = value; NotifyPropertyChanged(); } }
        public static bool ReturnToCenter { get => Settings.Instance.Training.AttackDefendTeam; set { Settings.Instance.Training.AttackDefendTeam = value; NotifyPropertyChanged(); } }
        public static bool FocusUnique { get => Settings.Instance.Training.AttackFocusUnique; set { Settings.Instance.Training.AttackFocusUnique = value; NotifyPropertyChanged(); } }
        public static bool AutoAttackEventMonster { get => Settings.Instance.Training.AttackAutoAttackEvent; set { Settings.Instance.Training.AttackAutoAttackEvent = value; NotifyPropertyChanged(); } }
        public static bool WarlockDOTSwitch { get => Settings.Instance.Training.WarlockDOTSwitch; set { Settings.Instance.Training.WarlockDOTSwitch = value; NotifyPropertyChanged(); } }

        public static List<MonsterType> BeserkList { get; set; }
        public static bool BeserkWhenReady { get => Settings.Instance.Training.ZerkUseWhenReady; set { Settings.Instance.Training.ZerkUseWhenReady = value; NotifyPropertyChanged(); } }
        public static bool BeserkWhenXAttackers { get => Settings.Instance.Training.ZerkAttackersMoreThan; set { Settings.Instance.Training.ZerkAttackersMoreThan = value; NotifyPropertyChanged(); } }
        public static int XAttackers { get => Settings.Instance.Training.ZerkAttackersX; set { Settings.Instance.Training.ZerkAttackersX = value; NotifyPropertyChanged(); } }

        public static List<MonsterType> IgnoreList { get; set; }

        public static Dictionary<MonsterType, bool> IgnoreListSpecial { get; set; }
        public static bool DontAttack { get => Settings.Instance.Training.IgnoreRemainIdle; set { Settings.Instance.Training.IgnoreRemainIdle = value; NotifyPropertyChanged(); } }

        public static bool ReturnDead { get => Settings.Instance.Training.ReturnDead; set { Settings.Instance.Training.ReturnDead = value; OnsettingChanged(new BoolEventArgs(Dead())); NotifyPropertyChanged(); } }
        public static bool ReturnDeadXMinutes { get => Settings.Instance.Training.ReturnDeadMinutesMoreThan; set { Settings.Instance.Training.ReturnDeadMinutesMoreThan = value; NotifyPropertyChanged(); } }
        public static int XMinutes { get => Settings.Instance.Training.XMinutes; set { Settings.Instance.Training.XMinutes = value; NotifyPropertyChanged(); } }

        public static bool ReturnInventoryFull { get => Settings.Instance.Training.ReturnFull; set { Settings.Instance.Training.ReturnFull = value; OnsettingChanged(new BoolEventArgs(InventoryFull())); NotifyPropertyChanged(); } }
        public static bool ReturnUnique { get => Settings.Instance.Training.ReturnUniqueFound; set { Settings.Instance.Training.ReturnUniqueFound = value; OnsettingChanged(new BoolEventArgs(Unique())); NotifyPropertyChanged(); } }
        public static bool ReturnGMFound { get => Settings.Instance.Training.ReturnGMFound; set { Settings.Instance.Training.ReturnGMFound = value; OnsettingChanged(new BoolEventArgs(GMFound())); NotifyPropertyChanged(); } }
        public static bool ReturnNoVigours { get => Settings.Instance.Training.ReturnNoVigours; set { Settings.Instance.Training.ReturnNoVigours = value; OnsettingChanged(new BoolEventArgs(NoVigours())); NotifyPropertyChanged(); } }
        public static bool ReturnNoUniversalPills { get => Settings.Instance.Training.ReturnNoUniversalPills; set { Settings.Instance.Training.ReturnNoUniversalPills = value; OnsettingChanged(new BoolEventArgs(NoUniversalPills())); NotifyPropertyChanged(); } }
        public static bool ReturnNoSpecialPills { get => Settings.Instance.Training.ReturnNoSpecialPills; set { Settings.Instance.Training.ReturnNoSpecialPills = value; OnsettingChanged(new BoolEventArgs(NoSpecialPills())); NotifyPropertyChanged(); } }

        public static bool ReturnLowHPPots { get => Settings.Instance.Training.ReturnLowHPPots; set { Settings.Instance.Training.ReturnLowHPPots = value; OnsettingChanged(new BoolEventArgs(LowHPPotsF())); NotifyPropertyChanged(); } }
        public static int LowHPPots { get => Settings.Instance.Training.ReturnHPPotsX; set { Settings.Instance.Training.ReturnHPPotsX = value; OnsettingChanged(new BoolEventArgs(LowHPPotsF())); NotifyPropertyChanged(); } }

        public static bool ReturnLowMPPots { get => Settings.Instance.Training.ReturnLowMPPots; set { Settings.Instance.Training.ReturnLowMPPots = value; OnsettingChanged(new BoolEventArgs(LowMPPotsF())); NotifyPropertyChanged(); } }
        public static int LowMPPots { get => Settings.Instance.Training.ReturnMpPotsX; set { Settings.Instance.Training.ReturnMpPotsX = value; OnsettingChanged(new BoolEventArgs(LowMPPotsF())); NotifyPropertyChanged(); } }

        public static bool ReturnLowAmmo { get => Settings.Instance.Training.ReturnLowAmmo; set { Settings.Instance.Training.ReturnLowAmmo = value; OnsettingChanged(new BoolEventArgs(LowAmmoF())); NotifyPropertyChanged(); } }
        public static int LowAmmo { get => Settings.Instance.Training.ReturnAmmoX; set { Settings.Instance.Training.ReturnAmmoX = value; OnsettingChanged(new BoolEventArgs(LowAmmoF())); NotifyPropertyChanged(); } }

        public static bool ReturnLowWeaponDurability { get => Settings.Instance.Training.ReturnLowWeaponDur; set { Settings.Instance.Training.ReturnLowWeaponDur = value; OnsettingChanged(new BoolEventArgs(LowWeaponDurabilityF())); NotifyPropertyChanged(); } }
        public static int LowWeaponDurability { get => Settings.Instance.Training.ReturnWeaponDurX; set { Settings.Instance.Training.ReturnWeaponDurX = value; OnsettingChanged(new BoolEventArgs(LowWeaponDurabilityF())); NotifyPropertyChanged(); } }

        public static bool ReturnXBrokenItems { get => Settings.Instance.Training.ReturnBrokenItems; set { Settings.Instance.Training.ReturnBrokenItems = value; OnsettingChanged(new BoolEventArgs(XBrokenItemsF())); NotifyPropertyChanged(); } }
        public static int XBrokenItems { get => Settings.Instance.Training.ReturnBrokenItemsX; set { Settings.Instance.Training.ReturnBrokenItemsX = value; OnsettingChanged(new BoolEventArgs(XBrokenItemsF())); NotifyPropertyChanged(); } }

        public static bool ReturnQuestsFinished { get => Settings.Instance.Training.ReturnQuestsDone; set { Settings.Instance.Training.ReturnQuestsDone = value; OnsettingChanged(new BoolEventArgs(false)); NotifyPropertyChanged(); } } //todo
        public static int XQuestsFinished { get => Settings.Instance.Training.ReturnQuestsDoneX; set { Settings.Instance.Training.ReturnQuestsDoneX = value; OnsettingChanged(new BoolEventArgs(false)); NotifyPropertyChanged(); } } //todo

        private static bool Dead() { return Bot.CharData.IsAlive == false && (ReturnDead || ReturnDeadXMinutes && XMinutes < 1); }
        private static bool InventoryFull() { return ReturnInventoryFull && Bot.CharData.CharacterInventoryIsFull; }
        private static bool PetInventoryFull() { return ReturnInventoryFull && Bot.PickupPet.UniqueID != 0 && CharInfoGlobal.PetInventory.Count() == CharInfoGlobal.PetInventorySize; }
        private static bool Unique() { return ReturnUnique && SpawnListsGlobal.MonsterSpawns.Any(monster => monster.Value.Type == MonsterType.Unique); }
        private static bool GMFound() { return ReturnGMFound && SpawnListsGlobal.PlayerList.Any(player => player.GMFlag == 0x1); }
        private static bool NoVigours() { return ReturnNoVigours && CharInfoGlobal.Inventory.All(i => i.Value.Type != PK2.Pk2ItemType.VigourPot || i.Value.Type != PK2.Pk2ItemType.VigourGrain); }
        private static bool NoUniversalPills() { return ReturnNoUniversalPills && CharInfoGlobal.Inventory.All(i => i.Value.Type != PK2.Pk2ItemType.UniversalPill); }
        private static bool NoSpecialPills() { return ReturnNoSpecialPills && CharInfoGlobal.Inventory.All(i => i.Value.Type != PK2.Pk2ItemType.PurificationPill); }
        private static bool LowHPPotsF() { return ReturnLowHPPots && ConsumablesGlobal.HPPots.Sum(i => i.Value.StackCount) <= LowHPPots; }
        private static bool LowMPPotsF() { return ReturnLowMPPots && ConsumablesGlobal.MPPots.Sum(i => i.Value.StackCount) <= LowMPPots; }
        private static bool LowAmmoF() { return ReturnLowAmmo && ConsumablesGlobal.Ammo.Sum(i => i.Value.StackCount) <= LowAmmo; }
        private static bool LowWeaponDurabilityF() { return ReturnLowWeaponDurability && (CharInfoGlobal.Inventory.FirstOrDefault(item => item.Key == (byte)EquipmentSlot.Primary).Value as Equipment).Durability <= LowWeaponDurability; }
        private static bool XBrokenItemsF() { return ReturnXBrokenItems && CharInfoGlobal.Inventory.Where(item => item.Key <= CharInfoGlobal.EQUIPMENT_SLOTS && item.Value is Equipment && (item.Value as Equipment).Durability < 1).Count() >= XBrokenItems; }
        private static bool XQuestsFinishedF() { return /*ReturnQuestsFinished*/ false; } // todo

        private static readonly Dictionary<Func<bool>, string> ReturnCriteria = new Dictionary<Func<bool>, string>
            {
                { Dead, "Character is dead" },
                { InventoryFull, "Inventory is full" },
                { PetInventoryFull, "Pet inventory is full" },
                { Unique, "Unique Spawned" },
                { GMFound, "GM spawned" },
                { NoVigours, "No vigor potions left" },
                { NoUniversalPills, "No universal pills left"},
                { NoSpecialPills, "No special pills left" },
                { LowHPPotsF, "Few HP pots left" },
                { LowMPPotsF, "Few MP pots left"},
                { LowAmmoF, "Low ammo" },
                { LowWeaponDurabilityF, "Low weapon durability"},
                { XBrokenItemsF, $"{XBrokenItems} broken items" },
                { XQuestsFinishedF, $"{XQuestsFinished} quests finished" }
        };


        public static bool ShouldReturnToTown()
        {
            foreach (KeyValuePair<Func<bool>, string> kvp in ReturnCriteria)
            {
                if (kvp.Key.Invoke())
                {
                    System.Diagnostics.Debug.WriteLine($"Returning to town: {kvp.Value}");
                    Logger.Log(LogLevel.BOTTING, $"Returning to town: {kvp.Value}");
                    return true;
                }
            }

            return false;
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }
    }



    public static class TrainingAreaSettings
    {
        public static double Area1X { get => Settings.Instance.Training.TrainingArea1.ObjectOrDefault().X; set { Settings.Instance.Training.TrainingArea1 = Settings.Instance.Training.TrainingArea1.AreaFromValue(AreaProperty.X, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaOne)); } }
        public static double Area1Y { get => Settings.Instance.Training.TrainingArea1.ObjectOrDefault().Y; set { Settings.Instance.Training.TrainingArea1 = Settings.Instance.Training.TrainingArea1.AreaFromValue(AreaProperty.Y, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaOne)); } }
        public static double Area1Radius { get => Settings.Instance.Training.TrainingArea1.ObjectOrDefault().Radius; set { Settings.Instance.Training.TrainingArea1 = Settings.Instance.Training.TrainingArea1.AreaFromValue(AreaProperty.R, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaOne)); } }
        public static double Area2X { get => Settings.Instance.Training.TrainingArea2.ObjectOrDefault().X; set { Settings.Instance.Training.TrainingArea2 = Settings.Instance.Training.TrainingArea2.AreaFromValue(AreaProperty.X, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaTwo)); } }
        public static double Area2Y { get => Settings.Instance.Training.TrainingArea2.ObjectOrDefault().Y; set { Settings.Instance.Training.TrainingArea2 = Settings.Instance.Training.TrainingArea2.AreaFromValue(AreaProperty.Y, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaTwo)); } }
        public static double Area2Radius { get => Settings.Instance.Training.TrainingArea2.ObjectOrDefault().Radius; set { Settings.Instance.Training.TrainingArea2 = Settings.Instance.Training.TrainingArea2.AreaFromValue(AreaProperty.R, value); OnAreaChanged(new AreaChangedArgs(AreaType.AreaTwo)); } }
        public static bool UseSecondArea { get => Settings.Instance.Training.TrainingUseArea2; set { Settings.Instance.Training.TrainingUseArea2 = value; } }
        public static bool StayIdleOption { get => Settings.Instance.Training.TrainingStayIdleOption; set { Settings.Instance.Training.TrainingStayIdleOption = value; } }

        public static event EventHandler<AreaChangedArgs> AreaChanged = delegate { };

        private static void OnAreaChanged(AreaChangedArgs e)
        {
            EventHandler<AreaChangedArgs> handler = AreaChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }

    public static class AutoPotionSettings
    {
        public static class PlayerSettings
        {
            public static Timer VigourTimer { get; set; } = new Timer(1000);
            public static Timer HPPotionTimer { get; set; } = new Timer(1000);
            public static Timer MPPotionTimer { get; set; } = new Timer(1000);
            public static Timer UniversalPillTimer { get; set; } = new Timer(1000);
            public static Timer PurificationTimer { get; set; } = new Timer(10000);
            public static int PotionDelay { get => Settings.Instance.Training.AutoPotionDelay; set { Settings.Instance.Training.AutoPotionDelay = value; NotifyPropertyChanged(); } }

            public static bool HPUsePot { get => Settings.Instance.Training.AutoPotionsLowHP; set { Settings.Instance.Training.AutoPotionsLowHP = value; NotifyPropertyChanged(); } }
            public static int HPPotPercent { get => Settings.Instance.Training.AutoPotionsLowHPX; set { Settings.Instance.Training.AutoPotionsLowHPX = value; NotifyPropertyChanged(); } }
            public static bool HPUseVigour { get => Settings.Instance.Training.AutoPotionsLowVigourHP; set { Settings.Instance.Training.AutoPotionsLowVigourHP = value; NotifyPropertyChanged(); } }
            public static int HPVigourPercent { get => Settings.Instance.Training.AutoPotionsLowVigourHPX; set { Settings.Instance.Training.AutoPotionsLowVigourHPX = value; NotifyPropertyChanged(); } }

            public static bool MPUsePot { get => Settings.Instance.Training.AutoPotionsLowMP; set { Settings.Instance.Training.AutoPotionsLowMP = value; NotifyPropertyChanged(); } }
            public static int MPPotPercent { get => Settings.Instance.Training.AutoPotionsLowMPX; set { Settings.Instance.Training.AutoPotionsLowMPX = value; NotifyPropertyChanged(); } }
            public static bool MPUseVigour { get => Settings.Instance.Training.AutoPotionsLowVigourMP; set { Settings.Instance.Training.AutoPotionsLowVigourMP = value; NotifyPropertyChanged(); } }
            public static int MPVigourPercent { get => Settings.Instance.Training.AutoPotionsLowVigourMPX; set { Settings.Instance.Training.AutoPotionsLowVigourMPX = value; NotifyPropertyChanged(); } }

            public static bool BadStatusPillOrSpell { get => Settings.Instance.Training.AutoPotionBadStatusUsePillOrSpell; set { Settings.Instance.Training.AutoPotionBadStatusUsePillOrSpell = value; NotifyPropertyChanged(); } }
            public static uint BadStatusSpell { get => Settings.Instance.Training.AutoPotionBadStatusSpell; set { Settings.Instance.Training.AutoPotionBadStatusSpell = value; NotifyPropertyChanged(); } }
            public static bool CursePillOrSpell { get => Settings.Instance.Training.AutoPotionCurseUsePillOrSpell; set { Settings.Instance.Training.AutoPotionCurseUsePillOrSpell = value; NotifyPropertyChanged(); } }
            public static uint CurseSpell { get => Settings.Instance.Training.AutoPotionCurseSpell; set { Settings.Instance.Training.AutoPotionCurseSpell = value; NotifyPropertyChanged(); } }

            static PlayerSettings()
            {
                VigourTimer.Elapsed += OnElapsedTime;
                HPPotionTimer.Elapsed += OnElapsedTime;
                MPPotionTimer.Elapsed += OnElapsedTime;
                UniversalPillTimer.Elapsed += OnElapsedTime;
                PurificationTimer.Elapsed += OnElapsedTime;
            }

            public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

            private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
                if (handler != null)
                {
                    handler(null, new PropertyChangedEventArgs(propertyName));
                }
            }

            private static void OnElapsedTime(object source, ElapsedEventArgs e)
            {
                if (source is Timer)
                {
                    ((Timer)source).Stop();
                }
            }
        }

        public static class PetSettings
        {
            public static readonly Timer PotionTimer = new Timer(1000);
            public static readonly Timer PillTimer = new Timer(1000);
            public static readonly Timer HGPTimer = new Timer(1000);

            public static bool RevivePet { get => Settings.Instance.Training.RevivePet; set { Settings.Instance.Training.RevivePet = value; NotifyPropertyChanged("RevivePet"); } }

            public static bool HPUsePot { get => Settings.Instance.Training.AutoPotionsLowPetHP; set { Settings.Instance.Training.AutoPotionsLowPetHP = value; NotifyPropertyChanged(); } }
            public static int HPPotPercent { get => Settings.Instance.Training.AutoPotionsLowPetHPX; set { Settings.Instance.Training.AutoPotionsLowPetHPX = value; NotifyPropertyChanged(); } }
            public static bool HGPUsePot { get => Settings.Instance.Training.AutoPotionsLowPetHGP; set { Settings.Instance.Training.AutoPotionsLowPetHGP = value; NotifyPropertyChanged(); } }
            public static int HGPPotPercent { get => Settings.Instance.Training.AutoPotionsLowPetHGPX; set { Settings.Instance.Training.AutoPotionsLowPetHGPX = value; NotifyPropertyChanged(); } }

            static PetSettings()
            {
                PotionTimer.Elapsed += OnElapsedTime;
                PillTimer.Elapsed += OnElapsedTime;
                HGPTimer.Elapsed += OnElapsedTime;
            }

            public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

            private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
                if (handler != null)
                {
                    handler(null, new PropertyChangedEventArgs(propertyName));
                }
            }

            private static void OnElapsedTime(object source, ElapsedEventArgs e)
            {
                if (source is Timer)
                {
                    ((Timer)source).Stop();
                }
            }
        }

        public static class COSSettings
        {
            public static readonly Timer PotionDelay = new Timer(1000);

            public static bool HPUsePot { get => Settings.Instance.Training.AutoPotionsLowTransportHP; set { Settings.Instance.Training.AutoPotionsLowTransportHP = value; NotifyPropertyChanged(); } }
            public static int HPPotPercent { get => Settings.Instance.Training.AutoPotionsTransportHPLowerThanX; set { Settings.Instance.Training.AutoPotionsTransportHPLowerThanX = value; NotifyPropertyChanged(); } }

            static COSSettings()
            {
                PotionDelay.Elapsed += OnElapsedTime;
            }

            public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

            private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
                if (handler != null)
                {
                    handler(null, new PropertyChangedEventArgs(propertyName));
                }
            }

            private static void OnElapsedTime(object source, ElapsedEventArgs e)
            {
                if (source is Timer)
                {
                    ((Timer)source).Stop();
                }
            }
        }
    }
}
