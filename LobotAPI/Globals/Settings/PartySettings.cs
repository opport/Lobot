﻿using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LobotAPI.Globals.Settings
{
    public class BoolEventArgs : EventArgs
    {
        public bool Flag;

        public BoolEventArgs(bool result)
        {
            this.Flag = result;
        }
    }

    public static class PartySettings
    {
        private static string s_title;

        public static Dictionary<PartyObjective, string> DefaultPartyentries = new Dictionary<PartyObjective, string> {
            { PartyObjective.Hunting,"For open hunting in Silkroad." },
            { PartyObjective.Quest,"When will the quests end on the Silkroad?" },
            { PartyObjective.Thief,"No need for blood or tears for thieves! Robbery is the only way to go!" },
            { PartyObjective.TradeHunter,"Until the day the Silkroad is all covered with money!" },};

        public static string Title { get { return s_title; } set { s_title = value; NotifyPropertyChanged(); } }

        public static PartyType Type { get => Settings.Instance.Party.Type; set { Settings.Instance.Party.Type = value; NotifyPropertyChanged(); } }
        public static PartyObjective Objective { get => Settings.Instance.Party.Objective; set { Settings.Instance.Party.Objective = value; Title = DefaultPartyentries[Objective]; NotifyPropertyChanged(); } }
        public static bool MembersCanInvite { get => Settings.Instance.Party.MembersCanInvite; set { Settings.Instance.Party.MembersCanInvite = value; OnToggledMembersCanInvite(new BoolEventArgs(Settings.Instance.Party.MembersCanInvite)); NotifyPropertyChanged(); } }
        public static bool AutoAcceptInvite { get => Settings.Instance.Party.AutoInviteAccept; set { Settings.Instance.Party.AutoInviteAccept = value; OnToggledAutoAcceptInvite(new BoolEventArgs(Settings.Instance.Party.AutoInviteAccept)); NotifyPropertyChanged(); } }
        public static bool AutoReform { get => Settings.Instance.Party.AutoReform; set { Settings.Instance.Party.AutoReform = value; OnToggledAutoReform(new BoolEventArgs(Settings.Instance.Party.AutoReform)); NotifyPropertyChanged(); } }
        public static bool AutoJoin { get => Settings.Instance.Party.AutoJoin; set { Settings.Instance.Party.AutoJoin = value; OnToggledToggledAutoJoin(new BoolEventArgs(Settings.Instance.Party.AutoJoin)); NotifyPropertyChanged(); } }
        public static byte MinLevel { get => Settings.Instance.Party.MinLevel; set { Settings.Instance.Party.MinLevel = value; NotifyPropertyChanged(); } }
        public static byte MaxLevel { get => Settings.Instance.Party.MaxLevel; set { Settings.Instance.Party.MaxLevel = value; NotifyPropertyChanged(); } }

        public static event EventHandler<BoolEventArgs> ToggledMembersCanInvite = delegate { };
        public static event EventHandler<BoolEventArgs> ToggledAutoAcceptInvite = delegate { };
        public static event EventHandler<BoolEventArgs> ToggledAutoReform = delegate { };
        public static event EventHandler<BoolEventArgs> ToggledAutoJoin = delegate { };

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        private static void OnToggledMembersCanInvite(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledMembersCanInvite;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnToggledAutoAcceptInvite(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledAutoAcceptInvite;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnToggledAutoReform(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledAutoReform;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnToggledToggledAutoJoin(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledAutoJoin;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
