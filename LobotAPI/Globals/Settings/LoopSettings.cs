﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LobotAPI.Globals.Settings
{
    public static class ScriptSettings
    {
        public static string TrainingScriptPath { get => Settings.Instance.Script.TrainingScript; set { Settings.Instance.Script.TrainingScript = value; NotifyPropertyChanged(); } }
        public static string QuestScriptPath { get => Settings.Instance.Script.QuestScript; set { Settings.Instance.Script.QuestScript = value; NotifyPropertyChanged(); } }
        public static bool UseQuestScript { get => Settings.Instance.Script.UseQuestScript; set { Settings.Instance.Script.UseQuestScript = value; NotifyPropertyChanged(); } }
        public static bool SpeedBuffOrPotion { get => Settings.Instance.Script.UseSpeedBufforSpell; set { Settings.Instance.Script.UseSpeedBufforSpell = value; NotifyPropertyChanged(); } }
        public static uint SpeedBuffID { get => Settings.Instance.Script.SpeedBuff; set { Settings.Instance.Script.SpeedBuff = value; NotifyPropertyChanged(); } }
        public static uint SpeedPotionID { get => Settings.Instance.Script.SpeedDrug; set { Settings.Instance.Script.SpeedDrug = value; NotifyPropertyChanged(); } }

        public static bool UseReverseReturn { get => Settings.Instance.Script.UseReturnScroll; set { Settings.Instance.Script.UseReturnScroll = value; NotifyPropertyChanged(); } }
        public static byte ReverseReturnOption { get => Settings.Instance.Script.ReverseReturnOption; set { Settings.Instance.Script.ReverseReturnOption = value; NotifyPropertyChanged(); } }

        public static bool SwitchToShield { get => Settings.Instance.Script.SwitchToShield; set { Settings.Instance.Script.SwitchToShield = value; NotifyPropertyChanged(); } }
        public static bool RideHorse { get => Settings.Instance.Script.RideHorse; set { Settings.Instance.Script.RideHorse = value; NotifyPropertyChanged(); } }
        public static bool UseReturnScroll { get => Settings.Instance.Script.UseReturnScroll; set { Settings.Instance.Script.UseReturnScroll = value; NotifyPropertyChanged(); } }

        public static bool RecordSkills { get => Settings.Instance.Script.RecordSkills; set { Settings.Instance.Script.RecordSkills = value; NotifyPropertyChanged(); } }
        public static bool RecordQuests { get => Settings.Instance.Script.RecordQuests; set { Settings.Instance.Script.RecordQuests = value; NotifyPropertyChanged(); } }

        public static ObservableCollection<string> RecordedScript = new ObservableCollection<string>();

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public static class ShoppingSettings
    {
        public static uint HPPotionIndex { get => Settings.Instance.Town.HPPotsIndex; set { Settings.Instance.Town.HPPotsIndex = value; NotifyPropertyChanged(); } }
        public static uint HPPotionType { get; set; } = 4;
        public static uint MPPotionIndex { get => Settings.Instance.Town.MPPotsIndex; set { Settings.Instance.Town.MPPotsIndex = value; NotifyPropertyChanged(); } }
        public static uint MPPotionType { get; set; } = 11;
        public static uint VigorPotionIndex { get => Settings.Instance.Town.VigorsIndex; set { Settings.Instance.Town.VigorsIndex = value; NotifyPropertyChanged(); } }
        public static uint VigorPotionType { get; set; } = 18;
        public static uint UniversalPillIndex { get => Settings.Instance.Town.UniPillsIndex; set { Settings.Instance.Town.UniPillsIndex = value; NotifyPropertyChanged(); } }
        public static uint UniversalPillType { get; set; } = 55;
        public static uint PurificationPillIndex { get => Settings.Instance.Town.UniPillsIndex; set { Settings.Instance.Town.UniPillsIndex = value; NotifyPropertyChanged(); } }
        public static uint PurificationPillType { get; set; } = 10374;

        public static uint ProjectileIndex
        {
            get => Settings.Instance.Town.AmmoIndex; set { Settings.Instance.Town.AmmoIndex = value; NotifyPropertyChanged(); }
        }
        public static uint ProjectileType { get; set; } = 62;
        public static uint SpeedPotIndex { get => Settings.Instance.Town.SpeedDrugIndex; set { Settings.Instance.Town.SpeedDrugIndex = value; NotifyPropertyChanged(); } }
        public static uint SpeedPotType { get; set; } = 7098;
        public static uint ReturnScrollIndex { get => Settings.Instance.Town.ReturnScrollIndex; set { Settings.Instance.Town.AmmoIndex = value; NotifyPropertyChanged(); } }
        public static uint ReturnScrollType { get; set; } = 61;
        public static uint TransportIndex { get => Settings.Instance.Town.HorseIndex; set { Settings.Instance.Town.HorseIndex = value; NotifyPropertyChanged(); } }
        public static uint TransportType { get; set; } = 2137;
        public static uint BeserkPotionIndex { get => Settings.Instance.Town.ZerkPotsIndex; set { Settings.Instance.Town.ZerkPotsIndex = value; NotifyPropertyChanged(); } }
        public static uint BeserkPotionType { get; set; } = 0;

        public static int HPPotionQuantity { get => Settings.Instance.Town.HPPotsQt; set { Settings.Instance.Town.HPPotsQt = value; NotifyPropertyChanged(); } }
        public static int MPPotionQuantity { get => Settings.Instance.Town.MPPotsQt; set { Settings.Instance.Town.MPPotsQt = value; NotifyPropertyChanged(); } }
        public static int VigorPotionQuantity { get => Settings.Instance.Town.VigorsQt; set { Settings.Instance.Town.VigorsQt = value; NotifyPropertyChanged(); } }
        public static int UniversalPillQuantity { get => Settings.Instance.Town.UniPillsQt; set { Settings.Instance.Town.UniPillsQt = value; NotifyPropertyChanged(); } }
        public static int PurificationPillQuantity { get => Settings.Instance.Town.PurificationQt; set { Settings.Instance.Town.PurificationQt = value; NotifyPropertyChanged(); } }

        public static int ProjectileQuantity { get => Settings.Instance.Town.AmmoQt; set { Settings.Instance.Town.AmmoQt = value; NotifyPropertyChanged(); } }
        public static int SpeedPotQuantity { get => Settings.Instance.Town.SpeedDrugQt; set { Settings.Instance.Town.SpeedDrugQt = value; NotifyPropertyChanged(); } }
        public static int ReturnScrollQuantity { get => Settings.Instance.Town.ReturnScrollQt; set { Settings.Instance.Town.ReturnScrollQt = value; NotifyPropertyChanged(); } }
        public static int TransportQuantity { get => Settings.Instance.Town.HorseQt; set { Settings.Instance.Town.HorseQt = value; NotifyPropertyChanged(); } }
        public static int BeserkPotionQuantity { get => Settings.Instance.Town.ZerkPotsQt; set { Settings.Instance.Town.ZerkPotsQt = value; NotifyPropertyChanged(); } }


        public static uint PetHPPotionIndex { get => Settings.Instance.Town.PetHPPotsIndex; set { Settings.Instance.Town.PetHPPotsIndex = value; NotifyPropertyChanged(); } }
        public static uint PetHPPotionType { get; set; } = 2143;
        public static uint PetPillIndex { get => Settings.Instance.Town.PetPillsIndex; set { Settings.Instance.Town.PetPillsIndex = value; NotifyPropertyChanged(); } }
        public static uint PetPillType { get; set; } = 9226;
        public const uint PetHGPID = 7553;
        public const uint PetGrassOfLifeID = 7552;

        public static int PetHPPotionQuantity { get => Settings.Instance.Town.PetHPPotsQt; set { Settings.Instance.Town.PetHPPotsQt = value; NotifyPropertyChanged(); } }
        public static int PetPillQuantity { get => Settings.Instance.Town.PetPillsQt; set { Settings.Instance.Town.PetPillsQt = value; NotifyPropertyChanged(); } }
        public static int HGPPotionQuantity { get => Settings.Instance.Town.PetHGPQt; set { Settings.Instance.Town.PetHGPQt = value; NotifyPropertyChanged(); } }
        public static int GrassOfLifeQuantity { get => Settings.Instance.Town.PetGrassOfLifeQt; set { Settings.Instance.Town.PetGrassOfLifeQt = value; NotifyPropertyChanged(); } }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
