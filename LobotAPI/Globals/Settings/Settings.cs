﻿using Config.Net;

using LobotAPI.Scripting;
using LobotAPI.Structs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace LobotAPI.Globals.Settings
{
    public class Settings : INotifyPropertyChanged
    {
        /// <summary>
        /// Use this to rename default "settings.json to [characterName].json after loading"
        /// </summary>
        private const string DEFAULT_SETTINGS_NAME = "settings.json";
        private static readonly string DEFAULT_SETTINGS_PATH = DirectoryGlobal.SettingsFolder + "settings.json";
        private static string currentFilePath = DEFAULT_SETTINGS_PATH;

        public static string GetSettingsPath => DEFAULT_SETTINGS_PATH; // string.IsNullOrEmpty(Bot.CharData.Name)
        //        ? DEFAULT_SETTINGS_PATH
        //        : DirectoryGlobal.SettingsFolder + Bot.CharData.Name + ".json";

        private static ISettings instance;

        /// <summary>
        /// Singleton constructor.
        /// </summary>
        public static ISettings Instance
        {
            get
            {
                if (instance == null)
                {
                    Directory.CreateDirectory(DirectoryGlobal.SettingsFolder);
                    instance = GetSettings();
                    PopulateLists();
                    InitializeAreas();
                    //Bot.CharData.CharNameChanged += HandleNameChanged;
                }
                return instance;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Compare two json objects and create union. 
        /// </summary>
        /// <param name="json">Element to get new elements from</param>
        /// <param name="jsonNew">Element to be completed</param>
        /// <returns>New object with all elements of old element</returns>
        private static JObject GetUnion(JObject json, JObject jsonNew)
        {
            List<JProperty> oldProperties = json.Properties().ToList();

            Queue<JToken> remaining = new Queue<JToken>(json.Properties());
            JToken current = remaining.Dequeue();

            while (current != null)
            {
                JToken foundCurrent = jsonNew.SelectToken(current.Path);

                // Add missing elements from current
                if (foundCurrent == null)
                {
                    JToken targetNode = jsonNew.SelectToken(current.Parent.Path);
                    (targetNode as JObject).Add(current);
                }

                // Search for missing elements in subelements or recurse to next depth
                if (current.HasValues)
                {
                    List<JToken> values = current.Values().ToList();

                    foreach (JToken token in values)
                    {
                        if (token is JProperty && token.HasValues)
                        {
                            JToken foundToken = jsonNew.SelectToken((token as JProperty).Path);

                            // Property not found in new json
                            if (token is JProperty && foundToken == null)
                            {
                                JToken targetNode = jsonNew.SelectToken(token.Parent.Path);
                                (targetNode as JObject).Add(token);
                                continue;
                            }
                        }

                        remaining.Enqueue(token);
                    }
                }

                current = remaining.Count > 0 ? remaining.Dequeue() : null;
            }

            return jsonNew;
        }

        /// <summary>
        /// Move current settings to new character settings.
        /// Optional enhancement for future release.
        /// </summary>
        /// <param name="newCharPath"></param>
        private static void TransferSettings(string newCharPath)
        {
            if (string.IsNullOrEmpty(newCharPath) || string.IsNullOrWhiteSpace(newCharPath))
            {
                return;
            }

            // Rename default settings file to character last character picked
            if (File.Exists(DEFAULT_SETTINGS_PATH) && !File.Exists(newCharPath))
            {
                File.Move(DEFAULT_SETTINGS_PATH, currentFilePath);
                return;
            }

            string jsonStringDefault = File.Exists(DEFAULT_SETTINGS_PATH) ? File.ReadAllText(DEFAULT_SETTINGS_PATH) : "{}";
            string jsonStringNew = File.Exists(newCharPath) ? File.ReadAllText(newCharPath) : "{}";


            // Create json objects while preventing any tomfoolery by the user

            JObject jsonOld;
            try
            {
                jsonOld = JObject.Parse(jsonStringDefault);
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Malformed json string in file {newCharPath}.\nUsing empty json.", DateTime.Now));
                Logger.Log(LogLevel.WARNING, string.Format($"Malformed json string in file {newCharPath}.\nUsing empty json.", DateTime.Now));
                jsonOld = JObject.Parse("{}");
            }

            JObject jsonNew;
            try
            {
                jsonNew = JObject.Parse(jsonStringNew);
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Malformed json string in file {DEFAULT_SETTINGS_PATH}.\nUsing default empty json.", DateTime.Now));
                Logger.Log(LogLevel.WARNING, string.Format($"Malformed json string in file {DEFAULT_SETTINGS_PATH}.\nUsing default empty json.", DateTime.Now));
                jsonNew = JObject.Parse("{}");
            }

            // Transfer missing settings from old one to new one by making a union
            GetUnion(jsonOld, jsonNew).Properties().ToList();

            File.WriteAllText(newCharPath, jsonNew.ToString());
        }

        private static void HandleNameChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Bot.CharData.Name) || string.IsNullOrWhiteSpace(Bot.CharData.Name)) //|| Bot.CharData.Name == instance.LastCharacter)
            {
                return;
            }

            //instance.LastCharacter = Bot.CharData.Name;
            GetSettings(Bot.CharData.Name);

            if (currentFilePath == DEFAULT_SETTINGS_PATH)
            {
                TransferSettings(DirectoryGlobal.SettingsFolder + Bot.CharData.Name + ".json");
            }

            Bot.CharData.CharNameChanged -= HandleNameChanged;
        }

        private static ISettings GetSettings(string charName = null)
        {
            ISettings settings;

            if (string.IsNullOrEmpty(charName) || string.IsNullOrWhiteSpace(charName))
            {
                settings = new ConfigurationBuilder<ISettings>()
                    .UseTypeParser(new AreaParser())
                    .UseTypeParser(new ItemFilterParser())
                    .UseTypeParser(new DegreeFilterParser())
                    .UseTypeParser(new SkillParser())
                    .UseJsonFile(GetSettingsPath)
                    .Build();
            }
            else
            {
                if (currentFilePath == DEFAULT_SETTINGS_PATH)
                {
                    TransferSettings(DirectoryGlobal.SettingsFolder + charName + ".json");
                }

                currentFilePath = DirectoryGlobal.SettingsFolder + charName + ".json";

                settings = new ConfigurationBuilder<ISettings>()
                    .UseTypeParser(new AreaParser())
                    .UseTypeParser(new ItemFilterParser())
                    .UseTypeParser(new DegreeFilterParser())
                    .UseTypeParser(new SkillParser())
                    .UseJsonFile(currentFilePath)
                    .Build();
            }

            // Try to load last character picked
            //if (currentFilePath == DEFAULT_SETTINGS_PATH && !string.IsNullOrEmpty(settings.LastCharacter))
            //{
            //    string lastCharSettingsPath = DirectoryGlobal.SettingsFolder + settings.LastCharacter + ".json";

            //    if (currentFilePath == DEFAULT_SETTINGS_PATH)
            //    {
            //        TransferSettings(lastCharSettingsPath);
            //    }

            //    settings = new ConfigurationBuilder<ISettings>()
            //    .UseTypeParser(new AreaParser())
            //    .UseTypeParser(new ItemFilterParser())
            //    .UseTypeParser(new DegreeFilterParser())
            //    .UseTypeParser(new SkillParser())
            //    .UseJsonFile(lastCharSettingsPath)
            //    .Build();
            //}

            return settings;
        }

        /// <summary>
        /// The public function to load settings based on the current character
        /// </summary>
        /// <param name="charName"></param>
        public static void LoadCharacterSetting(string charName = null)
        {
            ISettings settings = GetSettings(charName);
            instance = settings;
        }

        private static void PopulateSkills(JObject json, string section, string entry)
        {
            if (json == null || string.IsNullOrEmpty(section) || string.IsNullOrEmpty(entry))
            {
                return;
            }

            // entry might not exist yet, so catch Nullreference
            if (json.TryGetValue(section, out JToken sectionToken))
            {
                JToken entryToken = json.SelectToken(section + "." + entry);

                if (entryToken != null)
                {
                    foreach (JValue item in entryToken)
                    {
                        Skill s = JsonConvert.DeserializeObject<Skill>(item.Value.ToString());

                        if (!CharInfoGlobal.Skills.Contains(s))
                        {
                            CharInfoGlobal.Skills.Add(s);
                        }
                    }
                }
            }
        }

        private static void PopulateMonsterSkillList(ObservableCollection<ObservableCollection<Skill>> c, JObject json, string section, string entry)
        {
            if (json == null || string.IsNullOrEmpty(section) || string.IsNullOrEmpty(entry))
            {
                return;
            }

            if (CharInfoGlobal.Skills == null || CharInfoGlobal.Skills.Count < 1)
            {
                System.Diagnostics.Debug.WriteLine($"Empty skill list: Cannot populate {entry} skills.", DateTime.Now);
                Logger.Log(LogLevel.WARNING, $"Empty skill list: Cannot populate {entry} skills. Add them manually in game");
            }

            // entry might not exist yet, so catch Nullreference
            if (json.TryGetValue(section, out JToken sectionToken))
            {
                JToken entryToken = json.SelectToken(section + "." + entry);

                if (entryToken != null)
                {
                    foreach (JProperty item in entryToken)
                    {
                        ObservableCollection<Skill> collection = JsonConvert.DeserializeObject<ObservableCollection<Skill>>(item.Value.ToString());

                        // Add skills from skill list to have same references
                        foreach (Skill skill in collection)
                        {
                            int skillIndex = CharInfoGlobal.Skills.IndexOf(skill);

                            if (skillIndex > -1)
                            {
                                c[int.Parse(item.Name)].Add(CharInfoGlobal.Skills[skillIndex]);
                            }
                        }
                    }
                }
            }
        }

        private static void PopulatePartyBuffs(ObservableCollection<ObservableCollection<PartyMember>> c, JObject json, string section, string entry)
        {
            if (json == null || string.IsNullOrEmpty(section) || string.IsNullOrEmpty(entry))
            {
                return;
            }

            // entry might not exist yet, so catch Nullreference
            if (json.TryGetValue(section, out JToken sectionToken))
            {
                JToken entryToken = json.SelectToken(section + "." + entry);

                if (entryToken != null)
                {
                    foreach (JProperty item in entryToken)
                    {
                        c[int.Parse(item.Name)] = JsonConvert.DeserializeObject<ObservableCollection<PartyMember>>(item.Value.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Workaround to the lack of write functionality for collections
        /// </summary>
        /// <param name="filePath"></param>
        private static void PopulateLists()
        {
            //string path = string.IsNullOrEmpty(instance?.LastCharacter)
            //    ? GetSettingsPath
            //    : DirectoryGlobal.SettingsFolder + instance.LastCharacter + ".json";

            string path = GetSettingsPath;

            // don't populate settings.json with skills
            if (!File.Exists(path)) //|| path == DEFAULT_SETTINGS_PATH)
            {
                return;
            }

            string jsonString = File.ReadAllText(path);
            JObject json = JObject.Parse(jsonString);

            // Make sure pk2skill info is there to add autoattack info (so autoattacks don't show up in skill list later)
            if (PK2DataGlobal.Skills == null
                && PK2Utils.ExistMediaPk2AndClient(LoginSettings.LoginClientDirectoryPath))
            {
                PK2Utils.ExtractAllInformation();
            }

            if (PK2DataGlobal.Skills == null)
            {
                System.Diagnostics.Debug.WriteLine($"Unable to extract PK2 info: No media.pk2 or sro_client found.");
                Logger.Log(LogLevel.WARNING, $"Unable to extract PK2 info: No media.pk2 or sro_client found.");
                return;
            }

            // Populate skill list first
            PopulateSkills(json, "Skills", "SkillList");

            PopulateMonsterSkillList(SkillSettings.InitiationSkills, json, "Skills", "Initiation");
            PopulateMonsterSkillList(SkillSettings.GeneralSkills, json, "Skills", "General");
            PopulateMonsterSkillList(SkillSettings.FinisherSkills, json, "Skills", "Finisher");
            PopulateMonsterSkillList(SkillSettings.BuffSkills, json, "Skills", "Buffs");
            PopulatePartyBuffs(SkillSettings.MemberBuffs, json, "Skills", "MemberBuffs");

            // Update changes after initialization
            SkillSettings.InitializeHandlers();
        }

        /// <summary>
        /// Make sure areas are set after loading to prevent nullpointer exceptions.
        /// </summary>
        private static void InitializeAreas()
        {
            if (instance.Training.TrainingArea1 == null)
            {
                instance.Training.TrainingArea1 = new Area();
            }
            if (instance.Training.TrainingArea2 == null)
            {
                instance.Training.TrainingArea2 = new Area();
            }
        }
    }
}
