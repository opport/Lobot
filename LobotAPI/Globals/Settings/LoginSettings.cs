﻿using LobotAPI.Connection;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Data;

namespace LobotAPI.Globals.Settings
{
    public static class LoginSettings
    {
        private static bool s_isConnected = false;

        public static bool CanLogin { get => s_isConnected; set { s_isConnected = value; OnToggledCanLogin(new BoolEventArgs(s_isConnected)); NotifyPropertyChanged(); } }
        public static bool CanSelectChar { get; set; }
        public static uint SessionID { get; set; } = 0x00;
        public static byte Locale { get; set; } = 0x16; //locale /22
        public static uint Version { get; set; } = 0xE4; //version 228
        public static ushort ShardID { get; set; } = 0x40;
        public static string Username { get => Settings.Instance.Login.UserName; set { Settings.Instance.Login.UserName = value; NotifyPropertyChanged(); } }
        public static string Password { get => Settings.Instance.Login.Password; set { Settings.Instance.Login.Password = value; NotifyPropertyChanged(); } }
        public static string CharName { get => Settings.Instance.Login.Character; set { Settings.Instance.Login.Character = value; NotifyPropertyChanged(); } }
        public static string LoginClientDirectoryPath { get => Settings.Instance.Login.ClientPath; set { Settings.Instance.Login.ClientPath = value; NotifyPropertyChanged(); } }
        // LoginClientPath

        public static List<Shard> ShardList = new List<Shard>();
        public static ObservableCollection<string> CharactersAvailable { get; set; }
        private static object CharactersAvailableLock = new object();
        public static bool AutoConnect { get => Settings.Instance.Login.Autoreconnect; set { Settings.Instance.Login.Autoreconnect = value; NotifyPropertyChanged(); } }
        public static bool AutoSelectCharacter { get => Settings.Instance.Login.AutoSelect; set { Settings.Instance.Login.AutoSelect = value; NotifyPropertyChanged(); } }
        public static bool DelayAutoSelect { get => Settings.Instance.Login.DelayAutoSelect; set { Settings.Instance.Login.DelayAutoSelect = value; NotifyPropertyChanged(); } }
        public static int DelayAutoSelectSeconds { get => Settings.Instance.Login.DelayAutoSelectSeconds; set { Settings.Instance.Login.DelayAutoSelectSeconds = value; NotifyPropertyChanged(); } }

        public static bool StartBotOnLogin { get => Settings.Instance.Login.StartBotOnLogin; set { Settings.Instance.Login.StartBotOnLogin = value; NotifyPropertyChanged(); } }
        public static bool ReturnOnLogin { get => Settings.Instance.Login.ReturnOnLogin; set { Settings.Instance.Login.ReturnOnLogin = value; NotifyPropertyChanged(); } }

        public static bool ClientOrClientless { get => Settings.Instance.Login.TypeClientOrClientless; set { Settings.Instance.Login.TypeClientOrClientless = value; NotifyPropertyChanged(); } }
        public static bool AutostartOrManual { get => Settings.Instance.Login.AutoStart; set { Settings.Instance.Login.AutoStart = value; NotifyPropertyChanged(); } }

        public static bool AutoSaveSettings { get => Settings.Instance.Login.AutosaveSettings; set { Settings.Instance.Login.AutosaveSettings = value; NotifyPropertyChanged(); } }

        static LoginSettings()
        {
            CharactersAvailable = new ObservableCollection<string>();
            BindingOperations.EnableCollectionSynchronization(CharactersAvailable, CharactersAvailableLock);
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static event EventHandler<BoolEventArgs> ToggledCanLogin = delegate { };

        private static void OnToggledCanLogin(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = ToggledCanLogin;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
