﻿using Config.Net;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LobotAPI.Globals.Settings
{
    public interface ILoginSettings
    {
        [Option(DefaultValue = "")]
        string UserName { get; set; }
        [Option(DefaultValue = "")]
        string Password { get; set; }
        [Option(DefaultValue = "")]
        string Character { get; set; }
        [Option(DefaultValue = true)]
        bool AutosaveSettings { get; set; }
        [Option(DefaultValue = "")]
        string ClientPath { get; set; }
        [Option(DefaultValue = false)]
        bool DisplayCaptcha { get; set; }
        [Option(DefaultValue = true)]
        bool AutoStart { get; set; }
        [Option(DefaultValue = false)]
        bool DelayAutoSelect { get; set; }
        [Option(DefaultValue = 0)]
        int DelayAutoSelectSeconds { get; set; }
        [Option(DefaultValue = false)]
        bool TypeClientOrClientless { get; set; }
        [Option(DefaultValue = false)]
        bool Autoreconnect { get; set; }
        [Option(DefaultValue = false)]
        bool StartBotOnLogin { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnOnLogin { get; set; }
        [Option(DefaultValue = true)]
        bool AutoSelect { get; set; }
    }

    public interface ITownSettings
    {
        [Option(DefaultValue = (uint)0)]
        uint HPPotsIndex { get; set; }
        [Option(DefaultValue = 300)]
        int HPPotsQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint MPPotsIndex { get; set; }
        [Option(DefaultValue = 200)]
        int MPPotsQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint UniPillsIndex { get; set; }
        [Option(DefaultValue = 50)]
        int UniPillsQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint PurificationIndex { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint AmmoIndex { get; set; }
        [Option(DefaultValue = 1500)]
        int AmmoQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint SpeedDrugIndex { get; set; }
        [Option(DefaultValue = 5)]
        int SpeedDrugQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint ReturnScrollIndex { get; set; }
        [Option(DefaultValue = 5)]
        int ReturnScrollQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint HorseIndex { get; set; }
        [Option(DefaultValue = 1)]
        int HorseQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint PetHPPotsIndex { get; set; }
        [Option(DefaultValue = 250)]
        int PetHPPotsQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint PetPillsIndex { get; set; }
        [Option(DefaultValue = 10)]
        int PetPillsQt { get; set; }
        [Option(DefaultValue = 10)]
        int PetHGPQt { get; set; }
        [Option(DefaultValue = 2)]
        int PetGrassOfLifeQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint VigorsIndex { get; set; }
        [Option(DefaultValue = 0)]
        int VigorsQt { get; set; }
        [Option(DefaultValue = 0)]
        int PurificationQt { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint ZerkPotsIndex { get; set; }
        [Option(DefaultValue = 0)]
        int ZerkPotsQt { get; set; }
    }

    public interface IScriptSettings
    {
        [Option(DefaultValue = "")]
        string TrainingScript { get; set; }
        [Option(DefaultValue = "")]
        string QuestScript { get; set; }
        [Option(DefaultValue = false)]
        bool UseQuestScript { get; set; }
        [Option(DefaultValue = false)]
        bool UseSpeedBufforSpell { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint SpeedBuff { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint SpeedDrug { get; set; }
        [Option(DefaultValue = 0)]
        int UseReverseReturn { get; set; }
        [Option(DefaultValue = (byte)0)]
        byte ReverseReturnOption { get; set; }
        [Option(DefaultValue = false)]
        bool SwitchToShield { get; set; }
        [Option(DefaultValue = false)]
        bool RideHorse { get; set; }
        [Option(DefaultValue = false)]
        bool UseReturnScroll { get; set; }
        [Option(DefaultValue = false)]
        bool RecordSkills { get; set; }
        [Option(DefaultValue = false)]
        bool RecordQuests { get; set; }
    }

    public interface IFilterSettings
    {
        [Option(DefaultValue = true)]
        bool PickOwnItems { get; set; }
        [Option(DefaultValue = false)]
        bool PickFreeItems { get; set; }

        ItemFilter WeaponElixir { get; set; }
        ItemFilter ShieldElixir { get; set; }
        ItemFilter ProtectorElixir { get; set; }
        ItemFilter AccessoryElixir { get; set; }
        ItemFilter AlchemyMaterial { get; set; }
        ItemFilter Grains { get; set; }
        ItemFilter Arrows { get; set; }
        ItemFilter Bolts { get; set; }
        ItemFilter Gold { get; set; }
        ItemFilter Head { get; set; }
        ItemFilter Chest { get; set; }
        ItemFilter Hose { get; set; }
        ItemFilter Shoulder { get; set; }
        ItemFilter Glove { get; set; }
        ItemFilter Boots { get; set; }
        ItemFilter Necklace { get; set; }
        ItemFilter Earrings { get; set; }
        ItemFilter Ring { get; set; }
        ItemFilter EUHead { get; set; }
        ItemFilter EUChest { get; set; }
        ItemFilter EUHose { get; set; }
        ItemFilter EUShoulder { get; set; }
        ItemFilter EUGloves { get; set; }
        ItemFilter EUBoots { get; set; }
        ItemFilter EUNecklace { get; set; }
        ItemFilter EUEarrings { get; set; }
        ItemFilter EURing { get; set; }
        ItemFilter EUCrossbow { get; set; }
        ItemFilter EUClericStaff { get; set; }
        ItemFilter JadeStrength { get; set; }
        ItemFilter JadeIntelligence { get; set; }
        ItemFilter JadeMaster { get; set; }
        ItemFilter JadeStrikes { get; set; }
        ItemFilter JadeDiscipline { get; set; }
        ItemFilter JadePenetration { get; set; }
        ItemFilter JadeDodging { get; set; }
        ItemFilter JadeStamina { get; set; }
        ItemFilter JadeMagic { get; set; }
        ItemFilter JadeFogs { get; set; }
        ItemFilter JadeAir { get; set; }
        ItemFilter JadeFire { get; set; }
        ItemFilter JadeImmunity { get; set; }
        ItemFilter JadeRevival { get; set; }
        ItemFilter JadeSteady { get; set; }
        ItemFilter JadeLuck { get; set; }
        ItemFilter RubyCourage { get; set; }
        ItemFilter RubyWarriors { get; set; }
        ItemFilter RubyPhilosophy { get; set; }
        ItemFilter RubyMeditation { get; set; }
        ItemFilter RubyChallenge { get; set; }
        ItemFilter RubyFocus { get; set; }
        ItemFilter RubyFlesh { get; set; }
        ItemFilter RubyLife { get; set; }
        ItemFilter RubyMind { get; set; }
        ItemFilter RubySpirit { get; set; }
        ItemFilter RubyDodging { get; set; }
        ItemFilter RubyAgility { get; set; }
        ItemFilter RubyTraining { get; set; }
        ItemFilter RubyPrayer { get; set; }
        ItemFilter HPPots { get; set; }
        ItemFilter MPPots { get; set; }
        ItemFilter VigourPots { get; set; }
        ItemFilter UniversalPills { get; set; }
        ItemFilter PurificationPills { get; set; }
        ItemFilter ReturnScrolls { get; set; }
        ItemFilter COSItems { get; set; }
        ItemFilter EventItems { get; set; }
        ItemFilter Shield { get; set; }
        ItemFilter Sword { get; set; }
        ItemFilter Blade { get; set; }
        ItemFilter Spear { get; set; }
        ItemFilter Glaive { get; set; }
        ItemFilter Bow { get; set; }
        ItemFilter EUShield { get; set; }
        ItemFilter EUSword1H { get; set; }
        ItemFilter EUSword2H { get; set; }
        ItemFilter EUAxe { get; set; }
        ItemFilter EUDagger { get; set; }
        ItemFilter EUWarlockStaff { get; set; }
        ItemFilter EUWizardStaff { get; set; }
        ItemFilter EUHarp { get; set; }
        DegreeFilter DegreesCH { get; set; }
        DegreeFilter DegreesEU { get; set; }
        DegreeFilter DegreesAlchemy { get; set; }
    }

    public interface ITrainingSettings
    {
        [Option(DefaultValue = true)]
        bool AttackPrioritizeWeakest { get; set; }
        [Option(DefaultValue = false)]
        bool AttackSwitchIfPlayerAttacks { get; set; }
        [Option(DefaultValue = false)]
        bool AttackDefendTeam { get; set; }
        [Option(DefaultValue = false)]
        bool AttackFocusUnique { get; set; }
        [Option(DefaultValue = false)]
        bool AttackAutoAttackEvent { get; set; }
        [Option(DefaultValue = true)]
        bool WarlockDOTSwitch { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkUnique { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkGiant { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkElite { get; set; }
        [Option(DefaultValue = false)]
        bool ZerkChampion { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkPartyGiant { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkPartyChampion { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkPartyGeneral { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkStrong { get; set; }
        [Option(DefaultValue = false)]
        bool ZerkQuest { get; set; }
        [Option(DefaultValue = false)]
        bool ZerkEvent { get; set; }
        [Option(DefaultValue = false)]
        bool ZerkUseWhenReady { get; set; }
        [Option(DefaultValue = true)]
        bool ZerkAttackersMoreThan { get; set; }
        [Option(DefaultValue = 5)]
        int ZerkAttackersX { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreUnique { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreElite { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreGiant { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreChampion { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreGeneral { get; set; }
        [Option(DefaultValue = false)]
        bool IgnorePartyGiant { get; set; }
        [Option(DefaultValue = false)]
        bool IgnorePartyChampion { get; set; }
        [Option(DefaultValue = false)]
        bool IgnorePartyGeneral { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreStrong { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreQuest { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreEvent { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreDimensionPillar { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreCaveTrap { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreSnowSlave { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreEnvy { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreDontAttack { get; set; }
        [Option(DefaultValue = false)]
        bool IgnoreRemainIdle { get; set; }
        [Option(DefaultValue = true)]
        bool ReturnDead { get; set; }
        [Option(DefaultValue = true)]
        bool ReturnDeadMinutesMoreThan { get; set; }
        [Option(DefaultValue = 5)]
        int XMinutes { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnFull { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnUniqueFound { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnGMFound { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnNoVigours { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnNoUniversalPills { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnNoSpecialPills { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnLowHPPots { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnHPPotsX { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnLowMPPots { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnMpPotsX { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnLowAmmo { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnAmmoX { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnLowWeaponDur { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnWeaponDurX { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnBrokenItems { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnBrokenItemsX { get; set; }
        [Option(DefaultValue = false)]
        bool ReturnQuestsDone { get; set; }
        [Option(DefaultValue = 0)]
        int ReturnQuestsDoneX { get; set; }
        [Option(DefaultValue = true)]
        bool TrainingUseArea2 { get; set; }
        [Option(DefaultValue = false)]
        bool TrainingStayIdleOption { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowHP { get; set; }
        [Option(DefaultValue = 50)]
        int AutoPotionsLowHPX { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowVigourHP { get; set; }
        [Option(DefaultValue = 20)]
        int AutoPotionsLowVigourHPX { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowMP { get; set; }
        [Option(DefaultValue = 30)]
        int AutoPotionsLowMPX { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowVigourMP { get; set; }
        [Option(DefaultValue = 10)]
        int AutoPotionsLowVigourMPX { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowPetHP { get; set; }
        [Option(DefaultValue = 50)]
        int AutoPotionsLowPetHPX { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowPetHGP { get; set; }
        [Option(DefaultValue = 20)]
        int AutoPotionsLowPetHGPX { get; set; }
        [Option(DefaultValue = true)]
        bool RevivePet { get; set; }
        [Option(DefaultValue = true)]
        bool AutoPotionsLowTransportHP { get; set; }
        [Option(DefaultValue = 1000)]
        int AutoPotionDelay { get; set; }
        [Option(DefaultValue = false)]
        bool AutoPotionBadStatusUsePillOrSpell { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint AutoPotionBadStatusSpell { get; set; }
        [Option(DefaultValue = false)]
        bool AutoPotionCurseUsePillOrSpell { get; set; }
        [Option(DefaultValue = (uint)0)]
        uint AutoPotionCurseSpell { get; set; }
        [Option(DefaultValue = 50)]
        int AutoPotionsTransportHPLowerThanX { get; set; }
        Area TrainingArea1 { get; set; }
        Area TrainingArea2 { get; set; }
    }

    public interface ISkillSettings
    {
        Skill Imbue { get; set; }
        Skill SpeedBuff { get; set; }
        Skill HealSpell { get; set; }
        Skill ManaSpell { get; set; }
        Skill ResSpell { get; set; }

        [Option(DefaultValue = false)]
        bool CycleSkills { get; set; }
        [Option(DefaultValue = false)]
        bool UseHeal { get; set; }
        [Option(DefaultValue = 50)]
        int UseHealPercentage { get; set; }
        [Option(DefaultValue = false)]
        bool UseMPSpell { get; set; }
        [Option(DefaultValue = 30)]
        int UseMPSpellPercentage { get; set; }
        [Option(DefaultValue = false)]
        bool UseRes { get; set; }
        [Option(DefaultValue = true)]
        bool SpeedBuffOrPot { get; set; }
        [Option(DefaultValue = false)]
        bool BuffAllOrMembers { get; set; }
        [Option(DefaultValue = "")]
        IEnumerable<uint> SkillList { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> Buffs { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> MembersToBuff { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> MembersBuffs { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> Initiations { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> General { get; /*set;*/ }
        [Option(DefaultValue = "")]
        IEnumerable<uint> Finishers { get; /*set;*/ }
    }

    public interface IPartySettings
    {
        [Option(DefaultValue = PartyObjective.Hunting)]
        PartyObjective Objective { get; set; }
        [Option(DefaultValue = (byte)0)]
        byte MinLevel { get; set; }
        [Option(DefaultValue = (byte)80)]
        byte MaxLevel { get; set; }
        [Option(DefaultValue = PartyType.LTP)]
        PartyType Type { get; set; }
        [Option(DefaultValue = false)]
        bool AutoJoin { get; set; }
        [Option(DefaultValue = true)]
        bool AutoInviteAccept { get; set; }
        [Option(DefaultValue = true)]
        bool AutoReform { get; set; }
        [Option(DefaultValue = true)]
        bool MembersCanInvite { get; set; }
    }

    public interface IStallSettings
    {
        [Option(DefaultValue = true)]
        bool ResumeTraining { get; set; }
        [Option(DefaultValue = true)]
        bool AutoAdd { get; set; }
    }

    public interface ILoggingSettings
    {
        [Option(DefaultValue = true)]
        bool Debug { get; set; }
        [Option(DefaultValue = true)]
        bool Information { get; set; }
        [Option(DefaultValue = true)]
        bool Warning { get; set; }
        [Option(DefaultValue = true)]
        bool Error { get; set; }
        [Option(DefaultValue = true)]
        bool Botting { get; set; }
        [Option(DefaultValue = true)]
        bool Script { get; set; }
        [Option(DefaultValue = true)]
        bool Pk2 { get; set; }
        [Option(DefaultValue = true)]
        bool Network { get; set; }
        [Option(DefaultValue = true)]
        bool Handler { get; set; }
        [Option(DefaultValue = true)]
        bool All { get; set; }
    }

    public interface ISettings
    {
        [JsonProperty(Order = 1)]
        ILoginSettings Login { get; set; }
        [JsonProperty(Order = 2)]
        ITownSettings Town { get; set; }
        [JsonProperty(Order = 3)]
        IScriptSettings Script { get; set; }
        [JsonProperty(Order = 4)]
        IFilterSettings Filter { get; set; }
        [JsonProperty(Order = 5)]
        ITrainingSettings Training { get; set; }
        [JsonProperty(Order = 6)]
        ISkillSettings Skills { get; set; }
        [JsonProperty(Order = 7)]
        IPartySettings Party { get; set; }
        [JsonProperty(Order = 8)]
        IStallSettings Stall { get; set; }
        [JsonProperty(Order = 9)]
        ILoggingSettings Logging { get; set; }

        [Option(DefaultValue = true)]
        bool UpgradeRequired { get; set; }
    }
}
