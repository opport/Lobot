﻿using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LobotAPI.Globals
{
    public class ServerGlobal
    {
        public static List<Silkroad.Server> server;
        public static List<Silkroad.ListedCharacter> listedCharacters;

        public static Dictionary<String, TabPage> activeConversations = new Dictionary<String, TabPage>();
    }
}
