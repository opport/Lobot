﻿using System;

namespace LobotAPI.Globals
{
    public class NetworkGlobal
    {
        /// <summary>
        /// The Directory containing the sro_client.exe file.
        /// </summary>
        public static string ClientDirectory = "";
        /// <summary>
        /// The full path to the sro_client.exe file.
        /// </summary>
        public static string ClientFilePath = "";
        public static string DllFilePath = AppDomain.CurrentDomain.BaseDirectory + "ubx.dll";

        //Proxy
        public static readonly string PROXY_ADDRESS = "127.0.0.1";
        /// <summary>
        /// The port used to connect the client to the proxy.
        /// </summary>
        public static ushort ProxyGatewayPort = 15779;
        /// <summary>
        /// The port used to connect the proxy to the game server.
        /// </summary>
        public static ushort ProxyAgentPort = 20001;
        public static ushort Bot_Port = 20002;

        /// <summary>
        /// The gateway division (login server number) to connect to the login server.
        /// </summary>
        public static string GatewayServerDivision = "";
        /// <summary>
        /// The remote IP address (Joymax port) used to connect to the login server.
        /// </summary>
        public static string GatewayIP = "";
        /// <summary>
        /// The remote IP address (Joymax port) used to connect to the game server.
        /// </summary>
        public static string AgentIP = "";
        /// <summary>
        /// The remote port (Joymax port) used to connect to the login server.
        /// </summary>
        public static ushort AgentPort = 15884;
    }
}
