﻿using LobotAPI.PK2;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace LobotAPI.Globals
{
    public static class CharInfoGlobal
    {
        public static Dictionary<Pk2ItemType, Skill> AutoAttackSkills { get; set; } = new Dictionary<Pk2ItemType, Skill>() { { Pk2ItemType.Other, new Skill(0x01, 1, true) } };

        public static Comparison<PartyEntry> PartyComparison = new Comparison<PartyEntry>((p1, p2) => { return (int)(p1.PartyNumber - p2.PartyNumber); });
        public static Comparison<Skill> SkillComparison = new Comparison<Skill>((s1, s2) => { return string.Compare(s1.Name, s2.Name); });

        public const byte EQUIPMENT_SLOTS = 13;
        public static ObservableConcurrentDictionary<byte, Item> Equipment { get; set; }
        public static ObservableConcurrentDictionary<byte, Item> Inventory { get; set; }
        public static ObservableConcurrentDictionary<byte, Item> PetInventory { get; set; }
        public static byte PetInventorySize { get; set; }

        public static ObservableConcurrentDictionary<byte, Item> AvatarInventory { get; set; }
        public static uint StorageSize = 0;
        public static ObservableConcurrentDictionary<byte, Item> Storage { get; set; }
        public static uint GuildStorageSize = 0;
        public static ObservableConcurrentDictionary<byte, Item> GuildStorage { get; set; }
        public static ObservableCollection<Skill> Skills { get; set; }

        public static ObservableCollection<PartyEntry> PartyMatchingList { get; set; }
        public static ObservableCollection<Item> BuyBackList { get; set; }


        // Lock objects to enable changing observablecollections in threads different from the UI-thread they were created in
        private static object AvatarInventoryLock = new object();
        private static object EquipmentsLock = new object();
        private static object InventoryLock = new object();
        private static object StorageLock = new object();
        private static object PetInventoryLock = new object();
        private static object GuildStorageLock = new object();
        private static object SkillsLock = new object();

        private static object PartyMatchingLock = new object();
        private static object BuyBackLock = new object();

        static CharInfoGlobal()
        {
            AvatarInventory = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(AvatarInventory, AvatarInventoryLock);

            Equipment = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(Equipment, EquipmentsLock);

            Inventory = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(Inventory, InventoryLock);

            PetInventory = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(PetInventory, PetInventoryLock);

            Storage = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(Storage, StorageLock);

            GuildStorage = new ObservableConcurrentDictionary<byte, Item>();
            BindingOperations.EnableCollectionSynchronization(GuildStorage, GuildStorageLock);

            Skills = new ObservableCollection<Skill>();
            BindingOperations.EnableCollectionSynchronization(Skills, SkillsLock);

            PartyMatchingList = new ObservableCollection<PartyEntry>();
            BindingOperations.EnableCollectionSynchronization(PartyMatchingList, PartyMatchingLock);

            BuyBackList = new ObservableCollection<Item>();
            BindingOperations.EnableCollectionSynchronization(BuyBackList, BuyBackLock);
        }
    }
}
