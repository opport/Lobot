﻿namespace LobotAPI.StateMachine
{
    /// <summary>
    /// The parent state for bots
    /// </summary>
    public interface IState
    {
        bool IsRunning { get; }
        void Start();
        void Stop();
    }
}
