﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.StateMachine
{
    public class StateChangedArgs
    {
        public IState State;

        public StateChangedArgs(IState s)
        {
            this.State = s ?? throw new ArgumentNullException(nameof(s));
        }
    }
}
