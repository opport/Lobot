﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.StateMachine
{
    public enum StateTypes
    {
        Idle,
        Townscript,
        WalkScript,
        Stalling,
        Alchemy,
        Exchange,
        Trade,
        //Town
        Storage,
        ShopSmith,
        ShopPotion,
        ShopSpecialty,
        ShopStable,
        //Training
        Attack,
        CastBuff,
        CastHeal,
        Pickup,
        Resurrect,
        Return,
        Walk
    }
}
