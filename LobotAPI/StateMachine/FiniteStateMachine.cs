﻿namespace LobotAPI.StateMachine
{
    public abstract class FiniteStateMachine
    {
        protected bool IsRunning = false;

        private IState _prevState;
        private IState _state;

        public IState PrevState { get => _prevState; protected set => _prevState = value; }
        public IState State { get => _state; protected set => _state = value; }
        public void Initialise(IState state) { State = state; }

        public abstract void Start();
        public abstract void Stop();
    }
}
