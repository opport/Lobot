﻿using System;

namespace LobotAPI.StateMachine
{
    [Serializable]
    public class StateChangeException : Exception
    {
        public StateChangeException() : base() { }
        public StateChangeException(string message) : base(message) { }
        public StateChangeException(IState targetState) : base($"A transition loop occured in state {targetState.GetType().Name}") { }
    }
}
