﻿using SilkroadSecurityApi;

namespace LobotAPI.Packets
{
    /// <summary>
    /// The abstract derived class of ICommand.
    /// This abstraction is used to implement default methods for subclasses.
    /// </summary>
    public abstract class Command : ICommand
    {
        public abstract ushort Opcode { get; }

        abstract protected Packet AddPayload();

        protected Packet CreateCommandPacket()
        {
            return AddPayload();
        }

        public Packet Execute()
        {
            return CreateCommandPacket();
        }

    }
}
