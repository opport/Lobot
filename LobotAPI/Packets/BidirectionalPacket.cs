﻿using SilkroadSecurityApi;
using System;

namespace LobotAPI.Packets
{
    /// <summary>
    /// The abstract derived class of ICommand and IHandler.
    /// This abstraction is used to implement default methods for subclasses that send and receive packets.
    /// </summary>
    public abstract class BidirectionalPacket : ICommand, IHandler
    {
        public abstract ushort ServerOpcode { get; }

        public abstract ushort Opcode { get; }

        public virtual event EventHandler HandledPacket = delegate { };

        abstract protected Packet AddPayload();

        protected Packet CreateCommandPacket()
        {
            return AddPayload();
        }

        public Packet Execute()
        {
            return CreateCommandPacket();
        }

        protected abstract ICommand ParsePacket(Packet p);

        public ICommand Handle(Packet p)
        {
            p.Lock();
            return ParsePacket(p);
        }

        protected virtual void OnHandled<T>(T e) where T : EventArgs
        {
            EventHandler handler = HandledPacket;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
