﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.Stall
{
    public class StallItemArgs : EventArgs
    {
        public StallItem StallItem;

        public StallItemArgs(StallItem stallItem)
        {
            StallItem = stallItem;
        }
    }
}
