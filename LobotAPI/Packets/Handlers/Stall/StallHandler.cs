﻿using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.Linq;
using static LobotAPI.Structs.Item;

namespace LobotAPI.Packets.Stall
{
    public abstract class StallHandler : BidirectionalPacket
    {
        private static Comparison<StallItem> Comparison = new Comparison<StallItem>((i1, i2) => { return i1.Slot - i2.Slot; });

        public static event EventHandler<StallItemArgs> StallItemCreated = delegate { };
        public static event EventHandler<StallItemArgs> StallItemRemoved = delegate { };

        private static void OnStallItemCreated(StallItemArgs e)
        {
            EventHandler<StallItemArgs> handler = StallItemCreated;
            if (handler != null)
            {
                handler(null, e);
            }
        }


        private static void OnStallItemRemoved(StallItemArgs e)
        {
            EventHandler<StallItemArgs> handler = StallItemRemoved;
            if (handler != null)
            {
                handler(null, e);
            }
        }
        protected StallHandler() { }

        protected static void ParseItems(Packet p)
        {
            // cut this short because the item bytes depend on generic item info
            //while (true)
            //{
            //    byte registeredItemSlot = p.ReadUInt8();   //within Stall (0-9)
            //    if (registeredItemSlot == byte.MaxValue)
            //        break;

            //    //byte[]  registeredItem.< genericItemData > //depends on TypeIDs
            // byte registeredItemSourceSlot = p.ReadUInt8();//from OwnerInventory
            //    ushort registeredItemStackCount = p.ReadUInt16(); //sale stack count
            //    ulong registeredItemPrice = p.ReadUInt64(); //sale price
            //}


            // Previous items for comparison
            List<StallItem> oldItems = StallSettings.StallItems.ToList();

            // Reset stall
            StallSettings.StallItems.Clear();


            for (int i = 0; i < StallSettings.MAX_STALL_ITEMS; i++)
            {
                Item newItem = new Item();

                byte stallSlot = p.ReadUInt8();

                if (stallSlot == 0xFF)
                    break;

                uint ownerID = p.ReadUInt32(); // self should be 0
                newItem.RefItemID = p.ReadUInt32();

                if (newItem.Pk2Item.IsEquipmentItem()) // ITEMS
                {
                    Equipment e = new Equipment(newItem);
                    e.OptLevel = p.ReadUInt8();
                    e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.ReadUInt64());
                    e.Durability = p.ReadUInt32(); //(=> Durability)
                    e.MagParamNum = p.ReadUInt8(); //(=> Blue, Red)

                    for (int j = 0; j < e.MagParamNum; j++)
                    {
                        MagParamType type = (MagParamType)p.ReadUInt32();
                        uint value = p.ReadUInt32();
                        e.Blues.ChangeValue(type, value);
                    }

                    uint OptType = p.ReadUInt8(); //(1 => Socket)
                    uint OptCount = p.ReadUInt8();

                    for (int j = 0; j < OptCount; j++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.NParam1 = p.ReadUInt32(); //(=> Reference to Socket)

                    }
                    OptType = p.ReadUInt8(); //(2 => Advanced elixir)
                    OptCount = p.ReadUInt8();

                    for (int j = 0; j < OptCount; j++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.OptValue = p.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")

                    }
                }
                else if (newItem.Type == Pk2ItemType.AbilityPet || newItem.Type == Pk2ItemType.GrowthPet) // COS
                {
                    AbilityPet abilityPet = new AbilityPet(newItem);
                    abilityPet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                    abilityPet.UniqueID = p.ReadUInt32();
                    abilityPet.Name = p.ReadAscii();

                    if (newItem.Type == Pk2ItemType.AbilityPet)
                    {
                        abilityPet.SecondsToRentEndTime = p.ReadUInt32();
                    }
                    abilityPet.unk02 = p.ReadUInt8(); // -> Check for != 0
                }
                else // ETC
                {
                    newItem.StackCount = p.ReadUInt16();
                }

                newItem.Slot = p.ReadUInt8(); //from OwnerInventory
                newItem.StackCount = p.ReadUInt16(); //ushort saleStackCount 
                ulong salePrice = p.ReadUInt64();

                StallItem stallItem = new StallItem(newItem, stallSlot, salePrice);

                StallSettings.StallItems.InsertSorted(stallItem, Comparison);


            }

            // All removed items - in old but not in new
            foreach (StallItem stallItem in oldItems.Where(i => !StallSettings.StallItems.Contains(i)))
            {
                OnStallItemRemoved(new StallItemArgs(stallItem));
            }

            // All added items - in new but not in old
            foreach (StallItem stallItem in StallSettings.StallItems.Where(i => !oldItems.Contains(i)))
            {
                OnStallItemCreated(new StallItemArgs(stallItem));
            }

        }
    }
}
