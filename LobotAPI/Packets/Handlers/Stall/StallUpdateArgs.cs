﻿using System;

namespace LobotAPI.Packets.Stall
{
    public class StallUpdateArgs : EventArgs
    {
        public StallUpdateType UpdateType;

        public StallUpdateArgs(StallUpdateType updateType)
        {
            UpdateType = updateType;
        }
    }
}