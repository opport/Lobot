﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.Party
{
    public class PartyInvitationArgs : EventArgs
    {
        public uint InviterID;
        public PartyType PartyType;

        public PartyInvitationArgs(uint inviterID, PartyType partyType)
        {
            InviterID = inviterID;
            PartyType = partyType;
        }
    }
}
