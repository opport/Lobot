﻿using System;

namespace LobotAPI.Packets.Party
{
    public class PartyJoinArgs : EventArgs
    {
        public uint PartyNumber;
        public uint MemberID;

        public PartyJoinArgs(uint partyNumber, uint playerID)
        {
            PartyNumber = partyNumber;
            MemberID = playerID;
        }
    }
}
