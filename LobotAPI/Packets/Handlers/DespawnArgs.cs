﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LobotAPI.Globals.SpawnListsGlobal;

namespace LobotAPI.Packets
{
    public class DespawnArgs : EventArgs
    {
        public uint uniqueID;
        public BotLists Type;

        public DespawnArgs(uint uniqueID, BotLists list)
        {
            this.uniqueID = uniqueID;
            Type = list;
        }
    }
}
