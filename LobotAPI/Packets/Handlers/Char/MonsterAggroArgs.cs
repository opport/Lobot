﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.Char
{
    public class MonsterAggroArgs : EventArgs
    {
        public Monster Monster;

        public MonsterAggroArgs(Monster monster)
        {
            Monster = monster;
        }
    }
}
