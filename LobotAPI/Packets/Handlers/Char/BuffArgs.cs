﻿using System;

namespace LobotAPI.Packets.Char
{
    public class BuffArgs : EventArgs
    {
        public uint CasterID;
        public uint RefID;
        public uint UniqueID;

        public BuffArgs(uint uniqueID, uint refID = 0, uint casterID = 0)
        {
            CasterID = casterID;
            RefID = refID;
            UniqueID = uniqueID;
        }
    }
}
