﻿using System;

namespace LobotAPI.Packets.Char
{
    public class CharStopMovementArgs : EventArgs
    {
        public uint uniqueID;

        public CharStopMovementArgs(uint uniqueID)
        {
            this.uniqueID = uniqueID;
        }
    }
}
