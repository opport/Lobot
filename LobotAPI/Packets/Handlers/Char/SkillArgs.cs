﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.Char
{
    public class SkillArgs : EventArgs
    {
        public Skill skill;

        public SkillArgs(Skill skill)
        {
            this.skill = skill ?? throw new ArgumentNullException(nameof(skill));
        }
    }
}
