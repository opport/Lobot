﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.Packets.Char
{
    public class SkillErrorArgs : EventArgs
    {
        public SkillErrorType Type;

        public SkillErrorArgs(SkillErrorType type)
        {
            Type = type;
        }
    }
}
