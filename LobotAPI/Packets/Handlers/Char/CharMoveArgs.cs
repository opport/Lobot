﻿using LobotAPI.Scripting;
using System;
using System.Windows;

namespace LobotAPI.Packets.Char
{
    public class CharMoveArgs : EventArgs
    {
        public uint UniqueID;
        public Point Origin;
        public Point Destination;
        public uint OriginAngle;

        public CharMoveArgs(uint uniqueID, Point destination = default(Point), Point origin = default(Point), ushort originAngle = 0)
        {
            UniqueID = uniqueID;
            this.Destination = destination;
            this.Origin = origin;
            this.OriginAngle = originAngle;
        }

        public CharMoveArgs(uint uniqueID, uint x, uint y, uint z = 0)
        {
            UniqueID = uniqueID;
            this.Origin = new Point(x, y);
        }
    }
}
