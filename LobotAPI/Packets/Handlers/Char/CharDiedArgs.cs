﻿namespace LobotAPI.Handlers.Char
{
    public class CharDiedArgs
    {
        public uint UniqueID;

        public CharDiedArgs(uint uniqueID)
        {
            UniqueID = uniqueID;
        }
    }
}
