﻿namespace LobotAPI.Packets.Quest
{
    public enum QuestMark : byte
    {
        New = 1,
        Open = 2,
        Complete = 3,
        Hard = 4,
    }
}
