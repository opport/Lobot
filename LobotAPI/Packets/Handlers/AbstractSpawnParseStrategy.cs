﻿using SilkroadSecurityApi;
using System;

namespace LobotAPI.Packets
{
    public abstract class AbstractSpawnParseStrategy
    {
        public static event EventHandler<SpawnArgs> Parsed = delegate { };

        public abstract void Parse(Packet p, uint refID);

        protected virtual void OnParsed(SpawnArgs e)
        {
            EventHandler<SpawnArgs> handler = Parsed;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
