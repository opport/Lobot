﻿using LobotAPI.Globals;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LobotAPI.Globals.SpawnListsGlobal;

namespace LobotAPI.Packets
{
    public class SpawnArgs : EventArgs
    {
        public ISpawnType Spawn;
        public BotLists List;

        public SpawnArgs(ISpawnType spawn, BotLists list)
        {
            Spawn = spawn ?? throw new ArgumentNullException(nameof(spawn));
            List = list;
        }
    }
}
