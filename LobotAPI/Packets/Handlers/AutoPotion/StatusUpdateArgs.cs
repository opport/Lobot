﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.AutoPotion
{
    public class StatusUpdateArgs : EventArgs
    {
        public uint UniqueID;
        public HPMPUpdateType Type;
        public DebuffStatus Status;
        public bool StatusBeginOrEnd;

        public StatusUpdateArgs(uint uniqueID, HPMPUpdateType type, DebuffStatus status = DebuffStatus.Normal, bool statusBeginOrEnd = false)
        {
            UniqueID = uniqueID;
            Type = type;
            Status = status;
            StatusBeginOrEnd = statusBeginOrEnd;
        }
    }
}
