﻿namespace LobotAPI.Packets.AutoPotion
{
    public enum HPMPUpdateType : byte
    {
        OnlyHP = 0x01,
        OnlyMP = 0x02,
        HPAndMP = 0x03,
        BadStatus = 0x04,
        HPAndBadStatusOrMonster = 0x05,
        MPAndBadStatus = 0x06
    }
}
