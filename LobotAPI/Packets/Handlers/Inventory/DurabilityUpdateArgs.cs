﻿using LobotAPI.Structs;
using System;

namespace LobotAPI.Packets.Inventory
{
    public class DurabilityUpdateArgs : EventArgs
    {
        public Structs.Item UpdatedItem;

        public DurabilityUpdateArgs(Item i)
        {
            this.UpdatedItem = i ?? throw new ArgumentNullException(nameof(i));
        }
    }
}
