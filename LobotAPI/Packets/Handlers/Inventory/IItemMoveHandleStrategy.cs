﻿using LobotAPI.Packets.Inventory;

namespace LobotAPI.Packets.Inventory
{
    public interface IItemMoveHandleStrategy
    {
        void HandleMove(ItemMoveParams p);
    }
}
