﻿using LobotAPI.Structs;
using System;

namespace LobotDLL.Packets.Inventory
{
    public class ItemUseArgs
    {
        public byte Slot;
        public Item Item;

        public ItemUseArgs(byte slot, Item item)
        {
            Slot = slot;
            Item = item ?? throw new ArgumentNullException(nameof(item));
        }
    }
}
