﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.Packets.Commands.Alchemy
{
    public enum AlchemyAction : byte
    {
        Cancel = 1,
        Fuse = 2,
        Create = 3, //for Socket
        Remove = 4, //for Socket
    }
}
