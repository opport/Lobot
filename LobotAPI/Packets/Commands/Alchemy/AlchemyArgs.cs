﻿using System;

namespace LobotAPI.Packets.Commands.Alchemy
{
    public class AlchemyArgs : EventArgs
    {
        public bool Success;
        public byte OptLevel;
        public string ItemString;

        public AlchemyArgs(bool success, byte optLevel, string itemString)
        {
            Success = success;
            OptLevel = optLevel;
            ItemString = itemString;
        }
    }
}
