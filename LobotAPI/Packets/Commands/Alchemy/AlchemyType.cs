﻿namespace LobotAPI.Packets.Commands.Alchemy
{
    public enum AlchemyType : byte
    {
        Disjoin = 1,
        Manufacture = 2,
        Elixir = 3,
        MagicStone = 4,
        AttributeStone = 5,
        AdvancedElixir = 8,
        Socket = 9,
    }
}
