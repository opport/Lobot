﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System.Collections.Generic;
using static LobotAPI.Structs.Item;

namespace LobotAPI.Packets.Inventory
{
    public abstract class AbstractItemMoveStrategy
    {
        public abstract ItemMovementType Type { get; }

        //move in inventory
        //[C -> S][7034]
        //00                flag
        //0D                from slot = 13
        //15                to slot = 21
        //14 00             amount = 20
        public virtual Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            p.Packet.WriteUInt16(p.Amount);

            return p.Packet;
        }

        protected void Move(IDictionary<byte, Item> itemContainer, byte fromSlot, byte toSlot)
        {
            itemContainer[toSlot] = itemContainer[fromSlot];
            itemContainer[toSlot].Slot = toSlot;
            itemContainer.Remove(fromSlot);
        }

        protected void Switch(IDictionary<byte, Item> itemContainer, Item fromItem, Item toItem)
        {
            // change slots to match new items
            byte tempSlot = fromItem.Slot;
            fromItem.Slot = toItem.Slot;
            toItem.Slot = tempSlot;

            itemContainer[fromItem.Slot] = fromItem;
            // delete to ensure change in observablecollection (inconsistent behaviour compared to monster.hp changes)
            itemContainer.Remove(toItem.Slot);
            itemContainer[toItem.Slot] = toItem;
        }

        protected void Split(IDictionary<byte, Item> itemContainer, Item fromItem, byte toSlot, ushort amount)
        {
            itemContainer[fromItem.Slot].StackCount -= amount;
            itemContainer[toSlot] = new Item(fromItem);
            itemContainer[toSlot].StackCount = amount;
            itemContainer[toSlot].Slot = toSlot;
        }

        protected void Stack(IDictionary<byte, Item> itemContainer, Item fromItem, Item toItem, ushort amount)
        {
            if (PK2DataGlobal.Items.TryGetValue(fromItem.RefItemID, out Pk2Item item))
            {
                //switch stacks if toItem already maxed out
                if (toItem.StackCount == item.MaxStacks)
                {
                    toItem.StackCount = amount;
                    fromItem.StackCount = (uint)item.MaxStacks;
                }
                // stack onto target item
                // and reduce on initial item
                else if (item.MaxStacks >= amount + toItem.StackCount)
                {
                    toItem.StackCount += amount;
                    fromItem.StackCount -= amount;
                }
                else
                {
                    toItem.StackCount = (uint)item.MaxStacks;
                    fromItem.StackCount = (uint)item.MaxStacks - amount;
                }

                // set from object, to trigger collectionchange
                itemContainer[toItem.Slot] = toItem;

                // Remove fromItem if all stacks were transferred or set to new fromItem
                if (fromItem.StackCount == 0)
                {
                    itemContainer.Remove(fromItem.Slot);
                }
                else
                {
                    itemContainer[fromItem.Slot] = fromItem;
                }
            }
        }

        protected void ParseNewItem(ref Item item, ItemMoveParams p)
        {
            uint unknown = p.Packet.ReadUInt32();
            item.RefItemID = p.Packet.ReadUInt32();

            if (PK2DataGlobal.Items == null)
            {
                PK2Utils.ExtractAllInformation();
            }

            if (PK2DataGlobal.Items.TryGetValue(item.RefItemID, out Pk2Item value))
            {
                if (value.IsEquipmentItem())
                {
                    Equipment e = new Equipment(item);
                    e.OptLevel = p.Packet.ReadUInt8();
                    e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.Packet.ReadUInt64());
                    e.Durability = p.Packet.ReadUInt32(); //(=> Durability)
                    e.MagParamNum = p.Packet.ReadUInt8(); //(=> Blue, Red)

                    for (int j = 0; j < e.MagParamNum; j++)
                    {
                        MagParamType type = (MagParamType)p.Packet.ReadUInt32();
                        uint valueBlue = p.Packet.ReadUInt32();
                        e.Blues.ChangeValue(type, valueBlue);
                    }

                    uint OptType = p.Packet.ReadUInt8(); //(1 => Socket)
                    uint OptCount = p.Packet.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.Packet.ReadUInt8();
                        e.ItemOption.ID = p.Packet.ReadUInt32();
                        e.ItemOption.NParam1 = p.Packet.ReadUInt32(); //(=> Reference to Socket)

                    }
                    OptType = p.Packet.ReadUInt8(); //(2 => Advanced elixir)
                    OptCount = p.Packet.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.Packet.ReadUInt8();
                        e.ItemOption.ID = p.Packet.ReadUInt32();
                        e.ItemOption.OptValue = p.Packet.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")

                    }
                    //Notice for Advanced Elixir modding.
                    //Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
                    //You also have to fix error when "Buy back" from NPC
                    //## Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater::DoWork_AddBindingOption 1366
                    //Storage Operation Failed!!! [OperationType: 34, ErrorCode: 174627]
                    //Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
                    //AQ Failed! Log out!! [AQType: 1]			
                    CharInfoGlobal.Inventory[item.Slot] = e;
                }
                else
                {
                    switch (value.Type)  //from Reference
                    {
                        case Pk2ItemType.AttributeStone:
                        case Pk2ItemType.MagicStone:
                            item.StackCount = p.Packet.ReadUInt16();
                            //item.AttributeAssimilationProbability = p.Packet.ReadUInt8(); // -- probability not exposed in all silkroad versions
                            CharInfoGlobal.Inventory[item.Slot] = item;
                            break;

                        case Pk2ItemType.GrowthPet:
                            PetScroll pet = new PetScroll(item);
                            pet.Status = (PetStatus)p.Packet.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                            pet.UniqueID = p.Packet.ReadUInt32();
                            //pet.NameLength = p.Packet.ReadUInt16();
                            pet.Name = p.Packet.ReadAscii();
                            pet.unk02 = p.Packet.ReadUInt8(); // -> Check for != 0
                            CharInfoGlobal.Inventory.Add(item.Slot, pet);
                            break;
                        case Pk2ItemType.AbilityPet:
                            AbilityPet abilityPet = new AbilityPet(item);
                            abilityPet.Status = (PetStatus)p.Packet.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                            if (abilityPet.Status != PetStatus.Unsummoned)
                            {
                                abilityPet.UniqueID = p.Packet.ReadUInt32();
                                //abilityPet.NameLength = p.Packet.ReadUInt16();
                                abilityPet.Name = p.Packet.ReadAscii();
                                abilityPet.SecondsToRentEndTime = p.Packet.ReadUInt32();
                                abilityPet.unk02 = p.Packet.ReadUInt8(); // -> Check for != 0
                            }
                            CharInfoGlobal.Inventory.Add(item.Slot, abilityPet);
                            break;
                        case Pk2ItemType.ItemExchangeCoupon:
                            item.StackCount = p.Packet.ReadUInt16();
                            uint MagParamNum = p.Packet.ReadUInt8();

                            for (int i = 0; i < MagParamNum; i++)
                            {
                                ItemExchangeCoupon iec = new ItemExchangeCoupon(item);
                                iec.MagParamValues.Add(p.Packet.ReadUInt64());
                                //1. MagParam => CouponRefItemID [fixed]
                                //2. MagParam => CouponItemAmount [fixed]
                                //When Coupon holds Scrolls or similar, these 2 MagParams above are used.
                                //When Coupon holds Equipment, 8 MagParams are used
                                //They are defined in database by "[BIIV]<M:str,1,3><M:int,1,3><O:3>"
                                //As MagParams we get those 2 above and
                                //01 4D 01 72 74 73 00 00                           MagParam3 - .M.rts..........	(=> There is our str, don't ask me why its reversed)
                                //03 00 00 00 00 00 00 00                           MagParam4 - MagParam.Value		(=> Amount of +STR)
                                //01 4D 01 74 6E 69 00 00                           MagParam5 - .M.tni..........	(=> There is our int, don't ask me why its reversed)
                                //03 00 00 00 00 00 00 00                           MagParam6 - MagParam.Value		(=>	Amount of +INT)
                                //01 4F 00 00 00 00 00 00                           MagParam7 - .O..............	(=> There is our O for OptLevel)
                                //03 00 00 00 00 00 00 00                           MagParam8 - OptLevel			(=> Amount of +Overall)
                            }
                            CharInfoGlobal.Inventory[item.Slot] = item;
                            break;
                        case Pk2ItemType.MagicCube:
                            MagicCube magicCube = new MagicCube(item);
                            magicCube.StoredItemCount = p.Packet.ReadUInt32();
                            CharInfoGlobal.Inventory[item.Slot] = item;
                            break;
                        default:
                            item.StackCount = p.Packet.ReadUInt16();
                            CharInfoGlobal.Inventory[item.Slot] = item;
                            break;
                    }
                }
            }
            else
            {
                CharInfoGlobal.Inventory[item.Slot] = item;
            }
        }
    }
}
