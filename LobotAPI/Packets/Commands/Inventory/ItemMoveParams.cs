﻿using SilkroadSecurityApi;

namespace LobotAPI.Packets.Inventory
{
    public struct ItemMoveParams
    {
        public ItemMoveParams(byte fromSlot = 0, byte toSlot = 0, ushort amount = 0, Packet p = null)
        {
            FromSlot = fromSlot;
            ToSlot = toSlot;
            Amount = amount;
            Packet = p;
        }

        public byte FromSlot { get; set; }
        public byte ToSlot { get; set; }
        public ushort Amount { get; set; }
        public Packet Packet { get; set; }
    }
}
