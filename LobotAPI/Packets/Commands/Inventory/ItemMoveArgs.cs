﻿namespace LobotAPI.Packets.Inventory
{
    public class ItemMoveArgs
    {
        public byte FromSlot;
        public byte ToSlot;
        public ItemMovementType Type;

        public ItemMoveArgs(byte fromSlot, byte toSlot, ItemMovementType type)
        {
            FromSlot = fromSlot;
            ToSlot = toSlot;
            Type = type;
        }
    }
}
