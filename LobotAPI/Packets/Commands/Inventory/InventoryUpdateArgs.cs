﻿using System;

namespace LobotAPI.Packets.Commands.Inventory
{
    public class InventoryUpdateArgs : EventArgs
    {
        public byte Itemslot;
        public byte UpdateType;
        public byte NewStatus;

        public InventoryUpdateArgs(byte itemslot, byte updateType, byte updateInfo = 0)
        {
            Itemslot = itemslot;
            UpdateType = updateType;
            NewStatus = updateInfo;
        }
    }
}
