﻿using SilkroadSecurityApi;
using System;

namespace LobotAPI.Packets
{
    /// <summary>
    /// The abstract derived class of IHandler.
    /// This abstraction is used to implement default methods for subclasses.
    /// </summary>
    public abstract class Handler : IHandler
    {
        public virtual event EventHandler HandledPacket = delegate { };

        public abstract ushort ServerOpcode { get; }

        protected abstract ICommand ParsePacket(Packet p);

        public ICommand Handle(Packet p)
        {
            p.Lock();
            return ParsePacket(p);
        }

        protected virtual void OnHandled<T>(T e) where T : EventArgs
        {
            EventHandler handler = HandledPacket;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
