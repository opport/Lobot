﻿using SilkroadSecurityApi;

namespace LobotAPI.Packets
{
    /// <summary>
    /// A request sent to the server to execute an bot action.
    /// Their main use is to run the state machine.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// The unique code linked to the command.
        /// It determines the packet type recognized by the server.
        /// </summary>
        ushort Opcode { get; }

        /// <summary>
        /// Command to execute.
        /// The main use is to write then send a packet.
        /// </summary>
        Packet Execute();

    }
}
