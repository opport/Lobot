﻿using SilkroadSecurityApi;
using System;

namespace LobotAPI.Packets
{
    public interface IHandler
    {
        /// <summary>
        /// The standard event to be fired once a packet has been handled.
        /// </summary>
        event EventHandler HandledPacket;

        /// <summary>
        /// The unique code linked to the server response.
        /// It determines the packet type sent by the server.
        /// </summary>
        ushort ServerOpcode { get; }
        /// <summary>
        /// Command to handle server responses.
        /// The received packet is evaluated and a command can be sent as a reply.
        /// </summary>
        ICommand Handle(Packet p);
    }
}
