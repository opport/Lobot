﻿using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace LobotAPI
{
    public static class Logger
    {
        private readonly static int LINES_BEFORE_WRITING_TO_FILE = 1;
        private static readonly object SyncObject = new object();
        public static List<string> LogList = new List<string>();

        private static string FileName { get { return System.DateTime.Today.ToString("yyyy-MM-dd") + ".txt"; } }
        private static string LogTime { get { return System.DateTime.Now.ToShortTimeString(); } }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static bool Debug { get => Settings.Instance.Logging.Debug; set { Settings.Instance.Logging.Debug = value; NotifyPropertyChanged(); } }
        public static bool Information { get => Settings.Instance.Logging.Information; set { Settings.Instance.Logging.Information = value; NotifyPropertyChanged(); } }
        public static bool Warning { get => Settings.Instance.Logging.Warning; set { Settings.Instance.Logging.Warning = value; NotifyPropertyChanged(); } }
        public static bool Error { get => Settings.Instance.Logging.Error; set { Settings.Instance.Logging.Error = value; NotifyPropertyChanged(); } }
        public static bool Botting { get => Settings.Instance.Logging.Botting; set { Settings.Instance.Logging.Botting = value; NotifyPropertyChanged(); } }
        public static bool Script { get => Settings.Instance.Logging.Script; set { Settings.Instance.Logging.Script = value; NotifyPropertyChanged(); } }
        public static bool Pk2 { get => Settings.Instance.Logging.Pk2; set { Settings.Instance.Logging.Pk2 = value; NotifyPropertyChanged(); } }
        public static bool Network { get => Settings.Instance.Logging.Network; set { Settings.Instance.Logging.Network = value; NotifyPropertyChanged(); } }
        public static bool Handler { get => Settings.Instance.Logging.Handler; set { Settings.Instance.Logging.Handler = value; NotifyPropertyChanged(); } }
        public static bool All { get => Settings.Instance.Logging.All; set { Settings.Instance.Logging.All = value; NotifyPropertyChanged(); } }


        public static Dictionary<LogLevel, bool> LogLevelActiveStatus = new Dictionary<LogLevel, bool>
        {
            {LogLevel.DEBUG, Debug },
            {LogLevel.INFORMATION, Information},
            {LogLevel.WARNING, Warning},
            {LogLevel.ERROR, Error },
            {LogLevel.BOTTING, Botting },
            {LogLevel.SCRIPT, Script},
            {LogLevel.PK2, Pk2},
            {LogLevel.NETWORK, Network},
            {LogLevel.HANDLER, Handler},
            {LogLevel.All, All}
        };

        private static string LogLevelString(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.DEBUG:
                    return "[DBG]";
                case LogLevel.INFORMATION:
                    return "[INFO]";
                case LogLevel.WARNING:
                    return "[WARNING]";
                case LogLevel.ERROR:
                    return "[ERROR]";
                case LogLevel.BOTTING:
                    return "[BOT]";
                case LogLevel.SCRIPT:
                    return "[SCRIPT]";
                case LogLevel.PK2:
                    return "[PK2]";
                case LogLevel.NETWORK:
                    return "[NET]";
                case LogLevel.HANDLER:
                    return "[HANDLER]";
                case LogLevel.All:
                    return "-----";
                default:
                    return "> ";
            }
        }

        /// <summary>
        /// The standard logging function.
        /// The message format corresponding to the log level is added to the Log list before being written to the external log file.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="logMessage"></param>
        public static void Log(LogLevel level, string logMessage)
        {
            // Skip if not active
            if (LogLevelActiveStatus == null || (LogLevelActiveStatus.TryGetValue(level, out bool value) && value == false))
            {
                return;
            }

            try
            {
                lock (SyncObject)
                {
                    LogList.Add(LogTime + LogLevelString(level) + logMessage);

                    if (LogList.Count > LINES_BEFORE_WRITING_TO_FILE)
                    {
                        WriteToFile();
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Log exception: {}", e.Message);
            }

        }

        public static void ProxyOutput(LogLevel level, string identifier, Packet p)
        {
            // Skip if not active
            if (LogLevelActiveStatus != null && (LogLevelActiveStatus.TryGetValue(level, out bool isValid) && isValid))
            {
                Console.WriteLine($"{identifier}: {p.Opcode.ToString("X4")} Data: {BitConverter.ToString(p.GetBytes()).Replace("-", " ")}");
            }
        }

        public static void OpenLog()
        {
            Log(LogLevel.All, "Starting Bot");
            WriteToFile();
        }

        public static void CloseLog()
        {
            Log(LogLevel.All, "Exiting Bot");
            WriteToFile();
        }

        private static void WriteToLog(string msg)
        {

        }

        private static void WriteToFile()
        {
            File.AppendAllLines(DirectoryGlobal.LogFolder + FileName, LogList);
            LogList.Clear();
        }
    }

    public abstract class AbstractLogger
    {
        protected LogLevel Mask;
        private readonly AbstractLogger Successor;

        public readonly string Type;

        protected AbstractLogger(LogLevel mask, AbstractLogger successor, string typeID)
        {
            this.Mask = mask;
            this.Successor = successor;
            this.Type = typeID;
        }

        public bool CanLog(LogLevel level, string logMessage) => level == Mask || Successor == null;
    }
}
