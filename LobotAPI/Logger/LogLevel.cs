﻿using System;

namespace LobotAPI
{
    /// <summary>
    /// Simple logger based on the Chain of Responsibility pattern.
    /// </summary>
    [Flags]
    public enum LogLevel
    {
        DEBUG = 1,
        INFORMATION = 2,
        WARNING = 3,
        ERROR = 4,
        BOTTING = 5,
        SCRIPT = 6,
        PK2 = 7,
        NETWORK = 8,
        HANDLER = 9,
        All
    }
}
