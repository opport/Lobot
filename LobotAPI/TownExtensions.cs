﻿using LobotAPI.Structs;
using System;
using System.Windows;

namespace LobotAPI
{
    public static class TownExtensions
    {

        /// <summary>
        /// Show whether a point is within range of a given town's radius.
        /// </summary>
        /// <param name="town">Town to evaluate</param>
        /// <param name="charLocation">Character location</param>
        /// <returns>Whether point is in polygon</returns>
        /// <seealso href="http://www.visibone.com/inpoly/"/>
        public static Boolean IsInTown(this Town town, Point charLocation)
        {
            Point p1, p2;
            bool inside = false;

            if (town.Polygon.Count < 3)
            {
                return inside;
            }

            Point oldPoint = new Point(town.Polygon[town.Polygon.Count - 1].X, town.Polygon[town.Polygon.Count - 1].Y);

            foreach (Point currentPoint in town.Polygon)
            {
                if (currentPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = currentPoint;
                }
                else
                {
                    p1 = currentPoint;
                    p2 = oldPoint;
                }

                if ((currentPoint.X < charLocation.X) == (charLocation.X <= oldPoint.X)
                    && ((long)charLocation.Y - (long)p1.Y) * (long)(p2.X - p1.X)
                     < ((long)p2.Y - (long)p1.Y) * (long)(charLocation.X - p1.X))
                {
                    inside = !inside;
                }
                oldPoint = currentPoint;

            }
            return inside;
        }
    }
}
