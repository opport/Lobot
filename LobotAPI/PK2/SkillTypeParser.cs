﻿using System;
using System.Collections.Generic;

namespace LobotAPI.PK2
{
    /// <summary>
    /// A condensed Chain of responsibility type parser for skill pk2 entries.
    /// </summary>
    public static class SkillTypeParser
    {
        ////BasicType  == 0
        //Passive = 0,
        ////BasicType == 1
        //Imbue = 1, // Selfbuff -> Powerup scrolls count as imbues as well
        //MovementBuff = 2, // BasicType == 1 && skillAttributes[72] > 0
        //// BasicType == 2
        //Buff = 3,
        //Heal = 4, //Target required = 1 (index 22) && Heal > 0 (index 70)
        //SelfHeal = 5, //Target required = 0 (index 22) && Heal > 0 (index 70)
        //KnockDown = 6,
        //Resurrection = 6, //Target required = 0 (index 22) && Heal > 0 (index 70)
        //Active = 7,
        //Other = 8

        private static Dictionary<Pk2SkillType, Predicate<string[]>> TypePredicate = new Dictionary<Pk2SkillType, Predicate<string[]>>
        {
            {Pk2SkillType.Passive, s => s[8] == "0" },
            {Pk2SkillType.Imbue, s => s[3].Contains("GIGONGTA") || s[3].Contains("POISONA_BLADE") },
            {Pk2SkillType.SpeedBuff, s => s[3].Contains("SKILL_CH_LIGHTNING_GYEONGGONG_A") || s[3].Contains("SKILL_CH_LIGHTNING_GYEONGGONG_C") || s[3].Contains("BARD_SPEEDUPA_MSPEED") || s[3].Contains("SPEED_UP")}, // Chinese,  Euro and Item Speedbuff
            {Pk2SkillType.Buff, s => s[3].Contains("SKILL_CH_SWORD_SHIELD") // Blade
            || s[3].Contains("SKILL_CH_SPEAR_SPIN") // Spear
            || s[3].Contains("SKILL_CH_BOW_CALL") || s[3].Contains("SKILL_CH_BOW_NORMAL") // Bow
            || s[3].Contains("SKILL_CH_COLD_GANGGI") || s[3].Contains("SKILL_CH_COLD_SHIELD") // Cold
            || s[3].Contains("SKILL_CH_FIRE_GONGUP") || s[3].Contains("SKILL_CH_FIRE_GANGGI") || s[3].Contains("SKILL_CH_FIRE_SHIELD") || s[3].Contains("SKILL_CH_FIRE_HWABYEOK") // Fire
            || s[3].Contains("SKILL_CH_LIGHTNING_GWANTONG") || s[3].Contains("SKILL_CH_LIGHTNING_JIPJUNG") // Lightnining
            || s[3].Contains("SKILL_EU_CLERIC_HEALA_TARGET") // Force
            || s[3].Contains("SKILL_EU_WARRIOR_FRENZYA_HEALTH") || s[3].Contains("SKILL_EU_WARRIOR_FRENZYA_PHYSICAL_BLOCK") || s[3].Contains("SKILL_EU_WARRIOR_FRENZYA_MASICAL_BLOCK") || s[3].Contains("SKILL_EU_WARRIOR_FRENZYA_DAMAGE") || s[3].Contains("SKILL_EU_WARRIOR_GUARDA") // Warrior
            || s[3].Contains("SKILL_EU_ROG_POISONA_GUARD") || s[3].Contains("SKILL_EU_ROG_BOWP_MAD_BOW_UP") || s[3].Contains("SKILL_EU_ROG_POIS_DAGP_DAGGAR_UP") // Rogue
            || s[3].Contains("SKILL_EU_WIZARD_MENTALA_DAMAGEUP") || s[3].Contains("SKILL_EU_WIZARD_EARTHA_GUARD") || s[3].Contains("SKILL_EU_WIZARD_MENTALA_DAMAGEUP") // Wizard
            || s[3].Contains("SKILL_EU_WARLOCK_SOULA_CHAOS") || s[3].Contains("SKILL_EU_WARLOCK_SOULA_STUNLINK") || s[3].Contains("SKILL_EU_WARLOCK_SOULA_RETURN") // Warlock
            || s[3].Contains("SKILL_EU_BARD_BATTLAA_GUARD") || s[3].Contains("EU_BARD_SPEEDUPA_HITRATE") || s[3].Contains("SKILL_EU_BARD_RECOVERA_ABNORMAL") || s[3].Contains("SKILL_EU_BARD_DANCEA")
            || s[3].Contains("SKILL_EU_BARD_FORGETA_ATTACK") // Bard
            || s[3].Contains("SKILL_EU_CLERIC_SAINTA_INNOCENT") || s[3].Contains("SKILL_EU_CLERIC_RECOVERYA_HEALSHIELD") || s[3].Contains("SKILL_EU_CLERIC_SAINTA_ABNORMAL") // Cleric
            || s[3].Contains("_DAMAGE_DIVIDE") // Chinese damage divide
            || s[3].Contains("SKILL_OP_HARMONY")}, // GM Skill
            {Pk2SkillType.Heal, s => s[3].Contains("SKILL_CH_WATER_HEAL") // Force
            || s[3].Contains("SKILL_EU_CLERIC_HEALA_TARGET") || s[3].Contains("SKILL_EU_CLERIC_HEALA_DIVIDE") || s[3].Contains("SKILL_EU_CLERIC_HEALA_CYCLE") || s[3].Contains("SKILL_EU_CLERIC_RECOVERYA_TARGET") }, // Cleric
            {Pk2SkillType.SelfHeal, s => s[3].Contains("SN_SKILL_CH_WATER_SELFHEAL") // Force
            || s[3].Contains("SKILL_EU_CLERIC_HEALA_GROUP") || s[3].Contains("SKILL_EU_CLERIC_RECOVERYA_GROUP") || s[3].Contains("SKILL_EU_CLERIC_RECOVERYA_QUICK") }, // Cleric
            {Pk2SkillType.ManaBuff, s => s[3].Contains("SKILL_EU_BARD_RECOVERA_MPHEAL") || s[3].Contains("EU_BARD_RECOVERA_MANATRANS") }, // Bard mana buffs
            {Pk2SkillType.Resurrection, s => s[3].Contains("SKILL_CH_WATER_RESURRECTION") || s[3].Contains("SN_SKILL_EU_CLERIC_REBIRTH") },
            {Pk2SkillType.Debuff, s => s[3].Contains("SKILL_CH_COLD_GIGONGJANG") // Coldwave
            || s[3].Contains("SKILL_CH_COLD_BINGPAN") // Frost nova
            || s[3].Contains("SKILL_CH_WATER_CANCEL") // Vital Spot
            || s[3].Contains("SKILL_EU_WIZARD_COLDA_MANADRY") || s[3].Contains("SKILL_EU_WIZARD_EARTHA_ABNORMAL") || s[3].Contains("SKILL_EU_WIZARD_PSYCHICA_UNTOUCH") // Wizard mana drain, root and fear
            || s[3].Contains("SKILL_EU_WARLOCK_SOULA") || s[3].Contains("SKILL_EU_WARLOCK_RAZEA") || s[3].Contains("SKILL_EU_WARLOCK_CONFUSIONA") // Warlock debuffs
            || s[3].Contains("SKILL_EU_BARD_FORGETA_TARGET") || s[3].Contains("SKILL_EU_BARD_BATTLAA_ROOT")}, // Bard temptation, root
            {Pk2SkillType.Cure, s => s[3].Contains("SKILL_CH_WATER_CURE") // Force Cure
            || s[3].Contains("SKILL_EU_BARD_RECOVERA_ABNORMAL") // Bard cure series
            || s[3].Contains("SKILL_EU_CLERIC_SAINTA_INNOCENT")}, // Cleric cure series
            {Pk2SkillType.KnockDownOnly, s => s[3].Contains("SKILL_CH_SWORD_DOWNATTACK") }, // Blader Skills
            {Pk2SkillType.Knockdown, s => s[3].Contains("SKILL_EU_WARRIOR_TWOHANDA_CHARGE") || s[3].Contains("SKILL_EU_WARRIOR_TWOHANDA_CRY") || s[3].Contains("SKILL_EU_WARRIOR_TWOHANDA_CHARGE")// Warrior 2hand
            || s[3].Contains("SKILL_EU_ROG_BOWA_POWER") || s[3].Contains("SN_SKILL_EU_ROG_DAGGERA_WOUND")}, // Rogue
            {Pk2SkillType.WarlockDOT, s => s[3].Contains("SKILL_EU_WARLOCK_DOT")}, // Warlock DOT
        };

        public static Pk2SkillType GetType(string[] skillEntry)
        {
            foreach (KeyValuePair<Pk2SkillType, Predicate<string[]>> kvp in TypePredicate)
            {
                if (kvp.Value(skillEntry) == true)
                {
                    return kvp.Key;
                }
            }

            return Pk2SkillType.Other;
        }
    }
}
