﻿using LobotAPI.Globals;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LobotAPI.PK2
{
    class Pk2ExtractorSkills : Pk2ExtractorTemplate
    {
        private static Regex nameFilter = new Regex(@"^SKILL_((PUNCH)_01|(CH|EU)_(\w+)_BASE_01)");

        protected override void ExtractAndMapData()
        {
            List<string> skillFileNames = ExtractFileNames();
            List<Pk2Skill> skills = new List<Pk2Skill>();
            Dictionary<uint, Pk2Skill> mappedSkills = new Dictionary<uint, Pk2Skill>();

            foreach (string fileName in skillFileNames)
            {
                skills.AddRange(ExtractDataFromPK2Entry(fileName).Cast<Pk2Skill>());
            }

            foreach (Pk2Skill skill in skills)
            {
                try
                {
                    mappedSkills[skill.Id] = skill;
                }
                catch
                {
                    Logger.Log(LogLevel.PK2, "mapSkill: " + skill.ToString());
                    System.Diagnostics.Debug.WriteLine("mapSkill: " + skill.ToString());
                }

            }

            File.WriteAllLines(DirectoryGlobal.SkillData, mappedSkills.Select(x => x.Value.ToString()));

            PK2DataGlobal.Skills = mappedSkills;

            AddAutoAttackSkillsToSkillList();
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            byte[] fileBytes = entry.IndexOf("enc", StringComparison.OrdinalIgnoreCase) >= 0
                ? PK2CryptoHelper.Decrypt(PK2Main.PK2Class.GetFile(entry)) // get encrypted files if file name is e.g. skilldata_5000enc.txt
                : PK2Main.PK2Class.GetFile(entry);
            List<IPk2Types> skills = new List<IPk2Types>();

            using (StreamReader sr = new StreamReader(new MemoryStream(fileBytes)))
            {
                while (!sr.EndOfStream)
                {
                    string[] skillAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (skillAttributes.Length < 1 || skillAttributes.Length < 63 || skillAttributes[0].StartsWith("//"))
                    {
                        continue;
                    }
                    // Make string with
                    //ID,Data,Name,CastTime,Cooldown,Duration,MP,Type
                    try
                    {
                        try
                        {
                            if (PK2DataGlobal.Names.TryGetValue(skillAttributes[62], out string value))
                            {
                                skillAttributes[62] = value;
                            }
                            skills.Add(new Pk2Skill(skillAttributes[1], skillAttributes[3], skillAttributes[62], skillAttributes[13], skillAttributes[14], skillAttributes[70], skillAttributes[53], skillAttributes, skillAttributes[22]));

                            TryAddAutoAttackSkills(skillAttributes[1], skillAttributes[3]);
                        }
                        catch { }
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractSkill: " + string.Join(",", skillAttributes));
                        System.Diagnostics.Debug.WriteLine("extractSkill: " + string.Join(",", skillAttributes));
                    }
                }
            }

            return skills;
        }

        protected override List<string> ExtractFileNames()
        {
            List<string> skillFiles = new List<string>();

            // filter out names for skilldata files
            Regex regex = new Regex(@"skilldata_\d+\.txt", RegexOptions.IgnoreCase);
            Regex regexEnc = new Regex(@"skilldata_\d+enc\.txt", RegexOptions.IgnoreCase);

            HashSet<string> files = new HashSet<string>(PK2.PK2Main.PK2Class.GetFileNames().Where(fileName => regex.IsMatch(fileName)));
            HashSet<string> filesEnc = new HashSet<string>(PK2.PK2Main.PK2Class.GetFileNames().Where(fileName => regexEnc.IsMatch(fileName)));

            // Compare files and keep the bigger one
            if (files.Count < 1)
            {
                skillFiles = filesEnc.ToList();
            }
            else if (filesEnc.Count < 1)
            {
                skillFiles = files.ToList();
            }
            else
            {
                // Switched to extracting using text file signature because some private servers leave random additional (IMPORTANT) skill files without replacing old ones
                foreach (string fString in files)
                {
                    byte[] bytes = PK2Main.PK2Class.GetFile(fString) ?? new byte[0];
                    string fEncString = fString.Replace(".txt", "enc.txt");
                    byte[] bytesEnc = PK2Main.PK2Class.GetFile(filesEnc.FirstOrDefault(fEnc => fEnc == fEncString) ?? "") ?? new byte[0];

                    if (bytes.Length > bytesEnc.Length)
                    {
                        skillFiles.Add(fString);
                    }
                    else
                    {
                        skillFiles.Add(fEncString);
                    }
                }
            }

            return skillFiles;
        }

        private static void TryAddAutoAttackSkills(string id, string skill)
        {
            Match match = nameFilter.Match(skill);

            if (match.Groups.Count > 0)
            {
                Skill s = new Skill(uint.Parse(id), 1, true);

                if (match.Groups[2].Value == "PUNCH")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Other] = s;
                }
                else if (match.Groups[3].Value == "CH" && match.Groups[4].Value == "SWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Blade] = s;
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Sword] = s;
                }
                else if (match.Groups[4].Value == "SPEAR")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Glaive] = s;
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Spear] = s;
                }
                else if (match.Groups[4].Value == "BOW")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Bow] = s;
                }
                else if (match.Groups[3].Value == "EU" && match.Groups[4].Value == "SWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.SwordOnehander] = s;
                }
                else if (match.Groups[4].Value == "TSWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.SwordTwohander] = s;
                }
                else if (match.Groups[4].Value == "AXE")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Axe] = s;
                }
                else if (match.Groups[4].Value == "CROSSBOW")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Crossbow] = s;
                }
                else if (match.Groups[4].Value == "DAGGER")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Dagger] = s;
                }
                else if (match.Groups[4].Value == "STAFF")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.MageStaff] = s;
                }
                else if (match.Groups[4].Value == "WAND_WARLOCK")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Darkstaff] = s;
                }
                else if (match.Groups[4].Value == "HARP")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Harp] = s;
                }
                else if (match.Groups[4].Value == "WAND_CLERIC")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.ClericStaff] = s;
                }
            }
        }

        private static void AddAutoAttackSkillsToSkillList()
        {
            foreach (KeyValuePair<Pk2ItemType, Skill> kvp in CharInfoGlobal.AutoAttackSkills)
            {
                if (!CharInfoGlobal.Skills.Contains(kvp.Value))
                {
                    CharInfoGlobal.Skills.Add(kvp.Value);
                }
            }
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<uint, Pk2Skill> skills = new Dictionary<uint, Pk2Skill>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.SkillData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] skill = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(skill, skill.Length, 9) || skills.ContainsKey(uint.Parse(skill[0])))
                        {
                            continue;
                        }
                        skills.Add(uint.Parse(skill[0]), new Pk2Skill(skill[0], skill[1], skill[2], skill[3], skill[4], skill[5], skill[6], skill[7], skill[8]));

                        TryAddAutoAttackSkills(skill[0], skill[1]);
                    }
                }

                PK2DataGlobal.Skills = skills;

                AddAutoAttackSkillsToSkillList();
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/SkillData.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/SkillData.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
