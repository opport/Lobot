﻿using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorItems : Pk2ExtractorTemplate
    {
        protected override void ExtractAndMapData()
        {
            List<string> itemFileNames = ExtractFileNames();
            List<Pk2Item> items = new List<Pk2Item>();
            Dictionary<uint, Pk2Item> mappedItems = new Dictionary<uint, Pk2Item>();

            foreach (string fileName in itemFileNames)
            {
                items.AddRange(ExtractDataFromPK2Entry(fileName).Cast<Pk2Item>());
            }

            foreach (Pk2Item item in items)
            {
                try
                {
                    //mappedItems.Add(item.Id, item);
                    mappedItems[item.Id] = item;
                }
                catch (Exception) { }
            }

            File.WriteAllLines(DirectoryGlobal.ItemData, mappedItems.Select(x => x.Value.ToString()));

            PK2DataGlobal.Items = mappedItems;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            byte[] itemFileBytes = PK2Main.PK2Class.GetFile(entry);
            List<IPk2Types> items = new List<IPk2Types>();

            using (StreamReader sr = new StreamReader(new MemoryStream(itemFileBytes)))
            {
                while (!sr.EndOfStream)
                {
                    string[] itemAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (itemAttributes.Length < 1 || itemAttributes.Length < 58 || itemAttributes[0].StartsWith("//"))
                    {
                        continue;
                    }

                    //TODO find out what each attribute is for
                    // return line with 
                    //id, longId, name, type, level, maxStacks
                    try
                    {
                        if (PK2DataGlobal.Names.TryGetValue(itemAttributes[5], out string value))
                        {
                            itemAttributes[5] = value;
                        }

                        //concatenate type flags to define type later on
                        string typeName = int.Parse(itemAttributes[10]).ToString("X") + int.Parse(itemAttributes[11]).ToString("X") + int.Parse(itemAttributes[12]).ToString("X");
                        // prevent out of bounds exceptions for items without any gender or degree
                        string genderString = itemAttributes.Length > 58
                            ? itemAttributes[58]
                            : "2";
                        //degree number is given as (the tier number * degree), so a division by 3 is needed, <see href="http://www.cs.nott.ac.uk/~psarb2/G51MPC/slides/NumberLogic.pdf"/> 
                        int degree = itemAttributes.Length > 61
                            ? (int.Parse(itemAttributes[61]) + 3 - 1) / 3
                            : 0;

                        items.Add(
                        new Pk2Item(itemAttributes[1], itemAttributes[2], itemAttributes[5], itemAttributes[7], typeName, itemAttributes[14], itemAttributes[15], itemAttributes[19], itemAttributes[33], itemAttributes[57], genderString, degree.ToString())
                        );
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractItem: " + string.Join(",", itemAttributes));
                        System.Diagnostics.Debug.WriteLine("extractItem: " + string.Join(",", itemAttributes));
                    }
                }
            }

            return items;
        }

        protected override List<string> ExtractFileNames()
        {
            List<string> itemFiles = new List<string>();
            byte[] itemData = PK2Main.PK2Class.GetFile("itemdata.txt");

            if (PK2Main.PK2Class == null || itemData == null)
            {
                return itemFiles;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(itemData)))
            {
                while (!sr.EndOfStream)
                {
                    itemFiles.Add(sr.ReadLine());
                }
            }

            return itemFiles;
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<uint, Pk2Item> items = new Dictionary<uint, Pk2Item>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.ItemData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] item = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(item, item.Length, 12) || items.ContainsKey(uint.Parse(item[0])))
                        {
                            continue;
                        }
                        items.Add(uint.Parse(item[0]), new Pk2Item(item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], item[10], item[11]));
                    }
                }

                PK2DataGlobal.Items = items;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/Items.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/Items.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
