﻿using LobotAPI.Globals;
using LobotAPI.Scripting;
using LobotAPI.Scripting.NavMesh.Structs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;

namespace LobotAPI.PK2
{
    class Pk2ExtractorNavMesh : Pk2ExtractorTemplate
    {
        private PK2Class dataPK2;

        protected override void ExtractAndMapData()
        {
            // Find out which .nav files have already been extracted to save some time.
            List<string> navMeshFileNames = ExtractFileNames();
            IEnumerable<string> serializedFiles = Directory.EnumerateFiles(DirectoryGlobal.NavMeshFolder, "*.nvm").Select(Path.GetFileName);
            List<string> fileNames = navMeshFileNames.Except(serializedFiles).ToList();
            List<IPk2Types> NavEntries = new List<IPk2Types>();

            foreach (var fileName in fileNames)
            {
                NavEntries.AddRange(ExtractDataFromPK2Entry(fileName));
            }
        }

        private ushort GetSectorYX(ushort sectorX, ushort sectorY)
        {
            return (ushort)(sectorY << 8 ^ sectorX);
        }

        private List<NavObject> GetObjects(BinaryReader br)
        {
            List<NavObject> meshes = new List<NavObject>();
            float x, y, z;

            ushort entryCount = br.ReadUInt16();

            for (int i = 0; i < entryCount; i++)
            {
                uint id = br.ReadUInt32();
                x = br.ReadSingle() / 10;
                z = br.ReadSingle() / 10;
                y = 192 - br.ReadSingle() / 10;
                Position position = new Position(x, y, z);
                ushort collisionFlag = br.ReadUInt16();
                float yaw = br.ReadSingle(); //radiant
                ushort uniqueID = br.ReadUInt16();
                ushort scale = br.ReadUInt16();
                ushort eventZoneFlag = br.ReadUInt16();
                ushort regionID = br.ReadUInt16();

                ushort mountCount = br.ReadUInt16();
                // Skip mountPointData 
                br.BaseStream.Position += 6 * mountCount;

                meshes.Add(new NavObject(id, position, collisionFlag, yaw, uniqueID, scale, eventZoneFlag, regionID));
            }

            return meshes;
        }

        private List<NavCell> GetCells(BinaryReader reader)
        {
            List<NavCell> cells = new List<NavCell>();
            float x, y;
            Position min;
            Position max;

            uint cellCount = reader.ReadUInt32();
            uint cellExtraCount = reader.ReadUInt32();
            for (int cellIndex = 0; cellIndex < cellCount; cellIndex++)
            {
                x = reader.ReadSingle() / 10;
                y = 192 - reader.ReadSingle() / 10;
                min = new Position(x, y);
                x = reader.ReadSingle() / 10;
                y = 192 - reader.ReadSingle() / 10;
                max = new Position(x, y);
                List<ushort> navObjectIndex = new List<ushort>();
                List<ushort> navCellIndex = new List<ushort>();
                List<ushort> navCellLinkIndex = new List<ushort>();

                byte entryCount = reader.ReadByte();
                for (int i = 0; i < entryCount; i++)
                {
                    navObjectIndex.Add(reader.ReadUInt16());
                }
                cells.Add(new NavCell(min, max, navObjectIndex, navCellIndex, navCellLinkIndex));
            }

            return cells;
        }

        private List<NavBorder> GetBorders(BinaryReader reader, NavMeshSegment navSegment)
        {
            List<NavBorder> navBorders = new List<NavBorder>();
            float x, y;
            Point min;
            Point max;
            byte lineFlag;
            byte lineSource;
            byte lineDestination;
            ushort cellSource;
            ushort cellDestination;
            ushort regionSource;
            ushort regionDestination;


            uint regionLinkCount = reader.ReadUInt32();
            for (int linkIndex = 0; linkIndex < regionLinkCount; linkIndex++)
            {
                x = reader.ReadSingle() / 10;
                y = 192 - reader.ReadSingle() / 10;
                min = new Point(x, y);
                x = reader.ReadSingle() / 10;
                y = 192 - reader.ReadSingle() / 10;
                max = new Point(x, y);

                lineFlag = reader.ReadByte();
                lineSource = reader.ReadByte();
                lineDestination = reader.ReadByte();

                cellSource = reader.ReadUInt16();
                cellDestination = reader.ReadUInt16();
                regionSource = reader.ReadUInt16();
                regionDestination = reader.ReadUInt16();

                if (GetSectorYX(navSegment.SectorX, navSegment.SectorY) == regionSource)
                {
                    navSegment.NavigationCells[cellSource].NavBorderIndex.Add((ushort)linkIndex);
                }
                else
                {
                    navSegment.NavigationCells[cellDestination].NavBorderIndex.Add((ushort)linkIndex);
                }

                navBorders.Add(new NavBorder(min, max, lineFlag, lineSource, lineDestination, cellSource, cellDestination, regionSource, regionDestination));
            }

            return navBorders;
        }

        private List<NavCellLink> GetCellLinks(BinaryReader reader, NavMeshSegment navSegment)
        {
            List<NavCellLink> cellLinks = new List<NavCellLink>();
            float x, y;
            Point min;
            Point max;
            byte lineFlag;
            byte lineSource;
            byte lineDestination;
            ushort cellSource;
            ushort cellDestination;

            uint cellLinkCount = reader.ReadUInt32();
            for (int linkIndex = 0; linkIndex < cellLinkCount; linkIndex++)
            {
                x = reader.ReadUInt32() / 10;
                y = 192 - reader.ReadSingle() / 10;
                min = new Point(x, y);
                x = reader.ReadUInt32() / 10;
                y = 192 - reader.ReadSingle() / 10;
                max = new Point(x, y);

                lineFlag = reader.ReadByte();
                lineSource = reader.ReadByte();
                lineDestination = reader.ReadByte();

                cellSource = reader.ReadUInt16();
                cellDestination = reader.ReadUInt16();

                cellLinks.Add(new NavCellLink(min, max, lineFlag, lineSource, lineDestination, cellSource, cellDestination));

                if (cellSource != ushort.MaxValue)
                {
                    navSegment.NavigationCells[cellSource].NavCellLinkIndex.Add((ushort)linkIndex);
                }
                if (cellDestination != ushort.MaxValue)
                {
                    navSegment.NavigationCells[cellDestination].NavCellLinkIndex.Add((ushort)linkIndex);
                }
            }
            return cellLinks;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            byte[] fileBytes = dataPK2.GetFile(entry);
            NavMeshSegment navSegment = new NavMeshSegment(entry);

            using (BinaryReader reader = new BinaryReader(new MemoryStream(fileBytes)))
            {
                float[] heightMap = new float[9409];

                try
                {
                    char[] header = reader.ReadChars(12); //JMXVNVM 1000

                    navSegment.NavigationObjects = GetObjects(reader);
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tNavEntries");
                    navSegment.NavigationCells = GetCells(reader);
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tNavCells");
                    navSegment.NavigationBorders = GetBorders(reader, navSegment);
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tRegionLinks");
                    navSegment.NavigationCellLinks = GetCellLinks(reader, navSegment);
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tCellLinks");

                    //skip texturemaps
                    reader.BaseStream.Position += 0x12000;
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tTextureMap");

                    //continue until end of stream
                    long remainingFloats = (reader.BaseStream.Length - reader.BaseStream.Position - 36 - 36 * 4) / 4; // last3 and last4*4
                    long endOfHeightMap = Math.Min(9409, remainingFloats);
                    for (int i = 0; i < endOfHeightMap; i++)
                    {
                        heightMap[i] = reader.ReadSingle();
                    }

                    navSegment.HeightMap = heightMap;
                    //System.Diagnostics.Debug.WriteLine(reader.BaseStream.Position + "/" + reader.BaseStream.Length + "\tHeightMap");

                    Conversion.WriteToBinaryFile(DirectoryGlobal.NavMeshFolder + entry, navSegment, false);
                }
                catch (EndOfStreamException e)
                {
                    Logger.Log(LogLevel.NETWORK, String.Format("extractNav: {0} {1}", entry, e.Message));
                    System.Diagnostics.Debug.WriteLine(String.Format("extractNav: {0} {1}", entry, e.Message));
                }
                //catch (serialization e)
                //{
                //    Logger.Log(LogLevel.NETWORK, String.Format("extractNav: {0} {1}", entry, e.Message));
                //    System.Diagnostics.Debug.WriteLine(String.Format("extractNav: {0} {1}", entry, e.Message));
                //}
                catch (Exception e)
                {
                    Logger.Log(LogLevel.PK2, String.Format("extractNav: {0} {1}", entry, e.Message));
                    System.Diagnostics.Debug.WriteLine(String.Format("extractNav: {0} {1}", entry, e.Message));
                }

                return new List<IPk2Types> { navSegment };
            }
        }

        protected override List<string> ExtractFileNames()
        {
            return dataPK2.GetFileNames().FindAll(file => file.EndsWith(".nvm"));
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                // use Pk2Class for data.pk2
                dataPK2 = new PK2Class(PK2DataGlobal.PK2DataFilePath);
                ExtractAndMapData();
                dataPK2 = null;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"NavEntry\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"NavEntry\" not found. Attempting to extract PK2-Info.");
            }
        }
    }
}
