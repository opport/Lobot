﻿using LobotAPI.Globals;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorShops : Pk2ExtractorTemplate
    {
        protected override void ExtractAndMapData()
        {
            Dictionary<string, Pk2ShopData> shopData = ExtractShopDataFromPK2Entry("shopdata.txt");
            Dictionary<int, Shop> shops = new Dictionary<int, Shop>();

            using (StreamWriter file = new StreamWriter((DirectoryGlobal.ShopData)))
            {
                foreach (KeyValuePair<string, Pk2ShopData> entry in shopData)
                {
                    int tabNumber = 0;
                    foreach (Pk2ShopTabData tab in entry.Value.ShopTabs)
                    {
                        int slotNumber = 0;
                        foreach (PK2ShopItemRef item in tab.ShopTabItems) // empty tabs not added
                        {
                            try
                            {
                                file.WriteLine(entry.Value.ShopNPCID + "|" + item.ItemID + "|" + tabNumber + "|" + item.Slot + "|" + item.Price);

                                if (!shops.TryGetValue(entry.Value.ShopNPCID, out Shop resultShop))
                                {
                                    shops.Add(entry.Value.ShopNPCID, new Shop((uint)entry.Value.ShopNPCID));
                                }
                                shops[entry.Value.ShopNPCID].ShopItemEntries[item.ItemID] = new ShopItemEntry(item.ItemID, (byte)tabNumber, (byte)slotNumber, item.Price);
                            }
                            catch (Exception)
                            {
                                Logger.Log(LogLevel.PK2, "extractShopData: extract and map  " + item.ToString());
                                System.Diagnostics.Debug.WriteLine("extractShopData: extract and map   " + item.ToString());
                            }
                            slotNumber++;
                        }
                        tabNumber++;
                    }
                }
            }

            //lambda loop returns bad string for entries
            //File.WriteAllLines(Global.ShopData, shops.Select(x => x.Key + "," + x.Value.ToString()));

            PK2DataGlobal.Shops = shops;
        }

        private static Dictionary<string, Pk2ShopData> ExtractShopDataFromPK2Entry(string entry)
        {
            byte[] shopFileEntry = PK2Main.PK2Class.GetFile(entry);
            Dictionary<string, Pk2ShopData> shopData = new Dictionary<string, Pk2ShopData>();
            Dictionary<string, Pk2ShopTabData> shopTabData = ExtractShopTabsFromPK2Entry("shoptabdata.txt");

            using (StreamReader sr = new StreamReader(new MemoryStream(shopFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] shopDataAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);
                    List<Pk2ShopTabData> currentTabs = new List<Pk2ShopTabData>();

                    if (shopDataAttributes.Length < 1 || shopDataAttributes[0].StartsWith("//"))
                    {
                        continue;
                    }

                    try
                    {
                        //add tabs from possible slots
                        for (int i = 6; i < 16; i++)
                        {
                            if (shopTabData.TryGetValue(shopDataAttributes[i], out Pk2ShopTabData result)) // guard for single-civilization-servers (ch-only -> no shop tabs for euro items)
                            {
                                if (shopDataAttributes[i] != "0")
                                {
                                    currentTabs.Add(result);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }

                        shopData.Add(shopDataAttributes[1], new Pk2ShopData(shopDataAttributes[1], shopDataAttributes[5], currentTabs));
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractShopData: " + string.Join(",", shopDataAttributes));
                        System.Diagnostics.Debug.WriteLine("extractShopData: " + string.Join(",", shopDataAttributes));
                    }
                }
            }

            return shopData;
        }

        private static Dictionary<string, Pk2ShopTabData> ExtractShopTabsFromPK2Entry(string entry)
        {
            byte[] shopFileEntry = PK2Main.PK2Class.GetFile(entry);
            Dictionary<string, Pk2ShopTabData> tabs = new Dictionary<string, Pk2ShopTabData>();
            Dictionary<string, List<PK2ShopItemRef>> shopItemData = ExtractShopItemsFromPK2Entry();

            using (StreamReader sr = new StreamReader(new MemoryStream(shopFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] shopTabAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);
                    // shoptabdataID,		shoptabdataLongID,	shoptabdataID, 	shoptabdataName

                    try
                    {
                        if (shopItemData.TryGetValue(shopTabAttributes[2], out List<PK2ShopItemRef> value))
                        {
                            tabs.Add(shopTabAttributes[1], new Pk2ShopTabData(shopTabAttributes[1], shopTabAttributes[2], shopTabAttributes[3], shopTabAttributes[4], value));
                        }
                        else
                        {
                            tabs.Add(shopTabAttributes[1], new Pk2ShopTabData(shopTabAttributes[1], shopTabAttributes[2], shopTabAttributes[3], shopTabAttributes[4], new List<PK2ShopItemRef>()));
                        }

                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractShopTabs: " + string.Join(",", shopTabAttributes));
                        System.Diagnostics.Debug.WriteLine("extractShopTabs: " + string.Join(",", shopTabAttributes));
                    }
                }
            }

            return tabs;
        }

        // Map TabName to list of items in tab
        private static Dictionary<string, List<PK2ShopItemRef>> ExtractShopItemsFromPK2Entry()
        {
            byte[] shopFileEntry = PK2Main.PK2Class.GetFile("refpricepolicyofitem.txt");
            Dictionary<string, string> itemNames = new Dictionary<string, string>();
            List<PK2ShopItemRef> itemData = new List<PK2ShopItemRef>();

            using (StreamReader sr = new StreamReader(new MemoryStream(shopFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] shopItemAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);
                    // Package_item_longID, price

                    try
                    {
                        //Add new key and itemlist or add new item to existing key
                        if (!itemNames.ContainsKey(shopItemAttributes[2]))
                        {
                            itemNames.Add(shopItemAttributes[2], shopItemAttributes[5]);
                        }
                        else
                        {
                            itemNames[shopItemAttributes[2]] = shopItemAttributes[5];
                        }
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractShopItem: " + string.Join(",", shopItemAttributes));
                        System.Diagnostics.Debug.WriteLine("extractShopItem: " + string.Join(",", shopItemAttributes));
                    }
                }
            }

            //Add tabnames and itemIDs for the next steps
            itemData = ExtractTabNamesAndSlotIndexes(itemNames);
            FindItemIDs(itemData);
            return MapTabstoItems(itemData); // Key(ID), Value(Item))
        }

        private static List<PK2ShopItemRef> ExtractTabNamesAndSlotIndexes(Dictionary<string, string> items)
        {
            byte[] shopFileEntry = PK2Main.PK2Class.GetFile("refshopgoods.txt");
            List<PK2ShopItemRef> result = new List<PK2ShopItemRef>();

            using (StreamReader sr = new StreamReader(new MemoryStream(shopFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] refTabAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    try
                    {
                        result.Add(new PK2ShopItemRef(refTabAttributes[3], refTabAttributes[2], items[refTabAttributes[3]], int.Parse(refTabAttributes[4])));
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractShopItem: refTabAttributes " + string.Join(",", refTabAttributes));
                        System.Diagnostics.Debug.WriteLine("extractShopItem: refTabAttributes" + string.Join(",", refTabAttributes));
                    }

                }
            }

            return result;
        }

        private static void FindItemIDs(List<PK2ShopItemRef> items)
        {
            byte[] shopFileEntry = PK2Main.PK2Class.GetFile("refscrapofpackageitem.txt");
            Dictionary<string, string> packNameToRefName = new Dictionary<string, string>();

            using (StreamReader sr = new StreamReader(new MemoryStream(shopFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] itemEntry = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);
                    if (itemEntry.Length > 3)
                    {
                        packNameToRefName.Add(itemEntry[2], itemEntry[3]);
                    }
                }
            }

            foreach (PK2ShopItemRef item in items)
            {
                string actualItemRefName = packNameToRefName[item.LongID];
                item.ItemID = PK2DataGlobal.Items.Values.FirstOrDefault(i => i.LongId == actualItemRefName).Id;
            }
        }

        private static Dictionary<string, List<PK2ShopItemRef>> MapTabstoItems(List<PK2ShopItemRef> items)
        {
            Dictionary<string, List<PK2ShopItemRef>> result = new Dictionary<string, List<PK2ShopItemRef>>();

            foreach (PK2ShopItemRef item in items)
            {
                try
                {
                    //Add new key and itemlist or add new item to existing key
                    if (!result.ContainsKey(item.TabName))
                    {
                        result.Add(item.TabName, new List<PK2ShopItemRef> { item });
                    }
                    else
                    {
                        // Insert in slot order
                        if (item.Slot < result[item.TabName].Count) // 
                        {
                            result[item.TabName].Insert(item.Slot, item);
                        }
                        else //
                        {
                            result[item.TabName].Add(item);
                        }
                    }
                }
                catch (Exception)
                {
                    Logger.Log(LogLevel.PK2, "extractShopItem: " + item.ToString());
                    System.Diagnostics.Debug.WriteLine("extractShopItem: " + item.ToString());
                }
            }

            return result;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            throw new NotImplementedException();
        }

        protected override List<string> ExtractFileNames()
        {
            throw new NotImplementedException();
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<int, Shop> shops = new Dictionary<int, Shop>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.ShopData, FileMode.Open)))
                {
                    List<Pk2ShopTabData> tabs = new List<Pk2ShopTabData>();

                    while (!sr.EndOfStream)
                    {
                        string[] shopStr = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);
                        int[] shop = Array.ConvertAll<string, int>(shopStr, s => Int32.Parse(s));

                        if (!IsValid(shopStr, shopStr.Length, 4))
                        {
                            continue;
                        }

                        if (!shops.TryGetValue(shop[0], out Shop resultShop))
                        {
                            shops.Add(shop[0], new Shop((uint)shop[0]));
                        }
                        // shop id - tab - slot - itemID
                        shops[shop[0]].ShopItemEntries[(uint)shop[1]] = new ShopItemEntry((uint)shop[1], (byte)shop[2], (byte)shop[3], shop[4]);
                    }
                }

                PK2DataGlobal.Shops = shops;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/Shops.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/Shops.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
