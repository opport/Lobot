﻿using System;
using System.Collections.Generic;

namespace LobotAPI.PK2
{
    public abstract class Pk2ExtractorTemplate
    {
        protected static readonly char[] Separator = new char[] { '\t' };

        protected static bool IsValid(string[] str, int args = 2, int minArgs = 2)
        {
            return str.Length >= minArgs && !String.IsNullOrEmpty(str[0]) && !String.IsNullOrWhiteSpace(str[0]) && args >= minArgs && !str[0].StartsWith("//");
        }

        protected abstract List<IPk2Types> ExtractDataFromPK2Entry(string entry);
        protected abstract List<string> ExtractFileNames();
        protected abstract void ExtractAndMapData();

        /// <summary>
        /// Try to load the pk2 information from previously extracted *.txt file in botfolder/pk2
        /// before extracting directly from the pk2 file.
        /// </summary>
        protected abstract void LoadFromTxtFile();

        /// <summary>
        /// Template method summarizing the extraction process to get pk2 data.
        /// </summary>
        public void ExtractAll()
        {
            try
            {
                LoadFromTxtFile();
            }
            catch (Exception)
            {
                ExtractAndMapData();
            }
        }

    }
}
