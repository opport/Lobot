﻿using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorTeleporters : Pk2ExtractorTemplate
    {
        protected override void ExtractAndMapData()
        {
            List<string> teleporterFileNames = ExtractFileNames();
            List<Pk2Teleporter> teleporters = new List<Pk2Teleporter>();
            Dictionary<uint, Pk2Teleporter> mappedTeleporters = new Dictionary<uint, Pk2Teleporter>();

            foreach (string fileName in teleporterFileNames)
            {
                teleporters.AddRange(ExtractDataFromPK2Entry(fileName).Cast<Pk2Teleporter>());
            }

            foreach (Pk2Teleporter teleporter in teleporters)
            {
                mappedTeleporters.Add(teleporter.Id, teleporter);
            }

            File.WriteAllLines(DirectoryGlobal.TeleporterData, mappedTeleporters.Select(x => x.Value.ToString()));

            PK2DataGlobal.Teleporters = mappedTeleporters;
        }


        private Dictionary<uint, List<uint>> ExtractTeleporterLinks(string entry)
        {
            byte[] fileEntry = PK2Main.PK2Class.GetFile(entry);
            Dictionary<uint, List<uint>> teleporterLinks = new Dictionary<uint, List<uint>>();

            using (StreamReader sr = new StreamReader(new MemoryStream(fileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] linkAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (linkAttributes.Length < 3)
                    {
                        continue;
                    }

                    // Make string with
                    // ID   longID  nameID  level   hp
                    try
                    {
                        if (teleporterLinks.TryGetValue(uint.Parse(linkAttributes[1]), out List<uint> values))
                        {
                            values.Add(uint.Parse(linkAttributes[2]));
                        }
                        else
                        {
                            teleporterLinks.Add(uint.Parse(linkAttributes[1]), new List<uint> { uint.Parse(linkAttributes[2]) });
                        }
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractTeleporterLinks: " + string.Join(",", linkAttributes));
                        System.Diagnostics.Debug.WriteLine("extractTeleporterLinks: " + string.Join(",", linkAttributes));
                    }
                }
            }

            return teleporterLinks;
        }

        private List<Pk2TeleporterData> ExtractTeleporterData(string entry)
        {
            byte[] fileEntry = PK2Main.PK2Class.GetFile(entry);
            List<Pk2TeleporterData> teleporterData = new List<Pk2TeleporterData>();
            Dictionary<uint, List<uint>> teleporterLinks = ExtractTeleporterLinks("teleportlink.txt");

            using (StreamReader sr = new StreamReader(new MemoryStream(fileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] teleporterDataObject = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (teleporterDataObject.Length < 5)
                    {
                        continue;
                    }

                    // Make string with
                    // ID   longID  nameID  level   hp
                    try
                    {
                        if (PK2DataGlobal.Names.TryGetValue(teleporterDataObject[4], out string value))
                        {
                            teleporterDataObject[2] = value;
                        }

                        if (teleporterLinks.TryGetValue(uint.Parse(teleporterDataObject[1]), out List<uint> values))
                        {
                            teleporterData.Add(new Pk2TeleporterData(teleporterDataObject[1], teleporterDataObject[3], teleporterDataObject[2], values));
                        }
                        else
                        {
                            teleporterData.Add(new Pk2TeleporterData(teleporterDataObject[1], teleporterDataObject[3], teleporterDataObject[2], new List<uint>()));
                        }

                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractTeleporterData: " + string.Join(",", teleporterDataObject));
                        System.Diagnostics.Debug.WriteLine("extractTeleporterData: " + string.Join(",", teleporterDataObject));
                    }
                }
            }

            return teleporterData;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            byte[] fileNames = PK2Main.PK2Class.GetFile(entry);
            List<Pk2TeleporterData> teleporterData = ExtractTeleporterData("teleportdata.txt");
            List<IPk2Types> teleporters = new List<IPk2Types>();

            using (StreamReader sr = new StreamReader(new MemoryStream(fileNames)))
            {
                while (!sr.EndOfStream)
                {
                    string[] teleporterAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (teleporterAttributes.Length < 1 || teleporterAttributes[0].StartsWith("//"))
                    {
                        continue;
                    }
                    // Make string with
                    // ID   longID  nameID  level   hp
                    try
                    {
                        if (PK2DataGlobal.Names.TryGetValue(teleporterAttributes[5], out string value))
                        {
                            teleporterAttributes[5] = value;
                        }

                        if (teleporterData.TryFind(item => item.LongId == teleporterAttributes[1], out Pk2TeleporterData td))
                        {
                            teleporters.Add(new Pk2Teleporter(teleporterAttributes[1], teleporterAttributes[2], teleporterAttributes[5], td));
                        }
                        else
                        {
                            teleporters.Add(
                                new Pk2Teleporter(teleporterAttributes[1],
                                teleporterAttributes[2], teleporterAttributes[5],
                                new Pk2TeleporterData("", "", "", new List<uint>())));
                        }

                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractTeleporters: " + string.Join(",", teleporterAttributes));
                        System.Diagnostics.Debug.WriteLine("extractTeleporters: " + string.Join(",", teleporterAttributes));
                    }
                }
            }

            return teleporters;
        }

        protected override List<string> ExtractFileNames()
        {
            return new List<string> { "teleportbuilding.txt" };
        }


        private List<uint> LoadTeleporterLink(string[] teleporterLinkInfo)
        {
            List<uint> teleporterLink = new List<uint>();

            if (teleporterLinkInfo.Length < 1)
            {
                return new List<uint>();
            }
            else
            {
                foreach (string s in teleporterLinkInfo)
                {
                    teleporterLink.Add(uint.Parse(s));
                }
                return teleporterLink;
            }
        }

        private Pk2TeleporterData LoadTeleporterData(string[] teleporterDataInfo)
        {
            //Add possible teleporterLinks
            return (teleporterDataInfo.Length == 4)
                ? new Pk2TeleporterData(teleporterDataInfo[0],
                    teleporterDataInfo[1],
                    teleporterDataInfo[2],
                    LoadTeleporterLink(teleporterDataInfo[3].Split(new char[] { ',' }, StringSplitOptions.None))
                    )
                : new Pk2TeleporterData("", "", "", new List<uint>());
        }

        private Pk2Teleporter LoadTeleporter(string[] teleporterInfo)
        {
            return (teleporterInfo.Length == 4)
                ? new Pk2Teleporter(teleporterInfo[0],
                        teleporterInfo[1],
                        teleporterInfo[2],
                        LoadTeleporterData(teleporterInfo[3].Split(new char[] { ',' }, 4, StringSplitOptions.None))
                        )
                : (teleporterInfo.Length == 3)
                    ? new Pk2Teleporter(teleporterInfo[0], teleporterInfo[1], teleporterInfo[2], new Pk2TeleporterData("", "", "", new List<uint>()))
                    : default(Pk2Teleporter);
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<uint, Pk2Teleporter> teleporters = new Dictionary<uint, Pk2Teleporter>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.TeleporterData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] portal = sr.ReadLine().Split(new char[] { '|' }, 4, StringSplitOptions.None);
                        //Check for possible comment
                        if (!IsValid(portal, portal.Length, 3) || teleporters.ContainsKey(uint.Parse(portal[0])))
                        {
                            continue;
                        }

                        //Load teleporter
                        Pk2Teleporter teleporter = LoadTeleporter(portal);

                        //TODO add list of teleport links in last parameter List<>
                        if (teleporter.Equals(default(Pk2Teleporter)))
                        {
                            continue;
                        }

                        teleporters.Add(teleporter.Id, teleporter);
                    }
                }

                PK2DataGlobal.Teleporters = teleporters;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/Teleporters.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/Teleporters.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
