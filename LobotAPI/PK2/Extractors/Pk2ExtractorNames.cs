﻿using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorNames : Pk2ExtractorTemplate
    {
        protected override void ExtractAndMapData()
        {
            List<string> FileNames = ExtractFileNames();
            Dictionary<string, string> names = new Dictionary<string, string>();

            foreach (string fileName in FileNames)
            {
                Extensions.AddRange(names, ExtractNamesFromPKEntry(fileName));
            }

            File.WriteAllLines(DirectoryGlobal.DataFolder + "Names.txt", names.Select(x => x.Key + "|" + String.Join(",", x.Value)));

            PK2DataGlobal.Names = names;
        }

        private static Dictionary<string, string> ExtractNamesFromPKEntry(string entry)
        {
            byte[] nameFileEntry = PK2Main.PK2Class.GetFile(entry);
            Dictionary<string, string> names = new Dictionary<string, string>();

            if (PK2Main.PK2Class == null || nameFileEntry == null)
            {
                return names;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(nameFileEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] nameAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (nameAttributes.Length < 1 || nameAttributes[0].StartsWith("//") || nameAttributes[0].Equals("0"))
                    {
                        continue;
                    }

                    try
                    {
                        //if (nameAttributes.Length > 9 && nameAttributes[9] != "0")
                        //{
                        //    names[nameAttributes[1]] = nameAttributes[9];
                        //}
                        //else
                        if (nameAttributes.Length > 8)
                        {
                            names[nameAttributes[1]] = nameAttributes[8];
                        }
                        else if (nameAttributes.Length > 2)
                        {
                            names[nameAttributes[1]] = nameAttributes[2];
                        }
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractName: " + string.Join(",", nameAttributes));
                        System.Diagnostics.Debug.WriteLine("extractName: " + string.Join(",", nameAttributes));
                    }
                }
            }

            return names;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            throw new NotImplementedException();
        }

        protected override List<string> ExtractFileNames()
        {
            List<string> nameFiles = new List<string>();
            byte[] itemData = PK2Main.PK2Class.GetFile("textdataname.txt");

            if (PK2Main.PK2Class == null || itemData == null)
            {
                return nameFiles;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(itemData)))
            {
                while (!sr.EndOfStream)
                {
                    nameFiles.Add(sr.ReadLine());
                }
            }

            return nameFiles;
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<string, string> names = new Dictionary<string, string>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.NameData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] name = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(name, name.Length) || names.ContainsKey(name[0]))
                        {
                            continue;
                        }
                        names.Add(name[0], name[1]);
                    }
                }

                PK2DataGlobal.Names = names;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/Names.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/Names.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
