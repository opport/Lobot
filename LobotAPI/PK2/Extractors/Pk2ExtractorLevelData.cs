﻿using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorLevelData : Pk2ExtractorTemplate
    {
        protected override void ExtractAndMapData()
        {
            byte[] levelEntry = PK2Main.PK2Class.GetFile("leveldata.txt");
            Dictionary<string, string> levels = new Dictionary<string, string>();

            using (StreamReader sr = new StreamReader(new MemoryStream(levelEntry)))
            {
                while (!sr.EndOfStream)
                {
                    string[] levelInfo = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (levelInfo.Length > 1 && int.TryParse(levelInfo[0], out int valueType))
                    {
                        levels.Add(levelInfo[0], levelInfo[1]);
                    }
                }
            }

            File.WriteAllLines(DirectoryGlobal.LevelData, levels.Select(x => x.Key + "|" + x.Value.ToString()));

            PK2DataGlobal.LevelData = levels.ToDictionary(kvp => int.Parse(kvp.Key), kvp => ulong.Parse(kvp.Value));
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            throw new NotImplementedException();
        }

        protected override List<string> ExtractFileNames()
        {
            throw new NotImplementedException();
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<int, ulong> levels = new Dictionary<int, ulong>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.LevelData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] level = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(level, level.Length) || levels.ContainsKey(int.Parse(level[0])))
                        {
                            continue;
                        }

                        levels.Add(int.Parse(level[0]), ulong.Parse(level[1]));
                    }
                }

                PK2DataGlobal.LevelData = levels;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/Levels.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/Levels.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
