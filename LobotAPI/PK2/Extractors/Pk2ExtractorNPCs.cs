﻿using LobotAPI.Globals;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LobotAPI.PK2
{
    class Pk2ExtractorNPCs : Pk2ExtractorTemplate
    {

        protected override void ExtractAndMapData()
        {
            List<string> npcFileNames = ExtractFileNames();
            List<Pk2NPC> npcs = new List<Pk2NPC>();
            Dictionary<uint, Pk2NPC> mappedNPCs = new Dictionary<uint, Pk2NPC>();

            foreach (string fileName in npcFileNames)
            {
                npcs.AddRange(ExtractDataFromPK2Entry(fileName).Cast<Pk2NPC>());
            }

            foreach (Pk2NPC npc in npcs)
            {
                if (mappedNPCs.TryGetValue(npc.Id, out Pk2NPC value))
                {
                    System.Diagnostics.Debug.WriteLine($"duplicate npc at {npc}");
                }
                else
                {
                    mappedNPCs.Add(npc.Id, npc);
                }
            }

            File.WriteAllLines(DirectoryGlobal.NPCData, mappedNPCs.Select(x => x.Value.ToString()));

            PK2DataGlobal.NPCs = mappedNPCs;
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            byte[] fileBytes = PK2Main.PK2Class.GetFile(entry);
            List<IPk2Types> monsters = new List<IPk2Types>();

            using (StreamReader sr = new StreamReader(new MemoryStream(fileBytes)))
            {
                while (!sr.EndOfStream)
                {
                    string[] NPCAttributes = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (NPCAttributes.Length < 1 || NPCAttributes[0].StartsWith("//"))
                    {
                        continue;
                    }

                    // Make string with
                    // ID   longID  nameID  level   hp
                    try
                    {
                        if (PK2DataGlobal.Names.TryGetValue(NPCAttributes[5], out string value))
                        {
                            NPCAttributes[5] = value;
                        }

                        //concatenate type flags to define type later on
                        string typeName = NPCAttributes[10] + NPCAttributes[11] + NPCAttributes[12] + NPCAttributes[14] + NPCAttributes[15];

                        monsters.Add(new Pk2NPC(NPCAttributes[1], NPCAttributes[2], NPCAttributes[5], NPCAttributes[57], NPCAttributes[59], typeName));
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extractNPC: " + string.Join(",", NPCAttributes));
                        System.Diagnostics.Debug.WriteLine("extractNPC: " + string.Join(",", NPCAttributes));
                    }
                }
            }

            return monsters;
        }

        protected override List<string> ExtractFileNames()
        {
            List<string> monsterFiles = new List<string>();
            byte[] itemData = PK2Main.PK2Class.GetFile("characterdata.txt");

            if (PK2Main.PK2Class == null || itemData == null)
            {
                return monsterFiles;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(itemData)))
            {
                while (!sr.EndOfStream)
                {
                    monsterFiles.Add(sr.ReadLine());
                }
            }

            return monsterFiles;
        }

        protected override void LoadFromTxtFile()
        {
            try
            {
                Dictionary<uint, Pk2NPC> npcs = new Dictionary<uint, Pk2NPC>();

                using (StreamReader sr = new StreamReader(new FileStream(DirectoryGlobal.NPCData, FileMode.Open)))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] npc = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(npc, npc.Length, 6) || npcs.ContainsKey(uint.Parse(npc[0])))
                        {
                            continue;
                        }
                        npcs.Add(uint.Parse(npc[0]), new Pk2NPC(npc[0], npc[1], npc[2], npc[3], npc[4], npc[5]));
                    }
                }

                PK2DataGlobal.NPCs = npcs;
            }
            catch (Exception)
            {
                Logger.Log(LogLevel.PK2, "\"Data/NPCs.txt\" not found. Attempting to extract PK2-Info.");
                System.Diagnostics.Debug.WriteLine("\"Data/NPCs.txt\" not found. Attempting to extract PK2-Info.");
                ExtractAndMapData();
            }
        }
    }
}
