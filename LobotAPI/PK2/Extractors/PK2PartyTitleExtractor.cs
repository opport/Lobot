﻿using LobotAPI.Globals.Settings;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.IO;

namespace LobotAPI.PK2
{
    public class PK2PartyTitleExtractor : Pk2ExtractorTemplate
    {
        private bool IsHuntingText(string text)
        {
            return text.Equals("UIIT_MSG_PARTYMATCH_RECORD_DEFAULT1");
        }

        private bool IsQuestText(string text)
        {
            return text.Equals("UIIT_MSG_PARTYMATCH_RECORD_DEFAULT3");
        }

        private bool IsThiefText(string text)
        {
            return text.Equals("UIIT_MSG_PARTYMATCH_RECORD_DEFAULT4");
        }

        private bool IsTraderHunterText(string text)
        {
            return text.Equals("UIIT_MSG_PARTYMATCH_RECORD_DEFAULT2");
        }

        private void SetText(PartyObjective objective, string[] text, ref int titlesSet)
        {
            int index = 8;

            //find entry with meaningful text
            while (text[index] == "0" && index < text.Length - 1)
            {
                index++;
            }

            PartySettings.DefaultPartyentries[objective] = text[index];
            titlesSet++;
        }

        protected override void ExtractAndMapData()
        {
            byte[] textUISystem = PK2Main.PK2Class.GetFile("textuisystem.txt");
            int titlesSet = 0;

            using (StreamReader sr = new StreamReader(new MemoryStream(textUISystem)))
            {
                while (!sr.EndOfStream)
                {
                    string[] uiTextEntry = sr.ReadLine().Split(Separator, StringSplitOptions.RemoveEmptyEntries);

                    if (!IsValid(uiTextEntry, uiTextEntry.Length, 8))
                    {
                        continue;
                    }

                    if (titlesSet == 4) // all found and set
                    {
                        break;
                    }

                    try
                    {
                        //Add new key and itemlist or add new item to existing key
                        if (IsHuntingText(uiTextEntry[1]))
                        {
                            SetText(PartyObjective.Hunting, uiTextEntry, ref titlesSet);
                        }
                        else if (IsQuestText(uiTextEntry[1]))
                        {
                            SetText(PartyObjective.Quest, uiTextEntry, ref titlesSet);
                        }
                        else if (IsTraderHunterText(uiTextEntry[1]))
                        {
                            SetText(PartyObjective.TradeHunter, uiTextEntry, ref titlesSet);
                        }
                        else if (IsThiefText(uiTextEntry[1]))
                        {
                            SetText(PartyObjective.Thief, uiTextEntry, ref titlesSet);
                        }
                    }
                    catch (Exception)
                    {
                        Logger.Log(LogLevel.PK2, "extract default party title: " + string.Join(",", uiTextEntry));
                        System.Diagnostics.Debug.WriteLine("extract default party title: " + string.Join(",", uiTextEntry));
                    }
                }
            }
        }

        protected override List<IPk2Types> ExtractDataFromPK2Entry(string entry)
        {
            throw new NotImplementedException();
        }

        protected override List<string> ExtractFileNames()
        {
            throw new NotImplementedException();
        }

        protected override void LoadFromTxtFile()
        {
            ExtractAndMapData();
        }
    }
}
