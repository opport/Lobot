﻿using LobotAPI.Globals;
using LobotAPI.Structs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LobotAPI.PK2
{
    public interface IPk2Types
    {
        IPk2Types ToType { get; }
    }

    public static class Pk2ItemTypeExtensions
    {
        /// <summary>
        /// Returns the character's attack range based on weapon
        /// </summary>
        /// <param name="pk2ItemType"></param>
        /// <returns></returns>
        public static float GetAttackRange(this Pk2ItemType pk2ItemType)
        {
            switch (pk2ItemType)
            {
                case Pk2ItemType.Sword:
                case Pk2ItemType.Blade:
                    return 0.6f;
                case Pk2ItemType.Spear:
                case Pk2ItemType.Glaive:
                    return 1.8f;
                case Pk2ItemType.Bow:
                    return 18f;
                case Pk2ItemType.SwordOnehander:
                    return 0.6f;
                case Pk2ItemType.SwordTwohander:
                    return 1.8f;
                case Pk2ItemType.Axe:
                    return 0.6f;
                case Pk2ItemType.Darkstaff:
                    return 6f;
                case Pk2ItemType.MageStaff:
                    return 6f;
                case Pk2ItemType.Crossbow:
                    return 18f;
                case Pk2ItemType.Dagger:
                    return 3f;
                case Pk2ItemType.Harp:
                    return 6f;
                case Pk2ItemType.ClericStaff:
                    return 6f;
                default:
                    return 0.6f;
            }
        }

        public static EquipmentSlot GetRequiredWeaponSlot(this Pk2ItemType pk2ItemType)
        {
            switch (pk2ItemType)
            {
                case Pk2ItemType.ShieldCH:
                case Pk2ItemType.ShieldEU:
                    return EquipmentSlot.Secondary;
                default:
                    return EquipmentSlot.Primary;
            }
        }

        public static bool IsWeaponWithShield(this Pk2ItemType pk2ItemType)
        {
            switch (pk2ItemType)
            {
                case Pk2ItemType.Blade:
                case Pk2ItemType.Sword:
                case Pk2ItemType.SwordOnehander:
                case Pk2ItemType.Darkstaff:
                case Pk2ItemType.ClericStaff:
                    return true;
                default:
                    return false;
            }
        }

        public static bool Is2SlotWeapon(this Pk2ItemType pk2ItemType)
        {
            switch (pk2ItemType)
            {
                case Pk2ItemType.Glaive:
                case Pk2ItemType.Spear:
                case Pk2ItemType.SwordTwohander:
                case Pk2ItemType.Axe:
                case Pk2ItemType.MageStaff:
                case Pk2ItemType.Harp:
                    return false;
                default:
                    return true;
            }
        }
    }

    public enum Pk2ItemType : uint
    {
        Other = 0x01,
        Equipment = 0x110,

        //Garment
        GarmentHead = 0x111,
        GarmentShoulder = 0x112,
        GarmentBody = 0x113,
        GarmentLegs = 0x114,
        GarmentGloves = 0x115,
        GarmentFeet = 0x116,
        //Protector
        ProtectorHead = 0x121,
        ProtectorShoulder = 0x122,
        ProtectorBody = 0x123,
        ProtectorLegs = 0x124,
        ProtectorGloves = 0x125,
        ProtectorFeet = 0x126,
        //Armor
        ArmorHead = 0x131,
        ArmorShoulder = 0x132,
        ArmorBody = 0x133,
        ArmorLegs = 0x134,
        ArmorGloves = 0x135,
        ArmorFeet = 0x136,

        //Shield
        ShieldCH = 0x141,
        ShieldEU = 0x142,

        //CH Accessory
        EarringCH = 0x151,
        NecklaceCH = 0x152,
        RingCH = 0x153,

        //Robe
        RobeHead = 0x191,
        RobeShoulder = 0x192,
        RobeBody = 0x193,
        RobeLegs = 0x194,
        RobeGloves = 0x195,
        RobeFeet = 0x196,
        //LA
        LightArmorHead = 0x1A1,
        LightArmorShoulder = 0x1A2,
        LightArmorBody = 0x1A3,
        LightArmorLegs = 0x1A4,
        LightArmorGloves = 0x1A5,
        LightArmorFeet = 0x1A6,
        //HA
        HeavyArmorHead = 0x1B1,
        HeavyArmorShoulder = 0x1B2,
        HeavyArmorBody = 0x1B3,
        HeavyArmorLegs = 0x1B4,
        HeavyArmorGloves = 0x1B5,
        HeavyArmorFeet = 0x1B6,

        //Weapon
        Sword = 0x162,
        Blade = 0x163,
        Spear = 0x164,
        Glaive = 0x165,
        Bow = 0x166,
        SwordOnehander = 0x167,
        SwordTwohander = 0x168,
        Axe = 0x169,
        Darkstaff = 0x16A,
        MageStaff = 0x16B,
        Crossbow = 0x16C,
        Dagger = 0x16D,
        Harp = 0x16E,
        ClericStaff = 0x16F,
        //Other
        //Trade
        TraderFlag = 0x171,
        ThiefFlag = 0x172,
        HunterFlag = 0x173,


        //EU Accessory
        EarringEU = 0x1C1,
        NecklaceEU = 0x1C2,
        RingEU = 0x1C3,

        //Itemmall
        AvatarHead = 0x1D1,
        AvatarDress = 0x1D2,
        AvatarAttachment = 0x1D3,
        DevilsSpirit = 0x1E1,

        //Pet
        GrowthPet = 0x211,
        AbilityPet = 0x212,

        MagicCube = 0x231,

        //Consumables
        //Potion
        HPPot = 0x311,
        HPGrain = 0x3111,
        MPPot = 0x312,
        MPGrain = 0x3121,
        VigourPot = 0x313,
        VigourGrain = 0x3131,
        PetHPPotion = 0x314,
        GrassOfLife = 0x316,
        HGPPotion = 0x319,
        FortRepair = 0x31A,
        //BadStatus
        PurificationPill = 0x321,
        UniversalPill = 0x326,
        PetPill = 0x327,

        //
        ReturnScroll = 0x331,
        Vehicle = 0x332,
        ReverseReturnScroll = 0x333,
        StallDecoration = 0x334,
        GlobalChatting = 0x335,
        FortMonsterTablet = 0x336,
        GuildMonsterScroll = 0x337,
        FortManual = 0x338,
        FortFlag = 0x339,
        EXP_SP_or_Zerk_Scroll = 0x33A,
        FortScroll = 0x33B,
        SkillPointScroll = 0x33C,
        ItemPlusEnhanceScroll = 0x33E,

        //Ammo
        Bolt = 0x342,
        Arrow = 0x341,

        //Gold
        Gold = 0x350,
        GoldTrade = 0x351,

        ItemTrade = 0x381,
        ItemQuest = 0x390,

        //Alchemy
        Elixir = 0x3A1,

        WeaponElixir = 0x3A10,
        ShieldElixir = 0x3A11,
        ProtectorElixir = 0x3A12,
        AccessoryElixir = 0x3A13,
        LuckyPowder = 0x3A2,
        AdvancedElixir = 0x3A4,

        //Stones
        MagicStone = 0x3B1,

        MagicStoneStrength = 0x3B10,
        MagicStoneIntelligence = 0x3B11,
        MagicStoneMaster = 0x3B12,
        MagicStoneStrikes = 0x3B13,
        MagicStoneDiscipline = 0x3B14,
        MagicStonePenetration = 0x3B15,
        MagicStoneDodging = 0x3B16,
        MagicStoneStamina = 0x3B17,
        MagicStoneMagic = 0x3B18,
        MagicStoneFogs = 0x3B19,
        MagicStoneAir = 0x3B1A,
        MagicStoneFire = 0x3B1B,
        MagicStoneImmunity = 0x3B1C,
        MagicStoneRevival = 0x3B1D,
        MagicStoneSteady = 0x3B1E,
        MagicStoneLuck = 0x3B1F,
        MagicStoneAstral = 0x3B1F0,
        MagicStoneImmortal = 0x3B1F1,

        AttributeStone = 0x3B2,

        AttributeStoneCourage = 0x3B20,
        AttributeStoneWarriors = 0x3B21,
        AttributeStonePhilosophy = 0x3B22,
        AttributeStoneMeditation = 0x3B23,
        AttributeStoneChallenge = 0x3B24,
        AttributeStoneFocus = 0x3B25,
        AttributeStoneFlesh = 0x3B26,
        AttributeStoneLife = 0x3B27,
        AttributeStoneMind = 0x3B28,
        AttributeStoneSpirit = 0x3B29,
        AttributeStoneDodging = 0x3B2A,
        AttributeStoneAgility = 0x3B2B,
        AttributeStoneTraining = 0x3B2C,
        AttributeStonePrayer = 0x3B2D,

        //Tablets
        AlchemyTablet = 0x3B3,

        JadeTabletStrength = 0x3B300,
        JadeTabletIntelligence = 0x3B301,
        JadeTabletMaster = 0x3B302,
        JadeTabletStrikes = 0x3B303,
        JadeTabletDiscipline = 0x3B304,
        JadeTabletPenetration = 0x3B305,
        JadeTabletDodging = 0x3B306,
        JadeTabletStamina = 0x3B307,
        JadeTabletMagic = 0x3B308,
        JadeTabletFogs = 0x3B309,
        JadeTabletAir = 0x3B30A,
        JadeTabletFire = 0x3B30B,
        JadeTabletImmunity = 0x3B30C,
        JadeTabletRevival = 0x3B30D,
        JadeTabletSteady = 0x3B30E,
        JadeTabletLuck = 0x3B30F,
        RubyTabletCourage = 0x3B310,
        RubyTabletWarriors = 0x3B311,
        RubyTabletPhilosophy = 0x3B312,
        RubyTabletMeditation = 0x3B313,
        RubyTabletChallenge = 0x3B314,
        RubyTabletFocus = 0x3B315,
        RubyTabletFlesh = 0x3B316,
        RubyTabletLife = 0x3B317,
        RubyTabletMind = 0x3B318,
        RubyTabletSpirit = 0x3B319,
        RubyTabletDodging = 0x3B31A,
        RubyTabletAgility = 0x3B31B,
        RubyTabletTraining = 0x3B31C,
        RubyTabletPrayer = 0x3B31D,

        AlchemyMaterial = 0x3B4,
        AlchemyElement = 0x3B5,
        DestructionRondo = 0x3B6,
        MagicStoneMall = 0x3B7, // Immortality

        MagicPOP = 0x3E1,
        ItemExchangeCoupon = 0x3E2,

        //InventoryExpansionItem = 0x3D 11, --> 3 3 13 17
        BuffScroll = 0x3D1, // INT, STR, Speed
        SpeedPotion = 0x3D11,
        EXPBoostScroll = 0x3D4,
        SPBoostScroll = 0x3D5,
        RepairHammer = 0x3D7,
        ItemGenderSwitchScroll = 0x3D8,
        SkinChangeScroll = 0x3D9,

        Premium = 0x3DE, // including Pet Growth Potion
        PetWatch = 0x3DF,
    }

    public enum Gender : byte
    {
        Male = 0,
        Female = 1,
        Unisex = 2
    }

    public enum Race : byte
    {
        Chinese = 0,
        Euro = 1,
        Universal = 2
    }

    public struct Pk2Item : IPk2Types, IEnumerable
    {
        public uint Id;
        public string LongId;
        public string Name;
        public bool IsMallItem;
        public Pk2ItemType Type;
        public Race Race;
        public bool IsSOX;
        public int Level;
        public int Degree;
        public int MaxStacks;
        public Gender Gender;
        public bool IsStorable;

        public Pk2Item(string id, string longId, string name, string isMallItem, string type, string race, string isSOX, string isStorable, string level, string maxStacks, string gender, string degree)
        {
            this.Id = uint.Parse(id);
            this.LongId = longId;
            this.Name = name;
            this.IsMallItem = isMallItem == "1";
            this.Type = PK2Utils.GetItemtype(type, longId);
            this.Race = (Race)byte.Parse(race);
            this.IsSOX = isSOX == "1";
            this.IsStorable = int.Parse(isStorable) > 1;
            this.Level = int.Parse(level);
            this.MaxStacks = int.Parse(maxStacks);
            this.Gender = (Gender)byte.Parse(gender);
            this.Degree = int.Parse(degree);
        }


        public override string ToString()
        {
            return Id + "|" +
                LongId + "|" +
                Name + "|" +
                (IsMallItem ? "1" : "0") + "|" +
                Type.ToString("X").TrimStart('0') + "|" +
                Race.ToString("D") + "|" +
                (IsSOX ? "1" : "0") + "|" +
                (IsStorable ? "2" : "0") + "|" +
                Level + "|" +
                MaxStacks + "|" +
                Gender.ToString("D") + "|" +
                Degree;
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Id.ToString()).GetEnumerator();
        }

        public IPk2Types ToType => this;


        /// <summary>
        /// Return whether the item type is a type that counts as a consumable.
        /// Type uint-value is bitshifted to see whether the MSB equals 1.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsConsumable()
        {
            return ((uint)Type >> 8) == 3;
        }

        /// <summary>
        /// Return whether the item type is a type that counts as equipment.
        /// Type uint-value is bitshifted to see whether the MSB equals 1.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsEquipmentItem()
        {
            return ((uint)Type >> 8) == 1;
        }

        /// <summary>
        /// Return whether the item type is a type that counts as a weapon.
        /// A bitwise AND comparison with the basic weapon flags (0x160) should result in 0x160:
        /// 0x100 for equipment and 0x060 for weapon.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsWeapon()
        {
            return ((uint)Type & 0x160) == 0x160;
        }

        /// <summary>
        /// Return whether the item type is a type that counts as a shield.
        /// A bitwise AND comparison with the basic shield flags (0x140) should result in 0x140:
        /// 0x100 for equipment and 0x040 for shield.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsShield()
        {
            return ((uint)Type & 0x140) == 0x140;
        }

        /// <summary>
        /// Return whether the item type is a type that counts as apparel.
        /// IT should be an equipment item that is not a shield, accessory or weapon
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsClothing()
        {
            return IsEquipmentItem() && !(((uint)Type & 0x140) == 0x140 || ((uint)Type & 0x150) == 0x150 || ((uint)Type & 0x160) == 0x160 || ((uint)Type & 0x1C0) == 0x1C0);
        }

        /// <summary>
        /// Return whether the item type is a type that counts as an accessory.
        /// It either has the CH accessory flags (0x150) or EU accessory flags (0x1C0).
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsAccessory()
        {
            return ((uint)Type & 0x150) == 0x150 || ((uint)Type & 0x1C0) == 0x1C0;
        }
    }

    public enum Pk2NPCType : uint
    {
        Other = 0x00,
        PlayerCh = 10000,
        PlayerEU = 10010,
        Monster = 21100,
        MonsterUniqueA = 21110,
        MonsterUniqueB = 21113,
        MonsterEvent = 21130, // BossA
        MonsterBossB = 21131,
        MonsterBossC = 21133,
        MonsterBossD = 21136,
        MonsterEventStrong = 21137, // BossE
        MonsterRaidBoss = 21138, // BossF


        MonsterTradeThief = 21230,
        MonsterTradeHunter = 21330,
        MonsterQuestCH = 21400,
        MonsterQuestEU = 21410,
        MonsterQuestUnique = 21430,
        MonsterSummon = 21530,
        NPCInteractive = 22030,
        NPCFortressStructure = 22130,
        FortressSpecial1 = 0x23A32,
        FortressSpecial2 = 0x23B32,
        PetAbility = 23330,
        PetPickupStart = 23400,
        PetPickup = 23430,
        PetGuildCH = 23500,
        PetGuildEU = 23510,
        PetGuardianB = 24130,
        PetFortressHangar = 24330,
        PetVehicle = 23130,
        PetTransport = 23231,
        PetTransportMall = 23232
    }

    public struct Pk2NPC : IPk2Types, IEnumerable
    {
        public uint Id;
        public string LongId;
        public string Name;
        public int Level;
        public int HP;
        public Pk2NPCType Type;

        public Pk2NPC(string id, string longId, string name, string level, string hp, string type)
        {
            this.Id = uint.Parse(id);
            this.LongId = longId;
            this.Name = name;
            this.Level = int.Parse(level);
            this.HP = int.Parse(hp);
            this.Type = PK2Utils.GetNPCtype(type);
        }

        public override string ToString()
        {
            return Id + "|" + LongId + "|" + Name + "|" + Level + "|" + HP + "|" + (uint)Type;
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Id.ToString()).GetEnumerator();
        }

        public IPk2Types ToType => this;
    }

    public struct Pk2Teleporter : IPk2Types, IEnumerable
    {
        public uint Id;
        public string LongId;
        public string Name;
        public Pk2TeleporterData TeleporterData;

        public Pk2Teleporter(string id, string longId, string name, Pk2TeleporterData teleporterData)
        {
            this.Id = uint.Parse(id);
            this.LongId = longId;
            this.Name = name;
            this.TeleporterData = teleporterData;
        }

        public override string ToString()
        {
            return TeleporterData.TeleportLinks.Count == 0
                ? Id + "|" + LongId + "|" + Name
                : Id + "|" + LongId + "|" + Name + "|" + TeleporterData.ToString();
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Id.ToString()).GetEnumerator();
        }

        public IPk2Types ToType => this;
    }

    public struct Pk2TeleporterData : IPk2Types, IEnumerable
    {
        public uint Id;
        public string LongId;
        public string Name;
        public List<uint> TeleportLinks;

        public Pk2TeleporterData(string id, string longId, string name, List<uint> teleportLinks)
        {
            this.Id = string.IsNullOrWhiteSpace(id) ? 0 : uint.Parse(id);
            this.LongId = longId;
            this.Name = name;
            this.TeleportLinks = teleportLinks;
        }

        public override string ToString()
        {
            return Id == 0 && LongId == "" && Name == "" && TeleportLinks.Count == 0
                ? ""
                : TeleportLinks.Count == 0
                    ? Id + "|" + LongId + "|" + Name
                    : Id + "|" + LongId + "|" + Name + "|" + String.Join<uint>("|", TeleportLinks);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Id.ToString()).GetEnumerator();
        }

        public IPk2Types ToType => this;
    }

    //shoptabdata.txt --> 	shoptabdataID, shoptabdataLongID, shoptabdataID, shoptabdataName
    public struct Pk2ShopTabData : IEquatable<string>, IPk2Types, IEnumerable
    {
        public uint ShopTabDataID;
        public string ShopTabDataLongID;
        public uint ShopTabGroupID;
        public string ShopTabDataName;
        public List<PK2ShopItemRef> ShopTabItems;

        public Pk2ShopTabData(string shopTabDataID, string shopTabDataLongID, string shopTabGroupID, string shopTabDataName, List<PK2ShopItemRef> shopTabItems)
        {
            this.ShopTabDataID = uint.Parse(shopTabDataID);
            this.ShopTabDataLongID = shopTabDataLongID;
            this.ShopTabGroupID = uint.Parse(shopTabGroupID);
            this.ShopTabDataName = shopTabDataName;
            this.ShopTabItems = shopTabItems;
        }

        public IPk2Types ToType => this;

        public bool Equals(string other)
        {
            return ShopTabDataID.Equals(other);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)ShopTabDataID.ToString()).GetEnumerator();
        }

        public override string ToString()
        {
            return ShopTabDataID + "|" + ShopTabDataLongID + "|" + ShopTabGroupID + "|" + ShopTabDataName + "|" + String.Join<string>("|", Array.ConvertAll(ShopTabItems.ToArray(), x => x.ToString()));
        }
    }
    //shopdata.txt -->	shopID, shopLongID, xxx 0	???	tabID
    public struct Pk2ShopData : IEquatable<string>, IEnumerable
    {
        public uint ShopID; // int because value can be minus
        public int ShopNPCID;
        public List<Pk2ShopTabData> ShopTabs;

        public Pk2ShopData(string shopID, string shopNPCID, List<Pk2ShopTabData> shopTabs)
        {
            this.ShopID = uint.Parse(shopID);
            this.ShopNPCID = int.Parse(shopNPCID);
            this.ShopTabs = shopTabs;
        }

        public bool Equals(string other)
        {
            return ShopID.Equals(other);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)ShopID.ToString()).GetEnumerator();
        }

        public override string ToString()
        {
            return ShopID + "|" + ShopNPCID + "|" + ShopTabs.SelectMany(x => x.ShopTabDataLongID).ToString();
        }
    }
    //shopitemdata -->	shoptabID, itemID, shopgroupdataID
    public struct Pk2ShopItemData : IEquatable<string>, IPk2Types, IEnumerable
    {
        public uint ShopTabID;
        public uint ItemID;
        //string ShopGroupTabID;

        public Pk2ShopItemData(string shopTabID, string itemID/*, string shopGroupTabID*/)
        {
            this.ShopTabID = uint.Parse(shopTabID);
            this.ItemID = uint.Parse(itemID);
            //this.ShopGroupTabID = shopGroupTabID;
        }

        public IPk2Types ToType => this;

        public bool Equals(string other)
        {
            return ShopTabID.Equals(other);
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)ItemID.ToString()).GetEnumerator();
        }

        public override string ToString()
        {
            return ShopTabID + "|" + ItemID;
        }
    }

    public enum Pk2SkillType : byte
    {
        //BasicType  == 0
        Passive = 0x0,

        //BasicType == 1
        Imbue = 0x1,
        SpeedBuff = 0x2,

        // BasicType == 2
        Buff = 0x3,
        Heal = 0x4,
        SelfHeal = 0x5,
        ManaBuff = 0x6,
        Resurrection = 0x7,
        Debuff = 0x8,
        Cure = 0x9,
        KnockDownOnly = 0xA,
        Knockdown = 0xB,
        WarlockDOT = 0xC,
        Other = 0xF
    }

    public static class Pk2SkillTypeExtensions
    {
        /// <summary>
        /// Predicate of whether a weapon suits a given skill.
        /// Multiple weapons can match a skill. For example, glaive and spear can cast heuksal skills.
        /// </summary>
        /// <param name="pk2Skill">Skill to match</param>
        /// <param name="itemType">Weapon to match</param>
        /// <returns>Do skill and weapon match?</returns>
        public static bool DoesSkillMatchWeapon(this Pk2Skill pk2Skill, Pk2ItemType itemType)
        {
            switch (pk2Skill.RequiredWeapon)
            {
                case Pk2ItemType.Other:
                    return true;
                case Pk2ItemType.ShieldCH:
                    return itemType == Pk2ItemType.ShieldCH;
                case Pk2ItemType.ShieldEU:
                    return itemType == Pk2ItemType.ShieldEU;
                case Pk2ItemType.Sword:
                    return itemType == Pk2ItemType.Sword
                        || itemType == Pk2ItemType.Blade;
                case Pk2ItemType.Blade:
                    return itemType == Pk2ItemType.Sword
                        || itemType == Pk2ItemType.Blade;
                case Pk2ItemType.Spear:
                    return itemType == Pk2ItemType.Spear
                        || itemType == Pk2ItemType.Glaive;
                case Pk2ItemType.Glaive:
                    return itemType == Pk2ItemType.Spear
                        || itemType == Pk2ItemType.Glaive;
                case Pk2ItemType.Bow:
                    return itemType == Pk2ItemType.Bow;
                case Pk2ItemType.SwordOnehander:
                    return itemType == Pk2ItemType.SwordOnehander;
                case Pk2ItemType.SwordTwohander:
                    return itemType == Pk2ItemType.SwordTwohander;
                case Pk2ItemType.Axe:
                    return itemType == Pk2ItemType.Axe;
                case Pk2ItemType.Darkstaff:
                    return itemType == Pk2ItemType.Darkstaff;
                case Pk2ItemType.MageStaff:
                    return itemType == Pk2ItemType.MageStaff;
                case Pk2ItemType.Crossbow:
                    return itemType == Pk2ItemType.Crossbow;
                case Pk2ItemType.Dagger:
                    return itemType == Pk2ItemType.Dagger;
                case Pk2ItemType.Harp:
                    return itemType == Pk2ItemType.Harp;
                case Pk2ItemType.ClericStaff:
                    return itemType == Pk2ItemType.ClericStaff;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Predicate of whether the weapon in the primary slot is the correct one for a given skill.
        /// </summary>
        /// <param name="pk2Skill">Skill to be cast</param>
        /// <returns>Is correct weapon equipped</returns>
        public static bool IsRequiredWeaponEquipped(this Pk2Skill pk2Skill)
        {
            return CharInfoGlobal.Inventory.TryGetValue((byte)pk2Skill.RequiredWeapon.GetRequiredWeaponSlot(), out Item item) && DoesSkillMatchWeapon(pk2Skill, item.Type);
        }

        public static bool IsKnockDownSkill(this Pk2SkillType skill)
        {
            return skill == Pk2SkillType.Knockdown
                || skill == Pk2SkillType.KnockDownOnly;
        }

        public static bool IsAttack(this Pk2SkillType skill)
        {
            return skill == Pk2SkillType.Debuff
                || skill == Pk2SkillType.Knockdown
                || skill == Pk2SkillType.KnockDownOnly
                || skill == Pk2SkillType.WarlockDOT
                || skill == Pk2SkillType.Other;
        }

        public static bool IsBuff(this Pk2SkillType skill)
        {
            return skill == Pk2SkillType.Buff
                || skill == Pk2SkillType.ManaBuff
                || skill == Pk2SkillType.Heal
                || skill == Pk2SkillType.SelfHeal
                || skill == Pk2SkillType.SpeedBuff;
        }

        public static bool IsTransferableBuff(this Pk2Skill skill)
        {
            return skill.Type == Pk2SkillType.Buff
                && skill.LongId.Contains("SKILL_EU_CLERIC_RECOVERYA_QUICK_B")
                | skill.LongId.Contains("SKILL_EU_CLERIC_RECOVERYA_GROUP")
                | skill.LongId.Contains("BATTLAA_GUARD")
                | skill.LongId.Contains("BARD_DANCEA")
                | skill.LongId.Contains("BARD_SPEEDUPA_HITRATE");
        }
    }

    public struct Pk2Skill : IPk2Types, IEnumerable
    {
        public uint Id;
        public string LongId;
        public string Name;
        public int CastTime;
        public int Cooldown;
        public int Duration;
        public int MP;
        public Pk2SkillType Type;
        public Pk2ItemType RequiredWeapon;
        public bool TargetRequired;

        public Pk2Skill(string id, string longId, string name, string castTime, string cooldown, string duration, string mp, Pk2SkillType type, string targetRequired)
        {
            this.Id = uint.Parse(id);
            this.LongId = longId;
            this.Name = name;
            this.CastTime = int.Parse(castTime);
            this.Cooldown = int.Parse(cooldown);
            this.Duration = int.Parse(duration);
            this.MP = int.Parse(mp);
            this.Type = type;
            this.RequiredWeapon = GetRequiredWeaponType(longId);
            this.TargetRequired = targetRequired == "1";
        }

        private static Pk2ItemType GetRequiredWeaponType(string skillID)
        {
            if (skillID.StartsWith("SKILL_CH_SWORD"))
            {
                return Pk2ItemType.Sword;
            }
            else if (skillID.StartsWith("SKILL_CH_SPEAR"))
            {
                return Pk2ItemType.Spear;
            }
            else if (skillID.StartsWith("SKILL_CH_BOW"))
            {
                return Pk2ItemType.Bow;
            }
            else if (skillID.StartsWith("SKILL_CH_FIRE_SHIELD") || skillID.StartsWith("SKILL_CH_SWORD_SHIELD"))
            {
                return Pk2ItemType.ShieldCH;
            }
            else if (skillID.StartsWith("SKILL_EU_WARRIOR_ONEHANDA_SHIELD"))
            {
                return Pk2ItemType.ShieldEU;
            }
            else if (skillID.StartsWith("SKILL_EU_WARRIOR_ONEHANDA"))
            {
                return Pk2ItemType.SwordOnehander;
            }
            else if (skillID.StartsWith("SKILL_EU_WARRIOR_TWOHAND"))
            {
                return Pk2ItemType.SwordTwohander;
            }
            else if (skillID.StartsWith("SKILL_EU_WARRIOR_DUALA"))
            {
                return Pk2ItemType.Axe;
            }
            else if (skillID.StartsWith("SKILL_EU_WARLOCK"))
            {
                return Pk2ItemType.Darkstaff;
            }
            else if (skillID.StartsWith("SKILL_EU_WIZARD"))
            {
                return Pk2ItemType.MageStaff;
            }
            else if (skillID.StartsWith("SKILL_EU_ROG_BOW"))
            {
                return Pk2ItemType.Crossbow;
            }
            else if (skillID.StartsWith("SKILL_EU_ROG_DAGGER") || skillID.StartsWith("SKILL_EU_ROG_STEALTHA_ATTACK") || skillID.StartsWith("SKILL_EU_ROG_POIS_DAGP_DAGGAR"))
            {
                return Pk2ItemType.Dagger;
            }
            else if (skillID.StartsWith("SKILL_EU_BARD"))
            {
                return Pk2ItemType.Harp;
            }
            else if (skillID.StartsWith("SKILL_EU_CLERIC"))
            {
                return Pk2ItemType.ClericStaff;
            }
            else
            {
                return Pk2ItemType.Other;
            }
        }

        public Pk2Skill(string id, string longId, string name, string castTime, string cooldown, string duration, string mp, string type, string targetRequired)
            : this(id, longId, name, castTime, cooldown, duration, mp, PK2Utils.GetSkillType(type), targetRequired)
        {
        }

        //ID,LongID,Name,CastTime,Cooldown,Duration,MP,Type
        public Pk2Skill(string id, string longId, string name, string castTime, string cooldown, string duration, string mp, string[] type, string targetRequired)
            : this(id, longId, name, castTime, cooldown, duration, mp, PK2Utils.GetSkillType(type), targetRequired)
        {
        }

        public IPk2Types ToType => this;

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)Id.ToString()).GetEnumerator();
        }

        public override string ToString()
        {
            // take index 1 of type to remove preceding 0 instead of .TrimStart('0'), which removes value 0x00 entirely, resulting in "other" being parsed after loading instead.
            return Id + "|" + LongId + "|" + Name + "|" + CastTime + "|" + Cooldown + "|" + Duration + "|" + MP + "|" + Type.ToString("X")[1] + "|" + (TargetRequired ? "1" : "0");
        }
    }
}
