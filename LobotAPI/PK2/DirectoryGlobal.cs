﻿using System;

namespace LobotAPI.Globals
{
    public sealed partial class DirectoryGlobal
    {
        public static readonly string DataFolder = AppDomain.CurrentDomain.BaseDirectory + "Data\\";
        public static readonly string NavMeshFolder = AppDomain.CurrentDomain.BaseDirectory + "NavMesh\\";
        public static readonly string SettingsFolder = AppDomain.CurrentDomain.BaseDirectory + "Settings\\";
        public static readonly string LogFolder = AppDomain.CurrentDomain.BaseDirectory + "Log\\";
        public static readonly string ScriptFolder = AppDomain.CurrentDomain.BaseDirectory + "Scripts\\";
        public static readonly string LogData = LogFolder + "Log.txt";
        public static readonly string NameData = DataFolder + "Names.txt";
        public static readonly string ItemData = DataFolder + "Items.txt";
        public static readonly string LevelData = DataFolder + "Levels.txt";
        public static readonly string NPCData = DataFolder + "NPCs.txt";
        public static readonly string ShopData = DataFolder + "Shops.txt";
        public static readonly string SkillData = DataFolder + "Skills.txt";
        public static readonly string TeleporterData = DataFolder + "Teleporters.txt";
    }
}
