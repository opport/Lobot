﻿using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace LobotAPI
{
    /// <summary>
    /// The class used for pk2 operations which simplifies underlying procedures.
    /// Makes use of the facade pattern.
    /// </summary>
    public static class PK2Utils
    {
        private static CultureInfo cultureInfo = new CultureInfo("en-US");
        private static bool IsInitialized = false;

        private static Dictionary<Pk2ItemType, Func<Tuple<ItemFilter, DegreeFilter>>> TypeFilters = new Dictionary<Pk2ItemType, Func<Tuple<ItemFilter, DegreeFilter>>>()
        {
            //Other = 0x01,
            //Equipment = 0x110,

            //Garment
            { Pk2ItemType.GarmentHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Head, Settings.Instance.Filter.DegreesCH) },
            { Pk2ItemType.GarmentShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Shoulder , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.GarmentBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Chest , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.GarmentLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Hose , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.GarmentGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Glove , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.GarmentFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Boots , Settings.Instance.Filter.DegreesCH)},
            //Protector
            { Pk2ItemType.ProtectorHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Head , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ProtectorShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Shoulder , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ProtectorBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Chest , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ProtectorLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Hose , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ProtectorGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Glove , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ProtectorFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Boots , Settings.Instance.Filter.DegreesCH)},
            //Armor
            { Pk2ItemType.ArmorHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Head , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ArmorShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Shoulder , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ArmorBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Chest , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ArmorLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Hose , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ArmorGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Glove , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ArmorFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Boots , Settings.Instance.Filter.DegreesCH)},

            //Shield
            { Pk2ItemType.ShieldCH, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Shield , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.ShieldEU, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUShield , Settings.Instance.Filter.DegreesEU)},

            //CH Accessory
            { Pk2ItemType.EarringCH, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Earrings , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.NecklaceCH, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Necklace , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.RingCH, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Ring , Settings.Instance.Filter.DegreesCH)},

            //Robe
            { Pk2ItemType.RobeHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHead , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RobeShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUShoulder , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RobeBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUChest , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RobeLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHose , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RobeGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUGloves , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RobeFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUBoots , Settings.Instance.Filter.DegreesEU)},
            //LA
            { Pk2ItemType.LightArmorHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHead , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.LightArmorShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUShoulder , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.LightArmorBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUChest , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.LightArmorLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHose , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.LightArmorGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUGloves , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.LightArmorFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUBoots , Settings.Instance.Filter.DegreesEU)},
            //HA
            { Pk2ItemType.HeavyArmorHead, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHead , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.HeavyArmorShoulder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUShoulder , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.HeavyArmorBody, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUChest , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.HeavyArmorLegs, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHose , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.HeavyArmorGloves, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUGloves , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.HeavyArmorFeet, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUBoots , Settings.Instance.Filter.DegreesEU)},

            //Weapon
            { Pk2ItemType.Sword, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Sword , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.Blade, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Blade , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.Spear, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Spear , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.Glaive, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Glaive , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.Bow, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Bow , Settings.Instance.Filter.DegreesCH)},
            { Pk2ItemType.SwordOnehander, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUSword1H , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.SwordTwohander, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUSword2H , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.Axe, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUAxe , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.Darkstaff, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUWarlockStaff , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.MageStaff, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUWizardStaff , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.Crossbow, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUCrossbow , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.Dagger, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUDagger , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.Harp, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUHarp , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.ClericStaff, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUClericStaff , Settings.Instance.Filter.DegreesEU)},
            //Other
            //Trade
            { Pk2ItemType.TraderFlag, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.ThiefFlag, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.HunterFlag, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},


            //EU Accessory
            { Pk2ItemType.EarringEU, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUEarrings , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.NecklaceEU, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EUNecklace , Settings.Instance.Filter.DegreesEU)},
            { Pk2ItemType.RingEU, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.EURing , Settings.Instance.Filter.DegreesEU)},

            //Itemmall
            { Pk2ItemType.AvatarHead, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.AvatarDress, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.AvatarAttachment, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.DevilsSpirit, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},

            //Pet
            { Pk2ItemType.GrowthPet, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.AbilityPet, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},

            { Pk2ItemType.MagicCube, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},

            //Consumables
            //Potion
            { Pk2ItemType.HPPot, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.HPPots , null)},
            { Pk2ItemType.HPGrain, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.HPPots , null)},
            { Pk2ItemType.MPPot, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.MPPots , null)},
            { Pk2ItemType.MPGrain, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.MPPots , null)},
            { Pk2ItemType.VigourPot, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.VigourPots , null)},
            { Pk2ItemType.VigourGrain, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.VigourPots , null)},
            { Pk2ItemType.PetHPPotion, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.COSItems, null)},
            { Pk2ItemType.GrassOfLife, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.COSItems , null)},
            { Pk2ItemType.HGPPotion, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.COSItems , null)},
            { Pk2ItemType.FortRepair, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            //BadStatus
            { Pk2ItemType.PurificationPill, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.PurificationPills , null)},
            { Pk2ItemType.UniversalPill, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.UniversalPills , null)},
            { Pk2ItemType.PetPill, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.COSItems, null)},

            //
            { Pk2ItemType.ReturnScroll, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.ReturnScrolls , null)},
            { Pk2ItemType.Vehicle, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.COSItems , null)},
            { Pk2ItemType.ReverseReturnScroll, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.ReturnScrolls , null)},
            { Pk2ItemType.StallDecoration, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.GlobalChatting, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.FortMonsterTablet, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.GuildMonsterScroll, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.FortManual, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.FortFlag, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.EXP_SP_or_Zerk_Scroll, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.FortScroll, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.SkillPointScroll, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.ItemPlusEnhanceScroll, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},

            //Ammo
            { Pk2ItemType.Bolt, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Bolts , null)},
            { Pk2ItemType.Arrow, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Arrows, null)},

            //Gold
            { Pk2ItemType.Gold, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Gold , null)},
            { Pk2ItemType.GoldTrade, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.Gold , null)},

            { Pk2ItemType.ItemTrade, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.ItemQuest, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems, null)},

            //Alchemy
            { Pk2ItemType.WeaponElixir, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.WeaponElixir , null)},
            { Pk2ItemType.ShieldElixir, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.ShieldElixir , null)},
            { Pk2ItemType.ProtectorElixir, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.ProtectorElixir, null)},
            { Pk2ItemType.AccessoryElixir, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AccessoryElixir, null)},
            { Pk2ItemType.LuckyPowder, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AlchemyMaterial , null)},
            { Pk2ItemType.AdvancedElixir, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AlchemyMaterial , null)},

            { Pk2ItemType.MagicStoneStrength, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStrength, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneIntelligence, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeIntelligence, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneMaster, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeMaster, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneStrikes, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStrikes, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneDiscipline, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeDiscipline, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStonePenetration, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadePenetration, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneDodging, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeDodging, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneStamina, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStamina, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneMagic, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeMagic, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneFogs, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeFogs, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneAir, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeAir, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneFire, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeFire, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneImmunity, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeImmunity, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneRevival, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeRevival, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneSteady, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeSteady, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.MagicStoneLuck, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeLuck, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneCourage, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyCourage, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneWarriors, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyWarriors, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStonePhilosophy, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyPhilosophy, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneMeditation, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyMeditation, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneChallenge, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyChallenge, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneFocus, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyFocus, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneFlesh, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyFlesh, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneLife, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyLife, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneMind, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyMind, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneSpirit, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubySpirit, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneDodging, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyDodging, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneAgility, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyAgility, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStoneTraining, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyTraining, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.AttributeStonePrayer, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyPrayer, Settings.Instance.Filter.DegreesAlchemy)},

            { Pk2ItemType.JadeTabletStrength, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStrength, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletIntelligence, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeIntelligence, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletMaster, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeMaster, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletStrikes, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStrikes, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletDiscipline, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeDiscipline, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletPenetration, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadePenetration, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletDodging, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeDodging, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletStamina, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeStamina, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletMagic, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeMagic, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletFogs, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeFogs, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletAir, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeAir, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletFire, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeFire, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletImmunity, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeImmunity, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletRevival, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeRevival, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletSteady, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeSteady, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.JadeTabletLuck, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.JadeLuck, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletCourage, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyCourage, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletWarriors, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyWarriors, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletPhilosophy, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyPhilosophy, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletMeditation, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyMeditation, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletChallenge, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyChallenge, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletFocus, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyFocus, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletFlesh, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyFlesh, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletLife, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyLife, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletMind, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyMind, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletSpirit, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubySpirit, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletDodging, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyDodging, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletAgility, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyAgility, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletTraining, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyTraining, Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.RubyTabletPrayer, () => new Tuple<ItemFilter, DegreeFilter>( Settings.Instance.Filter.RubyPrayer, Settings.Instance.Filter.DegreesAlchemy)},


            { Pk2ItemType.AlchemyMaterial, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AlchemyMaterial , null)},
            { Pk2ItemType.AlchemyElement, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AlchemyMaterial , Settings.Instance.Filter.DegreesAlchemy)},
            { Pk2ItemType.DestructionRondo, () => new Tuple<ItemFilter, DegreeFilter>(Settings.Instance.Filter.AlchemyMaterial , null)},
            { Pk2ItemType.MagicStoneMall, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems, Settings.Instance.Filter.DegreesAlchemy)},

            { Pk2ItemType.MagicPOP, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},
            { Pk2ItemType.ItemExchangeCoupon, () => new Tuple<ItemFilter, DegreeFilter>(null , null)},

            //InventoryExpansionItem = 0x3D 11, --> 3 3 13 17
            { Pk2ItemType.BuffScroll, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)}, // INT, STR, Speed
            { Pk2ItemType.SpeedPotion, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},
            { Pk2ItemType.EXPBoostScroll, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},
            { Pk2ItemType.SPBoostScroll, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},
            { Pk2ItemType.RepairHammer, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},
            { Pk2ItemType.ItemGenderSwitchScroll, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},
            { Pk2ItemType.SkinChangeScroll, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)},

            { Pk2ItemType.Premium, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)}, // including Pet Growth Potion
            { Pk2ItemType.PetWatch, () => new Tuple<ItemFilter, DegreeFilter>(ItemFilterSettings.FilterQuestItems , null)}
        };

        private static Pk2ItemType GetElixirType(string longID)
        {
            if (longID.StartsWith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_WEAPON")) { return Pk2ItemType.WeaponElixir; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_SHIELD")) { return Pk2ItemType.ShieldElixir; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ARMOR")) { return Pk2ItemType.ProtectorElixir; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_REINFORCE_RECIPE_ACCESSARY")) { return Pk2ItemType.AccessoryElixir; }
            else { return Pk2ItemType.Elixir; }
        }

        private static Pk2ItemType GetMagicStoneType(string longID)
        {
            if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_STR")) { return Pk2ItemType.MagicStoneStrength; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_INT")) { return Pk2ItemType.MagicStoneIntelligence; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_DUR")) { return Pk2ItemType.MagicStoneMaster; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_HR")) { return Pk2ItemType.MagicStoneStrikes; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_BLOCK")) { return Pk2ItemType.MagicStoneDiscipline; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_EVADE_CRITICAL")) { return Pk2ItemType.MagicStonePenetration; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_ER")) { return Pk2ItemType.MagicStoneDodging; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_HP")) { return Pk2ItemType.MagicStoneStamina; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_MP")) { return Pk2ItemType.MagicStoneMagic; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_FROSTBITE")) { return Pk2ItemType.MagicStoneFogs; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_ESHOCK")) { return Pk2ItemType.MagicStoneAir; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_BURN")) { return Pk2ItemType.MagicStoneFire; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_POISON")) { return Pk2ItemType.MagicStoneImmunity; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_ZOMBIE")) { return Pk2ItemType.MagicStoneRevival; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_SOLID")) { return Pk2ItemType.MagicStoneSteady; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_LUCK")) { return Pk2ItemType.MagicStoneLuck; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_ASTRAL")) { return Pk2ItemType.MagicStoneImmortal; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICSTONE_ATHANASIA")) { return Pk2ItemType.MagicStoneAstral; }
            else { return Pk2ItemType.MagicStone; }
        }

        private static Pk2ItemType GetAttributeStoneType(string longID)
        {
            if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_PA")) { return Pk2ItemType.AttributeStoneCourage; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_PASTR")) { return Pk2ItemType.AttributeStoneWarriors; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_MA")) { return Pk2ItemType.AttributeStonePhilosophy; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_MAINT")) { return Pk2ItemType.AttributeStoneMeditation; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_CRITICAL")) { return Pk2ItemType.AttributeStoneChallenge; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_HR")) { return Pk2ItemType.AttributeStoneFocus; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_PD")) { return Pk2ItemType.AttributeStoneFlesh; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_PDSTR")) { return Pk2ItemType.AttributeStoneLife; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_MD")) { return Pk2ItemType.AttributeStoneMind; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_MDINT")) { return Pk2ItemType.AttributeStoneSpirit; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_ER")) { return Pk2ItemType.AttributeStoneDodging; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_BR")) { return Pk2ItemType.AttributeStoneAgility; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_PAR")) { return Pk2ItemType.AttributeStoneTraining; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRSTONE_MAR")) { return Pk2ItemType.AttributeStonePrayer; }
            else { return Pk2ItemType.AttributeStone; }
        }

        private static Pk2ItemType GetTabletType(string longID)
        {
            if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_STR")) { return Pk2ItemType.JadeTabletStrength; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_INT")) { return Pk2ItemType.JadeTabletIntelligence; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_DUR")) { return Pk2ItemType.JadeTabletMaster; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_HR")) { return Pk2ItemType.JadeTabletStrikes; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_BLOCK")) { return Pk2ItemType.JadeTabletDiscipline; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_EVADE_CRITICAL")) { return Pk2ItemType.JadeTabletPenetration; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_ER")) { return Pk2ItemType.JadeTabletDodging; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_HP")) { return Pk2ItemType.JadeTabletStamina; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_MP")) { return Pk2ItemType.JadeTabletMagic; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_FROSTBITE")) { return Pk2ItemType.JadeTabletFogs; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_ESHOCK")) { return Pk2ItemType.JadeTabletAir; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_BURN")) { return Pk2ItemType.JadeTabletFire; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_POISON")) { return Pk2ItemType.JadeTabletImmunity; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_ZOMBIE")) { return Pk2ItemType.JadeTabletRevival; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_SOLID")) { return Pk2ItemType.JadeTabletSteady; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_MAGICTABLET_LUCK")) { return Pk2ItemType.JadeTabletLuck; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_PA")) { return Pk2ItemType.RubyTabletCourage; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_PASTR")) { return Pk2ItemType.RubyTabletWarriors; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_MA")) { return Pk2ItemType.RubyTabletPhilosophy; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_MAINT")) { return Pk2ItemType.RubyTabletMeditation; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_CRITICAL")) { return Pk2ItemType.RubyTabletChallenge; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_HR")) { return Pk2ItemType.RubyTabletFocus; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_PD")) { return Pk2ItemType.RubyTabletFlesh; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_PDSTR")) { return Pk2ItemType.RubyTabletLife; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_MD")) { return Pk2ItemType.RubyTabletMind; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_MDINT")) { return Pk2ItemType.RubyTabletSpirit; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_ER")) { return Pk2ItemType.RubyTabletDodging; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_BR")) { return Pk2ItemType.RubyTabletAgility; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_PAR")) { return Pk2ItemType.RubyTabletTraining; }
            else if (longID.StartsWith("ITEM_ETC_ARCHEMY_ATTRTABLET_MAR")) { return Pk2ItemType.RubyTabletPrayer; }
            else { return Pk2ItemType.AlchemyTablet; }
        }

        /// <summary>
        /// Specify type when the numeric value is not enough
        /// </summary>
        /// <param name="value">numeric type value</param>
        /// <param name="longID">string identifier</param>
        /// <returns>Specific type</returns>
        private static Pk2ItemType DisambiguateType(uint value, string longID)
        {
            switch (value)
            {
                case (uint)Pk2ItemType.HPPot:
                    return longID.Contains("SPOTION") ? Pk2ItemType.HPGrain : Pk2ItemType.HPPot;
                case (uint)Pk2ItemType.MPPot:
                    return longID.Contains("SPOTION") ? Pk2ItemType.MPGrain : Pk2ItemType.MPPot;
                case (uint)Pk2ItemType.VigourPot:
                    return longID.Contains("SPOTION") ? Pk2ItemType.VigourGrain : Pk2ItemType.VigourPot;
                case (uint)Pk2ItemType.BuffScroll:
                    return longID.Contains("POTION_SPEED") ? Pk2ItemType.SpeedPotion : Pk2ItemType.BuffScroll;
                case (uint)Pk2ItemType.Elixir:
                    return GetElixirType(longID);
                case (uint)Pk2ItemType.MagicStone:
                    return GetMagicStoneType(longID);
                case (uint)Pk2ItemType.AttributeStone:
                    return GetAttributeStoneType(longID);
                case (uint)Pk2ItemType.AlchemyTablet:
                    return GetTabletType(longID);
                case (uint)Pk2ItemType.MagicStoneMall:
                    return GetMagicStoneType(longID);
                default:
                    return (Pk2ItemType)value;
            }
        }

        public static bool IsMagicStone(this Pk2ItemType type)
        {
            return (uint)type >> 4 == 0x3B1;
        }

        public static bool IsAttributeStone(this Pk2ItemType type)
        {
            return (uint)type >> 4 == 0x3B2;
        }

        public static bool IsJadeTablet(this Pk2ItemType type)
        {
            return (uint)type >> 4 == 0x3B30;
        }

        public static bool IsRubyTablet(this Pk2ItemType type)
        {
            return (uint)type >> 4 == 0x3B31;
        }
        public static Tuple<ItemFilter, DegreeFilter> GetFilterTuple(this Pk2ItemType type)
        {
            if (TypeFilters.TryGetValue(type, out Func<Tuple<ItemFilter, DegreeFilter>> tuple))
            {
                return tuple.Invoke();
            }
            else
            {
                return new Tuple<ItemFilter, DegreeFilter>(null, null);
            }
        }

        public static bool ShouldPick(this Pk2Item item)
        {
            Tuple<ItemFilter, DegreeFilter> tuple = item.Type.GetFilterTuple();
            bool matchingPickSetting = tuple.Item1 == null ? false : tuple.Item1.Pick;
            bool matchingDegree = tuple.Item2 == null ? true : tuple.Item2[item.Degree];
            return matchingPickSetting && matchingDegree;
        }

        public static bool ShouldStore(this Pk2Item item)
        {
            Tuple<ItemFilter, DegreeFilter> tuple = item.Type.GetFilterTuple();
            bool matchingStoreSetting = tuple.Item1 == null ? false : tuple.Item1.Store;
            bool matchingDegree = tuple.Item2 == null ? true : tuple.Item2[item.Degree];
            return matchingStoreSetting && matchingDegree;
        }

        public static bool ShouldSell(this Pk2Item item)
        {
            Tuple<ItemFilter, DegreeFilter> tuple = item.Type.GetFilterTuple();
            bool matchingSellSetting = tuple.Item1 == null ? false : tuple.Item1.Sell;
            bool matchingDegree = tuple.Item2 == null ? true : tuple.Item2[item.Degree];
            return matchingSellSetting && matchingDegree;
        }

        public static bool ShouldDrop(this Pk2Item item)
        {
            Tuple<ItemFilter, DegreeFilter> tuple = item.Type.GetFilterTuple();
            bool matchingDropSetting = tuple.Item1 == null ? false : tuple.Item1.Drop;
            bool matchingDegree = tuple.Item2 == null ? true : tuple.Item2[item.Degree];
            return matchingDropSetting && matchingDegree;
        }

        public static Pk2ItemType GetItemtype(string typeFlags, string longID)
        {
            if (uint.TryParse(typeFlags, NumberStyles.HexNumber, cultureInfo, out uint value))
            {
                if (Enum.IsDefined(typeof(Pk2ItemType), value))
                {
                    return DisambiguateType(value, longID);
                }
            }

            return Pk2ItemType.Other;
        }

        public static Pk2NPCType GetNPCtype(string typeFlags)
        {
            if (uint.TryParse(typeFlags, out uint value))
            {
                if (Enum.IsDefined(typeof(Pk2NPCType), value))
                {
                    return (Pk2NPCType)value;
                }
            }

            return Pk2NPCType.Other;
        }

        /// <summary>
        /// Custom chain of command has to be used due to opaque flags in pk2-entry.
        /// </summary>
        /// <param name="skillEntry">pk2 entry in skilldataxxx.txt file</param>
        /// <returns>The type of the skill</returns>
        /// <seealso href="https://gitlab.com/opport/Lobot/issues/132"/>
        public static Pk2SkillType GetSkillType(string[] skillEntry)
        {
            return SkillTypeParser.GetType(skillEntry);
        }

        /// <summary>
        /// Parse skill type from text file
        /// </summary>
        /// <param name="typeString">The Typestring of the item in the text file</param>
        /// <returns>The type of the skill</returns>
        /// <seealso href="https://gitlab.com/opport/Lobot/issues/132"/>
        public static Pk2SkillType GetSkillType(string typeString)
        {
            if (byte.TryParse(typeString, NumberStyles.HexNumber, cultureInfo, out byte value))
            {
                if (Enum.IsDefined(typeof(Pk2SkillType), value))
                {
                    return (Pk2SkillType)value;
                }
            }

            return Pk2SkillType.Other;
        }

        public static bool ExistMediaPk2AndClient(string directoryPath)
        {
            if (string.IsNullOrEmpty(directoryPath) || !Directory.Exists(directoryPath))
            {
                return false;
            }

            bool hasClient = false;
            bool hasMediaPK2 = false;

            foreach (string filepath in Directory.GetFiles(directoryPath))
            {
                string currentfile = filepath.ToLower();

                if (currentfile.Contains("sro_client.exe"))
                {
                    hasClient = true;
                }
                else if (currentfile.Contains("media.pk2"))
                {
                    hasMediaPK2 = true;
                }
            }

            return hasMediaPK2 && hasClient;
        }

        /// <summary>
        /// Prepare to switch to another silkroad version. For example to another private server
        /// </summary>
        private static void DeleteOldPK2Info()
        {
            File.Delete(DirectoryGlobal.DataFolder);
            File.Delete(DirectoryGlobal.ItemData);
            File.Delete(DirectoryGlobal.LevelData);
            File.Delete(DirectoryGlobal.NPCData);
            File.Delete(DirectoryGlobal.ShopData);
            File.Delete(DirectoryGlobal.SkillData);
            File.Delete(DirectoryGlobal.TeleporterData);
        }

        private static async Task ExtractParallelAsync(List<Action> actions)
        {
            List<Task> tasks = new List<Task>();

            foreach (Action a in actions)
            {
                tasks.Add(Task.Run(() => a.Invoke()));
            }

            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// The function for extracting and mapping information from pk2 files after choosing the sro directory.
        /// It makes use of the pk2 extractors and tries to load any previously extracted information from the bot data folder before attempting the more time-consuming extraction.
        /// </summary>
        public static async void ExtractAllInformation(bool deleteOldInfo = false)
        {
            try
            {
                Directory.CreateDirectory(DirectoryGlobal.DataFolder);
                Directory.CreateDirectory(DirectoryGlobal.LogFolder);
                Directory.CreateDirectory(DirectoryGlobal.NavMeshFolder);

                if (IsInitialized && deleteOldInfo)
                {
                    DeleteOldPK2Info();
                    IsInitialized = false;
                }

                if (!IsInitialized)
                {
                    IsInitialized = true;


                    new Pk2ExtractorNames().ExtractAll(); // Has to be parsed first for all others to use
                    new Pk2ExtractorSkills().ExtractAll(); // Parse synchronously to stay in dispatcher thread

                    List<Action> extractionActions = new List<Action>()
                    {
                        () => new Pk2ExtractorItems().ExtractAll(), // Parse before Shops 
                        () => new Pk2ExtractorNPCs().ExtractAll() // Parse before Teleporters and Shops
                    };

                    await ExtractParallelAsync(extractionActions);


                    extractionActions.Clear();
                    extractionActions = new List<Action>()
                    {
                        () => new Pk2ExtractorLevelData().ExtractAll(),
                        () => new Pk2ExtractorShops().ExtractAll(),
                        () => new Pk2ExtractorTeleporters().ExtractAll(),
                        //new Pk2ExtractorNavMesh().ExtractAll(); // TODO optimize to only get pathfinding data
                        () => new PK2PartyTitleExtractor().ExtractAll()
                    };

                    await ExtractParallelAsync(extractionActions);
                }

            }
            catch (IndexOutOfRangeException ex)
            {
                Logger.Log(LogLevel.PK2, ex.Message + ex.StackTrace);
            }
        }
    }
}