﻿using SilkroadSecurityApi;
using System;
using System.IO;
using System.Text;

namespace LobotAPI.PK2
{
    public class PK2Main
    {
        private static PK2Main s_instance;
        private static UnicodeEncoding _enc;
        private static PK2Class s_pK2Class;

        public static PK2Class PK2Class
        {
            get
            {
                if (s_pK2Class == null)
                {
                    s_pK2Class = new PK2Class(Globals.PK2DataGlobal.PK2MediaFilePath);
                }

                return s_pK2Class;
            }
        }
        public static UnicodeEncoding Enc
        {
            get
            {
                if (_enc == null)
                {
                    _enc = new UnicodeEncoding();
                }

                return _enc;
            }
        }

        public static PK2Main Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new PK2Main();
                }

                return s_instance;
            }
        }

        /// <summary>
        /// Get server version from sv.t file in media.pk2 to send a patch request.
        /// <seealso href="https://github.com/ProjectHax/DivisionInfo/blob/master/DivisionInfo/divisioninfo.cpp"/>
        /// </summary>
        public void GetSilkroadVersion()
        {
            byte[] bSilkroadVersion = PK2Class.GetFile("sv.t");
            byte[] initializationString = Encoding.ASCII.GetBytes("SILKROADVERSION");
            uint bytesToDecode = new BinaryReader(new MemoryStream(bSilkroadVersion, false)).ReadUInt32();

            if (bSilkroadVersion == null || bytesToDecode > 8)
            {
                return;
            }

            Blowfish bf = new Blowfish();
            bf.Initialize(initializationString, 0, 8);
            byte[] decoded = new byte[bytesToDecode];
            decoded = bf.Decode(bSilkroadVersion, 4, (int)bytesToDecode);

            string decodedAscii = Encoding.ASCII.GetString(decoded);
            Globals.Settings.LoginSettings.Version = UInt32.Parse(decodedAscii);
        }
        public void GetServerIPAddress()
        {
            byte[] bServerIPAddress = PK2Class.GetFile("divisioninfo.txt");

            if (bServerIPAddress == null)
            {
                return;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(bServerIPAddress)))
            {
                string strServerIPAddress = sr.ReadToEnd();
                string[] arrServerIPAddress = strServerIPAddress.Split('\0');
                Globals.Settings.LoginSettings.Locale = (byte)arrServerIPAddress[0][0];
                Globals.NetworkGlobal.GatewayServerDivision = arrServerIPAddress[arrServerIPAddress.Length - 6];
                Globals.NetworkGlobal.GatewayIP = arrServerIPAddress[arrServerIPAddress.Length - 2];
            }
        }
        public void GetServerPort()
        {
            byte[] bServerPort = PK2Class.GetFile("gateport.txt");

            if (bServerPort == null)
            {
                return;
            }

            using (StreamReader sr = new StreamReader(new MemoryStream(bServerPort)))
            {
                Globals.NetworkGlobal.AgentPort = Convert.ToUInt16(sr.ReadToEnd().Replace("\0", ""));
            }
        }
    }
}
