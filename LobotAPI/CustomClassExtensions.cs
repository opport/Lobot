﻿using LobotAPI.Scripting;
using LobotAPI.Structs;
using static LobotAPI.Scripting.Area;
using static LobotAPI.Structs.ItemFilter;

namespace LobotAPI
{
    public static class CustomClassExtensions
    {
        /// <summary>
        /// Create a new Filter from a given value and fill in the blanks from current one.
        /// This is used for config.net settings to recognize changes, since simply changing properties is not enough.
        /// </summary>
        /// <param name="p">Type of value being changed</param>
        /// <param name="value">value amount</param>
        /// <returns>New object with changed value</returns>
        public static ItemFilter ItemFilterFromValue(this ItemFilter filter, FilterProperty p, bool value)
        {
            if (filter == null)
            {
                switch (p)
                {
                    case FilterProperty.Pick:
                        return new ItemFilter(value, false, false, false);
                    case FilterProperty.Store:
                        return new ItemFilter(false, value, false, false);
                    case FilterProperty.Sell:
                        return new ItemFilter(false, false, value, false);
                    case FilterProperty.Drop:
                        return new ItemFilter(false, false, false, value);
                    default:
                        return filter;
                }
            }

            switch (p)
            {
                case FilterProperty.Pick:
                    return new ItemFilter(value, filter.Store, filter.Sell, filter.Drop);
                case FilterProperty.Store:
                    return new ItemFilter(filter.Pick, value, filter.Sell, filter.Drop);
                case FilterProperty.Sell:
                    return new ItemFilter(filter.Pick, filter.Store, value, filter.Drop);
                case FilterProperty.Drop:
                    return new ItemFilter(filter.Pick, filter.Store, filter.Sell, value);
                default:
                    return filter;
            }
        }

        /// <summary>
        /// Create a new DegreeFilter from a given value and fill in the blanks from current 1.
        /// This is used for config.net settings to recognize changes, since simply changing properties is not enough.
        /// </summary>
        /// <param name="p">Type of value being changed</param>
        /// <param name="value">value amount</param>
        /// <returns>New object with changed value</returns>
        public static DegreeFilter DegreeFilterFromValue(this DegreeFilter filter, int index, bool value)
        {
            if (filter == null)
            {
                DegreeFilter newFilter = new DegreeFilter();
                newFilter.Arr[index] = value;
                return newFilter;
            }
            else
            {
                if (index < 0 || index > 12)
                {
                    return filter;
                }
                else
                {
                    bool[] array = new bool[13];

                    for (int i = 0; i < index; i++)
                    {
                        array[i] = filter.Arr[i];
                    }

                    array[index] = value;

                    for (int i = index + 1; i < filter.Arr.Length; i++)
                    {
                        array[i] = filter.Arr[i];
                    }

                    return new DegreeFilter(array);
                }
            }
        }

        /// <summary>
        /// Create a new Area from a given value and fill in the blanks from current one.
        /// This is used for config.net settings to recognize changes, since simply changing properties is not enough.
        /// </summary>
        /// <param name="p">Type of value being changed</param>
        /// <param name="value">value amount</param>
        /// <returns>New object with changed value</returns>
        public static Area AreaFromValue(this Area a, AreaProperty p, double value)
        {
            if (a == null)
            {
                return new Area();
            }

            switch (p)
            {
                case AreaProperty.X:
                    return new Area(value, a.Y, a.Radius);
                case AreaProperty.Y:
                    return new Area(a.X, value, a.Radius);
                case AreaProperty.R:
                    return new Area(a.X, a.Y, value);
                default:
                    return a;
            }
        }
    }
}
