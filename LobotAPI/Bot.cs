﻿using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System;

namespace LobotAPI
{
    /// <summary>
    /// The bot instance used to implement all functions provided.
    /// </summary>
    public static partial class Bot
    {
        private static BotStatus s_Status;
        private static bool s_isInteracting = false;
        private static ISpawnType s_selectedNPC = new Monster();
        public static BotStatus Status { get => s_Status; set { s_Status = value; OnBottingStatusChanged(EventArgs.Empty); NotifyPropertyChanged(); } }

        public static PartyEntry PartyCurrent = new PartyEntry(0);
        public static uint PartyMemberID = 0;
        public static ISpawnType SelectedNPC { get => s_selectedNPC; set { s_selectedNPC = value; OnSelectednpcIDChanged(EventArgs.Empty); } }

        public static bool IsInteracting { get => s_isInteracting; set { s_isInteracting = value; OnIsInteractingChanged(new BoolEventArgs(s_isInteracting)); } }
        public static CharInfo CharInfo { get; set; } = new CharInfo();
        public static CharData CharData { get; set; } = new CharData();
        public static ItemMallInfo ItemMallInfo { get; set; }
        public static Pet PickupPet { get; set; } = new Pet();
        public static GrowthPet GrowthPet { get; set; } = new GrowthPet(new Pet());
        public static TransportPet Transport { get; set; } = new TransportPet(new Pet());

        public static void Setup()
        {
            PK2Main pk2 = new PK2Main();
            pk2.GetServerIPAddress();
            pk2.GetServerPort();
            pk2.GetSilkroadVersion();
            PK2Utils.ExtractAllInformation();
        }
    }
}
