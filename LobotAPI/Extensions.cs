﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LobotAPI
{
    public static class Extensions
    {
        /// <summary>
        /// Check if the object is null and either return the object or its default if it is null;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T ObjectOrDefault<T>(this T t) where T : new() => t == null ? new T() : t;

        public static bool TryFind<T>(this List<T> list, Predicate<T> predicate, out T output)
        {
            int index = list.FindIndex(predicate);
            if (index != -1)
            {
                output = list[index];
                return true;
            }
            output = default(T);
            return false;
        }

        public static void AddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Collection is null");
            }

            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    // handle duplicate key issue here
                }
            }
        }

        public static ICollection<KeyValuePair<TKey, TValue>> AsCollection<TKey, TValue>(this IDictionary<TKey, TValue> dictionary) { return dictionary; }

        public static void RemoveAll<T>(this Collection<T> collection, Func<T, bool> condition)
        {
            for (int i = collection.Count - 1; i >= 0; i--)
            {
                if (condition(collection[i]))
                {
                    collection.RemoveAt(i);
                }
            }
        }

        public static void InsertSorted<T>(this ObservableCollection<T> collection, T item, Comparison<T> comparison)
        {
            if (collection.Count == 0)
            {
                collection.Add(item);
            }
            else
            {
                bool last = true;
                for (int i = 0; i < collection.Count; i++)
                {
                    int result = comparison.Invoke(collection[i], item);
                    if (result >= 1)
                    {
                        collection.Insert(i, item);
                        last = false;
                        break;
                    }
                }
                if (last)
                {
                    collection.Add(item);
                }
            }
        }
    }
}
