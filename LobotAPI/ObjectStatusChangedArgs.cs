﻿using System;

namespace LobotAPI
{
    public class ObjectStatusChangedArgs : EventArgs
    {
        public readonly ObjectStatus PreviousStatus;
        public readonly ObjectStatus CurrentStatus;

        public ObjectStatusChangedArgs(ObjectStatus previousStatus, ObjectStatus currentStatus)
        {
            PreviousStatus = previousStatus;
            CurrentStatus = currentStatus;
        }
    }
}
