﻿using System;

namespace LobotAPI
{
    public abstract class Message
    {
        /// <summary> The messageContent contains the actual message. </summary>
        private string messageContent { get; set; }
        /// <summary>  The messageSender denotes the origin of the message. </summary>
        private string messageSender { get; set; }
        /// <summary> The timeStamp denotes when the message was received. </summary>
        private DateTime timeStamp;


        /// <summary>
        ///  The standard constructor for a message object. Includes the message id, content and sender.
        /// </summary>
        /// <param name="messageID"> Unique id to identify message.</param>
        /// <param name="messageContent">The message.</param>
        /// <param name="messageSender">The origin of the message.</param>
        public Message(string messageID, string messageContent, string messageSender)
        {
            this.messageContent = messageContent;
            this.messageSender = messageSender;
            this.timeStamp = DateTime.Now;
        }
    }
}
