﻿using LobotAPI.Globals;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LobotAPI
{
    public static partial class Bot
    {
        private static CancellationTokenSource Cancellation = new CancellationTokenSource();

        private static async Task DelayedPositionUpdate(TimeSpan delay, uint targetID, Point p)
        {
            await Task.Delay(delay, Cancellation.Token).ContinueWith(task =>
            {
                if (!Cancellation.Token.IsCancellationRequested)
                {
                    if (targetID == Bot.CharData.UniqueID)
                    {
                        CharData.Position = p;
                    }
                    else if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn))
                    {
                        spawn.Position = p;
                    }
                }
            });
        }

        private static void DispatchDelayedUpdated(CharMoveArgs e, Point originPosition)
        {
            TimeSpan delay = TimeSpan.FromSeconds(ScriptUtils.GetWalkTime(originPosition, e.Destination));

            // cancel previous scheduled position update
            if (Cancellation.Token.CanBeCanceled)
            {
                Cancellation.Cancel();
            }
            Cancellation = new CancellationTokenSource();

            // queue new task to be run after delay
            DelayedPositionUpdate(delay, e.UniqueID, e.Destination).ConfigureAwait(false);
        }

        public static void CharMovementRegistered(object sender, CharMoveArgs e)
        {
            if (e.UniqueID == Bot.CharData.UniqueID)
            {
                // Make sure interaction shows false for moving bot
                Bot.IsInteracting = false;

                if (e.Origin != default(Point))
                {
                    CharData.Position = e.Origin;
                }

                if (e.Destination != CharData.Position) // No Origin point given in B021 packet
                {
                    // queue new task to be run after delay
                    DispatchDelayedUpdated(e, CharData.Position);
                }
            }
            else if (SpawnListsGlobal.TryGetSpawnObject(e.UniqueID, out ISpawnType spawn))
            {
                if (e.Destination != spawn.Position) // No Origin point given in B021 packet
                {
                    // queue new task to be run after delay
                    DispatchDelayedUpdated(e, spawn.Position);
                }
            }
        }

        public static void HandleCharStopMovement(object sender, CharStopMovementArgs e)
        {
            Cancellation.Cancel();
        }
    }
}
