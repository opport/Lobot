﻿using System;

namespace LobotAPI
{
    public enum PartyChangeType
    {
        Empty = 1,
        Update = 2,
        Full = 3
    }

    public class PartyChangedArgs : EventArgs
    {
        public PartyChangeType Type;

        public PartyChangedArgs(PartyChangeType type)
        {
            Type = type;
        }
    }
}
