﻿using System;
using System.Diagnostics;
using System.IO;

namespace LobotAPI.Client
{
    public class Main
    {
        public Process procClient = null;
        private IntPtr procHWND = IntPtr.Zero;

        public int procID = 0;

        public Main()
        {
            procClient = new Process();
            procClient.StartInfo.WorkingDirectory = Globals.NetworkGlobal.ClientDirectory;
            procClient.StartInfo.FileName = Globals.NetworkGlobal.ClientFilePath;
            procClient.StartInfo.Arguments = "/22 0 0";
        }

        public void Launch()
        {
            if (File.Exists(procClient.StartInfo.FileName))
                procClient.Start();

            procID = procClient.Id;
            procHWND = procClient.MainWindowHandle;
        }
        public void Close()
        {
            if (procClient == null || procClient.HasExited)
                return;

            procClient.Kill();
        }
    }
}
