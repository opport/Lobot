﻿namespace LobotAPI.Captcha
{
    /// <summary>
    /// Structure of a captcha image package [0x2322]
    /// 1   byte    image.Flag
    /// 2   ushort image.remain    //in bytes
    /// 2   ushort image.compressed //in bytes
    /// 2   ushort image.uncompressed //in bytes
    /// 2   ushort image.width //200
    /// 2   ushort image.height //64
    /// * byte[] image.compressedData
    /// </summary>
    public struct Captcha
    {
        public byte flag;
        public ushort remain;    //in bytes
        public ushort compressed; //in bytes
        public ushort uncompressed; //in bytes
        public ushort width; //200
        public ushort height; //64
        public byte[] compressedData;
    }
}
