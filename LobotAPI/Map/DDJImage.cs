﻿using System;

namespace LobotAPI.Map
{
    /// <summary>
    /// 
    // DDJ file has at least these 20 bytes of overhead before the DDS begins
    //12  string Header                              //JMXVDDJ 1000
    //4   uint TextureSize
    //4   uint TextureHeaderLength                 //Joymax writes 3 for "DDS" but FourCC is "DDS "
    //* byte[] TextureFileBuffer
    /// <seealso href="https://github.com/DummkopfOfHachtenduden/SilkroadDoc/wiki/JMXVDDJ-1000"/>
    /// </summary>
    public class DDJImage
    {
        private byte[] buffer;
        string header;
        public readonly uint TextureSize;
        public readonly uint TextureHeaderLength;
        byte[] textureFileBuffer; // "DDS"
        private DDSImage _dDS;

        public DDSImage DDS { get => _dDS; private set => _dDS = value; }
        public byte[] Buffer { get => buffer; private set => buffer = value; }
        public string Header { get => header; private set => header = value; }
        public byte[] TextureFileBuffer { get => textureFileBuffer; private set => textureFileBuffer = value; }

        public DDJImage(byte[] ddjBuffer)
        {
            Buffer = ddjBuffer ?? throw new NullReferenceException();

            byte[] headerBuffer = new byte[12];
            Array.Copy(ddjBuffer, headerBuffer, 12);
            Header = System.Text.Encoding.Default.GetString(headerBuffer);

            byte[] textureBuffer = new byte[] { ddjBuffer[12], ddjBuffer[13], ddjBuffer[14], ddjBuffer[15] };
            TextureSize = BitConverter.ToUInt32(textureBuffer, 0);

            byte[] textureHeaderLengthBuffer = new byte[] { ddjBuffer[16], ddjBuffer[17], ddjBuffer[18], ddjBuffer[19] };
            TextureHeaderLength = BitConverter.ToUInt32(textureHeaderLengthBuffer, 0);

            TextureFileBuffer = new byte[] { ddjBuffer[20], ddjBuffer[21], ddjBuffer[22] };

            DDS = new DDSImage(DDJToDDS(Buffer));
        }

        public static byte[] DDJToDDS(byte[] ddjStream)
        {
            byte[] ddsStream = new byte[ddjStream.Length - 20];

            Array.Copy(ddjStream, 20, ddsStream, 0, ddsStream.Length);

            return ddsStream;
        }
    }
}
