﻿using LobotAPI.Packets.Char;
using LobotAPI.PK2;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

namespace LobotAPI.Map
{
    public static class MapUtils
    {
        private static ushort xSectorOld;
        private static ushort ySectorOld;

        private static ushort xSector;
        public static ushort XSector { get => xSector; private set => xSector = value; }
        private static ushort ySector;
        public static ushort YSector { get => ySector; private set => ySector = value; }



        public delegate void RenderMap(ushort x, ushort y);
        //public static event RenderMap RenderNewPosition;

        public const int SectorSize = 256;
        private static BitmapImage s_mapImage;
        private static BitmapImage s_oldMapBitmapImage;

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Update map if x or y sector has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void HandleMovementRegistered(object sender, EventArgs e)
        {
            if (e == EventArgs.Empty && (xSector != Bot.CharData.XSec || ySector != Bot.CharData.YSec))
            {
                xSectorOld = xSector;
                ySectorOld = ySector;

                xSector = Bot.CharData.XSec;
                ySector = Bot.CharData.YSec;

                // Switch to previously rendered map image if sectors are the same
                if (xSectorOld == Bot.CharData.XSec && ySectorOld == Bot.CharData.YSec)
                {
                    BitmapImage temp = MapBitMapImage;
                    MapBitMapImage = OldMapBitmapImage;
                    OldMapBitmapImage = temp;
                }
                else
                {
                    OldMapBitmapImage = MapBitMapImage;
                    MapBitMapImage = GetBitMapImage();
                    MapBitMapImage.Freeze();
                }
            }
            else if (e is CharMoveArgs)
            {
                CharMoveArgs cma = e as CharMoveArgs;
                ushort xSectorOrigin = Scripting.ScriptUtils.GetSectorX((int)cma.Origin.X);
                ushort ySectorOrigin = Scripting.ScriptUtils.GetSectorY((int)cma.Origin.Y);
                ushort xSectorDestination = Scripting.ScriptUtils.GetSectorX((int)cma.Destination.X);
                ushort ySectorDestination = Scripting.ScriptUtils.GetSectorY((int)cma.Destination.Y);

                if (xSector != xSectorDestination || ySector != ySectorDestination)
                {
                    xSectorOld = xSector;
                    ySectorOld = ySector;

                    xSector = xSectorDestination;
                    ySector = ySectorDestination;

                    // Switch to previously rendered map image if sectors are the same
                    if (xSectorOld == xSectorDestination && ySectorOld == ySectorDestination)
                    {
                        BitmapImage temp = MapBitMapImage;
                        MapBitMapImage = OldMapBitmapImage;
                        OldMapBitmapImage = temp;
                    }
                    else
                    {
                        OldMapBitmapImage = MapBitMapImage;
                        MapBitMapImage = GetBitMapImage();
                        MapBitMapImage.Freeze();
                    }
                }
            }
        }

        /// <summary>
        /// Rendered map image of last sector. The map is switched to this one for performance reasons, if the player runs back into the previous sector.
        /// </summary>
        private static BitmapImage OldMapBitmapImage { get => s_oldMapBitmapImage ?? s_mapImage; set => s_oldMapBitmapImage = value; }

        public static BitmapImage MapBitMapImage
        {
            get
            {
                if (s_mapImage == null)
                {
                    s_mapImage = GetBitMapImage();
                }
                return s_mapImage;
            }
            set
            {
                s_mapImage = value;
                NotifyPropertyChanged();
            }
        }


        public static Bitmap DDJToBMP(byte[] ddjStream)
        {
            byte[] bmpStream = DDJToDDS(ddjStream);

            using (MemoryStream ms = new MemoryStream(bmpStream))
            {
                Bitmap bmp = new Bitmap(ms);
                return bmp;
            }
        }

        private static BitmapImage GetBitMapImage()
        {
            BitmapImage bitmapImage = new BitmapImage();

            using (MemoryStream memory = new MemoryStream())
            {
                GetMap(xSector, ySector).Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;

                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
            }

            return bitmapImage;
        }

        public static Bitmap GetMap()
        {
            return GetMap(Bot.CharData.XSec, Bot.CharData.YSec);
        }

        public static Bitmap GetMap(int xSec = 168, int ySec = 97)
        {
            Bitmap bmp = new Bitmap(SectorSize * 3, SectorSize * 3, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(bmp);

            if (Bot.CharData.XSec == 0 && Bot.CharData.YSec == 0)
            {
                return new Bitmap(256, 256, PixelFormat.Format16bppArgb1555);
            }

            g.DrawImage(LoadSector(xSec - 1, ySec + 1), 0, 0, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec - 1, ySec), 0, SectorSize, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec - 1, ySec - 1), 0, SectorSize * 2, SectorSize, SectorSize);

            g.DrawImage(LoadSector(xSec, ySec + 1), SectorSize, 0, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec, ySec), SectorSize, SectorSize, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec, ySec - 1), SectorSize, SectorSize * 2, SectorSize, SectorSize);

            g.DrawImage(LoadSector(xSec + 1, ySec + 1), SectorSize * 2, 0, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec + 1, ySec), SectorSize * 2, SectorSize, SectorSize, SectorSize);
            g.DrawImage(LoadSector(xSec + 1, ySec - 1), SectorSize * 2, SectorSize * 2, SectorSize, SectorSize);

            return bmp;
        }

        public static int GetDwFloor()
        {
            int zpos = int.Parse(Bot.CharData.ZOffset.ToString());
            int floornr = 1;

            if (zpos < 114)
                floornr = 1;
            else if (zpos < 253)
                floornr = 2;
            else if (zpos < 392)
                floornr = 3;
            else if (zpos > 392)
                floornr = 4;

            return floornr;
        }

        public static Image LoadSector(int xSec, int ySec)
        {
            string name = Bot.CharData.IsInCave
                ? "qt_a01_floor0" + (8 - Bot.CharData.XSec).ToString() + "_" + xSec.ToString() + "x" + ySec.ToString() + ".ddj"
                : xSec.ToString() + "x" + ySec.ToString() + ".ddj";
            name = Bot.CharData.IsInCave && Bot.CharData.XSec == 0x01
                ? "dh_a01_floor0" + GetDwFloor().ToString() + "_" + xSec.ToString() + "x" + ySec.ToString() + ".ddj"
                : name;

            byte[] bytes = PK2Main.PK2Class.GetFile(name);

            if (bytes == null || bytes.Length < 1)
            {
                return new Bitmap(256, 256, PixelFormat.Format16bppArgb1555);
            }
            else
            {
                return new DDJImage(bytes).DDS.images[0];
            }
        }

        public static byte[] DDJToDDS(byte[] ddjStream)
        {
            byte[] ddsStream = new byte[ddjStream.Length - 20];

            Array.Copy(ddjStream, 20, ddsStream, 0, ddsStream.Length);

            return ddsStream;
        }

        public static byte[] DDSToDDJ(byte[] ddsStream)
        {
            byte[] ddjStream = new byte[ddsStream.Length + 20];

            Int32 ddjStreamLen = ddsStream.Length + 8;

            byte[] header1 = { 0x4A, 0x4D, 0x58, 0x56, 0x44, 0x44, 0x4A, 0x20, 0x31, 0x30, 0x30, 0x30 };
            byte[] header2 = BitConverter.GetBytes(ddjStreamLen);
            byte[] header3 = { 0x03, 0x00, 0x00, 0x00 };

            Array.Copy(header1, 0, ddjStream, 0, header1.Length);
            Array.Copy(header2, 0, ddjStream, header1.Length, header2.Length);
            Array.Copy(header3, 0, ddjStream, header1.Length + header2.Length, header3.Length);
            Array.Copy(ddsStream, 0, ddjStream, header1.Length + header2.Length + header3.Length, ddsStream.Length);

            return ddjStream;
        }
    }
}
