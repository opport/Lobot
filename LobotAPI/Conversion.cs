﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace LobotAPI
{
    public static class Conversion
    {
        /// <summary>
        /// Writes the given object instance to a binary file. <see href="https://stackoverflow.com/a/22417240"/>
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the XML file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the XML file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file. <see href="https://stackoverflow.com/a/22417240"/>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the XML.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Serializes an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializableObject"></param>
        /// <param name="fileName"></param>
        public static void SerializeObject<T>(T serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(fileName);
                    stream.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.NETWORK, "extractNav: " + e.Message);
                System.Diagnostics.Debug.WriteLine("extractNav: " + e.Message);
            }
        }


        /// <summary>
        /// Deserializes an xml file into an object list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T DeSerializeObject<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(T); }

            T objectOut = default(T);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(T);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (T)serializer.Deserialize(reader);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.NETWORK, "extractNav: " + e.Message);
                System.Diagnostics.Debug.WriteLine("extractNav: " + e.Message);
            }

            return objectOut;
        }

        public static string SerializeObject<T>(T serializableObject, string fileName, bool append = false)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(memoryStream, serializableObject);

                using (Stream stream = File.Open(fileName, append ? FileMode.Append : FileMode.Create))
                {
                    var binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(stream, serializableObject);
                }

                return Convert.ToBase64String(memoryStream.ToArray());
            }
        }

        public static object DeserializeObject<T>(string filePath)
        {
            using (StreamReader streamReader = new StreamReader(filePath))
            {
                byte[] bytes = Convert.FromBase64String(streamReader.ReadToEnd());

                using (MemoryStream stream = new MemoryStream(bytes))
                {
                    return new BinaryFormatter().Deserialize(stream);
                }
            }
        }

        /*
        // basConvert: Utilities to convert between byte arrays, hex strings,
        // strings containing binary values, and 32-bit word arrays.

        // NB: On 32-bit Unicode/CJK systems you may need to do a global
        // replace of Asc() and Chr() with AscW() and ChrW() respectively.

        // Version 2. November 2003: removed cv_BytesFromString which can be
        // done with abBytes = StrConv(strInput, vbFromUnicode).
        // - Added error handling to catch empty arrays.
        // - Made HexFromByte public.
        // Version 1. First published January 2002
        //************************* COPYRIGHT NOTICE*************************
        // This code was originally written in Visual Basic by David Ireland
        // and is copyright (c) 2000-2 D.I. Management Services Pty Limited,
        // all rights reserved.

        // You are free to use this code as part of your own applications
        // provided you keep this copyright notice intact.

        // This code may only be used as part of an application. It may
        // not be reproduced or distributed separately by any means without
        // the express written permission of the author.

        // David Ireland and DI Management Services Pty Limited make no
        // representations concerning either the merchantability of this
        // software or the suitability of this software for any particular
        // purpose. It is provided "as is" without express or implied
        // warranty of any kind.

        // Please forward comments or bug reports to <code@di-mgt.com.au>.
        // The latest version of this source code can be downloaded from
        // www.di-mgt.com.au/crypto.html.
        //****************** END OF COPYRIGHT NOTICE*************************

        // The Public Functions in this module are:
        // cv_BytesFromHex(sInputHex): Returns array of bytes
        // cv_WordsFromHex(sHex): Returns array of words (Longs)
        // cv_HexFromWords(aWords): Returns hex string
        // cv_HexFromBytes(aBytes()): Returns hex string
        // cv_HexFromString(str): Returns hex string
        // cv_StringFromHex(strHex): Returns string of ascii characters
        // cv_GetHexByte(sInputHex, iIndex): Extracts iIndex'th byte from hex string
        // RandHexByte(): Returns random byte as a 2-digit hex string
        // HexFromByte(x): Returns 2-digit hex string representing byte x


        // ERROR: Not supported in C#: DeclareDeclaration
        // ERROR: Not supported in C#: DeclareDeclaration
        // ERROR: Not supported in C#: DeclareDeclaration
        static int[] FloatToTwoSInt(float S)
        {
            byte[] bytArray = new byte[3];
            int Word1;
            int Word2;
            //Example Use W()=FloatToTwoSInt(-118.625)
            //Copy the memory area of the float into the memory area of our declared 4 byte array
            CopyMemory(bytArray(0), S, 4);
            //As the 4 byte array is stored consecutively in memory, we can then extract the Sint values
            CopyMemory(Word1, bytArray(0), 2);
            CopyMemory(Word2, bytArray(2), 2);
            //Return an array(0-1) of SInts
            FloatToTwoSInt = {
                Word1,
            Word2
    
        };
        }
        public static string DecToHexLong(long decvalue)
        {
            int HexLen;
            DecToHexLong = Hex(decvalue);
            HexLen = Len(DecToHexLong);
            if (HexLen < 8)
            {
                HexLen = 8 - HexLen;
                DecToHexLong = "0" + DecToHexLong;
            }
        }

        public static string DecToHex10Long(long decvalue)
        {
            int HexLen;
            DecToHex10Long = Hex(decvalue);
            HexLen = Len(DecToHex10Long);
            if (HexLen < 10)
            {
                HexLen = 10 - HexLen;
                DecToHex10Long = "0" + DecToHex10Long;
            }
        }

        public static string DecToHexWord(int decvalue)
        {
            int HexLen;
            DecToHexWord = Hex(decvalue);
            HexLen = Len(DecToHexWord);

            if (HexLen < 4)
            {
                HexLen = 4 - HexLen;
                DecToHexWord = "0" + DecToHexWord;
            }
        }
        public static string DWordFromInteger(int data)
        {
            int i;
            DWordFromInteger = Hex(data);
            if (Len(DWordFromInteger) == 1 | Len(DWordFromInteger) == 3 | Len(DWordFromInteger) == 5 | Len(DWordFromInteger) == 7)
            {
                DWordFromInteger = "0" + DWordFromInteger;
            }
            while (Len(DWordFromInteger) < 8)
            {
                DWordFromInteger = "0" + DWordFromInteger;
            }
            //Now flip
            string sTemp;
            for (i = 8; i >= 1; i += -1)
            {
                sTemp = sTemp + Mid(DWordFromInteger, i - 1, 2);
                i = i - 1;
            }
            DWordFromInteger = sTemp;
        }
        public static string ByteFromInteger(int data)
        {
            ByteFromInteger = Hex(data);
            if (Len(ByteFromInteger) == 1)
                ByteFromInteger = "0" + ByteFromInteger;
        }
        public static string Float2Hex(float TmpFloat)
        {
            byte[] TmpBytes = new byte[3];
            float TmpSng;
            string tmpStr;
            long x;
            TmpSng = TmpFloat;
            CopyMemory(());
            for (x = 3; x >= 0; x += -1)
            {
                if (Len(Hex(TmpBytes(x))) == 1)
                {
                    tmpStr = tmpStr + "0" + Hex(TmpBytes(x));
                }
                else
                {
                    tmpStr = tmpStr + Hex(TmpBytes(x));
                }
            }
            Float2Hex = tmpStr;

        }
        public static byte[] cv_BytesFromHex(string sInputHex)
        {
            // Returns array of bytes from hex string in big-endian order
            // E.g. sHex="FEDC80" will return array {&HFE, &HDC, &H80}
            long i;
            long M;
            byte[] aBytes;
            if (Len(sInputHex) % 2 != 0)
            {
                sInputHex = "0" + sInputHex;
            }

            M = Len(sInputHex) / 2;
            if (M <= 0)
            {
                // Version 2: Returns empty array
                cv_BytesFromHex = aBytes;
                return;
            }

            // ERROR: Not supported in C#: ReDimStatement


            for (i = 0; i <= M - 1; i++)
            {
                aBytes(i) = Val("&H" + Mid(sInputHex, i * 2 + 1, 2));
            }

            cv_BytesFromHex = aBytes;

        }

        public static long[] cv_WordsFromHex(string sHex)
        {
            // Converts string <sHex> with hex values into array of words (long ints)
            // E.g. "fedcba9876543210" will be converted into {&HFEDCBA98, &H76543210}
            const int ncLEN = 8;
            long i;
            long nWords;
            long[] aWords;

            nWords = Len(sHex) / ncLEN;
            if (nWords <= 0)
            {
                // Version 2: Returns empty array
                cv_WordsFromHex = aWords;
                return;
            }

            // ERROR: Not supported in C#: ReDimStatement

            for (i = 0; i <= nWords - 1; i++)
            {
                aWords(i) = Val("&H" + Mid(sHex, i * ncLEN + 1, ncLEN));
            }

            cv_WordsFromHex = aWords;

        }

        public static string StringToHex(string sMessage)
        {
            int i;
            string sHex;
            for (i = 1; i <= Len(sMessage); i++)
            {
                sHex = Hex(Asc(Mid(sMessage, i, 1)));
                if (Len(sHex) < 2)
                    sHex = 2 - Len(sHex) + "0" + sHex;
                StringToHex = StringToHex + sHex;
            }
        }

        public static string HexToString(string sMessage)
        {
            int i;
            for (i = 1; i <= Len(sMessage); i += 2)
            {
                HexToString = HexToString + Chr(Int("&h" + Mid(sMessage, i, 2)));
            }
        }

        public static string cv_HexFromWords(aWords)
        {
            // Converts array of words (Longs) into a hex string
            // E.g. {&HFEDCBA98, &H76543210} will be converted to "FEDCBA9876543210"
            const int ncLEN = 8;
            long i;
            long nWords;
            char[] sHex = new char[ncLEN];
            long iIndex;

            //Set up error handler to catch empty array
            // ERROR: Not supported in C#: OnErrorStatement

            if (!IsArray(aWords))
            {
                return;
            }

            nWords = UBound(aWords) - LBound(aWords) + 1;
            cv_HexFromWords = nWords * ncLEN + " ";
            iIndex = 0;
            for (i = 0; i <= nWords - 1; i++)
            {
                sHex = Hex(aWords(i));
                sHex = ncLEN - Len(sHex) + "0" + sHex;
                Mid(cv_HexFromWords, iIndex + 1, ncLEN) = sHex;
                iIndex = iIndex + ncLEN;
            }
            ArrayIsEmpty:



    }

        public static string inversebyte(string sData)
        {
            int i;
            if (Len(sData) % 2 == 0 & Len(sData) > 0)
            {
                for (i = 1; i <= Len(sData); i += 2)
                {
                    inversebyte = Mid(sData, i, 2) + inversebyte;
                }
                inversebyte = "" + inversebyte + "";
            }
            else
            {
                inversebyte = sData;
            }
        }

        public static string cv_HexFromBytes(byte[] aBytes)
        {
            // Returns hex string from array of bytes
            // E.g. aBytes() = {&HFE, &HDC, &H80} will return "FEDC80"
            long i;
            long iIndex;
            long nLen;

            //Set up error handler to catch empty array
            // ERROR: Not supported in C#: OnErrorStatement


            nLen = UBound(aBytes) - LBound(aBytes) + 1;

            cv_HexFromBytes = string(nLen * 2, " ");
            iIndex = 0;
            for (i = LBound(aBytes); i <= UBound(aBytes); i++)
            {
                Mid(cv_HexFromBytes, iIndex + 1, 2) = HexFromByte(aBytes(i));
                iIndex = iIndex + 2;
            }
            ArrayIsEmpty:



    }

        public static string cv_HexFromString(string str)
        {
            // Converts string <str> of ascii chars to string in hex format
            // str may contain chars of any value between 0 and 255.
            // E.g. "abc." will be converted to "6162632E"
            byte byt;
            long i;
            long n;
            long iIndex;
            string sHex = "";

            n = Len(str);
            sHex = sHex.PadRight(n * 2, " ");
            iIndex = 0;
            for (i = 1; i <= n; i++)
            {
                byt = Convert.ToByte(Asc(Mid(str, i, 1)) & 0xff);
                Mid(sHex, iIndex + 1, 2) = HexFromByte(byt);
                iIndex = iIndex + 2;
            }
            cv_HexFromString = sHex;

        }

        public static string cv_StringFromHex(string strHex)
        {
            // Converts string <strHex> in hex format to string of ascii chars
            // with value between 0 and 255.
            // E.g. "6162632E" will be converted to "abc."
            int i;
            int nBytes;

            nBytes = Len(strHex) / 2;
            cv_StringFromHex = cv_StringFromHex.PadRight(nBytes, " ");
            for (i = 0; i <= nBytes - 1; i++)
            {
                Mid(cv_StringFromHex, i + 1, 1) = Chr(Val("&H" + Mid(strHex, i * 2 + 1, 2)));
            }

        }

        public static byte cv_GetHexByte(string sInputHex, long iIndex)
        {
            // Extracts iIndex'th byte from hex string (starting at 1)
            // E.g. cv_GetHexByte("fecdba98", 3) will return &HBA
            long i;
            i = 2 * iIndex;
            if (i > Len(sInputHex) | i <= 0)
            {
                cv_GetHexByte = 0;
            }
            else
            {
                cv_GetHexByte = Val("&H" + Mid(sInputHex, i - 1, 2));
            }

        }
        public static string BinNumber { get; set; }
        static long i;
        static string sBinChar;
        static long lLength = Len(BinNumber);
        static long lPosiValue;
        static string Number;

        static int mlNumber = 0;
        */
    }

}
