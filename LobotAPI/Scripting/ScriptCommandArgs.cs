﻿using System;

namespace LobotAPI.Scripting
{
    public class ScriptCommandArgs : EventArgs
    {
        AbstractExpression expr;

        public ScriptCommandArgs(AbstractExpression expr)
        {
            this.expr = expr ?? throw new ArgumentNullException(nameof(expr));
        }
    }
}
