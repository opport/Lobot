﻿using LobotAPI.StateMachine;
using LobotAPI.Structs;
using System;

namespace LobotAPI.Scripting
{
    public class ShoppingArgs : EventArgs
    {
        public Monster NPC;
        public IState ShoppingState;

        public ShoppingArgs(Monster npc, IState shoppingSstate)
        {
            NPC = npc ?? throw new ArgumentNullException(nameof(npc));
            ShoppingState = shoppingSstate ?? throw new ArgumentNullException(nameof(shoppingSstate));
        }
    }
}
