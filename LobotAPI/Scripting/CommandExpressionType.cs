﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.Scripting
{
	public interface IShoppingType
	{
		string Signature { get; }
	}

	public sealed class ShoppingExpressionType : AbstractCommandExpressionType, IShoppingType
	{
		private string _signature;
		public string Signature => _signature;

		public ShoppingExpressionType(int value, string name, string signature) : base(value, name)
		{
			_signature = signature; 
		}
	}

	public sealed class CommandExpressionType : AbstractCommandExpressionType
	{
		public CommandExpressionType(int value, string name) : base(value, name)
		{
		}
	}

	/// <summary>
	/// The command expression type in the stype of the type-safe enum pattern.
	/// <see href="http://www.javacamp.org/designPattern/enum.html"/>
	/// </summary>
	public abstract class AbstractCommandExpressionType
	{
		private readonly String Name;
		private readonly int Value;

		private static readonly Dictionary<string, AbstractCommandExpressionType> instance = new Dictionary<string, AbstractCommandExpressionType>();

		public static readonly AbstractCommandExpressionType GO = new CommandExpressionType(1, "go");
		public static readonly AbstractCommandExpressionType WAIT = new CommandExpressionType(2, "wait");
		public static readonly AbstractCommandExpressionType TELEPORT = new CommandExpressionType(3, "teleport");
		public static readonly AbstractCommandExpressionType QUEST = new CommandExpressionType(4, "quest");
		public static readonly ShoppingExpressionType STORE = new ShoppingExpressionType(5, "store", "_WAREHOUSE");
		public static readonly ShoppingExpressionType STABLE = new ShoppingExpressionType(6, "stable", "_HORSE");
		public static readonly ShoppingExpressionType REPAIR = new ShoppingExpressionType(7, "repair", "_SMITH");
		public static readonly ShoppingExpressionType SHOP = new ShoppingExpressionType(8, "shop", "_SMITH");
		public static readonly ShoppingExpressionType POTION = new ShoppingExpressionType(9, "potion", "_POTION");
		public static readonly ShoppingExpressionType SPECIAL = new ShoppingExpressionType(10, "special", "_ACCESSORY");
		public static readonly AbstractCommandExpressionType MOUNT = new CommandExpressionType(11, "mount");
		public static readonly AbstractCommandExpressionType DISMOUNT = new CommandExpressionType(12, "dismount");
		public static readonly AbstractCommandExpressionType SETAREA1 = new CommandExpressionType(13, "setArea2");
		public static readonly AbstractCommandExpressionType SETAREA2 = new CommandExpressionType(14, "setArea2");

		protected AbstractCommandExpressionType(int value, String name)
		{
			this.Name = name;
			this.Value = value;
			instance[name] = this;
		}

		public override String ToString()
		{
			return Name;
		}

		/// <summary>
		/// Explicit conversion operator
		/// </summary>
		/// <param name="str">Type to convert from</param>
		/// <seealso href="https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/conversion-operators"/>
		public static explicit operator AbstractCommandExpressionType(string str)
		{
			if (instance.TryGetValue(str, out AbstractCommandExpressionType result))
			{
				return result;
			}
			else
			{
				throw new InvalidCastException();
			}
		}
	}
}
