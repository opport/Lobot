﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.Scripting
{
    public class DelayArgs : EventArgs
    {
        public int Delay;

        public DelayArgs(int delay  = 0)
        {
            Delay = delay;
        }
    }
}
