﻿using System;
using System.Windows;

namespace LobotAPI.Scripting
{
    [Serializable]
    public class Position : IEquatable<Position>
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Position(double x = 0, double y = 0, double z = 0)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Position(Vector vector, double z = 0) : this(vector.X, vector.Y) { }

        public Position(Point point, double z = 0) : this(point.X, point.Y) { }

        public ushort GetSectorY()
        {
            return (ushort)(Math.Floor(Y / 192 + 92));
        }
        public ushort GetSectorX()
        {
            return (ushort)(Math.Floor(X / 192 + 135));
        }

        public int GetSectorXOffset()
        {
            int result = (int)X % 192;
            if (result < 0)
            {
                result += 192;
            }
            return result;
        }

        public int GetSectorYOffset()
        {
            int result = (int)Y % 192;
            if (result < 0)
            {
                result += 192;
            }
            return result;
        }

        public bool Equals(Position other)
        {
            return X == other.X && Y == other.Y && Z == other.Z;
        }
    }
}
