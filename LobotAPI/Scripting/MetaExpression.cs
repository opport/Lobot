﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace LobotAPI.Scripting
{
    /// <summary>
    /// A script expression that can be used to decide which script to execute.
    /// </summary>
    public class MetaExpression : AbstractExpression
    {
        public string ExpressionName { get; private set; }
        public string Argument1 { get; private set; }
        public string Argument2 { get; private set; }

        public MetaExpression(string token)
        {
            if (Regex.IsMatch(token, Patterns.META_EXPR))
            {
                Evaluate(token);
            }
            else
            {
                throw new FormatException("Invalid META_EXPR: " + token);
            }
        }

        public MetaExpression(string command, string arg1)
        {
            ExpressionName = command;
            Argument1 = arg1;
        }

        public MetaExpression(string command, string arg1, string arg2)
        {
            ExpressionName = command;
            Argument1 = arg1;
            Argument2 = arg2;
        }

        /// <summary>
        /// Parse information from the injected token.
        /// </summary>
        /// <param name="token">Line to be evaluated from script</param>
        private void Evaluate(string token)
        {
            string[] splitToken = token.Split(new string[] { "//" }, 2, StringSplitOptions.RemoveEmptyEntries);
            splitToken = splitToken[0].Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            ExpressionName = splitToken[0];
            Argument1 = splitToken[1];

            if (splitToken.Length == 3)
            {
                if (ExpressionName == "NextScript")
                {
                    Argument2 = splitToken[2];
                }
                else
                {
                    throw new FormatException("Invalid META_EXPR, too many arguments:+ " + token);
                }
            }
        }

        public override bool Equals(object other)
        {
            MetaExpression other1 = (MetaExpression)other;
            return (this.ExpressionName == other1.ExpressionName
                && this.Argument1 == other1.Argument1
                && this.Argument2 == other1.Argument2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(ExpressionName);

            sb.Append(ExpressionName);

            if (Argument1.Length > 0)
            {
                sb.Append(Argument1);

                if (Argument2.Length > 0)
                {
                    sb.Append(", " + Argument2);
                }
            }

            return sb.ToString();
        }
    }
}
