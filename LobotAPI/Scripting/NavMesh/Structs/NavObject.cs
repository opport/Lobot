﻿using LobotAPI.PK2;
using System;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    /// <summary>
    /// //NavigationEntries
    //2   ushort entryCount
    //for (int entryIndex = 0; entryIndex<entryCount; entryIndex++)
    //{
    //    4   uint ID
    //    12  Vector3 Position
    //    2   ushort CollisionFlag   //0x00 = No, 0xFFFF = Yes
    //    4   float Yaw
    //    2   ushort UniqueID
    //    2   ushort Scale
    //    2   ushort EventZoneFlag   //0 = No, 256 = collision works when your character has CTF suit (red/blue ones)
    //    2   ushort RegionID
    //    2   ushort mountPointCount
    //    for (int i = 0; i<mountPointCount; i++)
    //    {   
    //        6   byte[] mountPointData      //where you can enter the object (bridges etc..)
    //    }
    //}
    /// </summary>
    [Serializable]
    public struct NavObject : IPk2Types, IEquatable<uint>
    {
        public uint ID;
        public Position Position;
        public ushort CollisionFlag;
        public float Yaw;
        public ushort UniqueID;
        public ushort Scale;
        public ushort EventZoneFlag;
        public ushort RegionID;

        public NavObject(uint iD, Position position, ushort collisionFlag, float yaw, ushort uniqueID, ushort scale, ushort eventZoneFlag, ushort regionID)
        {
            ID = iD;
            Position = position;
            CollisionFlag = collisionFlag;
            Yaw = yaw;
            UniqueID = uniqueID;
            Scale = scale;
            EventZoneFlag = eventZoneFlag;
            RegionID = regionID;
        }

        public IPk2Types ToType => this;

        public bool Equals(uint other)
        {
            return ID.Equals(other);
        }
    }
}
