﻿namespace LobotAPI.Scripting.NavMesh.Structs
{

    internal enum NodeType : byte
    {
        NotSet = 0,
        aLContour = 1,
        aBContour = 2,
        aTriEdge = 3,
        aObject = 4
    }
}
