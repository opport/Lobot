﻿using System.Collections.Generic;
using System.Windows;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    public class NavCellSplit
    {
        public class Line : IEqualityComparer<Line>
        {
            public ushort P1, p2;
            public LineType LineType = LineType.NonBlock;
            public List<ushort> Neighbors = new List<ushort>();
            public ushort ExtInfo;
            public Line(ushort pA, ushort pB)
            {
                P1 = pA;
                p2 = pB;
            }

            public bool Equals(Line x, Line y)
            {
                if ((x.P1 == y.P1 && x.p2 == y.p2) || (x.P1 == y.p2 && x.p2 == y.P1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public int GetHashCode(Line obj)
            {
                return obj.GetHashCode();
            }
        }

        public class Tri
        {
            public ushort P3I, P1I, P2I;
            public ushort L1I, L2I, L3I;
            public Tri(ushort P1I, ushort P2I, ushort P3I, ushort L1I, ushort L2I, ushort L3I)
            {
                this.P1I = P1I;
                this.P2I = P2I;
                this.P3I = P3I;
                this.L1I = L1I;
                this.L2I = L2I;
                this.L3I = L3I;
            }
        }

        public enum LineType : byte
        {

            NonBlock = 1,
            Block = 0,
            LC = 2,
            BC = 3,
            ObjectEntrance = 4

        }

        public List<Point> Points;
        public List<Line> Lines;
        public List<Tri> Triangles;

        public List<NavCellLink> LContour;
        public List<NavBorder> BContour;
    }
}
