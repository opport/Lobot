﻿using LobotAPI.PK2;
using System;
using System.Collections.Generic;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    /// <summary>
    /// //NavigationCells
    //4   uint cellCount
    //4   uint cellExtraCount
    //for (int cellIndex= 0; cellIndex<cellCount; cellIndex++)
    //{
    //    8   Vector2 Min
    //    8   Vector2 Max

    //    1   byte entryCount
    //    for (int i = 0; i<entryCount; i++)
    //    {
    //        2   ushort entryIndex
    //    }
    //}
    /// </summary>
    [Serializable]
    public struct NavCell : IPk2Types, IEquatable<NavCell>
    {
        public Position Min;
        public Position Max;

        public List<ushort> NavObjectIndex;
        public List<ushort> NavBorderIndex;
        public List<ushort> NavCellLinkIndex;

        public NavCell(Position min, Position max, List<ushort> navObjectIndex, List<ushort> navBorderIndex, List<ushort> navCellLinkIndex)
        {
            Min = min;
            Max = max;
            this.NavObjectIndex = navObjectIndex;
            this.NavBorderIndex = navBorderIndex;
            this.NavCellLinkIndex = navCellLinkIndex;
        }

        public bool Has_Object()
        {
            return NavObjectIndex.Count != 0;
        }

        public IPk2Types ToType => this;

        public bool Equals(NavCell other)
        {
            return Min.Equals(other.Min) && Max.Equals(other.Max) && NavObjectIndex.Equals(other.NavObjectIndex);
        }
    }
}
