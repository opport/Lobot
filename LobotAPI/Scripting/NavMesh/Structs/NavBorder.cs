﻿using LobotAPI.PK2;
using System;
using System.Windows;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    /// <summary>
    /// //NavigationRegionLinks
    //4   uint regionLinkCount
    //for (int linkIndex = 0; linkIndex<regionLinkCount; linkIndex++)
    //{
    //    8   Point2 Min
    //    8   Point2 Max

    //    1   byte LineFlag
    //    1   byte LineSource
    //    1   byte LineDestination

    //    2   ushort CellSource
    //    2   ushort CellDestination

    //    2   ushort RegionSource
    //    2   ushort RegionDestination
    //}
    /// </summary>
    [Serializable]
    public class NavBorder : IPk2Types, IEquatable<NavCell>
    {
        public struct Neighbour
        {
            public ushort SectorYX;
            public ushort ZoneIndex;
            public Neighbour(ushort SectorYX, ushort ZoneIndex)
            {
                this.SectorYX = SectorYX;
                this.ZoneIndex = ZoneIndex;
            }
        }

        public Point Min;
        public Point Max;
        public byte LineFlag;
        public byte LineSource;
        public byte LineDestination;
        public ushort CellSourceSector;
        public ushort CellDestinationSector;
        public ushort RegionSourceYX;
        public ushort RegionDestinationYX;

        public NavBorder(Point min, Point max, byte lineFlag, byte lineSource, byte lineDestination, ushort cellSource, ushort cellDestination, ushort regionSource, ushort regionDestination)
        {
            Min = min;
            Max = max;
            LineFlag = lineFlag;
            LineSource = lineSource;
            LineDestination = lineDestination;
            CellSourceSector = cellSource;
            CellDestinationSector = cellDestination;
            RegionSourceYX = regionSource;
            RegionDestinationYX = regionDestination;
        }

        public IPk2Types ToType => this;

        public bool Equals(NavCell other)
        {
            return Min.Equals(other.Min) && Max.Equals(other.Max);
        }

        public Neighbour GetNeighbour(ushort fromSectorYX)
        {
            if (fromSectorYX == RegionSourceYX)
            {
                return new Neighbour(RegionDestinationYX, CellDestinationSector);
            }
            else
            {
                return new Neighbour(RegionSourceYX, CellSourceSector);
            }
        }

        public bool Has_Neighbour()
        {
            if ((CellSourceSector == ushort.MaxValue) || (CellDestinationSector == ushort.MaxValue))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
