﻿namespace LobotAPI.Scripting.NavMesh.Structs
{
    internal struct Node
    {
        public NodeType NodeType;
        public ushort ZoneIndex;
        public ushort Index;
        public ushort SectorYX;

        public uint H, G;
        public ulong Parent;

        public ulong GetID()
        {
            return (ulong)(ZoneIndex) << 40 | (ulong)(Index) << 24 | (ulong)(NodeType) << 16 | SectorYX;
        }
        public byte GetSectorX()
        {
            return (byte)(0xFF & SectorYX);
        }
        public byte GetSectorY()
        {
            return (byte)(SectorYX >> 8);
        }
    }
}
