﻿using LobotAPI.PK2;
using System;
using System.Windows;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    /// <summary>
    /// //NavigationCellLinks
    //4   uint cellLinkCount
    //for (int linkIndex = 0; linkIndex<cellLinkCount; linkIndex++)
    //{
    //    8   Vector2 Min
    //    8   Vector2 Max

    //    1   byte LineFlag
    //    1   byte LineSource
    //    1   byte LineDestination

    //    2   ushort CellSource
    //    2   ushort CellDestination
    //}
    /// </summary>
    [Serializable]
    public class NavCellLink : IPk2Types, IEquatable<NavCell>
    {
        public Point Min;
        public Point Max;

        public byte LineFlag;
        public byte LineSource;
        public byte LineDestination;
        public ushort CellSourceSector;
        public ushort CellDestinationSector;

        public NavCellLink(Point min, Point max, byte lineFlag, byte lineSource, byte lineDestination, ushort cellSource, ushort cellDestination)
        {
            Min = min;
            Max = max;
            LineFlag = lineFlag;
            LineSource = lineSource;
            LineDestination = lineDestination;
            CellSourceSector = cellSource;
            CellDestinationSector = cellDestination;
        }

        public IPk2Types ToType => this;

        public bool Equals(NavCell other)
        {
            return Min.Equals(other.Min) && Max.Equals(other.Max);
        }

        public ushort GetNeighbour(ushort fromIndex)
        {
            if (CellSourceSector == fromIndex)
            {
                return CellDestinationSector;
            }
            else
            {
                return CellSourceSector;
            }
        }
        public bool Has_Neighbour()
        {
            if ((CellSourceSector == ushort.MaxValue) | (CellDestinationSector == ushort.MaxValue))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
