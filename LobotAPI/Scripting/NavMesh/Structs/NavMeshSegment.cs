﻿using LobotAPI.PK2;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LobotAPI.Scripting.NavMesh.Structs
{
    /// <summary>
    /// A parsed navigation file (.nvm).
    /// </summary>
    [Serializable]
    public class NavMeshSegment : IPk2Types, IEquatable<NavMeshSegment>
    {
        public readonly string NavFile;
        public readonly ushort SectorX;
        public readonly ushort SectorY;
        private static string SectorPattern = @"^nv_([0-9a-fA-F]{2})([0-9a-fA-F]{2})\.nvm$"; //filter the 4 sector identifiers

        //Parameterless constructor for the serializer
        private NavMeshSegment() { }

        public NavMeshSegment(string navFile)
        {
            NavFile = navFile;

            MatchCollection matches = Regex.Matches(NavFile, SectorPattern);
            SectorX = ushort.Parse(matches[0].Groups[2].Value, System.Globalization.NumberStyles.HexNumber);
            SectorY = ushort.Parse(matches[0].Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
        }

        public NavMeshSegment(byte SectorX, byte SectorY)
        {
            this.SectorX = SectorX;
            this.SectorY = SectorY;
            //this.LoadNVM();
        }

        public List<NavObject> NavigationObjects { get; set; }
        public List<NavCell> NavigationCells { get; set; }
        public List<NavBorder> NavigationBorders { get; set; }
        public List<NavCellLink> NavigationCellLinks { get; set; }

        public IPk2Types ToType => this;

        //float[] TextureMap = new float[96][96];
        public float[] HeightMap = new float[9409];
        //byte[] last3 = new byte[36];
        //float[] last4 = new float[36];

        public bool Equals(NavMeshSegment other)
        {
            return NavFile.Equals(other.NavFile);
        }
    }
}
