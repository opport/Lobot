﻿using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.Windows;

namespace LobotAPI.Scripting
{
    /// <summary>
    /// The constant values for towns.
    /// </summary>
    //public enum Town : int
    //{
    //    None = -1,
    //    Jangan = 0,
    //    Donwang = 1,
    //    Hotan = 2,
    //    Samarkand = 3,
    //    Constantinople = 4,
    //    AlexNorth = 5,
    //    AlexSouth = 6
    //}

    /// <summary>
    /// The general functions to get the ScriptExecutor running.
    /// </summary>
    public sealed class ScriptUtils
    {
        /// <summary>
        /// Calculates the distance between two points.
        /// </summary>
        /// <param name="point1">First point</param>
        /// <param name="point2">Second point</param>
        /// <returns>Distance between points</returns>
        public static double GetDistance(Point point1, Point point2)
        {
            return point1 == point2 ? 0 : Convert.ToDouble(Math.Sqrt(Math.Abs((point1.X - point2.X) * (point1.X - point2.X) + (point1.Y - point2.Y) * (point1.Y - point2.Y))));
        }

        /// <summary>
        /// Calculates the distance between two points.
        /// </summary>
        /// <param name="point1">First point</param>
        /// <param name="x">X-coordinate of second point</param>
        /// <param name="y">Y-coordinate of second point</param>
        /// <returns>Distance between points</returns>
        public static double GetDistance(Point point1, double x, double y)
        {
            return GetDistance(point1, new Point(x, y));
        }

        /// <summary>
        /// Calculates the distance between two points by their coordinates.
        /// </summary>
        /// <param name="a">First point</param>
        /// <param name="x1">X-coordinate of first point</param>
        /// <param name="y1">Y-coordinate of first point</param>
        /// <param name="x2">X-coordinate of second point</param>
        /// <param name="y2">Y-coordinate of second point</param>
        /// <returns>Distance between points</returns>
        public static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return GetDistance(new Point(x1, y1), new Point(x2, y2));
        }

        /// <summary>
        /// Calculates the distance between two points by their go commands.
        /// </summary>
        /// <param name="point1">First point</param>
        /// <param name="point2">second point</param>
        /// <returns>Distance between points</returns>
        public static double GetDistance(CommandExpression point1, CommandExpression point2)
        {
            return GetDistance(new Point(Convert.ToDouble(point1.Arguments[0]), Convert.ToDouble(point1.Arguments[1])),
                new Point(Convert.ToDouble(point2.Arguments[0]), Convert.ToDouble(point2.Arguments[1])));
        }

        /// <summary>
        /// Calculates distance to from bot position to area.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static double GetCharDistanceToArea(Area a)
        {
            return GetDistance(Bot.CharData.Position, a.GetCenterPoint()) - a.Radius;
        }

        /// <summary>
        /// Calculates distance from given position to area.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public static double GetDistanceToArea(Area a, Point p)
        {
            return GetDistance(p, a.GetCenterPoint()) - a.Radius;
        }

        public static double DistanceToTrainingArea1 { get { return GetCharDistanceToArea(Settings.Instance.Training.TrainingArea1); } }
        public static double DistanceToTrainingArea2 { get { return GetCharDistanceToArea(Settings.Instance.Training.TrainingArea2); } }
        public static double DistanceToClosestArea { get { return GetCharDistanceToArea(ClosestAreaToChar); } }

        public static bool IsCharInArea1 { get { return ScriptUtils.DistanceToTrainingArea1 <= Settings.Instance.Training.TrainingArea1.Radius; } }
        public static bool IsCharInArea2 { get { return ScriptUtils.DistanceToTrainingArea2 <= Settings.Instance.Training.TrainingArea2.Radius; } }

        public static bool IsPointInAnyArea(Point p)
        {
            return ScriptUtils.DistanceToTrainingArea1 <= Settings.Instance.Training.TrainingArea1.Radius || ScriptUtils.DistanceToTrainingArea2 <= Settings.Instance.Training.TrainingArea2.Radius;
        }

        public static bool IsPointInClosestArea(Point p) { return GetDistanceToArea(ClosestAreaToChar, p) <= ClosestAreaToChar.Radius; }

        public static Area ClosestAreaToChar
        {
            get
            {
                return ScriptUtils.GetCharDistanceToArea(Settings.Instance.Training.TrainingArea1) < ScriptUtils.GetCharDistanceToArea(Settings.Instance.Training.TrainingArea2)
                        ? Settings.Instance.Training.TrainingArea1
                        : Settings.Instance.Training.TrainingArea2;
            }
        }

        public static Area GetClosestAreaToPoint(Point p)
        {
            return ScriptUtils.GetDistanceToArea(Settings.Instance.Training.TrainingArea1, p) < ScriptUtils.GetDistanceToArea(Settings.Instance.Training.TrainingArea2, p)
                    ? Settings.Instance.Training.TrainingArea1
                    : Settings.Instance.Training.TrainingArea2;
        }

        public static Point GeneratePointInArea1()
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int angle = random.Next(360);
            int x = (int)(Settings.Instance.Training.TrainingArea1.X + Settings.Instance.Training.TrainingArea1.RadiusWithoutEdge * Math.Cos(angle));
            int y = (int)(Settings.Instance.Training.TrainingArea1.Y + Settings.Instance.Training.TrainingArea1.RadiusWithoutEdge * Math.Sin(angle));

            return new Point(x, y);
        }

        public static Point GeneratePointInArea2()
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            int angle = random.Next(360);
            int x = (int)(Settings.Instance.Training.TrainingArea2.X + Settings.Instance.Training.TrainingArea2.RadiusWithoutEdge * Math.Cos(angle));
            int y = (int)(Settings.Instance.Training.TrainingArea2.Y + Settings.Instance.Training.TrainingArea2.RadiusWithoutEdge * Math.Sin(angle));

            return new Point(x, y);
        }

        public static Point GetWalkPointWithinClosestArea()
        {
            return DistanceToTrainingArea1 < DistanceToTrainingArea2
                ? GeneratePointInArea1()
                : GeneratePointInArea2();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="point1">Origin</param>
        /// <param name="point2">Destination</param>
        /// <returns>The time required to travel from the origin to the destination point in seconds</returns>
        /// <seealso href="https://www.elitepvpers.com/forum/sro-coding-corner/1702587-incremental-walking-position-update-coordinate-need-help-three-questions.html"/>
        public static double GetWalkTime(double x1, double y1, double x2, double y2)
        {
            double distance = GetDistance(x1, y1, x2, y2);

            return distance / (Bot.CharData.CurrentSpeed * 0.1);
        }
        public static double GetWalkTime(Point point1, Point point2)
        {
            return GetWalkTime(point1.X, point1.Y, point2.X, point2.Y);
        }
        public static double GetWalkTime(Position point1, Position point2)
        {
            return GetWalkTime(point1.X, point1.Y, point2.X, point2.Y);
        }

        /// <summary>
        /// Get the first walk point.
        /// </summary>
        /// <param name="script">Script to be evaluated</param>
        /// <returns>The first walk point</returns>
        public static Point GetFirstWalkPoint(List<AbstractExpression> script)
        {
            CommandExpression command;

            foreach (AbstractExpression expr in script)
            {
                if (expr.GetType().Equals(typeof(CommandExpression)))
                {
                    command = (CommandExpression)expr;

                    if (command.CommandName.Equals("go"))
                    {
                        return new Point(Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
                    }
                }
            }
            throw new NullReferenceException("No walk point found.");
        }

        /// <summary>
        /// Get the index of the first walk point.
        /// </summary>
        /// <param name="script">Script to be evaluated</param>
        /// <returns>The index of the first walk point</returns>
        public static int GetFirstWalkPointIndex(List<AbstractExpression> script)
        {
            CommandExpression command;

            for (int i = 0; i < script.Count; i++)
            {
                if (script[i].GetType().Equals(typeof(CommandExpression)))
                {
                    command = (CommandExpression)script[i];

                    if (command.CommandName.Equals("go"))
                    {
                        return i;
                    }
                }
            }
            throw new NullReferenceException("No walk point found.");
        }

        public static double TotalDistance(List<AbstractExpression> script)
        {
            double totalDistance = 0;

            if (script.Count == 0 || script == null)
            {
                return totalDistance;
            }

            int currentPointIndex = 0;
            CommandExpression previousCommand = null;
            CommandExpression currentCommand;

            // Find first movement command
            while (previousCommand == null)
            {
                if (script[currentPointIndex].GetType().Equals(typeof(CommandExpression)))
                {
                    currentCommand = (CommandExpression)script[currentPointIndex];

                    if (currentCommand.CommandName.Equals("go"))
                    {
                        previousCommand = (CommandExpression)script[currentPointIndex];
                    }
                }
                else
                {
                    currentPointIndex++;
                }
            }

            // Calculate distance between all points
            for (int i = currentPointIndex; i < script.Count; i++)
            {
                if (script[i].GetType().Equals(typeof(CommandExpression)))
                {
                    currentCommand = (CommandExpression)script[i];

                    if (currentCommand.CommandName.Equals("go"))
                    {
                        totalDistance += GetDistance(previousCommand, currentCommand);
                        previousCommand = currentCommand;
                    }
                }
            }

            return totalDistance;
        }

        /// <summary>
        /// Check the list of towns to see whether a given point is within any of their radii.
        /// </summary>
        /// <param name="coord">Coordinates to evaluate for inclusion.</param>
        /// <returns>The town's enum value, 0 for no town.</returns>
        public static Town CurrentTown(Point coord)
        {
            foreach (KeyValuePair<string, Town> entry in ScriptGlobal.Towns)
            {
                if (entry.Value.IsInTown(coord))
                {
                    return entry.Value;
                }
            }

            return null;
        }

        /// <summary>
        /// Calculate x offset to send coordinate with 7021 packet.
        /// </summary>
        /// <param name="point">Point with normal coordinate </param>
        /// <returns>Tuple of the calculated X Offset with its Sector</returns>
        public static Tuple<int, byte> GetXOffset(int x)
        {
            byte xSec = GetSectorX(x);
            int xOffset = (Bot.CharData.IsInCave)
                ? xOffset = (x - ((xSec - 135) * 192)) * 10
                : xOffset = (ushort)((x - ((xSec - 135) * 192)) * 10);

            return new Tuple<int, byte>(xOffset, xSec);
        }

        /// <summary>
        /// Calculate x offset to send coordinate with 7021 packet.
        /// </summary>
        /// <param name="point">Point with normal coordinate </param>
        /// <returns>Tuple of the calculated Y Offset with its Sector</returns>
        public static Tuple<int, byte> GetYOffset(int y)
        {
            byte ySec;
            int yOffset;

            if ((Bot.CharData.IsInCave))
            {
                ySec = 0x80;
                yOffset = (y - ((ySec - 92) * 192)) * 10;
            }
            else
            {
                ySec = GetSectorY(y);
                yOffset = (ushort)((y - ((ySec - 92) * 192)) * 10);
            }

            return new Tuple<int, byte>(yOffset, ySec);
        }

        /// <summary>
        /// Calculate the x-coordinate using the x-sector in movement response packet B021.
        /// </summary>
        /// <see cref="CharMoveHandler"/>
        /// <param name="x">The x-offset information</param>
        /// <returns>The calculated x-coordinate</returns>
        public static int GetXCoord(float xOffset, byte xSector, int offsetDivisor = 10)
        {
            return (int)((xSector - 135) * 192 + (xOffset / offsetDivisor));
        }

        /// <summary>
        /// Calculate the y- coordinate using the X sector in movement response packet B021.
        /// </summary>
        /// <see cref="CharMoveHandler"/>
        /// <param name="yOffset">The y-offset information</param>
        /// <returns>The calculated y-coordinate</returns>
        public static int GetYCoord(float yOffset, byte ySector, int offsetDivisor = 10)
        {
            return (int)((ySector - 92) * 192 + (yOffset / offsetDivisor));
        }

        /// <summary>
        /// Calculate the origin x-coordinate using the x-sector in movement response packet B021.
        /// </summary>
        /// <see cref="CharMoveHandler"/>
        /// <param name="x">The origin x-offset information</param>
        /// <returns>The calculated origin x-coordinate</returns>
        public static int GetXCoordOrigin(ushort xOffset, byte xSector)
        {
            return GetXCoord(xOffset, xSector, 100);
        }

        /// <summary>
        /// Calculate the origin y-coordinate using the X sector in movement response packet B021.
        /// </summary>
        /// <see cref="CharMoveHandler"/>
        /// <param name="yOffset">The origin y-offset information</param>
        /// <returns>The calculated origin y-coordinate</returns>
        public static int GetYCoordOrigin(ushort yOffset, byte ySector)
        {
            return GetYCoord(yOffset, ySector, 100);
        }

        /// <summary>
        /// Get a walk packet sector to calculate the character location.
        /// </summary>
        /// <param name="x">The y-sector information</param>
        /// <returns>The calculated x-sector</returns>
        /// <see cref="LobotDLL.Commands.CharMove"/>
        public static byte GetSectorX(int x)
        {
            return (byte)Math.Floor((double)x / 192 + 135);
        }

        /// <summary>
        /// Get a walk packet sector to calculate the character location.
        /// </summary>
        /// <param name="y">The y-sector information</param>
        /// <returns>The calculated y-sector</returns>
        /// /// <see cref="LobotDLL.Commands.CharMove"/>
        public static byte GetSectorY(int y)
        {
            return (byte)Math.Floor((double)y / 192 + 92);
        }

        /// <summary>
        /// Goes through the script and tries to find closest point to start at.
        /// </summary>
        /// <param name="script">Script to be evaluated</param>
        /// <returns>0-n for valid point, -1 if distance is too large or character is in town.</returns>
        public static int IndexOfClosestPoint(Point currentPosition, List<AbstractExpression> script)
        {
            if (script == null)
            {
                return -1;
            }

            CommandExpression command;
            Point commandPosition;

            double distance;
            double currentDistance = 1000;
            int currentIndex = -1;

            for (int i = 0; i < script.Count; i++)
            {
                if (script[i].GetType().Equals(typeof(CommandExpression)))
                {
                    command = (CommandExpression)script[i];

                    if (command.CommandName.Equals("go"))
                    {
                        commandPosition = new Point(Int32.Parse(command.Arguments[0]), Int32.Parse(command.Arguments[1]));
                        distance = GetDistance(currentPosition, commandPosition);

                        if (distance < currentDistance)
                        {
                            currentDistance = distance;
                            currentIndex = i;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            return (currentDistance <= ScriptGlobal.MAX_GO_DISTANCE) ? currentIndex : -1;
        }

        /// <summary>
        /// Returns the index of the point in a list with the shortest distance to a given position.
        /// </summary>
        /// <param name="currentPosition"></param>
        /// <param name="points"></param>
        /// <returns></returns>
        public static int IndexOfClosestPoint(Point currentPosition, List<Point> points)
        {
            double distance;
            double currentDistance = 1000;
            int currentIndex = -1;

            for (int i = 0; i < points.Count; i++)
            {
                distance = GetDistance(currentPosition, points[i]);

                if (distance < currentDistance)
                {
                    currentDistance = distance;
                    currentIndex = i;
                }
            }

            return currentIndex;
        }

        /// <summary>
        /// Check whether the script matches the MetaExpression.
        /// </summary>
        /// <param name="meta">Meta expression to be evaluated</param>
        /// <returns>Whether the script matches the MetaExpression.</returns>
        public static Boolean MatchesRequirement(MetaExpression meta, List<AbstractExpression> script)
        {
            switch (meta.ExpressionName)
            {
                case "StartingTown":
                    if (ScriptGlobal.Towns.TryGetValue(meta.Argument1.ToLower(), out Town value))
                    {
                        if (CurrentTown(GetFirstWalkPoint(script)).Name.Equals(value.Name))
                        {
                            return true;
                        }
                    }

                    return false;
                case "MonsterLevel":
                    //TODO compare internal monster list level to given MonsterLevel
                    return false;
                case "MonsterDifficulty":
                    //TODO find character's level difference (considering average set +) compared to monster level
                    return false;
                case "ScriptDifficulty":
                    //TODO find a way to determine monster levels of points that character has to run through
                    return false;
                case "NextScript":
                    //TODO search script folder for script with containing meta name
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static Boolean MatchesRequirements(List<AbstractExpression> script)
        {
            //TODO Enhancement
            //MetaExpression meta;


            //for (int i = 0; i < script.Count; i++)
            //{
            //    if (script[i].GetType().Equals(typeof(MetaExpression)))
            //    {
            //        meta = (MetaExpression)script[i];

            //        if (!MatchesRequirement(meta, script))
            //        {
            //            return false;
            //        }
            //    }
            //}

            return true;
        }
    }
}
