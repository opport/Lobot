﻿using Config.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LobotAPI.Scripting
{
    class AreaParser : ITypeParser
    {
        public IEnumerable<Type> SupportedTypes => new[] { typeof(Area) };

        public string ToRawString(object value)
        {
            if (value == null) return null;

            return value.ToString();
        }

        public bool TryParse(string value, Type t, out object result)
        {
            if (value == null)
            {
                result = null;
                return false;
            }

            if (t == typeof(Area))
            {
                string[] parts = value.Split(new char[] { ',' });
                Area area = new Area();
                area.X = Convert.ToInt32(parts[0]);
                area.Y = parts.Length > 1 ? Convert.ToInt32(parts[1]) : 0;
                area.Radius = parts.Length > 2 ? Convert.ToInt32(parts[2]) : 0;
                result = area;
                return true;
            }

            result = null;
            return false;
        }
    }

    public class AreaConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] parts = ((string)value).Split(new char[] { ',' });
                Area area = new Area();
                area.X = Convert.ToInt32(parts[0]);
                area.Y = parts.Length > 1 ? Convert.ToInt32(parts[1]) : 0;
                area.Radius = parts.Length > 2 ? Convert.ToInt32(parts[2]) : 0;
                return area;
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                Area area = value as Area;
                return string.Format("{0},{1},{2}", area.X, area.Y, area.Radius);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    [TypeConverter(typeof(AreaConverter))]
    [SettingsSerializeAs(SettingsSerializeAs.String)]
    public class Area : IEquatable<Area>, INotifyPropertyChanged
    {
        public enum AreaProperty
        {
            X = 0,
            Y = 1,
            R = 2
        }

        private double _radius = 0;
        private double _y = 0;
        private double _x = 0;

        public override string ToString() => string.Format("{0},{1},{2}", X, Y, Radius);

        [NonSerialized]
        public Dictionary<Point, HashSet<Point>> AdjacencyList = new Dictionary<Point, HashSet<Point>>();

        public double X
        {
            get => _x; set
            {
                if (_x != value)
                {
                    _x = value;
                    ResetAdjacencyList();
                    NotifyPropertyChanged();
                }
            }
        }
        public double Y
        {
            get => _y; set
            {
                if (_y != value)
                {
                    _y = value;
                    ResetAdjacencyList();
                    NotifyPropertyChanged();
                }
            }
        }
        public double Radius
        {
            get => _radius; set
            {
                if (_radius != value)
                {
                    _radius = value;
                    ResetAdjacencyList();
                    NotifyPropertyChanged();
                }
            }
        }
        public double RadiusWithoutEdge { get => _radius > 30 ? _radius - 15 : _radius; } // prevent moving right at edge and stay closer to middle
        private double PointDistance { get => RadiusWithoutEdge > 15 ? (RadiusWithoutEdge / 5) + 10 : _radius; } // prevent moving right at edge and stay closer to middle

        /// <summary>
        /// Empty constructor for settings initialization
        /// </summary>
        public Area() { }

        public Area(double x = 0, double y = 0, double radius = 0)
        {
            X = x;
            Y = y;
            Radius = radius;
            AddAdjacentPoints(GetCenterPoint(), GetCenterPoint());
        }

        public Area(Vector vector, double radius = 0) : this(vector.X, vector.Y, radius) { }

        public Area(Point point, double radius = 0) : this(point.X, point.Y, radius) { }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ushort GetSectorY()
        {
            return (ushort)(Math.Floor(Y / 192 + 92));
        }
        public ushort GetSectorX()
        {
            return (ushort)(Math.Floor(X / 192 + 135));
        }

        public int GetSectorXOffset()
        {
            int result = (int)X % 192;
            if (result < 0)
            {
                result += 192;
            }
            return result;
        }

        public int GetSectorYOffset()
        {
            int result = (int)Y % 192;
            if (result < 0)
            {
                result += 192;
            }
            return result;
        }

        public Point GetCenterPoint()
        {
            return new Point(X, Y);
        }

        private void ResetAdjacencyList()
        {
            AdjacencyList.Clear();
            AddAdjacentPoints(GetCenterPoint(), GetCenterPoint());
        }

        /// <summary>
        /// Get an adjecency list of points within the area radius in a flood fill 8 manner.
        /// The points make up a grid of points separated by a calculated point distance each.
        /// </summary>
        /// <returns></returns>
        private void AddAdjacentPoints(Point prev, Point curr)
        {
            //Only continue if current point is within radius
            if (ScriptUtils.GetDistance(curr, X, Y) <= RadiusWithoutEdge)
            {
                // Recurse if not yet in adjacency list
                if (!AdjacencyList.ContainsKey(curr))
                {
                    // Add entry with reference to previous point
                    AdjacencyList.Add(curr, new HashSet<Point>());

                    // Prevent self reference in adjacency list
                    if (prev != curr)
                    {
                        AdjacencyList[curr].Add(prev);
                    }

                    AddAdjacentPoints(curr, new Point(curr.X, curr.Y - PointDistance)); // down
                    AddAdjacentPoints(curr, new Point(curr.X, curr.Y + PointDistance)); // up
                    AddAdjacentPoints(curr, new Point(curr.X - PointDistance, curr.Y)); // left
                    AddAdjacentPoints(curr, new Point(curr.X + PointDistance, curr.Y)); // right

                    AddAdjacentPoints(curr, new Point(curr.X - PointDistance, curr.Y - PointDistance)); // bottom left
                    AddAdjacentPoints(curr, new Point(curr.X - PointDistance, curr.Y + PointDistance)); // top left
                    AddAdjacentPoints(curr, new Point(curr.X - PointDistance, curr.Y + PointDistance)); // bottom right
                    AddAdjacentPoints(curr, new Point(curr.X + PointDistance, curr.Y + PointDistance)); // top right
                }

                // Add to previous point if points are not the same
                if (prev != curr)
                {
                    AdjacencyList[prev].Add(curr);
                }
            }
            return;
        }

        public bool Equals(Area other)
        {
            return X == other.X && Y == other.Y && Radius == other.Radius;
        }
    }
}
