﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace LobotAPI.Scripting
{
    /// <summary>
    /// Class used to parse script files.
    /// </summary>
    public static class ExpressionParser
    {

        /// <summary>
        /// Parse loop for lines in file.
        /// </summary>
        /// <param name="file">Lines to be parsed</param>
        /// <returns></returns>
        private static List<AbstractExpression> ParseLines(string[] lines)
        {
            List<AbstractExpression> commandList = new List<AbstractExpression>();

            foreach (string line in lines)
            {
                if (Regex.IsMatch(line, Patterns.COMMENT) || line == "")
                {
                    continue;
                }
                else if (Regex.IsMatch(line, Patterns.META_EXPR))
                {
                    commandList.Add(new MetaExpression(line));
                }
                else if (Regex.IsMatch(line, Patterns.COMMAND_EXPR))
                {
                    commandList.Add(new CommandExpression(line));
                }
                else
                {
                    throw new FormatException("Invalid line in script: " + line);
                }
            }
            return commandList;
        }

        /// <summary>
        /// Main parsing function for script in file.
        /// </summary>
        /// <param name="filePath">File to be parsed.</param>
        /// <returns>Parsed commands from file.</returns>
        public static List<AbstractExpression> ParseScript(string filePath)
        {
            return string.IsNullOrEmpty(filePath) ? null : ParseLines(File.ReadAllLines(filePath));
        }

        /// <summary>
        /// Main parsing function for script as a string array.
        /// </summary>
        /// <param name="filePath">File to be parsed.</param>
        /// <returns>Parsed commands from file.</returns>
        public static List<AbstractExpression> ParseScript(string[] file)
        {
            try
            {
                return ParseLines(file);
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.SCRIPT, e.Message);
                Console.WriteLine(e.Message);
                throw;
            }
        }
    }
}
