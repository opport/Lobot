﻿namespace LobotAPI.Scripting
{
    /// <summary>
    /// The patterns used to identify valid expressions for a script.
    /// </summary>
    sealed internal class Patterns
    {
        internal const string TELEPORTMODE = "\\b((\\s*\\d+\\s*,\\s*\\d+\\s*)" +     //NPC_ID,TeleportOption
                            "|reverseRecall" +
                            "|reverseDeath" +
                            "|reverseLocation\\s+location\\b)";
        internal const string COMMAND = "\\b(go (?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*)(\\s*\\s*$|,\\d+\\s*))" +
                            "|wait\\b(\\s*|\\s+\\d*\\s*)$" +
                            "|teleport\\s+" + TELEPORTMODE +
                            "|quest\\s+(?x)((\\s*\\d+\\s*),(\\s*\\d+\\s*))" +
                            "|store\\s*$" +      // stash items
                            "|stable\\s*$" +     // buy COSitems
                            "|repair\\s*$" +      // repair items at the weapons trader
                            "|potions\\s*$" +      // buy potions
                            "|special\\s*$" +      // buy special items
                            "|mount\\s*$" +
                            "|dismount\\s*$" +
                            "|setArea1\\s+(?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*),(\\s*\\d+\\s*))" +
                            "|setArea2\\s+(?x)((\\s*(-)?\\d+\\s*),(\\s*(-)?\\d+\\s*),(\\s*\\d+\\s*)))";
        internal const string TOWN = "\\b(?i)(Jangan" +
                            "|Donwang " +
                            "|Hotan" +
                            "|Samarkand" +
                            "|Constantinople" +
                            "|Alexandria(\\s+(North|South))?)\\b";
        internal const string META_EXPR = "\\b(StartingTown\\s*" + TOWN +
                            "|MonsterLevel\\s+\\b(1|[0-9]?[0-9]|100)\\b" +
                            "|MonsterDifficulty\\s+\\b([1-9]|10)\\b" +
                            "|ScriptDifficulty\\s+\\b([1-9]|10)\\b" +
                            "|NextScript\\s+\\b([01]?[0-9]|100)\\b(?x)(\\b(\\s*,\\s*[0-9]|10)?\\b))" + "\\s*($|" + COMMENT + ")"; //MonsterLevel(,MonsterDifficulty)?;
        internal const string COMMENT = "\\s*//(\\s*)(.)*$";
        internal const string COMMAND_EXPR = COMMAND + "\\s*($|" + COMMENT + ")" +
                            "|" + COMMENT;
        internal const string LINE_EXPR = META_EXPR +
                            "|" + COMMAND_EXPR +
                            "|(\\s)*";
    }
}