﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LobotAPI.Scripting
{
    /// <summary>
    /// A script expression that can be used to execute an action inside the game.
    /// </summary>
    public class CommandExpression : AbstractExpression
    {
        public string CommandName { get; private set; }
        public string[] Arguments { get; private set; }

        public CommandExpression(string expression)
        {
            if (Regex.IsMatch(expression, Patterns.COMMAND_EXPR))
            {
                EvaluateExpression(expression);
            }
            else
            {
                throw new FormatException("Invalid CMD_EXPR: " + expression);
            }
        }

        public CommandExpression(string command, string[] args)
        {
            CommandName = command;
            Arguments = args;
        }

        /// <summary>
        /// Glean information from the token given in the constructor.
        /// </summary>
        /// <param name="expression"></param>
        private void EvaluateExpression(string expression)
        {
            // remove trailing comments
            string[] splitToken = expression.Split(new string[] { "//" }, 2, StringSplitOptions.RemoveEmptyEntries);
            splitToken = splitToken[0].Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

            CommandName = splitToken[0];
            Arguments = new string[splitToken.Length - 1];

            for (int i = 1; i < splitToken.Length; i++)
            {
                Arguments[i - 1] = splitToken[i];
            }
        }

        public override bool Equals(object other)
        {
            CommandExpression other1 = (CommandExpression)other;
            return (Enumerable.SequenceEqual(this.Arguments, other1.Arguments)
                && this.CommandName == other1.CommandName);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(CommandName);

            for (int i = 0; i < Arguments.Length; i++)
            {
                if (i == 0)
                {
                    if (String.IsNullOrEmpty(Arguments[i]) == false)
                    {
                        sb.Append(" " + Arguments[i]);
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(Arguments[i]) == false)
                    {
                        sb.Append(", " + Arguments[i]);
                    }
                }
            }

            return sb.ToString();
        }
    }
}
