﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotAPI.Scripting
{
    public enum AreaType : byte
    {
        None = 0x0,
        AreaOne = 0x01,
        AreaTwo = 0x02
    }

    public class AreaChangedArgs : EventArgs
    {        
        public AreaType AreaNumber;

        public AreaChangedArgs(AreaType areaNumber)
        {
            AreaNumber = areaNumber;
        }
    }
}
