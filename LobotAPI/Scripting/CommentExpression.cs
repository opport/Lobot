﻿using System;
using System.Text.RegularExpressions;

namespace LobotAPI.Scripting
{
    /// <summary>
    /// A script expression used to add information to the script that is not computed.
    /// </summary>
    public class CommentExpression : AbstractExpression
    {
        public string Comment { get; private set; }

        public CommentExpression(string token)
        {
            if (Regex.IsMatch(token, Patterns.COMMENT))
            {
                Comment = token;
            }
            else
            {
                throw new FormatException("Invalid comment: " + token);
            }
        }

        public override bool Equals(object other)
        {
            CommentExpression other1 = (CommentExpression)other;
            return this.Comment == other1.Comment;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return Comment;
        }
    }
}
