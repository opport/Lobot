﻿using LobotAPI.Globals;
using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace LobotAPI.Connection
{
    public class Client
    {
        const int MAX_RECV_SIZE = 8192;

        public delegate void PacketReceivedEventHandler(Packet p);
        public event PacketReceivedEventHandler HandlePacketReceived;
        public delegate void ConnectedEventHandler();
        public event ConnectedEventHandler HandleConnected;
        public delegate void DisconnectedEventHandler();
        public event DisconnectedEventHandler HandleDisconnected;

        Socket LocalListener;
        Socket LocalSocket;

        Security LocalSecurity;

        TransferBuffer LocalRecvBuffer;
        List<Packet> LocalRecvPackets;
        List<KeyValuePair<TransferBuffer, Packet>> LocalSendBuffers;

        Thread ThreadPacketProcessor;
        bool IsClosing;
        bool DoPacketProcess;


        //Listen for ClientConnection on port
        public void Listen(ushort port)
        {
            LocalSecurity = new Security();
            LocalSecurity.GenerateSecurity(true, true, true);

            LocalRecvBuffer = new TransferBuffer(MAX_RECV_SIZE, 0, 0);
            LocalListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            LocalSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                if (LocalListener.IsBound == false)
                {
                    LocalListener.Bind(new IPEndPoint(IPAddress.Loopback, port));
                    LocalListener.Listen(1);
                }
                LocalListener.BeginAccept(new AsyncCallback(OnClientConnect), null);
                Console.WriteLine($"Listening for Client on {NetworkGlobal.PROXY_ADDRESS}:{port}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Log(LogLevel.NETWORK, $"Proxy Listener Exception: {ex.Message}");
            }
        }

        private void Listen()
        {
            DoPacketProcess = false;

            if (LocalSocket != null)
            {
                LocalSocket.Shutdown(SocketShutdown.Both);
                LocalSocket.Close();
            }

            LocalSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Accept a new Client.
            LocalListener.BeginAccept(new AsyncCallback(OnClientConnect), null);
        }

        public void Shutdown()
        {
            DoPacketProcess = false;
            IsClosing = true;

            ThreadPacketProcessor.Join();

            //Close Socket
            if (LocalSocket != null)
            {
                LocalSocket.Shutdown(SocketShutdown.Both);
                LocalSocket.Close();
            }
            LocalSocket = null;

            //Close listener
            if (LocalListener != null)
            {
                LocalListener.Close();
                LocalListener = null;
            }

            LocalSecurity = null;

            LocalRecvBuffer = null;
            LocalRecvPackets = null;
            LocalSendBuffers = null;

            if (ThreadPacketProcessor != null)
            {
                ThreadPacketProcessor = null;
            }
        }

        public void Disconnect()
        {
            DoPacketProcess = false;
            IsClosing = true;

            if (LocalSocket != null)
            {
                if (LocalSocket.Connected)
                {
                    LocalSocket.Shutdown(SocketShutdown.Both);
                }
                LocalSocket.Close();
                LocalSocket = null;
            }

            IsClosing = false;
            HandleDisconnected?.Invoke();

            Listen();
        }

        private void OnClientConnect(IAsyncResult ar)
        {
            if (!IsClosing)
            {
                try
                {
                    DoPacketProcess = true;
                    LocalSocket = LocalListener.EndAccept(ar);
                    LocalSocket.BeginReceive(LocalRecvBuffer.Buffer, 0, MAX_RECV_SIZE, SocketFlags.None, new AsyncCallback(WaitForData), LocalSocket);

                    LocalSecurity = new Security();
                    LocalSecurity.GenerateSecurity(true, true, true);

                    //Thread Management
                    if (ThreadPacketProcessor == null)
                    {
                        ThreadPacketProcessor = new Thread(ThreadedPacketProcessing);
                        ThreadPacketProcessor.Name = "Proxy.Network.Client.PacketProcessor";
                        ThreadPacketProcessor.IsBackground = true;
                        ThreadPacketProcessor.Start();
                    }
                    //RaiseEvent if event is linked
                    if (HandleConnected != null)
                    {
                        HandleConnected();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(LogLevel.NETWORK, $"Network.Client.OnClientConnect: {ex.Message}");
                    throw (new Exception("Network.Client.OnClientConnect: " + ex.Message, ex));
                }
            }
        }

        private void WaitForData(IAsyncResult ar)
        {
            if (!IsClosing && DoPacketProcess)
            {
                Socket worker = null;
                try
                {
                    worker = (Socket)ar.AsyncState;
                    int rcvdBytes = worker.EndReceive(ar);
                    if (rcvdBytes > 0)
                    {
                        LocalRecvBuffer.Size = rcvdBytes;
                        LocalSecurity.Recv(LocalRecvBuffer);
                    }
                    else
                    {
                        //Raise Event
                        HandleDisconnected?.Invoke();
                        Listen();
                    }
                }
                catch (SocketException se)
                {
                    if (se.SocketErrorCode == SocketError.ConnectionReset) //Client Disconnected
                    {
                        //Raise Event
                        HandleDisconnected?.Invoke();
                        Listen();

                        //Mark worker as null to stop receiving.
                        worker = null;
                    }
                    else
                    {
                        Logger.Log(LogLevel.NETWORK, $"Proxy.Network.Client.WaitForData: {se.Message}");
                        throw (new Exception("Proxy.Network.Client.WaitForData: " + se.Message, se));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Log(LogLevel.NETWORK, $"Proxy.Network.Client.WaitForData: {ex.Message}");
                    throw (new Exception("Proxy.Network.Client.WaitForData: " + ex.Message, ex));
                }
                finally
                {
                    if (worker != null)
                    {
                        worker.BeginReceive(LocalRecvBuffer.Buffer, 0, MAX_RECV_SIZE, SocketFlags.None, new AsyncCallback(WaitForData), worker);
                    }
                }
            }
        }

        private void Send(byte[] buffer)
        {
            if (!IsClosing && DoPacketProcess) //if not closing and also packetprocessing
            {
                if (LocalSocket.Connected)
                {
                    try
                    {
                        LocalSocket.Send(buffer);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Logger.Log(LogLevel.NETWORK, $"Send Error: {ex.Message}");
                    }
                }
            }
        }
        public void Send(Packet packet)
        {
            LocalSecurity.Send(packet);
        }

        private void ThreadedPacketProcessing()
        {
            begin:

            //Wait until we should process packets
            while (!DoPacketProcess && !IsClosing)
            {
                Thread.Sleep(1);
            }

            while (DoPacketProcess && !IsClosing)
            {
                ProcessClientPackets();
                Thread.Sleep(1);
            }

            if (IsClosing)
            {
                return; //Jump out.
            }

            goto begin; //goto begin and wait until we should process again
        }
        private void ProcessClientPackets()
        {
            if (!IsClosing && DoPacketProcess)
            {
                LocalRecvPackets = LocalSecurity.TransferIncoming();
                if (LocalRecvPackets != null)
                {
                    foreach (var packet in LocalRecvPackets)
                    {
                        Logger.ProxyOutput(LogLevel.NETWORK, "C->P", packet);

                        if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000 || packet.Opcode == 0x2001)
                        //if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000)
                        {
                            continue;
                        }

                        //Send to PacketHandler
                        HandlePacketReceived?.Invoke(packet);
                    }
                }

                LocalSendBuffers = LocalSecurity.TransferOutgoing();
                if (LocalSendBuffers != null)
                {
                    foreach (var buffer in LocalSendBuffers)
                    {
                        Logger.ProxyOutput(LogLevel.NETWORK, "P->C", buffer.Value);
                        Send(buffer.Key.Buffer);
                    }
                }
            }
        }
    }
}
