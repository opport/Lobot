﻿using SilkroadSecurityApi;

namespace LobotAPI.Connection
{
    public enum ShardStatus : byte
    {
        Online = 0,
        Check = 1
    }
    public class Shard
    {
        public Shard(Packet packet)
        {
            ID = packet.ReadUInt16();
            Name = packet.ReadAscii();
            Players = packet.ReadUInt16();
            Capacity = packet.ReadUInt16();
            Status = (ShardStatus)packet.ReadInt8();
            packet.ReadUInt8(); //GlobalOperationID
        }

        public ushort ID { get; set; }
        public string Name { get; set; }
        public ushort Players { get; set; }
        public ushort Capacity { get; set; }

        public ShardStatus Status { get; set; }

        public override string ToString()
        {
            return $"{ID} - {Name},{Status}";
        }
    }
}
