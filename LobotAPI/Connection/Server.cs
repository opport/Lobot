﻿using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;

namespace LobotAPI.Connection
{
    public class Server
    {
        const int MAX_RECVSIZE = 8192;

        public delegate void ConnectedEventHandler(string ip, ushort port);
        public event ConnectedEventHandler HandleConnected;
        public delegate void DisconnectedEventHandler();
        public event DisconnectedEventHandler HandleDisconnected;

        // Not sure about this
        public delegate void KickedEventHandler();
        public event KickedEventHandler HandleKicked;

        public delegate void PacketReceivedEventHandler(Packet packet);
        public event PacketReceivedEventHandler HandlePacketReceived;

        Socket RemoteSocket;

        Security RemoteSecurity;

        //Used for Transfare and Processing
        TransferBuffer RemoteRecvBuffer;
        List<Packet> RemoteRecvPackets;
        List<KeyValuePair<TransferBuffer, Packet>> RemoteSendBuffers;

        Thread thPacketProcessor;

        //Used to Provide secure connect/disconnect while changing from Gateway to Agent
        bool isClosing;
        bool doPacketProcess;

        DateTime lastPacket;

        public void Connect(string ip, ushort port)
        {
            if (RemoteSocket != null)
            {
                Disconnect();
            }

            //Create objects 
            RemoteRecvBuffer = new TransferBuffer(MAX_RECVSIZE, 0, 0);
            RemoteSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //Thread Management
            if (thPacketProcessor == null)
            {
                thPacketProcessor = new Thread(ThreadedPacketProcessing);
                thPacketProcessor.Name = "Proxy.Network.Server.PacketProcessor";
                thPacketProcessor.IsBackground = true;
                thPacketProcessor.Start();
            }

            try
            {
                //Recreate the Security
                RemoteSecurity = new Security();

                //Connect
                RemoteSocket.Connect(ip, port);

                if (RemoteSocket.Connected)
                {
                    HandleConnected?.Invoke(ip, port);
                    doPacketProcess = true;
                    RemoteSocket.BeginReceive(RemoteRecvBuffer.Buffer, 0, MAX_RECVSIZE, SocketFlags.None, new AsyncCallback(WaitForServerData), RemoteSocket);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Log(LogLevel.NETWORK, $"Agent connection error: {ex.Message}");
            }
        }

        public void Disconnect()
        {
            doPacketProcess = false;
            isClosing = true;

            if (RemoteSocket != null)
            {
                if (RemoteSocket.Connected)
                {
                    RemoteSocket.Shutdown(SocketShutdown.Both);
                }
                RemoteSocket.Close();
                RemoteSocket = null;
            }

            isClosing = false;
            HandleDisconnected?.Invoke();
        }

        private void WaitForServerData(IAsyncResult ar)
        {
            if (!isClosing && doPacketProcess) //if not closing and also packetprocessing
            {
                Socket worker = null;
                try
                {
                    worker = (Socket)ar.AsyncState;
                    int rcvdBytes = worker.EndReceive(ar);
                    if (rcvdBytes > 0)
                    {
                        RemoteRecvBuffer.Size = rcvdBytes;
                        RemoteSecurity.Recv(RemoteRecvBuffer);
                    }
                    else
                    {
                        //RaiseEvent
                        if (HandleKicked != null)
                        {
                            HandleKicked();
                            worker = null;
                        }
                        else
                        {
                            Console.WriteLine("You have been kicked by the Security Software.");
                        }
                    }
                }
                catch (SocketException se)
                {
                    if (se.SocketErrorCode == SocketError.ConnectionReset) //Disconnected
                    {
                        Console.WriteLine("You have been disconnected from the Server.");

                        //RaiseEvent
                        HandleDisconnected?.Invoke();

                        //Mark worker as null to stop reciving
                        worker = null;
                    }
                    else
                    {
                        Console.WriteLine(se.ErrorCode);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.Log(LogLevel.NETWORK, $"Agent server packet error: {ex.Message}");
                }
                finally
                {
                    if (worker != null)
                    {
                        worker.BeginReceive(RemoteRecvBuffer.Buffer, 0, MAX_RECVSIZE, SocketFlags.None, new AsyncCallback(WaitForServerData), worker);
                    }
                }
            }
        }

        private void Send(byte[] buffer)
        {
            if (!isClosing && doPacketProcess) //if not closing and also packetprocessing
            {
                if (RemoteSocket.Connected)
                {
                    try
                    {
                        RemoteSocket.Send(buffer);
                        if (buffer.Length == 0)
                        {
                            Console.WriteLine("buffer.Length == 0");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Logger.Log(LogLevel.NETWORK, $"Agent send packet error: {ex.Message}");
                    }
                }
            }
        }
        public void Send(Packet packet)
        {
            if (RemoteSecurity != null)
                RemoteSecurity.Send(packet);
        }

        private void ThreadedPacketProcessing()
        {
            begin:

            //Wait until we should process packets
            while (!doPacketProcess && !isClosing)
            {
                Thread.Sleep(1);
            }

            lastPacket = DateTime.Now; //ANTI INSTA PING
            while (doPacketProcess && !isClosing)
            {
                ProcessServerPackets();
                Thread.Sleep(1);
            }

            if (isClosing)
            {
                return; //Jump out.
            }

            goto begin; //goto begin and wait until we should process again
        }

        private void ProcessServerPackets()
        {
            if (!isClosing && doPacketProcess)
            {
                RemoteRecvPackets = RemoteSecurity.TransferIncoming();
                if (RemoteRecvPackets != null)
                {
                    foreach (var packet in RemoteRecvPackets)
                    {
                        if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000)
                        {
                            continue;
                        }

                        //RaiseEvent if event is assigned.
                        if (HandlePacketReceived != null)
                        {
                            Logger.ProxyOutput(LogLevel.NETWORK, "S->P", packet);
                            HandlePacketReceived(packet);
                        }
                    }
                }

                RemoteSendBuffers = RemoteSecurity.TransferOutgoing();
                if (RemoteSendBuffers != null)
                {
                    foreach (var buffer in RemoteSendBuffers)
                    {
                        Logger.ProxyOutput(LogLevel.NETWORK, "P->S", buffer.Value);
                        lastPacket = DateTime.Now;
                        Send(buffer.Key.Buffer);
                    }
                }

                if (DateTime.Now.Subtract(lastPacket).TotalMilliseconds > 5000)
                {
                    //                    lastPacket = DateTime.Now;
                    Packet ping = new Packet(0x2002);
                    Send(ping);
                }

            }
        }
    }
}
