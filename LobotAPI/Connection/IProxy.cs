﻿using LobotAPI.Packets;
using System.Threading.Tasks;

namespace LobotAPI.Proxy
{
    public interface IProxy
    {
        void SendCommandAG(ICommand c);
        Task SendCommandAG(int delay, ICommand c);
        void SendCommandGW(ICommand c);
        Task SendCommandGW(int delay, ICommand c);
    }
}