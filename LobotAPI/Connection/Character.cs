﻿using SilkroadSecurityApi;

namespace LobotAPI.Connection
{
    public class Character
    {
        public string Name { get; set; }
        public byte Level { get; set; }

        public Character(Packet packet)
        {
            packet.ReadUInt32(); //Model
            Name = packet.ReadAscii();
            packet.ReadUInt8(); //Volume
            Level = packet.ReadUInt8(); //Level
            packet.ReadUInt64(); //EXP
            packet.ReadUInt16(); //STR
            packet.ReadUInt16(); //INT
            packet.ReadUInt16(); //STAT
            packet.ReadUInt32(); //HP
            packet.ReadUInt32(); //MP
            var restoreFlag = packet.ReadUInt8() == 0x01;
            if (restoreFlag)
                packet.ReadUInt32(); //Delete Time

            packet.ReadUInt8(); //Something
            packet.ReadUInt8(); //with
            packet.ReadUInt8(); //Guild,Trader etc...

            var itemCount = packet.ReadUInt8();
            for (int i = 0; i < itemCount; i++)
            {
                packet.ReadUInt32();
                packet.ReadUInt8();
            }

            var avatarCount = packet.ReadUInt8();
            for (int i = 0; i < avatarCount; i++)
            {
                packet.ReadUInt32();
                packet.ReadUInt8();
            }
        }

        public override string ToString()
        {
            return $"{Name} - [Lv. {Level}";
        }
    }
}
