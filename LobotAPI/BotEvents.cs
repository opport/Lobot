﻿using LobotAPI.Globals.Settings;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LobotAPI
{
    /// <summary>
    /// The bot instance used to implement all functions provided.
    /// </summary>
    public static partial class Bot
    {
        public static event EventHandler SelectednpcIDChanged = delegate { };
        public static event EventHandler PartyChanged = delegate { };
        public static event EventHandler BotStatusChanged = delegate { };
        public static event EventHandler<BoolEventArgs> BotIsInteractingChanged = delegate { };
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            EventHandler<PropertyChangedEventArgs> handler = StaticPropertyChanged;
            if (handler != null)
            {
                handler(null, new PropertyChangedEventArgs(propertyName));
            }
        }


        private static void OnPartyChanged(PartyChangedArgs e)
        {
            EventHandler handler = PartyChanged;
            if (handler != null)
            {
                if (Bot.PartyCurrent.IsFull)
                {
                    handler(null, new PartyChangedArgs(PartyChangeType.Full));
                }
                else if (Bot.PartyCurrent.MemberCount < 0)
                {
                    handler(null, new PartyChangedArgs(PartyChangeType.Update));
                }
                else
                {
                    handler(null, new PartyChangedArgs(PartyChangeType.Empty));
                }
            }
        }

        private static void OnSelectednpcIDChanged(EventArgs e)
        {
            EventHandler handler = SelectednpcIDChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnBottingStatusChanged(EventArgs e)
        {
            EventHandler handler = BotStatusChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnIsInteractingChanged(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = BotIsInteractingChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
