﻿namespace LobotAPI
{
    public enum BotStatus : byte
    {
        Disconnected = 0x00,
        Login = 0x01,
        CharacterSelect = 0x02,
        Loading = 0x03,
        Idle = 0x04,
        Botting = 0x05
    }
}
