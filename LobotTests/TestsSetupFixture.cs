﻿
using LobotAPI.Globals;
using NUnit.Framework;

namespace LobotTests
{
    [SetUpFixture]
    public class TestsSetupFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            TestsSetupFixtureHelper.LoadItems();
            TestsSetupFixtureHelper.LoadLevels();
            TestsSetupFixtureHelper.LoadNames();
            // TestsSetupFixtureHelper.LoadNavmesh();
            TestsSetupFixtureHelper.LoadNPCs();
            TestsSetupFixtureHelper.LoadShops();
            TestsSetupFixtureHelper.LoadSkills();
            TestsSetupFixtureHelper.LoadTeleporters();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            PK2DataGlobal.Items = null;
            PK2DataGlobal.LevelData = null;
            PK2DataGlobal.Names = null;
            PK2DataGlobal.NavigationSegments = null;
            PK2DataGlobal.NPCs = null;
            PK2DataGlobal.Shops = null;
            PK2DataGlobal.Skills = null;
            PK2DataGlobal.Teleporters = null;
        }
    }
}