﻿using LobotDLL.Packets;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Login
{
    [TestFixture]
    public class LoginClientTest
    {
        LoginClient LoginClientPacketHandler;
        Packet LoginClientValid;
        Packet LoginClientInvalid;
        Packet LoginClientResponseValid;
        Packet LoginClientResponseInvalid;

        [SetUp]
        public void OneTimeSetUp()
        {
            LoginClientPacketHandler = new LoginClient();

            LoginClientValid = new Packet(0x6103, true, false, new byte[] { 0xD4, 0x00, 0x00, 0x00, 0x05, 0x00, 0x60, 0x60, 0x60, 0x60, 0x70, 0x00, 0x00, 0x30, 0x30, 0x30, 0x30, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });

            LoginClientInvalid = new Packet(0x6103, true, false, new byte[] { 0xD6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });

            LoginClientResponseValid = new Packet(0xA102, true, false, new byte[] { 0x01, 0xD4, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x39, 0x34, 0x2E, 0x31, 0x36, 0x2E, 0x31, 0x31, 0x35, 0x2E, 0x32, 0x39, 0x0C, 0x3E });

            LoginClientResponseInvalid = new Packet(0xA102, true, false, new byte[] { 0x02, 0x01 });
        }

        [TearDown]
        public void OneTimeTearDown()
        {
            LoginClientPacketHandler = null;
            LoginClientInvalid = null;
            LoginClientValid = null;
            LoginClientResponseInvalid = null;
        }

        [Test]
        public void LoginClientValidTest()
        {
            LoginClientValid.Lock();
            Assert.IsFalse(LoginClient.TrySpoofClientLoginPacket(LoginClientValid, out Packet result));
        }

        [Test]
        public void LoginClientInvalidTest()
        {
            LoginClientInvalid.Lock();
            Assert.IsTrue(LoginClient.TrySpoofClientLoginPacket(LoginClientInvalid, out Packet result));
        }

        [Test]
        public void LoginClientResponseValidTest()
        {
            Assert.IsNotNull(LoginClientPacketHandler.Handle(LoginClientResponseValid));
        }

        [Test]
        public void LoginClientResponseInvalidTest()
        {
            Assert.IsNull(LoginClientPacketHandler.Handle(LoginClientResponseInvalid));
        }
    }
}
