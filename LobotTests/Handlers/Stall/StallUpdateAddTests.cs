﻿using LobotAPI.Globals.Settings;
using LobotDLL.Packets.Stall;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Stall
{
    [TestFixture]
    class StallUpdateAddTests
    {
        private StallUpdate StallUpdateHandler;
        private Packet packetSingleItem;
        private Packet packet10Items;

        [OneTimeSetUp]
        public void TestInitialize()
        {
            StallUpdateHandler = new StallUpdate();

            //cassiopeia moonlight orb - steady, lucky blues
            //slot 0
            //[S->C][B0BA]
            //01                                                ................
            //02                                                ................
            //00 00                                             ................
            //00                                                ................
            //00 00 00 00                                       ................
            //C1 2B 00 00                                       .+..............
            //00                                                ................
            //EB 92 52 C2 01 00 00 00                           ..R.............
            //41 00 00 00                                       A...............
            //02                                                ................
            //38 00 00 00 06 00 00 00                           8...............
            //3B 00 00 00 01 00 00 00                           ;...............
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //28                                                (...............
            //01 00                                             ................
            //01 00 00 00 00 00 00 00                           ................
            //FF                                                ................
            packetSingleItem = new Packet(0xB0BA, false, false,
                new byte[] { 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC1, 0x2B,
                    0x00, 0x00, 0x00, 0xEB, 0x92, 0x52, 0xC2, 0x01, 0x00, 0x00, 0x00, 0x41, 0x00,
                    0x00, 0x00, 0x02, 0x38, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x3B, 0x00,
                    0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x28, 0x01, 0x00,
                    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF
                });

            //[S -> C] [B0BA]
            //01                                                result
            //02                                                addItem
            //00 00                                             errorCode
            //00                                                slot 0
            //00 00 00 00                                       owner = self
            //8F 28 00 00                                       refID bolts
            //04 01                                             stacks = 260
            //1E                                                inventory slot 30
            //04 01                                             260
            //0B 00 00 00 00 00 00 00                           price = 11
            //01                                                slot 1
            //00 00 00 00                                       owner = self
            //3E 00 00 00                                       >...............
            //8C 00                                             stacks
            //21                                                inventory slot
            //8C 00                                             ................
            //16 00 00 00 00 00 00 00                           price 22
            //02                                                slot 2
            //00 00 00 00                                       owner = self
            //62 0E 00 00                                       b...............
            //15 00                                             ................
            //18                                                inventory slot
            //15 00                                             ................
            //21 00 00 00 00 00 00 00                           price 33
            //03                                                slot 3
            //00 00 00 00                                       owner = self
            //5F 0E 00 00                                       _...............
            //10 00                                             ................
            //1B                                                inventory slot
            //10 00                                             ................
            //2C 00 00 00 00 00 00 00                           price 44
            //04                                                slot 4
            //00 00 00 00                                       owner = self
            //60 0E 00 00                                       `...............
            //0C 00                                             ................
            //16                                                inventory slot
            //0C 00                                             ................
            //37 00 00 00 00 00 00 00                           price 55
            //05                                                slot 5
            //00 00 00 00                                       owner = self
            //61 0E 00 00                                       a...............
            //0B 00                                             ................
            //15                                                inventory slot
            //0B 00                                             ................
            //42 00 00 00 00 00 00 00                           price 66
            //06                                                slot 6
            //00 00 00 00                                       owner = self
            //39 00 00 00                                       9...............
            //28 00                                             (...............
            //1D                                                inventory slot
            //28 00                                             (...............
            //4D 00 00 00 00 00 00 00                           price 77
            //07                                                slot 7
            //00 00 00 00                                       owner = self
            //05 01 00 00                                       refID "ITEM_CH_SHIELD_04_B"
            //01                                                optlvl 1
            //A2 B0 13 1C 00 00 00 00                           whiteparams
            //39 00 00 00                                       durability
            //01                                                number of blues
            //30 00 00 00 05 00 00 00                           blues(immortal 5)
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //1C                                                ................
            //01 00                                             ................
            //58 00 00 00 00 00 00 00                           price 88
            //08                                                slot 8
            //00 00 00 00                                       owner = self
            //B2 33 00 00                                       .3..............
            //00                                                ................
            //66 89 7B 14 00 00 00 00                           f.{.............
            //46 00 00 00                                       F...............
            //03                                                ................
            //35 00 00 00 02 00 00 00                           steady? 53 x 2 ?
            //05 00 00 00 02 00 00 00                           int? 5 x 2
            //32 00 00 00 01 00 00 00                           immortal 1
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //25                                                %...............
            //01 00                                             ................
            //63 00 00 00 00 00 00 00                           price 99
            //09                                                slot 9 (final)
            //00 00 00 00                                       owner = self
            //40 1D 00 00                                       refID grey wolf
            //03                                                status
            //FB 17 00 00                                       6139
            //00 00                                             name
            //00                                                unkn
            //11                                                inventory slot 17
            //01 00                                             stackcount 1
            //F2 03 00 00 00 00 00 00                           price 1010
            //FF                                                slot = end flag
            packet10Items = new Packet(0xB0BA, false, false,
                            new byte[] { 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x8F, 0x28,
                0x00, 0x00, 0x04, 0x01, 0x1E, 0x04, 0x01, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
                0x00, 0x00, 0x00, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x8C, 0x00, 0x21, 0x8C, 0x00, 0x16, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x62, 0x0E, 0x00, 0x00, 0x15, 0x00,
                0x18, 0x15, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00,
                0x5F, 0x0E, 0x00, 0x00, 0x10, 0x00, 0x1B, 0x10, 0x00, 0x2C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x60, 0x0E, 0x00, 0x00, 0x0C, 0x00, 0x16, 0x0C, 0x00, 0x37,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x61, 0x0E, 0x00, 0x00,
                0x0B, 0x00, 0x15, 0x0B, 0x00, 0x42, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00,
                0x00, 0x00, 0x39, 0x00, 0x00, 0x00, 0x28, 0x00, 0x1D, 0x28, 0x00, 0x4D, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x05, 0x01, 0x00, 0x00, 0x01, 0xA2, 0xB0, 0x13,
                0x1C, 0x00, 0x00, 0x00, 0x00, 0x39, 0x00, 0x00, 0x00, 0x01, 0x30, 0x00, 0x00, 0x00, 0x05, 0x00,
                0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x1C, 0x01, 0x00, 0x58, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0xB2, 0x33, 0x00, 0x00, 0x00, 0x66, 0x89, 0x7B, 0x14, 0x00,
                0x00, 0x00, 0x00, 0x46, 0x00, 0x00, 0x00, 0x03, 0x35, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
                0x05, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
                0x01, 0x00, 0x02, 0x00, 0x25, 0x01, 0x00, 0x63, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09,
                0x00, 0x00, 0x00, 0x00, 0x40, 0x1D, 0x00, 0x00, 0x03, 0xFB, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x11, 0x01, 0x00, 0xF2, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF
                                             });
        }

        [OneTimeTearDown]
        public void TestCleanup()
        {
            StallUpdateHandler = null;
            packetSingleItem = null;
            packet10Items = null;
            StallSettings.StallItems.Clear();
        }

        [TearDown]
        public void TestTearDown()
        {
            StallSettings.StallItems.Clear();
        }

        [Test]
        public void StallAddSingleItemTest()
        {
            packet10Items.Lock();
            Assert.DoesNotThrow(() => StallUpdateHandler.Handle(packetSingleItem));
            Assert.AreEqual(1, StallSettings.StallItems.Count);
        }

        [Test]
        public void StallAdd10ItemsTest()
        {
            packet10Items.Lock();
            Assert.DoesNotThrow(() => StallUpdateHandler.Handle(packet10Items));
            Assert.AreEqual(10, StallSettings.StallItems.Count);
        }
    }
}
