﻿using LobotDLL.Packets.Community.Guild;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Community
{
    [TestFixture]
    public class FortressInfoHandlerTest
    {
        FortressInfoHandler fortressInfoHandler;
        Packet Inpanic;
        Packet Valtyr;

        [SetUp]
        public void OneTimeSetUp()
        {
            fortressInfoHandler = new FortressInfoHandler();

            // version 248
            //[S -> C] [385F]
            //00                                                unkn
            //03                                                number of fortresses (?)
            //01 00 00 00                                       fortressID jangan
            //05 00                                             name length
            //55 6C 63 75 73                                    Ulcus...........
            //DF 09 00 00                                       unkn (guild number?)
            //05 00                                             guildmaster length
            //42 6C 69 6E 6B                                    Blink...........
            //31 00                                             guild message length
            //5A 61 72 74 62 69 74 74 65 72 20 44 69 6E 6B 65   Zartbitter.Dinke
            //6C 20 4B 6E 75 73 70 65 72 77 61 66 66 65 6C 6E   l.Knusperwaffeln
            //20 32 30 30 67 20 66 FC 72 20 33 2C 32 39 80 20   .200g.f.r.3,29..
            //20                                                unkn 32
            //03 00 00 00                                       3
            //50 00 00 00                                       item price 80 (100 - 20% taxes)
            //01 00 00 00                                       1
            //00                                                unkn
            //00                                                unkn
            //03 00 00 00                                       fortressID bandit
            //00 00                                             name length 0
            //00 00 00 00                                       guild number
            //00 00                                             guildmaster name length 0
            //00 00                                             guild message length 0
            //00 00 00 00                                       unkn
            //00 00 00 00                                       item price percent
            //00 00 00 00                                       unkn
            //00                                                unkn
            //00                                                unkn
            //06 00 00 00                                       fortressID hotan
            //00 00                                             name length
            //00 00 00 00                                       guild number
            //00 00                                             guild master length
            //00 00                                             guild message length
            //00 00 00 00                                       unkn
            //00 00 00 00                                       item price percent
            //00 00 00 00                                       unkn
            //00                                                unkn
            //00                                                unkn
            //02                                                unkn
            //00 00 00 00                                       ................
            Inpanic = new Packet(0x385F, false, false,
                new byte[] {
                    0x00, 0x03, 0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x55, 0x6C, 0x63, 0x75, 0x73, 0xDF, 0x09, 0x00, 0x00, 0x05, 0x00,
                    0x42, 0x6C, 0x69, 0x6E, 0x6B, 0x31, 0x00, 0x5A, 0x61, 0x72, 0x74, 0x62, 0x69, 0x74, 0x74, 0x65, 0x72, 0x20, 0x44,
                    0x69, 0x6E, 0x6B, 0x65, 0x6C, 0x20, 0x4B, 0x6E, 0x75, 0x73, 0x70, 0x65, 0x72, 0x77, 0x61, 0x66, 0x66, 0x65, 0x6C,
                    0x6E, 0x20, 0x32, 0x30, 0x30, 0x67, 0x20, 0x66, 0xFC, 0x72, 0x20, 0x33, 0x2C, 0x32, 0x39, 0x80, 0x20, 0x20, 0x03,
                    0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00,
                });
            // version 57
            //[S -> C] [385F]
            //00                                                unkn
            //03                                                number of forts
            //01 00 00 00                                       fortID jangan
            //00 00                                             guild namelength
            //00 00 00 00                                       guild number
            //00 00                                             gm namelength
            //00 00                                             message length
            //00 00 00 00                                       unkn
            //00 00 00 00                                       price percent
            //00 00 00 00                                       unkn
            //01                                                ................
            //00 00 00 00                                       ................
            //01                                                ................
            //00 00 00 00                                       ................
            //03 00 00 00                                       fortID bandit
            //00 00                                             guild namelength
            //00 00 00 00                                       guild number
            //00 00                                             gm namelength
            //00 00                                             message length
            //00 00 00 00                                       unkn
            //00 00 00 00                                       percentage
            //00 00 00 00                                       unkn
            //01                                                ?
            //00 00 00 00                                       unkn uint
            //01                                                ?
            //00 00 00 00                                       ................
            //06 00 00 00                                       fortID hotan?
            //0C 00                                             name length
            //53 75 69 63 69 64 65 53 71 75 61 64               SuicideSquad....
            //32 04 00 00                                       guild number
            //09 00                                             gm length
            //47 65 6E 69 54 61 6C 69 73                        GeniTalis.......
            //57 00                                             message length
            //47 75 69 6C 64 20 33 30 30 20 61 6E 64 20 53 75   Guild.300.and.Su
            //69 63 69 64 65 53 71 75 61 64 20 69 73 20 6C 6F   icideSquad.is.lo
            //6F 6B 69 6E 67 20 66 6F 72 20 6E 65 77 20 6D 65   oking.for.new.me
            //6D 62 65 72 73 20 70 6D 20 47 65 6E 69 54 61 6C mbers.pm.GeniTal
            //69 73 20 6F 72 20 44 61 76 79 4A 6F 6E 65 73 20   is.or.DavyJones.
            //0A 74 68 78 20 3D 29                              .thx.=).........
            //01 00 00 00                                       unkn
            //00 00 00 00                                       percentage
            //00 00 00 00                                       unkn
            //00                                                ................
            //00                                                ................
            //04                                                ................
            //00 00 00 00                                       ................   
            Valtyr = new Packet(0x385F, false, false,
                new byte[] {
                    0x00,0x03,0x01, 0x00, 0x00, 0x00,0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x01,0x00, 0x00, 0x00, 0x00,0x01,
                    0x00, 0x00, 0x00, 0x00,0x03, 0x00, 0x00, 0x00,0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x01,0x00, 0x00, 0x00, 0x00,
                    0x01,0x00, 0x00, 0x00, 0x00,0x06, 0x00, 0x00, 0x00,0x0C, 0x00, 0x53, 0x75, 0x69, 0x63, 0x69, 0x64, 0x65, 0x53, 0x71, 0x75, 0x61, 0x64,
                    0x32, 0x04, 0x00, 0x00,0x09, 0x00,0x47, 0x65, 0x6E, 0x69, 0x54, 0x61, 0x6C, 0x69, 0x73, 0x57, 0x00,0x47, 0x75, 0x69, 0x6C, 0x64, 0x20, 0x33, 0x30, 0x30, 0x20, 0x61, 0x6E, 0x64, 0x20, 0x53, 0x75,
                    0x69, 0x63, 0x69, 0x64, 0x65, 0x53, 0x71, 0x75, 0x61, 0x64, 0x20, 0x69, 0x73, 0x20, 0x6C, 0x6F,0x6F, 0x6B, 0x69, 0x6E, 0x67, 0x20, 0x66, 0x6F, 0x72, 0x20, 0x6E, 0x65, 0x77, 0x20, 0x6D, 0x65,
                    0x6D, 0x62, 0x65, 0x72, 0x73, 0x20, 0x70, 0x6D, 0x20, 0x47, 0x65, 0x6E, 0x69, 0x54, 0x61, 0x6C,0x69, 0x73, 0x20, 0x6F, 0x72, 0x20, 0x44, 0x61, 0x76, 0x79, 0x4A, 0x6F, 0x6E, 0x65, 0x73, 0x20,
                    0x0A, 0x74, 0x68, 0x78, 0x20, 0x3D, 0x29,0x01, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00, 0x00, 0x00, 0x00,0x00,0x00,0x04,0x00, 0x00, 0x00, 0x00,
                });
        }

        [TearDown]
        public void OneTimeTearDown()
        {
            fortressInfoHandler = null;
            Valtyr = null;
        }

        [Test]
        public void FortressInfoInPanicServerTest()
        {
            Assert.DoesNotThrow(() => fortressInfoHandler.Handle(Valtyr));
        }

        [Test]
        public void FortressInfoValtyrServerTest()
        {
            Assert.DoesNotThrow(() => fortressInfoHandler.Handle(Valtyr));
        }
    }
}
