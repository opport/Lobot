﻿using LobotDLL.StateMachine;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.StateMachine.ScriptingStates
{
    [TestFixture]
    public class ShoppingStateTests
    {

        private static Packet JanganBuyItem;

        [SetUpFixture]
        public class SingleSpawnSetup
        {

            [OneTimeSetUp]
            public void RunBeforeAnyTests()
            {
                // Buy red horse in jangan
                //[S -> C][B034]
                //01                                                flag
                //08                                                movementType 8 buy
                //00                                                itemTab 0
                //00                                                itemSlot 0
                //01                                                quantity
                //17                                                toSlot
                //01 00                                             stackAmount 1
                //00 00 00 00                                       recipientNPCID ? 0 => self
                JanganBuyItem = new Packet(0xB034, false, false, new byte[] { 0x01, 0x08, 0x00, 0x00, 0x01, 0x17, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00 });
            }

            [OneTimeTearDown]
            public void RunAfterAnyTests()
            {
                JanganBuyItem = null;
            }
        }

        [Test(Description = "Try to fake a Packet without throwing an exception")]
        public void FakeBuyItemJanganTest()
        {
            Assert.DoesNotThrow(() => ShoppingState.FakeItemMovement(JanganBuyItem));
        }

        [Test(Description = "Faking function shouldn't return anything if the item to fake was not found")]
        public void FakeBuyItemJanganEmptyTest()
        {
            Packet[] expected = new Packet[0];
            Packet[] result = ShoppingState.FakeItemMovement(JanganBuyItem);
            Assert.AreNotEqual(result, null);
            Assert.AreEqual(result, expected);
        }
    }
}
