﻿using LobotAPI.Globals;
using LobotDLL.Packets.Spawn;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Spawn
{
    [TestFixture]
    class ParsePortalTests
    {
        private ParsePortal PortalParser;
        private Packet packetPortal;
        private Packet packetPortalViper;

        [OneTimeSetUp]
        public void TestInitialize()
        {
            PortalParser = new ParsePortal();

            //[S -> C] [3019]
            //01                                                ................
            //01 00                                             ................
            //2E 08 00 00                                       ................
            //07 00 00 00                                       ................
            //A8 61                                             .a..............
            //00 C0 9C 44 00 00 C0 C0 00 C0 AB 44 00 00         ...D.......D....
            //01                                                ................
            //00                                                ................
            //00                                                ................
            //01                                                ................
            //00 00 00 00                                       ................
            //00 00 00 00                                       ................
            packetPortal = new Packet(0x3019, false, false,
                new byte[] { /*0x01, 0x01, 0x00,*/ 0x2E, 0x08, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0xA8, 0x61,
                                0x00, 0xC0, 0x9C, 0x44, 0x00, 0x00, 0xC0, 0xC0, 0x00, 0xC0, 0xAB, 0x44, 0x00, 0x00,
                                0x01, 0x00, 0x00, 0x01,
                                0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00
                                 });
            //[S -> C] [3019]
            //70 59 00 00 0F 00 00 00 A8 62 00 80 E5 44 00 00   pY.......b...D..
            //A0 41 00 00 DC 43 00 00 01 00 00 01 00 00 00 00   .A...C..........
            //00 00 00 00                                       ................

            //[S -> C] [3019]
            //01                                                ................
            //01 00                                             ................
            //70 59 00 00                                       pY..............
            //0F 00 00 00                                       ................
            //A8 62                                             .b..............
            //00 80 E5 44 00 00 A0 41 00 00 DC 43 00 00         ...D...A...C....
            //01                                                ................
            //00                                                ................
            //00                                                ................
            //01                                                ................
            //00 00 00 00                                       ................
            //00 00 00 00                                       ................
            packetPortalViper = new Packet(0x3019, false, false,
                new byte[] { 0x70, 0x59, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0xA8, 0x62, 0x00, 0x80, 0xE5, 0x44, 0x00, 0x00,
                            0xA0, 0x41, 0x00, 0x00, 0xDC, 0x43, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0x00,  });
        }

        [OneTimeTearDown]
        public void TestCleanup()
        {
            PortalParser = null;
            packetPortal = null;
            packetPortalViper = null;
            SpawnListsGlobal.RemoveAllSpawnEntries();
            GroupSpawnBeginHandler.RemainingSpawns = 0;
        }

        [Test]
        public void ParsePortalTest()
        {
            packetPortal.Lock();
            Assert.DoesNotThrow(() => PortalParser.Parse(packetPortal, packetPortal.ReadUInt32()));
        }

        [Test]
        public void ParsePortalViperTest()
        {
            packetPortalViper.Lock();
            Assert.DoesNotThrow(() => PortalParser.Parse(packetPortalViper, packetPortalViper.ReadUInt32()));
        }
    }
}
