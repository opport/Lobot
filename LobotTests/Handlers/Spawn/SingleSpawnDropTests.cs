﻿using LobotAPI.Globals;
using LobotDLL.Packets.Spawn;
using NUnit.Framework;
using SilkroadSecurityApi;
using System;

namespace LobotTests.Handlers.Spawn
{
    [TestFixture]
    public class SingleSpawnDropTests
    {
        private static SingleSpawnHandler SpawnHandler;
        private static SingleDespawnHandler DespawnHandler;
        private static Packet Spawn;
        private static Packet Despawn;
        private static Packet SpawnPet;
        private static Packet DespawnPet;

        [SetUpFixture]
        public class SingleSpawnSetup
        {

            [OneTimeSetUp]
            public void RunBeforeAnyTests()
            {
                SpawnHandler = new SingleSpawnHandler();
                DespawnHandler = new SingleDespawnHandler();

                //[S->C][3015]
                //01 00 00 00
                //32 00 00 00
                //2B 22 55 00
                //A9 61
                //8C FE BB 44 00 00 00 00 BD 67 51 44 9C 5B
                //00
                //00
                //06
                //00 00 00 00
                Spawn = new Packet(0x3015, false, false, new byte[] {
                0x01, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x2B, 0x22, 0x55, 0x00, 0xA9, 0x61,
                0x8C, 0xFE, 0xBB, 0x44, 0x00, 0x00, 0x00, 0x00, 0xBD, 0x67, 0x51, 0x44, 0x9C, 0x5B,
                0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00
                });
                //[S->C][3016]
                //2B 22 55 00
                Despawn = new Packet(0x3016, false, false, new byte[] { 0x2B, 0x22, 0x55, 0x00 });
                //[S -> C][3015]
                //EC 17 00 00                                       ................
                //12 24 32 08                                       .$2.............
                //89 5B                                             .[..............
                //A6 17 6F 44 76 C2 1A 42 97 60 AE 44 FA 04         ..oDv..B.`.D....
                //00                                                ................
                //01                                                ................
                //00                                                ................
                //FA 04                                             ................
                //01                                                ................
                //00                                                ................
                //00                                                ................
                //02                                                ................
                //00 00 5C 42                                       ..\B............
                //00 00 A6 42                                       ...B............
                //00 00 C8 42                                       ...B............
                //00                                                ................
                //00                                                ................
                //00 00                                             ................
                //05 00                                             ................
                //4C 6F 62 6F 74                                    Lobot...........
                //04                                                ................
                //00                                                ................
                //B1 23 32 08                                       .#2.............
                //01                                                ................
                SpawnPet = new Packet(0x3015, false, false, new byte[] {
                0xEC, 0x17, 0x00, 0x00, 0x12, 0x24, 0x32, 0x08, 0x89, 0x5B, 0xA6, 0x17, 0x6F, 0x44, 0x76,
                0xC2, 0x1A, 0x42, 0x97, 0x60, 0xAE, 0x44, 0xFA, 0x04, 0x00, 0x01, 0x00, 0xFA, 0x04, 0x01,
                0x00, 0x00, 0x02, 0x00, 0x00, 0x5C, 0x42, 0x00, 0x00, 0xA6, 0x42, 0x00, 0x00, 0xC8, 0x42,
                0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x4C, 0x6F, 0x62, 0x6F, 0x74, 0x04, 0x00, 0xB1, 0x23,
                0x32, 0x08, 0x01,
                });
                //[S -> C] [3016]
                //12 24 32 08                                       .$2.............
                DespawnPet = new Packet(0x3016, false, false, new byte[] { 0x12, 0x24, 0x32, 0x08 });
            }

            [OneTimeTearDown]
            public void RunAfterAnyTests()
            {
                SpawnHandler = null;
                DespawnHandler = null;
                Spawn = null;
                Despawn = null;
                SpawnListsGlobal.RemoveAllSpawnEntries();
                GroupSpawnBeginHandler.RemainingSpawns = 0;
            }
        }

        [Test]
        public void SingleSpawnThenDespawnGoldTest()
        {
            try
            {
                int initialDrops = SpawnListsGlobal.Drops.Keys.Count;
                int remainingSpawns = GroupSpawnBeginHandler.RemainingSpawns;
                SpawnHandler.Handle(Spawn);
                Assert.True(SpawnListsGlobal.AllSpawnEntries.Count == 1 && SpawnListsGlobal.Drops.Keys.Count == 1, string.Format("initial drops: {0}\nspawnentries: {1}\ndrop count: {2}", initialDrops, SpawnListsGlobal.AllSpawnEntries.Count, SpawnListsGlobal.Drops.Keys.Count));
                DespawnHandler.Handle(Despawn);
                Assert.True(GroupSpawnBeginHandler.RemainingSpawns == 0, string.Format("initial drops: {0}\n result: {1}", initialDrops, remainingSpawns));
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + e.StackTrace);
                throw;
            }
        }

        [Test]
        public void SingleSpawnPetThenDespawnTest()
        {
            try
            {
                int initialSpawns = SpawnListsGlobal.NPCSpawns.Keys.Count;
                int remainingSpawns = GroupSpawnBeginHandler.RemainingSpawns;
                SpawnHandler.Handle(SpawnPet);
                Assert.True(SpawnListsGlobal.AllSpawnEntries.Count == 1, string.Format("initial spawns: {0}\nspawnentries: {1}\nspawn count: {2}", initialSpawns, SpawnListsGlobal.AllSpawnEntries.Count, SpawnListsGlobal.AllSpawnEntries.Count));
                DespawnHandler.Handle(DespawnPet);
                Assert.True(GroupSpawnBeginHandler.RemainingSpawns == 0, string.Format("initial spawns: {0}\n result: {1}", initialSpawns, remainingSpawns));
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message + e.StackTrace);
                throw;
            }
        }
    }
}
