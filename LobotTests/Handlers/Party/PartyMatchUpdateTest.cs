﻿using LobotAPI.Globals;
using LobotDLL.Packets.Party;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Party
{
    [TestFixture]
    public class PartyMatchUpdateTest
    {
        private static PartyMatchRefresh PartyMatching;
        private static Packet Response;

        [SetUpFixture]
        public class GroupSpawnSetup
        {
            [OneTimeSetUp]
            public void RunBeforeAnyTests()
            {
                PartyMatching = new PartyMatchRefresh();
                //[S -> C] [B06C]
                //01                                                flag
                //01                                                totalPages
                //00                                                currentPage
                //05                                                partiesOnCurrentPage(5)
                //23 4F 00 00                                       partyNumber(20259)
                //5D 73 63 5A                                       unknownFlag(partyLeader ID) ?
                //08 00                                             leaderNameLength
                //4B 6F 52 6F 5A 61 4B 69                           partyLeaderName(KoRoZaKi)
                //00                                                race (0 == chinese)
                //02                                                members(2)
                //05                                                partyType(canInvite, itemfree, expshare) ?
                //00                                                purpose(hunting LTP)
                //01                                                minLevel(1)
                //50                                                maxLevel(80)
                //08 00                                             titleLength
                //76 65 6D 20 61 6D 6F 72                           partyTitle(vem.amor)
                //32 4F 00 00                                       partyNumber(20274)
                //69 7D 63 5A                                       unknownFlag(partyLeader ID) ?
                //04 00                                             leaderNameLength
                //58 4C 4C 4C                                       partyLeaderName(XLLL)
                //00                                                race (0 == chinese)
                //04                                                members(4)
                //07                                                partyType(canInvite, itemshare, expshare)?
                //00                                                purpose(hunting LTP)
                //01                                                minLevel(1)
                //50                                                maxLevel(80)
                //12 00                                             titleLength
                //41 42 59 53 20 43 4F 4D 45 20 41 4C 4C 20 32 34   ABYS.COME.ALL.24
                //2F 37                                             /7..............
                //33 4F 00 00                                       partyNumber(20275)
                //2F 7B 63 5A                                       unknownFlag(partyLeader ID) ?
                //08 00                                             leaderNameLength
                //44 72 5F 50 6C 75 54 6F                           partyLeaderName(Dr_PluTo)
                //00                                                race (0 == chinese)
                //05                                                members(5)
                //03                                                partyType(canInvite, itemshare, expshare)?
                //00                                                purpose(hunting LTP)
                //01                                                minLevel(1)
                //50                                                maxLevel(80)
                //06 00                                             titleLength
                //55 6E 69 71 75 65                                 Unique..........
                //34 4F 00 00                                       partyNumber(20276)
                //63 7B 63 5A                                       unknownFlag(partyLeader ID) ?
                //06 00                                             leaderNameLength
                //59 61 6D 69 72 6F                                 Yamiro..........
                //00                                                race (0 == chinese)
                //01                                                members(1)
                //04                                                partyType(canInvite, itemfree, expfree)?
                //00                                                purpose(hunting LTP)
                //01                                                minLevel(1)
                //8C                                                maxLevel(140)
                //21 00                                             titleLength
                //46 6F 72 20 6F 70 65 6E 20 68 75 6E 74 69 6E 67   For.open.hunting
                //20 6F 6E 20 74 68 65 20 53 69 6C 6B 72 6F 61 64   .on.the.Silkroad
                //21                                                !...............
                //3E 4F 00 00                                       partyNumber(20286)
                //32 7E 63 5A                                       unknownFlag(partyLeader ID) ?
                //09 00                                             leaderNameLength
                //4C 65 5F 46 61 6D 6F 75 73                        partyLeaderName(Le_Famous)
                //00                                                race (0 == chinese)
                //01                                                members(1)
                //07                                                partyType(canInvite, itemshare, expshare)?
                //02                                                purpose(hunter)
                //50                                                minLevel(80)
                //8C                                                maxLevel(140)
                //27 00                                             titleLength
                //7C 7C 20 4A 6F 62 20 54 65 6D 70 65 6C 20 32 34   ||.Job.Tempel.24
                //20 2F 20 37 20 7C 7C 20 20 28 20 20 B0 20 20 29   ./.7.||..(.....)
                //28 20 20 B0 20 20 29                              (.....).........
                Response = new Packet(0x706C, false, false, new byte[] {
                    0x01, 0x01, 0x00, 0x05, 0x23, 0x4F, 0x00, 0x00, 0x5D, 0x73, 0x63, 0x5A, 0x08, 0x00, 0x4B, 0x6F,
                    0x52, 0x6F, 0x5A, 0x61, 0x4B, 0x69, 0x00, 0x02, 0x05, 0x00, 0x01, 0x50, 0x08, 0x00, 0x76, 0x65,
                    0x6D, 0x20, 0x61, 0x6D, 0x6F, 0x72, 0x32, 0x4F, 0x00, 0x00, 0x69, 0x7D, 0x63, 0x5A, 0x04, 0x00,
                    0x58, 0x4C, 0x4C, 0x4C, 0x00, 0x04, 0x07, 0x00, 0x01, 0x50, 0x12, 0x00, 0x41, 0x42, 0x59, 0x53,
                    0x20, 0x43, 0x4F, 0x4D, 0x45, 0x20, 0x41, 0x4C, 0x4C, 0x20, 0x32, 0x34, 0x2F, 0x37, 0x33, 0x4F,
                    0x00, 0x00, 0x2F, 0x7B, 0x63, 0x5A, 0x08, 0x00, 0x44, 0x72, 0x5F, 0x50, 0x6C, 0x75, 0x54, 0x6F,
                    0x00, 0x05, 0x03, 0x00, 0x01, 0x50, 0x06, 0x00, 0x55, 0x6E, 0x69, 0x71, 0x75, 0x65, 0x34, 0x4F,
                    0x00, 0x00, 0x63, 0x7B, 0x63, 0x5A, 0x06, 0x00, 0x59, 0x61, 0x6D, 0x69, 0x72, 0x6F, 0x00, 0x01,
                    0x04, 0x00, 0x01, 0x8C, 0x21, 0x00, 0x46, 0x6F, 0x72, 0x20, 0x6F, 0x70, 0x65, 0x6E, 0x20, 0x68,
                    0x75, 0x6E, 0x74, 0x69, 0x6E, 0x67, 0x20, 0x6F, 0x6E, 0x20, 0x74, 0x68, 0x65, 0x20, 0x53, 0x69,
                    0x6C, 0x6B, 0x72, 0x6F, 0x61, 0x64, 0x21, 0x3E, 0x4F, 0x00, 0x00, 0x32, 0x7E, 0x63, 0x5A, 0x09,
                    0x00, 0x4C, 0x65, 0x5F, 0x46, 0x61, 0x6D, 0x6F, 0x75, 0x73, 0x00, 0x01, 0x07, 0x02, 0x50, 0x8C,
                    0x27, 0x00, 0x7C, 0x7C, 0x20, 0x4A, 0x6F, 0x62, 0x20, 0x54, 0x65, 0x6D, 0x70, 0x65, 0x6C, 0x20,
                    0x32, 0x34, 0x20, 0x2F, 0x20, 0x37, 0x20, 0x7C, 0x7C, 0x20, 0x20, 0x28, 0x20, 0x20, 0xB0, 0x20,
                    0x20, 0x29, 0x28, 0x20, 0x20, 0xB0, 0x20, 0x20, 0x29
                });
            }

            [OneTimeTearDown]
            public void RunAfterAnyTests()
            {
                PartyMatching = null;
                CharInfoGlobal.PartyMatchingList.Clear();
                Response = null;
            }
        }

        [Test]
        public void UpdatePartyMatchTest()
        {
            PartyMatching.Handle(Response);
            Assert.AreEqual(CharInfoGlobal.PartyMatchingList.Count, 5);
        }
    }
}
