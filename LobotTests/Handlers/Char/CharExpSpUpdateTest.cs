﻿using LobotAPI;
using LobotAPI.Packets.Char;
using LobotDLL.Packets.Char;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Char
{
    [TestFixture]
    public class CharExpSpUpdateTest
    {
        private CharExpSpUpdate ExpSpUpdateHandler;
        private CharLevelUpHandler CharLevelUpHandler;
        private Packet levelUp;
        private Packet expSpUpdate1;
        private Packet expSpUpdateLvlUp;
        private static long ExpGain1 = 73530;
        private static byte BotLvl = 32;
        private ulong ExpBeforeLvlUp = 1806816;
        private ulong ExpAfterLvlUp = 66019;

        private Packet expSpUpdateLvlUp2;
        //private static long ExpGain2 = 146610;
        private static byte BotLvl2 = 35;
        private ulong ExpBeforeLvlUp2 = 2486809;
        private ulong ExpAfterLvlUp2 = 28376;

        [SetUp]
        public void TestInitialize()
        {
            ExpSpUpdateHandler = new CharExpSpUpdate();
            CharLevelUpHandler = new CharLevelUpHandler();
            levelUp = new Packet(0x3054, false, false);
            levelUp.WriteInt32(Bot.CharData.UniqueID);

            //[S -> C][3056]
            //AC 04 20 00                                       ................
            //3A 1F 01 00 00 00 00 00                           :...............
            //58 20 00 00 00 00 00 00                           X...............
            //00                                                ................
            expSpUpdate1 = new Packet(0xB021, false, false,
                new byte[] {
                    0xAC, 0x04, 0x20, 0x00,
                    0x3A, 0x1F, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x58, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00,
                }
            );

            //[S -> C][3056]
            //AC 04 20 00                                       ................
            //CE 30 01 00 00 00 00 00                           :...............
            //B2 20 00 00 00 00 00 00                           X...............
            //00                                                ................
            expSpUpdateLvlUp = new Packet(0xB021, false, false,
                new byte[] {
                    0xAC, 0x04, 0x20, 0x00,
                    0xCE, 0x30, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0xB2, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00,
                }
            );

            //[S -> C][3056]
            //AC 04 20 00                                       ................
            //B2 3C 02 00 00 00 00 00                           :...............
            //B2 20 00 00 00 00 00 00                           X...............
            //00                                                ................
            expSpUpdateLvlUp2 = new Packet(0xB021, false, false,
                new byte[] {
                    0xAC, 0x04, 0x20, 0x00,
                    0xB2, 0x3C, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0xB2, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00,
                }
            );
        }

        [TearDown]
        public void TestCleanup()
        {
            Bot.CharData.ExpOffset = 0;
            Bot.CharData.Level = 1;
            ExpSpUpdateHandler = null;
            expSpUpdate1 = null;
            expSpUpdateLvlUp = null;
        }

        [Test(Description = "Make sure EXP and SP update values are correct")]
        public void ExpSpUpdate1()
        {
            Bot.CharData.Level = 31;
            Bot.CharData.ExpOffset = 0;
            Assert.DoesNotThrow(() => ExpSpUpdateHandler.Handle(expSpUpdate1));
            Assert.AreEqual(Bot.CharData.ExpOffset, ExpGain1);
        }

        [Test(Description = "Make sure EXP and SP update values are correct after level up")]
        public void ExpSpUpdateLevelUp()
        {
            Bot.CharData.Level = BotLvl;
            Bot.CharData.ExpOffset = ExpBeforeLvlUp;
            Assert.DoesNotThrow(() => CharLevelUpHandler.Handle(levelUp));
            Assert.DoesNotThrow(() => ExpSpUpdateHandler.Handle(expSpUpdateLvlUp));
            Assert.AreEqual(ExpAfterLvlUp, Bot.CharData.ExpOffset);
            Assert.AreEqual(BotLvl + 1, Bot.CharData.Level);
        }

        [Test(Description = "Make sure EXP and SP update values are correct after level up")]
        public void ExpSpUpdateLevelUp2()
        {
            Bot.CharData.Level = BotLvl2;
            Bot.CharData.ExpOffset = ExpBeforeLvlUp2;
            Assert.DoesNotThrow(() => CharLevelUpHandler.Handle(levelUp));
            Assert.DoesNotThrow(() => ExpSpUpdateHandler.Handle(expSpUpdateLvlUp2));
            Assert.AreEqual(ExpAfterLvlUp2, Bot.CharData.ExpOffset);
            Assert.AreEqual(BotLvl2 + 1, Bot.CharData.Level);
        }
    }
}
