using LobotAPI;
using LobotDLL.Packets.Char;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Char
{
    [TestFixture]
    public class CharSpeedUpdateHandlerTests
    {
        private CharSpeedUpdateHandler CharSpeedUpdateHandler;
        private Packet speedCompleteGarment;
        private Packet speedIncompleteGarment;


        [SetUp]
        public void TestInitialize()
        {
            CharSpeedUpdateHandler = new CharSpeedUpdateHandler();
            Bot.CharData.UniqueID = 6414104;

            //[S -> C] [30D0]
            //18 DF 61 00                                       uniqueID
            //9A 99 99 41                                       speedFlag00
            //01 00 70 42                                       speedFlag01
            speedCompleteGarment = new Packet(0x30D0, false, false,
                new byte[] {0x18, 0xDF, 0x61, 0x00,
                            0x9A, 0x99, 0x99, 0x41,
                            0x01, 0x00, 0x70, 0x42 });

            //[S -> C] [30D0]
            //18 DF 61 00                                       ..a.............
            //00 00 80 41                                       ...A............
            //00 00 48 42                                       ..HB............
            speedIncompleteGarment = new Packet(0x30D0, false, false,
                new byte[] {0x18, 0xDF, 0x61, 0x00,
                            0x00, 0x00, 0x80, 0x41,
                            0x00, 0x00, 0x48, 0x42 });
        }

        [TearDown]
        public void TestCleanup()
        {
            CharSpeedUpdateHandler = null;
            speedCompleteGarment = null;
        }

        [Test]
        public void TestHandleSpeedCompleteGarmentUpdate()
        {
            Assert.DoesNotThrow(() => CharSpeedUpdateHandler.Handle(speedCompleteGarment));
        }

        [Test]
        public void TestHandleSpeedIncompleteGarmentUpdate()
        {
            Assert.DoesNotThrow(() => CharSpeedUpdateHandler.Handle(speedIncompleteGarment));
        }
    }
}