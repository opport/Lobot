using LobotAPI;
using LobotDLL.Packets.Char;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Char
{
    [TestFixture]
    public class CharStopMovementHandlerTests
    {
        private CharStopMovementHandler CharStopMovementHandler;
        Packet p;


        [SetUp]
        public void TestInitialize()
        {
            Bot.CharData.UniqueID = 114670184;
            CharStopMovementHandler = new CharStopMovementHandler();
            p = new Packet(0xB023, false, false,
                new byte[] { 0x68, 0xBA, 0xD5, 0x06,
                            0xA8, 0x60,
                            0x29, 0xEF, 0xB3, 0x43, 0x00, 0x00, 0x00, 0x00, 0x2E, 0x06, 0xEE, 0x44, 0xC6, 0x0E });
        }

        [TearDown]
        public void TestCleanup()
        {
            CharStopMovementHandler = null;
            p = null;
        }

        [Test]
        public void TestCharStopMovement()
        {
            Assert.DoesNotThrow(() => CharStopMovementHandler.Handle(p));
        }
    }
}