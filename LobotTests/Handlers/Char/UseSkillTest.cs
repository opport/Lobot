﻿using LobotDLL.Packets.Char;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Char
{
    [TestFixture]
    public class UseSkillTest
    {
        UseSkillHandler SkillHandler;
        Packet p;

        [SetUp]
        public void OneTimeSetUp()
        {
            SkillHandler = new UseSkillHandler();
            //[S -> C] [B070]
            //01                                                ................
            //02 30                                             .0..............
            //02 00 00 00                                       ................
            //1F C9 0A 00                                       ................
            //32 D9 03 00                                       2...............
            //84 60 0A 00                                       .`..............
            //01                                                ................
            //02                                                ................
            //01                                                ................
            //84 60 0A 00                                       .`..............
            //80                                                ................
            //01 01 03 00                                       ................
            //00 00 00 00                                       ................
            //08                                                ................      
            p = new Packet(0xB070, false, false,
                new byte[] {
                    0x01,
                    0x02, 0x30,
                    0x02, 0x00, 0x00, 0x00,
                    0x1F, 0xC9, 0x0A, 0x00,
                    0x32, 0xD9, 0x03, 0x00,
                    0x84, 0x60, 0x0A, 0x00,
                    0x01,
                    0x02,
                    0x01,
                    0x84, 0x60, 0x0A, 0x00,
                    0x80,
                    0x01, 0x01, 0x03, 0x00,
                    0x00, 0x00, 0x00, 0x00,
                    0x08,
                });
        }

        [TearDown]
        public void OneTimeTearDown()
        {
            SkillHandler = null;
            p = null;
        }

        [Test]
        public void SwordAutoAttack2HitsTest()
        {
            Assert.DoesNotThrow(() => SkillHandler.Handle(p));
        }
    }
}