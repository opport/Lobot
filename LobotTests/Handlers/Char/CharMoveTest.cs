﻿using LobotAPI;
using LobotDLL.Packets.Char;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Char
{

    [TestFixture]
    public class CharMoveTest
    {
        private CharMove CharMove;
        // Move east on the bridge out of hotan
        private Packet CharMoveHotanEastBridge1;
        // Move west on the bridge towards hotan
        private Packet CharMoveHotanEastBridge2;
        private uint tempBotUniqueID;


        [SetUp]
        public void TestInitialize()
        {
            CharMove = new CharMove();

            //[C -> S] [7021]
            //01                                                ................
            //89 5C                                             .\..............
            //91 FE                                             ................
            //0B 00                                             ................
            //E3 01                                             ................

            //[S -> C] [B021]
            //34 0E FD 00                                       4...............
            //01                                                ................
            //89 5C                                             .\..............
            //91 FE                                             ................
            //0B 00                                             ................
            //E3 01                                             ................
            //01                                                ................
            //89 5C                                             .\..............
            //0C 03                                             ................
            //63 15 35 41                                       c.5A............
            //5C 12                                             \...............
            CharMoveHotanEastBridge1 = new Packet(0xB021, false, false,
                new byte[] {
                    0x34, 0x0E, 0xFD, 0x00, 0x01, 0x89, 0x5C, 0x91, 0xFE, 0x0B, 0x00, 0xE3, 0x01, 0x01, 0x89, 0x5C, 0x0C, 0x03, 0x63, 0x15, 0x35, 0x41, 0x5C, 0x12
                }
            );

            //[C -> S] [7021]
            //01                                                ................
            //88 5C                                             .\..............
            //AC 05                                             ................
            //0B 00                                             ................
            //E4 01                                             ................

            //[S -> C] [B021]
            //34 0E FD 00                                       4...............
            //01                                                ................
            //88 5C                                             .\..............
            //AC 05                                             ................
            //0B 00                                             ................
            //E4 01                                             ................
            //01                                                ................
            //88 5C                                             .\..............
            //AD 3C                                             .<..............
            //7B 16 35 41                                       {.5A............
            //DD 12                                             ................
            CharMoveHotanEastBridge2 = new Packet(0xB021, false, false,
                new byte[] {
                    0x34, 0x0E, 0xFD, 0x00, 0x01, 0x88, 0x5C, 0xAC, 0x05, 0x0B, 0x00, 0xE4, 0x01, 0x01, 0x88, 0x5C, 0xAD, 0x3C, 0x7B, 0x16, 0x35, 0x41, 0xDD, 0x12
                }
            );

            tempBotUniqueID = 0xFD0E34;
        }

        [TearDown]
        public void TestCleanup()
        {
            CharMove = null;
            CharMoveHotanEastBridge1 = null;
            CharMoveHotanEastBridge2 = null;
        }

        [Test(Description = "CharMove test for moving into Hotan from the east gate. The final location should be close to 337,48")]
        public void CharMoveHotanEastGateTest()
        {
            Bot.CharData.UniqueID = tempBotUniqueID; // set to uniqueID in packet to correctly set new location
            Assert.DoesNotThrow(() => CharMove.Handle(CharMoveHotanEastBridge1));
            Assert.DoesNotThrow(() => CharMove.Handle(CharMoveHotanEastBridge2));
            Assert.True(Bot.CharData.Position.X < 350);
            Assert.True(Bot.CharData.Position.Y < 50);
        }
    }
}
