﻿using LobotDLL.Packets.Storage;
using NUnit.Framework;
using SilkroadSecurityApi;

namespace LobotTests.Handlers.Inventory
{
    [TestFixture]
    public class StorageInfoDataHandlerTest
    {
        StorageInfoDataHandler StorageHandler;
        Packet StorageEquipmentAndElixirs;
        Packet StorageAvatarItem;
        Packet StorageAlchemyStones;
        Packet StorageMallMagicAndRubyStones;

        //[S -> C] [0000]
        //96                                                ................
        //08                                                ................
        //01                                                ................
        //00 00 00 00                                       ................
        //AA 06 00 00                                       ................
        //05                                                ................
        //A0 B5 D6 1A 00 00 00 00                           ................
        //39 00 00 00                                       9...............
        //02                                                ................
        //45 00 00 00 03 00 00 00                           E...............
        //4B 00 00 00 03 00 00 00                           K...............
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //03                                                ................
        //00 00 00 00                                       ................
        //5E 07 00 00                                       ^...............
        //05                                                ................
        //AD 01 00 00 00 00 00 00                           ................
        //00 00 00 00                                       ................
        //07                                                ................
        //45 00 00 00 03 00 00 00                           E...............
        //4B 00 00 00 03 00 00 00                           K...............
        //B4 00 00 00 14 00 00 00                           ................
        //AE 00 00 00 14 00 00 00                           ................
        //A8 00 00 00 14 00 00 00                           ................
        //BA 00 00 00 14 00 00 00                           ................
        //C0 00 00 00 14 00 00 00                           ................
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //05                                                ................
        //00 00 00 00                                       ................
        //3E 06 00 00                                       >...............
        //05                                                ................
        //A0 B5 D6 1A 00 00 00 00                           ................
        //38 00 00 00                                       8...............
        //02                                                ................
        //45 00 00 00 03 00 00 00                           E...............
        //4B 00 00 00 03 00 00 00                           K...............
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //06                                                ................
        //00 00 00 00                                       ................
        //86 06 00 00                                       ................
        //05                                                ................
        //A0 B5 D6 1A 00 00 00 00                           ................
        //39 00 00 00                                       9...............
        //02                                                ................
        //45 00 00 00 03 00 00 00                           E...............
        //4B 00 00 00 03 00 00 00                           K...............
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //78                                                x...............
        //00 00 00 00                                       ................
        //43 11 00 00                                       C...............
        //00                                                ................
        //88 84 F6 06 00 00 00 00                           ................
        //40 00 00 00                                       @...............
        //02                                                ................
        //80 00 00 00 14 00 00 00                           ................
        //50 00 00 00 1E 00 00 00                           P...............
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //79                                                y...............
        //00 00 00 00                                       ................
        //77 16 00 00                                       w...............
        //00                                                ................
        //4A 01 00 00 00 00 00 00                           J...............
        //00 00 00 00                                       ................
        //01                                                ................
        //17 00 00 00 03 00 00 00                           ................
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //7A z...............
        //00 00 00 00                                       ................
        //DC 0F 00 00                                       ................
        //00                                                ................
        //4E 04 F4 C4 03 00 00 00                           N...............
        //64 00 00 00                                       d...............
        //01                                                ................
        //2F 00 00 00 05 00 00 00                           /...............
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................
        //7C                                                |...............
        //00 00 00 00                                       ................
        //A5 65 00 00                                       .e..............
        //01 00                                             ................
        [SetUp]
        public void OneTimeSetUp()
        {
            StorageHandler = new StorageInfoDataHandler();

            StorageEquipmentAndElixirs = new Packet(0x3049, false, false,
                new byte[] {
                    0x96, 0x08, 0x01, 0x00, 0x00, 0x00, 0x00, 0xAA, 0x06, 0x00, 0x00, 0x05, 0xA0, 0xB5, 0xD6, 0x1A,
                    0x00, 0x00, 0x00, 0x00, 0x39, 0x00, 0x00, 0x00, 0x02, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00,
                    0x00, 0x4B, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x00,
                    0x00, 0x00, 0x5E, 0x07, 0x00, 0x00, 0x05, 0xAD, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x07, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x4B, 0x00, 0x00, 0x00,
                    0x03, 0x00, 0x00, 0x00, 0xB4, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0xAE, 0x00, 0x00, 0x00,
                    0x14, 0x00, 0x00, 0x00, 0xA8, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0xBA, 0x00, 0x00, 0x00,
                    0x14, 0x00, 0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00,
                    0x05, 0x00, 0x00, 0x00, 0x00, 0x3E, 0x06, 0x00, 0x00, 0x05, 0xA0, 0xB5, 0xD6, 0x1A, 0x00, 0x00,
                    0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x02, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x4B,
                    0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00,
                    0x86, 0x06, 0x00, 0x00, 0x05, 0xA0, 0xB5, 0xD6, 0x1A, 0x00, 0x00, 0x00, 0x00, 0x39, 0x00, 0x00,
                    0x00, 0x02, 0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x03, 0x00,
                    0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x43, 0x11, 0x00, 0x00, 0x00,
                    0x88, 0x84, 0xF6, 0x06, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x02, 0x80, 0x00, 0x00,
                    0x00, 0x14, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02,
                    0x00, 0x79, 0x00, 0x00, 0x00, 0x00, 0x77, 0x16, 0x00, 0x00, 0x00, 0x4A, 0x01, 0x00, 0x00, 0x00,
                    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x17, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
                    0x01, 0x00, 0x02, 0x00, 0x7A, 0x00, 0x00, 0x00, 0x00, 0xDC, 0x0F, 0x00, 0x00, 0x00, 0x4E, 0x04,
                    0xF4, 0xC4, 0x03, 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x00, 0x01, 0x2F, 0x00, 0x00, 0x00, 0x05,
                    0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x7C, 0x00, 0x00, 0x00, 0x00, 0xA5, 0x65, 0x00, 0x00,
                    0x01, 0x00,
                });
            //[S -> C] [0000]
            //96                                                ................
            //0F                                                ................
            //02                                                ................
            //00 00 00 00                                       ................
            //A0 5F 00 00                                       ._..............
            //00                                                ................
            //00 00 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //00                                                ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //03                                                ................
            //00 00 00 00                                       ................
            //62 0E 00 00                                       b...............
            //03 00                                             ................
            //04                                                ................
            //00 00 00 00                                       ................
            //5F 0E 00 00                                       _...............
            //01 00                                             ................
            //05                                                ................
            //00 00 00 00                                       ................
            //60 0E 00 00                                       `...............
            //01 00                                             ................
            //06                                                ................
            //00 00 00 00                                       ................
            //61 0E 00 00                                       a...............
            //02 00                                             ................
            //5A Z...............
            //00 00 00 00                                       ................
            //5E 07 00 00                                       ^...............
            //05                                                ................
            //AD 01 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //07                                                ................
            //45 00 00 00 03 00 00 00                           E...............
            //4B 00 00 00 03 00 00 00                           K...............
            //B4 00 00 00 14 00 00 00                           ................
            //AE 00 00 00 14 00 00 00                           ................
            //A8 00 00 00 14 00 00 00                           ................
            //BA 00 00 00 14 00 00 00                           ................
            //C0 00 00 00 14 00 00 00                           ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //78                                                x...............
            //00 00 00 00                                       ................
            //02 1B 00 00                                       ................
            //01 00                                             ................
            //00                                                ................
            //79                                                y...............
            //00 00 00 00                                       ................
            //B2 1B 00 00                                       ................
            //01 00                                             ................
            //7A z...............
            //00 00 00 00                                       ................
            //AE 1A 00 00                                       ................
            //01 00                                             ................
            //00                                                ................
            //7B                                                {...............
            //00 00 00 00................
            //0A 19 00 00................
            //01 00................
            //7C |...............
            //00 00 00 00................
            //E2 19 00 00................
            //01 00................
            //7D                                                }...............
            //00 00 00 00                                       ................
            //E6 18 00 00                                       ................
            //01 00                                             ................
            //7E                                                ~...............
            //00 00 00 00                                       ................
            //5E 60 00 00                                       ^`..............
            //14 00                                             ................
            //82                                                ................
            //00 00 00 00                                       ................
            //02 1B 00 00                                       ................
            //01 00                                             ................
            //00                                                ................
            //83                                                ................
            //00 00 00 00                                       ................
            //AA 18 00 00                                       ................
            //01 00                                             ................
            StorageAvatarItem = new Packet(0x3049, false, false,
            new byte[] {
                0x96, 0x0F, 0x02, 0x00, 0x00, 0x00, 0x00, 0xA0, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x03, 0x00, 0x00,
                0x00, 0x00, 0x62, 0x0E, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x5F, 0x0E, 0x00,
                0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x60, 0x0E, 0x00, 0x00, 0x01, 0x00, 0x06, 0x00,
                0x00, 0x00, 0x00, 0x61, 0x0E, 0x00, 0x00, 0x02, 0x00, 0x5A, 0x00, 0x00, 0x00, 0x00, 0x5E, 0x07,
                0x00, 0x00, 0x05, 0xAD, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07,
                0x45, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x4B, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
                0xB4, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0xAE, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00,
                0xA8, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0xBA, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00,
                0xC0, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x78, 0x00, 0x00, 0x00,
                0x00, 0x02, 0x1B, 0x00, 0x00, 0x01, 0x00, 0x00, 0x79, 0x00, 0x00, 0x00, 0x00, 0xB2, 0x1B, 0x00,
                0x00, 0x01, 0x00, 0x7A, 0x00, 0x00, 0x00, 0x00, 0xAE, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x00, 0x7B,
                0x00, 0x00, 0x00, 0x00, 0x0A, 0x19, 0x00, 0x00, 0x01, 0x00, 0x7C, 0x00, 0x00, 0x00, 0x00, 0xE2,
                0x19, 0x00, 0x00, 0x01, 0x00, 0x7D, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x18, 0x00, 0x00, 0x01, 0x00,
                0x7E, 0x00, 0x00, 0x00, 0x00, 0x5E, 0x60, 0x00, 0x00, 0x14, 0x00, 0x82, 0x00, 0x00, 0x00, 0x00,
                0x02, 0x1B, 0x00, 0x00, 0x01, 0x00, 0x00, 0x83, 0x00, 0x00, 0x00, 0x00, 0xAA, 0x18, 0x00, 0x00,
                0x01, 0x00
            });
            //[S -> C] [0000]
            //96                                                ................
            //11                                                ................
            //00                                                ................
            //00 00 00 00                                       ................
            //61 0E 00 00                                       a...............
            //22 00                                             "...............
            //01                                                ................
            //00 00 00 00                                       ................
            //62 0E 00 00                                       b...............
            //2F 00                                             /...............
            //02                                                ................
            //00 00 00 00                                       ................
            //5F 0E 00 00                                       _...............
            //2A 00                                             *...............
            //03                                                ................
            //00 00 00 00                                       ................
            //60 0E 00 00                                       `...............
            //19 00                                             ................
            //04                                                ................
            //00 00 00 00                                       ................
            //7A 16 00 00                                       z...............
            //00                                                ................
            //20 00 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //01                                                ................
            //29 00 00 00 05 00 00 00                           )...............
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //05                                                ................
            //00 00 00 00                                       ................
            //A0 5F 00 00                                       ._..............
            //00                                                ................
            //00 00 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //00                                                ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //06                                                ................
            //00 00 00 00                                       ................
            //D0 1A 00 00                                       ................
            //01 00                                             ................
            //07                                                ................
            //00 00 00 00                                       ................
            //B2 1B 00 00                                       ................
            //01 00                                             ................
            //08                                                ................
            //00 00 00 00                                       ................
            //44 1D 00 00                                       D...............
            //BA 00                                             ................
            //09                                                ................
            //00 00 00 00                                       ................
            //AA 18 00 00                                       ................
            //01 00                                             ................
            //0A                                                ................
            //00 00 00 00                                       ................
            //46 12 00 00                                       F...............
            //00                                                ................
            //00 A4 E0 02 00 00 00 00                           ................
            //42 00 00 00                                       B...............
            //01                                                ................
            //05 00 00 00 03 00 00 00                           ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //0B                                                ................
            //00 00 00 00                                       ................
            //02 1B 00 00                                       ................
            //02 00                                             ................
            //00                                                ................
            //0C                                                ................
            //00 00 00 00                                       ................
            //5E 60 00 00                                       ^`..............
            //14 00                                             ................
            //0D                                                ................
            //00 00 00 00                                       ................
            //E6 18 00 00                                       ................
            //01 00                                             ................
            //0E                                                ................
            //00 00 00 00                                       ................
            //E2 19 00 00                                       ................
            //01 00                                             ................
            //0F                                                ................
            //00 00 00 00                                       ................
            //0A 19 00 00                                       ................
            //01 00                                             ................
            //10                                                ................
            //00 00 00 00                                       ................
            //AE 1A 00 00                                       ................
            //01 00                                             ................
            //00                                                ................
            StorageAlchemyStones = new Packet(0x3049, false, false,
            new byte[] {
                0x96, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x61, 0x0E, 0x00, 0x00, 0x22, 0x00, 0x01, 0x00, 0x00,
                0x00, 0x00, 0x62, 0x0E, 0x00, 0x00, 0x2F, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x5F, 0x0E, 0x00,
                0x00, 0x2A, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x60, 0x0E, 0x00, 0x00, 0x19, 0x00, 0x04, 0x00,
                0x00, 0x00, 0x00, 0x7A, 0x16, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x01, 0x29, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02,
                0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0xA0, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00,
                0x00, 0xD0, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0xB2, 0x1B, 0x00, 0x00,
                0x01, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x44, 0x1D, 0x00, 0x00, 0xBA, 0x00, 0x09, 0x00, 0x00,
                0x00, 0x00, 0xAA, 0x18, 0x00, 0x00, 0x01, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x46, 0x12, 0x00,
                0x00, 0x00, 0x00, 0xA4, 0xE0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x42, 0x00, 0x00, 0x00, 0x01, 0x05,
                0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00,
                0x02, 0x1B, 0x00, 0x00, 0x02, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x5E, 0x60, 0x00, 0x00,
                0x14, 0x00, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x18, 0x00, 0x00, 0x01, 0x00, 0x0E, 0x00, 0x00,
                0x00, 0x00, 0xE2, 0x19, 0x00, 0x00, 0x01, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x19, 0x00,
                0x00, 0x01, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0xAE, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x00,
            });
            //[S -> C] [0000]
            //96                                                ................
            //11                                                ................
            //00                                                ................
            //00 00 00 00                                       ................
            //61 0E 00 00                                       a...............
            //28 00                                             (...............
            //01                                                ................
            //00 00 00 00                                       ................
            //62 0E 00 00                                       b...............
            //36 00                                             6...............
            //02                                                ................
            //00 00 00 00                                       ................
            //5F 0E 00 00                                       _...............
            //31 00                                             1...............
            //03                                                ................
            //00 00 00 00                                       ................
            //60 0E 00 00                                       `...............
            //1F 00                                             ................
            //04                                                ................
            //00 00 00 00                                       ................
            //7A 16 00 00                                       z...............
            //00                                                ................
            //20 00 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //01                                                ................
            //29 00 00 00 05 00 00 00                           )...............
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //05                                                ................
            //00 00 00 00                                       ................
            //A0 5F 00 00                                       ._..............
            //00                                                ................
            //00 00 00 00 00 00 00 00                           ................
            //00 00 00 00                                       ................
            //00                                                ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //06                                                ................
            //00 00 00 00                                       ................
            //D0 1A 00 00                                       ................
            //01 00                                             ................
            //07                                                ................
            //00 00 00 00                                       ................
            //B2 1B 00 00                                       ................
            //01 00                                             ................
            //08                                                ................
            //00 00 00 00                                       ................
            //44 1D 00 00                                       D...............
            //BA 00                                             ................
            //09                                                ................
            //00 00 00 00                                       ................
            //AA 18 00 00                                       ................
            //01 00                                             ................
            //0A                                                ................
            //00 00 00 00                                       ................
            //46 12 00 00                                       F...............
            //00                                                ................
            //00 A4 E0 02 00 00 00 00                           ................
            //42 00 00 00                                       B...............
            //01                                                ................
            //05 00 00 00 03 00 00 00                           ................
            //01                                                ................
            //00                                                ................
            //02                                                ................
            //00                                                ................
            //0B                                                ................
            //00 00 00 00                                       ................
            //02 1B 00 00                                       ................
            //02 00                                             ................
            //00                                                ................
            //0C                                                ................
            //00 00 00 00                                       ................
            //5E 60 00 00                                       ^`..............
            //14 00                                             ................
            //0D                                                ................
            //00 00 00 00                                       ................
            //E6 18 00 00                                       ................
            //01 00                                             ................
            //0E                                                ................
            //00 00 00 00                                       ................
            //E2 19 00 00                                       ................
            //01 00                                             ................
            //0F                                                ................
            //00 00 00 00                                       ................
            //0A 19 00 00                                       ................
            //01 00                                             ................
            //10                                                ................
            //00 00 00 00                                       ................
            //AE 1A 00 00                                       ................
            //01 00                                             ................
            //00                                                ................
            StorageMallMagicAndRubyStones = new Packet(0x3049, false, false,
            new byte[] {
                0x96, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x61, 0x0E, 0x00, 0x00, 0x28, 0x00, 0x01, 0x00, 0x00,
                0x00, 0x00, 0x62, 0x0E, 0x00, 0x00, 0x36, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x5F, 0x0E, 0x00,
                0x00, 0x31, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x60, 0x0E, 0x00, 0x00, 0x1F, 0x00, 0x04, 0x00,
                0x00, 0x00, 0x00, 0x7A, 0x16, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x01, 0x29, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02,
                0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0xA0, 0x5F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00,
                0x00, 0xD0, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0xB2, 0x1B, 0x00, 0x00,
                0x01, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x44, 0x1D, 0x00, 0x00, 0xBA, 0x00, 0x09, 0x00, 0x00,
                0x00, 0x00, 0xAA, 0x18, 0x00, 0x00, 0x01, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x46, 0x12, 0x00,
                0x00, 0x00, 0x00, 0xA4, 0xE0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x42, 0x00, 0x00, 0x00, 0x01, 0x05,
                0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00,
                0x02, 0x1B, 0x00, 0x00, 0x02, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x5E, 0x60, 0x00, 0x00,
                0x14, 0x00, 0x0D, 0x00, 0x00, 0x00, 0x00, 0xE6, 0x18, 0x00, 0x00, 0x01, 0x00, 0x0E, 0x00, 0x00,
                0x00, 0x00, 0xE2, 0x19, 0x00, 0x00, 0x01, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x19, 0x00,
                0x00, 0x01, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0xAE, 0x1A, 0x00, 0x00, 0x01, 0x00, 0x00,
            });
        }

        [TearDown]
        public void OneTimeTearDown()
        {
            StorageHandler = null;
            StorageEquipmentAndElixirs = null;
            StorageAvatarItem = null;
            StorageAlchemyStones = null;
            StorageMallMagicAndRubyStones = null;
        }

        [Test]
        public void StorageDataEquipmentTest()
        {
            Assert.DoesNotThrow(() => StorageHandler.Handle(StorageEquipmentAndElixirs));
        }

        [Test]
        public void StorageAvatarItemTest()
        {
            Assert.DoesNotThrow(() => StorageHandler.Handle(StorageAvatarItem));
        }

        [Test]
        public void StorageAlchemyStonesTest()
        {
            Assert.DoesNotThrow(() => StorageHandler.Handle(StorageAlchemyStones));
        }

        [Test]
        public void StorageMallMagicAndRubyStonesTest()
        {
            Assert.DoesNotThrow(() => StorageHandler.Handle(StorageMallMagicAndRubyStones));
        }
    }
}
