﻿using LobotAPI;

using NUnit.Framework;
using System;
using System.Text.RegularExpressions;

namespace LobotTests.Logger
{
    [TestFixture]
    public class LoggerTests
    {
        private string TestMessage = "This is a test";
        private Array LoggerTypes = Enum.GetValues(typeof(LogLevel));
        private static string RegexPrefix = @"\d{2}:\d{2}";
        private Regex[] Regex = new Regex[] {
            new Regex(RegexPrefix +@"\[DBG\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[INFO\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[WARNING\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[ERROR\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[BOT\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[SCRIPT\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[PK2\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[NET\]" +"This is a test"),
            new Regex(RegexPrefix +@"\[HANDLER\]" +"This is a test"),
            new Regex(RegexPrefix +"-----" +"This is a test"),
        };

        [OneTimeSetUp]
        public void TestInitialize()
        {
            LobotAPI.Logger.LogList.Clear();

            foreach (LogLevel level in LoggerTypes)
            {
                LobotAPI.Logger.Log(level, TestMessage);
            }
        }


        [OneTimeTearDown]
        public void TestCleanup()
        {
            LobotAPI.Logger.LogList.Clear();
        }

        [Test(Description = "Logger test for all logger levels")]
        public void LoggerTestAllLevels()
        {
            for (int i = 0; i < LoggerTypes.Length; i++)
            {
                Assert.True(Regex[i].IsMatch(LobotAPI.Logger.LogList[i]));
            }
        }
    }
}
