﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Scripting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class ScriptUtilsTests
    {
        List<AbstractExpression> script = new List<AbstractExpression>
        {
            new CommandExpression("go", new string[] { "6485", "1120" }),
            new CommandExpression("go", new string[] { "6490", "1120" }),
            new CommandExpression("go", new string[] { "6495", "1120" })
        };
        private const int TOTALDISTANCE = 131;
        private readonly Point currentLocation = new Point(6480, 1120);
        Dictionary<string, Point> LocationsInTowns = new Dictionary<string, Point>
        {
            {"jangan", new Point(6480, 1120) },
            {"donwang", new Point(3547, 2063) },
            {"hotan", new Point(114,23) },
            {"samarkand", new Point(-5179,2843) },
            {"constantinople", new Point(-10648,2622) },
            {"alexandria north", new Point(-16105,39) },
            {"alexandria south", new Point(-16609,-310) },
        };

        ushort destinationX = 6673;
        ushort destinationY = 1314;
        ushort originX = 6675;//(actually fraction 6674)
        ushort originY = 1343;

        private byte xSector = 169;
        private byte ySector = 98;

        private ushort x = 1459;
        private ushort y = 1629;
        //private ushort z = 0;

        private ushort xOrigin = 14700;
        private ushort yOrigin = 19140;
        //private ushort zOrigin = 44753;

        private readonly Point botPosition1 = new Point(600, 100);
        private readonly Point botPosition2 = new Point(590, -100);
        private readonly Area area1 = new Area(new Point(565, -20), 60);

        [Test(Description = "ScriptUtils test for the index of the closest point in the script.")]
        public void IndexOfClosestPointTest()
        {
            double result;
            result = ScriptUtils.IndexOfClosestPoint(currentLocation, script);
            Assert.AreEqual(0, result);

        }

        [Test(Description = "ScriptUtils test for the index of the closest point an area. The result should not be the center point (index > 0)")]
        public void IndexOfClosestAreaPointTest()
        {
            List<Point> pointList = area1.AdjacencyList.Keys.ToList();

            int result;
            result = ScriptUtils.IndexOfClosestPoint(botPosition1, pointList);
            Point p = pointList[result];
            Assert.Greater(result, 0);
            Assert.GreaterOrEqual(p.X, area1.X);
            Assert.GreaterOrEqual(p.Y, area1.Y);

        }

        [Test(Description = "ScriptUtils test for the index of the closest point an area. The result should not be the center point (index > 0)")]
        public void IndexOfClosestAreaPoint2Test()
        {
            List<Point> pointList = area1.AdjacencyList.Keys.ToList();

            int result;
            result = ScriptUtils.IndexOfClosestPoint(botPosition2, pointList);
            Point p = pointList[result];
            Assert.Greater(result, 0);
            Assert.Greater(p.X, area1.X);
            Assert.Less(p.Y, area1.Y);

        }

        [Test(Description = "ScriptUtils test to calculate distance using two points.")]
        public void GetDistanceTestTwoPoints()
        {
            CommandExpression command = (CommandExpression)script[0];
            Point nextPoint = new Point(Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
            double result = ScriptUtils.GetDistance(currentLocation, nextPoint);
            Assert.AreEqual(5, result);
        }

        [Test(Description = "ScriptUtils test to calculate distance using a point and two coords.")]
        public void GetDistancePointAndCoords()
        {
            CommandExpression command = (CommandExpression)script[0];
            Point nextPoint = new Point(Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
            double result = ScriptUtils.GetDistance(currentLocation, Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
            Assert.AreEqual(5, result);
        }

        [Test(Description = "ScriptUtils test to calculate distance using two points.")]
        public void GetDistanceTestFourCoords()
        {
            CommandExpression command = (CommandExpression)script[0];
            Point nextPoint = new Point(Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
            double result = ScriptUtils.GetDistance(currentLocation.X, currentLocation.Y,
                 Convert.ToDouble(command.Arguments[0]), Convert.ToDouble(command.Arguments[1]));
            Assert.AreEqual(5, result);
        }

        [Test(Description = "ScriptUtils test to calculate distance using a point and two coords.")]
        public void GetDistanceTwoCommands()
        {
            CommandExpression firstCommand = (CommandExpression)script[0];
            double result = ScriptUtils.GetDistance(firstCommand, (CommandExpression)script[1]);
            Assert.AreEqual(5, result);
        }

        [Test(Description = "ScriptUtils test for checking whether character is in town. Returns boolean")]
        public void IsInTownTest()
        {
            Assert.IsTrue(ScriptGlobal.Towns["jangan"].IsInTown(currentLocation));
        }

        [Test(Description = "ScriptUtils test for checking whether character is in town. Returns boolean")]
        public void IsInTownAllTest()
        {
            foreach (KeyValuePair<string, Point> kvp in LocationsInTowns)
            {
                Assert.IsTrue(ScriptGlobal.Towns[kvp.Key].IsInTown(kvp.Value));
            }
        }

        [Test(Description = "ScriptUtils test for finding current town. Returns a Town enum")]
        public void CurrentTownTest()
        {
            Assert.AreEqual("Jangan", ScriptUtils.CurrentTown(currentLocation).Name);
        }

        [Test(Description = "ScriptUtils test for finding the first walk point.")]
        public void GetFirstWalkPointTest()
        {
            CommandExpression expCommand = (CommandExpression)script[0];
            Point exp = new Point(Convert.ToDouble(expCommand.Arguments[0]), Convert.ToDouble(expCommand.Arguments[1]));
            Assert.AreEqual(exp, ScriptUtils.GetFirstWalkPoint(new List<AbstractExpression>(script)));
        }

        [Test(Description = "ScriptUtils test for finding the index of the first walk point.")]
        public void GetFirstWalkPointIndexTest()
        {
            Assert.AreEqual(0, ScriptUtils.GetFirstWalkPointIndex((new List<AbstractExpression>(script))));
        }

        [Test(Description = "ScriptUtils test for matching a metadata requirement. Returns boolean")]
        public void MatchesRequirementTest()
        {
            Assert.IsTrue(ScriptUtils.MatchesRequirement(new MetaExpression("StartingTown", "Jangan"), new List<AbstractExpression>(script)));
        }

        [Test(Description = "ScriptUtils test for matching requirements of metdata. Returns boolean")]
        public void MatchesRequirementsTest()
        {
            Assert.IsTrue(ScriptUtils.MatchesRequirements(new List<AbstractExpression>(script)));
        }
        //from	x 6674	y 1343
        //to x 6673	y 1314
        [Test(Description = "ScriptUtils test for calculating the x-coordinate from movement packet")]
        public void GetXCoordTest()
        {
            Assert.AreEqual(destinationX, ScriptUtils.GetXCoord(x, xSector));
        }

        [Test(Description = "ScriptUtils test for calculating the y-coordinate from movement packet")]
        public void GetYCoordTest()
        {
            Assert.AreEqual(destinationY, ScriptUtils.GetYCoord(y, ySector));
        }

        [Test(Description = "ScriptUtils test for calculating the origin x-coordinate from movement packet")]
        public void GetXCoordOriginTest()
        {
            Assert.AreEqual(originX, ScriptUtils.GetXCoordOrigin(xOrigin, xSector));
        }

        [Test(Description = "ScriptUtils test for calculating the origin y-coordinate from movement packet")]
        public void GetYCoordOriginTest()
        {
            Assert.AreEqual(originY, ScriptUtils.GetYCoordOrigin(yOrigin, ySector));
        }

        [Test(Description = "ScriptUtils test for for calculating the x-sector to send with a movement packet")]
        public void GetSectorXTest()
        {
            Assert.AreEqual(xSector, ScriptUtils.GetSectorX(6673));
        }

        [Test(Description = "ScriptUtils test for calculating the y-sector to send with a movement packet")]
        public void GetSectorYTest()
        {
            Assert.AreEqual(ySector, ScriptUtils.GetSectorY(1343));
        }
    }
}