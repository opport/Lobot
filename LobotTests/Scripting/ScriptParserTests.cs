﻿using LobotAPI.Scripting;
using LobotTests;
using NUnit.Framework;
using System.Collections.Generic;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class ScriptParserTests
    {
        CommandExpression[] ExpectedAllCommands = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("store"),
                new CommandExpression("stable"),
                new CommandExpression("repair"),
                new CommandExpression("potions"),
                new CommandExpression("special"),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };

        [Test(Description = "ScriptParser test for getting extracting commands from script file.")]
        public void CommandsParseAllTest()
        {
            string scriptPath = TestsSetupFixtureHelper.DataFolderPath + @"\Scripts\command_all_commands.txt";
            List<AbstractExpression> results = ExpressionParser.ParseScript(scriptPath);

            Assert.AreEqual(results.Count, ExpectedAllCommands.Length);

            for (int i = 0; i < ExpectedAllCommands.Length; i++)
            {
                Assert.AreEqual(ExpectedAllCommands[i], results[i]);
            }

        }

    }
}