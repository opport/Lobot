﻿using LobotAPI.Scripting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LobotTests.Scripting
{
    [TestFixture]
    public class AreaTest
    {
        private Area TestArea;

        [SetUp]
        public void TestInitialize()
        {
        }

        [Test(Description = "Make sure area adjacency list includes its center x,y for a radius of 0")]
        public void AreaTestRadius0()
        {
            TestArea = new Area(new Point(0, 0), 0);
            Assert.AreEqual(1, TestArea.AdjacencyList.Count);
        }

        [Test(Description = "Make sure area finds the minimum amount of points in a given radius")]
        public void AreaTestRadius15()
        {
            TestArea = new Area(new Point(0, 0), 15);
            Assert.AreEqual(5, TestArea.AdjacencyList.Count);
        }

        [Test(Description = "Make sure area points do not refer to themselves in their adjacencylists")]
        public void AreaTestRadius50()
        {
            TestArea = new Area(new Point(0, 0), 50);

            foreach (KeyValuePair<Point, HashSet<Point>> kvp in TestArea.AdjacencyList)
            {
                Assert.IsFalse(kvp.Value.Contains(kvp.Key));
            }
        }
    }
}
