﻿using LobotAPI.Scripting;
using NUnit.Framework;
using System;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class CommentExpressionTests
    {
        [Test()]
        public void CommentExpressionTest()
        {
            string test = "// test";
            CommentExpression comment = new CommentExpression(test);

            Assert.IsInstanceOf(typeof(CommentExpression), comment);
        }

        [Test()]
        public void CommentExpressionTestInvalid()
        {
            string test = " test";
            //CommentExpression comment = new CommentExpression(test);

            Assert.That(() => new CommentExpression(test),
                Throws.TypeOf<FormatException>());
        }

        [Test()]
        public void EqualsTestComment()
        {
            string test = "// test";
            CommentExpression comment = new CommentExpression(test);
            Assert.IsTrue(comment.Equals(new CommentExpression(test)));
        }

        [Test()]
        public void EqualsTestNotEqual()
        {
            string test = "// test";
            CommentExpression comment1 = new CommentExpression(test);
            CommentExpression comment2 = new CommentExpression(test + " ");
            Assert.IsFalse(comment1.Equals(comment2));
        }

        [Test()]
        public void ToStringTestComment()
        {
            string test = "// test";
            CommentExpression comment = new CommentExpression(test);

            Assert.AreEqual(test, comment.ToString());
        }
    }
}