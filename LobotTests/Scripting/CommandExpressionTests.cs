﻿using LobotAPI.Scripting;
using NUnit.Framework;
using System;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class CommandExpressionTests
    {
        CommandExpression[] expectedEXPR;
        string[] testStrings;

        [Test()]
        public void CommandExpressionTest()
        {
            testStrings = new string[]
            {
                "go 0,0,0",
                "go 0,0",
                "wait",
                "wait 1000",
                "teleport reverseRecall",
                "quest 1000,1",
                "mount",
                "dismount",
                "setArea1 0,0,0",
                "setArea2 0,0,0"
            };
            CommandExpression[] result = new CommandExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new CommandExpression(testStrings[i]);
            }

            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void CommandExpressionTestSpaces()
        {
            testStrings = new string[]
            {
                "go 0 ,0,0",
                "go 0, 0",
                " wait",
                "wait  1000",
                "teleport reverseRecall ",
                "quest 1000,1 ",
                " mount",
                "dismount ",
                "setArea1  0, 0, 0",
                "setArea2 0 , 0 , 0"
            };
            CommandExpression[] result = new CommandExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new CommandExpression(testStrings[i]);
            }

            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void CommandExpressionComments()
        {
            testStrings = new string[]
            {
                "go 0,0,0 //one",
                "go 0,0 // two",
                "wait // //",
                "wait 1000 ////",
                "teleport reverseRecall // // another one",
                "quest 1000,1 // all i do is bot",
                "mount // comment //",
                "dismount // oh // baby // a triple",
                "setArea1 0,0,0 // romcomment",
                "setArea2 0,0,0 // // // //"
            };
            CommandExpression[] result = new CommandExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new CommandExpression(testStrings[i]);
            }

            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void CommandExpressionTestTeleport()
        {
            testStrings = new string[]
            {
                "teleport 1,1",
                "teleport 0,0",
                "teleport 1000,0",
                "teleport 1000,1",
                "teleport 1,0",
                "teleport reverseRecall",
                "teleport reverseDeath",
                "teleport reverseLocation location",
            };
            CommandExpression[] result = new CommandExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new CommandExpression(testStrings[i]);
            }

            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("teleport", new string[] { "1" , "1"} ),
                new CommandExpression("teleport", new string[] { "0", "0" } ),
                new CommandExpression("teleport", new string[] { "1000", "0" } ),
                new CommandExpression("teleport", new string[] { "1000", "1" } ),
                new CommandExpression("teleport", new string[] { "1", "0" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("teleport", new string[] { "reverseDeath" } ),
                new CommandExpression("teleport", new string[] { "reverseLocation", "location" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }

        }

        [Test()]
        public void CommandExpressionTestTeleportInvalid()
        {
            testStrings = new string[]
            {
                "teleport 1,-1",
                "teleport 0,-0",
                "teleport -1000,0",
                "teleport -1000,1",
                "teleport 1,",
                "teleport rreverseRecall",
                "teleport reverseDeathh",
                "teleport reverseLocation locationn",
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => new CommandExpression(testStrings[i]), "Invalid CMD_EXPR: " + testStrings[i]);
            }

        }

        [Test()]
        public void CommandExpressionTestInvalid()
        {
            testStrings = new string[]
            {
                "go 0,0,-1",
                "go 0,0,",
                "shoop",
                "waitt",
                "wait -1000",
                "teleport teleporter 1000,-1",
                "quest 1000,-1",
                "mmount",
                "dismount 1",
                "setArea1 0,0,-1",
                "setArea2 0,0,-1"
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => new CommandExpression(testStrings[i]), "Invalid CMD_EXPR: " + testStrings[i]);
            }
        }

        [Test()]
        public void EqualsTestCommand()
        {
            CommandExpression[] result = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "1000", "1" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };
            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0", "0", "0" } ),
                new CommandExpression("go", new string[] { "0", "0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "1000", "1" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0", "0", "0" } ),
                new CommandExpression("setArea2", new string[] { "0", "0", "0" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.IsTrue(expectedEXPR[i].Equals(result[i]));
            }
        }

        [Test()]
        public void EqualsTestNotEqual()
        {
            CommandExpression[] result = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0", "0", "0" } ),
                new CommandExpression("go", new string[] { "0", "0" } ),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "1000", "1" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("setArea1", new string[] { "0", "0", "0" } ),
                new CommandExpression("setArea2", new string[] { "0", "0", "0" } )
            };
            expectedEXPR = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "1", "0", "0" } ),
                new CommandExpression("go", new string[] { "1", "0" } ),
                new CommandExpression("wait", new string[] { "1001" } ),
                new CommandExpression("teleport", new string[] { "1001", "1" } ),
                new CommandExpression("quest", new string[] { "1001", "1" } ),
                new CommandExpression("setArea1", new string[] { "1", "0", "0" } ),
                new CommandExpression("setArea2", new string[] { "1", "0", "0" } )
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreNotEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void ToStringTestCommand()
        {
            testStrings = new string[]
            {
                "go 0,0,0",
                "go 0,0",
                "wait",
                "wait 1000",
                "teleport reverseRecall",
                "quest 1000,1",
                "mount",
                "dismount",
                "setArea1 0,0,0",
                "setArea2 0,0,0"
            };

            String[] exp = new string[]
            {
                "go 0, 0, 0",
                "go 0, 0",
                "wait",
                "wait 1000",
                "teleport reverseRecall",
                "quest 1000, 1",
                "mount",
                "dismount",
                "setArea1 0, 0, 0",
                "setArea2 0, 0, 0"
            };

            CommandExpression[] commands = new CommandExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                commands[i] = new CommandExpression(testStrings[i]);
            }

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.AreEqual(exp[i], commands[i].ToString());
            }

        }
    }
}