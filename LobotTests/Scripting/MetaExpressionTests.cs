﻿using LobotAPI.Scripting;
using NUnit.Framework;
using System;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class MetaExpressionTests
    {
        MetaExpression[] expectedEXPR;
        string[] testStrings;

        [Test()]
        public void MetaExpressionTestValidLines()
        {
            testStrings = new string[]
            {
               "StartingTown Jangan",
                "MonsterLevel 1",
                "MonsterDifficulty 1",
                "ScriptDifficulty 1",
                "NextScript 1", //MonsterLevel(,MonsterDifficulty)?;
                 "NextScript 1,1" //MonsterLevel(,MonsterDifficulty)?;
            };
            MetaExpression[] result = new MetaExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new MetaExpression(testStrings[i]);
            }

            expectedEXPR = new MetaExpression[]
            {
                new MetaExpression("StartingTown", "Jangan"),
                new MetaExpression("MonsterLevel", "1"),
                new MetaExpression("MonsterDifficulty", "1"),
                new MetaExpression("ScriptDifficulty", "1"),
                new MetaExpression("NextScript", "1"),
                new MetaExpression("NextScript", "1", "1")
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void MetaExpressionTestValidLinesSpaces()
        {
            testStrings = new string[]
            {
               "StartingTown  Jangan",
                "MonsterLevel 1 ",
                "MonsterDifficulty 1  ",
                "ScriptDifficulty   1",
                "NextScript 1 ", //MonsterLevel(,MonsterDifficulty)?;
                 "NextScript 1 , 1 " //MonsterLevel(,MonsterDifficulty)?;
            };
            MetaExpression[] result = new MetaExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new MetaExpression(testStrings[i]);
            }

            expectedEXPR = new MetaExpression[]
            {
                new MetaExpression("StartingTown", "Jangan"),
                new MetaExpression("MonsterLevel", "1"),
                new MetaExpression("MonsterDifficulty", "1"),
                new MetaExpression("ScriptDifficulty", "1"),
                new MetaExpression("NextScript", "1"),
                new MetaExpression("NextScript", "1", "1")
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void MetaExpressionTestValidComments()
        {
            testStrings = new string[]
            {
               "StartingTown Jangan // one",
                "MonsterLevel 1 //two",
                "MonsterDifficulty 1 //three ",
                "ScriptDifficulty 1 // four //",
                "NextScript 1 // five // //",
                 "NextScript 1,1 ////"
            };
            MetaExpression[] result = new MetaExpression[testStrings.Length];
            for (int i = 0; i < testStrings.Length; i++)
            {
                result[i] = new MetaExpression(testStrings[i]);
            }

            expectedEXPR = new MetaExpression[]
            {
                new MetaExpression("StartingTown", "Jangan"),
                new MetaExpression("MonsterLevel", "1"),
                new MetaExpression("MonsterDifficulty", "1"),
                new MetaExpression("ScriptDifficulty", "1"),
                new MetaExpression("NextScript", "1"),
                new MetaExpression("NextScript", "1", "1")
            };

            for (int i = 0; i < expectedEXPR.Length; i++)
            {
                Assert.AreEqual(expectedEXPR[i], result[i]);
            }
        }

        [Test()]
        public void MetaExpressionTestInvalidCommands()
        {
            testStrings = new string[]
            {
               "StartingTownJangan",
                "MonsterLevell 1",
                "Monster Difficulty 1",
                "ScriptDifficult 1",
                "NNextScript 1" //MonsterLevel(,MonsterDifficulty)?;
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => new MetaExpression(testStrings[i]), "Invalid META_EXPR: " + testStrings[i]);
            }
        }

        [Test()]
        public void MetaExpressionTestInvalidArgumentsLowBound()
        {
            testStrings = new string[]
            {
               "StartingTown angan",
                "MonsterLevel -1",
                "MonsterDifficulty -1",
                "ScriptDifficulty -1",
                "NextScript -1", //MonsterLevel(,MonsterDifficulty)?;
                "NextScript -1,-1"
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => new MetaExpression(testStrings[i]), "Invalid META_EXPR: " + testStrings[i]);
            }
        }

        [Test()]
        public void MetaExpressionTestInvalidArgumentsUpperBound()
        {
            testStrings = new string[]
            {
               "StartingTown angan",
                "MonsterLevel 101",
                "MonsterDifficulty 11",
                "ScriptDifficulty 11",
                "NextScript 101", //MonsterLevel(,MonsterDifficulty)?;
                "NextScript 101,11"
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => new MetaExpression(testStrings[i]), "Invalid META_EXPR: " + testStrings[i]);
            }
        }

        [Test()]
        public void MetaExpressionExecuteTest()
        {
            testStrings = new string[]
               {
               "StartingTown Jangan",
                "MonsterLevel 1",
                "MonsterDifficulty 1",
                "ScriptDifficulty 1",
                "NextScript 1" //MonsterLevel(,MonsterDifficulty)?;
               };

            expectedEXPR = new MetaExpression[]
                {
                    new MetaExpression("StartingTown", "Jangan"),
                    new MetaExpression("MonsterLevel", "1"),
                    new MetaExpression("MonsterDifficulty", "1"),
                    new MetaExpression("ScriptDifficulty", "1"),
                    new MetaExpression("NextScript", "1" )
                };

            foreach (MetaExpression expr in expectedEXPR)
            {
                Assert.Throws<NotImplementedException>(() => expr.Execute());
            }
        }

    }
}