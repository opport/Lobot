﻿using LobotAPI.Scripting;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace LobotDLL.Scripting.Tests
{
    [TestFixture()]
    public class ExpressionParserTests
    {

        String[] testStrings;
        AbstractExpression[] expResult;

        [Test(Description = "Parser test for comment-only script. Should return an empty commandlist")]
        public void ParseScriptTestCommentsOnly()
        {
            String[] script = {
                "// this",
                "// is",
                "// a",
                "// test"
            };

            List<AbstractExpression> result = ExpressionParser.ParseScript(script);

            for (int i = 0; i < script.Length; i++)
            {
                Assert.IsEmpty(result);
            }
        }

        [Test(Description = "Parser test for metaCommand-only script. Should return a list containing metaCommands.")]
        public void ParseScriptTestMetasOnly()
        {
            String[] script = {
                "StartingTown Jangan",
                "MonsterLevel 1",
                "MonsterDifficulty 1",
                "ScriptDifficulty 1",
                "NextScript 1", //MonsterLevel(,MonsterDifficulty)?;
                 "NextScript 1,1" //MonsterLevel(,MonsterDifficulty)?;
            };

            MetaExpression[] expResult =
            {
                new MetaExpression("StartingTown" , "Jangan" ),
                new MetaExpression("MonsterLevel" , "1"),
                new MetaExpression("MonsterDifficulty" , "1"),
                new MetaExpression("ScriptDifficulty" , "1"),
                new MetaExpression("NextScript" , "1"),
                new MetaExpression("NextScript" , "1", "1")
            };

            List<AbstractExpression> result = ExpressionParser.ParseScript(script);

            for (int i = 0; i < script.Length; i++)
            {
                Assert.AreEqual(expResult[i], result[i]);
            }
        }


        [Test(Description = "Parser test for metaCommand-only script. Should throw exceptions.")]
        public void ParseScriptTestMetasInvalidCommands()
        {
            testStrings = new string[]
            {
               "StartingTownJangan",
                "MonsterLevell 1",
                "Monster Difficulty 1",
                "ScriptDifficult 1",
                "NNextScript 1" //MonsterLevel(,MonsterDifficulty)?;
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => ExpressionParser.ParseScript(testStrings), "Invalid line: " + testStrings[i]);
            }
        }

        [Test(Description = "Parser test for command-only script. Should return a list containing commands.")]
        public void ParseScriptTestCommandsOnly()
        {
            testStrings = new string[]
            {
                "go 0,0,0",
                "go 0,0",
                "wait",
                "wait 1000",
                "teleport reverseRecall",
                "quest 1000,1",
                "mount",
                "dismount",
                "setArea1 0,0,0",
                "setArea2 0,0,0"
            };

            List<AbstractExpression> result = ExpressionParser.ParseScript(testStrings);

            expResult = new CommandExpression[]
            {
                new CommandExpression("go", new string[] { "0" ,"0","0"} ),
                new CommandExpression("go", new string[] { "0","0" } ),
                new CommandExpression("wait"),
                new CommandExpression("wait", new string[] { "1000" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("quest", new string[] { "1000", "1" } ),
                new CommandExpression("mount"),
                new CommandExpression("dismount"),
                new CommandExpression("setArea1", new string[] { "0","0","0" } ),
                new CommandExpression("setArea2", new string[] { "0","0","0" } )
            };

            for (int i = 0; i < expResult.Length; i++)
            {
                Assert.AreEqual(expResult[i], result[i]);
            }
        }

        [Test(Description = "Parser test for teleport commands. Should return a list containing commands.")]
        public void ParseScriptTestCommandExpressionTeleport()
        {
            testStrings = new string[]
            {
                "teleport 1,1",
                "teleport 0,0",
                "teleport 1000,0",
                "teleport 1000,1",
                "teleport 1,0",
                "teleport reverseRecall",
                "teleport reverseDeath",
                "teleport reverseLocation location",
            };

            List<AbstractExpression> result = ExpressionParser.ParseScript(testStrings);

            expResult = new CommandExpression[]
            {
                new CommandExpression("teleport", new string[] { "1" , "1"} ),
                new CommandExpression("teleport", new string[] { "0", "0" } ),
                new CommandExpression("teleport", new string[] { "1000", "0" } ),
                new CommandExpression("teleport", new string[] { "1000", "1" } ),
                new CommandExpression("teleport", new string[] { "1", "0" } ),
                new CommandExpression("teleport", new string[] { "reverseRecall" } ),
                new CommandExpression("teleport", new string[] { "reverseDeath" } ),
                new CommandExpression("teleport", new string[] { "reverseLocation", "location" } )
            };

            for (int i = 0; i < expResult.Length; i++)
            {
                Assert.AreEqual(expResult[i], result[i]);
            }

        }

        [Test(Description = "Parser test for invalid teleport commands. Should throw exceptions.")]
        public void ParseScriptTestCommandExpressionTeleportInvalid()
        {
            testStrings = new string[]
            {
                "teleport 1,-1",
                "teleport 0,-0",
                "teleport -1000,0",
                "teleport -1000,1",
                "teleport 1,",
                "teleport rreverseRecall",
                "teleport reverseDeathh",
                "teleport reverseLocation locationn",
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => ExpressionParser.ParseScript(testStrings), "Invalid CMD_EXPR: " + testStrings[i]);
            }

        }

        [Test(Description = "Parser test for invalid commands. Should throw exceptions.")]
        public void CommandExpressionInvalid()
        {
            testStrings = new string[]
            {
                "go 0,0,-1",
                "go 0,0,",
                "shoop",
                "waitt",
                "wait -1000",
                "teleport teleporter 1000,-1",
                "quest 1000,-1",
                "mmount",
                "dismount 1",
                "setArea1 0,0,-1",
                "setArea2 0,0,-1"
            };

            for (int i = 0; i < testStrings.Length; i++)
            {
                Assert.Throws<FormatException>(() => ExpressionParser.ParseScript(testStrings), "Invalid CMD_EXPR: " + testStrings[i]);
            }
        }
    }
}