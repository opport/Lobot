﻿using LobotAPI.Globals;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace LobotTests
{
    public static class TestsSetupFixtureHelper
    {
        public static string DataFolderPath { get { return Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName; } }
        private static FileStream ItemsData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Items.txt"); } }
        private static FileStream LevelsData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Levels.txt"); } }
        private static FileStream NamesData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Names.txt"); } }
        private static FileStream NPCsData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\NPCs.txt"); } }
        private static FileStream ShopsData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Shops.txt"); } }
        private static FileStream SkillsData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Skills.txt"); } }
        private static FileStream TeleportersData { get { return File.OpenRead(DataFolderPath + @"\DataFiles\Teleporters.txt"); } }

        private static Regex nameFilter = new Regex(@"^SKILL_((PUNCH)_01|(CH|EU)_(\w+)_BASE_01)");

        private static bool IsValid(string str, int args = 2, int minArgs = 2)
        {
            return !String.IsNullOrEmpty(str) && !String.IsNullOrWhiteSpace(str) && args >= minArgs && !str.StartsWith("//");
        }

        internal static void LoadItems()
        {
            try
            {
                Dictionary<uint, Pk2Item> items = new Dictionary<uint, Pk2Item>();
                using (StreamReader sr = new StreamReader(ItemsData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] item = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(item[0], item.Length, 12) || items.ContainsKey(uint.Parse(item[0])))
                        {
                            continue;
                        }
                        items.Add(uint.Parse(item[0]), new Pk2Item(item[0], item[1], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9], item[10], item[11]));
                    }
                }

                PK2DataGlobal.Items = items;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/Items.txt\" not found.");
                throw;
            }
        }

        internal static void LoadLevels()
        {
            try
            {
                Dictionary<int, ulong> levels = new Dictionary<int, ulong>();

                using (StreamReader sr = new StreamReader(LevelsData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] level = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(level[0], level.Length) || levels.ContainsKey(int.Parse(level[0])))
                        {
                            continue;
                        }

                        levels.Add(int.Parse(level[0]), ulong.Parse(level[1]));
                    }
                }

                PK2DataGlobal.LevelData = levels;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/Levels.txt\" not found.");
                throw;
            }
        }

        internal static void LoadNames()
        {
            try
            {
                Dictionary<string, string> names = new Dictionary<string, string>();

                using (StreamReader sr = new StreamReader(NamesData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] name = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(name[0], name.Length) || names.ContainsKey(name[0]))
                        {
                            continue;
                        }
                        names.Add(name[0], name[1]);
                    }
                }

                PK2DataGlobal.Names = names;
            }
            catch (ArgumentException a)
            {
                System.Diagnostics.Debug.WriteLine("error adding \"{0}\" to name list", a.ParamName);
                throw;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/Names.txt\" not found.");
                throw;
            }
        }

        internal static void LoadNavmesh()
        {
            // TBA
        }

        internal static void LoadNPCs()
        {
            try
            {
                Dictionary<uint, Pk2NPC> npcs = new Dictionary<uint, Pk2NPC>();

                using (StreamReader sr = new StreamReader(NPCsData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] npc = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(npc[0], npc.Length, 6) || npcs.ContainsKey(uint.Parse(npc[0])))
                        {
                            continue;
                        }
                        npcs.Add(uint.Parse(npc[0]), new Pk2NPC(npc[0], npc[1], npc[2], npc[3], npc[4], npc[5]));
                    }
                }

                PK2DataGlobal.NPCs = npcs;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/NPCs.txt\" not found.");
                throw;
            }
        }

        internal static void LoadShops()
        {
            try
            {
                Dictionary<int, Shop> shops = new Dictionary<int, Shop>();

                using (StreamReader sr = new StreamReader(ShopsData))
                {
                    List<Pk2ShopTabData> tabs = new List<Pk2ShopTabData>();

                    while (!sr.EndOfStream)
                    {
                        int[] shop = Array.ConvertAll<string, int>(sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None), s => Int32.Parse(s));

                        if (!IsValid(shop[0].ToString(), shop.Length, 4))
                        {
                            continue;
                        }

                        if (!shops.TryGetValue(shop[0], out Shop resultShop))
                        {
                            shops.Add(shop[0], new Shop((uint)shop[0]));
                        }
                        // shop id - tab - slot - itemID
                        shops[shop[0]].ShopItemEntries[(uint)shop[1]] = new ShopItemEntry((uint)shop[1], (byte)shop[2], (byte)shop[3], shop[4]);
                    }
                }

                PK2DataGlobal.Shops = shops;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/Shops.txt\" not found.");
                throw;
            }
        }

        private static void TryAddAutoAttackSkills(string id, string skill)
        {
            Match match = nameFilter.Match(skill);

            if (match.Groups.Count > 0)
            {
                Skill s = new Skill(uint.Parse(id), 1, true);

                if (match.Groups[2].Value == "PUNCH")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Other] = s;
                }
                else if (match.Groups[3].Value == "CH" && match.Groups[4].Value == "SWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Sword] = s;
                }
                else if (match.Groups[4].Value == "SPEAR")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Spear] = s;
                }
                else if (match.Groups[4].Value == "BOW")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Bow] = s;
                }
                else if (match.Groups[3].Value == "EU" && match.Groups[4].Value == "SWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.SwordOnehander] = s;
                }
                else if (match.Groups[4].Value == "TSWORD")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.SwordTwohander] = s;
                }
                else if (match.Groups[4].Value == "AXE")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Axe] = s;
                }
                else if (match.Groups[4].Value == "CROSSBOW")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Crossbow] = s;
                }
                else if (match.Groups[4].Value == "DAGGER")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Dagger] = s;
                }
                else if (match.Groups[4].Value == "STAFF")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.MageStaff] = s;
                }
                else if (match.Groups[4].Value == "WAND_WARLOCK")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Darkstaff] = s;
                }
                else if (match.Groups[4].Value == "HARP")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.Harp] = s;
                }
                else if (match.Groups[4].Value == "WAND_CLERIC")
                {
                    CharInfoGlobal.AutoAttackSkills[Pk2ItemType.ClericStaff] = s;
                }
            }
        }

        private static void AddAutoAttackSkillsToSkillList()
        {
            foreach (KeyValuePair<Pk2ItemType, Skill> kvp in CharInfoGlobal.AutoAttackSkills)
            {
                CharInfoGlobal.Skills.Add(kvp.Value);
            }
        }

        internal static void LoadSkills()
        {
            try
            {
                Dictionary<uint, Pk2Skill> skills = new Dictionary<uint, Pk2Skill>();

                using (StreamReader sr = new StreamReader(SkillsData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] skill = sr.ReadLine().Split(new char[] { '|' }, StringSplitOptions.None);

                        if (!IsValid(skill[0], skill.Length, 9) || skills.ContainsKey(uint.Parse(skill[0])))
                        {
                            continue;
                        }
                        skills.Add(uint.Parse(skill[0]), new Pk2Skill(skill[0], skill[1], skill[2], skill[3], skill[4], skill[5], skill[6], skill[7], skill[8]));

                        TryAddAutoAttackSkills(skill[0], skill[1]);
                    }
                }

                PK2DataGlobal.Skills = skills;

                AddAutoAttackSkillsToSkillList();
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/SkillData.txt\" not found.");
                throw;
            }
        }

        private static List<uint> LoadTeleporterLink(string[] teleporterLinkInfo)
        {
            List<uint> teleporterLink = new List<uint>();

            if (teleporterLinkInfo.Length < 1)
            {
                return new List<uint>();
            }
            else
            {
                foreach (string s in teleporterLinkInfo)
                {
                    teleporterLink.Add(uint.Parse(s));
                }
                return teleporterLink;
            }
        }

        private static Pk2TeleporterData LoadTeleporterData(string[] teleporterDataInfo)
        {
            //Add possible teleporterLinks
            return (teleporterDataInfo.Length == 4)
                ? new Pk2TeleporterData(teleporterDataInfo[0],
                    teleporterDataInfo[1],
                    teleporterDataInfo[2],
                    LoadTeleporterLink(teleporterDataInfo[3].Split(new char[] { '|' }, StringSplitOptions.None))
                    )
                : new Pk2TeleporterData("", "", "", new List<uint>());
        }

        private static Pk2Teleporter LoadTeleporter(string[] teleporterInfo)
        {
            return (teleporterInfo.Length == 4)
                ? new Pk2Teleporter(teleporterInfo[0],
                        teleporterInfo[1],
                        teleporterInfo[2],
                        LoadTeleporterData(teleporterInfo[3].Split(new char[] { '|' }, 4, StringSplitOptions.None))
                        )
                : (teleporterInfo.Length == 3)
                    ? new Pk2Teleporter(teleporterInfo[0], teleporterInfo[1], teleporterInfo[2], new Pk2TeleporterData("", "", "", new List<uint>()))
                    : default(Pk2Teleporter);
        }

        internal static void LoadTeleporters()
        {
            try
            {
                Dictionary<uint, Pk2Teleporter> teleporters = new Dictionary<uint, Pk2Teleporter>();

                using (StreamReader sr = new StreamReader(TeleportersData))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] portal = sr.ReadLine().Split(new char[] { '|' }, 4, StringSplitOptions.None);
                        //Check for possible comment
                        if (!IsValid(portal[0], portal.Length, 3) || teleporters.ContainsKey(uint.Parse(portal[0])))
                        {
                            continue;
                        }

                        //Load teleporter
                        Pk2Teleporter teleporter = LoadTeleporter(portal);

                        //TODO add list of teleport links in last parameter List<>
                        if (teleporter.Equals(default(Pk2Teleporter)))
                        {
                            continue;
                        }

                        teleporters.Add(teleporter.Id, teleporter);
                    }
                }

                PK2DataGlobal.Teleporters = teleporters;
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("\"DataFiles/Teleporters.txt\" not found.");
                throw;
            }
        }
    }
}
