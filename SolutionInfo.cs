﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyTrademark("")]

[assembly: AssemblyProduct("Lobot")]

[assembly: AssemblyDescription("Automation tool for Silkroad Online VSRO 1.188 Private servers")]
[assembly: AssemblyCompany("opport")]
[assembly: AssemblyCopyright("GPL-3.0+")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en", UltimateResourceFallbackLocation.MainAssembly)]