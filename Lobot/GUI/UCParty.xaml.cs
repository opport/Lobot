﻿using LobotAPI.Globals.Settings;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Party;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCParty.xaml
    /// </summary>
    public partial class UCParty : UserControl
    {
        public UCParty()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void RefreshAvaliablePartiesButton_Click(object sender, RoutedEventArgs e)
        {
            Proxy.Instance.SendCommandAG(new PartyMatchRefresh());
        }

        private void PartyObjectiveRadioButtonsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.RadioButton rb = sender as System.Windows.Controls.RadioButton;
            string tag = rb.Tag as string;
            if (rb.IsChecked.Value)
            {
                PartySettings.Objective = (PartyObjective)Enum.Parse(typeof(PartyObjective), tag);
            }
        }

        private void PartyTypeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.RadioButton rb = sender as System.Windows.Controls.RadioButton;
            byte typeBits = byte.Parse(rb.Tag as string);
            byte isChecked = AutoreformCanInviteCheckBox.IsChecked is true ? (byte)4 : (byte)0;
            PartySettings.Type = (PartyType)(typeBits | isChecked);
        }
    }
}
