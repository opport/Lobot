﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for MediumMode.xaml
    /// </summary>
    public partial class MediumMode : Window
    {
        public MediumMode(Window window)
        {
            InitializeComponent();
            this.Owner = window;
        }

        private void buttonMediumExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void buttonMediumMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void buttonMediumMainWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            this.Owner.Show();
        }
    }
}
