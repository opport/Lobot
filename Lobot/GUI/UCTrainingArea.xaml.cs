﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Scripting;
using System.Linq;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCTrainingArea.xaml
    /// </summary>
    public partial class UCTrainingArea : UserControl
    {
        public UCTrainingArea()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Validate by adding new symbol to the position of the caret then attempting to parse to the desired type.
        /// </summary>
        /// <seealso href="https://stackoverflow.com/a/48082972/9597363"/>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            // Use SelectionStart property to find the caret position.
            // Insert the previewed text into the existing text in the textbox.
            var fullText = textBox.Text.Insert(textBox.SelectionStart, e.Text);

            int val;
            // If parsing is successful, set Handled to false
            e.Handled = !int.TryParse(fullText, out val);
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void Area1SetFromCurrentButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AreaX1TextBox.Text = Bot.CharData.Position.X.ToString();
            AreaY1TextBox.Text = Bot.CharData.Position.Y.ToString();
        }

        private void Area2SetFromCurrentButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            AreaX2TextBox.Text = Bot.CharData.Position.X.ToString();
            AreaY2TextBox.Text = Bot.CharData.Position.Y.ToString();
        }

        private void AreaSwapButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!Settings.Instance.Training.TrainingArea1.Equals(Settings.Instance.Training.TrainingArea2))
            {
                Area temp = Settings.Instance.Training.TrainingArea1;
                AreaX1TextBox.Text = AreaX2TextBox.Text;
                AreaY1TextBox.Text = AreaY2TextBox.Text;
                AreaRadius1TextBox.Text = AreaRadius2TextBox.Text;

                AreaX2TextBox.Text = temp.X.ToString();
                AreaY2TextBox.Text = temp.Y.ToString();
                AreaRadius2TextBox.Text = temp.Radius.ToString();
            }
        }
    }
}
