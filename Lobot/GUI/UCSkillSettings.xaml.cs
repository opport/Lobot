﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCSkillSettings.xaml
    /// </summary>
    public partial class UCSkillSettings : UserControl
    {
        public UCSkillSettings()
        {
            InitializeComponent();

            // Map skills after skill list is loaded
            Bot.BotStatusChanged += MapSkillsFromList;
        }

        private void MapSkillsFromList(object sender, EventArgs e)
        {
            if (Bot.Status != BotStatus.Idle || Bot.Status != BotStatus.Botting)
            {
                return;
            }

            List<ObservableCollection<ObservableCollection<Skill>>> c = new List<ObservableCollection<ObservableCollection<Skill>>>
            { SkillSettings.InitiationSkills, SkillSettings.GeneralSkills, SkillSettings.FinisherSkills};

            foreach (ObservableCollection<ObservableCollection<Skill>> listlistOfLists in c)
            {
                foreach (ObservableCollection<Skill> list in listlistOfLists)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        Skill skill = CharInfoGlobal.Skills.FirstOrDefault(s => s.RefSkillID == list[i].RefSkillID);

                        if (skill != null)
                        {
                            App.Current.Dispatcher.Invoke((System.Action)delegate // ObservableCollections can only be manipulated by the UI thread
                            {
                                list[i] = skill;
                            });
                        }
                        else
                        {
                            App.Current.Dispatcher.Invoke((System.Action)delegate // ObservableCollections can only be manipulated by the UI thread
                            {
                                list.RemoveAt(i);
                            });
                        }
                    }
                }
            }
            // unsubscribe from future changes
            Bot.BotStatusChanged -= MapSkillsFromList;
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void AddDistinctSkill(IList skillList, int skillIndex)
        {
            if (skillIndex > -1 && !skillList.Contains(CharInfoGlobal.Skills[skillIndex]))
            {
                skillList.Add(CharInfoGlobal.Skills[skillIndex]);
            }
        }

        private void AddDistinctSkills(IList items, IList selectedItems)
        {
            foreach (var i in selectedItems)
            {
                int indexInFullList = CharInfoGlobal.Skills.IndexOf(i as Skill);

                //Check for availability in full list to prevent errors
                if (indexInFullList > -1)
                {
                    AddDistinctSkill(items, indexInFullList);
                }
            }
        }

        private void MoveItemsUpInList(IList items, IList selectedItems)
        {
            for (int i = 0; i < selectedItems.Count; i++)
            {
                int indexInFullList = items.IndexOf(selectedItems[i]);

                //Check for availability and that index is not first item in full list to prevent errors
                // and that selected item isn't preceded by other selected item
                if (indexInFullList < 1
                    || (i > 0 && items[indexInFullList - 1] == selectedItems[i - 1]))
                {
                    continue;
                }
                else
                {
                    object item = items[indexInFullList];
                    items.RemoveAt(indexInFullList);
                    items.Insert(indexInFullList - 1, item);
                }
            }
        }

        private void MoveItemsDownInList(IList items, IList selectedItems)
        {
            for (int i = selectedItems.Count - 1; i >= 0; i--)
            {
                int indexInFullList = items.IndexOf(selectedItems[i]);

                //Check for availability and that index is not last item in full list to prevent errors
                // and that selected item isn't followed by other selected item
                if (indexInFullList < 0 || indexInFullList >= items.Count - 1
                    || (i < selectedItems.Count - 1 && items[indexInFullList + 1] == selectedItems[i + 1]))
                {
                    continue;
                }
                else
                {
                    object item = items[indexInFullList];
                    items.RemoveAt(indexInFullList);
                    items.Insert(indexInFullList + 1, item);
                }
            }
        }

        private void DeleteItemsFromList(IList items, IList selectedItems)
        {
            for (int i = selectedItems.Count - 1; i >= 0; i--)
            {
                items.Remove(selectedItems[i]);
            }
        }

        //Skills
        private void SkillComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InitiationAttacksListBox != null)
            {
                InitiationAttacksListBox.ItemsSource = SkillSettings.InitiationSkills[SkillComboBox.SelectedIndex];
            }
            SkillSettings.SelectedInitiationSkills = SkillSettings.InitiationSkills[SkillComboBox.SelectedIndex];

            if (GeneralAttacksListBox != null)
            {
                GeneralAttacksListBox.ItemsSource = SkillSettings.GeneralSkills[SkillComboBox.SelectedIndex];
            }
            SkillSettings.SelectedGeneralSkills = SkillSettings.GeneralSkills[SkillComboBox.SelectedIndex];

            if (FinisherAttacksListBox != null)
            {
                FinisherAttacksListBox.ItemsSource = SkillSettings.FinisherSkills[SkillComboBox.SelectedIndex];
            }
            SkillSettings.SelectedFinisherSkills = SkillSettings.FinisherSkills[SkillComboBox.SelectedIndex];
        }

        private void InitiationAttacksButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            IList filteredAttacks = SkillListBox.SelectedItems.Cast<Skill>().Where(skill => skill.Type.IsAttack()).ToList();
            AddDistinctSkills(SkillSettings.SelectedInitiationSkills, filteredAttacks);
        }

        private void InitiationUpButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsUpInList(SkillSettings.SelectedInitiationSkills, InitiationAttacksListBox.SelectedItems);
        }

        private void InitiationDownButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsDownInList(SkillSettings.SelectedInitiationSkills, InitiationAttacksListBox.SelectedItems);
        }

        private void InitiationDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(SkillSettings.SelectedInitiationSkills, InitiationAttacksListBox.SelectedItems);
        }

        private void GeneralAttacksButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            IList filteredAttacks = SkillListBox.SelectedItems.Cast<Skill>().Where(skill => skill.Type.IsAttack()).ToList();
            AddDistinctSkills(SkillSettings.SelectedGeneralSkills, filteredAttacks);
        }

        private void GeneralAttacksUpButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsUpInList(SkillSettings.SelectedGeneralSkills, GeneralAttacksListBox.SelectedItems);
        }

        private void GeneralAttacksDownButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsDownInList(SkillSettings.SelectedGeneralSkills, GeneralAttacksListBox.SelectedItems);
        }

        private void GeneralAttacksDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(SkillSettings.SelectedGeneralSkills, GeneralAttacksListBox.SelectedItems);
        }

        private void FinisherAttacksButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            IList filteredAttacks = SkillListBox.SelectedItems.Cast<Skill>().Where(skill => skill.Type.IsAttack()).ToList();
            AddDistinctSkills(SkillSettings.SelectedFinisherSkills, filteredAttacks);
        }

        private void FinisherAttacksUpButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsUpInList(SkillSettings.SelectedFinisherSkills, FinisherAttacksListBox.SelectedItems);
        }

        private void FinisherAttacksDownButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsDownInList(SkillSettings.SelectedFinisherSkills, FinisherAttacksListBox.SelectedItems);
        }

        private void FinisherAttacksDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(SkillSettings.SelectedFinisherSkills, FinisherAttacksListBox.SelectedItems);
        }

        //Buffs
        private void BuffComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BuffsListBox != null)
            {
                BuffsListBox.ItemsSource = SkillSettings.BuffSkills[BuffComboBox.SelectedIndex];
            }
            SkillSettings.SelectedBuffs = SkillSettings.BuffSkills[BuffComboBox.SelectedIndex];
        }

        private void BuffsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            IList filteredBuffs = SkillListBox.SelectedItems.Cast<Skill>().Where(skill => skill.Type == Pk2SkillType.Buff).ToList();
            AddDistinctSkills(SkillSettings.SelectedBuffs, filteredBuffs);
        }

        private void BuffsUpButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsUpInList(SkillSettings.SelectedBuffs, BuffsListBox.SelectedItems);
        }

        private void BuffsDownButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsDownInList(SkillSettings.SelectedBuffs, BuffsListBox.SelectedItems);
        }

        private void BuffsDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(SkillSettings.SelectedBuffs, BuffsListBox.SelectedItems);
        }

        private void MemberAddButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SkillSettings.MembersToBuff.Add(MembersToBuffListBox.SelectedValue.ToString());
        }

        private void MemberDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(MembersToBuffListBox.Items, MembersToBuffListBox.SelectedItems);
        }

        private void PartyBuffButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SkillSettings.MemberBuffs[SkillComboBox.SelectedIndex].Add(Bot.PartyCurrent.Members[MemberComboBox.SelectedIndex]);
        }

        private void PartyBuffUpButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsUpInList(SkillSettings.MemberBuffs, PartyBuffsListBox.SelectedItems);
        }

        private void PartyBuffDownButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MoveItemsDownInList(SkillSettings.MemberBuffs, PartyBuffsListBox.SelectedItems);
        }

        private void PartyBuffDeleteButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DeleteItemsFromList(SkillSettings.MemberBuffs, PartyBuffsListBox.SelectedItems);
        }

        private void ImbueComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.Imbue = (ImbueComboBox.SelectedItem as Skill);
        }

        private void SpeedBuffComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.SpeedBuff = (SpeedBuffComboBox.SelectedItem as Skill);
        }

        private void HealComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.HealSpell = (HealComboBox.SelectedItem as Skill);
        }

        private void MPComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.ManaSpell = (MPComboBox.SelectedItem as Skill);
        }

        private void ResComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.ResSpell = (ResComboBox.SelectedItem as Skill);
        }

        // Use to filter Skills from Skill list
        private void CollectionViewSource_Filter_ActiveSkills(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type != Pk2SkillType.Passive && !skill.IsAutoAttack)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_Attacks(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type.IsAttack() && skill.IsAutoAttack == false)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_AttacksKD(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.Knockdown || skill.Type == Pk2SkillType.KnockDownOnly)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_AttacksDebuffs(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.Debuff)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_AttacksDOT(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.WarlockDOT)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_Buffs(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type.IsBuff())
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_Imbue(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.Imbue)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_SpeedBuffs(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.SpeedBuff)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_HealSkills(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.Heal || skill.Type == Pk2SkillType.SelfHeal)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_ManaBuffs(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.ManaBuff)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void CollectionViewSource_Filter_ResSkills(object sender, System.Windows.Data.FilterEventArgs e)
        {
            Skill skill = e.Item as Skill;
            if (skill.Type == Pk2SkillType.Resurrection)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }
    }
}
