﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using static LobotAPI.Globals.Settings.AutoPotionSettings;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCAutoPotions.xaml
    /// </summary>
    public partial class UCAutoPotions : UserControl
    {
        public UCAutoPotions()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void AutopotionDelayIncrementButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (PlayerSettings.PotionDelay + 500 > 10000)
            {
                PlayerSettings.PotionDelay = 10000;
            }
            else
            {
                PlayerSettings.PotionDelay += 500;
            }
        }

        private void AutopotionDelayDecrementButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (PlayerSettings.PotionDelay - 500 < 500)
            {
                PlayerSettings.PotionDelay = 500;
            }
            else
            {
                PlayerSettings.PotionDelay -= 500;
            }
        }

        private void AutopotionDelayUpdateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PlayerSettings.HPPotionTimer.Interval = PlayerSettings.PotionDelay;
            PlayerSettings.MPPotionTimer.Interval = PlayerSettings.PotionDelay;
            PlayerSettings.VigourTimer.Interval = PlayerSettings.PotionDelay;
            PlayerSettings.PurificationTimer.Interval = PlayerSettings.PotionDelay;
            PlayerSettings.UniversalPillTimer.Interval = PlayerSettings.PotionDelay;
        }

        private void CollectionViewSource_Filter_CureSkills(object sender, System.Windows.Data.FilterEventArgs e)
        {
            LobotAPI.Structs.Skill skill = e.Item as LobotAPI.Structs.Skill;
            if (skill.Type == LobotAPI.PK2.Pk2SkillType.Cure)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void BadStatusSpellComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PlayerSettings.BadStatusSpell = (BadStatusSpellComboBox.SelectedItem as LobotAPI.Structs.Skill).RefSkillID;
        }

        private void CurseSpellComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PlayerSettings.CurseSpell = (CurseSpellComboBox.SelectedItem as LobotAPI.Structs.Skill).RefSkillID;
        }
    }
}
