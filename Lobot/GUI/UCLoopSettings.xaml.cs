﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotAPI.Packets.Inventory;
using LobotDLL.Connection;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Community;
using LobotDLL.Packets.Inventory;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCLoopSettings.xaml
    /// </summary>
    public partial class UCLoopSettings : System.Windows.Controls.UserControl
    {
        public UCLoopSettings()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        // Town settings

        private void ShoppingHPComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.HPPotionType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingMPComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.MPPotionType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingVigourComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.VigorPotionType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingUniPillsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.UniversalPillType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingSpecialPillsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.PurificationPillType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingProjectileComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.ProjectileType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingSpeedPotComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.SpeedPotType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingReturnScrollsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.ReturnScrollType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingTransportsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.TransportType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingBeserkComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.BeserkPotionType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingPetHPPotComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.PetHPPotionType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void ShoppingPetPillsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            System.Windows.Controls.ComboBox cb = sender as System.Windows.Controls.ComboBox;
            ShoppingSettings.PetPillType = UInt32.Parse((cb.SelectedItem as ComboBoxItem).Tag.ToString());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Proxy.Instance.SendCommandAG(new CharMoveItem(ItemMovementType.Buy, 0, 0, 1, 0x19E)); // Herbalist Bori, DW
            Proxy.Instance.SendCommandAG(new MoveItem(ItemMovementType.Buy, 0, 6, 198, Bot.SelectedNPC.UniqueID));
        }

        // Script Settings

        /// <summary>
        /// Set questscript for bot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoopQuestScriptButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            fileDialog.Title = " Select questscript";
            fileDialog.Filter = " questscript (*.txt)| *.txt";
            fileDialog.FilterIndex = 1;
            fileDialog.Multiselect = false;

            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LoopQuestScriptTextBox.Text = fileDialog.FileName;
                ScriptSettings.QuestScriptPath = fileDialog.FileName;
            }
        }

        /// <summary>
        /// Set training script for for bot
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoopTrainingScriptButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = ScriptGlobal.WalkScriptFolder;
            fileDialog.Title = " Select training script";
            fileDialog.Filter = " training script (*.txt)| *.txt";
            fileDialog.FilterIndex = 1;
            fileDialog.Multiselect = false;

            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LoopTrainingScriptTextBox.Text = fileDialog.FileName;
                ScriptSettings.TrainingScriptPath = fileDialog.FileName;
            }
        }

        /// <summary>
        /// Save recorded script to file
        /// </summary>
        /// <param name="sender"></param>C:\Program Files (x86)\NUnit.org\nunit-console\nunit3-console.exe
        /// <param name="e"></param>
        private void ScriptSaveScriptButton_Click(object sender, RoutedEventArgs e)
        {
            Directory.CreateDirectory(DirectoryGlobal.ScriptFolder);

            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.FileName = "script"; // Default file name
            saveFile.DefaultExt = ".txt";
            saveFile.Filter = "Text (*.txt) | *.txt";
            saveFile.InitialDirectory = DirectoryGlobal.ScriptFolder;

            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (var sw = new StreamWriter(saveFile.FileName, false))
                    sw.Write(ScriptRecordTextBox.Text + Environment.NewLine);
            }
        }


        private void ScriptStartRecordingbutton_Click(object sender, RoutedEventArgs e)
        {
            ScriptStartRecordingbutton.IsEnabled = false;
            CharMove.BotMovementRegistered += ScriptAddLine;
            FriendInfoHandler.HandledFriendInfo += ScriptAddLine;
        }

        private void ScriptStopRecordingButton_Click(object sender, RoutedEventArgs e)
        {
            ScriptStartRecordingbutton.IsEnabled = true;
            CharMove.BotMovementRegistered -= ScriptAddLine;
        }

        private void UpdateScriptTextBox(string text)
        {
            // InvokeRequired for WPF to see whether current method is running on UI thread
            // Checking if this thread has access to the object.
            if (CheckAccess())
            {
                // This thread has access so it can update the UI thread.
                ScriptRecordTextBox.AppendText(text);
                ScriptRecordTextBox.AppendText(Environment.NewLine);
            }
            else
            {
                // This thread does not have access to the UI thread.
                // Place the update method on the Dispatcher of the UI thread.
                Dispatcher.Invoke(() => { ScriptRecordTextBox.AppendText(text); ScriptRecordTextBox.AppendText(Environment.NewLine); });
            }
        }

        private void ScriptAddLine(object sender, EventArgs e)
        {
            if (e is CharMoveArgs)
            {
                CharMoveArgs charMoveArgs = e as CharMoveArgs;
                if (charMoveArgs.UniqueID == Bot.CharData.UniqueID && charMoveArgs.Destination != null)
                {
                    int x = (int)charMoveArgs.Destination.X;
                    int y = (int)charMoveArgs.Destination.Y;

                    string goCommand = string.Format("go {0},{1}", x, y);
                    UpdateScriptTextBox(goCommand);
                }
            }
            else
            {
                UpdateScriptTextBox("wait");
            }
        }

        private void ScriptSpeedBuffComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SkillSettings.SpeedBuff = (ScriptSpeedBuffComboBox.SelectedItem as LobotAPI.Structs.Skill);
        }

        private void CollectionViewSource_Filter_SpeedBuffs(object sender, System.Windows.Data.FilterEventArgs e)
        {
            LobotAPI.Structs.Skill skill = e.Item as LobotAPI.Structs.Skill;
            if (skill.Type == LobotAPI.PK2.Pk2SkillType.SpeedBuff)
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }
    }
}
