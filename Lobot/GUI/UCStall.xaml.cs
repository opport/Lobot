﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Stall;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Pet;
using LobotDLL.Packets.Stall;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UCStall : UserControl
    {
        private DispatcherTimer Timer = new DispatcherTimer()
        {
            Interval = TimeSpan.FromSeconds(1)
        };

        private DateTime LastPointInTime = DateTime.Now;
        private TimeSpan TotalElapsed = new TimeSpan(0);
        private static bool WasStallOpenedManually = false;

        public UCStall()
        {
            InitializeComponent();

            Timer.Tick += timer_Tick;
            StallCreate.StallCreated += HandleStallCreated;
            StallUpdate.StallUpdated += HandleStallUpdated;
            StallClose.StallClosed += HandleStallClosed;
            StallHandler.StallItemCreated += HandleItemCreated;
            StallHandler.StallItemRemoved += HandleItemRemoved;
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            DateTime nextPointInTime = DateTime.Now;
            TotalElapsed += (nextPointInTime - LastPointInTime);
            LastPointInTime = nextPointInTime;
            StallTimeStringLabel.Content = TotalElapsed.ToString(@"hh\:mm\:ss");
        }

        private void HandleItemCreated(object sender, StallItemArgs e)
        {
            // Update items to only show the ones that are not in stall
            //BindingExpression be = StallInventoryListBox.GetBindingExpression(ListBox.ItemsSourceProperty);
            //Dispatcher.Invoke(() => { be.UpdateTarget(); });
            CollectionViewSource cvs = FindResource("NonEquippedItems") as CollectionViewSource;
            Dispatcher.Invoke(() => { cvs.View.Refresh(); });

            if (e.StallItem.Slot >= StallSettings.MAX_STALL_ITEMS)
            {
                e.StallItem.Status = StallItemStatus.Reserved;
                string s = $"Reserved [{e.StallItem.StackCount}]x {e.StallItem.InventoryItem.ItemName}\nPrice:{e.StallItem.Price}";
                Console.WriteLine(s);
                Dispatcher.Invoke(() => { StallSettings.StallLog.Add(s); });
            }
            else
            {
                e.StallItem.Status = StallItemStatus.InStall;
                string s = $"Added [{e.StallItem.StackCount}]x {e.StallItem.InventoryItem.ItemName}\nPrice:{e.StallItem.Price}";
                Console.WriteLine(s);
                Dispatcher.Invoke(() => { StallSettings.StallLog.Add(s); });
            }
        }

        private void HandleItemRemoved(object sender, StallItemArgs e)
        {
            // Update items to only show the ones that are not in stall
            //BindingExpression be = StallInventoryListBox.GetBindingExpression(ListBox.ItemsSourceProperty);
            //Dispatcher.Invoke(() => { be.UpdateTarget(); });
            CollectionViewSource cvs = FindResource("NonEquippedItems") as CollectionViewSource;
            Dispatcher.Invoke(() => { cvs.View.Refresh(); });

            if (StallSettings.IsOpen)
            {
                Dispatcher.Invoke(() =>
                {
                    StallSettings.StallLog.Add($"Sold {e.StallItem.Name} for {e.StallItem.Price}");
                    if (ulong.TryParse(StallProfitStringLabel.Content.ToString(), out ulong result))
                    {
                        StallProfitStringLabel.Content = (result + e.StallItem.Price).ToString();
                    }
                });
            }
        }

        private void HandleStallCreated(object sender, EventArgs e)
        {
            // manually send update message so that client opens stall window
            if (WasStallOpenedManually)
            {
                WasStallOpenedManually = false;
                Proxy.Instance.SendCommandAG(StallUpdate.UpdateMessage());
            }

            Dispatcher.Invoke(() =>
            {
                StallCreateButton.IsEnabled = false;
                StallOpenButton.IsEnabled = true;
                StallModifyButton.IsEnabled = false;
                StallCloseButton.IsEnabled = true;
                StallNameUpdateButton.IsEnabled = true;
                StallMessageUpdateButton.IsEnabled = true;
            });
        }

        private void HandleStallOpenedOrModifying()
        {
            // enable open and modification button based on whether stall is open or not
            Dispatcher.Invoke(() =>
            {
                StallOpenButton.IsEnabled = !StallSettings.IsOpen;
                StallNameUpdateButton.IsEnabled = !StallSettings.IsOpen;
                StallModifyButton.IsEnabled = StallSettings.IsOpen;
            });

            if (StallSettings.IsOpen)
            {
                LastPointInTime = DateTime.Now;
                Timer.Start();
            }
            else
            {
                TotalElapsed = new TimeSpan(0); // reset clock
                Timer.Stop();
            }

        }

        private void HandleStallClosed(object sender, EventArgs e)
        {
            // enable open and modification button based on whether stall is open or not
            Dispatcher.Invoke(() =>
            {
                StallCreateButton.IsEnabled = true;
                StallOpenButton.IsEnabled = false;
                StallModifyButton.IsEnabled = false;
                StallCloseButton.IsEnabled = false;
                StallNameUpdateButton.IsEnabled = false;
                StallMessageUpdateButton.IsEnabled = false;
            });
        }

        private void HandleStallUpdated(object sender, StallUpdateArgs e)
        {
            switch (e.UpdateType)
            {
                case LobotAPI.Packets.Stall.StallUpdateType.UpdateItem:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.AddItem:
                    Dispatcher.Invoke(() => { StallAddItemButton.IsEnabled = StallSettings.StallItems.Count < StallSettings.MAX_STALL_ITEMS; }); // enable item adding button based on whether stall is full or not
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.RemoveItem:
                    Dispatcher.Invoke(() => { StallAddItemButton.IsEnabled = StallSettings.StallItems.Count < StallSettings.MAX_STALL_ITEMS; }); // enable item adding button based on whether stall is full or not
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.FleaMarketMode:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.State:
                    HandleStallOpenedOrModifying();
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.Message:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.Name:
                    break;
                default:
                    break;
            }
        }

        private void StallCreateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Bot.CharData.InteractionMode != 4)
            {
                Proxy.Instance.SendCommandAG(new StallCreate());
                WasStallOpenedManually = true;
            }
        }

        private void StallCloseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Bot.CharData.InteractionMode == 4)
            {
                Proxy.Instance.SendCommandAG(new StallClose());
            }
        }

        private void StallOpenButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Despawn pet so it does not die of hunger while you stall
            if (Bot.GrowthPet.UniqueID != 0)
            {
                Proxy.Instance.SendCommandAG(new PetUnsummon(Bot.GrowthPet.UniqueID));
            }

            Proxy.Instance.SendCommandAG(StallUpdate.StallUpdateOpenState(true));
            StallCreate.StallCreated += HandleStallCreated;

        }

        private void StallModifyButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (StallSettings.IsOpen)
            {
                Proxy.Instance.SendCommandAG(StallUpdate.StallUpdateOpenState(false));
            }
        }

        private void StallAddItemButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!StallSettings.IsOpen
                && PriceTextBox.Text.Length > 0
                && ulong.TryParse(PriceTextBox.Text, out ulong price)
                && StallInventoryListBox.SelectedItem is KeyValuePair<byte, Item>
                && StallSettings.StallItems.Count < StallSettings.MAX_STALL_ITEMS)
            {
                Item selectedItem = ((KeyValuePair<byte, Item>)StallInventoryListBox.SelectedItem).Value;
                StallItem stallItem = new StallItem(selectedItem, selectedItem.Slot, price);

                // find first open slot
                for (int i = 0; i < StallSettings.MAX_STALL_ITEMS; i++)
                {
                    if (StallSettings.StallItems.All(si => si.Slot != i))
                    {
                        Proxy.Instance.SendCommandAG(StallUpdate.AddItem((byte)StallSettings.StallItems.Count, selectedItem.Slot, stallItem.StackCount, price, 1, 0));
                        break;
                    }
                }

            }
        }

        private void StallRemoveItemButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!StallSettings.IsOpen
                && StallItemsListBox.SelectedItem is StallItem
                && StallSettings.StallItems.Count < StallSettings.MAX_STALL_ITEMS)
            {
                StallItem selectedItem = StallItemsListBox.SelectedItem as StallItem;

                Proxy.Instance.SendCommandAG(StallUpdate.RemoveItem(selectedItem.Slot, 0));
            }
        }

        private void CollectionViewSource_Filter_NonEquippedItems(object sender, System.Windows.Data.FilterEventArgs e)
        {
            // Item should not be equipped or already in the stall
            if (e.Item is KeyValuePair<byte, Item>
                && ((KeyValuePair<byte, Item>)e.Item).Key >= CharInfoGlobal.EQUIPMENT_SLOTS
                && !StallSettings.StallItems.Any(i => i.InventoryItem.Slot == ((KeyValuePair<byte, Item>)e.Item).Key))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void StallNameUpdateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Bot.CharData.InteractionMode == 4 && !StallSettings.IsOpen)
            {
                Proxy.Instance.SendCommandAG(StallUpdate.UpdateName());
            }
        }

        private void StallMessageUpdateButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Bot.CharData.InteractionMode == 4)
            {
                Proxy.Instance.SendCommandAG(StallUpdate.UpdateMessage());
            }
        }
    }
}
