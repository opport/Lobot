﻿using Lobot.GUI;
using LobotAPI;
using LobotAPI.StateMachine;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Lobot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Windows.Forms.NotifyIcon notifyTrayIcon;
        About aboutWindow;
        MediumMode mediumWindow;

        public delegate void InvokeDelegate();// for updating elements from outside of the UI thread

        public MainWindow()
        {
            InitializeComponent();

            notifyTrayIcon = new NotifyIcon();
            Stream iconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Resources/Symbols/Icons/Lobot_default.ico")).Stream;
            notifyTrayIcon.Icon = new System.Drawing.Icon(iconStream);
            notifyTrayIcon.Visible = false;
            notifyTrayIcon.DoubleClick += delegate (Object o, EventArgs e)
            {
                this.Show();
                this.WindowState = WindowState.Normal;
            };

            Bot.BotStatusChanged += EnableOrDisableBotting;
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (MenuMinimizeToTray.IsChecked && WindowState == WindowState.Minimized)
            {
                notifyTrayIcon.Visible = true;
                this.Hide();
            }
            else if (WindowState == WindowState.Normal)
            {
                notifyTrayIcon.Visible = false;
            }

            base.OnStateChanged(e);
        }

        // Menu item functions
        //
        //
        private void EnableOrDisableBotting(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((System.Action)delegate // ObservableCollections can only be manipulated by the UI thread
            {
                if (Bot.Status == BotStatus.Botting)
                {
                    MenuStartBotButton.IsEnabled = false;
                    MenuStopBotButton.IsEnabled = true;
                }
                else if (Bot.Status == BotStatus.Idle)
                {
                    MenuStartBotButton.IsEnabled = true;
                    MenuStopBotButton.IsEnabled = false;
                }
            });

        }

        private void MenuStartBotButton_Click(object sender, RoutedEventArgs e)
        {
            // Only start if the bot is connected, loaded and not already botting
            if (Bot.Status == BotStatus.Idle)
            {
                BotStateMachine.Instance.Start();
            }
        }

        private void MenuStopBotButton_Click(object sender, RoutedEventArgs e)
        {
            // Only stop if bot is actually botting
            if (Bot.Status == BotStatus.Botting)
            {
                BotStateMachine.Instance.Stop();
            }
        }

        private void MenuHideButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void MenuMediumWindowButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();

            if (mediumWindow == null)
            {
                mediumWindow = new MediumMode(this);
            }

            mediumWindow.Show();
        }

        private void MenuMiniWindowButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Add Miniwindow mode
        }

        private void MenuAboutButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();

            if (aboutWindow == null)
            {
                aboutWindow = new About(this);
            }

            aboutWindow.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.CloseLog();
            System.Windows.Application.Current.Shutdown();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            //TODO
        }

        private void HideBotButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void UCLoopSettings_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
