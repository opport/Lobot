﻿using System.Windows;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCAlchemyFilters.xaml
    /// </summary>
    public partial class UCAlchemyFilters : UserControl
    {
        public UCAlchemyFilters()
        {
            InitializeComponent();
        }

        private void FilterJadePickButton_Click(object sender, RoutedEventArgs e)
        {
            FilterJadePickStackPanel.ToggleCheckBoxes();
        }

        private void FilterJadeStoreButton_Click(object sender, RoutedEventArgs e)
        {
            FilterJadeStoreStackPanel.ToggleCheckBoxes();
        }

        private void FilterJadeSellButton_Click(object sender, RoutedEventArgs e)
        {
            FilterJadeSellStackPanel.ToggleCheckBoxes();
        }

        private void FilterJadeDropButton_Click(object sender, RoutedEventArgs e)
        {
            FilterJadeDropStackPanel.ToggleCheckBoxes();
        }

        private void FilterRubyPickButton_Click(object sender, RoutedEventArgs e)
        {
            FilterRubyPickStackPanel.ToggleCheckBoxes();
        }

        private void FilterRubyStoreButton_Click(object sender, RoutedEventArgs e)
        {
            FilterRubyStoreStackPanel.ToggleCheckBoxes();
        }

        private void FilterRubySellButton_Click(object sender, RoutedEventArgs e)
        {
            FilterRubySellStackPanel.ToggleCheckBoxes();
        }

        private void FilterRubyDropButton_Click(object sender, RoutedEventArgs e)
        {
            FilterRubyDropStackPanel.ToggleCheckBoxes();
        }

        private void FilterAlchemyDegreesButton_Click(object sender, RoutedEventArgs e)
        {
            AlchemyDegreesStackPanel.ToggleCheckBoxes();
        }
    }
}
