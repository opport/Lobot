﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCTrainingSettings.xaml
    /// </summary>
    public partial class UCTrainingSettings : UserControl
    {
        public UCTrainingSettings()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void PercentValidationTextBox(object sender, TextChangedEventArgs e)
        {
            System.Windows.Controls.TextBox textBox = (System.Windows.Controls.TextBox)sender;
            if (!int.TryParse(textBox.Text, out int result) || result < 0 || result > 100)
            {
                int removeCount = e.Changes.First().AddedLength;
                textBox.Text = textBox.Text.Substring(0, textBox.Text.Count() - removeCount);
                textBox.CaretIndex = textBox.Text.Length;
            }
        }
    }
}
