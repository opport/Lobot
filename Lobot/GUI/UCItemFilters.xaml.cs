﻿using System.Windows;
using System.Windows.Controls;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCItemFilters.xaml
    /// </summary>
    public partial class UCItemFilters : UserControl
    {
        public UCItemFilters()
        {
            InitializeComponent();
        }

        private void FilterPickButton_Click(object sender, RoutedEventArgs e)
        {
            FilterPickStackPanel.ToggleCheckBoxes();
        }

        private void FilterStoreButton_Click(object sender, RoutedEventArgs e)
        {
            FilterStoreStackPanel.ToggleCheckBoxes();
        }

        private void FilterSellButton_Click(object sender, RoutedEventArgs e)
        {
            FilterSellStackPanel.ToggleCheckBoxes();
        }

        private void FilterDropButton_Click(object sender, RoutedEventArgs e)
        {
            FilterDropStackPanel.ToggleCheckBoxes();
        }


        // CH item filters
        private void FilterCHPickButton_Click(object sender, RoutedEventArgs e)
        {
            FilterCHPickStackPanel.ToggleCheckBoxes();
        }

        private void FilterCHStoreButton_Click(object sender, RoutedEventArgs e)
        {
            FilterCHStoreStackPanel.ToggleCheckBoxes();
        }

        private void FilterCHSellButton_Click(object sender, RoutedEventArgs e)
        {
            FilterCHSellStackPanel.ToggleCheckBoxes();
        }

        private void FilterCHDropButton_Click(object sender, RoutedEventArgs e)
        {
            FilterCHDropStackPanel.ToggleCheckBoxes();
        }

        private void FilterCHDegreesButton_Click(object sender, RoutedEventArgs e)
        {
            FilterCHDegreesStackPanel.ToggleCheckBoxes();
        }

        // EU item filters
        private void FilterEUPickButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEUPickStackPanel.ToggleCheckBoxes();
        }

        private void FilterEUStoreButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEUStoreStackPanel.ToggleCheckBoxes();
        }

        private void FilterEUSellButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEUSellStackPanel.ToggleCheckBoxes();
        }

        private void FilterEUDropButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEUDropStackPanel.ToggleCheckBoxes();
        }

        private void FilterEUDegreesButton_Click(object sender, RoutedEventArgs e)
        {
            FilterEUDegreesStackPanel.ToggleCheckBoxes();
        }
    }
}
