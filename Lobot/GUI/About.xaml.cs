﻿using System.Reflection;
using System.Windows;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About(MainWindow mainwindow)
        {
            this.Owner = mainwindow;

            InitializeComponent();

            AboutVersionLabel.Content = "Version " + Assembly.GetEntryAssembly().GetName().Version;
            AboutCopyrightLabel.Content = "Copyright " + System.Windows.Forms.Application.CompanyName + "© 2016";
        }

        private void buttonAboutOk_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            this.Owner.Show();
        }
    }
}
