﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotDLL.Connection;
using LobotDLL.Packets;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class UCLogin : System.Windows.Controls.UserControl
    {
        bool EnableConnectButton => DivisionServerComboBox.Items.Count > 0
                    && !string.IsNullOrEmpty(ServerAddressTextBox.Text)
                    && !string.IsNullOrEmpty(ServerPortTextBox.Text);

        public UCLogin()
        {
            InitializeComponent();
            // Try to load client using settings
            TryGetClientAndMediaPK2FromPath(LoginSettings.LoginClientDirectoryPath);
            InitializeEventHandlers();
        }

        private void InitializeEventHandlers()
        {
            LoginSettings.ToggledCanLogin += ToggleLoginButton;
            Bot.BotStatusChanged += HandleBotStatusChanged;
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ToggleLoginButton(object sender, EventArgs e)
        {
            if (LoginButton == null)
            {
                return;
            }

            App.Current.Dispatcher.Invoke((System.Action)delegate // ObservableCollections can only be manipulated by the UI thread
            {
                if (!string.IsNullOrEmpty(UsernametextBox.Text) && !string.IsNullOrEmpty(PasswordtextBox.Password) && LoginSettings.CanLogin)
                {
                    LoginButton.IsEnabled = true;
                }
                else
                {
                    LoginButton.IsEnabled = false;
                }
            });

        }

        private void HandleBotStatusChanged(object sender, EventArgs e)
        {
            App.Current.Dispatcher.Invoke((System.Action)delegate // ObservableCollections can only be manipulated by the UI thread
            {
                if (Bot.Status == BotStatus.Disconnected)
                {
                    ClientPathButton.IsEnabled = true;
                    DivisionServerComboBox.IsEnabled = true;
                    ConnectButton.IsEnabled = EnableConnectButton;
                    DisconnectButton.IsEnabled = false;
                }
                else
                {
                    ClientPathButton.IsEnabled = false;
                    DivisionServerComboBox.IsEnabled = false;
                    ConnectButton.IsEnabled = false;
                    DisconnectButton.IsEnabled = true;
                }

                if (Bot.Status == BotStatus.Login)
                {
                    CaptchaSubmitButton.IsEnabled = true;
                }
                else
                {
                    CaptchaSubmitButton.IsEnabled = false;
                }

                if (Bot.Status == BotStatus.CharacterSelect)
                {
                    SelectCharacterComboBox.IsEnabled = true;
                    ConfirmSelectCharacterButton.IsEnabled = true;
                }
                else
                {
                    SelectCharacterComboBox.IsEnabled = false;
                    ConfirmSelectCharacterButton.IsEnabled = false;
                }
            });
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            Loader.Instance.StartClient(LoginSettings.LoginClientDirectoryPath, Proxy.Instance.LocalGWPort, LoginSettings.Locale);
        }

        private void HideBotButton_Click(object sender, RoutedEventArgs e)
        {
            //this.WindowState = WindowState.Minimized;
        }

        private void TryGetClientAndMediaPK2FromPath(string directoryPath)
        {
            if (string.IsNullOrWhiteSpace(directoryPath) || !Directory.Exists(directoryPath))
            {
                return;
            }

            foreach (string filepath in Directory.GetFiles(directoryPath))
            {
                string currentfile = filepath.ToLower();

                if (currentfile.Contains("sro_client.exe"))
                {
                    NetworkGlobal.ClientFilePath = filepath;
                }
                else if (currentfile.Contains("media.pk2"))
                {
                    LoginSettings.LoginClientDirectoryPath = directoryPath;
                }
            }

            if (!string.IsNullOrWhiteSpace(NetworkGlobal.ClientFilePath) && !string.IsNullOrWhiteSpace(PK2DataGlobal.PK2MediaFilePath))
            {
                LoginSettings.LoginClientDirectoryPath = directoryPath;
                ClientPathTextBox.Text = directoryPath + "\\sro_client.exe";
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"sro_client path({LoginSettings.LoginClientDirectoryPath}) does not contain \"media.pk2\" and \"sro_client.exe\""));
                Logger.Log(LogLevel.PK2, $"sro_client path({LoginSettings.LoginClientDirectoryPath}) does not contain \"media.pk2\" and \"sro_client.exe\"");
            }

        }

        /// <summary>
        /// Set sro_client path for logging in.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientPathButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = " Select sro_client.exe";
            fileDialog.Filter = " sro_client.exe (sro_client.exe)| sro_client.exe";
            fileDialog.FilterIndex = 1;
            fileDialog.Multiselect = false;

            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string directoryPath = Path.GetDirectoryName(fileDialog.FileName);
                TryGetClientAndMediaPK2FromPath(directoryPath);
            }
        }

        private void ClientPathTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            try
            {
                Bot.Setup();
                Proxy.Instance.Listen();

                // Remove old entries
                DivisionServerComboBox.Items.Clear();

                // Update other text boxes after getting Server info
                DivisionServerComboBox.Items.Insert(0, NetworkGlobal.GatewayServerDivision);
                DivisionServerComboBox.SelectedIndex = 0;
                ServerAddressTextBox.Text = NetworkGlobal.GatewayIP;
                ServerPortTextBox.Text = NetworkGlobal.AgentPort.ToString();

                ConnectButton.IsEnabled = DivisionServerComboBox.Items.Count > 0
                    && !String.IsNullOrEmpty(ServerAddressTextBox.Text)
                    && !String.IsNullOrEmpty(ServerPortTextBox.Text);
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"sro_client path({LoginSettings.LoginClientDirectoryPath}) does not contain \"media.pk2\" and \"sro_client.exe\""));
                Logger.Log(LogLevel.PK2, $"sro_client path({LoginSettings.LoginClientDirectoryPath}) does not contain \"media.pk2\" and \"sro_client.exe\"");
            }
        }

        private void UsernametextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            LoginSettings.Username = UsernametextBox.Text;
        }

        private void PasswordtextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            LoginSettings.Password = PasswordtextBox.Password;
            ToggleLoginButton(sender, e);
        }

        private void CharacterTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            LoginSettings.CharName = CharacterTextBox.Text;
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (LoginSettings.CanLogin && UsernametextBox.Text.Length > 0)
            {
                Proxy.Instance.SendCommandGW(new LoginGateWay(UsernametextBox.Text, PasswordtextBox.Password, LoginSettings.Locale, LoginSettings.ShardID));
            }
        }

        private void SelectCharacterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectCharacterComboBox.Items.Count > 0)
            {
                SelectCharacterComboBox.IsEnabled = true;
                SelectCharacterComboBox.IsEditable = false;
                ConfirmSelectCharacterButton.IsEnabled = true;
            }
            else
            {
                SelectCharacterComboBox.IsEnabled = false;
                SelectCharacterComboBox.IsEditable = true;
                ConfirmSelectCharacterButton.IsEnabled = false;
            }
        }

        private void ConfirmSelectCharacterButton_Click(object sender, RoutedEventArgs e)
        {
            string charName = SelectCharacterComboBox.Items[SelectCharacterComboBox.SelectedIndex].ToString();
            Proxy.Instance.SendCommandAG(new SelectCharacter(charName));
            SelectCharacterComboBox.IsEnabled = false;
            ConfirmSelectCharacterButton.IsEnabled = false;
        }

        private void SaveSettingsButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Logout Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Proxy.Instance.SendCommandAG(new Logout(LogoutMode.Exit));
            }
        }
    }
}
