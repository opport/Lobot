﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Lobot.GUI
{
    /// <summary>
    /// Valueconverter for accessing index of collection nested in ObservableCollection in skills tab.
    /// <see href="https://stackoverflow.com/questions/3843829/how-can-i-bind-against-the-index-of-a-listboxitem"/>
    /// </summary>
    public class ItemContainerToZIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var itemContainer = (DependencyObject)value;
            var itemsControl = DependencyObjectExtensions.FindAncestor<ItemsControl>(itemContainer);
            int index = itemsControl.ItemContainerGenerator.IndexFromContainer(itemContainer);
            return -index;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    public static class DependencyObjectExtensions
    {
        public static T FindAncestor<T>(this DependencyObject obj) where T : DependencyObject
        {
            var tmp = VisualTreeHelper.GetParent(obj);
            while (tmp != null && !(tmp is T))
            {
                tmp = VisualTreeHelper.GetParent(tmp);
            }
            return (T)tmp;
        }
    }
}
