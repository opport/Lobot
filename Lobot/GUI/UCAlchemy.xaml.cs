﻿using LobotAPI.Globals.Settings;
using LobotAPI.Structs;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;

namespace Lobot.GUI
{
    /// <summary>
    /// Interaction logic for UCAlchemy.xaml
    /// </summary>
    public partial class UCAlchemy : UserControl
    {
        public UCAlchemy()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            // Use SelectionStart property to find the caret position.
            // Insert the previewed text into the existing text in the textbox.
            var fullText = textBox.Text.Insert(textBox.SelectionStart, e.Text);

            uint val;
            // If parsing is successful, set Handled to false
            e.Handled = !uint.TryParse(fullText, out val);
        }

        // Use to filter Equipment only from inventory
        private void CollectionViewSource_Filter_EquipmentItems(object sender, FilterEventArgs e)
        {
            if (e.Item.GetType() == typeof(KeyValuePair<byte, Item>))
            {
                Item item = ((KeyValuePair<byte, Item>)e.Item).Value;
                e.Accepted = item is Equipment;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void InventoryListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InventoryListView != null)
            {
                Equipment equipment = ((KeyValuePair<byte, Item>)InventoryListView.SelectedItem).Value as Equipment;
                AlchemySettings.SelectedItem = equipment;
            }
        }
    }
}
