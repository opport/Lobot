﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Lobot
{
    public static class StackPanelExtensions
    {
        /// <summary>
        /// Toggle all checkboxes in stackpanel to checked or unchecked status.
        /// If any checkbox is unchecked, all checkboxes will be checked.
        /// Otherwise all checkboxes will be unchecked.
        /// </summary>
        /// <param name="panel"></param>
        public static void ToggleCheckBoxes(this StackPanel panel)
        {
            IEnumerable<CheckBox> toBeChecked = panel.Children.OfType<CheckBox>().Where(c => c.IsChecked == false).ToList();

            if (toBeChecked.Count() < 1)
            {
                foreach (CheckBox cb in panel.Children.OfType<CheckBox>())
                {
                    cb.IsChecked = false;
                }
            }
            else
            {
                foreach (CheckBox cb in toBeChecked)
                {
                    cb.IsChecked = true;
                }
            }
        }
    }
}
