﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Lobot
{
    /// <summary>
    /// Converter used to bind multiple radiobuttons to a single property
    /// <see href="https://stackoverflow.com/questions/397556/how-to-bind-radiobuttons-to-an-enum/2908885#2908885"/>
    /// </summary>
    public class EnumBooleanConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Boolean Parameter guard
            if (parameter is bool)
            {
                return (bool)value == (bool)parameter;
            }

            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            if (Enum.IsDefined(value.GetType(), value) == false)
                return DependencyProperty.UnsetValue;

            if (parameterString.Contains("|"))
            {
                string[] parameters = parameterString.Split(new char[] { '|' });
                Type type = value.GetType();

                foreach (string p in parameters)
                {
                    object result = Enum.Parse(value.GetType(), p);

                    if (result.Equals(value))
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {
                object parameterValue = Enum.Parse(value.GetType(), parameterString);

                return parameterValue.Equals(value);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // Boolean Parameter guard to prevent red rectangular outlines <seealso href="https://stackoverflow.com/a/3636821/9597363"/>
            if (parameter is bool)
            {
                return (bool)value ? parameter : Binding.DoNothing;
            }

            string parameterString = parameter as string;
            if (parameterString == null)
                return DependencyProperty.UnsetValue;

            if (parameterString.Contains("|"))
            {
                string[] parameters = parameterString.Split(new char[] { '|' });

                foreach (string p in parameters)
                {
                    try
                    {
                        return Enum.Parse(targetType, p);
                    }
                    catch (Exception) { }
                }

                return false;
            }

            return Enum.Parse(targetType, parameterString);
        }
        #endregion
    }
}
