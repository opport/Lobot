﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.PK2;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using System;
using System.Linq;
using System.Timers;
using static LobotAPI.Globals.Settings.AutoPotionSettings;

namespace LobotDLL.StateMachine
{
    public static class AutoPotions
    {
        public const uint MAX_HGP = 10000;
        public const uint HGP_TICK_TIME = 3000;
        public readonly static Timer HGPTimer = new Timer(HGP_TICK_TIME);

        static AutoPotions()
        {
            HGPTimer.Elapsed += HandleHGPTick;
        }

        /// <summary>
        /// Handle changes by pet update handler 0x30C9
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void HandleHGPChanged(object sender, BoolEventArgs e)
        {
            if (e.Flag)
            {
                HGPTimer.Start();
            }
            else
            {
                HGPTimer.Stop();
            }
        }

        private static void HandleHGPTick(object sender, ElapsedEventArgs e)
        {
            if (Bot.GrowthPet?.HGP > 0)
            {
                Bot.GrowthPet.HGP--;
                TryUseHGPPot(Bot.GrowthPet.UniqueID);
            }
            else
            {
                HGPTimer.Stop();
            }
        }

        private static bool SendUsePotionCommand(int potionIndex, Timer timer, uint targetID = 0)
        {
            if (!timer.Enabled)
            {
                Proxy.Instance.SendCommandAG(new UseItem((byte)potionIndex, UseItemType.UseOnPet, targetID));
                timer.Start();
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool ShouldUsePot(uint currentValue, uint maxValue, int percentage, bool useCondition1, bool useCondition2 = true)
        {
            return useCondition1 && useCondition2
                && (Decimal.Divide(currentValue, maxValue) * 100) < percentage;
        }

        private static void TryUsePot(Timer timer, uint resourceCurrent, uint resourceMax, int resourceUsePercent, bool useResource, Pk2ItemType type, Pk2ItemType type2 = Pk2ItemType.Other)
        {
            if (Bot.CharData.IsAlive && ShouldUsePot(resourceCurrent, resourceMax, resourceUsePercent, useResource))
            {
                LobotAPI.Structs.Item potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == type).Value;
                if (potion != null)
                {
                    SendUsePotionCommand(potion.Slot, timer);
                }
                else if (type2 != Pk2ItemType.Other)
                {
                    potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == type).Value;

                    if (potion != null)
                    {
                        SendUsePotionCommand(potion.Slot, timer);
                    }
                }
            }
        }


        public static void TryUseHPPot()
        {
            if (!PlayerSettings.HPPotionTimer.Enabled)
            {
                TryUsePot(PlayerSettings.HPPotionTimer,
                    Bot.CharData.CurrentHP,
                    Bot.CharInfo.MaxHP,
                    PlayerSettings.HPPotPercent,
                    PlayerSettings.HPUsePot, Pk2ItemType.HPPot, Pk2ItemType.HPGrain);
            }
        }

        public static void TryUseMPPot()
        {
            if (!PlayerSettings.MPPotionTimer.Enabled)
            {
                TryUsePot(PlayerSettings.MPPotionTimer,
                    Bot.CharData.CurrentMP,
                    Bot.CharInfo.MaxMP,
                    PlayerSettings.MPPotPercent,
                    PlayerSettings.MPUsePot, Pk2ItemType.MPPot, Pk2ItemType.MPGrain);
            }
        }

        /// <summary>
        /// Uses a vigour potion after checking whether conditions to use potions are met and potions are available.
        /// The potion is used then a timer signifying the potion delay is started.
        /// </summary>
        /// <returns>Whether a command to use the potion was sent</returns>
        public static bool TryUseVigour()
        {
            if (ShouldUsePot(Bot.CharData.CurrentHP, Bot.CharInfo.MaxHP,
                PlayerSettings.HPVigourPercent,
                PlayerSettings.HPUseVigour,
                ShouldUsePot(Bot.CharData.CurrentMP, Bot.CharInfo.MaxMP,
                PlayerSettings.MPVigourPercent,
                PlayerSettings.MPUseVigour)))
            {
                LobotAPI.Structs.Item potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.VigourPot || item.Value.Type == Pk2ItemType.VigourGrain).Value;
                if (potion != null)
                {
                    return SendUsePotionCommand(potion.Slot, PlayerSettings.VigourTimer);
                }
                else
                {
                    potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.VigourGrain).Value;
                    if (potion != null)
                    {
                        return SendUsePotionCommand(potion.Slot, PlayerSettings.VigourTimer);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public static void TryCureBadStatusOrCureDebuff()
        {
            if (Bot.CharData.DebuffState == DebuffStatus.BadStatus)
            {
                TryCureBadStatus();
            }
            else if (Bot.CharData.DebuffState == DebuffStatus.Debuff)
            {
                TryCureDebuff();
            }
        }

        public static void TryCureBadStatus()
        {
            if (Bot.CharData.DebuffState == DebuffStatus.BadStatus)
            {
                if (PlayerSettings.BadStatusPillOrSpell
                    && PlayerSettings.BadStatusSpell != 0
                    && CharInfoGlobal.Skills.FirstOrDefault(s => s.RefSkillID == PlayerSettings.BadStatusSpell).SkillEnabled == 0x01)
                {
                    Proxy.Instance.SendCommandAG(new UseSkill(PlayerSettings.BadStatusSpell, ObjectActionType.UseSkill, Bot.CharData.UniqueID));
                }
                else if (!PlayerSettings.UniversalPillTimer.Enabled)
                {
                    LobotAPI.Structs.Item potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.UniversalPill).Value;
                    if (potion != null)
                    {
                        SendUsePotionCommand(potion.Slot, PlayerSettings.UniversalPillTimer);
                    }
                }

            }
        }

        //TODO find out what difference to universal pill bad status is
        public static void TryCureDebuff()
        {
            if (Bot.CharData.DebuffState == DebuffStatus.Debuff || Bot.CharData.ActiveBuffs.Any(b => b.Type == Pk2SkillType.Debuff))
            {
                if (PlayerSettings.CursePillOrSpell
                    && PlayerSettings.CurseSpell != 0
                    && CharInfoGlobal.Skills.FirstOrDefault(s => s.RefSkillID == PlayerSettings.CurseSpell).SkillEnabled == 0x01)
                {
                    Proxy.Instance.SendCommandAG(new UseSkill(PlayerSettings.CurseSpell, ObjectActionType.UseSkill, Bot.CharData.UniqueID));
                }
                else
                {
                    LobotAPI.Structs.Item pill = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.PurificationPill).Value;
                    if (pill != null)
                    {
                        SendUsePotionCommand(pill.Slot, PlayerSettings.PurificationTimer);
                    }
                }
            }
        }

        public static void TryCurePetBadStatus()
        {
            if (Bot.GrowthPet.BadStatus != 0 && !PetSettings.PillTimer.Enabled)
            {
                LobotAPI.Structs.Item potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.PetPill).Value;
                if (potion != null)
                {
                    SendUsePotionCommand(potion.Slot, PlayerSettings.UniversalPillTimer);
                }
            }
        }

        //TODO
        public static bool TryUseCOSPot(uint cosID, uint resourceCurrent, uint resourceMax, int resourceUsePercent, bool useResource, string potionNamePrefix)
        {
            if (Bot.GrowthPet.IsAlive && ShouldUsePot(resourceCurrent, resourceMax, resourceUsePercent, useResource))
            {
                LobotAPI.Structs.Item potion = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.PetHPPotion).Value;
                if (potion != null)
                {
                    return SendUsePotionCommand(potion.Slot, PetSettings.PotionTimer, cosID);
                }
            }
            return false;
        }

        /// <summary>
        /// Uses an transport HP potion after checking whether conditions to use potions are met and potions are available.
        /// The potion is used then a timer signifying the potion delay is started.
        /// </summary>
        /// <returns>Whether a command to use the potion was sent</returns>
        public static void TryUseTransportHPPot(uint cosID)
        {
            if (!COSSettings.PotionDelay.Enabled)
            {
                TryUseCOSPot(cosID, Bot.Transport.CurrentHP, (uint)Bot.Transport.MaxHP,
                   COSSettings.HPPotPercent,
                   COSSettings.HPUsePot, "ITEM_ETC_COS_HP_POTION_");
            }
        }

        /// <summary>
        /// Uses a pet HP potion after checking whether conditions to use potions are met and potions are available.
        /// The potion is used then a timer signifying the potion delay is started.
        /// </summary>
        /// <returns>Whether a command to use the potion was sent</returns>
        public static void TryUsePetHPPot(uint cosID)
        {
            if (!PetSettings.PotionTimer.Enabled)
            {
                TryUseCOSPot(cosID, Bot.GrowthPet.CurrentHP, (uint)Bot.GrowthPet.MaxHP,
                   PetSettings.HPPotPercent,
                   PetSettings.HPUsePot, "ITEM_ETC_COS_HP_POTION_");
            }
        }

        /// <summary>
        /// Uses a pet pill after checking whether conditions to use pills are met and potions are available.
        /// The potion is used then a timer signifying the potion delay is started.
        /// </summary>
        /// <returns>Whether a command to use the potion was sent</returns>
        public static void TryUsePetPill(uint cosID)
        {
            if (!PetSettings.PotionTimer.Enabled)
            {
                TryUseCOSPot(cosID, Bot.GrowthPet.CurrentHP, (uint)Bot.GrowthPet.MaxHP,
                   PetSettings.HPPotPercent,
                   PetSettings.HPUsePot, "ITEM_ETC_COS_HP_POTION_");
            }
        }

        /// <summary>
        /// Uses an HGP potion after checking whether conditions to use potions are met and potions are available.
        /// The potion is used then a timer signifying the potion delay is started.
        /// </summary>
        /// <returns>Whether a command to use the potion was sent</returns>
        public static bool TryUseHGPPot(uint cosID)
        {
            return TryUseCOSPot(cosID, Bot.GrowthPet.HGP, MAX_HGP,
                PetSettings.HGPPotPercent,
                PetSettings.HGPUsePot, "ITEM_COS_P_HGP_POTION_");
        }
    }
}
