﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using LobotDLL.Scripting;
using System;
using System.Linq;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// The state of executing the script file to get to the bot area.
    /// </summary>
    public class TownScript : ScriptState
    {
        protected override void InitializeEventHandlers()
        {
            Initialize();
            BuffEndHandler.BuffEnded += HandleSpeedBuffEnded;
            SpeedBuffSetup();

            //InnerLoop.Elapsed += RepeatCommand;
            //OuterLoop.Elapsed += ExecuteNextCommand;

            ExecutedScriptCommand += OuterLoopFunction;
            CommandExpressionExtensions.SkippedCommand += HandleSkippedCommand;
            CommandExpressionExtensions.ExecutedShoppingCommand += HandleShoppingCommand;
            CommandExpressionExtensions.ExecutedSetAreaCommand += OuterLoopFunction;
            CommandExpressionExtensions.ExecutedDelayCommand += HandleExecutedCommand;
            CommandExpressionExtensions.ExecutedMoveCommand += HandleExecutedMoveCommand;
            CharMove.BotMovementRegistered += HandleExecutedMoveCommand;
            SkillHandler.TeleportSkillUsed += HandleTeleportSkill;
        }
        private void Initialize()
        {
            // skip initialization and continue with script if paused
            if (IsPaused)
            {
                IsPaused = false;
                return;
            }

            CurrentTown = ScriptUtils.CurrentTown(Bot.CharData.Position);
            Script = CurrentTown != null
                ? CurrentTown.TownScript
                : null;

            // Check if close to training area
            if (CanStartTraining())
            {
                Transition(GetNextState());
                return;
            }

            if (Script == null || Script.Count < 1)
            {
                Stop();
                return;
            }

            // Get closest point or first walkpoint in town
            CommandIndex = CurrentTown == null
                ? ScriptUtils.IndexOfClosestPoint(Bot.CharData.Position, Script)
                : ScriptUtils.GetFirstWalkPointIndex(Script);

            if (CommandIndex.Equals(-1))
            {
                Stop();
                //return to town
                Item returnScroll = CharInfoGlobal.Inventory.Values.FirstOrDefault(tuple => tuple.Type == LobotAPI.PK2.Pk2ItemType.ReturnScroll);
                if (returnScroll != null)
                {
                    Proxy.Instance.SendCommandAG(new UseItem(returnScroll.Slot));
                }

                return;
            }

            // TODO MetaExpression Enhancement
            //if (ScriptUtils.MatchesRequirements(script.Where(expr => expr is MetaExpression))
            //{
            //    //Try to execute
            //}
            //else
            //{
            //    //TODO: search script that fulfills the predicates given in the script
            //}

            //Get all commands after closest walkpoint
            Script = Script.Skip(CommandIndex).Where(expr => expr is CommandExpression).ToList();
            // Command set to -1 for innerloop to start with first item
            CommandIndex = -1;

            //Start with first command in new list
            //RepeatCommand(null, EventArgs.Empty);
        }

        protected override void RemoveEventHandlers()
        {
            BuffEndHandler.BuffEnded -= HandleSpeedBuffEnded;
            SpeedBuffCleanUp();

            // Clean up if not paused
            if (!IsPaused)
            {
                Script = null;
                CommandIndex = -1;
            }

            //InnerLoop.Elapsed -= RepeatCommand;
            //OuterLoop.Elapsed -= ExecuteNextCommand;

            ExecutedScriptCommand -= OuterLoopFunction;
            CommandExpressionExtensions.SkippedCommand -= HandleSkippedCommand;
            CommandExpressionExtensions.ExecutedShoppingCommand -= HandleShoppingCommand;
            CommandExpressionExtensions.ExecutedSetAreaCommand -= OuterLoopFunction;
            CommandExpressionExtensions.ExecutedDelayCommand -= HandleExecutedCommand;
            CommandExpressionExtensions.ExecutedMoveCommand -= HandleExecutedMoveCommand;
            CharMove.BotMovementRegistered -= HandleExecutedMoveCommand;
            SkillHandler.TeleportSkillUsed -= HandleTeleportSkill;

            Logger.Log(LogLevel.SCRIPT, "Stopping script");
            Console.WriteLine("Stopping script");
        }

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        public void HandleShoppingCommand(object sender, ShoppingArgs e)
        {
            Pause();
            Transition(e.ShoppingState, e.NPC);
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            // Reached the end of script
            if (CurrentScriptCommand == null)
            {
                TransitionToNextScript();
                return;
            }

            try
            {
                CurrentScriptCommand?.Execute();
            }
            catch (NotImplementedException ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0}", ex.Message));
                Logger.Log(LogLevel.SCRIPT, ex.Message);
                //OnExecutedScriptCommand(new ScriptCommandArgs(CurrentScriptCommand));
                InnerLoop.Stop();
                OuterLoop.Stop();
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            InnerLoop.Stop();
            OuterLoop.Stop();
            CommandIndex++;

            // Reached the end of script
            if (!IsIndexInRange(CommandIndex, Script.Count))
            {
                if (TransitionToNextScript())
                {
                    return;
                }
            }

            try
            {
                CurrentScriptCommand?.Execute();
            }
            catch (NotImplementedException ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0}", ex.Message));
                Logger.Log(LogLevel.SCRIPT, ex.Message);
                OnExecutedScriptCommand(new ScriptCommandArgs(CurrentScriptCommand));
            }
        }
    }
}
