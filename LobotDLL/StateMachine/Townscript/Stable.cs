﻿using LobotAPI.Globals.Settings;
using System;
using System.Collections.Generic;

namespace LobotDLL.StateMachine.Townscript
{
    public sealed class Stable : ShoppingState
    {
        public Stable() : base("_HORSE") { } // Some private versions might use "_STABLE"

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<uint, Func<int>> ShoppingList => new Dictionary<uint, Func<int>>()
        {
            {ShoppingSettings.TransportType, () => LobotAPI.Structs.ShoppingList.TransportQuantity},
            {ShoppingSettings.PetHPPotionType, () => LobotAPI.Structs.ShoppingList.PetHPPotionQuantity},
            {ShoppingSettings.PetPillType, () => LobotAPI.Structs.ShoppingList.PetPillQuantity},
            {ShoppingSettings.PetHGPID, () => LobotAPI.Structs.ShoppingList.HGPPotionQuantity},
            {ShoppingSettings.PetGrassOfLifeID, () => LobotAPI.Structs.ShoppingList.GrassOfLifeQuantity},
        };
    }
}
