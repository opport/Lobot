﻿using LobotAPI.Globals.Settings;
using System;
using System.Collections.Generic;

namespace LobotDLL.StateMachine.Townscript
{
    public class Potions : ShoppingState
    {
        public Potions() : base("_POTION") { }

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<uint, Func<int>> ShoppingList => new Dictionary<uint, Func<int>>()
        {
            {ShoppingSettings.HPPotionType, () => LobotAPI.Structs.ShoppingList.HPPotionQuantity},
            {ShoppingSettings.MPPotionType, () => LobotAPI.Structs.ShoppingList.MPPotionQuantity},
            {ShoppingSettings.VigorPotionType, () => LobotAPI.Structs.ShoppingList.VigorPotionQuantity},
            {ShoppingSettings.UniversalPillType, () => LobotAPI.Structs.ShoppingList.UniversalPillQuantity},
            {ShoppingSettings.PurificationPillType, () => LobotAPI.Structs.ShoppingList.PurificationPillQuantity},
        };
    }
}
