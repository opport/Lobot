﻿using LobotAPI.Globals.Settings;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using System;
using System.Collections.Generic;

namespace LobotDLL.StateMachine.Townscript
{
    public sealed class Smith : ShoppingState
    {
        private static bool areItemsRepaired = false;

        protected override Dictionary<uint, Func<int>> ShoppingList => new Dictionary<uint, Func<int>>()
        {
            { ShoppingSettings.ProjectileType, () => LobotAPI.Structs.ShoppingList.ProjectileQuantity}
        };

        public Smith() : base("_SMITH") { CharDataHandler.ParsedCharData += ResetItemsRepaired; }

        private static void ResetItemsRepaired(object sender, EventArgs e)
        {
            areItemsRepaired = false;
        }

        private void HandleItemsRepaired(object sender, EventArgs e)
        {
            NPCRepair.ItemsRepaired -= HandleItemsRepaired;
            areItemsRepaired = true;
            Console.WriteLine("[Shop] Repaired items");

            if (HasToSellItems)
            {
                SellNextItem(null, EventArgs.Empty);
            }
            else
            {
                BuyNextItem(null, EventArgs.Empty);
            }
        }

        private void Repair()
        {
            NPCRepair.ItemsRepaired += HandleItemsRepaired;
            Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCRepair(CurrentTarget.UniqueID)).ConfigureAwait(false);
        }

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (!areItemsRepaired)
            {
                Repair();
            }
            else
            {
                if (HasToSellItems)
                {
                    SellNextItem(null, EventArgs.Empty);
                }
                else
                {
                    BuyNextItem(null, EventArgs.Empty);
                }
            }
        }
    }
}
