﻿using LobotAPI.Globals.Settings;
using System;
using System.Collections.Generic;

namespace LobotDLL.StateMachine.Townscript
{
    public sealed class Special : ShoppingState
    {
        public Special() : base("_ACCESSORY") { } // Some versions use "_SPECIAL"

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<uint, Func<int>> ShoppingList => new Dictionary<uint, Func<int>>()
        {
            { ShoppingSettings.SpeedPotType, () => LobotAPI.Structs.ShoppingList.SpeedPotQuantity },
            { ShoppingSettings.ReturnScrollType, () => LobotAPI.Structs.ShoppingList.ReturnScrollQuantity },
            { ShoppingSettings.BeserkPotionType, () => LobotAPI.Structs.ShoppingList.BeserkPotionQuantity}
        };
    }
}
