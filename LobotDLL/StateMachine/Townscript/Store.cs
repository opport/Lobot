﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Packets.NPC;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.Storage;
using System;
using System.Linq;

namespace LobotDLL.StateMachine.Townscript
{
    /// <summary>
    /// Store action during townscript.
    /// 
    /// Starts after the abstract NPCInteractionstate phase is finished and an NPC has been opened.
    /// </summary>
    public sealed class Store : NPCInteractionState
    {
        /// <summary>
        /// The flag indicating whether the server will send storage info when the storage is open.
        /// This is true after teleporting and false after the first storageinfo packet is sent after teleporting.
        /// </summary>
        private bool IsStorageInfoNeeded = true;

        protected override NPCInteractionType InteractionType => NPCInteractionType.Store;

        private const ItemMovementType Type = ItemMovementType.InventoryToStorage;
        private Item CurrentItemToStore = null;

        public Store() : base("_WAREHOUSE")
        {
            CharDataHandler.ParsedCharData += HandleCharDataParsed;
            StorageInfoDataHandler.StorageParsed += HandleStorageParsed;
        }

        /// <summary>
        /// Reset storageinfo flag after teleporting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleCharDataParsed(object sender, EventArgs e)
        {
            IsStorageInfoNeeded = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleStorageParsed(object sender, EventArgs e)
        {
            IsStorageInfoNeeded = false;

            if (IsRunning)
            {
                Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new Packets.NPC.NPCOpenDialogue(CurrentTarget.UniqueID, InteractionType)).ConfigureAwait(false);
            }

            // Continue without interaction
            //InnerLoopFunction(null, EventArgs.Empty);
        }


        protected override void InitializeEventHandlers()
        {
            MoveStorageToStorage.ItemMoved += HandleItemSorted;
            MoveInventoryToStorage.ItemStored += HandleItemStored;
        }

        protected override void RemoveEventHandlers()
        {
            MoveStorageToStorage.ItemMoved -= HandleItemSorted;
            MoveInventoryToStorage.ItemStored -= HandleItemStored;
        }

        protected override void HandleNPCSelected(object sender, EventArgs e)
        {
            if (Bot.SelectedNPC.UniqueID == CurrentTarget.UniqueID
                && SpawnListsGlobal.AllSpawnEntries.TryGetValue(Bot.SelectedNPC.UniqueID, out SpawnListsGlobal.BotLists list))
            {
                Bot.SelectednpcIDChanged -= HandleNPCSelected;
                Proxy.Instance.SendCommandAG(new NPCCloseDialogue(CurrentTarget.UniqueID));

                // In case storage information has not been parsed yet, wait for info from the StroageInfoHandler 0x3049
                if (IsStorageInfoNeeded)
                {
                    // Client should send 7046 request to server, wait for this request
                    Packets.NPC.NPCOpenDialogue.InteractionStarted += HandleInteractionStarted;
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCOpenStorage(CurrentTarget.UniqueID)).ConfigureAwait(false);
                }
                else
                {
                    // Continue with waiting for interaction to start
                    Packets.NPC.NPCOpenDialogue.InteractionStarted += HandleInteractionStarted;
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new Packets.NPC.NPCOpenDialogue(CurrentTarget.UniqueID, InteractionType)).ConfigureAwait(false);

                    //Continue without interaction
                    //InnerLoopFunction(null, EventArgs.Empty);
                }
            }
        }

        protected override void GetNextTarget()
        {
            CurrentItemToStore = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Key >= CharInfoGlobal.EQUIPMENT_SLOTS && item.Value.Pk2Item.ShouldStore()).Value;
        }

        private void HandleItemSorted(object sender, ItemMoveArgs e)
        {
            for (byte i = (byte)CharInfoGlobal.StorageSize; i > 0; i--)
            {
                if (CharInfoGlobal.Storage.ContainsKey(i))
                {
                    MoveOrStackStorage(CharInfoGlobal.Storage[i]);
                    break;
                }
            }

            // continue with storing if no sorting is done
            HandleItemStored(null, null);
        }

        private uint GetNumberOfStacksToMoveStorage(byte fromSlot, byte toSlot)
        {
            long stacksAvailable = CharInfoGlobal.Storage[toSlot].MaxStacks - CharInfoGlobal.Storage[toSlot].StackCount;

            if (stacksAvailable > 0)
            {
                return stacksAvailable >= CharInfoGlobal.Storage[fromSlot].StackCount
                    ? CharInfoGlobal.Storage[fromSlot].StackCount // enough slots left
                    : CharInfoGlobal.Storage[toSlot].StackCount; // insufficient slots
            }
            else
            {
                return (uint)CharInfoGlobal.Storage[toSlot].MaxStacks - CharInfoGlobal.Storage[toSlot].StackCount;
            }
        }

        private void MoveOrStackStorage(Item currentItem)
        {
            for (byte i = 0; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (!CharInfoGlobal.Storage.ContainsKey(i)) // Move
                {
                    MoveItem command = new MoveItem(ItemMovementType.StorageToStorage, currentItem.Slot, i, (ushort)currentItem.StackCount, CurrentTarget.UniqueID);
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, command).ConfigureAwait(false);
                    return;
                }
                else if (CharInfoGlobal.Storage[i].RefItemID == currentItem.RefItemID && CharInfoGlobal.Storage[i].StackCount < CharInfoGlobal.Storage[i].MaxStacks) // Stack
                {
                    ushort stacksToMove = (ushort)GetNumberOfStacksToMoveStorage(currentItem.Slot, i);
                    MoveItem command = new MoveItem(ItemMovementType.StorageToStorage, currentItem.Slot, i, stacksToMove, CurrentTarget.UniqueID);
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, command).ConfigureAwait(false);
                    return;
                }
            }

            // continue with storing if no sorting is done
            HandleItemStored(null, null);
        }

        private void HandleItemStored(object sender, ItemMoveArgs e)
        {
            // Store next item if any left to store
            // find first free slot
            if (CharInfoGlobal.StorageSize == CharInfoGlobal.Storage.Count())
            {
                StopInteraction(null, EventArgs.Empty);
            }
            else
            {
                GetNextTarget();

                // Check if any item has been selected
                if (CurrentItemToStore == null)
                {
                    // Try to sort inventory then stop interaction
                    StartStacking();

                    // TODO try and retrieve items from pet
                    //

                    return;
                }

                byte toSlot = 0;
                for (byte i = 0; i < CharInfoGlobal.StorageSize; i++)
                {
                    if (!CharInfoGlobal.Storage.ContainsKey(i))
                    {
                        toSlot = i;
                        break;
                    }
                }
                MoveItem command = new MoveItem(Type, CurrentItemToStore.Slot, toSlot, (ushort)CurrentItemToStore.StackCount, CurrentTarget.UniqueID);
                Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, command).ConfigureAwait(false);
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            HandleItemSorted(null, null);
        }
    }
}
