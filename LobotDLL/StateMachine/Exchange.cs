﻿using System;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// The state of exchanging items with another player.
    /// </summary>
    public class Exchange : BotState
    {
        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InitializeEventHandlers()
        {
            throw new NotImplementedException();
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void RemoveEventHandlers()
        {
            throw new NotImplementedException();
        }
    }
}
