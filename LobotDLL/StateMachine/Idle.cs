﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using System;
using System.Timers;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// The default state.
    /// The bot should only be in this state before it is started or after a fatal error has occured.
    /// </summary>
    public class Idle : BotState
    {
        private static Timer DeathTimer;

        protected override void GetNextTarget()
        {
            //
        }

        private void ReturnOnDeathTimerElapsed(object sender, ElapsedEventArgs e)
        {
            DeathTimer.Stop();
            DeathTimer.Elapsed -= ReturnOnDeathTimerElapsed;
            DeathTimer.Dispose();
            Transition(Return);
        }

        protected override void InitializeEventHandlers()
        {
            // TODO accept res
            DeathTimer = new Timer((double)GeneralTrainingSettings.XMinutes * 60 * 1000);

            if (!Bot.CharData.IsAlive && GeneralTrainingSettings.ReturnDeadXMinutes)
            {
                if (GeneralTrainingSettings.XMinutes < 1)
                {
                    Transition(Return); // waiting less than a minute means instantaneous return attempt
                }
                else
                {
                    DeathTimer.Elapsed += ReturnOnDeathTimerElapsed;
                    DeathTimer.Start();
                }
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            //
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            // Stop of stalling
            if (Bot.CharData.InteractionMode == 4)
            {
                Transition(Idle);
                return;
            }

            // This should be invoked when bot is started and immediately switch to the next best state.
            if (!DeathTimer.Enabled)
            {
                Transition(GetNextState());
            }
        }

        protected override void RemoveEventHandlers()
        {
            //
            DeathTimer.Elapsed -= ReturnOnDeathTimerElapsed;
            DeathTimer.Stop();
            DeathTimer.Dispose();
        }
    }
}
