﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Inventory;
using System;
using System.Linq;

namespace LobotDLL.StateMachine
{
    public class PetItemTransfer : BotState
    {
        private const ItemMovementType Type = ItemMovementType.PetToInventory;
        private const int ITEM_MOVE_DELAY = 5000;
        private static Item CurrentItemToMove;

        protected override void GetNextTarget()
        {
            CurrentItemToMove = CharInfoGlobal.PetInventory.FirstOrDefault().Value;
        }

        protected override void InitializeEventHandlers()
        {
            MoveInventoryToInventory.ItemMoved += HandleInventoryItemSorted;
            MovePetToInventory.ItemMoved += HandleItemMoved;
        }

        protected override void RemoveEventHandlers()
        {
            MoveInventoryToInventory.ItemMoved -= HandleInventoryItemSorted;
            MovePetToInventory.ItemMoved -= HandleItemMoved;
            CurrentItemToMove = null;
        }

        protected virtual uint GetNumberOfStacksToMove(byte fromSlot, byte toSlot)
        {
            long stacksAvailable = CharInfoGlobal.PetInventory[toSlot].MaxStacks - CharInfoGlobal.PetInventory[toSlot].StackCount;

            if (stacksAvailable > 0)
            {
                return stacksAvailable >= CharInfoGlobal.PetInventory[fromSlot].StackCount
                    ? CharInfoGlobal.PetInventory[fromSlot].StackCount // enough slots left
                    : CharInfoGlobal.PetInventory[toSlot].StackCount; // insufficient slots
            }
            else
            {
                return (uint)CharInfoGlobal.PetInventory[toSlot].MaxStacks - CharInfoGlobal.PetInventory[toSlot].StackCount;
            }
        }
        private uint GetNumberOfStacksToMoveStorage(byte fromSlot, byte toSlot)
        {
            long stacksAvailable = CharInfoGlobal.Inventory[toSlot].MaxStacks - CharInfoGlobal.Inventory[toSlot].StackCount;

            if (stacksAvailable > 0)
            {
                return stacksAvailable >= CharInfoGlobal.Inventory[fromSlot].StackCount
                    ? CharInfoGlobal.Inventory[fromSlot].StackCount // enough slots left
                    : CharInfoGlobal.Inventory[toSlot].StackCount; // insufficient slots
            }
            else
            {
                return (uint)CharInfoGlobal.Inventory[toSlot].MaxStacks - CharInfoGlobal.Inventory[toSlot].StackCount;
            }
        }

        protected virtual bool StackItem(Item currentItem)
        {
            for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (CharInfoGlobal.PetInventory.ContainsKey(i)
                    && CharInfoGlobal.PetInventory[i].RefItemID == currentItem.RefItemID
                    && CharInfoGlobal.PetInventory[i].StackCount < CharInfoGlobal.PetInventory[i].MaxStacks) // Stack
                {
                    ushort stacksToMove = (ushort)GetNumberOfStacksToMove(currentItem.Slot, i);
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, stacksToMove);
                    Proxy.Instance.SendCommandAG(ITEM_MOVE_DELAY, command).ConfigureAwait(false);
                    return true;
                }
            }
            return false;
        }
        protected virtual void SortItem(Item currentItem)
        {
            for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (!CharInfoGlobal.PetInventory.ContainsKey(i)) // Move
                {
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, (ushort)currentItem.StackCount);
                    Proxy.Instance.SendCommandAG(ITEM_MOVE_DELAY, command).ConfigureAwait(false);
                    return;
                }
            }

            StopSorting();
        }
        protected void StartStacking()
        {
            MovePetToPet.ItemStacked += HandlePetItemStacked;
            HandlePetItemStacked(null, null);
        }
        protected void StopStacking()
        {
            MovePetToPet.ItemStacked -= HandlePetItemStacked;
            StartSorting();
        }
        protected void StartSorting()
        {
            MovePetToPet.ItemMoved += HandlePetItemSorted;
            HandlePetItemSorted(null, null);
        }
        protected void StopSorting()
        {
            MovePetToPet.ItemMoved -= HandlePetItemSorted;
            Stop();
        }
        protected void HandlePetItemStacked(object sender, ItemMoveArgs e)
        {
            for (byte i = CharInfoGlobal.PetInventorySize; i >= 0; i--)
            {
                if (CharInfoGlobal.PetInventory.ContainsKey(i))
                {
                    if (StackItem(CharInfoGlobal.PetInventory[i]))
                    {
                        return;
                    }
                }
            }
            // continue with sorting
            StopStacking();
        }
        protected void HandlePetItemSorted(object sender, ItemMoveArgs e)
        {
            for (byte i = CharInfoGlobal.PetInventorySize; i >= 0; i--)
            {
                if (CharInfoGlobal.PetInventory.ContainsKey(i))
                {
                    SortItem(CharInfoGlobal.PetInventory[i]);
                    break;
                }
            }
        }

        private void HandleInventoryItemSorted(object sender, ItemMoveArgs e)
        {
            for (byte i = Bot.CharData.Inventory.Size; i >= CharInfoGlobal.EQUIPMENT_SLOTS; i--)
            {
                if (CharInfoGlobal.Inventory.ContainsKey(i))
                {
                    MoveOrStackInventory(CharInfoGlobal.Inventory[i]);
                    break;
                }
            }
        }

        private void MoveOrStackInventory(Item currentItem)
        {
            // Start after Equipment slots
            for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < currentItem.Slot; i++)
            {
                // Find free slot to move or slot that can be stacked
                if (!CharInfoGlobal.Inventory.ContainsKey(i)) // Move
                {
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, (ushort)currentItem.StackCount);
                    Proxy.Instance.SendCommandAG(ITEM_MOVE_DELAY, command).ConfigureAwait(false);
                    return;
                }
                else if (CharInfoGlobal.Inventory[i].RefItemID == currentItem.RefItemID && CharInfoGlobal.Inventory[i].StackCount < CharInfoGlobal.Inventory[i].MaxStacks) // Stack
                {
                    ushort stacksToMove = (ushort)GetNumberOfStacksToMoveStorage(currentItem.Slot, i);
                    MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, currentItem.Slot, i, stacksToMove);
                    Proxy.Instance.SendCommandAG(ITEM_MOVE_DELAY, command).ConfigureAwait(false);
                    return;
                }
            }

            // continue with moving from pet to inventory if no sorting is done
            HandleItemMoved(null, null);
        }

        private void HandleItemMoved(object sender, ItemMoveArgs e)
        {
            // Move next item from pet to inventory if any left to move
            // find first free slot
            if (Bot.CharData.Inventory.Size == CharInfoGlobal.Inventory.Count())
            {
                Stop();
            }
            else
            {
                GetNextTarget();

                // Check if any item has been selected
                if (CurrentItemToMove == null)
                {
                    // Try to sort inventory then stop interaction
                    StartStacking();

                    return;
                }

                byte toSlot = 0;
                for (byte i = CharInfoGlobal.EQUIPMENT_SLOTS; i < Bot.CharData.Inventory.Size; i++)
                {
                    if (!CharInfoGlobal.Inventory.ContainsKey(i))
                    {
                        toSlot = i;
                        break;
                    }
                }
                MoveItem command = new MoveItem(Type, CurrentItemToMove.Slot, toSlot, (ushort)CurrentItemToMove.StackCount);
                Proxy.Instance.SendCommandAG(ITEM_MOVE_DELAY, command).ConfigureAwait(false);
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            HandleInventoryItemSorted(null, null);
        }
    }
}
