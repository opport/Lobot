﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Commands.Inventory;
using LobotAPI.PK2;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.NPC;
using System;
using System.ComponentModel;
using System.Linq;
using static LobotAPI.Globals.Settings.AutoPotionSettings;

namespace LobotDLL.StateMachine
{
    // Summon pet
    //[C -> S][704C]
    //16                                                petscroll slot
    //CC 08                                             itemType pet summon scroll

    //[S -> C][B04C]
    //01                                                success
    //16                                                petscroll slot
    //01 00                                             stacks
    //CC 08                                             itemType pet summon scroll

    //[S -> C][3040]
    //16                                                petscroll slot
    //40                                                update type pet?
    //02                                                status summoned

    // Use Grass of life
    //[C -> S][704C]
    //22                                                grass of life slot
    //EC 30                                             item type
    //11                                                target pet slot

    //[S -> C][B04C]
    //01                                                success
    //22                                                item used slot
    //02 00                                             stackcount
    //EC 30                                             item type


    public static class PetReviveFunctions
    {
        public static void InitializePetReviveHandlers()
        {
            PetSettings.StaticPropertyChanged += HandlePetReviveChanged;
            ObjectStatusHandler.ExitedCombat += HandleExitedCombat;
            UpdateInventory.PetScrollUpdated += HandlePetStatusChange;

            if (PetSettings.RevivePet)
            {
                TryRevive();
            }
        }

        public static void RemovePetReviveHandlers()
        {
            PetSettings.StaticPropertyChanged -= HandlePetReviveChanged;
            ObjectStatusHandler.ExitedCombat -= HandleExitedCombat;
            UpdateInventory.PetScrollUpdated -= HandlePetStatusChange;
        }

        private static bool ShouldSummonPet => PetSettings.RevivePet && Bot.Status == BotStatus.Botting && Bot.GrowthPet.UniqueID == 0;

        private static void TryRevive()
        {
            PetScroll petScroll = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.GrowthPet).Value as PetScroll;

            if (petScroll != null)
            {
                switch (petScroll.Status)
                {
                    case PetStatus.Summoned:
                        break;
                    case PetStatus.Unsummoned:
                    case PetStatus.Alive:
                        if (Bot.CharData.InCombat == 0)
                        {
                            Summon(petScroll);
                        }
                        break;
                    case PetStatus.Dead:
                        TryUseGrassOfLife(petScroll);
                        break;
                    default:
                        break;
                }
            }
            else
            {

            }
        }

        private static void TryUseGrassOfLife(PetScroll petScroll)
        {
            Item grassOfLife = CharInfoGlobal.Inventory.FirstOrDefault(item => item.Value.Type == Pk2ItemType.GrassOfLife).Value;

            if (grassOfLife != null)
            {
                Proxy.Instance.SendCommandAG(new UseItem(grassOfLife.Slot, UseItemType.UseOnItem, 0, petScroll.Slot));
            }
        }

        private static void Summon(PetScroll petScroll)
        {
            Proxy.Instance.SendCommandAG(new UseItem(petScroll.Slot));
        }

        private static void HandleExitedCombat(object sender, EventArgs e)
        {
            if (ShouldSummonPet)
            {
                TryRevive();
            }
        }

        private static void HandlePetSummoned(object sender, ItemUseArgs e)
        {
            if (e.Item.Type == Pk2ItemType.GrowthPet)
            {
                Logger.Log(LogLevel.BOTTING, $"{e.Item.ItemName} summoned from slot {e.Slot}");
            }
        }

        private static void HandlePetStatusChange(object sender, InventoryUpdateArgs e)
        {
            if (ShouldSummonPet && (e.UpdateType == 1 || e.UpdateType == 4))
            {
                TryRevive();
            }
        }

        private static void HandlePetReviveChanged(object sender, PropertyChangedEventArgs e)
        {
            if (ShouldSummonPet && e.PropertyName.Equals("RevivePet"))
            {
                TryRevive();
            }
        }
    }
}
