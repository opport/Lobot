﻿using LobotAPI.Globals.Settings;
using LobotDLL.Packets.Alchemy;
using System;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// The state of doing alchemy.
    /// </summary>
    public class Alchemy : BotState
    {
        /// <summary>
        /// The method being used in the inner loop.
        /// 
        /// </summary>
        private Action CurrentStrategy = UseElixir;

        private static void UseElixir()
        {

        }

        private static void UseJadeTablet()
        {

        }

        private static void UseRubyTablet()
        {

        }

        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InitializeEventHandlers()
        {
            AlchemyReinforce.ReinforceParsed += AlchemySettings.HandleReinforce;
        }
        protected override void RemoveEventHandlers()
        {
            AlchemyReinforce.ReinforceParsed -= AlchemySettings.HandleReinforce;
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            // Item should be chosen before Alchemy process is started
            if (AlchemySettings.SelectedItem == null)
            {
                AlchemySettings.Log.Add("Stopping Alchemy: No item found to enhance.");
                Stop();
            }

            if (AlchemySettings.SelectedItem.OptLevel < AlchemySettings.PlusGoal)
            {
                AlchemySettings.Log.Add($"Stopping Alchemy: Plus target {AlchemySettings.PlusGoal} reached.");
                Stop();
            }
        }
    }
}
