﻿using LobotAPI;
using LobotAPI.Globals;

using LobotAPI.Packets.Inventory;
using LobotAPI.Packets.NPC;
using LobotAPI.StateMachine;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.NPC;
using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace LobotDLL.StateMachine
{
    public abstract class ShoppingState : NPCInteractionState
    {
        private const int ItemMovementTypeIndex = 7;
        protected override NPCInteractionType InteractionType => NPCInteractionType.BuySell;

        protected static bool HasToSellItems => CharInfoGlobal.Inventory.Any(kvp => kvp.Key >= CharInfoGlobal.EQUIPMENT_SLOTS && kvp.Value.Pk2Item.ShouldSell());

        protected abstract Dictionary<uint, Func<int>> ShoppingList { get; }

        protected ShoppingState(string shopSignature, Timer innerLoop = null, Timer outerLoop = null) : base(shopSignature, innerLoop, outerLoop) { }

        /// <summary>
        /// Determine whether the client should receive the packet or not. The bot still evaluates the result.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static bool ShouldFilter(Packet p)
        {
            bool Isrunning = BotStateMachine.Instance.State is NPCInteractionState && BotStateMachine.Instance.State.IsRunning;
            bool IsInteractionPacket = p.Opcode == 0xB04B || p.Opcode == 0xB046 || p.Opcode == 0x3047 || p.Opcode == 0x3048 || p.Opcode == 0x3049;

            return Bot.IsInteracting && Isrunning && IsInteractionPacket;
        }
        /// <summary>
        /// Determine whether B034 response packet after buying should be spoofed for client to ensure that it accepts the bought/sold item
        /// </summary>
        public static bool ShouldBeFaked(Packet p)
        {
            if (p.Opcode == 0xB034 && BotStateMachine.Instance.State is NPCInteractionState && BotStateMachine.Instance.State.IsRunning)
            {
                p.Lock();
                byte[] itemMoveBytes = p.GetBytes();

                return itemMoveBytes[0] == 0x01
                    && (itemMoveBytes[1] == (byte)ItemMovementType.Buy || itemMoveBytes[1] == (byte)ItemMovementType.BuyBack); // || itemMoveBytes[1] == (byte)ItemMovementType.Sell); <-- not so sure about selling
            }
            return false;
        }

        public static Packet[] FakeItemMovement(Packet p)
        {
            Packet packetCopy = new Packet(p);
            packetCopy.Lock();
            Logger.Log(LogLevel.HANDLER, string.Format("{0:x} - {1} - {2} - {3}", packetCopy.Opcode, packetCopy.Encrypted.ToString(), packetCopy.Massive.ToString(), String.Join(" ", Array.ConvertAll(packetCopy.GetBytes(), x => x.ToString("X2")))));

            Packet start = new FakeItemSuccess(ObjectActionStage.Start).Execute();
            Packet end = new FakeItemSuccess(ObjectActionStage.End).Execute();
            Packet[] fakeItemPackets;

            byte success = packetCopy.ReadUInt8(); // 0x01 success
            byte type = packetCopy.ReadUInt8(); // 0x08 for buying
            type = (byte)ItemMovementType.PickUp;
            byte tab = packetCopy.ReadUInt8();
            byte slot = packetCopy.ReadUInt8();
            byte stacks = packetCopy.ReadUInt8();
            Item item = MoveBuy.FindItem(Bot.SelectedNPC.RefID, tab, slot);

            fakeItemPackets = new Packet[stacks];

            // Make sure item is registered in PK2 database 
            if (item != default(Item) && item != null)
            {
                ushort[] toSlots = new ushort[stacks];

                for (int i = 0; i < stacks; i++)
                {
                    toSlots[i] = packetCopy.ReadUInt8();
                }

                ushort amount = packetCopy.ReadUInt16();

                // For unstackable items, set amount to 1 for each stack
                if (stacks > 1)
                {
                    amount = 0x01;
                }

                for (int i = 0; i < stacks; i++)
                {
                    Packet fake = new Packet(p.Opcode, p.Encrypted, p.Massive);
                    fake.WriteUInt8(0x01); // success
                    fake.WriteUInt8(type); // type: pickup
                    fake.WriteUInt8(toSlots[i]);
                    fake.WriteUInt32(0x00); // user flag
                    fake.WriteUInt32(item.RefItemID);

                    // TODO: Get item stats straight from pk2
                    if (item is Equipment)
                    {
                        fake.WriteUInt8(0x00); //Enhancement +
                        fake.WriteUInt64(0x00); //Stats uint 64
                        fake.WriteUInt32((item as Equipment).Durability);
                        fake.WriteUInt8(0x00); //Blues amount
                        fake.WriteUInt8(1);
                        fake.WriteUInt8(2);
                        fake.WriteUInt8(3);
                        fake.WriteUInt8(4);
                    }
                    else if (item is PetScroll)
                    {
                        fake.WriteUInt8(0x03); //Alive
                        fake.WriteUInt32(item.RefItemID); //1LV wolf (no other pets in shop)
                        fake.WriteUInt16(0x00); //Name Length (NoName)
                        fake.WriteUInt8(0x00); //Unknown
                    }
                    else
                    {
                        fake.WriteUInt16(amount);
                    }

                    fakeItemPackets[i] = fake;
                }

                return fakeItemPackets;
            }
            else
            {
                return new Packet[0];
            }
        }

        protected override void HandleNPCSelected(object sender, EventArgs e)
        {
            if (Bot.SelectedNPC.UniqueID == CurrentTarget.UniqueID)
            {
                Bot.SelectednpcIDChanged -= HandleNPCSelected;

                if (PK2DataGlobal.Shops.ContainsKey((int)Bot.SelectedNPC.RefID) && SpawnListsGlobal.AllSpawnEntries.TryGetValue(Bot.SelectedNPC.UniqueID, out SpawnListsGlobal.BotLists list))
                {
                    // Continue with waiting for interaction to start
                    NPCOpenDialogue.InteractionStarted += HandleInteractionStarted;
                    //Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCToggleInteraction(CurrentTarget.UniqueID)).ConfigureAwait(false);
                    Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new NPCOpenDialogue(CurrentTarget.UniqueID, InteractionType)).ConfigureAwait(false);

                    //No interaction needed: Continue with buying and selling
                    //InnerLoopFunction(null, EventArgs.Empty);
                }
            }
        }

        protected override void InitializeEventHandlers()
        {
            Bot.BotIsInteractingChanged += OnStopInteraction;
            //NPCOpenDialogue.InteractionStarted += BuyNextItem;
            MoveItem.ItemMoveError += HandleItemMoveError;
            MoveBuy.ItemBought += HandleItemMoved;
            MovePickUp.ItemPickedUp += HandleItemMoved;
            MoveSell.ItemSold += HandleItemMoved;
        }

        protected override void RemoveEventHandlers()
        {
            Bot.BotIsInteractingChanged -= OnStopInteraction;
            //NPCOpenDialogue.InteractionStarted -= BuyNextItem;
            MoveItem.ItemMoveError -= HandleItemMoveError;
            MoveBuy.ItemBought -= HandleItemMoved;
            MovePickUp.ItemPickedUp -= HandleItemMoved; // keep track of fake pickup
            MoveSell.ItemSold -= HandleItemMoved;
        }

        /// <summary>
        /// Usually called when npc selection dialogue is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnStopInteraction(object sender, EventArgs e)
        {
            if (!Bot.IsInteracting)
            {
                Stop();
                Console.WriteLine($"[{GetType().Name}] Stopped interaction with {CurrentTarget?.Name}");
            }
        }

        protected void SellNextItem(object sender, EventArgs e)
        {
            Item item = CharInfoGlobal.Inventory.FirstOrDefault(kvp => kvp.Key >= CharInfoGlobal.EQUIPMENT_SLOTS && kvp.Value.Pk2Item.ShouldSell()).Value;

            if (item != default(Item))
            {
                Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new MoveItem(ItemMovementType.Sell, item.Slot, 0, (ushort)item.StackCount, CurrentTarget.UniqueID)).ConfigureAwait(false);
                Console.WriteLine("[Shop]Sell {0} {1}", item.StackCount, item.ItemName);
            }
            else
            {
                // Start buying if nothing left to sell
                BuyNextItem(null, EventArgs.Empty);
            }
        }

        protected void BuyNextItem(object sender, EventArgs e)
        {
            try
            {
                LobotAPI.Structs.Shop shop = SpawnListsGlobal.NPCSpawns.Any(npc => npc.Key == CurrentTarget.UniqueID)
                    ? PK2DataGlobal.Shops[(int)CurrentTarget.RefID]
                    : default(LobotAPI.Structs.Shop);

                uint itemID = ShoppingList.FirstOrDefault(kvp => shop.ShopItemEntries.ContainsKey(kvp.Key) && kvp.Value.Invoke() > 0).Key;
                int requiredStacks = itemID > 0
                    ? ShoppingList[itemID].Invoke()
                    : 0;

                if (requiredStacks > 0
                    && shop != default(Shop) && itemID != 0
                    && Bot.CharData.Inventory.Size > CharInfoGlobal.Inventory.Keys.Count
                    && shop.ShopItemEntries.TryGetValue(itemID, out ShopItemEntry item))
                {
                    if (requiredStacks >= item.MaxStacks) // Has to be bought more than once
                    {
                        Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new MoveItem(ItemMovementType.Buy, item.TabNumber, item.SlotNumber, (ushort)item.MaxStacks)).ConfigureAwait(false);
                        Console.WriteLine("[{0}]Buy {1} {2}", shop.Pk2Entry.Name, item.MaxStacks, PK2DataGlobal.Items[item.ItemID].Name);
                    }
                    else // Can be bought once
                    {
                        Proxy.Instance.SendCommandAG(NPC_ACTION_DELAY, new MoveItem(ItemMovementType.Buy, item.TabNumber, item.SlotNumber, (ushort)requiredStacks)).ConfigureAwait(false);
                        Console.WriteLine("[{0}]Buy {1} {2}", shop.Pk2Entry.Name, requiredStacks, PK2DataGlobal.Items[item.ItemID].Name);
                    }
                }
                else
                {
                    //StopInteraction(sender, e);

                    // No need to stop interaction
                    // Stack and sort bought items then continue with the walk script
                    StartStacking();
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LobotAPI.LogLevel.SCRIPT, ex.Message + ex.StackTrace);
                StartSorting();
            }
        }

        protected void HandleItemMoved(object sender, ItemMoveArgs e)
        {
            InnerLoopFunction(null, EventArgs.Empty);
        }
        protected void HandleItemMoveError(object sender, EventArgs e)
        {
            StartSorting();
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (HasToSellItems)
            {
                SellNextItem(null, EventArgs.Empty);
            }
            else
            {
                BuyNextItem(null, EventArgs.Empty);
            }
        }
    }
}
