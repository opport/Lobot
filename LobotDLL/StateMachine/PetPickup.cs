﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Pet;
using LobotDLL.Packets.Spawn;
using LobotDLL.StateMachine.Training;
using System;
using System.Threading.Tasks;

namespace LobotDLL.StateMachine
{
    public class PetPickup : Pickup
    {
        /// <summary>
        /// Separate target used for pickup pet
        /// </summary>
        private ItemDrop CurrentTargetItem = null;

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (CurrentTargetItem == null)
            {
                Stop();
                return;
            }
            else
            {
                // Use pet to pickup if 
                if (Bot.PickupPet.UniqueID != 0) // && Bot.PickupPet.Inventory != full
                {
                    Proxy.Instance.SendCommandAG(new PetAction(CurrentTargetItem.UniqueID));
                }
                else
                {
                    Stop();
                }
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            InnerLoop.Stop();
            OuterLoop.Stop();
            GetNextTarget();

            if (CurrentTargetItem == null)
            {
                Stop();
                return;
            }

            // Send pickup packet once, then send the packet on a loop, until item despawns
            InnerLoopFunction(null, EventArgs.Empty);
            InnerLoop.Start();
        }

        protected new async Task DelayUnIgnoreTarget(uint targetID)
        {
            await Task.Delay(IgnoreDelay);
            if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn))
            {
                (spawn as IIgnorable).IsIgnored = false;
            }
        }

        private void HandleStopMovement(object sender, CharStopMovementArgs e)
        {
            // Determine if character stopped to pick item (distance > 5 ) or got stopped on the way there
            if (e.uniqueID == Bot.PickupPet.UniqueID && CurrentTargetItem != null)
            {
                //ignore item for a while
                (CurrentTargetItem as IIgnorable).IsIgnored = true;
                DelayUnIgnoreTarget(CurrentTargetItem.UniqueID).ConfigureAwait(false);

                // Switch to next target
                OuterLoopFunction(null, EventArgs.Empty);
            }
        }

        protected void HandleDespawnItem(object sender, DespawnArgs e)
        {
            if (e.Type == SpawnListsGlobal.BotLists.Drops && CurrentTargetItem?.UniqueID == e.uniqueID)
            {
                CurrentTargetItem = null; // clean up for next target
                OuterLoopFunction(null, null);
            }
            else if (e.uniqueID == Bot.PickupPet.UniqueID)
            {
                Stop();
            }
        }

        protected override void GetNextTarget()
        {
            foreach (ItemDrop drop in SpawnListsGlobal.Drops.Values)
            {
                if (IsDropValidTarget(drop))
                {
                    // get closest drop
                    if (CurrentTargetItem == null
                        || ScriptUtils.IsPointInClosestArea(drop.Position)
                        && ScriptUtils.GetDistance(Bot.PickupPet.Position, CurrentTargetItem.Position) > ScriptUtils.GetDistance(Bot.PickupPet.Position, drop.Position))
                    {
                        CurrentTargetItem = drop;
                    }
                }
            }
        }

        protected override void InitializeEventHandlers()
        {
            SpawnHandler.Despawned += HandleDespawnItem;
            CharStopMovementHandler.CharStopMovement += HandleStopMovement;
        }

        protected override void RemoveEventHandlers()
        {
            SpawnHandler.Despawned -= HandleDespawnItem;
            CharStopMovementHandler.CharStopMovement -= HandleStopMovement;

            CurrentTargetItem = null;
        }
    }
}
