﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;

using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.StateMachine;
using LobotDLL.StateMachine.Training;
using System;

namespace LobotAPI.StateMachine
{
    public class BotStateMachine : FiniteStateMachine
    {
        //private static List<Monster> Monsters = new List<Monster>();
        //private static List<ItemDrop> Drops = new List<ItemDrop>();
        private static BotStateMachine instance;

        private BotStateMachine() { State = BotState.Idle; }

        /// <summary>
        /// Singleton constructor.
        /// </summary>
        public static BotStateMachine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BotStateMachine();
                }
                return instance;
            }
        }

        private void InitializeEventhandlers()
        {
            BotState.StateChanged += HandleStateChange;
            Bot.BotStatusChanged += HandleBotStatusChange;

            //CharMoveHandler.SpawnMovementRegistered += HandleSpawnMovement;
            //ParseMonster.Parsed += HandleMonsterSpawn;
            //ParseItem.Parsed += HandleDropSpawn;
            //SpawnHandler.Despawned += RemoveSpawn;
        }

        private void RemoveEventhandlers()
        {
            BotState.StateChanged -= HandleStateChange;
            Bot.BotStatusChanged -= HandleBotStatusChange;

            //CharMoveHandler.SpawnMovementRegistered -= HandleSpawnMovement;
            //ParseMonster.Parsed -= HandleMonsterSpawn;
            //ParseItem.Parsed -= HandleDropSpawn;
            //SpawnHandler.Despawned -= RemoveSpawn;
        }

        public override void Start()
        {
            if (IsRunning)
            {
                return;
            }
            IsRunning = true;
            Bot.Status = BotStatus.Botting;

            PetReviveFunctions.InitializePetReviveHandlers();
            InitializeEventhandlers();
            HandleStateChange(null, null);
        }

        public override void Stop()
        {
            if (!IsRunning)
            {
                return;
            }
            IsRunning = false;

            PetReviveFunctions.RemovePetReviveHandlers();
            RemoveEventhandlers();

            State.Stop();
            PrevState = BotState.Idle;
            State = BotState.Idle;
            Bot.Status = BotStatus.Idle;
            Logger.Log(LogLevel.BOTTING, "Stopping training");
            Console.WriteLine("Stopping training");
        }

        private bool ShouldReturnToTown(object sender, BoolEventArgs e)
        {
            return e.Flag;
        }

        private bool ShouldSwitchToAttackMode(CharMoveArgs e, ISpawnType spawn)
        {
            return spawn is Monster
                && ScriptUtils.IsPointInAnyArea(e.Destination)
                && !(State is Attack);
        }

        private void HandleStateChange(object sender, StateChangedArgs e)
        {
            if (e == null)
            {
                System.Diagnostics.Debug.WriteLine($"Start <{State.GetType().Name}>");
                State.Start();
            }
            else if (e.State == Instance.State)
            {
                throw new StateChangeException(e.State);
            }
            else if (e.State == BotState.Idle)
            {
                System.Diagnostics.Debug.WriteLine($"Switch to <Idle> and stop");
                Stop();
            }
            else
            {
                PrevState = State;
                State = e.State;
                System.Diagnostics.Debug.WriteLine($"Switch to state <{State.GetType().Name}>");
                State?.Start();
            }
        }

        private void HandleBotStatusChange(object sender, EventArgs e)
        {
            if (Bot.Status != BotStatus.Botting)
            {
                Stop();
            }
        }
    }
}
