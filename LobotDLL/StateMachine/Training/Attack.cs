﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Packets.Inventory;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.NPC;
using LobotDLL.Packets.Spawn;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LobotDLL.StateMachine.Training
{
    public class Attack : BotState
    {
        private readonly TimeSpan IgnoreDelay = new TimeSpan(15000);
        private static CancellationTokenSource Cancellation = new CancellationTokenSource();
        private static bool TargetSelected = false;
        private static IEnumerator<Skill> SkillsToCycle;

        private int MovementInterruptionCount = 0;

        public Attack() : base() { }

        private async Task DelayUnIgnoreTarget(uint targetID)
        {
            await Task.Delay(IgnoreDelay);
            if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn))
            {
                (spawn as IIgnorable).IsIgnored = false;
            }
        }

        /// <summary>
        /// Remove any imbue that hasn't beeen deleted from the buff list after its duration is over.
        /// </summary>
        /// <param name="imbue"></param>
        private static async void DispatchDelayedImbueDelete(Skill imbue)
        {
            // cancel previous scheduled buff deletion
            if (Cancellation.Token.CanBeCanceled)
            {
                Cancellation.Cancel();
            }
            Cancellation = new CancellationTokenSource();

            // queue new task to be run after delay
            await Task.Delay(imbue.Pk2Skill.Duration + 1000, Cancellation.Token).ContinueWith(task =>
            {
                if (Bot.CharData.ActiveBuffs.TryFind(b => b.Type == Pk2SkillType.Imbue, out Buff buff))
                {
                    Bot.CharData.ActiveBuffCount--;
                    Bot.CharData.ActiveBuffs.Remove(buff);
                }
            });
        }

        public static void HandleCharStopMovement(object sender, CharStopMovementArgs e)
        {
            Cancellation.Cancel();
        }

        private void HandleDropSpawn(object sender, SpawnArgs e)
        {
            // Switch to picking up if not under attack and drop is valid
            if (e.List == SpawnListsGlobal.BotLists.Drops
                && !SpawnListsGlobal.MonsterSpawns.Any(m => m.Value.IsAlive && m.Value.Target == Bot.CharData.UniqueID)
                && IsDropValidTarget(e.Spawn as ItemDrop))
            {
                if (Bot.PickupPet != null && !PetPickup.IsRunning)
                {
                    Task.Run(() =>
                    {
                        PetPickup.Start();
                    }).ConfigureAwait(false);
                }
                else
                {
                    Transition(Pickup, e.Spawn as ItemDrop);
                }
            }
        }

        private void OnStopMovement(object sender, EventArgs e)
        {
            // This should handle player input
            if (CurrentTarget != null)
            {
                return;
            }

            MovementInterruptionCount++;

            // Make sure movement stop was caused by something outside of attack range to rule out self-root for skillcast
            if (CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Primary, out Item weapon)
                && CurrentTarget != null
                && ScriptUtils.GetDistance(Bot.CharData.Position, CurrentTarget.Position) > weapon.Type.GetAttackRange() || MovementInterruptionCount > 5)
            {
                //ignore monster for a while
                if (CurrentTarget != null)
                {
                    (CurrentTarget as IIgnorable).IsIgnored = true;
                    DelayUnIgnoreTarget(CurrentTarget.UniqueID).ConfigureAwait(false);
                }

                // Switch to next target
                TargetSelected = false;
                CurrentTarget = null;
                OuterLoopFunction(null, EventArgs.Empty);
            }
        }

        private void OnBuffEnded(object sender, BuffArgs e)
        {
            if (e.RefID == SkillSettings.Imbue?.RefSkillID)
            {
                Proxy.Instance.SendCommandAG(new UseImbue(SkillSettings.Imbue.RefSkillID));
                DispatchDelayedImbueDelete(SkillSettings.Imbue); // backup in case no packet for imbue cancellation arrives
            }
            else if (SkillSettings.BuffSkills[0].Any(s => s.RefSkillID == e.RefID)
                || !SkillSettings.SpeedBuffOrPot && e.RefID == SkillSettings.SpeedBuff?.RefSkillID) // Any buff to be recast
            {
                Transition(CastBuff);
            }
        }

        private void OnSkillCast(object sender, SkillArgs e)
        {
            if (e.skill.RefSkillID == CurrentSkill?.RefSkillID)
            {
                InnerLoop.Stop();
                OuterLoop.Stop();
                // Wait until cast time before continuing with next skill

                // Make outerloop switch target if current one is not a valid warlock target
                if (CurrentTarget != null && !IsValidWarlockTarget(CurrentTarget as Monster))
                {
                    CurrentTarget = null;
                    TargetSelected = false;
                }

                OuterLoop.Interval = CurrentSkill.CastTime > 0 ? CurrentSkill.CastTime : 1;
                OuterLoop.Start();
            }
        }

        private void OnDespawn(object sender, DespawnArgs e)
        {
            if (e.Type == SpawnListsGlobal.BotLists.MonsterSpawns && e.uniqueID == CurrentTarget?.UniqueID)
            {
                System.Diagnostics.Debug.WriteLine($"Despawn {CurrentTarget.UniqueID.ToString("X")}");
                CurrentTarget = null;
                TargetSelected = false;

                OuterLoopFunction(null, EventArgs.Empty);
            }
        }

        private void HandleSkillError(object sender, SkillErrorArgs e)
        {
            switch (e.Type)
            {
                case SkillErrorType.Obstacle:
                    break;
                case SkillErrorType.WrongWeapon:
                    if (CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Primary, out Item weapon))
                    {
                        System.Diagnostics.Debug.WriteLine($"Wrong weapon type equipped: {weapon.Type}, needed: {CurrentSkill?.RequiredWeapon}");
                        Logger.Log(LogLevel.BOTTING, $"Wrong weapon type equipped: {weapon.Type}, needed: {CurrentSkill?.RequiredWeapon}");
                    }
                    else if (CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Secondary, out Item shield))
                    {
                        System.Diagnostics.Debug.WriteLine($"Wrong weapon type equipped: {shield.Type}, needed: {CurrentSkill?.RequiredWeapon}");
                        Logger.Log(LogLevel.BOTTING, $"Wrong weapon type equipped: {shield.Type}, needed: {CurrentSkill?.RequiredWeapon}");
                    }
                    break;
                case SkillErrorType.SkillOnCooldown:
                case SkillErrorType.InvalidTarget:
                    if (CurrentTarget != null)
                    {
                        // knocked down but non-knockdown skill used
                        if (CurrentTarget != null && (CurrentTarget as ICombatType).IsKnockedDown && !CurrentSkill.Type.IsKnockDownSkill())
                        {
                            Skill skill = GetSkillForMonsterType(SkillSettings.GeneralSkills, (CurrentTarget as Monster).Type);
                            if (skill != null)
                            {
                                CurrentSkill = skill;
                                InnerLoopFunction(null, EventArgs.Empty);
                                return;
                            }
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine($"Skill Error Type: {e.Type}, {CurrentTarget?.UniqueID.ToString("X")}--{CurrentTarget.Name} - Alive: {(CurrentTarget as ICombatType)?.IsAlive}");
                            (CurrentTarget as ICombatType).IsAlive = false;
                        }
                    }

                    BotState nextState = GetNextState();

                    if (nextState == this)
                    {
                        TargetSelected = false;
                        OuterLoopFunction(null, EventArgs.Empty);
                    }
                    else
                    {
                        TargetSelected = false;
                        Transition(nextState);
                    }
                    break;
                case SkillErrorType.NoBolts:
                    System.Diagnostics.Debug.WriteLine($"Skill Error Type: {e.Type}");
                    Item bolts = CharInfoGlobal.Inventory.FirstOrDefault(i => i.Value.Type == Pk2ItemType.Bolt).Value;
                    if (bolts != null)
                    {
                        System.Diagnostics.Debug.WriteLine($"Equipping ammo from slot {bolts.Slot}");
                        MoveItem command = new MoveItem(ItemMovementType.InventoryToInventory, bolts.Slot, (byte)EquipmentSlot.Secondary);
                        MoveInventoryToInventory.ItemMoved += HandleAmmoEquipped;
                        Proxy.Instance.SendCommandAG(command);
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine($"No ammo found. Returning to town");
                        //Transition(Return);
                    }
                    break;
                default:
                    break;
            }
        }

        private void HandleAmmoEquipped(object sender, ItemMoveArgs e)
        {
            if (e.ToSlot == (byte)EquipmentSlot.Secondary && CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Secondary, out Item value))
            {
                MoveInventoryToInventory.ItemMoved -= HandleAmmoEquipped;
                System.Diagnostics.Debug.WriteLine($"Ammo equipped");
                InnerLoopFunction(null, EventArgs.Empty);
            }
        }

        private void HandleDrewAggro(object sender, MonsterAggroArgs e)
        {
            // If monster is different from current target, weaker and should be prioritized
            if (CurrentTarget != null && CurrentTarget?.UniqueID != e.Monster.UniqueID
                && GeneralTrainingSettings.PrioritizeWeakest
                && (CurrentTarget as Monster).Type > e.Monster.Type
                && ScriptUtils.IsPointInAnyArea(e.Monster.Position))
            {
                CurrentTarget = e.Monster;
                TargetSelected = false;
            }
        }

        private void HandleDrewAggroPet(object sender, MonsterAggroArgs e)
        {
            if (CurrentTarget != null && CurrentTarget?.UniqueID != e.Monster.UniqueID && GeneralTrainingSettings.DefendPet && ScriptUtils.IsPointInAnyArea(e.Monster.Position))
            {
                CurrentTarget = e.Monster;
                TargetSelected = false;
            }
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            if (CurrentTarget == null)
            {
                return;
            }
            else if (!(CurrentTarget as ICombatType).IsAlive)
            {
                TargetSelected = false;
                OuterLoopFunction(null, EventArgs.Empty);
                return;
            }

            if (CurrentSkill != null && CurrentSkill?.SkillEnabled == 0x01)
            {
                System.Diagnostics.Debug.WriteLine($"Use skill {CurrentSkill.Name} on {CurrentTarget?.Name}({CurrentTarget?.UniqueID.ToString("X")}) HP({(CurrentTarget as Monster)?.CurrentHP}/{(CurrentTarget as Monster)?.MaxHP}) Alive:{(CurrentTarget as Monster)?.IsAlive} ");
                Proxy.Instance.SendCommandAG(new UseSkill(CurrentSkill.RefSkillID, ObjectActionType.UseSkill, CurrentTarget.UniqueID));
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"AutoAttack {CurrentTarget?.UniqueID.ToString("X")} HP({(CurrentTarget as Monster)?.CurrentHP}/{(CurrentTarget as Monster)?.MaxHP}) Alive:{(CurrentTarget as Monster)?.IsAlive} ");
                Proxy.Instance.SendCommandAG(new UseSkill(1, ObjectActionType.AutoAttack, CurrentTarget.UniqueID));
            }

            if (!Bot.CharData.ActiveBuffs.Any(b => b.Type == Pk2SkillType.Imbue)
                && SkillSettings.Imbue != null && SkillSettings.Imbue.SkillEnabled == 0x01)
            {
                Proxy.Instance.SendCommandAG(new UseImbue(SkillSettings.Imbue.RefSkillID));
                DispatchDelayedImbueDelete(SkillSettings.Imbue); // backup in case no packet for imbue cancellation arrives
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            InnerLoop.Stop();
            OuterLoop.Stop();
            if (CurrentTarget == null || !(CurrentTarget is Monster) || !(CurrentTarget as Monster).IsAlive || (CurrentTarget as Monster).IsIgnored)
            {
                GetNextTarget();

                // Transition if no target was found
                // otherwise, select target (this is the condition for the server to send HPMP updates with opcode 3057)
                if (CurrentTarget == null)
                {
                    Transition(GetNextState());
                    return;
                }
            }

            if (!TargetSelected && CurrentTarget != null)
            {
                Proxy.Instance.SendCommandAG(new NPCSelect(CurrentTarget.UniqueID));
                TargetSelected = true;
                System.Diagnostics.Debug.WriteLine($"Switch to {CurrentTarget?.UniqueID.ToString("X")}");
            }

            GetNextSkill();

            if (CurrentSkill == null)
            {
                Transition(GetNextState());
                return;
            }

            UseSkillOrSwitchWeapon(() => GetNextSkill());
        }

        private Monster GetClosestMonster()
        {
            Monster result = null;

            // get the monster within the shortest distance
            // excluding current target, which may be a corpse waiting to despawn
            foreach (KeyValuePair<uint, Monster> tuple in SpawnListsGlobal.MonsterSpawns.Where(m => m.Value.IsAlive && m.Value.UniqueID != CurrentTarget?.UniqueID && IsValidWarlockTarget(m.Value)))
            {
                if (ScriptUtils.IsPointInAnyArea(tuple.Value.Position)
                    && (result == null || ScriptUtils.GetDistance(Bot.CharData.Position, tuple.Value.Position) < ScriptUtils.GetDistance(Bot.CharData.Position, result.Position)))
                {
                    result = tuple.Value;
                }
            }
            return result;
        }

        protected override void GetNextTarget()
        {
            InnerLoop.Stop();

            CurrentTarget = GetClosestMonster();
            //Reset skill in order to start with initiation skill
            SkillsToCycle = null;
            CurrentSkill = null;
            MovementInterruptionCount = 0;
        }

        private Skill GetNextSkillInCycle(MonsterType type)
        {
            if (SkillsToCycle == null)
            {
                for (int i = monsterTypes.IndexOf(type); i > -1; i--)
                {
                    // get first skill list closest to monster type or just general skills, if none are found
                    if (SkillSettings.GeneralSkills[i].Count > 0 || i == 0)
                    {
                        SkillsToCycle = SkillSettings.GeneralSkills[i].GetEnumerator();
                        break;
                    }
                }
            }

            // Cycle through skill list for current monster type

            Skill firstSkill = SkillsToCycle.Current;
            Skill skill = null;

            // Make sure enum is not empty before cycling
            if (firstSkill != null)
            {
                do
                {
                    // Cycle to beginning if current element is last element
                    if (!SkillsToCycle.MoveNext())
                    {
                        SkillsToCycle.Reset();
                        SkillsToCycle.MoveNext();
                    }

                    // Warlock DOT checks
                    if ((CurrentTarget as Monster).Buffs.Select(b => b.Pk2Entry.Type == Pk2SkillType.WarlockDOT).Count() > 1
                        && SkillsToCycle.Current.Type == Pk2SkillType.WarlockDOT
                        || (CurrentTarget as Monster).Buffs.Any(b => b.RefSkillID == SkillsToCycle.Current.RefSkillID))
                    {
                        // Don't try to cast warlock DOT's if target already has them
                        SkillsToCycle.MoveNext();
                    }
                    else
                    {
                        if ((CurrentTarget as ICombatType).IsKnockedDown && SkillsToCycle.Current.Type.IsKnockDownSkill())
                        {
                            skill = SkillsToCycle.Current;
                        }
                        else if (SkillsToCycle.Current.Type != Pk2SkillType.KnockDownOnly)
                        {
                            skill = SkillsToCycle.Current;
                        }
                    }
                } while (firstSkill != SkillsToCycle.Current && skill == null);
            }

            // attempt to find skill from lower lists if nothing was found from cycling
            // loop through skill lists for decreasingly powerful monster types
            // to try and find a skill
            if (skill == null)
            {
                skill = GetSkillForMonsterType(SkillSettings.GeneralSkills, type);
            }

            return skill;
        }

        private Skill GetSkillForMonsterType(ObservableCollection<ObservableCollection<Skill>> skillCollection, MonsterType type)
        {
            Skill skill = null;

            // loop through skill lists for decreasingly powerful monster types
            // to try and find a skill
            for (int i = monsterTypes.IndexOf(type); i > -1; i--)
            {
                ICollection<Skill> skillList = skillCollection[i];

                // Filter out warlock dots already placed on target from list of skill candidates
                skillList = skillList.Where(s => !(CurrentTarget as Monster).Buffs.Any(b => b.RefSkillID == s.RefSkillID)).ToList();

                // Don't try to cast warlock DOT's if target already has them
                if ((CurrentTarget as Monster).Buffs.Select(b => b.Pk2Entry.Type == Pk2SkillType.WarlockDOT).Count() > 1)
                {
                    // Don't try to cast warlock DOT's if target already has them
                    skillList = skillList.Where(s => s.Type != Pk2SkillType.WarlockDOT).ToList();
                }

                if ((CurrentTarget as ICombatType).IsKnockedDown)
                {
                    skill = skillList.FirstOrDefault(s => CanSkillBeCast(s) && s.Type.IsKnockDownSkill());
                }
                else
                {
                    skill = skillList.FirstOrDefault(s => CanSkillBeCast(s) && s.Type != Pk2SkillType.KnockDownOnly);
                }

                if (skill != null)
                {
                    return skill;
                }
            }

            return skill;
        }

        /// <summary>
        /// Use this to prevent strong monsters like uniques influencing the kill range number.
        /// Number gained from polynomial of HP values for chinese monster lvl 1-80
        /// equation -> 135 + 2.07x + 1.33x^2 + 0.00907x^3
        /// <seealso href="https://docs.google.com/spreadsheets/d/1n27ixz3O2iWOcbYwJkhf3nrtgB92IjH1tp95olMMmD0/edit#gid=1818032259"/>
        /// </summary>
        private static readonly Func<int, int> GetAverageMonsterHealth = level => (int)(135 + 2.07 * level + 1.33 * Math.Pow(level, 2) + 0.00907 * (Math.Pow(level, 3)));

        /// <summary>
        /// Monster below 20% HP is within kill range.
        /// This takes the smaller value of the average monster level and the current monster's basic hp.
        /// </summary>
        private static readonly Predicate<Monster> IsWithinKillRange = m => m.CurrentHP < Math.Min((GetAverageMonsterHealth(m.Level) / 5), (m.MaxHP / m.Type.GetHPMultiplier()) / 5);

        private void GetNextSkill()
        {
            if (CurrentTarget != null)
            {
                if (IsWithinKillRange(CurrentTarget as Monster)) // Finish low hp monsters
                {
                    CurrentSkill = GetSkillForMonsterType(SkillSettings.FinisherSkills, (CurrentTarget as Monster).Type);

                    System.Diagnostics.Debug.WriteLine($"Finisher Skill <{CurrentSkill?.Name}> chosen");
                }
                else if (CurrentSkill == null) // start with initiation
                {
                    CurrentSkill = GetSkillForMonsterType(SkillSettings.InitiationSkills, (CurrentTarget as Monster).Type);

                    // Get general skill if no finisher or initiation skill found
                    if (CurrentSkill == null)
                    {
                        if (SkillSettings.CycleSkills)
                        {
                            CurrentSkill = GetNextSkillInCycle((CurrentTarget as Monster).Type);
                        }
                        else
                        {
                            CurrentSkill = GetSkillForMonsterType(SkillSettings.GeneralSkills, (CurrentTarget as Monster).Type);
                        }
                    }
                }
                else // Get normal skills
                {
                    if (SkillSettings.CycleSkills)
                    {
                        CurrentSkill = GetNextSkillInCycle((CurrentTarget as Monster).Type);
                    }
                    else
                    {
                        CurrentSkill = GetSkillForMonsterType(SkillSettings.GeneralSkills, (CurrentTarget as Monster).Type);
                    }
                }
            }
            //if no general skill found (list empty or everything on cooldown) get simple autoattack instead
            if (CurrentSkill == null && CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Primary, out Item value))
            {
                Pk2ItemType weaponType = value.Type;
                CurrentSkill = CharInfoGlobal.AutoAttackSkills[weaponType];
            }
        }

        protected override void InitializeEventHandlers()
        {
            UseSkillHandler.SkillCast += OnSkillCast;
            BuffEndHandler.BuffEnded += OnBuffEnded;
            SpawnHandler.Despawned += OnDespawn;
            ObjectStatusHandler.DeathRegistered += OnDespawn;
            CharStopMovementHandler.CharStopMovement += OnStopMovement;
            SkillHandler.TeleportSkillUsed += HandleTeleportSkill;
            AbstractSpawnParseStrategy.Parsed += HandleDropSpawn;
            UseSkillHandler.SkillCastError += HandleSkillError;

            UseSkillHandler.DrewMonsterAggro += HandleDrewAggro;
            UseSkillHandler.DrewMonsterAggroPet += HandleDrewAggroPet;
        }

        protected override void RemoveEventHandlers()
        {
            UseSkillHandler.SkillCast -= OnSkillCast;
            BuffEndHandler.BuffEnded -= OnBuffEnded;
            SpawnHandler.Despawned -= OnDespawn;
            ObjectStatusHandler.DeathRegistered -= OnDespawn;
            CharStopMovementHandler.CharStopMovement -= OnStopMovement;
            SkillHandler.TeleportSkillUsed -= HandleTeleportSkill;
            AbstractSpawnParseStrategy.Parsed -= HandleDropSpawn;
            UseSkillHandler.SkillCastError -= HandleSkillError;

            UseSkillHandler.DrewMonsterAggro -= HandleDrewAggro;
            UseSkillHandler.DrewMonsterAggroPet -= HandleDrewAggroPet;

            TargetSelected = false;
            CurrentSkill = null;
        }
    }
}
