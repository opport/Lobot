﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Stall;
using LobotDLL.Packets.Stall;
using System;

namespace LobotDLL.StateMachine
{
    public class Stalling : BotState
    {
        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InitializeEventHandlers()
        {
            StallCreate.StallCreated += HandleStallCreated;
            StallUpdate.StallUpdated += HandleStallUpdated;
            StallClose.StallClosed += HandleStallClosed;
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void RemoveEventHandlers()
        {
            throw new NotImplementedException();
        }


        private void HandleStallCreated(object sender, EventArgs e)
        {
            StallCreate.StallCreated -= HandleStallCreated;
            StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Stall opened");
        }

        private void HandleStallUpdated(object sender, StallUpdateArgs e)
        {
            switch (e.UpdateType)
            {
                case LobotAPI.Packets.Stall.StallUpdateType.UpdateItem:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.AddItem:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.RemoveItem:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.FleaMarketMode:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.State:
                    if (StallSettings.IsOpen)
                    {
                        StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Opened stall");
                    }
                    else
                    {
                        StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Modifying stall");
                    }
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.Message:
                    break;
                case LobotAPI.Packets.Stall.StallUpdateType.Name:
                    break;
                default:
                    break;
            }

            StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Stall closed");
        }

        private void HandleItemAdded(object sender, EventArgs e)
        {
            StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Stall closed");
        }

        private void HandleStallClosed(object sender, EventArgs e)
        {
            StallSettings.StallLog.Add($"{DateTime.Now.TimeOfDay.ToString("HH:mm:ss")} Stall closed");
        }
    }
}
