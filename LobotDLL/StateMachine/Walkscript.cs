﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Scripting;
using LobotAPI.StateMachine;
using LobotDLL.Packets.Char;
using LobotDLL.Scripting;
using System;
using System.Linq;

namespace LobotDLL.StateMachine
{
    /// <summary>
    /// The state of executing the script file to get to the bot area.
    /// </summary>
    public class Walkscript : ScriptState
    {
        protected override void GetNextTarget()
        {
            throw new NotImplementedException();
        }

        protected override void InitializeEventHandlers()
        {
            Initialize();
            SpeedBuffSetup();

            //InnerLoop.Elapsed += RepeatCommand;
            //OuterLoop.Elapsed += ExecuteNextCommand;

            ExecutedScriptCommand += OuterLoopFunction;
            CommandExpressionExtensions.SkippedCommand += HandleSkippedCommand;
            CommandExpressionExtensions.ExecutedSetAreaCommand += OuterLoopFunction;
            CommandExpressionExtensions.ExecutedDelayCommand += HandleExecutedCommand;
            CommandExpressionExtensions.ExecutedMoveCommand += HandleExecutedMoveCommand;
            CharMove.BotMovementRegistered += HandleExecutedMoveCommand;
            SkillHandler.TeleportSkillUsed += HandleTeleportSkill;
        }

        protected override void RemoveEventHandlers()
        {
            Script = null;
            CommandIndex = -1;

            // Clean up if not paused
            if (!IsPaused)
            {
                Script = null;
                CommandIndex = -1;
            }

            //InnerLoop.Elapsed -= RepeatCommand;
            //OuterLoop.Elapsed -= ExecuteNextCommand;

            ExecutedScriptCommand -= OuterLoopFunction;
            CommandExpressionExtensions.SkippedCommand -= HandleSkippedCommand;
            CommandExpressionExtensions.ExecutedSetAreaCommand -= OuterLoopFunction;
            CommandExpressionExtensions.ExecutedDelayCommand -= HandleExecutedCommand;
            CommandExpressionExtensions.ExecutedMoveCommand -= HandleExecutedMoveCommand;
            CharMove.BotMovementRegistered -= HandleExecutedMoveCommand;
            SkillHandler.TeleportSkillUsed -= HandleTeleportSkill;

            Logger.Log(LogLevel.SCRIPT, "Stopping script");
            Console.WriteLine("Stopping script");
        }
        private void Initialize()
        {
            // skip initialization and continue with script if paused
            if (IsPaused)
            {
                IsPaused = false;
                return;
            }

            // Check if close to training area
            if (CanStartTraining())
            {
                Transition(GetNextState());
                return;
            }

            Script = ExpressionParser.ParseScript(ScriptSettings.TrainingScriptPath);
            CommandIndex = ScriptUtils.IndexOfClosestPoint(Bot.CharData.Position, Script);

            if (Script == null || Script.Count < 1)
            {
                if (CommandIndex.Equals(-1))
                {
                    Logger.Log(LogLevel.SCRIPT, "Too far from next walk point");
                    Console.WriteLine("Too far from next walk point");
                }
                else
                {
                    Logger.Log(LogLevel.SCRIPT, "No walkscript found");
                    Console.WriteLine("No walkscript found");
                }

                Transition(Return);
                return;
            }
            // TODO MetaExpression Enhancement
            //if (ScriptUtils.MatchesRequirements(script.Where(expr => expr is MetaExpression))
            //{
            //    //Try to execute
            //}
            //else
            //{
            //    //TODO: search script that fulfills the predicates given in the script
            //}

            //Get all commands after closest walkpoint
            Script = Script.Skip(CommandIndex).Where(expr => expr is CommandExpression).ToList();
            // Command set to -1 for innerloop to start with first item
            CommandIndex = -1;

            //Start with first command in new list
            //RepeatCommand(null, EventArgs.Empty);
        }

        protected override void InnerLoopFunction(object sender, EventArgs e)
        {
            // Reached the end of script
            if (CurrentScriptCommand == null)
            {
                TransitionToNextScript();
                return;
            }

            try
            {
                CurrentScriptCommand?.Execute();
            }
            catch (NotImplementedException ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0}", ex.Message));
                Logger.Log(LogLevel.SCRIPT, ex.Message);
                //OnExecutedScriptCommand(new ScriptCommandArgs(CurrentScriptCommand));
                InnerLoop.Stop();
                OuterLoop.Stop();
            }
        }

        /// <summary>
        /// Make sure bot is always within training range afer script stops.
        /// If last position does not end up in or close to an area, Area1 is set to the current position.
        /// This is done to prevent the bot from just returning to town immediately.
        /// </summary>
        private void EnsureThatScriptEndsInTrainingArea()
        {
            if (ScriptUtils.DistanceToClosestArea > ScriptGlobal.MAX_GO_DISTANCE)
            {
                TrainingAreaSettings.Area1X = Bot.CharData.Position.X;
                TrainingAreaSettings.Area1Y = Bot.CharData.Position.Y;
            }
        }

        protected override void OuterLoopFunction(object sender, EventArgs e)
        {
            InnerLoop.Stop();
            OuterLoop.Stop();
            CommandIndex++;

            // Reached the end of script
            if (!IsIndexInRange(CommandIndex, Script.Count))
            {
                IState next = GetNextState();

                // prevent endless loop
                if (next == WalkScript || next == TownScript)
                {
                    EnsureThatScriptEndsInTrainingArea();
                    Transition(Idle);
                }
                else
                {
                    Transition(next);
                }

                return;
            }

            try
            {
                CurrentScriptCommand?.Execute();
            }
            catch (NotImplementedException ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0}", ex.Message));
                Logger.Log(LogLevel.SCRIPT, ex.Message);
                OnExecutedScriptCommand(new ScriptCommandArgs(CurrentScriptCommand));
            }
        }
    }
}
