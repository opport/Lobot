﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.StateMachine;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.StateMachine.Townscript;
using LobotDLL.StateMachine.Training;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace LobotDLL.StateMachine
{
    public abstract class BotState : IState
    {
        public static readonly double RepeatInterval = 5000;
        //private ItemSwitchArgs SwitchBackWeapons = null;
        protected static Skill CurrentSkill;

        public static readonly Idle Idle = new Idle();
        public static readonly TownScript TownScript = new TownScript();

        public static readonly Potions Potions = new Potions();
        public static readonly Smith Smith = new Smith();
        public static readonly Special Special = new Special();
        public static readonly Stable Stable = new Stable();
        public static readonly Store Store = new Store();

        public static readonly Walkscript WalkScript = new Walkscript();
        public static readonly Stalling Stalling = new Stalling();
        public static readonly Alchemy Alchemy = new Alchemy();
        public static readonly Exchange Exchange = new Exchange();
        public static readonly Trade Trade = new Trade();

        public static readonly Attack Attack = new Attack();
        public static readonly CastBuff CastBuff = new CastBuff();
        public static readonly CastHeal CastHeal = new CastHeal();
        public static readonly Pickup Pickup = new Pickup();
        public static readonly PetPickup PetPickup = new PetPickup();
        public static readonly PetItemTransfer PetItemTransfer = new PetItemTransfer();
        public static readonly Resurrect Resurrect = new Resurrect();
        public static readonly SwitchWeapon SwitchWeapon = new SwitchWeapon();

        public static readonly Return Return = new Return();
        public static readonly Walk Walk = new Walk();

        public static readonly List<MonsterType> monsterTypes = Enum.GetValues(typeof(MonsterType)).Cast<MonsterType>().ToList();
        public static ISpawnType CurrentTarget { get; protected set; }
        /// <summary>
        /// Use as flag to transition to CastBuff and back to BotStateMachine.PrevState.
        /// </summary>
        public static bool IsCastingSpeedBuff { get; protected set; }
        public bool IsRunning { get; protected set; }

        protected BotState(Timer innerLoop = null, Timer outerLoop = null)
        {
            InnerLoop = innerLoop ?? new Timer(RepeatInterval);
            OuterLoop = outerLoop ?? new Timer(RepeatInterval);

            InnerLoop.Elapsed += InnerLoopFunction;
            OuterLoop.Elapsed += OuterLoopFunction;
            IsRunning = false;
        }

        /// <summary>
        /// Event used to signal a transition for the FSM.
        /// </summary>
        public static event EventHandler<StateChangedArgs> StateChanged = delegate { };

        private static void OnStateChanged(StateChangedArgs s)
        {
            EventHandler<StateChangedArgs> handler = StateChanged;

            if (StateChanged != null)
            {
                handler(null, s);
            }
        }

        /// <summary>
        /// Choose to either attack or switch weapons based on current skill.
        /// Takes an action to determine the next skill from the skill list.
        /// If the current skill can be cast, the skill is cast normally.
        /// If the weapon required to cast the current skill is not yet equipped but is found in the inventory, the state switches to SwitchWeapon.
        /// If the weapon is not found, a loop commences to find the next skill with a weapon that can cast it, disabling all uncastable skills.
        /// In case no attacks are found, the loop concludes with a normal attack.
        /// </summary>
        /// <param name="getNextSkill">Method used to set next skill</param>
        /// <returns>false if skill used, else true if switched</returns>
        protected bool UseSkillOrSwitchWeapon(Action getNextSkill)
        {
            Equipment requiredItem = null;

            // loop until right weapon is equipped or required item for weapon switch is found
            do
            {
                if (CurrentSkill == null)
                {
                    Transition(GetNextState());
                    return false;
                }
                // Check if corresponding weapon is equipped
                else if (CurrentSkill.Pk2Skill.IsRequiredWeaponEquipped())
                {
                    // Send skill packet once, then send the skill on a loop, until it has been cast
                    InnerLoopFunction(null, EventArgs.Empty);
                    InnerLoop.Start();
                    return false;
                }
                else
                {
                    // Check if item is available
                    if (TryFindWeaponToSwitch(CurrentSkill.RequiredWeapon, out requiredItem))
                    {
                        //TODO: Disable skill until weapon of required type is added to inventory
                        SwitchWeapon.WeaponToEquip = requiredItem;
                        Transition(SwitchWeapon);
                        return true;
                    }
                    else
                    {
                        getNextSkill();
                    }
                }


            } while (CurrentSkill.Pk2Skill.IsRequiredWeaponEquipped() || requiredItem != null);

            return false;
        }

        /// <summary>
        /// Find a weapon from the inventory with the given type to switch, in order to use a skill.
        /// This takes the last weapon switched as the first choice then gets the weapon with the highest level (weapon level + enhancement ). 
        /// </summary>
        /// <param name="itemType"></param>
        /// <param name="weapon"></param>
        /// <returns></returns>
        protected bool TryFindWeaponToSwitch(Pk2ItemType itemType, out Equipment weapon)
        {
            weapon = null;

            foreach (KeyValuePair<byte, Item> kvp in CharInfoGlobal.Inventory)
            {
                // Choose item with matching type if weapon has not been assigned yet or the effective level is highest
                if (kvp.Value.Type == itemType
                    && (weapon == null || (kvp.Value as Equipment).EffectiveLevel > weapon.EffectiveLevel))
                {
                    weapon = (Equipment)kvp.Value;
                }
            }
            return weapon != null;
        }

        protected virtual void HandleTeleportSkill(object sender, CharMoveArgs e)
        {
            if (e.UniqueID == Bot.CharData.UniqueID && IsRunning)
            {
                OuterLoopFunction(null, EventArgs.Empty);
            }
        }

        protected virtual void HandleDeath(object sender, EventArgs e)
        {
            // Make sure buffs are reset to allow imbues to be cast after death
            Bot.CharData.ActiveBuffs.Clear();

            // Return to town if conditions are met
            if (GeneralTrainingSettings.ShouldReturnToTown())
            {
                Transition(Return);
            }
            else // wait for resurrection
            {
                Transition(Idle);
            }
        }

        /// <summary>
        /// Use this to handle switching between invincible and non-invincible states.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void HandleCharStatusChanged(object sender, ObjectStatusChangedArgs e)
        {
            if (e.PreviousStatus == ObjectStatus.Invincible
                && Bot.CharData.IsAlive
                && BotStateMachine.Instance.State == TownScript || BotStateMachine.Instance.State == WalkScript || BotStateMachine.Instance.State == Walk || BotStateMachine.Instance.State == Idle)
            {
                BotState nextState = GetNextState();
                if (nextState != BotStateMachine.Instance.State)
                {
                    Transition(nextState);
                }
            }
        }



        public static bool ShouldCastBuff()
        {
            bool anyBuffs = SkillSettings.BuffSkills[0].Count > 0;
            bool canCast = SkillSettings.BuffSkills[0].Any(b => CanBuffBeCast(b));
            bool isSpeedBuffSet = !SkillSettings.SpeedBuffOrPot && SkillSettings.SpeedBuff != null;
            bool noSpeedBuffCast = !Bot.CharData.ActiveBuffs.Any(ab => ab.Type == Pk2SkillType.SpeedBuff);

            if ((anyBuffs && canCast || isSpeedBuffSet && noSpeedBuffCast) && BotStateMachine.Instance.State == CastBuff)
            {
                throw new StateChangeException(CastBuff);
            }

            return anyBuffs && canCast || isSpeedBuffSet && noSpeedBuffCast;
        }

        protected static bool ShouldUseSpeedPot()
        {
            bool noSpeedBuffCast = !Bot.CharData.ActiveBuffs.Any(ab => ab.Type == Pk2SkillType.SpeedBuff);
            bool hasSpeedBuffItem = CharInfoGlobal.Inventory.Any(kvp => kvp.Value.Type == Pk2ItemType.SpeedPotion);

            return noSpeedBuffCast
                && hasSpeedBuffItem
                && SkillSettings.SpeedBuffOrPot;
        }

        protected static bool ShouldCastSpeedBuff()
        {
            return CurrentSkill == null
                && SkillSettings.SpeedBuffOrPot == false
                && SkillSettings.SpeedBuff != null
                && !Bot.CharData.ActiveBuffs.Any(b => b.RefSkillID == SkillSettings.SpeedBuff.RefSkillID);
        }

        private static bool ShouldWalk()
        {
            return ScriptGlobal.MAX_GO_DISTANCE > ScriptUtils.DistanceToClosestArea - ScriptUtils.ClosestAreaToChar.Radius;
        }

        /// <summary>
        /// Predicate to find next valid target
        /// </summary>
        public static readonly Predicate<Monster> IsMonsterValidTarget = m => ScriptUtils.IsPointInAnyArea(m.Position) && m.IsAlive && !m.IsIgnored;
        /// <summary>
        /// Predicate to make sure that monster is valid target in case of warlock DOT conditions.
        /// </summary>
        public static readonly Predicate<Monster> IsValidWarlockTarget = m => m != null && (!GeneralTrainingSettings.WarlockDOTSwitch
        || m.Type != MonsterType.Normal
        || (CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Primary, out Item item) && item.Type != Pk2ItemType.Darkstaff)
        || m.Buffs.Select(b => b.Pk2Entry.Type == Pk2SkillType.WarlockDOT).Count() < 2);
        /// <summary>
        /// Predicate to find next drop to pick up
        /// </summary>
        public static readonly Predicate<ItemDrop> IsDropValidTarget = i => i != null && !Bot.CharData.CharacterInventoryIsFull && i.Pk2Item.ShouldPick() && ScriptUtils.IsPointInAnyArea(i.Position)
        && (i.OwnerID == 0xFFFFFFFF && ItemFilterSettings.PickFreeItems || i.OwnerID == Bot.CharData.AccountID || i.OwnerID == 0); //  0xFFFFFFFF == *Bot.CharData.UniqueID* || i.UniqueID == 0x0  // TODO unclaimed drop for petmanager
                                                                                                                                   /// Predicate to check whether attack can be cast                                                                                                                                                                                                                                                                                                    /// </summary>
        public static readonly Predicate<Skill> CanSkillBeCast = s => s.SkillEnabled == 0x01 && CharInfoGlobal.Inventory.Any(i => s.Pk2Skill.DoesSkillMatchWeapon(i.Value.Type));
        /// <summary>
        /// Predicate to check whether skill can be cast. Either the item is equipped or it can be switched from the inventory.
        /// </summary>
        public static bool CanSkillWeaponBeEquipped(Skill s)
        {
            if (CharInfoGlobal.Inventory.TryGetValue((byte)s.RequiredWeaponSlot, out Item item) && s.Pk2Skill.DoesSkillMatchWeapon(item.Type))
            {
                return true;
            }
            else
            {
                bool primaryEquipped = CharInfoGlobal.Inventory.TryGetValue((byte)EquipmentSlot.Primary, out Item weapon);
                bool isInventoryFull = Bot.CharData.CharacterInventoryIsFull;

                // find item in inventory and find out whether the weapon has enough space to be switched in.
                // A weapon must exist and there should be enough space to switch out the equipped items
                return CharInfoGlobal.Inventory.Any(i => s.Pk2Skill.DoesSkillMatchWeapon(i.Value.Type))
                    && (!primaryEquipped || weapon.Type.Is2SlotWeapon() || !isInventoryFull);
            }
        }
        /// <summary>
        /// Predicate to check whether buff can be cast
        /// </summary>
        public static readonly Predicate<Skill> CanBuffBeCast = s =>
        s.SkillEnabled == 0x01
        && !Bot.CharData.ActiveBuffs.Any(b => b.RefSkillID == s.RefSkillID
        && CanSkillWeaponBeEquipped(s));

        public static BotState GetNextState()
        {
            // Just stopping by for some speed.
            if (IsCastingSpeedBuff)
            {
                IsCastingSpeedBuff = false;
                return BotStateMachine.Instance.PrevState as BotState;
            }

            // Check if in town or too far from training area and training script is set
            if (ScriptUtils.CurrentTown(Bot.CharData.Position) != null)
            {
                return TownScript;
            }
            else if (!string.IsNullOrEmpty(ScriptSettings.TrainingScriptPath))
            {
                return WalkScript;
            }

            // Send pet around if there is anything to pick up 
            if (Bot.PickupPet != null && !Pickup.IsRunning && SpawnListsGlobal.Drops.Any(i => IsDropValidTarget(i.Value)))
            {
                Task.Run(() =>
                {
                    PetPickup.Start();
                }).ConfigureAwait(false);
            }


            BotState nextState;

            // Only use combat/skill states after the 5 second invincibility after death/teleporting

            // TODO heal
            // TODO res
            if (GeneralTrainingSettings.ShouldReturnToTown())
            {
                nextState = Return;
            }
            else if (Bot.CharData.Status != ObjectStatus.Invincible && ShouldCastBuff())
            {
                nextState = CastBuff;
            }
            else if (Bot.PickupPet.OwnerID == 0 && SpawnListsGlobal.Drops.Any(i => IsDropValidTarget(i.Value)) && !SpawnListsGlobal.MonsterSpawns.Any(m => m.Value.Target == Bot.CharData.UniqueID))
            {
                nextState = Pickup;
            }
            else if (Bot.CharData.Status != ObjectStatus.Invincible && SpawnListsGlobal.MonsterSpawns.Any(m => IsMonsterValidTarget(m.Value) && IsValidWarlockTarget(m.Value)))
            {
                nextState = Attack;
            }

            // wait
            else if (ShouldWalk())
            {
                nextState = Walk;
            }
            else
            {
                nextState = Idle;
            }

            return nextState;
        }

        /// <summary>
        /// The loop to repeatedly send a command until the next command is found in the outer loop
        /// </summary>
        protected Timer InnerLoop;
        /// <summary>
        /// The loop to find a command, start the inner loop then find the next command once conditions are met.
        /// </summary>
        protected Timer OuterLoop;

        /// <summary>
        /// The function executed upon elapsed innerloop timer interval.
        /// </summary>
        protected abstract void InnerLoopFunction(object sender, EventArgs e);
        /// <summary>
        /// The function executed upon elapsed outerloop timer interval.
        /// </summary>
        protected abstract void OuterLoopFunction(object sender, EventArgs e);

        /// <summary>
        /// Wire up event handlers before start. 
        /// </summary>
        protected abstract void InitializeEventHandlers();
        /// <summary>
        /// Remove event handlers before end.
        /// </summary>
        protected abstract void RemoveEventHandlers();

        /// <summary>
        /// Sets up the state and starts the loop within the state.
        /// </summary>
        public void Start()
        {
            if (IsRunning)
            {
                return;
            }
            else if (Bot.IsInteracting)
            {
                NPCCloseDialogue.DialogueClosed += HandleInteractionStoppedForBotStart;
                Proxy.Instance.SendCommandAG(new NPCCloseDialogue(CurrentTarget.UniqueID));
            }
            else
            {
                // Eventhandlers for general events
                CharDeathHandler.CharDied += HandleDeath;
                Bot.CharData.CharStatusChanged += HandleCharStatusChanged;

                IsRunning = true;
                InitializeEventHandlers();
                OuterLoopFunction(null, null);
            }

        }

        protected abstract void GetNextTarget();

        /// <summary>
        /// Reset botstate superclass variables for next state.
        /// An optional next target can prime the next state.
        /// </summary>
        /// <param name="nextTarget">Target to start with in next state</param>
        private void PrepareForTransition(ISpawnType nextTarget = null)
        {
            if (!IsRunning)
            {
                return;
            }

            // Eventhandlers for general events
            CharDeathHandler.CharDied -= HandleDeath;
            Bot.CharData.CharStatusChanged -= HandleCharStatusChanged;

            IsRunning = false;
            CurrentTarget = nextTarget;
            RemoveEventHandlers();
            InnerLoop.Stop();
            OuterLoop.Stop();
        }

        /// <summary>
        /// Stops the loop within the state and proceeds to the next state; 
        /// </summary>
        protected void Transition(IState s, ISpawnType nextTarget = null)
        {
            PrepareForTransition(nextTarget);
            OnStateChanged(new StateChangedArgs(s));
        }

        /// <summary>
        /// Stops state without switching.
        /// Allows FSM to shut down.
        /// </summary>
        public void Stop()
        {
            PrepareForTransition();
        }
        private void HandleInteractionStoppedForBotStart(object sender, EventArgs e)
        {
            Console.WriteLine($"[{GetType().Name}] Stopped interaction with {CurrentTarget?.Name} to start botting");

            // Eventhandlers for general events
            CharDeathHandler.CharDied += HandleDeath;
            Bot.CharData.CharStatusChanged += HandleCharStatusChanged;

            IsRunning = true;
            InitializeEventHandlers();
            OuterLoopFunction(null, null);
        }
    }
}
