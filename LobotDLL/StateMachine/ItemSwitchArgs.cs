﻿using LobotAPI.Structs;

namespace LobotDLL.StateMachine
{
    internal class ItemSwitchArgs
    {
        public Item Primary;
        public Item Secondary;

        public ItemSwitchArgs(Item primary = null, Item secondary = null)
        {
            Primary = primary;
            Secondary = secondary;
        }
    }
}