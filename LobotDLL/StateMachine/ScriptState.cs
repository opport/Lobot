﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets.Char;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets.Inventory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace LobotDLL.StateMachine
{
    public abstract class ScriptState : BotState
    {
        protected const int RepetitionInterval = 5000;
        protected static List<AbstractExpression> Script;
        public static Town CurrentTown;
        protected int CommandIndex;
        public CommandExpression CurrentScriptCommand { get { return CommandIndex > -1 ? Script?[CommandIndex] as CommandExpression : null; } }
        /// <summary>
        /// Use flag to only add handlers for speed buff once.
        /// </summary>
        protected static bool IsSpeedBuffSetup = false;
        protected bool IsPaused = false;


        public static event EventHandler<EventArgs> StoppedScript = delegate { };

        public static event EventHandler<ScriptCommandArgs> ExecutedScriptCommand = delegate { };

        protected void Pause()
        {
            IsPaused = true;
            RemoveEventHandlers();
        }

        protected void Resume()
        {
            OuterLoopFunction(null, EventArgs.Empty);
        }

        protected static void OnExecutedScriptCommand(ScriptCommandArgs e)
        {
            EventHandler<ScriptCommandArgs> handler = ExecutedScriptCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnStoppedScript(EventArgs e)
        {
            EventHandler<EventArgs> handler = StoppedScript;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        protected void CastSpeedBuffAfterInteraction(object Sender, BoolEventArgs e)
        {
            if (!Bot.IsInteracting)
            {
                SpeedBuffApply();
            }
        }

        protected void CastSpeedBuffAfterInvincible(object Sender, ObjectStatusChangedArgs e)
        {
            if (e.CurrentStatus != ObjectStatus.Invincible
                && e.PreviousStatus == ObjectStatus.Invincible
                && !Bot.IsInteracting)// Wait until interaction stops before casting buff
            {
                SpeedBuffApply();
            }
        }

        protected void SpeedBuffApply()
        {
            if (ShouldCastSpeedBuff())
            {
                IsCastingSpeedBuff = true;
                // keep last script command to prevent restarting
                Pause();
                Transition(CastBuff);
            }
            else if (ShouldUseSpeedPot())
            {
                foreach (KeyValuePair<byte, Item> item in CharInfoGlobal.Inventory)
                {
                    if (item.Value.Type == Pk2ItemType.SpeedPotion)
                    {
                        Proxy.Instance.SendCommandAG(new UseItem(item.Key));
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// Setup handlers to cast speed buff or use speed pot
        /// </summary>
        protected void SpeedBuffSetup()
        {
            if (!IsSpeedBuffSetup)
            {
                IsSpeedBuffSetup = true;

                Bot.CharData.CharStatusChanged += CastSpeedBuffAfterInvincible;
                Bot.BotIsInteractingChanged += CastSpeedBuffAfterInteraction;
            }
            SpeedBuffApply();
        }
        /// <summary>
        /// Remove handlers for speed buff or speed pot
        /// </summary>
        protected void SpeedBuffCleanUp()
        {
            Bot.CharData.CharStatusChanged -= CastSpeedBuffAfterInvincible;
            Bot.BotIsInteractingChanged -= CastSpeedBuffAfterInteraction;
        }
        /// <summary>
        /// Reapply speed buff if canceled or timed out.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HandleSpeedBuffEnded(object sender, BuffArgs e)
        {
            if (PK2DataGlobal.Skills.TryGetValue(e.RefID, out Pk2Skill pk2Skill) && pk2Skill.Type == Pk2SkillType.SpeedBuff)
            {
                SpeedBuffApply();
            }
        }

        protected bool CanStartTraining()
        {
            // Check if out of town and close to any training area
            return CurrentTown == null
                && (ScriptGlobal.MAX_GO_DISTANCE > ScriptUtils.DistanceToTrainingArea1 || (TrainingAreaSettings.UseSecondArea && ScriptGlobal.MAX_GO_DISTANCE > ScriptUtils.DistanceToTrainingArea2));
        }

        protected bool IsIndexInRange(int index, int upperBound)
        {
            return index > -1 && index < upperBound;
        }

        private void DelayedStart()
        {
            Start();

            Logger.Log(LogLevel.SCRIPT, "Starting script");
            Console.WriteLine("Starting script");
        }

        protected bool CanTransitionToWalkScript()
        {
            List<AbstractExpression> walkScript = null;

            try
            {
                walkScript = ExpressionParser.ParseScript(ScriptSettings.TrainingScriptPath);
            }
            catch (FormatException fe)
            {
                Logger.Log(LogLevel.SCRIPT, fe.Message);
            }

            return walkScript != null;
        }

        protected bool TransitionToNextScript()
        {
            if (CanStartTraining()) // Check if close to training area
            {
                Transition(GetNextState());
                return true;
            }
            else if (CanTransitionToWalkScript())
            {
                Transition(WalkScript);
                return true;
            }
            //TODO enable quest script
            //else if (CanTransitionToQuestScript())
            //{
            //    Transition(Quest);
            //    return true;
            //}
            else
            {
                Transition(Idle);
                return false;
            }
        }

        protected void HandleSkippedCommand(object sender, EventArgs e)
        {
            CommandIndex++;
            OuterLoopFunction(null, EventArgs.Empty);
        }

        /// <summary>
        /// Use to delay any further action after the script finds "wait" or the character has teleported
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HandleExecutedCommand(object sender, DelayArgs e)
        {
            RemoveEventHandlers();
            Task.Delay(TimeSpan.FromMilliseconds(e.Delay)).ContinueWith(task => DelayedStart());
            Logger.Log(LogLevel.SCRIPT, "wait " + e.Delay);
            Console.WriteLine("Delayed next command for {0}ms", e.Delay);
        }

        protected bool IsMovementScriptCommand(CharMoveArgs e)
        {
            return CurrentScriptCommand?.CommandName == "go"
                && e?.UniqueID == Bot.CharData?.UniqueID
                && e?.Destination.X == int.Parse(CurrentScriptCommand?.Arguments[0])
                && e?.Destination.Y == int.Parse(CurrentScriptCommand?.Arguments[1]);
        }

        protected void HandleExecutedMoveCommand(object sender, CharMoveArgs e)
        {
            // Make sure it is the movement command that was sent by the script
            if (IsMovementScriptCommand(e) && !OuterLoop.Enabled)
            {
                //Take missing origin point into account
                TimeSpan delay = TimeSpan.FromSeconds(
                    (e.Origin == default(Point))
                    ? ScriptUtils.GetWalkTime(Bot.CharData.Position, e.Destination)
                    : ScriptUtils.GetWalkTime(e.Origin, e.Destination));

                System.Diagnostics.Debug.WriteLine(string.Format("DelayCalculated:{0}", delay));

                if (delay.TotalMilliseconds > 0)
                {
                    InnerLoop.Interval = RepetitionInterval;
                    OuterLoop.Interval = delay.TotalMilliseconds;

                    InnerLoop.Start();
                    OuterLoop.Start();
                }
                else
                {
                    OnExecutedScriptCommand(new ScriptCommandArgs(CurrentScriptCommand));
                }
            }
        }
    }
}
