﻿using LobotAPI.Commands;
using LobotAPI.Handlers;
using LobotDLL.Commands.Chat;
using SilkroadSecurityApi;

namespace LobotDLL.Handlers.Chat
{
    /// <summary>
    /// [S -> C][3026]
    //06                                                flag 6
    //03 00                                             name length 3
    //77 33 33                                          w33
    //55 00                                             message length 85
    //57 54 53 20 53 49 4C 4B 20 57 54 53 20 63 68 72   WTS.SILK.WTS.chr
    //6F 6E 79 63 20 74 61 6C 69 73 6D 61 6E 73 20 57   onyc.talismans.W
    //54 53 20 73 75 6E 20 64 65 66 65 6E 64 65 72 20   TS.sun.defender.
    //61 72 6D 6F 72 20 63 68 6F 75 6C 64 65 72 20 57   armor.choulder.W
    //54 42 20 70 76 70 20 66 6F 72 20 35 30 6D 20 70   TB.pvp.for.50m.p
    //6D 20 6D 65 20                                    m.me............
    /// </summary>
    public class ChatMessageHandler : Handler
    {
        public ChatMessageHandler() : base(0x3026)
        {
        }

        protected override Command ParsePacket(Packet p)
        {
            ChatType chatType = (ChatType)p.ReadUInt8();
            uint senderID;
            string senderName;

            if (chatType == ChatType.All ||
               chatType == ChatType.AllGM ||
               chatType == ChatType.NPC)
            {
                senderID = p.ReadUInt32();
            }
            else if (chatType == ChatType.PM ||
                    chatType == ChatType.Party ||
                    chatType == ChatType.Guild ||
                    chatType == ChatType.Global ||
                    chatType == ChatType.Stall ||
                    chatType == ChatType.Union ||
                    chatType == ChatType.Accademy)
            {
                senderName = p.ReadAscii();
            }

            string message = p.ReadAscii();

            return null;
        }
    }
}
