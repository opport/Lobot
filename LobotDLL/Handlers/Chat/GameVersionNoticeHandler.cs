﻿using LobotAPI.Commands;
using LobotAPI.Handlers;
using SilkroadSecurityApi;

namespace LobotDLL.Handlers.Chat
{
    // Game version notice
    //[S -> C][B010]
    //01                                                ................
    //01 00                                             ................
    //27 00                                             '...............
    //49 6E 50 61 6E 69 63 2D 53 69 6C 6B 72 6F 61 64   InPanic-Silkroad
    //20 76 53 52 4F 20 47 75 61 72 64 20 52 65 76 2E   .vSRO.Guard.Rev.
    //31 2E 30 2E 30 2E 30                              1.0.0.0.........
    public class GameVersionNoticeHandler : Handler
    {
        public GameVersionNoticeHandler() : base(0xB010)
        {
        }

        protected override Command ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                ushort noticeNumber = p.ReadUInt16();
                string message = p.ReadAscii();
            }

            return null;
        }
    }
}
