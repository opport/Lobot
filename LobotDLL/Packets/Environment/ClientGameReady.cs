﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    public class ClientGameReady : Command
    {
        public ClientGameReady() { }

        public override ushort Opcode => 0x3012;

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }
    }
}
