﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    public class ClientGuide : BidirectionalPacket
    {
        GuideFlag GuideFlag;

        public ClientGuide() { }
        public ClientGuide(GuideFlag flag) { GuideFlag = flag; }

        public override ushort Opcode => 0x70EA;

        public override ushort ServerOpcode => 0xB0EA;

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(GuideFlag);

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 0x01)
            {
                ulong guideflag = p.ReadUInt64();
            }
            else
            {
                ushort errorCode = p.ReadUInt16();
            }
            return null;
        }
    }
}
