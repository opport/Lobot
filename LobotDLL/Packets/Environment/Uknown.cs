﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    //[S -> C][3027]
    //E8 00                                             ................
    //01                                                ................
    //0A                                                ................
    class Uknown : Handler
    {
        public override ushort ServerOpcode => 0x3027;
        public Uknown() { }

        protected override ICommand ParsePacket(Packet p)
        {
            ushort unkn0 = p.ReadUInt16();
            byte unkn1 = p.ReadUInt8();
            byte unkn2 = p.ReadUInt8();

            return null;
        }
    }
}
