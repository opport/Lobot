﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    //[S -> C]
    //[34BE]
    //D3 D4 50 19                                       servertime stamp (ca. 1:20?)
    public class ServerTimeUpdateHandler : Handler
    {
        public ServerTimeUpdateHandler() { }

        public override ushort ServerOpcode => 0x34BE;

        protected override ICommand ParsePacket(Packet p)
        {
            Bot.CharData.ServerTime = p.ReadUInt32();
            return null;
        }
    }
}
