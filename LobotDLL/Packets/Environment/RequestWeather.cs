﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{

    public class RequestWeather : BidirectionalPacket
    {
        public override ushort Opcode => 0x750E;
        public override ushort ServerOpcode => 0x3809;

        public RequestWeather() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();
            if (success == 0x01)
            {
                byte brightness = p.ReadUInt8();
            }

            return null;
        }
    }
}
