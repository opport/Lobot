﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    /// <summary>
    // request new x/y sector
    //[C -> S][34B6]

    // response new x/y sector
    //[S -> C][34B5]
    //A9 61                                             xSector, ySector
    /// </summary>
    public class RequestMapArea : BidirectionalPacket
    {
        public override ushort Opcode => 0x34B5;
        public override ushort ServerOpcode => 0x34B6;

        public RequestMapArea() { }

        protected override ICommand ParsePacket(Packet p)
        {
            ushort xSector = p.ReadUInt16();
            ushort ySector = p.ReadUInt16();
            return null;
        }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }
    }
}
