﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    //[S -> C][300C]
    //1C 0C                                             notificationID
    //01                                                part 1
    //1B 00                                             notificationLength
    //55 49 49 54 5F 4D 53 47 5F 49 4E 50 41 4E 49 43   UIIT_MSG_INPANIC
    //5F 47 55 41 52 44 5F 49 4E 46 4F                  _GUARD_INFO.....

    //[S -> C][300C]
    //1C 0C                                             notificationID
    //02                                                part 2
    //1B 00                                             notificationLength
    //55 49 49 54 5F 4D 53 47 5F 49 4E 50 41 4E 49 43   UIIT_MSG_INPANIC
    //5F 47 55 41 52 44 5F 49 4E 46 4F                  _GUARD_INFO.....

    //[S -> C][300C]
    //05 0C                                             notificationID
    //28 9A 01 00                                       unknw
    class GameNotify : Handler
    {
        public override ushort ServerOpcode => 0x300C;
        public GameNotify() { }

        protected override ICommand ParsePacket(Packet p)
        {
            //p.ReadUInt16();
            //p.ReadUInt8(); // unknown - either part or id
            //p.ReadAscii();
            return null;
        }
    }
}
