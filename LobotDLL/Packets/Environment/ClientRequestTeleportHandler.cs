﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Environment
{
    //[S -> C][30D2]
    //00 00 00 00                                       ................
    public class ClientRequestTeleportHandler : Handler
    {
        public ClientRequestTeleportHandler() { }

        public override ushort ServerOpcode => 0x30D2;

        protected override ICommand ParsePacket(Packet p)
        {
            uint payload = p.ReadUInt32();
            return null;
        }
    }
}
