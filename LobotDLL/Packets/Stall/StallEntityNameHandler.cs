﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Stall
{
    public class StallEntityNameHandler : Handler
    {
        public static event EventHandler StallUpdatedName = delegate { };

        private static void OnStallUpdatedName()
        {
            EventHandler handler = StallUpdatedName;
            if (handler != null)
            {
                handler(null, EventArgs.Empty);
            }
        }

        public override ushort ServerOpcode => 0x30BB;

        public StallEntityNameHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();
            if (Bot.CharData.UniqueID == uniqueID)
            {
                StallSettings.StallName = p.ReadAscii();
                OnStallUpdatedName();
            }

            return null;
        }
    }
}
