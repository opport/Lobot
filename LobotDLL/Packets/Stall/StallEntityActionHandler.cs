﻿using LobotAPI.Packets;
using LobotAPI.Packets.Stall;
using LobotDLL.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallEntityActionHandler : StallHandler
    {
        public override ushort ServerOpcode => 0x30B7;

        public override ushort Opcode => throw new System.NotImplementedException();

        public StallEntityActionHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            StallAction stallAction = (StallAction)p.ReadUInt8();
            if (stallAction == StallAction.Leave || stallAction == StallAction.Enter)
            {
                uint UniqueID = p.ReadUInt32();
            }
            else if (stallAction == StallAction.Buy)
            {
                byte Slot = p.ReadUInt8();   //within Stall (0-9)
                string Name = p.ReadAscii();

                StallHandler.ParseItems(p);
            }
            return null;
        }

        protected override Packet AddPayload()
        {
            throw new System.NotImplementedException();
        }
    }
}
