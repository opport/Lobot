﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Stall
{
    public class StallCreate : BidirectionalPacket
    {
        public static event EventHandler StallCreated = delegate { };

        private static void OnStallCreated()
        {
            EventHandler handler = StallCreated;
            if (handler != null)
            {
                handler(null, EventArgs.Empty);
            }
        }

        public override ushort Opcode => 0x70B1;
        public override ushort ServerOpcode => 0xB0B1;

        public StallCreate() { }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);

            string stallName = string.IsNullOrEmpty(StallSettings.StallName) ? StallSettings.DefaultName : StallSettings.StallName;
            p.WriteAscii(stallName);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 2)
            {
                ushort error = p.ReadUInt16();
            }
            else
            {
                Bot.CharData.InteractionMode = 0x04;
                OnStallCreated();
            }
            return null;
        }
    }
}
