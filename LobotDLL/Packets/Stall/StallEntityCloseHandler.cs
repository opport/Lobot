﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallEntityCloseHandler : Handler
    {
        public override ushort ServerOpcode => 0x30B9;

        public StallEntityCloseHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint unique = p.ReadUInt32();
            ushort errorCode = p.ReadUInt16();

            return null;
        }
    }
}
