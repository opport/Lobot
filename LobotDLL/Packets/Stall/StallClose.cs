﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Stall
{
    public class StallClose : BidirectionalPacket
    {
        public override ushort Opcode => 0x70B2;
        public override ushort ServerOpcode => 0xB0B2;

        public static event EventHandler StallClosed = delegate { };

        private static void OnStallClosed()
        {
            EventHandler handler = StallClosed;
            if (handler != null)
            {
                handler(null, EventArgs.Empty);
            }
        }

        public StallClose() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            if (result == 2)
            {
                ushort errorCode = p.ReadUInt16();
            }
            else
            {
                Bot.CharData.InteractionMode = 0x00;
                StallSettings.StallItems.Clear();
                OnStallClosed();
            }
            return null;
        }
    }
}
