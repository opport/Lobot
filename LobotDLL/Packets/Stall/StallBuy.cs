﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallBuy : BidirectionalPacket
    {
        private byte Slot;

        public override ushort Opcode => 0x70B4;
        public override ushort ServerOpcode => 0xB0B4;

        public StallBuy() { }

        public StallBuy(byte slot) { Slot = slot; }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8(Slot);

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 0x01)
            {
                byte slot = p.ReadUInt8();
            }
            else
            {
                ushort errorCode = p.ReadUInt16();
            }

            return null;
        }
    }
}
