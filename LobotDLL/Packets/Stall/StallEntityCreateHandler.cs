﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallEntityCreateHandler : Handler
    {
        public override ushort ServerOpcode => 0x30B8;

        public StallEntityCreateHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();
            string stallName = p.ReadAscii();
            uint stallStaticAvatar = p.ReadUInt32();

            return null;
        }
    }
}
