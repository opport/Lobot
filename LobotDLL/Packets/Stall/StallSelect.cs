﻿using LobotAPI.Packets;
using LobotAPI.Packets.Stall;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallSelect : StallHandler
    {
        private readonly uint UniqueID;

        public override ushort Opcode => 0x70B3;
        public override ushort ServerOpcode => 0xB0B3;

        public StallSelect() { }

        public StallSelect(uint uniqueID)
        {
            UniqueID = uniqueID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);

            p.WriteUInt32(UniqueID);

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            if (result == 1)
            {
                uint uniqueID = p.ReadUInt32();
                string message = p.ReadAscii();
                bool isOpen = p.ReadUInt8() == 0x01;
                byte fleaMarketMode = p.ReadUInt8();

                StallHandler.ParseItems(p);
            }
            else
            {
                ushort errorCode = p.ReadUInt8();
            }

            return null;
        }
    }
}
