﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Stall
{
    public class StallLeave : BidirectionalPacket
    {
        public override ushort Opcode => 0x70B5;
        public override ushort ServerOpcode => 0xB0B5;

        public StallLeave() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 0x02)
            {
                ushort errorCode = p.ReadUInt16();
            }
            return null;
        }
    }
}
