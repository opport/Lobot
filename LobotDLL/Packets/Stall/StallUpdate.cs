﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.Packets.Stall;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Stall
{
    public class StallUpdate : StallHandler
    {
        private StallUpdateType Type;
        private string StallName;
        private string StallMessage;
        private byte Slot;
        private byte SourceSlot;
        private ushort StackCount;
        private ulong Price;
        private ushort UnkUShort0;
        private byte FleaMarketMode;
        private uint FleaMarketNetworkTidGroup;
        private bool IsOpen;
        private ushort StallNetworkResult;

        public static event EventHandler<StallUpdateArgs> StallUpdated = delegate { };

        private static void OnStallUpdated(object sender, StallUpdateArgs e)
        {
            EventHandler<StallUpdateArgs> handler = StallUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort Opcode => 0x70BA;
        public override ushort ServerOpcode => 0xB0BA;

        public StallUpdate() { }

        public static StallUpdate UpdateItem(byte slot, ushort stackCount, ulong price, ushort unkUShort0 = 0)
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.UpdateItem,
                Slot = slot,
                StackCount = stackCount,
                Price = price,
                UnkUShort0 = unkUShort0
            };
            return su;
        }

        public static StallUpdate AddItem(byte slot, byte sourceSlot, ushort stackCount, ulong price, uint fleaMarketNetworkTidGroup, ushort unkUShort0 = 0)
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.AddItem,
                Slot = slot,
                SourceSlot = sourceSlot,
                StackCount = stackCount,
                Price = price,
                FleaMarketNetworkTidGroup = fleaMarketNetworkTidGroup,
                UnkUShort0 = unkUShort0
            };
            return su;
        }

        public static StallUpdate RemoveItem(byte slot, ushort unkUShort0 = 0)
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.RemoveItem,
                Slot = slot,
                UnkUShort0 = unkUShort0
            };
            return su;
        }

        public static StallUpdate FleamarketMode(byte fleaMarketMode)
        {
            StallUpdate su = new StallUpdate
            {
                FleaMarketMode = fleaMarketMode
            };
            return su;
        }

        public static StallUpdate StallUpdateOpenState(bool IsOpen)
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.State,
                IsOpen = IsOpen
            };
            return su;
        }


        public static StallUpdate UpdateMessage()
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.Message,
                StallMessage = string.IsNullOrEmpty(StallSettings.StallMessage) ? StallSettings.DefaultMessage : StallSettings.StallMessage
            };
            return su;
        }

        public static StallUpdate UpdateName()
        {
            StallUpdate su = new StallUpdate
            {
                Type = StallUpdateType.Name,
                StallName = string.IsNullOrEmpty(StallSettings.StallName) ? StallSettings.DefaultName : StallSettings.StallName
            };
            return su;
        }

        /// <summary>
        /// Create stall update to open 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isOpen"></param>
        /// <param name="stallNetworkResult"></param>
        public StallUpdate(StallUpdateType type, bool isOpen, ushort stallNetworkResult)
        {
            this.Type = (StallUpdateType)Convert.ToByte(type);
            IsOpen = isOpen;
            StallNetworkResult = stallNetworkResult;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8((byte)Type);

            switch (Type)
            {
                case StallUpdateType.UpdateItem:
                    p.WriteUInt8(Slot);    //within Stall (0-9)
                    p.WriteUInt8(SourceSlot);
                    p.WriteUInt64(Price);
                    p.WriteUInt8(UnkUShort0);
                    break;
                case StallUpdateType.AddItem:
                    p.WriteUInt8(Slot);    //within Stall (0-9)
                    p.WriteUInt8(SourceSlot); //from ownerInventory
                    p.WriteUInt16(StackCount);
                    p.WriteUInt64(Price);
                    p.WriteUInt32(FleaMarketNetworkTidGroup);
                    p.WriteInt16(UnkUShort0);
                    break;
                case StallUpdateType.RemoveItem:
                    p.WriteUInt8(Slot);    //within Stall (0-9)
                    p.WriteUInt16(UnkUShort0);
                    break;
                case StallUpdateType.FleaMarketMode:
                    p.WriteUInt8(FleaMarketNetworkTidGroup);
                    break;
                case StallUpdateType.State:
                    p.WriteUInt8(IsOpen);
                    p.WriteUInt16(StallNetworkResult);
                    break;
                case StallUpdateType.Message:
                    p.WriteAscii(StallSettings.StallMessage);
                    break;
                case StallUpdateType.Name:
                    p.WriteAscii(StallSettings.StallName);
                    break;
            }

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            StallUpdateType updateType = (StallUpdateType)p.ReadUInt8();
            if (updateType == StallUpdateType.UpdateItem)
            {
                byte Slot = p.ReadUInt8();    //within Stall (0-9)
                ushort StackCount = p.ReadUInt16();
                ulong Price = p.ReadUInt64();
                ushort errorCode = p.ReadUInt16();
            }
            else if (updateType == StallUpdateType.AddItem || updateType == StallUpdateType.RemoveItem)
            {
                ushort errorCode = p.ReadUInt16();
                StallHandler.ParseItems(p);
            }
            else if (updateType == StallUpdateType.FleaMarketMode)
            {
                byte fleaMarketMode = p.ReadUInt8();  //no noticable effects 1 and 2 responded with success, everything > 3 with errorCode 0x3C2B
            }
            else if (updateType == StallUpdateType.State)
            {
                StallSettings.IsOpen = p.ReadUInt8() == 0x01;
                ushort stallNetworkResult = p.ReadUInt16(); //01 00 = Registering of stall items at stall network is successful
            }
            else if (updateType == StallUpdateType.Message)
            {
                StallSettings.StallMessage = p.ReadAscii();
            }
            else if (updateType == StallUpdateType.Name)
            {
                //via 0x30BB - SERVER_AGENT_ENTITY_UPDATE_STALL_NAME
            }

            OnStallUpdated(null, new StallUpdateArgs(updateType));

            return null;
        }
    }
}
