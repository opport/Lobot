﻿using LobotAPI;

using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LobotDLL.Packets
{
    /// <summary>
    /// The class in charge of handling packets.
    /// Packets to be handled are enqueued then processed by a separate task. This task accesses a dictionary of handling functions which were collected from all available packet handlers using reflection.
    /// </summary>
    public sealed class Handling
    {
        /// <summary>
        /// The dictionary of all currently implemented handling functions.
        /// </summary>
        public static readonly Dictionary<ushort, Func<Packet, ICommand>> HandlingFunctions;
        /// <summary>
        /// The worker task for processing packets.
        /// </summary>
        private static Task WorkerTask = Task.Run(() => { });

        static Handling()
        {
            HandlingFunctions = new Dictionary<ushort, Func<Packet, ICommand>>();
            // make sure type is not abstract, interface, or static (included in IsAbstract check (-> abstract == !sealed && !static))
            IEnumerable<Type> types = Assembly.GetExecutingAssembly().GetTypes().Where(t => typeof(IHandler).IsAssignableFrom(t) && !t.IsAbstract && !t.IsInterface);
            foreach (Type t in types)
            {
                try
                {
                    IHandler h = (IHandler)Activator.CreateInstance(t);
                    HandlingFunctions.Add(h.ServerOpcode, new Func<Packet, ICommand>(h.Handle));
                }
                catch (System.MissingMethodException me)
                {
                    System.Diagnostics.Debug.WriteLine($"{t.FullName} missing parameterless constructor to be added to the list of handlers.\n {me.Message}");
                    Logger.Log(LogLevel.HANDLER, $"{t.FullName} missing parameterless constructor to be added to the list of handlers.\n {me.Message}");
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine($"{e.Message} - {t.ToString()}");
                    Logger.Log(LogLevel.HANDLER, $"{e.Message} - {t.ToString()}");
                    throw;
                }
            }
        }

        private static bool IsHandshakePacket(ushort opcode)
        {
            return (opcode == 0x5000 || opcode == 0x9000 || opcode == 0x2001);
        }

        //Packet Server -> Client 
        public static ICommand HandlePacket(Packet p)
        {
            try
            {
                if (!IsHandshakePacket(p.Opcode))
                {
                    p.Lock();

                    if (HandlingFunctions.TryGetValue(p.Opcode, out Func<Packet, ICommand> handleLogic))
                    {
                        //Logger.Log(LogLevel.HANDLER, "Received packet " + p.Opcode.ToString("X2") + " - " + String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2"))));
                        //Console.WriteLine("Received packet " + p.Opcode.ToString("X2"));
                        WorkerTask = WorkerTask.ContinueWith((t) =>
                        {
                            handleLogic(p);
                        });
                        return null;
                    }
                }
                return null;
            }
            catch (System.Exception ex)
            {
                Logger.Log(LogLevel.ERROR, string.Format("{0:x} - {1}\n{2}", p.Opcode, ex.Message, ex.StackTrace));
                Logger.Log(LogLevel.HANDLER, ex.Message + "\n" + ex.StackTrace);
                Console.WriteLine(ex.ToString());
                return null;
            }
        }

    }
}
