﻿using LobotAPI;
using LobotAPI.Globals;

using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.NPC
{
    //[S -> C][30BF]
    //70 62 BE 01                                       uniqueID
    //08                                                type (8 = combat)
    //01                                                inCombat flag (1 = yes)

    //[S -> C][30BF]
    //65 67 01 00                                       uniqueID
    //00                                                type (0 = dead)
    //02                                                deathFlag (2 = dead)

    //[S -> C][30BF]
    //10 4C 97 00                                       uniqueID
    //01                                                type (1 = speed)
    //02                                                speedType 2 = walk

    //[S -> C][30BF]
    //10 4C 97 00                                       uniqueID
    //01                                                type (1 = speed)
    //03                                                speedType 3 = run

    //[S -> C][30BF]
    //70 62 BE 01                                       uniqueID
    //08                                                type (8 = combat)
    //00                                                inCombat flag (0 = no)

    // Invincible after teleport
    //[S -> C][30BF]
    //CF 3D A7 00                                       uniqueID
    //04                                                type 4 = charStatus
    //02                                                status = invincible

    // Normal, after invincible or zerk times out
    //[S -> C][30BF]
    //CF 3D A7 00                                       
    //04                                                ................
    //00                                                ................
    public class ObjectStatusHandler : Handler
    {
        public static event EventHandler<DespawnArgs> DeathRegistered = delegate { };
        public static event EventHandler ExitedCombat = delegate { };

        private static void OnDeathRegistered(DespawnArgs e)
        {
            EventHandler<DespawnArgs> handler = DeathRegistered;
            if (handler != null)
            {
                handler(null, e);
            }
        }
        private static void OnExitedCombat(EventArgs e)
        {
            EventHandler handler = ExitedCombat;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0x30BF;

        public ObjectStatusHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            try
            {
                uint uniqueID = p.ReadUInt32();
                byte type = p.ReadUInt8();
                byte status = p.ReadUInt8();

                switch (type)
                {
                    case 0:
                        if (status == 0x02) // DEAD
                        {
                            if (SpawnListsGlobal.TryGetSpawnObject(uniqueID, out ISpawnType spawn) && spawn is ICombatType)
                            {
                                (spawn as ICombatType).IsAlive = false;
                            }

                            if (SpawnListsGlobal.AllSpawnEntries.TryGetValue(uniqueID, out SpawnListsGlobal.BotLists list))
                            {
                                OnDeathRegistered(new DespawnArgs(uniqueID, list));
                            }
                        }
                        else if (status == 0x01) // ALIVE
                        {
                            // TODO might be relevant for resurrection
                        }

                        break;
                    case 1: // Speed
                        if (uniqueID == Bot.CharData.UniqueID)
                        {
                            Bot.CharData.MotionState = status;
                            if (status == 2 || status == 3)
                            {
                                byte movementType = (byte)(status - 2); // 2 = walk, 3 = run
                                Bot.CharData.MovementType = movementType;  // Switch between walking and running
                            }
                        }
                        break;
                    case 4:

                        if (Bot.CharData.UniqueID == uniqueID)
                        {
                            Bot.CharData.Status = (ObjectStatus)status;
                        }
                        else if (status == 0x07 /* && Main.DetectInvisible*/)
                        {
                            if (SpawnListsGlobal.TryGetSpawnObject(uniqueID, out ISpawnType spawn))
                            {
                                Logger.Log(LogLevel.INFORMATION, $"Invisible Spawn detected: {spawn.Name}");
                            }
                            else
                            {
                                Logger.Log(LogLevel.INFORMATION, "Invisible Spawn detected out of range");
                            }
                        }

                        break;

                    case 7:
                        // PINK / RED NAME

                        if (Bot.CharData.UniqueID == uniqueID && type == 0x07)
                        {
                            switch (status)
                            {
                                case 0x00: // NORMAL
                                    break;

                                case 0x01: // PINK
                                    Logger.Log(LogLevel.WARNING, "Pink name detected!");
                                    break;

                                case 0x02: // RED
                                    Logger.Log(LogLevel.BOTTING, "RED NAME DETECTED!");
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case 8: // exit combat mode
                        Bot.CharData.InCombat = status;
                        OnExitedCombat(EventArgs.Empty);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine($"Objectstatus error: {p.Opcode:x} \n{e.Message}");
                Logger.Log(LogLevel.NETWORK, string.Format("Objectstatus error {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
            }

            return null;
        }
    }
}
