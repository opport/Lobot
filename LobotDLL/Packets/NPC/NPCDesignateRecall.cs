﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    //[C -> S][7059]
    //0D 00 00 00                                       ................

    //[S -> C][B059]
    //01                                                ................
    public class NPCDesignateRecall : BidirectionalPacket
    {
        uint NPCID;

        public override ushort Opcode => 0x7059;
        public override ushort ServerOpcode => 0xB059;

        public NPCDesignateRecall() { }

        NPCDesignateRecall(uint npcID)
        {
            NPCID = npcID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            return null;
        }
    }
}
