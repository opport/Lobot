﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    //start interaction
    //[C -> S][7046]
    //9E 01 00 00                                       npcID
    //01                                                interactionType 1 buySell
    //// Start interaction confirm
    //[S -> C][B046]
    //01                                                successflag 1
    //01                                                interactionType 1 buySell

    //[C -> S][704B]
    //02 01 00 00                                       toggle open/close deal with npc

    //[C->S][7046]
    //02 01 00 00                                       Menu Choice
    //01                                                1 (buy)

    //[S->C][B04B]
    //01                                                successFlag
    class NPCCloseDialogue : BidirectionalPacket
    {
        uint NPCID;

        public static event EventHandler DialogueClosed = delegate { };

        public override ushort Opcode => 0x704B;
        public override ushort ServerOpcode => 0xB04B;

        public NPCCloseDialogue() { }

        public NPCCloseDialogue(uint npcID)
        {
            NPCID = npcID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte successFlag = p.ReadUInt8();
            switch (successFlag)
            {
                case 0x01: // success
                    break;
                default:
                    break;
            }

            Bot.IsInteracting = successFlag != 0x01;

            OnDialogueClosed(new BoolEventArgs(Bot.IsInteracting));
            return null;
        }

        protected void OnDialogueClosed(BoolEventArgs e)
        {
            EventHandler handler = DialogueClosed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
