﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    //[C -> S][7045]
    //9E 01 00 00                                       npcID

    //[S -> C][B045]
    //01                                                successFlag
    //9E 01 00 00                                       npcID
    //00                                                ................
    //02                                                ................
    //01                                                ................
    //02                                                ................
    //00                                                ................
    public class NPCSelect : BidirectionalPacket
    {
        uint NPCID;

        public override ushort Opcode => 0x7045;
        public override ushort ServerOpcode => 0xB045;

        public NPCSelect() { }

        public NPCSelect(uint npcID)
        {
            NPCID = npcID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt32(NPCID);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                uint uniqueID = p.ReadUInt32();
                if (SpawnListsGlobal.TryGetSpawnObject(uniqueID, out ISpawnType spawn))
                {
                    Bot.SelectedNPC = spawn;
                }
                else
                {
                    Bot.SelectedNPC = new Monster();
                }
            }
            else
            {
                // Error
            }
            return null;
        }
    }
}
