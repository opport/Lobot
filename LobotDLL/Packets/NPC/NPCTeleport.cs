﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    //teleport from jangan to donwang
    //[C -> S][705A]
    //0D 00 00 00                                       teleporterID
    //02                                                action 2 teleport
    //02 00 00 00                                       option // 2 = donwang?

    //[S -> C][B05A]
    //02                                                option 2 teleport
    //01 00                                             unkn

    //[S -> C][B05A]
    //01                                                successFlag?

    //teleport from donwang to jangan
    //[C -> S][705A]
    //11 00 00 00                                       teleporterID
    //02                                                action 2 teleport
    //01 00 00 00                                       option // 1 = jangan?

    //[S -> C][B05A]
    //02                                                option 2 teleport
    //01 00                                             unkn

    //[S -> C][B05A]
    //01                                                successFlag?

    // teleport from jangan to hotan
    //[C -> S][705A]
    //0D 00 00 00                                       teleporterID
    //02                                                action 2 teleport
    //05 00 00 00                                       option // 5 = hotan ?

    //[S -> C][B05A]
    //02                                                action 2 teleport.
    //01 00                                             ................

    //[S -> C][B05A]
    //01                                                ................
    public class NPCTeleport : BidirectionalPacket
    {
        uint NPCID;
        uint TeleportOption;

        public static event EventHandler TeleportUsed = delegate { };
        public static event EventHandler TeleportError = delegate { };

        public override ushort Opcode => 0x705A;
        public override ushort ServerOpcode => 0xB05A;


        protected void OnTeleportUsed(EventArgs e)
        {
            EventHandler handler = TeleportUsed;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected void OnTeleportError(EventArgs e)
        {
            EventHandler handler = TeleportError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public NPCTeleport() { }
        public NPCTeleport(uint npcID, uint teleportOption)
        {
            NPCID = npcID;
            TeleportOption = teleportOption;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);
            p.WriteUInt8(0x02);
            p.WriteUInt32(TeleportOption);
            return p;
        }

        //[C -> S] [705A]
        //47 00 00 00                                       unique npc id ?
        //05                                                teleport type ?
        //02                                                option move to last recall point

        //[S -> C] [B05A]
        //02                                                ................
        //01 00                                             01 teleport request received

        //[S -> C] [B05A]
        //02                                                ................
        //20 1C                                             error: Cannot because lvl over 20 -- UIIT_MSG_NEWBIE_RETURN_LV20_OVER

        //[C->S][705A]
        //47 00 00 00                                       G...............
        //02                                                ................
        //2B 00 00 00                                       +...............

        //[S -> C] [B05A]
        //02                                                ................
        //01 00                                             ................

        //[S -> C] [B05A]
        //01                                                ................

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                Bot.Status = BotStatus.Loading;
                OnHandled(EventArgs.Empty);
            }
            else if (success == 0x02) // various codes
            {
                ushort code = p.ReadUInt16();

                if (code == 0x01)
                {
                    // success --> teleport request received
                }
                else
                {
                    OnTeleportError(EventArgs.Empty);
                    // error message --  UIIT_MSG_NEWBIE_RETURN_LV20_OVER
                }
            }

            return null;
        }
    }
}
