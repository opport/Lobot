﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    // Send repair command
    //[C -> S][703E]
    //2E 02 00 00                                       ................
    //02                                                ................

    // Receive response
    //[S -> C][B03E]
    //01                                                success
    public class NPCRepair : BidirectionalPacket
    {
        const byte Repair = 0x02;
        uint NPCID;

        public override ushort Opcode => 0x703E;
        public override ushort ServerOpcode => 0xB03E;

        public static event EventHandler ItemsRepaired = delegate { };

        private static void OnItemsRepaired(EventArgs e)
        {
            EventHandler handler = ItemsRepaired;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public NPCRepair() { }

        public NPCRepair(uint npcId)
        {
            NPCID = npcId;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);
            p.WriteUInt8(Repair);

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();
            OnItemsRepaired(EventArgs.Empty);
            return null;
        }
    }
}
