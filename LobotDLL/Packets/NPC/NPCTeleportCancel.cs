﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.NPC
{
    public class NPCTeleportCancel : BidirectionalPacket
    {
        public static event EventHandler TeleportCancelled = delegate { };

        public override ushort Opcode => 0x705B;
        public override ushort ServerOpcode => 0xB05B;

        public NPCTeleportCancel() { }

        protected override ICommand ParsePacket(Packet p)
        {
            OnTeleportCancelled(EventArgs.Empty);
            return null;
        }

        protected void OnTeleportCancelled(EventArgs e)
        {
            EventHandler handler = TeleportCancelled;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected override Packet AddPayload()
        {
            OnTeleportCancelled(EventArgs.Empty);
            return new Packet(Opcode);
        }
    }
}
