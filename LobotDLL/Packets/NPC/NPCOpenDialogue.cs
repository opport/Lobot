﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Packets.NPC;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.NPC
{
    //[C -> S][704B]
    //02 01 00 00                                       toggle open/close deal with npc

    //[C->S][7046]
    //02 01 00 00                                       Menu Choice
    //01                                                1 (buy)

    //[S->C][B04B]
    //01                                                successFlag

    //[S->C][B046]
    //01                                                successFlag1
    //01                                                successFlag2
    public class NPCOpenDialogue : BidirectionalPacket
    {
        uint NPCID;
        NPCInteractionType InteractionOption;

        public static event EventHandler InteractionStarted = delegate { };

        public override ushort Opcode => 0x7046;
        public override ushort ServerOpcode => 0xB046;

        public NPCOpenDialogue() { }

        public NPCOpenDialogue(uint npcID, NPCInteractionType interactionOption)
        {
            NPCID = npcID;
            InteractionOption = interactionOption;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);
            p.WriteUInt8(InteractionOption);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();
            NPCInteractionType type = (NPCInteractionType)p.ReadUInt8();

            switch (type) // todo
            {
                case NPCInteractionType.BuySell:
                    break;
                case NPCInteractionType.Talk:
                    break;
                case NPCInteractionType.Store:
                    break;
                case NPCInteractionType.PurchaseTradeGoods:
                    break;
                case NPCInteractionType.ParticipateInMagicPop:
                    break;
                case NPCInteractionType.GrantMagicOption:
                    break;
                default:
                    break;
            }

            Bot.IsInteracting = success == 0x01;

            OnInteractionStarted(EventArgs.Empty);
            return null;
        }

        protected void OnInteractionStarted(EventArgs e)
        {
            EventHandler handler = InteractionStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
