﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{

    //[C -> S][703C]
    //1B 03 00 00                                       ................
    //00                                                ................
    public class NPCOpenStorage : BidirectionalPacket
    {
        uint NPCID;

        public override ushort Opcode => 0x703C;
        public override ushort ServerOpcode => 0xB03C;

        public NPCOpenStorage() { }

        public NPCOpenStorage(uint uniqueID)
        {
            this.NPCID = uniqueID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(NPCID);
            p.WriteUInt8(0);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            return null;
        }
    }
}
