﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using LobotDLL.Packets.Party;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    /// <summary>
    //Create party
    //hunting
    //level 5-9
    //exp autoshare item autoshare


    //[C->S][7069]
    //00 00 00 00                                       unknown0
    //00 00 00 00                                       unknown1
    //07                                                partyType 
    //00                                                purpose hunting
    //05                                                levelMin 5
    //09                                                levelMax 9
    //1B 00                                             partyNameLength
    //57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
    //69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....
    /// </summary>
    public class PartyCreate : BidirectionalPacket
    {
        PartyEntry Party;

        public override ushort Opcode => 0x7069;
        public override ushort ServerOpcode => 0xB069;

        public PartyCreate() { }

        public PartyCreate(PartyEntry party)
        {
            Party = party;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt32(0x00); // unknown0
            p.WriteUInt32(0x00); // unknown1
            p.WriteUInt8(Party.Type);
            p.WriteUInt8(Party.Objective);
            p.WriteUInt8(Party.MinLevel);
            p.WriteUInt8(Party.MaxLevel);
            p.WriteAscii(Party.Title);
            return p;
        }

        //Create party
        //hunting
        //level 5-9
        //exp autoshare item autoshare


        //[C->S][7069]
        //00 00 00 00                                       unknown0
        //00 00 00 00                                       unknown1
        //07                                                partyType 
        //00                                                objective hunting
        //05                                                levelMin 5
        //09                                                levelMax 9
        //1B 00                                             partyNameLength
        //57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
        //69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....

        //[S->C][B069]
        //01                                                unknownFlag success?
        //53 04 00 00                                       partyNumber 1107
        //00 00 00 00                                       leaderID 0 == self
        //07                                                partyType
        //00                                                objective hunting
        //05                                                levelMin 5
        //09                                                levelMax 9
        //1B 00                                             partyNameLength
        //57 65 6C 63 6F 6D 65 20 74 6F 20 49 6E 50 61 6E   Welcome.to.InPan
        //69 63 2D 53 69 6C 6B 72 6F 61 64                  ic-Silkroad.....
        protected override ICommand ParsePacket(Packet p)
        {
            // Refresh party matching list
            byte success = p.ReadUInt8(); // successFlag

            if (success == 0x01)
            {
                Bot.PartyCurrent.PartyNumber = p.ReadUInt32();
                Bot.PartyCurrent.LeaderID = p.ReadUInt32();
                Bot.PartyCurrent.Type = (PartyType)p.ReadUInt8();
                Bot.PartyCurrent.Objective = (PartyObjective)p.ReadUInt8();
                Bot.PartyCurrent.MinLevel = p.ReadUInt8();
                Bot.PartyCurrent.MaxLevel = p.ReadUInt8();
                Bot.PartyCurrent.Title = p.ReadAscii();

                return new PartyMatchRefresh();
            }
            else
            {
                return null;
            }
        }
    }
}
