﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    //Quest
    //level 5-9
    //exp autoshare item autoshare

    //[C->S][706A]
    //53 04 00 00                                       partyNumber 1107
    //00 00 00 00                                       partyLeaderID 0 = self
    //07                                                partyType
    //01                                                partyObjective
    //05                                                levelMin 5
    //09                                                levelMax 9
    //29 00                                             partyNameLength
    //57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
    //65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
    //53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......
    /// </summary>
    public class PartyModify : BidirectionalPacket
    {
        PartyEntry Party;

        public override ushort Opcode => 0x706A;
        public override ushort ServerOpcode => 0xB06A;

        public PartyModify() { }

        public PartyModify(PartyEntry party)
        {
            Party = party;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt32(Party.PartyNumber);
            p.WriteUInt32(0);
            p.WriteUInt8(Party.Type);
            p.WriteUInt8(Party.Objective);
            p.WriteUInt8(Party.MinLevel);
            p.WriteUInt8(Party.MaxLevel);
            p.WriteUInt16(Party.Title.Length);
            p.WriteAscii(Party.Title);
            return p;
        }

        //Update party
        //Quest
        //level 5-9
        //exp autoshare item autoshare

        //[C->S][706A]
        //53 04 00 00                                       partyNumber 1107
        //00 00 00 00                                       unknown0
        //07                                                partyType
        //01                                                partyObjective
        //05                                                levelMin 5
        //09                                                levelMax 9
        //29 00                                             partyNameLength
        //57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
        //65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
        //53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......

        //[S -> C] [B06A]
        //01                                                successFlag?
        //53 04 00 00                                       partyNumber
        //00 00 00 00                                       unknown
        //07                                                partyType
        //01                   				                objective
        //05                                                levelMin
        //09                                                levelMax
        //29 00                                             partyNameLength
        //57 68 65 6E 20 77 69 6C 6C 20 74 68 65 20 71 75   When.will.the.qu
        //65 73 74 73 20 65 6E 64 20 6F 6E 20 74 68 65 20   ests.end.on.the.
        //53 69 6C 6B 72 6F 61 64 3F                        Silkroad?.......
        protected override ICommand ParsePacket(Packet p)
        {
            p.ReadUInt8(); // successFlag

            Bot.PartyCurrent.PartyNumber = p.ReadUInt32();
            Bot.PartyCurrent.LeaderID = p.ReadUInt32();
            Bot.PartyCurrent.Type = (PartyType)p.ReadUInt8();
            Bot.PartyCurrent.Objective = (PartyObjective)p.ReadUInt8();
            Bot.PartyCurrent.MinLevel = p.ReadUInt8();
            Bot.PartyCurrent.MaxLevel = p.ReadUInt8();
            Bot.PartyCurrent.Title = p.ReadAscii();

            //CharInfoGlobal.PartyMatchingList.RemoveAll(entry => entry.PartyNumber == party.PartyNumber);
            //CharInfoGlobal.PartyMatchingList.Add(party);
            return null;
        }
    }
}
