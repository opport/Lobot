﻿using LobotAPI.Packets;
using LobotAPI.Packets.Party;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    //[S -> C][706D]
    //29 00 00 00                                       partyNumber
    //4E 51 00 00                                       partyMemberID
    //74 04 00 00                                       unknown
    //00 00 00 00                                       unknown
    //00 00 00 00                                       unknown
    //04                                                unknown
    //FF                                                unknown
    //4E 51 00 00                                       partyID
    //09 00                                             nameLength
    //4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
    //84 07 00 00                                       player avatar
    //01                                                level
    //AA                                                unknown
    //A9 62                                             xSector ySector
    //A5 05                                             x
    //00 00                                             z
    //24 02                                             y
    //01 00 01 00                                       unknown
    //00 00                                             guildname length
    //04                                                unknown
    //00 00 00 00                                       mastery0
    //00 00 00 00                                       mastery1

    //[C->S][306E]
    //29 00 00 00                                       )...............
    //4E 51 00 00                                       NQ..............
    //01                                                ................

    //[S -> C] [B060]
    //01                                                ................
    //2E 51 00 00                                       .Q..............
    /// </summary>
    public class PartyMatchJoinHandler : Handler
    {
        public static event EventHandler<PartyJoinArgs> JoinRequest = delegate { };

        public override ushort ServerOpcode => 0x706D;

        public PartyMatchJoinHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint partyNumber = p.ReadUInt32();
            uint memberID = p.ReadUInt32();
            OnJoinRequest(new PartyJoinArgs(partyNumber, memberID));
            return null;
        }

        private void OnJoinRequest(PartyJoinArgs e)
        {
            EventHandler<PartyJoinArgs> handler = JoinRequest;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
