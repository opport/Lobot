﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    /// Request an updated party match list from the server.
    /// Receives response 0xB06C.
    //[C -> S][706C]
    //00                                                flag

    //[S -> C][B06C]
    //01                                                flag
    //00                                                ................
    //00                                                ................
    //00                                                ................
    /// </summary>
    public class PartyMatchRefresh : BidirectionalPacket
    {
        public override ushort Opcode => 0x706C;
        public override ushort ServerOpcode => 0xB06C;

        public PartyMatchRefresh() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode, true, false, new byte[] { 0x00 });
        }

        //[C -> S] [706C] Request party match list
        //00                                                ................

        //[S -> C] [B06C] Receive response
        //01                                                flag
        //01                                                totalPages
        //00                                                currentPage
        //05                                                partiesOnCurrentPage(5)
        //23 4F 00 00                                       partyNumber(20259)
        //5D 73 63 5A                                       unknownFlag(partyLeader ID) ?
        //08 00                                             leaderNameLength
        //4B 6F 52 6F 5A 61 4B 69                           partyLeaderName(KoRoZaKi)
        //00                                                race (0 == chinese)
        //02                                                members(2)
        //05                                                partyType(canInvite, itemfree, expshare) ?
        //00                                                purpose(hunting LTP)
        //01                                                minLevel(1)
        //50                                                maxLevel(80)
        //08 00                                             titleLength
        //76 65 6D 20 61 6D 6F 72                           partyTitle(vem.amor)
        protected override ICommand ParsePacket(Packet p)
        {
            // Refresh party matching list
            CharInfoGlobal.PartyMatchingList.Clear();

            p.ReadUInt8(); // unknown flag page?
            p.ReadUInt8(); // unknown flag
            p.ReadUInt8(); // unknown flag
            byte count = p.ReadUInt8();

            for (int i = 0; i < count; i++)
            {
                PartyEntry party = new PartyEntry(p.ReadUInt32());
                party.LeaderID = p.ReadUInt32();
                party.Leader = p.ReadAscii();
                party.Race = p.ReadUInt8();
                party.MemberCount = p.ReadUInt8();
                party.Type = (PartyType)p.ReadUInt8();
                party.Objective = (PartyObjective)p.ReadUInt8();
                party.MinLevel = p.ReadUInt8();
                party.MaxLevel = p.ReadUInt8();
                party.Title = p.ReadAscii();

                if (!CharInfoGlobal.PartyMatchingList.Contains(party))
                {
                    CharInfoGlobal.PartyMatchingList.InsertSorted(party, CharInfoGlobal.PartyComparison);
                }
            }

            return null;
        }
    }
}
