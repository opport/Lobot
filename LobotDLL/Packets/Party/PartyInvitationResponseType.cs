﻿namespace LobotDLL.Packets.Party
{
    public enum PartyInvitationResponseType : byte
    {
        Accept = 0x01,
        Decline = 0x02
    }
}
