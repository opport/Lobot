﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    //[S -> C] [3864]
    //02                                                flag add
    //FF pk2flag none
    //4E 51 00 00                                       memberID
    //09 00                                             nameLength
    //4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
    //84 07 00 00                                       charavatar
    //01                                                level 1
    //AA                                                unknown positionFlag?
    //A9 62                                             xSector ySector
    //69 05                                             x
    //00 00                                             z
    //54 06                                             y
    //01 00 01 00                                       angle
    //00 00                                             guild name
    //04                                                unknown 
    //00 00 00 00                                       mastery none
    //00 00 00 00                                       mastery none

    //// update position
    //[S->C][3864]
    //06                                                updateflag 6 move
    //4E 51 00 00                                       memberID
    //20                                                unknownflag
    //A9 62                                             xSector ySector
    //6B 05                                             x
    //00 00                                             z
    //3E 06                                             y
    //01 00 01 00                                       angle
    //
    //
    //leave Party
    //[C -> S]
    //[7061]

    //[S -> C] [3864]
    //01                                                ................
    //0B 00                                             ................
    /// </summary>
    public class PartyMemberUpdateHandler : Handler
    {
        private const ushort KickedFlag = 0xB0; // 0B 00

        public static event EventHandler<PartyChangedArgs> PartyMemberUpdated = delegate { };

        public override ushort ServerOpcode => 0x0384;

        public PartyMemberUpdateHandler() { }

        private void RemoveMember(Packet p)
        {
            ushort flag = p.ReadUInt16();
            if (flag == KickedFlag)
            {
                Bot.PartyCurrent = new PartyEntry(0);
            }
        }

        private void AddMember(Packet p)
        {
            p.ReadUInt8(); // pk2 flag
            PartyMember member = new PartyMember(p.ReadUInt32());

            member.Name = p.ReadAscii();
            member.Avatar = p.ReadUInt32();
            member.Level = p.ReadUInt8();
            byte unkn1 = p.ReadUInt8(); // unknown 170
            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();
            ushort x = p.ReadUInt16();
            ushort z = p.ReadUInt16();
            ushort y = p.ReadUInt16();
            float angle = p.ReadSingle();
            member.GuildName = p.ReadAscii();
            byte unkn3 = p.ReadUInt8(); //unknown 4
            member.Mastery1 = p.ReadUInt32(); //mastery 1
            member.Mastery1 = p.ReadUInt32(); // mastery 2

            Bot.PartyCurrent.Members.Add(member);
        }

        private void UpdateMemberMovement(Packet p)
        {
            p.ReadUInt8(); // pk2 flag
            uint memberID = p.ReadUInt32();
            byte unkn1 = p.ReadUInt8(); // unknown 32
            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();
            ushort x = p.ReadUInt16();
            ushort z = p.ReadUInt16();
            ushort y = p.ReadUInt16();
            float angle = p.ReadSingle();
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte updateFlag = p.ReadUInt8(); // update flag 1 = disbanded, 2 = add member, 6 = update member movement

            switch (updateFlag)
            {
                case 1: // leave
                    RemoveMember(p);
                    OnPartyMemberUpdated(new PartyChangedArgs(PartyChangeType.Empty));
                    break;
                case 2:
                    AddMember(p);
                    OnPartyMemberUpdated(new PartyChangedArgs(PartyChangeType.Update));
                    break;
                case 6:
                    UpdateMemberMovement(p);
                    OnPartyMemberUpdated(new PartyChangedArgs(PartyChangeType.Update));
                    break;
                default:
                    break;
            }

            return null;
        }

        private void OnPartyMemberUpdated(PartyChangedArgs e)
        {
            EventHandler<PartyChangedArgs> handler = PartyMemberUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
