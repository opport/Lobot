﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C -> S][7061]
    //
    //[S -> C][3864]
    //01                                                ................
    //0B 00                                             ................
    /// </summary>
    public class PartyLeave : Command
    {
        public static event EventHandler LeftParty = delegate { };

        public override ushort Opcode => 0x7061;

        public PartyLeave() { }

        protected override Packet AddPayload()
        {
            Bot.PartyCurrent = new PartyEntry(0);
            OnLeftParty(new PartyChangedArgs(PartyChangeType.Update));
            return new Packet(Opcode);
        }

        private void OnLeftParty(PartyChangedArgs e)
        {
            EventHandler handler = LeftParty;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
