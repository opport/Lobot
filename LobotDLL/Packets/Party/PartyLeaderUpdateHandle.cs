﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Party
{
    public class PartyLeaderUpdateHandler : Handler
    {
        /// <summary>
        //[S -> C][3065]
        //FF                                                pkflag 255 = none
        //BA 00 00 00                                       partynumber
        //2E 51 00 00                                       leaderID
        //07                                                partyType
        //01                                                partymembers
        //FF                                                unknownflag 255
        //2E 51 00 00                                       ID
        //05 00                                             namelength
        //4C 6F 62 6F 74                                    Lobot...........
        //73 07 00 00                                       charAvatar 1907
        //06                                                level 6
        //AA unknown
        //A9 62                                             xSector ySector
        //B6 05                                             x
        //00 00                                             z
        //A3 06                                             y
        //01 00 01 00                                       angle
        //00 00                                             guildnameLength
        //04                                                ................
        //03 01 00 00                                       mastery pacheon 259
        //13 01 00 00                                       mastery fire 275
        /// </summary>
        public override ushort ServerOpcode => 0x3065;

        public PartyLeaderUpdateHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte unkn0 = p.ReadUInt8(); //unknown 255
            Bot.PartyCurrent.PartyNumber = p.ReadUInt32();
            Bot.PartyCurrent.LeaderID = p.ReadUInt32();
            Bot.PartyCurrent.Type = (PartyType)p.ReadUInt8();
            Bot.PartyCurrent.MemberCount = p.ReadUInt8();
            byte unkn1 = p.ReadUInt8(); // unknown 255
            Bot.PartyCurrent.LeaderID = p.ReadUInt32(); //leader ID
            Bot.PartyCurrent.Leader = p.ReadAscii();
            p.ReadUInt32(); // leader avatar
            p.ReadUInt8(); // leader level
            byte unkn2 = p.ReadUInt8(); // unknown 170
            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();
            ushort x = p.ReadUInt16();
            ushort z = p.ReadUInt16();
            ushort y = p.ReadUInt16();
            float angle = p.ReadSingle();
            p.ReadAscii(); // leader guild name
            byte unkn3 = p.ReadUInt8(); //unknown 4
            p.ReadUInt32(); //mastery 1
            p.ReadUInt32(); // mastery 2

            return null;
        }
    }
}
