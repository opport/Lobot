﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Party
{
    public class PartyInvitationJoinACKHandler : Handler
    {
        /// <summary>
        // Invitation
        //[S -> C][3080]
        //02                                                invitationFlag 2
        //D6 8E CB 01                                       inviterID
        //07                                                partyType exp/item/autoshare

        //[C->S][3080]
        //02                                                inviteResponse 2 decline
        //0C 2C unknown

        //[S->C][B067]
        //02                                                inviteResponse 2 decline
        //0C 2C unknown



        //// Invitation
        //[S->C][3080]
        //02                                                invitationFlag 2
        //D6 8E CB 01                                       inviterID
        //07                                                partyType

        //[C->S][3080]
        //01                                                acceptFlag1
        //01                                                acceptFlag2

        //[S->C][B067]
        //01                                                confirmInvitationFlag
        //4E 51 00 00                                       partyMemberID
        /// </summary>
        public override ushort ServerOpcode => 0xB067;

        public PartyInvitationJoinACKHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            if (p.ReadUInt8() == 0x01)
            {
                Bot.PartyMemberID = p.ReadUInt32();
            }
            else
            {
                // declined invite next person ? 
            }

            return null;
        }
    }
}
