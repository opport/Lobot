﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C -> S][7063]
    //4E 51 00 00                                       memberID
    /// </summary>
    public class PartyBanMember : Command
    {
        uint MemberID;

        public override ushort Opcode => 0x7063;

        public PartyBanMember() { }

        public PartyBanMember(uint memberID)
        {
            MemberID = memberID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt32(MemberID);
            return p;
        }
    }
}
