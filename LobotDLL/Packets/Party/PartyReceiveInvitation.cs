﻿using LobotAPI.Packets;
using LobotAPI.Packets.Party;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    // Invitation
    //[S -> C][3080]
    //02                                                invitationFlag 2
    //D6 8E CB 01                                       inviterID
    //07                                                partyType exp/item/autoshare

    //[C->S][3080]
    //02                                                inviteResponse 2 decline
    //0C 2C unknown

    //[S->C][B067]
    //02                                                inviteResponse 2 decline
    //0C 2C unknown



    //// Invitation
    //[S->C][3080]
    //02                                                invitationFlag 2
    //D6 8E CB 01                                       inviterID
    //07                                                partyType

    //[C->S][3080]
    //01                                                acceptFlag1
    //01                                                acceptFlag2

    //[S->C][B067]
    //01                                                confirmInvitationFlag
    //4E 51 00 00                                       partyMemberID
    /// </summary>
    public class PartyReceiveInvitation : BidirectionalPacket
    {
        private const ushort DeclineByte = 11276; // 0C 2C
        PartyInvitationResponseType Response;

        public override ushort Opcode => 0x3080;
        public override ushort ServerOpcode => 0x3080;

        public static event EventHandler<PartyInvitationArgs> ReceivedInvitation = delegate { };

        protected void OnPacketReceived(PartyInvitationArgs e)
        {
            EventHandler<PartyInvitationArgs> handler = ReceivedInvitation;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public PartyReceiveInvitation() { }

        public PartyReceiveInvitation(PartyInvitationResponseType response)
        {
            Response = response;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            if (Response == PartyInvitationResponseType.Accept)
            {
                p.WriteUInt8(Response);
                p.WriteUInt8(Response);
            }
            else
            {
                p.WriteUInt8(0x02);
                p.WriteUInt8(DeclineByte);
            }
            return p;
        }

        // Invitation
        //[S -> C] [3080]
        //02                                                invitationFlag 2
        //D6 8E CB 01                                       inviterID
        //07                                                partyType exp/item/autoshare

        //[C->S][3080]
        //02                                                inviteResponse 2 decline
        //0C 2C unknown

        //[S->C][B067]
        //02                                                inviteResponse 2 decline
        //0C 2C unknown



        //// Invitation
        //[S->C][3080]
        //02                                                invitationFlag 2
        //D6 8E CB 01                                       inviterID
        //07                                                partyType

        //[C->S][3080]
        //01                                                acceptFlag1
        //01                                                acceptFlag2
        protected override ICommand ParsePacket(Packet p)
        {
            if (p.ReadUInt8() == 0x02) // flag invited
            {
                uint inviterID = p.ReadUInt32();
                byte type = p.ReadUInt8();
                EventArgs args = new EventArgs();
                OnPacketReceived(new PartyInvitationArgs(inviterID, (PartyType)type));
            }
            else
            {
                // todo find other values 
            }

            return null;
        }

    }
}
