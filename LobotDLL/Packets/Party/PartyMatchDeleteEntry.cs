﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C -> S][706B]
    //77 04 00 00                                       partyID

    //[S -> C][B06B]
    //01                                                successFlag
    //77 04 00 00                                       partyID
    /// </summary>
    public class PartyMatchDeleteEntry : BidirectionalPacket
    {
        public static event EventHandler PartyMatchEntryDeleted = delegate { };

        private void OnPartyMatchEntryDeleted(PartyChangedArgs e)
        {
            EventHandler handler = PartyMatchEntryDeleted;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort Opcode => 0x706B;
        public override ushort ServerOpcode => 0xB06B;

        public PartyMatchDeleteEntry() { }

        protected override Packet AddPayload()
        {

            if (Bot.PartyCurrent.PartyNumber != 0)
            {
                Packet p = new Packet(Opcode, false);
                p.WriteUInt32(Bot.PartyCurrent.PartyNumber);
                return p;
            }
            else
            {
                return null;
            }
        }
        protected override ICommand ParsePacket(Packet p)
        {
            if (p.ReadUInt8() == 0x01)
            {
                Bot.PartyCurrent = new PartyEntry(0);
            }

            OnPartyMatchEntryDeleted(new PartyChangedArgs(PartyChangeType.Update));
            return null;
        }
    }
}
