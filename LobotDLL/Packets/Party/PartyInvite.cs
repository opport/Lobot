﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Party
{
    /// <summary>
    //Invite to party
    //exp/item autoshare

    //[C->S][7060]
    //BF 29 BF 01                                       invitedPlayerID
    //07                                                partyType

    //[S->C][B060]
    //02                                                invitationResponse 2 decline
    //0C 2C unknown 716

    //[C -> S]
    //[7060]
    //BF 29 BF 01                                       invitedPlayerID
    //07                                                partyType

    //[S->C][B060]
    //01                                                invitationResponse 1 accept
    //2E 51 00 00                                       unknown
    /// </summary>
    public class PartyInvite : BidirectionalPacket
    {
        uint InvitedPlayerID;
        PartyType Type;

        public override ushort Opcode => 0x7060;
        public override ushort ServerOpcode => 0xB060;

        public PartyInvite() { }

        public PartyInvite(uint playerID, PartyType type)
        {
            InvitedPlayerID = playerID;
            Type = type;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt32(InvitedPlayerID);
            p.WriteUInt8(Type);
            return p;
        }

        //Invite to party
        //exp/item autoshare

        //[C->S][7060]
        //BF 29 BF 01                                       invitedPlayerID
        //07                                                partyType

        //[S->C][B060]
        //02                                                invitationResponse 2 decline
        //0C 2C                                             unknown 716

        //[C -> S]
        //[7060]
        //BF 29 BF 01                                       invitedPlayerID
        //07                                                partyType

        //[S->C][B060]
        //01                                                invitationResponse 1 accept
        //2E 51 00 00                                       partyLeaderID

        protected override ICommand ParsePacket(Packet p)
        {
            if (p.ReadUInt8() == 0x01)
            {
                Bot.PartyCurrent.LeaderID = p.ReadUInt32();
            }
            else
            {
                // declined invite next person ? 
            }

            return null;
        }
    }
}
