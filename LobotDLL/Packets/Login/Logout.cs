﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C -> S][7006]

    //[S -> C][B006]
    //01                                                ................                                     
    /// </summary>
    public class Logout : BidirectionalPacket
    {
        LogoutMode LogoutMode;

        public override ushort Opcode => 0x7005;
        public override ushort ServerOpcode => 0xB005;

        public Logout() { }

        public Logout(LogoutMode logoutOption)
        {
            LogoutMode = logoutOption;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(LogoutMode);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 1)
            {
                byte countdown = p.ReadUInt8();
                byte logoutMode = p.ReadUInt8();
            }
            else
            {
                //error
                ushort errorCode = p.ReadUInt8();
            }
            return null;
        }
    }
}
