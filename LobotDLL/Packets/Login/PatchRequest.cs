﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Login
{
    class PatchRequest : BidirectionalPacket
    {
        private const string SROCLIENT = "SR_Client";

        public override ushort Opcode => 0x6100;
        public override ushort ServerOpcode => 0xA100;

        public PatchRequest() { }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(LoginSettings.Locale);
            p.WriteAscii(SROCLIENT);
            p.WriteUInt32(LoginSettings.Version); //Version

            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            if (result == 1)
            {
                return new GatewayShardListRequest();
            }
            else
            {
                return null;
            }
        }
    }
}
