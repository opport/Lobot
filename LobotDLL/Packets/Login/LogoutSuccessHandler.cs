﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Login
{
    public class LogoutSuccessHandler : Handler
    {
        public static EventHandler Disconnected = delegate { };

        private static void OnDisconnected(EventArgs e)
        {
            EventHandler handler = Disconnected;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0x300A;

        public LogoutSuccessHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            Bot.Status = BotStatus.Disconnected;
            OnDisconnected(EventArgs.Empty);
            return null;
        }
    }
}
