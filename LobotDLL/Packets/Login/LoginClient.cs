﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    /// <summary>
    /// [C->S] [25] [0x6102] [E] 
    //16						            // Locale 0x16 -> 22 , alias Content.id (?)
    //06 00						            //username length = 6
    //6f 70 70 6f 72 74				        opport
    //0c 00						            //passwordlength = 10
    //31 32 33 34 71 77 65 72 61 73 64 66   1234qwerasdf@........
    //40 00						            //shard.id (?)

    //[S->C] [1] [0xa323] 
    //01                                                 ................

    //[S->C] [14] [0x2001] 
    //0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...

    //[C->S] [33] [0x6103] [E] 
    //28 00 00 00 06 00 6f 70 70 6f 72 74 0c 00 31 32    (.....opport..12
    //33 34 71 77 65 72 61 73 64 66 16 00 00 00 00 00    34qwerasdf......
    //00                                                 ................

    //[C->S] [33] [0x6103] [E] 
    //28 00 00 00                                        SessionID
    //06 00                                              NameLength 6
    //6f 70 70 6f 72 74                                  opport
    //0c 00                                              passwordlength 12
    //31 32 33 34 71 77 65 72 61 73 64 66                1234qwerasdf
    //16                                                 locale 40
    //00 00 00 00 00 00                                  mac address(empty)
    /// </summary>
    public class LoginClient : BidirectionalPacket
    {
        readonly uint SessionID;
        readonly string Username;
        readonly string Password;
        readonly ushort Locale;

        public override ushort Opcode => 0x6103;
        public override ushort ServerOpcode => 0xA103;

        public LoginClient() { }

        public LoginClient(string username, string password, uint sessionID, ushort locale)
        {
            this.Username = username;
            this.Password = password;
            this.SessionID = sessionID;
            this.Locale = locale;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(SessionID);
            p.WriteAscii(Username);
            p.WriteAscii(Password);
            p.WriteUInt16(Locale);
            // write random 6 octet mac address
            Random mac = new Random();
            p.WriteUInt16(0x0000); // 2 empty
            p.WriteUInt32((uint)mac.Next()); // 4 random
            return p;
        }

        //  1   byte result
        //  if(result == 0x02)
        //  {
        //      1   byte errorCode
        //  }
        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            if (result == 0x02)
            {
                System.Diagnostics.Debug.WriteLine($"LoginClient Error {p.Opcode:x}");
                Logger.Log(LogLevel.NETWORK, $"LoginClient Error {p.Opcode:x} - {p.Encrypted.ToString()} - {p.Massive.ToString()} - {String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))}");
            }
            else
            {
                Packet charList = new Packet(0x7007);
                charList.WriteUInt8(0x02);
                // Make sure login command cannot be sent again
                LoginSettings.CanLogin = false;
                return new CharSelectionExecuteAction(CharacterSelectionAction.List);
            }

            return null;
        }

        /// <summary>
        /// Check login packet and spoof it with valid data if necessary before sending to Agent server.
        /// After injecting a packet using a proxy program, the gateway connection is closed and a connection is established with the Agent server.
        /// The Client does not have the username or password however, as these were not entered manually in the client but sent through the proxy instead.
        /// This causes the client to send an invalid 0x6103 login packet with an empty username (and password) to the agent server after 0xA102.
        /// </summary>
        /// <param name="p">Input packet</param>
        /// <param name="result">Output packet</param>
        /// <returns>Whether the packet was replaced or not</returns>
        public static bool TrySpoofClientLoginPacket(Packet p, out Packet result)
        {
            if (p.Opcode == 0x6103)
            {
                ////Encrypted
                //4   uint Token //from LOGIN_RESPONSE
                //2   ushort Username.Length
                //* string  Username
                //2   ushort Password.Length
                //* string  Password
                //1   byte Content.ID
                //6   byte[] MAC-Address
                uint token = p.ReadUInt32();
                string username = p.ReadAscii();
                string password = p.ReadAscii();
                byte locale = p.ReadUInt8();
                byte[] macAddress = p.ReadUInt8Array(6);

                // Spoof packet if it is invalid
                if (string.IsNullOrEmpty(username))
                {
                    result = new Packet(0x6103, true);
                    result.WriteUInt32(token);
                    result.WriteAscii(LoginSettings.Username);
                    result.WriteAscii(LoginSettings.Password);
                    result.WriteUInt8(locale);
                    // write random 6 octet mac address
                    Random mac = new Random();
                    result.WriteUInt16(0x0000); // 2 empty
                    result.WriteUInt8Array(macAddress);
                    return true;
                }

            }

            // no changes if packet is valid
            result = p;
            return false;

        }
    }
}
