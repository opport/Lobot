﻿using LobotAPI.Packets;
using LobotDLL.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C -> S][7006]

    //[S -> C][B006]
    //01                                                ................   
    /// </summary>
    public class LogoutCancel : BidirectionalPacket
    {
        readonly byte ContentID;
        readonly string Username;
        readonly string Password;
        readonly ushort ShardID;

        public override ushort Opcode => 0x7006;
        public override ushort ServerOpcode => 0xB006;

        public LogoutCancel() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode, true);
        }

        public static void ResetTries() { }

        //[C -> S]
        //[7006]

        //[S -> C][B006]
        //01                                                ................   
        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();
            if (result == 2)
            {
                // error
                LogoutErrorCode errorCode = (LogoutErrorCode)p.ReadUInt16();
            }
            return null;
        }
    }
}
