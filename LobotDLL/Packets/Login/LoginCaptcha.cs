﻿using LobotAPI.Captcha;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    public class LoginCaptcha : BidirectionalPacket
    {
        //        CLIENT_GATEWAY_LOGIN_IBUV_CONFIRM_REQUEST = 0x6323
        //2   ushort Code.Length
        //*   string Code
        private readonly string Code;
        private readonly ushort CodeLength;
        private static int Counter = 0;

        public override ushort Opcode => 0x6323;
        public override ushort ServerOpcode => 0x2322;

        public LoginCaptcha() { }

        public LoginCaptcha(string code)
        {
            Code = code;
            CodeLength = (ushort)code.Length;
        }

        protected override Packet AddPayload()
        {
            if (Counter > 0)
            {
                return null;
            }
            //TODO Fix Captcha
            Packet p = new Packet(Opcode);
            //p.WriteUInt16(1); - length automatically prepended by WriteAscii()
            p.WriteAscii("A");
            Counter++;
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            CaptchaProcessor.BytesToCaptcha(p.GetBytes());
            return null;
        }
    }
}
