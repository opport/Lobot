﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotDLL.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Login
{
    /// <summary>
    //[S->C] [14] [0x2001] 
    //0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...
    /// </summary>
    public class ServerInfoHandler : Handler
    {
        private const string GATEWAY = "GatewayServer";
        private const string AGENT = "AgentServer";

        public override ushort ServerOpcode => 0x2001;

        public ServerInfoHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            string serverName = p.ReadAscii();

            if (serverName == GATEWAY)
            {
                return new PatchRequest();
            }
            else if (serverName == AGENT)
            {
                return new LoginClient(LoginSettings.Username, LoginSettings.Password, LoginSettings.SessionID, LoginSettings.Locale);
            }
            p.ReadUInt8();

            return null;
        }
    }
}
