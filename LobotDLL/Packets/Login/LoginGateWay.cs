﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    /// <summary>
    /// [C->S] [25] [0x6102] [E] 
    //16						            // Locale 0x16 -> 22 , alias Content.id (?)
    //06 00						            //username length = 6
    //6f 70 70 6f 72 74				        opport
    //0c 00						            //passwordlength = 10
    //31 32 33 34 71 77 65 72 61 73 64 66   1234qwerasdf@........
    //40 00						            //shard.id (?)

    //[S->C] [1] [0xa323] 
    //01                                                 ................

    //[S->C] [14] [0x2001] 
    //0b 00 41 67 65 6e 74 53 65 72 76 65 72 00          ..AgentServer...

    //[C->S] [33] [0x6103] [E] 
    //28 00 00 00 06 00 6f 70 70 6f 72 74 0c 00 31 32    (.....opport..12
    //33 34 71 77 65 72 61 73 64 66 16 00 00 00 00 00    34qwerasdf......
    //00                                                 ................

    //[S->C] [52] [0xa101] 
    //01
    //14						                    //farm.id
    //1b 00						                    //farm.name.length = 27
    //53 52 4f 5f 56 69 65 74 6e 61 6d 5f    		
    //54 65 73 74 4c 6f 63 61 6c 20 5b 46 5d 20 30	//SRO_Vietnam_TestLocal.[F].0.
    //00  						  
    //01						//hasEntries
    //40 00						//shard.id
    //08 00						//shard.Name.Length
    //54 65 72 6d 69 6e 75 73	//shard.Name = Terminus 
    //1a 00						//shard.Onlinecount = 6656
    //f4 01						//shard.Capacity = 62465
    //01						//shard.IsOperating = true
    //14						//shard.farmId = 20 
    //00                                     
    /// </summary>
    public class LoginGateWay : Command
    {
        readonly byte Locale;
        readonly string Username;
        readonly string Password;
        readonly ushort ShardID;

        public override ushort Opcode => 0x6102;
        public LoginGateWay() { }

        public LoginGateWay(string username, string password, byte contentID, ushort shardID)
        {
            this.Username = username;
            this.Password = password;
            this.Locale = contentID;
            this.ShardID = shardID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(Locale);
            p.WriteAscii(Username);
            p.WriteAscii(Password);
            p.WriteUInt16(ShardID);
            return p;
        }
    }
}
