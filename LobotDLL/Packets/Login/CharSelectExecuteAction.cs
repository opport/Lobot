﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotDLL.Connection;
using SilkroadSecurityApi;
using System;
using System.Linq;
using System.Timers;

namespace LobotDLL.Packets
{
    /// <summary>
    //    1   byte action
    //if(action == CharacterSelectionAction.Create)
    //{
    //	2   ushort Name.Length
    //	*   string Name
    //	4   uint RefObjID
    //	1   byte Scale
    //	4   uint RefItemID // EQUIP_SLOT_MAIL
    //	4   uint RefItemID // EQUIP_SLOT_PANTS
    //	4   uint RefItemID // EQUIP_SLOT_BOOTS
    //	4   uint RefItemID // EQUIP_SLOT_WEAPON
    //}
    //else if(action == CharacterSelectionAction.Delete || 
    //        action == CharacterSelectionAction.CheckName || 
    //        action == CharacterSelectionAction.Restore)
    //{
    //	2   ushort Name.Length
    //	*   string Name

    /// </summary>
    public class CharSelectionExecuteAction : BidirectionalPacket
    {
        public static event EventHandler CharListReceived = delegate { };

        CharacterSelectionAction Action;

        public override ushort Opcode => 0x7007;
        public override ushort ServerOpcode => 0xB007;

        public CharSelectionExecuteAction() { }

        public CharSelectionExecuteAction(CharacterSelectionAction action)
        {
            Action = action;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(Action);
            return p;
        }

        private static Timer CharSelectTimer = new Timer(3000) { AutoReset = false };
        private static void TimerElapsedFunction(object sender, EventArgs e)
        {
            string charName = LoginSettings.CharactersAvailable.FirstOrDefault(s => string.Equals(s, LoginSettings.CharName, StringComparison.OrdinalIgnoreCase));
            if (Bot.Status == BotStatus.CharacterSelect)
            {
                SelectCharacter.SelectedChar += HandleSelectectedChar;
                Proxy.Instance.SendCommandAG(new SelectCharacter(charName));
                // Toggle flag to prevent sending multiple SelectCharacter packets after receiving multiple B007 CharSelectLists 
                // Taken care of in B001     
                //Bot.Status = BotStatus.Loading;
            }
        }

        private static void HandleSelectectedChar(object sender, EventArgs e)
        {
            // prevent sending too many char select requests after first login
            CharSelectTimer.Elapsed -= TimerElapsedFunction;
        }

        //1   byte action
        //1   byte result
        //if(result == 0x01 && type == CharacterSelectionAction.List)
        //{
        //	1   byte characterCount
        //	foreach(character)
        //	{
        //		4   uint characterRefObjID = p.ReadUInt32();
        //		2   ushort characterName.Length
        //        *   string characterName
        //		1   byte characterScale
        //		1   byte characterCurLevel
        //		8   ulong characterExpOffset
        //		2   ushort characterStrength
        //		2   ushort characterIntelligence
        //		2   ushort characterStatPoint
        //		4   uint characterCurHP
        //		4   uint characterCurMP
        //		1   bool isDeleting
        //		if(isDeleting)

        //        {
        //        4   uint characterDeleteTime	//in Minutes

        //        }

        //		1   byte guildMemberClass
        //		1   bool isGuildRenameRequired
        //		if(isGuildRenameRequired)
        //		{
        //			2   ushort CurGuildName.Length
        //			*   string CurGuildName

        //        }		
        //		1   byte academyMemberClass

        //		1   byte itemCount
        //		foreach(item)
        //		{
        //			4   uint item.RefItemID
        //			1   byte item.Plus
        //		}

        //		1   byte avatarItemCount
        //		foreach(avatarItem)
        //		{
        //			4   uint avatarItem.RefItemID
        //			1   byte avatarItem.Plus
        //		}
        //	}
        //}
        //else if(result == 0x02)
        //{
        //	2   ushort errorCode
        //}                          


        //[C -> S][7007]
        //02                                                ................

        //[S -> C][B007]
        //02                                                ................
        //01                                                ................
        //01                                                ................
        //73 07 00 00                                       s...............
        //05 00                                             ................
        //4C 6F 62 6F 74                                    Lobot...........
        //22                                                "...............
        //06                                                ................
        //1C 03 00 00 00 00 00 00                           ................
        //19 00                                             ................
        //19 00                                             ................
        //0F 00                                             ................
        //F9 02 00 00                                       ................
        //F9 02 00 00                                       ................
        //00                                                ................
        //00                                                ................
        //00                                                ................
        //00                                                ................
        //08                                                ................
        //80 12 00 00                                       ................
        //07                                                ................
        //EC 12 00 00                                       ................
        //07                                                ................
        //C8 12 00 00                                       ................
        //07                                                ................
        //34 13 00 00                                       4...............
        //07                                                ................
        //10 13 00 00                                       ................
        //07                                                ................
        //58 13 00 00                                       X...............
        //07                                                ................
        //40 10 00 00                                       @...............
        //07                                                ................
        //47 0E 00 00                                       G...............
        //00                                                ................
        //02                                                ................
        //A0 5F 00 00                                       ._..............
        //00                                                ................
        //E8 5A 00 00                                       .Z..............
        //00                                                ................
        protected override ICommand ParsePacket(Packet p)
        {
            CharacterSelectionAction action = (CharacterSelectionAction)p.ReadUInt8();
            byte result = p.ReadUInt8();
            if (result == 0x01 && action == CharacterSelectionAction.List && Bot.Status != BotStatus.Loading)
            {
                // Reset to prevent duplication after creating new character
                LoginSettings.CharactersAvailable.Clear();

                byte characterCount = p.ReadUInt8();
                for (int i = 0; i < characterCount; i++)
                {
                    uint characterRefObjID = p.ReadUInt32();
                    //ushort characterNameLength = p.ReadAscii();
                    string characterName = p.ReadAscii();
                    LoginSettings.CharactersAvailable.Add(characterName); // Charselect List update!
                    byte characterScale = p.ReadUInt8();
                    byte characterCurLevel = p.ReadUInt8();
                    ulong characterExpOffset = p.ReadUInt64();
                    ushort characterStrength = p.ReadUInt16();
                    ushort characterIntelligence = p.ReadUInt16();
                    ushort characterStatPoint = p.ReadUInt16();
                    uint characterCurHP = p.ReadUInt32();
                    uint characterCurMP = p.ReadUInt32();
                    bool isDeleting = p.ReadUInt8() == 0x01;
                    if (isDeleting)

                    {
                        uint characterDeleteTime = p.ReadUInt32();	//in Minutes

                    }

                    byte guildMemberClass = p.ReadUInt8();
                    bool isGuildRenameRequired = p.ReadUInt8() == 0x01 ? true : false;
                    if (isGuildRenameRequired)
                    {
                        //ushort CurGuildNameLength = p.ReadUInt8();
                        string CurGuildName = p.ReadAscii();
                    }
                    byte academyMemberClass = p.ReadUInt8();

                    byte itemCount = p.ReadUInt8();
                    for (int j = 0; j < itemCount; j++)
                    {
                        uint itemRefItemID = p.ReadUInt32();
                        byte item = p.ReadUInt8();
                    }

                    byte avatarItemCount = p.ReadUInt8();
                    for (int k = 0; k < avatarItemCount; k++)
                    {
                        uint avatarItemRefItemID = p.ReadUInt32();
                        byte avatarItem = p.ReadUInt8();
                    }
                }

                SelectChar();
            }
            else if (result == 0x02)
            {
                ushort errorCode = p.ReadUInt16();
            }

            LoginSettings.CanSelectChar = true;
            Bot.Status = BotStatus.CharacterSelect;

            OnCharListreceived(EventArgs.Empty);
            return null;
        }

        private void SelectChar()
        {
            if (LoginSettings.AutoSelectCharacter && Bot.Status != BotStatus.Loading)
            {
                string charName = LoginSettings.CharactersAvailable.FirstOrDefault(s => string.Equals(s, LoginSettings.CharName, StringComparison.OrdinalIgnoreCase));

                if (!string.IsNullOrEmpty(charName))
                {
                    CharSelectTimer.Elapsed += TimerElapsedFunction;
                    CharSelectTimer.Start();
                }
            }
        }

        protected void OnCharListreceived(EventArgs e)
        {
            EventHandler handler = CharListReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
