﻿namespace LobotDLL.Packets
{
    /// <summary>
    /// https://github.com/DummkopfOfHachtenduden/SilkroadDoc/wiki/CharacterSelectionErrorCode
    /// </summary>
    public enum CharacterSelectionAction : byte
    {
        Create = 1,
        List = 2,
        Delete = 3,
        CheckName = 4,
        Restore = 5
    }
}
