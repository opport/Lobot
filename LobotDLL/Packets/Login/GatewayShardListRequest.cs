﻿using LobotAPI;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    public class GatewayShardListRequest : BidirectionalPacket
    {
        private const int MaxTries = 3;
        private static int CurrentTries = 0;
        public static EventHandler ReceivedShardList = delegate { };

        public override ushort Opcode => 0x6101;
        public override ushort ServerOpcode => 0xA101;

        public GatewayShardListRequest() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode, true);
        }

        public static void ResetTries()
        {
            CurrentTries = 0;
        }

        //[S->C] [52] [0xa101] 
        //01
        //14						                    //farm.id
        //1b 00						                    //farm.name.length = 27
        //53 52 4f 5f 56 69 65 74 6e 61 6d 5f    		
        //54 65 73 74 4c 6f 63 61 6c 20 5b 46 5d 20 30	//SRO_Vietnam_TestLocal.[F].0.
        //00  						  
        //01						//hasEntries
        //40 00						
        //08 00						//shard.Name.Length
        //54 65 72 6d 69 6e 75 73	//shard.Name = Terminus 
        //1a 00						//shard.Onlinecount = 6656
        //f4 01						//shard.Capacity = 62465
        //01						//shard.IsOperating = true
        //14						//shard.farmId = 20 
        //00                                     
        protected override ICommand ParsePacket(Packet p)
        {
            bool canLogin = p.ReadUInt8() == 0x01;

            if (canLogin)
            {
                LoginSettings.CanLogin = canLogin;
                byte farmID = p.ReadUInt8();
                string farmName = p.ReadAscii();
                byte unkn0 = p.ReadUInt8();
                byte hasEntries = p.ReadUInt8();

                while (hasEntries == 0x01)
                {
                    LoginSettings.ShardID = p.ReadUInt16();
                    string shardName = p.ReadAscii();
                    ushort shardOnlineCount = p.ReadUInt16();
                    ushort shardOnlineCapacity = p.ReadUInt16();
                    byte isOperating = p.ReadUInt8();
                    byte shardFarmID = p.ReadUInt8();
                    hasEntries = p.ReadUInt8();
                }
            }

            Bot.Status = BotStatus.Login;

            OnReceivedShardList(EventArgs.Empty);

            return null; //LoginSettings.AutoConnect
                         //&& LoginSettings.CanLogin
                         //&& !string.IsNullOrEmpty(LoginSettings.Username) && !string.IsNullOrEmpty(LoginSettings.Password)
                         //? TryToConnect()
                         //: null;
        }

        private Command TryToConnect()
        {
            if (CurrentTries < MaxTries && !string.IsNullOrEmpty(LoginSettings.Username) && !string.IsNullOrEmpty(LoginSettings.Password))
            {
                CurrentTries++;
                return new LoginGateWay(LoginSettings.Username, LoginSettings.Password, LoginSettings.Locale, LoginSettings.ShardID);
            }
            else
            {
                return null;
            }
        }

        protected void OnReceivedShardList(EventArgs e)
        {
            EventHandler handler = ReceivedShardList;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
