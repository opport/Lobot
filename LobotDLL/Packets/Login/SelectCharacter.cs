﻿using LobotAPI;
using LobotAPI.Packets;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    /// <summary>
    //[C->S] [7] [0x7001] [E] 
    //05 00 4c 6f 62 6f 74                               ..Lobot.........
    /// </summary>
    public class SelectCharacter : BidirectionalPacket
    {
        string SelectedCharName;

        public override ushort Opcode => 0x7001;
        public override ushort ServerOpcode => 0xB001;

        public static event EventHandler SelectedChar = delegate { };

        protected void OnSelectedChar(EventArgs e)
        {
            EventHandler handler = SelectedChar;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public SelectCharacter() { }

        public SelectCharacter(string selectedCharName)
        {
            SelectedCharName = selectedCharName ?? throw new ArgumentNullException(nameof(selectedCharName));
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteAscii(SelectedCharName);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte result = p.ReadUInt8();

            if (result == 0x01)
            {
                Bot.Status = BotStatus.Loading;

                PartyStateMachine.Instance.Start();

                OnSelectedChar(EventArgs.Empty);
            }
            else if (result == 0x02)
            {
                ushort errorCode = p.ReadUInt8();
            }

            return null;
        }

    }
}
