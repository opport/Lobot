﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    public class LoginUpdateCharInfo : Command
    {
        readonly Packet Packet;

        public override ushort Opcode => 0x01;

        public LoginUpdateCharInfo() { }

        public LoginUpdateCharInfo(Packet packet)
        {
            Packet = packet;
        }

        protected override Packet AddPayload()
        {
            throw new NotImplementedException();
        }
    }
}
