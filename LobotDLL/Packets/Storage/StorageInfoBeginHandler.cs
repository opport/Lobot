﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Storage
{
    public class StorageInfoBeginHandler : Handler
    {
        //[S -> C] [3047]
        //00 00 00 00 00 00 00 00                          ulong buffer
        public override ushort ServerOpcode => 0x3047;

        public StorageInfoBeginHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            p.ReadUInt64(); // empty buffer
            return null;
        }
    }
}
