﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using static LobotAPI.Structs.Item;

namespace LobotDLL.Packets.Storage
{
    //begin storage info
    //[S -> C][3047]
    //00 00 00 00 00 00 00 00                           ulong buffer

    //storage data
    //[S->C][3049]
    //96 02 01 00 00 00 00 05 00 00 00 01 00 02 00 00   ........
    //00 00 0B 00 00 00 04 00                           ........

    //storage data parsed
    //[S -> C] [0000]
    //96                                                storage size 150
    //02                                                number of items 2
    //01                                                slot number 1
    //00 00 00 00                                       unknown uint32
    //05 00 00 00                                       refID 5 -> hp herb
    //01 00                                             stack amount 1
    //02                                                slot number 2
    //00 00 00 00                                       unknown uint32
    //0B 00 00 00                                       refID 11 -> mp herb
    //04 00                                             stack amount 4
    public class StorageInfoDataHandler : Handler
    {
        public static event EventHandler StorageParsed = delegate { };

        private void OnStorageParsed(EventArgs e)
        {
            EventHandler handler = StorageParsed;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0x3049;

        public StorageInfoDataHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            CharInfoGlobal.Storage.AsCollection().Clear();
            CharInfoGlobal.StorageSize = p.ReadUInt8();

            byte numberOfItems = p.ReadUInt8();
            for (int i = 0; i < numberOfItems; i++)
            {
                Item item = ParseNewItem(p);
                if (item != null)
                {
                    CharInfoGlobal.Storage[item.Slot] = item;
                }
            }

            OnStorageParsed(EventArgs.Empty);
            return null;
        }

        protected Item ParseNewItem(Packet p)
        {
            Item item = new Item();
            item.Slot = p.ReadUInt8();
            item.RentType = p.ReadUInt32();

            switch (item.RentType)
            {
                case 1:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    break;

                case 2:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.MeterRateTime = p.ReadUInt32();
                    break;

                case 3:
                    item.Rent.CanDelete = p.ReadUInt16(); // (adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.PackingTime = p.ReadUInt32();
                    break;
            }

            item.RefItemID = p.ReadUInt32();

            if (PK2DataGlobal.Items == null)
            {
                PK2Utils.ExtractAllInformation();
            }

            if (PK2DataGlobal.Items.TryGetValue(item.RefItemID, out Pk2Item value))
            {
                string valueType = value.Type.ToString("X").Trim('0');

                if (value.IsEquipmentItem()) // Equipment
                {
                    Equipment e = new Equipment(item);
                    e.OptLevel = p.ReadUInt8();
                    e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.ReadUInt64());
                    e.Durability = p.ReadUInt32(); //(=> Durability)
                    e.MagParamNum = p.ReadUInt8(); //(=> Blue, Red)

                    for (int j = 0; j < e.MagParamNum; j++)
                    {
                        MagParamType type = (MagParamType)p.ReadUInt32();
                        uint valueBlue = p.ReadUInt32();
                        e.Blues.ChangeValue(type, valueBlue);
                    }

                    uint OptType = p.ReadUInt8(); //(1 => Socket)
                    uint OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.NParam1 = p.ReadUInt32(); //(=> Reference to Socket)

                    }
                    OptType = p.ReadUInt8(); //(2 => Advanced elixir)
                    OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.OptValue = p.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")

                    }
                    //Notice for Advanced Elixir modding.
                    //Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
                    //You also have to fix error when "Buy back" from NPC
                    //## Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater::DoWork_AddBindingOption 1366
                    //Storage Operation Failed!!! [OperationType: 34, ErrorCode: 174627]
                    //Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
                    //AQ Failed! Log out!! [AQType: 1]			
                    return e;
                }
                else if (value.Type.IsAttributeStone() || value.Type.IsMagicStone() || value.Type == Pk2ItemType.MagicStoneMall)
                {
                    item.StackCount = p.ReadUInt16();

                    // enhancement blues dont have assimilation probability
                    if (item.Type != Pk2ItemType.MagicStoneMall && item.Type != Pk2ItemType.MagicStoneSteady && item.Type != Pk2ItemType.MagicStoneLuck)
                    {
                        item.AttributeAssimilationProbability = p.ReadUInt8(); // -- probability not exposed in all silkroad versions
                    }
                    return item;
                }
                else if (value.Type == Pk2ItemType.GrowthPet)
                {
                    PetScroll pet = new PetScroll(item);
                    pet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)

                    if (pet.Status != PetStatus.Unsummoned)
                    {
                        pet.UniqueID = p.ReadUInt32();
                        pet.Name = p.ReadAscii();
                        pet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return pet;
                }
                else if (value.Type == Pk2ItemType.AbilityPet)
                {
                    AbilityPet abilityPet = new AbilityPet(item);
                    abilityPet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                    if (abilityPet.Status != PetStatus.Unsummoned)
                    {
                        abilityPet.UniqueID = p.ReadUInt32();
                        //abilityPet.NameLength = p.ReadUInt16();
                        abilityPet.Name = p.ReadAscii();
                        abilityPet.SecondsToRentEndTime = p.ReadUInt32();
                        abilityPet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return abilityPet;
                }
                else if (value.Type == Pk2ItemType.ItemExchangeCoupon)
                {
                    item.StackCount = p.ReadUInt16();
                    uint MagParamNum = p.ReadUInt8();

                    for (int i = 0; i < MagParamNum; i++)
                    {
                        ItemExchangeCoupon iec = new ItemExchangeCoupon(item);
                        iec.MagParamValues.Add(p.ReadUInt64());
                        //1. MagParam => CouponRefItemID [fixed]
                        //2. MagParam => CouponItemAmount [fixed]
                        //When Coupon holds Scrolls or similar, these 2 MagParams above are used.
                        //When Coupon holds Equipment, 8 MagParams are used
                        //They are defined in database by "[BIIV]<M:str,1,3><M:int,1,3><O:3>"
                        //As MagParams we get those 2 above and
                        //01 4D 01 72 74 73 00 00                           MagParam3 - .M.rts..........	(=> There is our str, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam4 - MagParam.Value		(=> Amount of +STR)
                        //01 4D 01 74 6E 69 00 00                           MagParam5 - .M.tni..........	(=> There is our int, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam6 - MagParam.Value		(=>	Amount of +INT)
                        //01 4F 00 00 00 00 00 00                           MagParam7 - .O..............	(=> There is our O for OptLevel)
                        //03 00 00 00 00 00 00 00                           MagParam8 - OptLevel			(=> Amount of +Overall)
                    }
                    return item;
                }
                else if (value.Type == Pk2ItemType.MagicCube)
                {
                    MagicCube magicCube = new MagicCube(item);
                    magicCube.StoredItemCount = p.ReadUInt32();
                    return magicCube;
                }
                else
                {
                    item.StackCount = p.ReadUInt16();
                    return item;
                }
            }
            else
            {
                return item;
            }
        }
    }
}
