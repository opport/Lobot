﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Storage
{
    public class StorageInfoEndHandler : Handler
    {
        public override ushort ServerOpcode => 0x3048;

        public StorageInfoEndHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            // no info on packet so far
            return null;
        }
    }
}
