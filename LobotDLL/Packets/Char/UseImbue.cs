﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    // cast fire imbue
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //04                                                type ? buff
    //7C 00 00 00                                       refID
    //00                                                targetted ?

    //[S -> C][B074]
    //01                                                confirmation flag
    //01                                                secondary cast when in range ?
    public class UseImbue : ObjectAction
    {
        uint ImbueRefID;

        public UseImbue(uint refID) : base()
        {
            ImbueRefID = refID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt8(0x01);
            p.WriteUInt8(0x04); // object action type ?
            p.WriteUInt32(ImbueRefID);
            p.WriteUInt8(0x00);
            return p;
        }
    }
}
