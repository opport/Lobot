﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Char
{
    /// <summary>
    /// //Opcode: 0x303D
    //Name: SERVER_AGENT_CHARACTER_STATS
    //Description:
    //Encryption: false
    //Massive: false
    //public const ushort SERVER_AGENT_CHARACTER_STATS = 0x303D;
    //	4	uint	PhyAtkMin
    //	4	uint	PhyAtkMax
    //	4	uint	MagAtkMin
    //	4	uint	MagAtkMax
    //	2	ushort	PhyDef
    //	2	ushort	MagDef
    //	2	ushort	HitRate
    //	2	ushort	ParryRate
    //	4	uint	MaxHP
    //	4	uint	MaxMP
    //	2	ushort	STR
    //	2	ushort	INT
    /// </summary>
    public class CharInfoHandler : Handler
    {
        public static event EventHandler ParsedCharInfo = delegate { };

        public override ushort ServerOpcode => 0x303D;

        public CharInfoHandler() { }

        protected sealed override ICommand ParsePacket(Packet p)
        {
            Bot.CharInfo.PhyAtkMin = p.ReadUInt32();
            Bot.CharInfo.PhyAtkMax = p.ReadUInt32();
            Bot.CharInfo.MagAtkMin = p.ReadUInt32();
            Bot.CharInfo.MagAtkMax = p.ReadUInt32();
            Bot.CharInfo.PhyDef = p.ReadUInt16();
            Bot.CharInfo.MagDef = p.ReadUInt16();
            Bot.CharInfo.HitRate = p.ReadUInt16();
            Bot.CharInfo.ParryRate = p.ReadUInt16();
            Bot.CharInfo.MaxHP = p.ReadUInt32();
            Bot.CharInfo.MaxMP = p.ReadUInt32();
            Bot.CharInfo.STR = p.ReadUInt16();
            Bot.CharInfo.INT = p.ReadUInt16();

            OnParsedCharInfo(EventArgs.Empty);
            return null;
        }

        private static void OnParsedCharInfo(EventArgs e)
        {
            EventHandler handler = ParsedCharInfo;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
