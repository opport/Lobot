﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[S -> C][3036]
    //3D 3F 93 06                                       uniqueID
    //10                                                unkn
    public class PickupAnimation : Handler
    {
        public PickupAnimation() { }

        public override ushort ServerOpcode => 0x3036;

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();
            byte unkn = p.ReadUInt8();
            return null;
        }
    }
}
