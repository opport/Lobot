﻿using LobotDLL.Packets.Char;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    // Cast anti devil bow
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //04                                                type
    //47 00 00 00                                       skillRefID
    //01                                                targetted ?
    //F3 17 A5 01                                       targetID

    //[S -> C][B074]
    //01                                                confirmation flag
    //01                                                secondary cast when in range ?

    // Start autoattack
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //01                                                type ? attack
    //01                                                targetted flag?
    //F3 17 A5 01                                       uniqueId

    //[S -> C][B074]
    //01                                                confirmation flag
    //01                                                secondary cast when in range ?
    public class UseSkill : ObjectAction
    {
        uint SkillRefID;
        ObjectActionType Type;
        uint TargetID;

        public UseSkill(uint refID, ObjectActionType type, uint targetID = 0x00) : base()
        {
            SkillRefID = refID;
            Type = type;
            TargetID = targetID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt8(0x01);

            switch (Type)
            {
                case ObjectActionType.AutoAttack:
                    p.WriteUInt8(0x01);
                    p.WriteUInt8(0x01); // targetted ?
                    p.WriteUInt32(TargetID);
                    break;
                case ObjectActionType.UseSkill:
                    p.WriteUInt8(0x04); // object action type ?
                    p.WriteUInt32(SkillRefID);

                    if (TargetID != 0x00)
                    {
                        p.WriteUInt8(0x01);
                        p.WriteUInt32(TargetID);
                    }
                    else
                    {
                        p.WriteUInt8(0x00);
                    }
                    break;
                default:
                    break;
            }

            return p;
        }
    }
}
