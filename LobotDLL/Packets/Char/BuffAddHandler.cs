﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Structs;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Char
{
    //Add buff fire shield pheonix
    //[S->C][B0BD]
    //54 18 11 00                                       casterID
    //7C 00 00 00                                       buffID 127 -> fire shield pheonix
    //CC C2 04 00                                       buff uniqueID

    // target
    //[C -> S][7045]
    //61 36 A4 00                                       a6..............

    // successful slumber on monster
    //[S -> C][B0BD]
    //61 36 A4 00                                       targetUniqueID
    //70 2B 00 00                                       skill refID -> 
    //00 00 00 00                                       ................

    //unsuccessful slumber
    //[S -> C][B0BD]
    //61 36 A4 00                                       targetUniqueID
    //70 2B 00 00                                       skill refID
    //00 00 00 00                                       ................

    //// warlock dot SKILL_EU_WARLOCK_DOTA_POISON
    //[S->C][B0BD]
    //47 58 A4 00                                       uniqueID target monster kokuro
    //D9 29 00 00                                       10713 SKILL_EU_WARLOCK_DOTA_POISON
    //3E 44 00 00                                       unkn
    //60 09 00 00                                       unkn

    // dot end ?
    //[S -> C][B072]
    //01                                                number of buffs ended
    //12 45 00 00                                       unkn
    public class BuffAddHandler : Handler
    {
        public static event EventHandler<BuffArgs> BuffAdded = delegate { };

        private static void OnBuffAdded(BuffArgs e)
        {
            EventHandler<BuffArgs> handler = BuffAdded;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0xB0BD;

        public BuffAddHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint targetID = p.ReadUInt32();

            Buff buff = new Buff(p.ReadUInt32());
            buff.TargetID = targetID;
            buff.UniqueID = p.ReadUInt32();

            // 0 = resist or debuff (which is handled in 3057)
            if (buff.UniqueID != 0)
            {
                if (targetID == Bot.CharData.UniqueID)
                {
                    Bot.CharData.ActiveBuffCount++;
                    Bot.CharData.ActiveBuffs.Add(buff);

                    if (buff.Type == LobotAPI.PK2.Pk2SkillType.Debuff)
                    {
                        AutoPotions.TryCureDebuff();
                    }

                    OnBuffAdded(new BuffArgs(buff.UniqueID, buff.RefSkillID, targetID));
                }
                else if (SpawnListsGlobal.TryGetSpawnObject(buff.UniqueID, out ISpawnType spawn))
                {
                    (spawn as Monster).Buffs.Add(buff);
                    SpawnListsGlobal.SpawnBuffs.Add(buff.UniqueID, spawn.UniqueID);
                }
            }

            return null;
        }
    }
}
