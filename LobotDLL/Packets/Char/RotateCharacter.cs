﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[C -> S][7024]
    //78 1B                                             new direction angle

    //[S->C][B024]
    //12 B7 16 01                                       uniqueID
    //78 1B                                             new angle
    public class RotateCharacter : BidirectionalPacket
    {
        private ushort NewAngle;

        public override ushort Opcode => 0x7024;
        public override ushort ServerOpcode => 0xB024;

        public RotateCharacter() { }
        public RotateCharacter(ushort newAngle) { NewAngle = newAngle; }

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();

            if (uniqueID == Bot.CharData.UniqueID)
            {
                ushort newAngle = (ushort)(p.ReadUInt16() * 360 / 65536);
                Bot.CharData.Angle = newAngle;
            }
            return null;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            ushort convertedAngle = (ushort)(NewAngle * 65536 / 360);
            p.WriteUInt16(convertedAngle);
            return p;
        }
    }
}
