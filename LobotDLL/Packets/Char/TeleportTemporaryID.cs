﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{

    //[S -> C][3020]
    //8B E4 74 01                                       temporaryID
    //B4 01                                             unkn0
    //08                                                unkn1
    //19                                                unkn2
    public class TeleportTemporaryID : Handler
    {
        public TeleportTemporaryID() { }

        public override ushort ServerOpcode => 0x3020;

        protected override ICommand ParsePacket(Packet p)
        {
            uint tempID = p.ReadUInt32();
            ushort unkn0 = p.ReadUInt16();
            byte unkn1 = p.ReadUInt8();
            byte unkn2 = p.ReadUInt8();

            return null;
        }
    }
}
