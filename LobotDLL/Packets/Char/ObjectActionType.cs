﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobotDLL.Packets.Char
{
    public enum ObjectActionType : byte
    {
        AutoAttack = 0x01,
        UseSkill = 0x04
    }
}
