﻿using LobotAPI;
using LobotAPI.Globals;

using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace LobotDLL.Packets.Char
{
    //[S -> C][B070]
    //01                                                success
    //02 30                                             type
    //87 02 00 00                                       skillID
    //C3 1B 7F 01                                       casterID
    //7E AF 02 00                                       skillUniqueID
    //AA 21 A2 00                                       targetID
    //00                                                instantResponse? 

    //[S -> C] [B071]
    //01                                                success
    //7E AF 02 00                                       skillUniqueID
    //AA 21 A2 00                                       targetID
    //01                                                hasDamage
    //01                                                hitCount
    //01                                                targetCount
    //AA 21 A2 00                                       targetID
    //00                                                skill effect (bitmask separated kibibyte values)
    //01 FA 04 00                                       critStatus (byte) damage (3 bytes including 1 byte of next line)
    //00 00 00 00                                       damage (1 byte), unkn, unkn, unkn

    // Ghost Walk Phantom
    //[S -> C][B070]
    //01                                                successFlag // 1 = success
    //02 30                                             unkn 12290
    //08 05 00 00                                       skillID 1288 Ghost-Walk Phantom 3 SKILL_CH_LIGHTNING_GYEONGGONG
    //31 27 86 00                                       casterID
    //AE 7F 00 00                                       unkn
    //00 00 00 00                                       targetID 0 == none
    //08                                                type = 8 = movement
    //8C 5C xsector ySector
    //9E 02 00 00 E8 FF FF FF F5 06 00 00               x 2    z 4    y 4
    public abstract class SkillHandler : Handler
    {
        private const int STAND_UP_DELAY = 3500;

        protected SkillHandler() { }

        private static async Task ResetKnockDown(uint targetID)
        {
            await Task.Delay(STAND_UP_DELAY).ContinueWith(task =>
            {
                if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn)
                    && spawn is ICombatType)
                {
                    (spawn as ICombatType).IsKnockedDown = false;
                }
            });
        }

        protected static void ParseDamage(Packet p)
        {
            try
            {
                byte hasDamage = p.ReadUInt8();

                if (hasDamage == 1)
                {
                    byte hitcount = p.ReadUInt8();
                    byte targetCount = p.ReadUInt8();

                    for (int i = 0; i < targetCount; i++)
                    {
                        uint targetID = p.ReadUInt32();

                        for (int ii = 0; ii < hitcount; ii++)
                        {
                            byte effects = p.ReadUInt8();  // bitmask --> ATTACK_STATE_KNOCKBACK = 0x01,    ATTACK_STATE_BLOCK = 0x02,    ATTACK_STATE_POSITION = 0x04,   abort ? --> 0x08   ATTACK_STATE_DEAD = 0x80

                            if ((effects & 0x0A) != 0x00)  // if ATTACK_STATE_BLOCK or abort --> no more data
                            {
                                continue;
                            }

                            byte critStatus = p.ReadUInt8(); // bitmask --> DAMAGE_STATE_NORMAL = 0x01,    DAMAGE_STATE_CRIT = 0x02,    DAMAGE_STATE_STATUS = 0x10

                            uint dmg = p.ReadUInt32();
                            byte unk4 = p.ReadUInt8();
                            byte unk5 = p.ReadUInt8();
                            byte unk6 = p.ReadUInt8();

                            if (effects == 0x04 || effects == 0x09) // knockdown or knockback
                            {
                                if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn))
                                {
                                    if (effects == 0x04 && spawn is ICombatType) // assign knockdown status
                                    {
                                        (spawn as ICombatType).IsKnockedDown = true;
                                        ResetKnockDown(targetID).ConfigureAwait(false);
                                    }

                                    byte regionX = p.ReadUInt8();
                                    byte regionY = p.ReadUInt8();
                                    float x = p.ReadSingle();
                                    float z = p.ReadSingle();
                                    float y = p.ReadSingle();

                                    spawn.Position = new Point(ScriptUtils.GetXCoord(x, regionX), ScriptUtils.GetYCoord(y, regionY));
                                }
                            }
                        }
                    }
                }
                else if (hasDamage == 8) // Teleport Movement
                {
                    byte xSector = p.ReadUInt8();
                    byte ySector = p.ReadUInt8();
                    uint x = p.ReadUInt32();
                    uint z = p.ReadUInt32();
                    uint y = p.ReadUInt32();

                    Bot.CharData.Position = new Point(ScriptUtils.GetXCoord(x, xSector), ScriptUtils.GetYCoord(y, ySector));
                    OnTeleportSkillUsed(new CharMoveArgs(Bot.CharData.UniqueID, Bot.CharData.Position));
                }
            }
            catch (System.Exception)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("SkillHandler error: {0:x}", p.Opcode));
                Logger.Log(LogLevel.NETWORK, string.Format("SkillHandler error {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
            }
        }

        public static event EventHandler<CharMoveArgs> TeleportSkillUsed = delegate { };

        private static void OnTeleportSkillUsed(CharMoveArgs e)
        {
            EventHandler<CharMoveArgs> handler = TeleportSkillUsed;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}
