﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using SilkroadSecurityApi;
using System;
using System.Windows;

namespace LobotDLL.Packets.Char
{
    /// <summary>
    /// The packet signalling that the character has (been) stopped.
    /// This includes: Collision with environment, picking up items and any action that requiress walking to stop.
    //[S -> C][B023]
    //83 5C 5D 05                                       uniqueID
    //8A 5A                                             xSector, ySector
    //3E 73 86 42 4F 6F 46 40 0C 5C EF 44 4B 35         x 4f, z 4f, y 4f, angle
    /// </summary>
    public class CharStopMovementHandler : Handler
    {
        public static event EventHandler<CharStopMovementArgs> CharStopMovement = delegate { };

        protected static void OnStopMovementRegistered(CharStopMovementArgs e)
        {
            EventHandler<CharStopMovementArgs> handler = CharStopMovement;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0xB023;

        public CharStopMovementHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();

            if (uniqueID == Bot.CharData.UniqueID)
            {
                byte xSector = p.ReadUInt8();
                byte ySector = p.ReadUInt8();
                float x = p.ReadSingle();
                float z = p.ReadSingle();
                float y = p.ReadSingle();
                Bot.CharData.Angle = (ushort)(p.ReadUInt16() * 360 / 65536);
                Bot.CharData.Position = new Point(ScriptUtils.GetXCoord(x, xSector), ScriptUtils.GetYCoord(y, ySector));

            }

            OnStopMovementRegistered(new CharStopMovementArgs(uniqueID));

            return null;
        }
    }
}
