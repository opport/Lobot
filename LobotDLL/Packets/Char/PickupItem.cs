﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LobotDLL.Connection;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //02                                                type ? 
    //01                                                amount?
    //1E D4 AA 01                                       uniqueItemID

    //[S -> C] [B074]
    //01                                                initiation confirmation
    //01                                                flag

    //[S -> C] [B074]
    //02                                                action phase flag // 1 = start 2 = cancel/complete
    //00                                                flag
    public class PickupItem : ObjectAction
    {
        uint UniqueItemID;

        public PickupItem(uint uniqueID) : base()
        {
            UniqueItemID = uniqueID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt8(0x01);
            p.WriteUInt8(0x02); // object action type ?
            p.WriteUInt8(0x01);
            p.WriteUInt32(UniqueItemID);
            return p;
        }
    }
}
