﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Linq;
using System.Windows;
using static LobotAPI.Structs.Item;
using static LobotAPI.Structs.Quest;

namespace LobotDLL.Packets.Char
{
    public class CharDataHandler : Handler
    {
        public static event EventHandler ParsedCharData = delegate { };

        public override ushort ServerOpcode => 0x3013;

        public CharDataHandler() { }

        private static void OnParsedCharData(EventArgs e)
        {
            EventHandler handler = ParsedCharData;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private Item ParsePrivateInventory(ref Packet p)
        {
            Item item = new Item();
            item.Slot = p.ReadUInt8();
            item.RentType = p.ReadUInt32();

            switch (item.RentType)
            {
                case 1:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    break;

                case 2:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.MeterRateTime = p.ReadUInt32();
                    break;

                case 3:
                    item.Rent.CanDelete = p.ReadUInt16(); // (adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.PackingTime = p.ReadUInt32();
                    break;
            }

            item.RefItemID = p.ReadUInt32();

            if (PK2DataGlobal.Items == null)
            {
                PK2Utils.ExtractAllInformation();
            }

            if (PK2DataGlobal.Items.TryGetValue(item.RefItemID, out Pk2Item value))
            {
                string valueType = value.Type.ToString("X").Trim('0');

                if (value.IsEquipmentItem()) // Equipment
                {
                    Equipment e = new Equipment(item);
                    e.OptLevel = p.ReadUInt8();
                    e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.ReadUInt64());
                    e.Durability = p.ReadUInt32(); //(=> Durability)
                    e.MagParamNum = p.ReadUInt8(); //(=> Blue, Red)

                    for (int j = 0; j < e.MagParamNum; j++)
                    {
                        MagParamType type = (MagParamType)p.ReadUInt32();
                        uint valueBlue = p.ReadUInt32();
                        e.Blues.ChangeValue(type, valueBlue);
                    }

                    uint OptType = p.ReadUInt8(); //(1 => Socket)
                    uint OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.NParam1 = p.ReadUInt32(); //(=> Reference to Socket)

                    }
                    OptType = p.ReadUInt8(); //(2 => Advanced elixir)
                    OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.OptValue = p.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")

                    }
                    //Notice for Advanced Elixir modding.
                    //Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
                    //You also have to fix error when "Buy back" from NPC
                    //## Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater::DoWork_AddBindingOption 1366
                    //Storage Operation Failed!!! [OperationType: 34, ErrorCode: 174627]
                    //Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
                    //AQ Failed! Log out!! [AQType: 1]			
                    return e;
                }
                else if (value.Type.IsAttributeStone() || value.Type.IsMagicStone() || value.Type == Pk2ItemType.MagicStoneMall)
                {
                    item.StackCount = p.ReadUInt16();

                    // enhancement blues dont have assimilation probability
                    if (item.Type != Pk2ItemType.MagicStoneMall && item.Type != Pk2ItemType.MagicStoneSteady && item.Type != Pk2ItemType.MagicStoneLuck)
                    {
                        item.AttributeAssimilationProbability = p.ReadUInt8(); // -- probability not exposed in all silkroad versions
                    }
                    return item;
                }
                else if (value.Type == Pk2ItemType.GrowthPet)
                {
                    LobotAPI.Structs.PetScroll pet = new LobotAPI.Structs.PetScroll(item);
                    pet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)

                    if (pet.Status != PetStatus.Unsummoned)
                    {
                        pet.UniqueID = p.ReadUInt32();
                        pet.Name = p.ReadAscii();
                        pet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return pet;
                }
                else if (value.Type == Pk2ItemType.AbilityPet)
                {
                    AbilityPet abilityPet = new AbilityPet(item);
                    abilityPet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                    if (abilityPet.Status != PetStatus.Unsummoned)
                    {
                        abilityPet.UniqueID = p.ReadUInt32();
                        //abilityPet.NameLength = p.ReadUInt16();
                        abilityPet.Name = p.ReadAscii();
                        abilityPet.SecondsToRentEndTime = p.ReadUInt32();
                        abilityPet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return abilityPet;
                }
                else if (value.Type == Pk2ItemType.ItemExchangeCoupon)
                {
                    item.StackCount = p.ReadUInt16();
                    uint MagParamNum = p.ReadUInt8();

                    for (int i = 0; i < MagParamNum; i++)
                    {
                        ItemExchangeCoupon iec = new ItemExchangeCoupon(item);
                        iec.MagParamValues.Add(p.ReadUInt64());
                        //1. MagParam => CouponRefItemID [fixed]
                        //2. MagParam => CouponItemAmount [fixed]
                        //When Coupon holds Scrolls or similar, these 2 MagParams above are used.
                        //When Coupon holds Equipment, 8 MagParams are used
                        //They are defined in database by "[BIIV]<M:str,1,3><M:int,1,3><O:3>"
                        //As MagParams we get those 2 above and
                        //01 4D 01 72 74 73 00 00                           MagParam3 - .M.rts..........	(=> There is our str, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam4 - MagParam.Value		(=> Amount of +STR)
                        //01 4D 01 74 6E 69 00 00                           MagParam5 - .M.tni..........	(=> There is our int, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam6 - MagParam.Value		(=>	Amount of +INT)
                        //01 4F 00 00 00 00 00 00                           MagParam7 - .O..............	(=> There is our O for OptLevel)
                        //03 00 00 00 00 00 00 00                           MagParam8 - OptLevel			(=> Amount of +Overall)
                    }
                    return item;
                }
                else if (value.Type == Pk2ItemType.MagicCube)
                {
                    MagicCube magicCube = new MagicCube(item);
                    magicCube.StoredItemCount = p.ReadUInt32();
                    return magicCube;
                }
                else
                {
                    item.StackCount = p.ReadUInt16();
                    return item;
                }
            }
            else
            {
                return item;
            }
        }

        protected sealed override ICommand ParsePacket(Packet p)
        {
            p.Lock();

            Bot.CharData.ServerTime = p.ReadUInt32();
            Bot.CharData.RefID = p.ReadUInt32();
            Bot.CharData.Scale = p.ReadUInt8();
            Bot.CharData.Level = p.ReadUInt8();
            Bot.CharData.MaxLevel = p.ReadUInt8();
            Bot.CharData.ExpOffset = p.ReadUInt64();
            Bot.CharData.SExpOffset = p.ReadUInt32();
            Bot.CharData.RemainGold = p.ReadUInt64();
            Bot.CharData.RemainSkillPoint = p.ReadUInt32();
            Bot.CharData.RemainStatPoint = p.ReadUInt16();
            Bot.CharData.RemainHwanCount = p.ReadUInt8();
            Bot.CharData.GatheredExpPoint = p.ReadUInt32();
            Bot.CharData.CurrentHP = p.ReadUInt32();
            Bot.CharData.CurrentMP = p.ReadUInt32();
            Bot.CharData.AutoInverstExp = p.ReadUInt8(); //(1 = Beginner Icon, 2 = Helpful, 3 = Beginner&Helpful)
            Bot.CharData.DailyPK = p.ReadUInt8();
            Bot.CharData.TotalPK = p.ReadUInt16();
            Bot.CharData.PKPenaltyPoint = p.ReadUInt32();
            Bot.CharData.HwanLevel = p.ReadUInt8();
            Bot.CharData.FreePVP = p.ReadUInt8(); //0 = None, 1 = Red, 2 = Gray, 3 = Blue, 4 = White, 5 = Gold

            Bot.CharData.Inventory.Size = p.ReadUInt8();
            Bot.CharData.Inventory.ItemCount = p.ReadUInt8();

            // Reset inventory
            CharInfoGlobal.Inventory.AsCollection().Clear();
            for (int i = 0; i < Bot.CharData.Inventory.ItemCount; i++)
            {
                Item item = ParsePrivateInventory(ref p);
                CharInfoGlobal.Inventory[item.Slot] = item;
            }

            Bot.CharData.Inventory.AvatarInventory.Size = p.ReadUInt8();
            Bot.CharData.Inventory.AvatarInventory.ItemCount = p.ReadUInt8();
            for (int i = 0; i < Bot.CharData.Inventory.AvatarInventory.ItemCount; i++)
            {
                Item item = ParsePrivateInventory(ref p);
                CharInfoGlobal.AvatarInventory[item.Slot] = item;
            }

            Bot.CharData.HasMask = p.ReadUInt8();  // -> Check for != 0 (MaskFlag?)

            Bot.CharData.MasteryFlag = p.ReadUInt8(); // [0 = done, 1 = Mastery]
            while (Bot.CharData.MasteryFlag == 1)
            {
                Mastery m = new Mastery();
                m.ID = p.ReadUInt32();
                m.Level = p.ReadUInt8();
                Bot.CharData.Masteries[m.ID] = m;
                Bot.CharData.MasteryFlag = p.ReadUInt8(); // (0 = done, 1 = Mastery)
            }

            p.ReadUInt8(); // Mastery end byte
            Bot.CharData.SkillFlag = p.ReadUInt8(); // [0 = done, 1 = Skill]

            while (Bot.CharData.SkillFlag == 1)
            {
                Skill s = new Skill(p.ReadUInt32());
                s.SkillEnabled = p.ReadUInt8();

                // Add distinct skills
                if (!CharInfoGlobal.Skills.Contains(s))
                {
                    // replace old version of skill if found
                    Skill lesserExistingSkill = CharInfoGlobal.Skills.FirstOrDefault(existingSkill => existingSkill.Name.Equals(s.Name) && existingSkill.RefSkillID < s.RefSkillID);

                    if (lesserExistingSkill != null)
                    {
                        CharInfoGlobal.Skills[CharInfoGlobal.Skills.IndexOf(lesserExistingSkill)] = s;
                    }
                    else
                    {
                        Bot.CharData.Skills.Add(s);
                        CharInfoGlobal.Skills.InsertSorted(s, CharInfoGlobal.SkillComparison);
                    }
                }
                Bot.CharData.SkillFlag = p.ReadUInt8(); // (0 = done, 1 = Skill)
            }

            //TODO unknown 
            //byte unknown = p.ReadUInt8();

            Bot.CharData.CompletedQuestCount = p.ReadUInt16();
            for (int i = 0; i < Bot.CharData.CompletedQuestCount; i++)
            {
                Bot.CharData.CompletedQuest.Add(p.ReadUInt32());
            }

            Bot.CharData.ActiveQuestCount = p.ReadUInt8();
            for (int i = 0; i < Bot.CharData.ActiveQuestCount; i++)
            {
                Quest q = new Quest();
                q.RefID = p.ReadUInt32();
                q.AchievementCount = p.ReadUInt8(); // (Repetition Amount = Bit && Completetion Amount = Bit)
                q.RequiresSharePt = p.ReadUInt8(); // -> Check for != 0
                q.Type = p.ReadUInt8(); // (8 = , 24 = , 28 = , 88 = )

                if (q.Type == 28)
                {
                    uint remainingTime = p.ReadUInt32();
                }

                q.Status = p.ReadUInt8(); // (1 = Untouched, 7 = Started, 8 = Complete)

                if (q.Type != 8)
                {
                    q.ObjectiveCount = p.ReadUInt8();

                    for (int objectiveindex = 0; objectiveindex < q.ObjectiveCount; objectiveindex++)
                    {
                        Objective o = new Objective();
                        o.ID = p.ReadUInt8();
                        o.Status = p.ReadUInt8(); // (00 = done, 01 = incomplete)
                        //o.NameLength = p.ReadUInt16();
                        o.Name = p.ReadAscii();
                        o.TaskCount = p.ReadUInt8();

                        for (int k = 0; k < o.TaskCount; k++)
                        {
                            o.Tasks.Add(p.ReadUInt32()); // (=> Killed monsters; Collected items)
                        }

                        q.Objectives.Add(o);
                    }
                }

                if (q.Type == 88)
                {
                    q.TaskCount = p.ReadUInt8();

                    for (int j = 0; j < q.TaskCount; j++)
                    {
                        q.TaskRefObjID.Add(p.ReadUInt32()); // (=> NPCs to deliver to, when complete you get reward)
                    }
                }

                Bot.CharData.ActiveQuests.Add(q);
            }

            Bot.CharData.unk05 = p.ReadUInt8();  // -> Check for != 0

            Bot.CharData.CollectionBookCount = p.ReadUInt32();  // -> Check for != 0
            for (int i = 0; i < Bot.CharData.CollectionBookCount; i++)
            {
                uint themeIndex = p.ReadUInt32();
                uint themeStartedDateTime = p.ReadUInt32();  //SROTimeStamp
                uint themePages = p.ReadUInt32();
            }

            Bot.CharData.UniqueID = p.ReadUInt32();
            Bot.CharData.XSec = p.ReadUInt8();
            Bot.CharData.YSec = p.ReadUInt8();  //(=> XSec; YSec)
            Bot.CharData.XOffset = p.ReadSingle();
            Bot.CharData.ZOffset = p.ReadSingle();
            Bot.CharData.YOffset = p.ReadSingle();
            Bot.CharData.Angle = (ushort)(p.ReadUInt16() * 360 / 65536);
            Bot.CharData.Position = new Point(ScriptUtils.GetXCoord(Bot.CharData.XOffset, Bot.CharData.XSec), ScriptUtils.GetYCoord(Bot.CharData.YOffset, Bot.CharData.YSec));

            Bot.CharData.HasDestination = p.ReadUInt8();
            Bot.CharData.MovementType = p.ReadUInt8(); //(0 = Walking, 1 = Running)
            if (Bot.CharData.HasDestination == 1)
            {
                Bot.CharData.DestXSec = p.ReadUInt8();
                Bot.CharData.DestYSec = p.ReadUInt8();

                if (Bot.CharData.IsInCave == false) // In world
                {
                    Bot.CharData.DestX = p.ReadUInt16();
                    Bot.CharData.DestZ = p.ReadUInt16();
                    Bot.CharData.DestY = p.ReadUInt16();
                }
                else // In cave
                {
                    Bot.CharData.DestX = p.ReadUInt32();
                    Bot.CharData.DestZ = p.ReadUInt32();
                    Bot.CharData.DestY = p.ReadUInt32();
                }

            }
            else
            {
                Bot.CharData.SkyClickFlag = p.ReadUInt8(); // (1 = Sky-/ArrowKey-walking)
                Bot.CharData.Angle = (ushort)(p.ReadUInt16() * 360 / 65536);
            }
            Bot.CharData.IsAlive = p.ReadUInt8() != 0x02; // now anything != 2 seems to be alive --- OLD(1 = Alive, 2 = Dead)
            Bot.CharData.DebuffState = (DebuffStatus)p.ReadUInt8(); // -> Check for != 0
            Bot.CharData.MotionState = p.ReadUInt8(); // (0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
            Bot.CharData.Status = (ObjectStatus)p.ReadUInt8(); //(0 = None,2 = ??@GrowthPet, 3 = Invincible, 4 = Invisible)
            Bot.CharData.WalkSpeed = p.ReadSingle();
            Bot.CharData.RunSpeed = p.ReadSingle();
            Bot.CharData.HwanSpeed = p.ReadSingle();
            Bot.CharData.ActiveBuffCount = p.ReadUInt8();

            for (int i = 0; i < Bot.CharData.ActiveBuffCount; i++)
            {
                Buff b = new Buff(p.ReadUInt32());
                b.BuffDuration = p.ReadUInt32();
                //b.RefSkillParam = p.ReadUInt32();
                if (b.Pk2Entry.IsTransferableBuff()) //b.RefSkillParam is 1701213281) // -> atfe -> "auto transfer effect" like Recovery Division)
                {
                    b.IsCreator = p.ReadUInt8();
                }
            }

            //Bot.CharData.NameLength = p.ReadUInt16();
            Bot.CharData.Name = p.ReadAscii();
            //Bot.CharData.JobNameLength = p.ReadUInt16();
            Bot.CharData.JobName = p.ReadAscii();
            Bot.CharData.JobType = p.ReadUInt8(); // (0 = None, 1 = Trader, 2 = Thief, 3 = Hunter)	
            Bot.CharData.JobLevel = p.ReadUInt8();
            Bot.CharData.JobExp = p.ReadUInt32();
            Bot.CharData.JobContribution = p.ReadUInt32();
            Bot.CharData.JobReward = p.ReadUInt32();
            Bot.CharData.PVPState = p.ReadUInt8(); // -> Check for != 0	(According to Spawn structure => MurderFlag 0 = White, 1 = Purple, 2 = Red)
            Bot.CharData.TransportFlag = p.ReadUInt8();// -> Check for != 0	(According to Spawn structure => RideFlag or AttackFlag?)
            Bot.CharData.InCombat = p.ReadUInt8(); // -> Check for != 0	(According to Spawn structure => EquipmentCountdown?)
            if (Bot.CharData.TransportFlag is 0x01)
            {
                Bot.CharData.TransportUniqueID = p.ReadUInt32();
            }
            Bot.CharData.PVPFlag = p.ReadUInt8(); // Flag(0 = Red Side, 1 = Blue Side, 255 = Disable(None), 34 = Enable)
            Bot.CharData.GuideFlag = p.ReadUInt64();
            Bot.CharData.AccountID = p.ReadUInt32(); //(=> GameAccountID) JID (Joymax ID)
            Bot.CharData.GMFlag = p.ReadUInt8();
            Bot.CharData.ActivationFlag = p.ReadUInt8(); //ConfigType:0 --> (0 = Not activated, 7 = activated)
            Bot.CharData.HotkeyCount = p.ReadUInt8();

            for (int i = 0; i < Bot.CharData.HotkeyCount; i++)
            {
                Hotkey h = new Hotkey();
                h.SlotSeq = p.ReadUInt8();
                h.SlotType = p.ReadUInt8(); //(37 = COS Command, 70 = InventoryItem, 71 = EquipedItem, 73 = Skill, 74 = Action, 78 = EquipedAvatar)
                h.Data = p.ReadUInt32();
                Bot.CharData.Hotkeys.Add(h);
            }
            Bot.CharData.HPSlot = p.ReadUInt8(); //(FValue  10 + Slot)
            Bot.CharData.HPValue = p.ReadUInt8(); //(Enabled = 128 + Value)
            Bot.CharData.MPSlot = p.ReadUInt8(); //(FValue  10 + Slot)
            Bot.CharData.MPValue = p.ReadUInt8();  //(Enabled = 128 + Value)
            Bot.CharData.UniversalSlot = p.ReadUInt8(); //(Enabled = 128 + Value) 
            Bot.CharData.UniversalValue = p.ReadUInt8();  //(Enabled = 128,0 = Disabled)
            Bot.CharData.PotionDelay = p.ReadUInt8();  //(Enabled = 128 + Value)

            Bot.CharData.BlockedPlayCount = p.ReadUInt8();

            for (int i = 0; i < Bot.CharData.BlockedPlayCount; i++)
            {
                BlockedPlayer b = new BlockedPlayer();

                //b.TargetNameLength = p.ReadUInt16();
                b.TargetName = p.ReadAscii();
                Bot.CharData.BlockedPlayers.Add(b);
            }
            Bot.CharData.unk13 = p.ReadUInt32();
            Bot.CharData.unk14 = p.ReadUInt8();

            Bot.Status = BotStatus.Idle;
            Bot.CharData.ActiveBuffs.Clear();
            OnParsedCharData(EventArgs.Empty);
            return null;
        }
    }

}
