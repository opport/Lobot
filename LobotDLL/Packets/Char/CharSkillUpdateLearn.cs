﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System.Linq;

namespace LobotDLL.Packets.Char
{
    //level up antidevil bow missile level 1
    //[C -> S] [70A1]
    //47 00 00 00                                       skillID 71

    //[S -> C] [B0A1]
    //01                                                flag success?
    //47 00 00 00                                       skillID 71 SKILL_CH_BOW_CRITICAL_A_01
    public class CharSkillUpdateLearn : BidirectionalPacket
    {
        private readonly uint SkillID;

        public override ushort ServerOpcode => 0xB0A1;

        public override ushort Opcode => 0x70A1;

        public CharSkillUpdateLearn() { }
        public CharSkillUpdateLearn(uint skillID) { SkillID = skillID; }

        protected sealed override ICommand ParsePacket(Packet p)
        {
            p.ReadUInt8(); //masteryLearnType success?
            Skill skill = new Skill(p.ReadUInt32());

            Skill existingSkill = CharInfoGlobal.Skills.FirstOrDefault(s => s.Name.Equals(skill.Name));

            // add new skill or replace existing skill
            if (existingSkill == null)
            {
                Bot.CharData.Skills.Add(skill);
                CharInfoGlobal.Skills.Add(skill);
            }
            else
            {
                int index = Bot.CharData.Skills.IndexOf(existingSkill);
                if (index > -1)
                {
                    Bot.CharData.Skills[index] = skill;
                }

                index = CharInfoGlobal.Skills.IndexOf(existingSkill);
                if (index > -1)
                {
                    CharInfoGlobal.Skills[index].RefSkillID = skill.RefSkillID;
                    Skill newSkill = CharInfoGlobal.Skills[index];

                    // Updated skill object in main skilllist should update all other references
                    CharInfoGlobal.Skills.RemoveAt(index);
                    CharInfoGlobal.Skills.Add(newSkill);
                }
            }

            return null;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(SkillID);
            return p;
        }
    }
}
