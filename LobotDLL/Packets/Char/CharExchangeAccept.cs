﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    public class CharExchangeAccept : Command
    {
        public override ushort Opcode => 0x7074;

        public CharExchangeAccept() { }

        protected override Packet AddPayload()
        {
            throw new NotImplementedException();
        }
    }
}
