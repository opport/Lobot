﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.AutoPotion;
using LobotAPI.Structs;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    //[S->C][3057]
    //B3 D7 79 01                                       uniqueID
    //04 00                                             unknw
    //02                                                type
    //60 0D 00 00                                       newValue

    //[S -> C] [3057]
    //99 CF 74 00                                       uniqueID
    //01 00                                             unknw
    //05                                                type 
    //31 16 00 00                                       1...............
    //00 00 00 00                                       ................

    // slumber
    //[C -> S][7045]
    //2A 04 E6 00                                       *...............

    //[S -> C][3057]
    //2A 04 E6 00                                       *...............
    //00 01                                             ................
    //05                                                ................
    //39 06 00 00                                       9...............
    //40 00 00 00                                       @...............
    //04                                                ................

    //[S -> C][3057]
    //2A 04 E6 00                                       *...............
    //00 01                                             ................
    //04                                                ................
    //40 00 00 00                                       @...............
    //04                                                ................
    public class HPMPUpdateHandler : Handler
    {
        /// <summary>
        /// Use a bitwise "xor" on this and the status uint --> (0x4000 ^ status)
        /// The result is either 
        /// Debuff > 0x4040
        /// Normal == (uint)BadStatus.Stun
        /// BadStatus < (uint)BadStatus.Stun 
        /// e.g.
        //Trap    0x1004000
        //Panic   0x204000
        //Bleed   0x4800

        //Stun    0x0000

        //Sleep   0x4040
        //Poison  0x4010
        //Normal  0x4000
        /// </summary>
        private static readonly uint BadStatusDebuffFilter = (uint)BadStatus.Stun;


        public static event EventHandler<StatusUpdateArgs> CharUpdated = delegate { };
        public static event EventHandler<StatusUpdateArgs> OtherUpdated = delegate { };

        protected void OnCharUpdate(StatusUpdateArgs e)
        {
            EventHandler<StatusUpdateArgs> handler = CharUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        protected void OnOtherUpdate(StatusUpdateArgs e)
        {
            EventHandler<StatusUpdateArgs> handler = OtherUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0x3057;

        public HPMPUpdateHandler() { }

        private static DebuffStatus GetDebuffStatus(uint statusFlags)
        {
            uint filterResult = BadStatusDebuffFilter ^ statusFlags;

            if (filterResult > 0x4040)
            {
                return DebuffStatus.Debuff;
            }
            else if (filterResult > (uint)BadStatus.Stun)
            {
                return DebuffStatus.BadStatus;
            }
            else
            {
                return DebuffStatus.Normal;
            }
        }

        private void UpdateCharHPMP(Packet p)
        {
            p.ReadUInt16(); // Unknown typeFlag
            HPMPUpdateType updateType = (HPMPUpdateType)p.ReadUInt8();
            uint status;

            switch (updateType)
            {
                case HPMPUpdateType.OnlyHP:
                    Bot.CharData.CurrentHP = p.ReadUInt32();

                    AutoPotions.TryUseHPPot();
                    AutoPotions.TryUseVigour();
                    break;
                case HPMPUpdateType.OnlyMP:
                    Bot.CharData.CurrentMP = p.ReadUInt32();

                    AutoPotions.TryUseMPPot();
                    AutoPotions.TryUseVigour();
                    break;
                case HPMPUpdateType.HPAndMP:
                    Bot.CharData.CurrentHP = p.ReadUInt32();
                    Bot.CharData.CurrentMP = p.ReadUInt32();

                    AutoPotions.TryUseHPPot();
                    AutoPotions.TryUseMPPot();
                    AutoPotions.TryUseVigour();
                    break;
                case HPMPUpdateType.BadStatus:
                    status = p.ReadUInt32(); // 0 = stop, 1 = fire (?), 2 = ice, 3 = freeze, 4 = electricity, 8 = fire, 16 = poison
                    Bot.CharData.DebuffState = GetDebuffStatus(status);

                    AutoPotions.TryCureBadStatusOrCureDebuff();
                    break;
                case HPMPUpdateType.HPAndBadStatusOrMonster:
                    Bot.CharData.CurrentHP = p.ReadUInt32();
                    status = p.ReadUInt32();
                    Bot.CharData.DebuffState = GetDebuffStatus(status);

                    AutoPotions.TryUseHPPot();
                    AutoPotions.TryUseVigour();
                    AutoPotions.TryCureBadStatusOrCureDebuff();
                    break;
                case HPMPUpdateType.MPAndBadStatus:
                    Bot.CharData.CurrentMP = p.ReadUInt32();
                    status = p.ReadUInt32();
                    Bot.CharData.DebuffState = GetDebuffStatus(status);

                    AutoPotions.TryUseMPPot();
                    AutoPotions.TryUseVigour();
                    AutoPotions.TryCureBadStatusOrCureDebuff();
                    break;
                default:
                    break;
            }

            OnCharUpdate(new StatusUpdateArgs(Bot.CharData.UniqueID, updateType, Bot.CharData.DebuffState, Bot.Status == 0));
        }

        private void UpdatePetHP(Packet p, uint objectID)
        {
            p.ReadUInt16(); // Unknown typeFlag
            HPMPUpdateType updateType = (HPMPUpdateType)p.ReadUInt8();

            if (objectID == Bot.Transport?.UniqueID)
            {
                if (updateType == HPMPUpdateType.HPAndBadStatusOrMonster)
                {
                    Bot.Transport.CurrentHP = p.ReadUInt32();
                    AutoPotions.TryUseTransportHPPot(objectID);
                }
            }
            else if (objectID == Bot.GrowthPet?.UniqueID)
            {
                if (updateType == HPMPUpdateType.HPAndBadStatusOrMonster)
                {
                    Bot.GrowthPet.CurrentHP = p.ReadUInt32();
                    AutoPotions.TryUsePetHPPot(objectID);
                }
                else if (updateType == HPMPUpdateType.BadStatus) // Attack Mode
                {
                    Bot.GrowthPet.BadStatus = p.ReadUInt8();
                    AutoPotions.TryCurePetBadStatus();
                }
            }
        }

        private void UpdateMonsterHP(Packet p, uint objectID)
        {
            p.ReadUInt16(); // Unknown typeFlag
            HPMPUpdateType updateType = (HPMPUpdateType)p.ReadUInt8();

            if (updateType == HPMPUpdateType.HPAndBadStatusOrMonster && SpawnListsGlobal.TryGetSpawnObject(objectID, out ISpawnType spawn))
            {
                (spawn as Monster).CurrentHP = p.ReadUInt32();
            }
        }

        protected override ICommand ParsePacket(Packet p)
        {
            uint objectID = p.ReadUInt32();

            if (Bot.CharData.UniqueID == objectID)
            {
                UpdateCharHPMP(p);
            }
            else if (SpawnListsGlobal.AllSpawnEntries.TryGetValue(objectID, out SpawnListsGlobal.BotLists botLists))
            {
                switch (botLists)
                {
                    case SpawnListsGlobal.BotLists.MonsterSpawns:
                        UpdateMonsterHP(p, objectID);
                        break;
                    case SpawnListsGlobal.BotLists.NPCs:
                        break;
                    case SpawnListsGlobal.BotLists.Portals:
                        break;
                    case SpawnListsGlobal.BotLists.Players:
                        //TODO
                        break;
                    case SpawnListsGlobal.BotLists.Pets:
                        UpdatePetHP(p, objectID);
                        break;
                    default:
                        break;
                }
            }
            return null;
        }
    }
}
