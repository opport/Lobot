﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    // Inpanic server
    // sp exp update
    //[S -> C] [3056]
    //26 A1 01 00                                       &...............
    //98 08 00 00 00 00 00 00                           ................
    //98 08 00 00 00 00 00 00                           ................
    //00                                                ................

    //[S -> C] [3056]
    //97 1F F1 00                                       unknown uniqueID?
    //8C 0A 00 00 00 00 00 00                           expAfter expBefore
    //40 1A 00 00 00 00 00 00                           spAfter spBefore
    //00                                                unknown flag
    //0C 00                                             unknown 12 

    //// level up from 5 to 7
    //[S -> C][3054]
    //0B 0B 02 03                                       uniqueID

    //[S -> C][3056]
    //D7 7D E9 02                                       unkn
    //00 32 00 00 00 00 00 00                           expAfter expBefore
    //62 2A 00 00 00 00 00 00                           spAfter spBefore
    //00                                                level down flag?
    //06 00                                             last level?

    ////level up from 7 to 13 18797 exp
    //[S->C][3054]
    //6A 4E 02 03                                       uniqueID

    //[S->C][3056]
    //C2 9C E9 02                                       unkn
    //77 72 02 00 00 00 00 00                           expAfter expBefore
    //B0 7B 01 00 00 00 00 00                           spAfter spBefore
    //00                                                level down flag?
    //12 00                                             last level = 12 ?
    //12|47940
    public class CharExpSpUpdate : Handler
    {
        public override ushort ServerOpcode => 0x3056;

        public CharExpSpUpdate() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint monsterID = p.ReadUInt32(); // monster that died

            uint finalExp = p.ReadUInt32() - p.ReadUInt32();
            uint finalSPExp = p.ReadUInt32() - p.ReadUInt32(); // exp to get single sp point

            ulong newExp = Bot.CharData.ExpOffset + finalExp;
            uint newOffset = (uint)(newExp - PK2DataGlobal.LevelData[Bot.CharData.Level]);

            if (PK2DataGlobal.LevelData.TryGetValue(Bot.CharData.Level, out ulong currentMaxEXP))
            {
                if (newExp < 0) // Exp decrease and level decrease , e.g. due to death
                {
                    Bot.CharData.ExpOffset += newExp;
                    Bot.CharData.Level--;
                }
                else
                {
                    Bot.CharData.ExpOffset = newExp;

                    // take multiple level ups into account
                    while (Bot.CharData.ExpOffset > currentMaxEXP)
                    {
                        if (PK2DataGlobal.LevelData.TryGetValue(Bot.CharData.Level + 1, out ulong nextLevelMaxExp))
                        {
                            Bot.CharData.Level++;
                            Bot.CharData.RemainStatPoint += 3;
                            Bot.CharData.ExpOffset -= currentMaxEXP;
                            currentMaxEXP = nextLevelMaxExp;
                        }
                        else
                        {
                            break; // exit loop if max level exp is somehow less than current exp
                        }
                    }
                }
            }

            return null;
        }
    }
}
