﻿using LobotAPI;
using LobotAPI.Globals;

using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LobotDLL.Packets.Char
{


    //Start using skill
    //[S -> C][B070]
    //01                                                successFlag // 1 = success 
    //02 30                                             unkn 12290
    //47 00 00 00                                       skillID 71 antidevil bow
    //0B 2C 10 00                                       casterID
    //2A BB 00 00                                       unkn 106790
    //26 A1 01 00                                       targetID
    //00                                                instant response
    //
    //Damage skill
    //[S -> C][B070]
    //01                                                successFlag // 1 = success 
    //02 30                                             skilltype
    //32 0E 00 00                                       skillID
    //7F 82 32 01                                       attackerID
    //6C 95 05 00                                       unknw
    //9D 86 78 01                                       targetID
    //01                                                ................
    //02                                                ................
    //01                                                ................
    //9D 86 78 01                                       targetID
    //00                                                ................
    //01 0A 00 00                                       ................
    //00 00 00 00                                       ................
    //02                                                ................

    //-----Errors

    // 스킬 관련							0								
    //1	UIIT_SKILL_USE_FAIL_NOTENOUGHMP MP가 부족해서 사용할 수 없습니다.    0	0	0	0	0	0	Cannot use due to insufficient MP	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_NOTENOUGHHP HP가 부족해서 사용할 수 없습니다.	0	0	0	0	0	0	Cannot use due to insufficient HP	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_TIMEDELAY_COOL_TIME 스킬 재 사용 시간이 지나지 않았습니다.  0	0	0	0	0	0	Still have time to reuse the skill. 0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_WRONGTARGET 올바른 타겟이 아닙니다.	0	0	0	0	0	0	Invalid target	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_NONESELECTETARGET 타겟이 선택되지 않았습니다.	0	0	0	0	0	0	The target is not selected.	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_OVERLAP 중복될 수 없는 아이템이나 스킬이 이미 사용되고 있습니다.    0	0	0	0	0	0	The selected item cannot be used together or the skill is already in use.   0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_CHAR_DEAD 캐릭터가 죽은 상태에서 해당 아이템을 사용할 수 없습니다.    0	0	0	0	0	0	Cannot the use selected item while dead.    0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_WRONGWEAPON 스킬을 쓸 수 없는 무기를 착용하고 있습니다.	0	0	0	0	0	0	You wear equipment which prohibits you from using the selected skill.   0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_RUNOUT_AMMO 화살이 부족합니다.  0	0	0	0	0	0	Insufficient arrows.	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_RUNOUT_AMMO_VOLT 볼트가 부족합니다.  0	0	0	0	0	0	Insufficient bolts.	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_BROKEN_WEAPON 무기가 고장나서 공격을 할 수 없습니다.  0	0	0	0	0	0	Cannot attack because the weapon is broken  0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_PATH_INTERRUPTED 장애물이 있어 공격을 할 수 없습니다.   0	0	0	0	0	0	Cannot attack due to an obstacle.	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_DUPLICATE 자신 혹은 상대방이 직업/자유대련/살인자 상태일 때는 사용할 수 없습니다. 또한 운영자 캐릭터에게도 사용할 수 없습니다.	0	0	0	0	0	0	Cannot be used when you or the opponent is under job or free battle or murderer state.	0	0	0	0	0	0	0	0	
    //1	UIIT_SKILL_USE_FAIL_SEALED 현재 사용할 수 없는 스킬입니다.	0	0	0	0	0	0	Unable to use that skill at this moment 0	0	0	0	0	0	0	0	
    //Fail to cast fire shield  becuase shield not equipped
    //[S -> C][B070]
    //02                                                failure
    //0D 30                                             failure code 0x300D -> wrong weapon

    //Ammo not equipped
    //[S -> C][B070]
    //02                                                failure
    //0E 30                                             failure code 0x300E -> Insufficient bolts UIIT_SKILL_USE_FAIL_RUNOUT_AMMO_VOLT
    /// <summary>
    /// <see href="https://www.elitepvpers.com/forum/sro-coding-corner/4280218-opcode-0xb071.html"/>
    /// </summary>
    public class UseSkillHandler : SkillHandler
    {
        public static event EventHandler<MonsterAggroArgs> DrewMonsterAggro = delegate { };
        public static event EventHandler<MonsterAggroArgs> DrewMonsterAggroPet = delegate { };
        public static event EventHandler<SkillErrorArgs> SkillCastError = delegate { };
        public static event EventHandler<SkillArgs> SkillCast = delegate { };

        public override ushort ServerOpcode => 0xB070;

        public UseSkillHandler() { }

        private static void OnSKillCast(SkillArgs e)
        {
            EventHandler<SkillArgs> handler = SkillCast;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnSkillCastError(SkillErrorArgs e)
        {
            EventHandler<SkillErrorArgs> handler = SkillCastError;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnDrewMonsterAggro(MonsterAggroArgs e)
        {
            EventHandler<MonsterAggroArgs> handler = DrewMonsterAggro;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnPetDrewMonsterAggro(MonsterAggroArgs e)
        {
            EventHandler<MonsterAggroArgs> handler = DrewMonsterAggroPet;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private async Task EnableSkill(int delay, Skill skill)
        {
            await Task.Delay(delay).ContinueWith(task =>
            {
                skill.SkillEnabled = 0x01;
            });
        }

        private void RegisterBotSkillCast(uint skillID)
        {
            Skill skill = CharInfoGlobal.Skills.FirstOrDefault(s => s.RefSkillID == skillID);

            if (skill != null) // TODO automatically add chain attacks to skill list to prevent null being returned
            {
                skill.SkillEnabled = 0x0;

                OnSKillCast(new SkillArgs(skill));
                EnableSkill(skill.Cooldown, skill).ConfigureAwait(false); // reset after cooldown
            }
        }

        private void RegisterSpawnSkillCast(uint attackerID, uint targetID)
        {
            if (SpawnListsGlobal.TryGetSpawnObject(attackerID, out ISpawnType attacker))
            {
                if (attacker is Monster) // Handle monster type attacking
                {
                    (attacker as Monster).Target = targetID;
                    if (Bot.CharData.UniqueID == targetID)
                    {
                        OnDrewMonsterAggro(new MonsterAggroArgs(attacker as Monster));
                    }
                    else if (Bot.GrowthPet.UniqueID == targetID)
                    {
                        OnPetDrewMonsterAggro(new MonsterAggroArgs(attacker as Monster));
                    }
                }
                else if (attacker is CharData)
                {
                    // Handle Player skill cast
                }
                else if (attacker is GrowthPet)
                {
                    // Handle Player skill cast
                }
                else
                {
                    // Unknown unparsed monster ?
                    System.Diagnostics.Debug.WriteLine(string.Format("Non-Monster-Attacker! ID {0:x}", attackerID));
                    Logger.Log(LogLevel.SCRIPT, string.Format("Non-Monster-Attacker! ID {0:x}", attackerID));
                }
            }
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                ushort setflag = p.ReadUInt16(); // 02 30

                // TODO figure out when to ignore type cast
                //if (setflag != 0x3002)
                //{
                //    return null;
                //}

                uint skillID = p.ReadUInt32();
                uint attackerID = p.ReadUInt32();
                uint unknown = p.ReadUInt32();
                uint targetID = p.ReadUInt32();

                if (attackerID == Bot.CharData.UniqueID) // set skill on cooldown
                {
                    RegisterBotSkillCast(skillID);
                }
                else if (targetID == Bot.CharData.UniqueID) // Handle being the target
                {
                    RegisterSpawnSkillCast(attackerID, targetID);
                }

                ParseDamage(p);
            }
            else
            {
                byte errorType = p.ReadUInt8();
                switch (errorType)
                {
                    case 0x05:
                        OnSkillCastError(new SkillErrorArgs(SkillErrorType.SkillOnCooldown));
                        break;
                    case 0x06:
                        OnSkillCastError(new SkillErrorArgs(SkillErrorType.InvalidTarget));
                        break;
                    case 0x10: // can't attack due to obstacle
                        OnSkillCastError(new SkillErrorArgs(SkillErrorType.Obstacle));
                        break;
                    case 0x0D:
                        byte subType = p.ReadUInt8();

                        if (subType == 0x30) // cannot cast skill due to wrong weapon
                        {
                            OnSkillCastError(new SkillErrorArgs(SkillErrorType.WrongWeapon));
                            // TODO change to right weapon
                        }
                        break;
                    case 0x0E: // Insufficient Bolts
                        OnSkillCastError(new SkillErrorArgs(SkillErrorType.NoBolts));
                        break;
                    default:
                        break;
                }
                // error
            }

            return null;
        }
    }
}
