﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[S -> C][3122]
    //DC 0A                                             2780
    //16                                                22
    //17                                                23
    public class SpawnUNKNPacket : Handler
    {
        public SpawnUNKNPacket() { }

        public override ushort ServerOpcode => 0x3122;

        protected override ICommand ParsePacket(Packet p)
        {
            ushort unkn0 = p.ReadUInt16();
            byte unkn1 = p.ReadUInt8();
            byte unkn2 = p.ReadUInt8();

            return null;
        }
    }
}
