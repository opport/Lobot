﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Char
{
    public class CharDeathHandler : Handler
    {
        public static event EventHandler CharDied = delegate { };

        private void OnCharDied()
        {
            EventHandler handler = CharDied;
            if (handler != null)
            {
                handler(null, EventArgs.Empty);
            }
        }

        public override ushort ServerOpcode => 0x3011;

        public CharDeathHandler() { }

        //[S -> C] [3011]
        //04                                                deathFlag 4
        protected override ICommand ParsePacket(Packet p)
        {
            switch (p.ReadUInt8())
            {
                case 0x04:
                    Bot.CharData.IsAlive = false;
                    Bot.Status = BotStatus.Idle;
                    OnCharDied();
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
