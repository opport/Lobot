﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets
{
    //Start- then cancel autoattack on big-eyed ghost    
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //01                                                type ? attack
    //01                                                targetted flag?
    //F3 17 A5 01                                       uniqueId

    //[S -> C][B074]
    //01                                                confirmation flag
    //01                                                secondary cast when in range ?

    //[C -> S][7074]
    //02                                                cancellation flag

    //[S -> C][B074]
    //02                                                cancellation flag
    //00                                                unknown flag

    //[S -> C][B074]
    //02                                                ................
    //00                                                ................
    public class ObjectActionCancel : ObjectAction
    {
        protected ObjectActionCancel() : base() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode, false, false, new byte[] { 0x02 });
        }
    }
}
