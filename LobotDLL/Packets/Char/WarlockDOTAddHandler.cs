﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[C -> S][7045]
    //C9 34 DD 00                                       targetID

    //[S -> C][B0BE]
    //02 2A 00 00                                       skillID
    //03 01 02 00                                       uniqueBuffID1
    //C9 34 DD 00                                       targetID
    //00 00                                             unkn

    //[S -> C][B0BE]

    //[S -> C][B0BD]
    //C9 34 DD 00                                       targetID
    //02 2A 00 00                                       skillID
    //2B D6 02 00                                       uniqueBuffID2
    //60 09 00 00                                       unkn

    //[S -> C] [B072]
    //01                                                success
    //03 01 02 00                                       uniqueBuffID1

    //[S -> C] [B072]
    //01                                                success
    //2B D6 02 00                                       uniqueBuffID2
    public class WarlockDOTAddHandler : Handler
    {
        /// <summary>
        /// Dictionary of skills to delete later and their targets 
        /// </summary>

        public WarlockDOTAddHandler() { }

        public override ushort ServerOpcode => 0xB0BE;

        protected override ICommand ParsePacket(Packet p)
        {
            uint skillID = p.ReadUInt32();
            uint uniqueSkillID = p.ReadUInt32();
            uint targetID = p.ReadUInt32();

            if (SpawnListsGlobal.TryGetSpawnObject(targetID, out ISpawnType spawn))
            {
                if (spawn is Monster)
                {
                    Buff b = new Buff(skillID);
                    b.UniqueID = uniqueSkillID;
                    (spawn as Monster).Buffs.Add(b);
                    SpawnListsGlobal.SpawnBuffs.Add(uniqueSkillID, spawn.UniqueID);
                }
            }

            return null;
        }
    }
}
