﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using SilkroadSecurityApi;
using System;
using System.Windows;

namespace LobotDLL.Packets.Char
{
    /// <summary>
    //from	x 6674	y 1343
    //to    x 6673	y 1314

    //[C -> S] [7021]
    //01                                                groundclick? yes
    //A9 62                                             xSector(169) ySector(98)
    //B3 05                                             x 1459
    //00 00                                             z 0
    //5D 06                                             y 1629

    //[S -> C]
    //    [B021]
    //    A2 27 66 02                                       uniqueID
    //01                                                groundclick? yes
    //A9 62                                             xSector(169) ySector(98)
    //B3 05                                             x 1459
    //00 00                                             z 0
    //5D 06                                             y 1629
    //01                                                hasOrigin? yes
    //A9 62                                             xSector(169) ySector(98)
    //6C 39                                             x 14700
    //D1 AE E1 C0                                       z 44753	angle 3297 * 360/65536 = 18
    //C4 4A y 19140 (switched place with angle might be server specific) 

    //x = ((Xsector - 135) * 192 + (Xoffset / 100));
    //y = ((Ysector - 92) * 192 + (Yoffset / 100));

    //destination
    //x = ((169 - 135) * 192 + (1459 / 10)) = 6673
    //y = ((98 - 92) * 192 + (1629 / 10)) = 1314

    //origin
    //x = ((169 - 135) * 192 + (14700 / 100)) = 6675
    //y = ((98 - 92) * 192 + (19140 / 100)) = 1343
    /// </summary>
    public class CharMove : BidirectionalPacket
    {
        readonly Point ToPoint;

        public static event EventHandler<CharMoveArgs> BotMovementRegistered = delegate { };
        public static event EventHandler<CharMoveArgs> SpawnMovementRegistered = delegate { };

        public override ushort Opcode => 0x7021;
        public override ushort ServerOpcode => 0xB021;

        public CharMove() { }

        /// <summary>
        /// The move command constructor using coordinates.
        /// </summary>
        /// <param name="x1">first x-coord</param>
        /// <param name="x2">second x-coord</param>
        /// <param name="y1">first-coord</param>
        /// <param name="y2">second y-coord</param>
        //public CharMove(int x1, int y1, int x2, int y2) : base(0x7021)
        //{
        //    this.FromPoint.X = x1;
        //    this.FromPoint.Y = y1;
        //    this.ToPoint.X = x2;
        //    this.ToPoint.Y = y2;
        //}

        /// <summary>
        /// The move command constructor using points.
        /// </summary>
        /// <param name="point1">The first point</param>
        /// <param name="point2">The second point</param>
        //public CharMove(Point point1, Point point2) : base(0x7021)
        //{
        //    this.FromPoint = point1;
        //    this.ToPoint = point2;
        //}

        public CharMove(int x, int y)
        {
            this.ToPoint.X = x;
            this.ToPoint.Y = y;
        }

        /// <summary>
        /// The move command constructor using points.
        /// </summary>
        /// <param name="point1">The first point</param>
        /// <param name="point2">The second point</param>
        public CharMove(Point point)
        {
            this.ToPoint = point;
        }

        /// <summary>
        /// [C -> S][7021]
        ///01                                                flag
        ///87 5D                                             xsector 135 ysector 93
        ///99 04                                             x 1177 (to be interpreted as 117.7 for final result of 117.7 )
        ///92 00                                             z
        ///46 02                                             582 (to be interpreted as 58.2 for final result of 250.2 )
        ///
        ///build packet for 117,250
        ///
        ///xsector = x / 192 + 135 	= 117 / 192 + 135 	= 135
        ///ysector = y / 192 + 92  	= 250 / 192 + 92 	= 93
        ///
        ///x = (xsector - 135) * 192 + xPos / 10 = (135 - 135) * 192 + xPos / 10 = 
        ///y = (ysector - 92) * 192 + yPos / 10  = (93 - 92) * 192 + yPos / 10   = 
        /// </summary>
        /// <returns>resulting packet</returns>
        protected override Packet AddPayload()
        {
            if (Bot.CharData == null || Proxy.Instance == null) return null;

            int xPos = 0;
            int yPos = 0;

            byte regionX = ScriptUtils.GetSectorX((int)ToPoint.X);
            byte regionY = ScriptUtils.GetSectorY((int)ToPoint.Y);

            Packet packet;

            packet = new Packet(Opcode, true);
            packet.WriteUInt8(0x01);
            packet.WriteUInt8(regionX);
            packet.WriteUInt8(regionY);

            if (Bot.CharData.IsInCave)
            {
                //regionX = Convert.ToByte(Bot.Char.CaveFloor); // will use normal regionX for now
                regionY = 0x80;

                xPos = (int)((ToPoint.X - ((regionX - 135) * 192)) * 10);
                yPos = (int)((ToPoint.Y - ((regionY - 92) * 192)) * 10);
            }
            else
            {
                xPos = (ushort)((ToPoint.X - ((regionX - 135) * 192)) * 10);
                yPos = (ushort)((ToPoint.Y - ((regionY - 92) * 192)) * 10);
            }

            packet.WriteUInt16(xPos);
            packet.WriteUInt16(0);
            packet.WriteUInt16(yPos);

            return packet;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            Point origin = default(Point), destination = default(Point);
            byte xSector, ySector, xSectorOrigin, ySectorOrigin = 0;
            int x, y, z, xOrigin, yOrigin, zOrigin;
            ushort angle = 0;

            uint uniqueID = p.ReadUInt32();
            byte isGroundClick = p.ReadUInt8();

            if (isGroundClick == 0x01)
            {
                xSector = p.ReadUInt8();
                ySector = p.ReadUInt8();

                if (Bot.CharData.IsInCave)
                {
                    x = p.ReadInt32();
                    z = p.ReadInt32();
                    y = p.ReadInt32();
                }
                else
                {
                    x = p.ReadInt16();
                    z = p.ReadInt16();
                    y = p.ReadInt16();
                }


                destination = new Point(ScriptUtils.GetXCoord(x, xSector), ScriptUtils.GetYCoord(y, ySector));
            }
            else
            {
                p.ReadUInt8(); // unknown 0x01
                angle = p.ReadUInt8();
            }

            byte hasOrigin = p.ReadUInt8();
            if (hasOrigin == 0x01)
            {
                xSectorOrigin = p.ReadUInt8();
                ySectorOrigin = p.ReadUInt8();
                xOrigin = p.ReadUInt16();
                zOrigin = p.ReadUInt16();
                angle = (ushort)(p.ReadUInt16() * 360 / 65536);
                yOrigin = p.ReadUInt16();

                origin = new Point(ScriptUtils.GetXCoordOrigin((ushort)xOrigin, xSectorOrigin), ScriptUtils.GetYCoordOrigin((ushort)yOrigin, ySectorOrigin));
            }

            HandleMovementRegistered(new CharMoveArgs(uniqueID, destination, origin, angle));
            return null;
        }

        protected static void OnBotMovementRegistered(CharMoveArgs e)
        {
            EventHandler<CharMoveArgs> handler = BotMovementRegistered;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        protected static void OnSpawnMovementRegistered(CharMoveArgs e)
        {
            EventHandler<CharMoveArgs> handler = SpawnMovementRegistered;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public static void HandleMovementRegistered(CharMoveArgs e)
        {
            if (e.UniqueID == Bot.CharData.UniqueID || Bot.CharData.TransportFlag == 0x01 && Bot.CharData.TransportUniqueID == e.UniqueID)
            {
                if (e.Origin != default(Point))
                {
                    Bot.CharData.Position = e.Origin;
                }

                if (e.Destination != Bot.CharData.Position) // No Origin point given in B021 packet
                {
                    // queue new task to be run after delay
                    //DispatchDelayedUpdated(e, Bot.CharData.Position);
                }

                OnBotMovementRegistered(e);
            }
            else if (SpawnListsGlobal.TryGetSpawnObject(e.UniqueID, out ISpawnType spawn))
            {
                if (e.Destination != spawn.Position) // No Origin point given in B021 packet
                {
                    // queue new task to be run after delay
                    //DispatchDelayedUpdated(e, spawn.Position);
                    spawn.Position = e.Destination; //FIXME remove delay to more accurately target monsters
                }

                OnSpawnMovementRegistered(e);
            }
        }
    }
    /// <summary>
    /// The command to move a player character or unit.
    /// 
    /// Opcode 7021 structure
    ///01
    ///A7 - X section[BYTE]
    ///62 - Y section[BYTE]
    ///3B 01 - X coord[WORD] --> shows the X * 10 (e.g. 85 means 8.5)
    ///28 00 - Z coord[WORD] --> ""
    ///3B 07 - Y coord[WORD] --> ""
    ///
    ///Confirming the move from Client Server sent down
    ///
    /// Skyclick
    ///[C -> S][7021]
    ///00                                                0 flag, means no ground click
    ///01                                                identifier, 01 is your own character
    ///AF 40                                             angle  x * 360 / 65536 ( for AF 40 -> 16559 * 360/ 65535 = 90)
    ///
    ///opcode B021
    ///
    ///9B 89 00 00 Object ID[DWORD]
    ///01 Has destination(00 if sky click)[BYTE]
    ///A7 X Section[BYTE]
    ///Section 62 Y[BYTE]
    ///1C 02 X coord[WORD]
    ///00 00 Z coord[WORD]
    ///C3 00 Y coord[WORD]
    ///00 No source[BYTE]
    ///
    //Can determine the ID of the character based on 2 packets
    //Can determine the position of the character, or object based on the B021

    //uint xPos = 0;
    //uint yPos = 0;

    //if (X > 0 && Y > 0)
    //{
    // xPos = (uint) ((X % 192) * 10);
    // yPos = (uint) ((Y % 192) * 10);
    //}
    //else
    //{
    // if(X< 0 && Y> 0)
    // {
    //  xPos = (uint) ((192 + (X % 192)) * 10);
    //  yPos = (uint) ((Y % 192) * 10);
    // }
    // else
    // {
    //  if (X > 0 && Y< 0)
    //  {
    //   xPos = (uint) ((X % 192) * 10);
    //   yPos = (uint) ((192 + (Y % 192)) * 10);
    //  }
    // }
    //}

    //byte xSector = (byte)((X - (int)(xPos / 10)) / 192 + 135);
    //byte ySector = (byte)((Y - (int)(yPos / 10)) / 192 + 92);
    //This is true until the opcode 1041
    /// </summary>

}
