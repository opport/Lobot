﻿using LobotAPI.Packets;

namespace LobotDLL.Packets
{

    public abstract class ObjectAction : Command
    {
        public override ushort Opcode => 0x7074;

        protected ObjectAction() { }
    }
}
