﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    public class CharAddInt : Command
    {
        public override ushort Opcode => 0x7051;

        public CharAddInt() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }
    }
}
