﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    public class CharSitStand : Command
    {
        public override ushort Opcode => 0x704F;

        public CharSitStand() { }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8(0x04);
            return p;
        }
    }
}
