﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{
    public class CharAddStr : Command
    {
        public override ushort Opcode => 0x7050;

        public CharAddStr() { }

        protected override Packet AddPayload()
        {
            return new Packet(Opcode);
        }
    }
}
