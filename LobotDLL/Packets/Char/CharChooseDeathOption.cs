﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets
{

    //[C -> S] [3053]
    //01                                                optionFlag 1= revive specified spot
    //[C -> S] [3053]
    //02                                                optionFlag 2 = revive current spot
    public class CharChooseDeathOption : Command
    {
        public override ushort Opcode => 0x3053;

        private CharDeathOption Option;

        public CharChooseDeathOption() { }
        public CharChooseDeathOption(CharDeathOption option) { Option = option; }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8(Option);
            return p;
        }
    }
}
