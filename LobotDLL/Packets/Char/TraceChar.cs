﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[C -> S][7074]
    //01                                                action phase flag // 1 = start 2 = cancel/complete
    //03                                                type
    //01                                                targetted?
    //1C 80 A8 01                                       targetID

    //[S -> C][B074]
    //01                                                ................
    //01                                                ................

    //[C -> S][7074]
    //02                                                stop action

    //[S -> C][B074]
    //02                                                stop confirm
    //00                                                flag

    //[S -> C][B074]
    //02                                                ................
    //00                                                ................
    public class TraceChar : ObjectAction
    {
        uint TargetID;

        protected TraceChar(uint targetID) : base()
        {
            TargetID = targetID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8(0x01);
            p.WriteUInt8(0x03);
            p.WriteUInt8(0x01);
            p.WriteUInt32(TargetID);
            return p;
        }
    }
}
