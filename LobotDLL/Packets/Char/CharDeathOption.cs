﻿namespace LobotDLL.Packets
{
    public enum CharDeathOption : byte
    {
        SpecifiedSpot = 0x01,
        CurrentSpot = 0x02
    }
}
