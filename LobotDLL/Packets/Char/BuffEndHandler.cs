﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Linq;

namespace LobotDLL.Packets.Char
{
    //Cast Skill river fire force imbue
    //[S -> C] [B070]
    //01                                                success
    //00 30                                             unkn 0x 30 // x =  buff
    //7C 00 00 00                                       buffID
    //54 18 11 00                                       unkn1
    //BC C1 04 00                                       
    //00 00 00 00                                       target?
    //00                                                end flag

    //[S->C][B0BD]
    //54 18 11 00                                       unkn1
    //7C 00 00 00                                       buffID
    //CC C2 04 00                                       buff uniqueID

    ////Buff end -> imbue
    //[S->C][B072]
    //01                                                count ? number of Buffs ended
    //CC C2 04 00                                       buff uniqueID

    //Fail to cast fire shield  becuase shield not equipped
    //[S -> C][B070]
    //02                                                failure
    //0D 30                                             failure code 0x300D -> wrong weapon?
    public class BuffEndHandler : Handler
    {
        public static event EventHandler<BuffArgs> BuffEnded = delegate { };

        private static void OnBuffEnded(BuffArgs e)
        {
            EventHandler<BuffArgs> handler = BuffEnded;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0xB072;

        public BuffEndHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte count = p.ReadUInt8();
            uint skillID = p.ReadUInt32();

            for (int i = 0; i < count; i++)
            {
                if (SpawnListsGlobal.SpawnBuffs.TryGetValue(skillID, out uint monsterID))
                {
                    if (SpawnListsGlobal.TryGetSpawnObject(monsterID, out ISpawnType spawn) && spawn is Monster)
                    {
                        Buff b = (spawn as Monster).Buffs.FirstOrDefault(buff => buff.UniqueID == skillID);
                        if (b != null)
                        {
                            (spawn as Monster).Buffs.Remove(b);
                        }
                    }
                    SpawnListsGlobal.SpawnBuffs.Remove(skillID);
                }
                else
                {
                    Buff b = Bot.CharData.ActiveBuffs.FirstOrDefault(buff => buff.UniqueID == skillID);

                    if (b != null)
                    {
                        Bot.CharData.ActiveBuffs.Remove(b);
                        OnBuffEnded(new BuffArgs(0, b.RefSkillID));
                    }
                }
            }

            return null;
        }
    }
}
