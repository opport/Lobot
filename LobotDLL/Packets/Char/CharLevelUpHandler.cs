﻿using LobotAPI.Globals;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System.Linq;

namespace LobotAPI.Packets.Char
{
    public class CharLevelUpHandler : Handler
    {
        public override ushort ServerOpcode => 0x3054;

        public CharLevelUpHandler() { }

        protected sealed override ICommand ParsePacket(Packet p)
        {
            uint id = p.ReadUInt32();

            if (id == Bot.CharData.UniqueID)
            {
                if (PK2DataGlobal.LevelData.TryGetValue(Bot.CharData.Level, out ulong currLevelMaxExp)) // reset exp for new level
                {
                    Bot.CharData.ExpOffset -= currLevelMaxExp;
                }

                Bot.CharData.Level++;
                Bot.CharData.RemainStatPoint += 3;

                // Take multi level levelups into account
                // update level until exp no longer exceeds required exp for current level
                //do
                //{
                //    Bot.CharData.Level++;
                //    Bot.CharData.RemainStatPoint += 3;
                //} while (PK2DataGlobal.LevelData.TryGetValue(Bot.CharData.Level, out ulong value) && value < Bot.CharData.ExpOffset);

                return null;
            }
            else if (id == Bot.GrowthPet?.UniqueID)
            {
                if (PK2DataGlobal.LevelData.TryGetValue(Bot.GrowthPet.Level, out ulong currLevelMaxExp)) // reset exp for new level
                {
                    Bot.GrowthPet.EXP -= currLevelMaxExp;
                }

                Bot.GrowthPet.Level++;


                // Take multi level levelups into account
                // update level until exp no longer exceeds required exp for current level
                //do
                //{
                //    Bot.GrowthPet.Level++;
                //} while (PK2DataGlobal.LevelData.TryGetValue(Bot.GrowthPet.Level, out ulong value) && value < Bot.GrowthPet.EXP);

                return null;
            }

            CharData player = SpawnListsGlobal.PlayerList.FirstOrDefault(iter => iter.UniqueID == id);

            if (player != null && player.UniqueID != 0)
            {
                SpawnListsGlobal.PlayerList[SpawnListsGlobal.PlayerList.IndexOf(player)].Level++;
            }
            return null;
        }
    }
}
