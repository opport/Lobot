﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //[S -> C]
    //[3201]
    //EF 00                                             quantity 239
    public class UpdateAmmo : Handler
    {
        public override ushort ServerOpcode => 0x3201;

        public UpdateAmmo() { }

        protected override ICommand ParsePacket(Packet p)
        {
            CharInfoGlobal.Inventory[(byte)EquipmentSlot.Secondary].StackCount = p.ReadUInt16();
            return null;
        }
    }
}
