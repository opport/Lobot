﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    //level up fire mastery to level 5
    //[C -> S][70A2]
    //13 01 00 00                                       masteryID
    //01                                                masteryLearnType

    //[S -> C][B0A2]
    //01                                                masteryLearnType
    //13 01 00 00                                       masteryID
    //05                                                mastery level
    public class CharMasteryUpdateLearn : BidirectionalPacket
    {
        private readonly uint MasteryID;
        private readonly byte LearnType;

        public override ushort ServerOpcode => 0xB0A2;

        public override ushort Opcode => 0x70A2;

        public CharMasteryUpdateLearn() { }

        public CharMasteryUpdateLearn(uint masteryID, byte masteryLearnType) { MasteryID = masteryID; LearnType = masteryLearnType; }

        protected sealed override ICommand ParsePacket(Packet p)
        {
            p.ReadUInt8(); //masteryLearnType success?
            uint masteryID = p.ReadUInt32();
            byte masteryLevel = p.ReadUInt8();

            if (Bot.CharData.Masteries.TryGetValue(masteryID, out Mastery mastery))
            {
                mastery.Level = masteryLevel;
            }
            else
            {
                Mastery m = new Mastery();
                m.ID = masteryID;
                m.Level = masteryLevel;

                Bot.CharData.Masteries.Add(masteryID, m);
            }
            return null;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(MasteryID);
            p.WriteUInt8(LearnType);
            return p;
        }
    }
}
