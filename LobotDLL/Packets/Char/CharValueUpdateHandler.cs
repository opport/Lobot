﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    public class CharValueUpdateHandler : Handler
    {

        //[S -> C] [304E]
        //01                                                updateType 1
        //CC 01 00 00 00 00 00 00                           amount
        //00                                                unknown
        enum CharUpdateType : byte
        {
            Gold = 0x01,
            SP = 0x02,
            //EXP = 0x03, ?
            Zerk = 0x04
        }

        public override ushort ServerOpcode => 0x304E;

        public CharValueUpdateHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            switch ((CharUpdateType)p.ReadUInt8())
            {
                case CharUpdateType.Gold:
                    Bot.CharData.RemainGold = p.ReadUInt64();
                    //p.ReadUInt8(); unknown
                    break;
                case CharUpdateType.SP:
                    Bot.CharData.RemainSkillPoint = p.ReadUInt32();
                    break;
                case CharUpdateType.Zerk:
                    Bot.CharData.RemainHwanCount = p.ReadUInt8();
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
