﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    ////Target self
    //[S -> C][B045]
    //01                                                success
    //C3 1B 7F 01                                       own ID

    //// Start casting heal ghost hand lvl 3
    //[S->C][B070]
    //01                                                success
    //02 30                                             skilltype
    //17 06 00 00                                       skillRefID
    //C3 1B 7F 01                                       caster id(self)
    //C4 DD 02 00                                       unknw
    //C3 1B 7F 01                                       targetID(self)
    //00                                                instantResponse
    //// End casting
    //[S->C][B071]
    //01                                                success
    //C4 DD 02 00                                       unknown
    //C3 1B 7F 01                                       target
    //00                                                hasDamage
    public class UseSkillEnd : SkillHandler
    {
        public override ushort ServerOpcode => 0xB071;

        public UseSkillEnd() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte isSuccess = p.ReadUInt8();
            if (isSuccess == 1)
            {
                uint casterID = p.ReadUInt32();
                uint targetID = p.ReadUInt32();

                ParseDamage(p);
            }
            else if (isSuccess == 2)
            {
                ushort ErrorCode = p.ReadUInt16();
                uint castWorldId = p.ReadUInt32();
            }

            return null;
        }
    }
}
