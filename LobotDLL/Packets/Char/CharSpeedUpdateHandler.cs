﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Char
{
    public class CharSpeedUpdateHandler : Handler
    {
        public override ushort ServerOpcode => 0x30D0;

        public CharSpeedUpdateHandler() { }

        //[S -> C] [30D0]
        //18 DF 61 00                                       uniqueID
        //9A 99 99 41                                       walkspeed
        //01 00 70 42                                       runspeed
        protected override ICommand ParsePacket(Packet p)
        {
            if (p.ReadUInt32() == Bot.CharData.UniqueID)
            {
                Bot.CharData.WalkSpeed = p.ReadSingle();
                Bot.CharData.RunSpeed = p.ReadSingle();
            }

            return null;
        }
    }
}
