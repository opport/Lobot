﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Pet
{
    //[C -> S][7116]
    //D8 47 79 07                                       uniqueID
    public class PetUnsummon : Command
    {
        private uint UniqueID;

        public PetUnsummon(uint uniqueID) { UniqueID = uniqueID; }

        public override ushort Opcode => 0x7116;

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(UniqueID);
            return p;
        }
    }
}
