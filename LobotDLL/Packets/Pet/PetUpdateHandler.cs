﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Pet
{
    //[S -> C][30C9]
    //FC 23 7F 07                                       uniqueID
    //03                                                type gain exp
    //34 25 01 00 00 00 00 00                           76050 exp
    //E5 EB DC 06                                       ownerID = character

    //[S -> C][30C9]
    //FC 23 7F 07                                       uniqueID
    //04                                                type HGP update
    //0F 27                                             hgp 9999

    //[S -> C][30C9]
    //FC 23 7F 07                                       uniqueID
    //07                                                type model update
    //E7 17 00 00                                       model number 6119
    public class PetUpdateHandler : Handler
    {
        public static event EventHandler<BoolEventArgs> HGPChanged = delegate { };

        private void OnHGPChanged(BoolEventArgs e)
        {
            EventHandler<BoolEventArgs> handler = HGPChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public PetUpdateHandler() { }

        public override ushort ServerOpcode => 0x30C9;

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();

            if (Bot.GrowthPet.UniqueID == uniqueID)
            {
                byte type = p.ReadUInt8();
                if (type == 0x01) // Unsummon: HGP stop counting down
                {
                    OnHGPChanged(new BoolEventArgs(false));
                }
                else if (type == 0x03) // gain exp
                {
                    ulong exp = p.ReadUInt64();
                    uint unkn = p.ReadUInt32(); // does not seem to be ownerID

                    Bot.GrowthPet.EXP += exp;

                    if (PK2DataGlobal.LevelData.TryGetValue(Bot.GrowthPet.Level, out ulong currentMaxEXP))
                    {
                        // take multiple level ups into account
                        while (Bot.GrowthPet.EXP > currentMaxEXP)
                        {
                            if (PK2DataGlobal.LevelData.TryGetValue(Bot.GrowthPet.Level + 1, out ulong nextLevelMaxExp))
                            {
                                Bot.GrowthPet.Level++;
                                Bot.GrowthPet.EXP -= currentMaxEXP;
                                currentMaxEXP = nextLevelMaxExp;
                            }
                            else
                            {
                                break; // exit loop if max level exp is somehow less than current exp
                            }
                        }
                    }
                }
                else if (type == 0x04) // HGP update
                {
                    Bot.GrowthPet.HGP = p.ReadUInt16();
                    OnHGPChanged(new BoolEventArgs(false));
                    AutoPotions.TryUseHGPPot(Bot.GrowthPet.UniqueID);
                }
                else if (type == 0x07) // refID update for new Model and Max HP after leveling up
                {
                    Bot.CharData.RefID = p.ReadUInt32();
                    if (PK2DataGlobal.NPCs.TryGetValue(Bot.CharData.RefID, out Pk2NPC pk2Pet))
                    {
                        Bot.CharData.MaxHP = pk2Pet.HP;
                        Bot.CharData.Level = (byte)pk2Pet.Level;
                    }
                }
            }

            return null;
        }
    }
}
