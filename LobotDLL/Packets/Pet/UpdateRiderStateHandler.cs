﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Pet
{
    // cos info
    //[S -> C][30C8]
    //5B B5 D7 01                                       pet ID
    //8F 08 00 00                                       refID (red horse)
    //D7 03 00 00                                       maxhp
    //D7 03 00 00                                       current hp
    //00                                                flag

    // update riderstate --> spawn horse
    //[S -> C] [B0CB]
    //01                                                riderIsSelf ? 
    //E1 90 D7 01                                       riderID -> check if self
    //01                                                ridingState
    //5B B5 D7 01                                       vehicleID

    //update riderstate --> despawn horse
    //[S -> C][B0CB]
    //01                                                riderIsSelf?
    //FD BB 6A 00                                       riderID
    //00                                                rideFlag
    //25 BC 6A 00                                       petID
    public class UpdateRiderStateHandler : Handler
    {
        public override ushort ServerOpcode => 0xB0CB;

        public UpdateRiderStateHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte isSelf = p.ReadUInt8();

            if (isSelf == 0x01)
            {
                uint riderID = p.ReadUInt32();


                if (riderID == Bot.CharData.UniqueID)
                {
                    Bot.CharData.TransportFlag = p.ReadUInt8();
                    Bot.CharData.TransportUniqueID = p.ReadUInt32();
                }
            }

            return null;
        }
    }
}
