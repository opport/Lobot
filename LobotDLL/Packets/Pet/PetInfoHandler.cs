﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using static LobotAPI.Structs.Item;

namespace LobotDLL.Packets.Pet
{
    public class PetInfoHandler : Handler
    {
        public PetInfoHandler() { }

        public override ushort ServerOpcode => 0x30C8;

        private Item ParseInventory(ref Packet p)
        {
            Item item = new Item();
            item.Slot = p.ReadUInt8();
            item.RentType = p.ReadUInt32();

            switch (item.RentType)
            {
                case 1:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    break;

                case 2:
                    item.Rent.CanDelete = p.ReadUInt16(); //(adds "Will be deleted when time period is over" to item)
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.MeterRateTime = p.ReadUInt32();
                    break;

                case 3:
                    item.Rent.CanDelete = p.ReadUInt16(); // (adds "Will be deleted when time period is over" to item)
                    item.Rent.PeriodBeginTime = p.ReadUInt32();
                    item.Rent.PeriodEndTime = p.ReadUInt32();
                    item.Rent.CanRecharge = p.ReadUInt16(); //(adds "Able to extend" to item)
                    item.Rent.PackingTime = p.ReadUInt32();
                    break;
            }

            item.RefItemID = p.ReadUInt32();

            if (PK2DataGlobal.Items == null)
            {
                PK2Utils.ExtractAllInformation();
            }

            if (PK2DataGlobal.Items.TryGetValue(item.RefItemID, out Pk2Item value))
            {
                string valueType = value.Type.ToString("X").Trim('0');

                if (value.IsEquipmentItem()) // Equipment
                {
                    Equipment e = new Equipment(item);
                    e.OptLevel = p.ReadUInt8();
                    e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.ReadUInt64());
                    e.Durability = p.ReadUInt32(); //(=> Durability)
                    e.MagParamNum = p.ReadUInt8(); //(=> Blue, Red)

                    for (int j = 0; j < e.MagParamNum; j++)
                    {
                        MagParamType type = (MagParamType)p.ReadUInt32();
                        uint valueBlue = p.ReadUInt32();
                        e.Blues.ChangeValue(type, valueBlue);
                    }

                    uint OptType = p.ReadUInt8(); //(1 => Socket)
                    uint OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.NParam1 = p.ReadUInt32(); //(=> Reference to Socket)

                    }
                    OptType = p.ReadUInt8(); //(2 => Advanced elixir)
                    OptCount = p.ReadUInt8();

                    for (int i = 0; i < OptCount; i++)
                    {
                        e.ItemOption.Slot = p.ReadUInt8();
                        e.ItemOption.ID = p.ReadUInt32();
                        e.ItemOption.OptValue = p.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")

                    }
                    //Notice for Advanced Elixir modding.
                    //Mutiple adv elixirs only possible with db edit. nOptValue of last nSlot will be shown as elixir in effect but total Plus value is correct
                    //You also have to fix error when "Buy back" from NPC
                    //## Stored procedure Error(-1): {?=CALL _Bind_Option_Manager (3, 174627, 1, 0, 0, 0, 0, 0, 0)} ## D:\WORK2005\Source\SilkroadOnline\Server\SR_GameServer\AsyncQuery_Storage.cpp AQ_StorageUpdater::DoWork_AddBindingOption 1366
                    //Storage Operation Failed!!! [OperationType: 34, ErrorCode: 174627]
                    //Query: {CALL _STRG_RESTORE_SOLDITEM_ITEM_MAGIC (174628, ?, ?, 34, 6696,13, 137,8,0,137, 1,4294967506,0,0,0,0,0,0,0,0,0,0,0,199970734269)}
                    //AQ Failed! Log out!! [AQType: 1]			
                    return e;
                }
                else if (value.Type.IsAttributeStone())
                {
                    item.StackCount = p.ReadUInt16();
                    item.AttributeAssimilationProbability = p.ReadUInt8(); // -- probability not exposed in all silkroad versions
                    return item;
                }
                else if (value.Type.IsMagicStone() || value.Type == Pk2ItemType.MagicStoneMall)
                {
                    item.StackCount = p.ReadUInt16();
                    //item.AttributeAssimilationProbability = p.ReadUInt8(); // -- probability not exposed in all silkroad versions
                    return item;
                }
                else if (value.Type == Pk2ItemType.GrowthPet)
                {
                    LobotAPI.Structs.PetScroll pet = new LobotAPI.Structs.PetScroll(item);
                    pet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)

                    if (pet.Status != PetStatus.Unsummoned)
                    {
                        pet.UniqueID = p.ReadUInt32();
                        pet.Name = p.ReadAscii();
                        pet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return pet;
                }
                else if (value.Type == Pk2ItemType.AbilityPet)
                {
                    AbilityPet abilityPet = new AbilityPet(item);
                    abilityPet.Status = (PetStatus)p.ReadUInt8(); //(1 = Unsumonned, 2 = Summoned, 3 = Alive, 4 = Dead)
                    if (abilityPet.Status != PetStatus.Unsummoned)
                    {
                        abilityPet.UniqueID = p.ReadUInt32();
                        //abilityPet.NameLength = p.ReadUInt16();
                        abilityPet.Name = p.ReadAscii();
                        abilityPet.SecondsToRentEndTime = p.ReadUInt32();
                        abilityPet.unk02 = p.ReadUInt8(); // -> Check for != 0
                    }
                    return abilityPet;
                }
                else if (value.Type == Pk2ItemType.ItemExchangeCoupon)
                {
                    item.StackCount = p.ReadUInt16();
                    uint MagParamNum = p.ReadUInt8();

                    for (int i = 0; i < MagParamNum; i++)
                    {
                        ItemExchangeCoupon iec = new ItemExchangeCoupon(item);
                        iec.MagParamValues.Add(p.ReadUInt64());
                        //1. MagParam => CouponRefItemID [fixed]
                        //2. MagParam => CouponItemAmount [fixed]
                        //When Coupon holds Scrolls or similar, these 2 MagParams above are used.
                        //When Coupon holds Equipment, 8 MagParams are used
                        //They are defined in database by "[BIIV]<M:str,1,3><M:int,1,3><O:3>"
                        //As MagParams we get those 2 above and
                        //01 4D 01 72 74 73 00 00                           MagParam3 - .M.rts..........	(=> There is our str, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam4 - MagParam.Value		(=> Amount of +STR)
                        //01 4D 01 74 6E 69 00 00                           MagParam5 - .M.tni..........	(=> There is our int, don't ask me why its reversed)
                        //03 00 00 00 00 00 00 00                           MagParam6 - MagParam.Value		(=>	Amount of +INT)
                        //01 4F 00 00 00 00 00 00                           MagParam7 - .O..............	(=> There is our O for OptLevel)
                        //03 00 00 00 00 00 00 00                           MagParam8 - OptLevel			(=> Amount of +Overall)
                    }
                    return item;
                }
                else if (value.Type == Pk2ItemType.MagicCube)
                {
                    MagicCube magicCube = new MagicCube(item);
                    magicCube.StoredItemCount = p.ReadUInt32();
                    return magicCube;
                }
                else
                {
                    item.StackCount = p.ReadUInt16();
                    return item;
                }
            }
            else
            {
                return item;
            }
        }

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();
            uint petRefID = p.ReadUInt32();

            if (Bot.PickupPet?.UniqueID == uniqueID)
            {
                CharInfoGlobal.PetInventory.AsCollection().Clear();
                p.ReadUInt64(); // Unknown
                p.ReadUInt32(); // Unknown
                Bot.PickupPet.CustomName = p.ReadAscii();

                CharInfoGlobal.PetInventorySize = p.ReadUInt8();
                byte itemCount = p.ReadUInt8();
                for (int i = 0; i < itemCount; i++)
                {
                    Item item = ParseInventory(ref p);
                    CharInfoGlobal.PetInventory[item.Slot] = item;
                }
            }
            else if (Bot.GrowthPet?.UniqueID == uniqueID)
            {
                Bot.GrowthPet.RefID = petRefID; // refID is used for for pet model and max hp update on lvl up
                Bot.GrowthPet.CurrentHP = p.ReadUInt32();
                int maxHP = (int)p.ReadUInt32();
                Bot.GrowthPet.EXP = p.ReadUInt64();
                Bot.GrowthPet.Level = p.ReadUInt8();

                if (maxHP > 0)
                {
                    Bot.GrowthPet.MaxHP = maxHP;
                }
                else if (PK2DataGlobal.NPCs.TryGetValue(Bot.GrowthPet.RefID, out Pk2NPC pk2Pet))
                {
                    Bot.GrowthPet.MaxHP = pk2Pet.HP;
                }

                Bot.GrowthPet.HGP = p.ReadUInt16();
                Bot.GrowthPet.AttackStatus = p.ReadUInt32() == 0x01;
                Bot.GrowthPet.CustomName = p.ReadAscii();

                byte unkn0 = p.ReadUInt8(); // Unknown
                Bot.GrowthPet.OwnerID = p.ReadUInt32();
                Bot.GrowthPet.ScrollSlot = p.ReadUInt8(); // Unknown
            }
            else if (Bot.CharData?.TransportUniqueID == uniqueID)
            {
                Bot.Transport.CurrentHP = p.ReadUInt32();
                Bot.Transport.MaxHP = (int)p.ReadUInt32();
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Unknown pet found! {0:x}", p.Opcode));
                Logger.Log(LogLevel.HANDLER, string.Format("Unknown pet found: {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
            }
            return null;
        }
    }
}
