﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Pet
{
    public class PetUpdateSettings : BidirectionalPacket
    {
        private uint UniqueID;
        private byte Type;
        private uint Value;

        public PetUpdateSettings() { }
        public PetUpdateSettings(uint uniqueID, byte type, uint value)
        {
            UniqueID = uniqueID;
            Type = type;
            Value = value;
        }

        public override ushort ServerOpcode => 0xB420;

        public override ushort Opcode => 0x7420;

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt32(UniqueID);
            p.WriteUInt8(Type);
            p.WriteUInt32(Value);
            return p;
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                uint uniqueID = p.ReadUInt32();

                if (Bot.GrowthPet.UniqueID == uniqueID)
                {
                    byte type = p.ReadUInt8();

                    if (type == 0x01) // Attack/Defense update
                    {
                        Bot.GrowthPet.AttackStatus = p.ReadUInt32() == 0x01;
                    }
                }
            }

            return null;
        }
    }
}
