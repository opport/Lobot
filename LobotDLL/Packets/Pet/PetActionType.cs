﻿namespace LobotDLL.Packets.Pet
{
    public enum PetActionType : byte
    {
        Move,
        Pickup
    }
}
