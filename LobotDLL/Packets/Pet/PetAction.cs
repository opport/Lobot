﻿using LobotAPI;
using LobotAPI.Packets;
using LobotAPI.Scripting;
using LobotDLL.Connection;
using SilkroadSecurityApi;
using System.Windows;

namespace LobotDLL.Packets.Pet
{
    public class PetAction : BidirectionalPacket
    {
        PetActionType Type;
        private Point ToPoint = new Point();
        private uint TargetID;

        public PetAction() { }

        public override ushort ServerOpcode => 0xB0C5;

        public override ushort Opcode => 0x70C5;

        /// <summary>
        /// Constructor for pet pickup command.
        /// </summary>
        /// <param name="uniqueID"></param>
        public PetAction(uint uniqueID)
        {
            Type = PetActionType.Pickup;
            TargetID = uniqueID;
        }
        /// <summary>
        /// Constructor for pet movement.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public PetAction(int x, int y)
        {
            Type = PetActionType.Move;
            ToPoint.X = x;
            ToPoint.Y = y;
        }
        /// <summary>
        /// Constructor for pet movement.
        /// </summary>
        /// <param name="point1">The first point</param>
        /// <param name="point2">The second point</param>
        public PetAction(Point point)
        {
            Type = PetActionType.Move;
            ToPoint = point;
        }

        protected Packet AddPickupPayload()
        {
            Packet p = new Packet(Opcode, true);

            p.WriteInt32(Bot.PickupPet.UniqueID);
            p.WriteUInt8(8);
            p.WriteUInt32(TargetID);

            return p;
        }

        protected Packet AddMovePayload()
        {
            if (Bot.CharData == null || Proxy.Instance == null) return null;

            int xPos = 0;
            int yPos = 0;

            byte regionX = ScriptUtils.GetSectorX((int)ToPoint.X);
            byte regionY = ScriptUtils.GetSectorY((int)ToPoint.Y);

            Packet packet;

            packet = new Packet(Opcode, true);
            packet.WriteUInt32(Bot.CharData.TransportUniqueID);
            packet.WriteUInt8(1);
            packet.WriteUInt8(1);

            packet.WriteUInt8(regionX);
            packet.WriteUInt8(regionY);

            if (Bot.CharData.IsInCave)
            {
                //regionX = Convert.ToByte(Bot.Char.CaveFloor); // will use normal regionX for now
                regionY = 0x80;

                xPos = (int)((ToPoint.X - ((regionX - 135) * 192)) * 10);
                yPos = (int)((ToPoint.Y - ((regionY - 92) * 192)) * 10);
            }
            else
            {
                xPos = (ushort)((ToPoint.X - ((regionX - 135) * 192)) * 10);
                yPos = (ushort)((ToPoint.Y - ((regionY - 92) * 192)) * 10);
            }

            packet.WriteUInt16(xPos);
            packet.WriteUInt16(0);
            packet.WriteUInt16(yPos);

            return packet;
        }

        protected override Packet AddPayload()
        {
            switch (Type)
            {
                case PetActionType.Move:
                    return AddMovePayload();
                case PetActionType.Pickup:
                default:
                    return AddPickupPayload();
            }
        }

        protected override ICommand ParsePacket(Packet p)
        {
            return null;
        }
    }
}
