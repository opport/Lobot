﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Pet
{
    //[C -> S][70CB]
    //00                                                ridingFlag
    //25 BC 6A 00                                       petID
    public class PetDismount : Command
    {
        public override ushort Opcode => 0x70CB;

        public PetDismount() { }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt8(0);
            p.WriteUInt32(Bot.CharData.TransportUniqueID);

            return p;
        }
    }
}
