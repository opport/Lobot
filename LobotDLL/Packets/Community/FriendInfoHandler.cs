﻿using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Community
{
    // empty friends list
    //[S -> C][3305]
    //00                                                ................

    //[S -> C][3305]
    //01                                                numberofFriends
    //B8 73 00 00                                       playerID ? 
    //09 00                                             nameLength 9
    //4C 6F 62 6F 74 54 65 73 74                        LobotTest.......
    //84 07 00 00                                       refID 1924 char_woman_fox
    //00                                                isOffline
    public class FriendInfoHandler : Handler
    {
        public static event EventHandler HandledFriendInfo = delegate { };

        public override ushort ServerOpcode => 0x3305;

        public FriendInfoHandler() { }

        private static void OnHandledFriendInfo(EventArgs e)
        {
            EventHandler handler = HandledFriendInfo;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        protected override ICommand ParsePacket(Packet p)
        {
            byte remainingFriends = p.ReadUInt8();

            for (int i = 0; i < remainingFriends; i++)
            {
                uint playerID = p.ReadUInt32();
                string playerName = p.ReadAscii();
                uint playerRefID = p.ReadUInt32();
                byte isOffline = p.ReadUInt8();
                // TODO add friends
            }

            OnHandledFriendInfo(EventArgs.Empty);
            return null;
        }
    }
}
