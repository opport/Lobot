﻿using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Community.Guild
{
    // Fortress messages

    //Jangan(large fortress)
    //occupying guild: Ulcus
    //Guild master: BLink
    //Guild message: Zartbitter.Dinkel.Knusperwaffeln.200g.für.3,29€.

    //Bandit Fortress
    //occupying guild: 
    //Guild master: 
    //Guild message: 

    //Hotan Fortress(Large)
    //occupying guild: 
    //Guild master: 
    //Guild message: 


    //[S -> C] [385F]
    //00                                                unkn
    //03                                                number of fortresses (?)
    //01 00 00 00                                       fortressID jangan
    //05 00                                             name length
    //55 6C 63 75 73                                    Ulcus...........
    //DF 09 00 00                                       unkn (guild number?)
    //05 00                                             guildmaster length
    //42 6C 69 6E 6B                                    Blink...........
    //31 00                                             guild message length
    //5A 61 72 74 62 69 74 74 65 72 20 44 69 6E 6B 65   Zartbitter.Dinke
    //6C 20 4B 6E 75 73 70 65 72 77 61 66 66 65 6C 6E   l.Knusperwaffeln
    //20 32 30 30 67 20 66 FC 72 20 33 2C 32 39 80 20   .200g.f.r.3,29..
    //20                                                unkn 32
    //03 00 00 00                                       3
    //50 00 00 00                                       item price 80 (100 - 20% taxes)
    //01 00 00 00                                       1
    //00                                                unkn
    //00                                                unkn
    //03 00 00 00                                       fortressID bandit
    //00 00                                             name length 0
    //00 00 00 00                                       guild number
    //00 00                                             guildmaster name length 0
    //00 00                                             guild message length 0
    //00 00 00 00                                       unkn
    //00 00 00 00                                       item price percent
    //00 00 00 00                                       unkn
    //00                                                unkn
    //00                                                unkn
    //06 00 00 00                                       fortressID hotan
    //00 00                                             name length
    //00 00 00 00                                       guild number
    //00 00                                             guild master length
    //00 00                                             guild message length
    //00 00 00 00                                       unkn
    //00 00 00 00                                       item price percent
    //00 00 00 00                                       unkn
    //00                                                unkn
    //00                                                unkn
    //02                                                unkn
    //00 00 00 00                                       ................

    // Unknown status
    //[S -> C] [385F]
    //04                                                unkn End of War?
    public class FortressInfoHandler : Handler
    {
        public override ushort ServerOpcode => 0x385F;

        public FortressInfoHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            try
            {
                byte unkn = p.ReadUInt8();

                if (unkn != 0) // Anything other than 0 can cause crash
                {
                    return null;
                }

                byte numberOfFortresses = p.ReadUInt8();

                for (int i = 0; i < numberOfFortresses; i++)
                {
                    uint fortressID = p.ReadUInt32();

                    string guildName = p.ReadAscii();
                    uint guildNumber = p.ReadUInt32();
                    string guildMasterName = p.ReadAscii();
                    string guildMessage = p.ReadAscii();

                    uint unkn2 = p.ReadUInt32();
                    uint itemPricePercentage = p.ReadUInt32();
                    uint unkn3 = p.ReadUInt32();

                    if (LoginSettings.Version > 228)
                    {
                        byte unkn4 = p.ReadUInt8();
                        byte unkn5 = p.ReadUInt8();
                    }
                    else if (i + 1 < numberOfFortresses)
                    {
                        byte unkn4 = p.ReadUInt8();
                        uint unkn5 = p.ReadUInt32();
                        byte unkn6 = p.ReadUInt8();
                        uint unkn7 = p.ReadUInt32();
                    }

                }
            }
            catch (System.Exception e)
            {
                System.Diagnostics.Debug.WriteLine($"{e.Message}\n{e.StackTrace}");
                throw;
            }

            return null;
        }
    }
}
