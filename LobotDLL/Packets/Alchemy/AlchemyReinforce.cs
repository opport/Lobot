﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Commands.Alchemy;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using static LobotAPI.Structs.Item;

namespace LobotDLL.Packets.Alchemy
{
    //Hero Steel Shield lvl 29 enhance, no lucky powder fail
    //[C->S][7150]
    //02                                                action = 2 fuse
    //03                                                type = 3 elixir
    //02                                                factors 2
    //32                                                itemslot = 50
    //34                                                elixirSlot = 52

    //[S -> C] [B150]
    //01                                                ................
    //02                                                ................
    //00                                                ................
    //32                                                2...............
    //00                                                ................
    //00 00 00 00                                       ................
    //06 01 00 00                                       ................
    //00                                                ................
    //49 19 31 14 00 00 00 00                           I.1.............
    //63 00 00 00                                       c...............
    //03                                                ................
    //50 00 00 00 3C 00 00 00                           P...<...........
    //35 00 00 00 03 00 00 00                           5...............
    //74 00 00 00 03 00 00 00                           t...............
    //01                                                ................
    //00                                                ................
    //02                                                ................
    //00                                                ................

    //[S -> C] [B034]
    //01                                                ................
    //0F                                                ................
    //34                                                4...............
    //04                                                ................

    //Hero Steel Shield lvl 29 enhance, lucky powder, success +1
    //[C -> S] [7150]
    //02                                                action = 2 fuse
    //03                                                type = elixir
    //03                                                factors
    //32                                                slotItem = 50
    //35                                                slotElixir = 53
    //34                                                slotLuckyPowder = 52 

    //[S -> C] [B150]
    //01                                                reinforceFinished
    //02                                                type 2 fuse
    //01                                                success 1
    //32                                                itemslot = 50
    //00 00 00 00                                       owner? self
    //06 01 00 00                                       refID 262 hero steel shield
    //01                                                opt = 1 (success)
    //49 19 31 14 00 00 00 00                           attributes
    //63 00 00 00                                       durability 99
    //03                                                number of blues
    //50 00 00 00 3C 00 00 00                           durability 60%
    //35 00 00 00 03 00 00 00                           critical? 3%
    //74 00 00 00 03 00 00 00                           steady? 3%
    //01                                                optType = 1 socket
    //00                                                opt count = 0
    //02                                                opttype = 2 advanced elixir
    //00                                                optcount = 0

    //// Remove item
    //[S -> C] [B034]
    //01                                                success = 1 
    //0F                                                itemmoveType = 15 remove
    //34                                                slotItemRemove = 52 (lucky powder)
    //04                                                removeType? = 4 (alchemy - no lucky powder left)
    public class AlchemyReinforce : BidirectionalPacket
    {
        private AlchemyAction Action;
        private AlchemyType Type;
        private byte ItemSlot;
        private byte ElixirSlot;
        private byte LuckyPowderSlot;

        public static event EventHandler<AlchemyArgs> ReinforceParsed = delegate { };

        private static void OnReinforceParsed(AlchemyArgs e)
        {
            EventHandler<AlchemyArgs> handler = ReinforceParsed;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public AlchemyReinforce() { }

        public AlchemyReinforce(AlchemyAction action, AlchemyType type, byte itemSlot, byte elixirSlot, byte luckyPowderSlot = 0)
        {
            Action = action;
            Type = type;
            ItemSlot = itemSlot;
            ElixirSlot = elixirSlot;
            LuckyPowderSlot = luckyPowderSlot;
        }

        public override ushort Opcode => 0x7150;
        public override ushort ServerOpcode => 0xB150;

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteUInt8(Action);
            p.WriteUInt8(Type);
            p.WriteUInt8(ItemSlot);
            p.WriteUInt8(ElixirSlot);

            if (LuckyPowderSlot > 0)
            {
                p.WriteUInt8(LuckyPowderSlot);
            }

            return p;

        }

        protected override ICommand ParsePacket(Packet p)
        {
            bool reinforceFinished = p.ReadUInt8() == 1;
            AlchemyType alchemyType = (AlchemyType)p.ReadUInt8();
            bool success = p.ReadUInt8() == 1;

            Equipment e = new Equipment(new Item());
            e.Slot = p.ReadUInt8();
            uint ownerID = p.ReadUInt32(); // 0 == self?

            e.RefItemID = p.ReadUInt32();
            e.OptLevel = p.ReadUInt8();
            e.Attributes = new WhiteAttributes(e.GetAttributeType(), p.ReadUInt64());
            e.Durability = p.ReadUInt32();
            e.MagParamNum = p.ReadUInt8();

            for (int j = 0; j < e.MagParamNum; j++)
            {
                MagParamType type = (MagParamType)p.ReadUInt32();
                uint valueBlue = p.ReadUInt32();
                e.Blues.ChangeValue(type, valueBlue);
            }

            byte OptType1 = p.ReadUInt8(); //(1 => Socket)
            byte OptCount1 = p.ReadUInt8();

            for (int i = 0; i < OptCount1; i++)
            {
                e.ItemOption.Slot = p.ReadUInt8();
                e.ItemOption.ID = p.ReadUInt32();
                e.ItemOption.NParam1 = p.ReadUInt32(); //(=> Reference to Socket)

            }
            byte OptType2 = p.ReadUInt8(); //(2 => Advanced elixir)
            byte OptCount2 = p.ReadUInt8();

            for (int i = 0; i < OptCount2; i++)
            {
                e.ItemOption.Slot = p.ReadUInt8();
                e.ItemOption.ID = p.ReadUInt32();
                e.ItemOption.OptValue = p.ReadUInt32(); //(=> "Advanced elixir in effect [+OptValue]")
            }

            CharInfoGlobal.AvatarInventory[e.Slot] = e;

            OnReinforceParsed(new AlchemyArgs(success, e.OptLevel, e.AlchemySettingsString));
            return null;
        }
    }
}
