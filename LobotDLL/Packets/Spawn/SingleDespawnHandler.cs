﻿using LobotAPI;

using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Spawn
{
    public class SingleDespawnHandler : SpawnHandler
    {
        public override ushort ServerOpcode => 0x3016;

        public SingleDespawnHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            try
            {
                uint objectID = p.ReadUInt32();
                Despawn(objectID);
                return null;
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.HANDLER, string.Format("Despawn: {0}\n{1}", e.Message, e.StackTrace));
                return null;
            }
        }
    }
}
