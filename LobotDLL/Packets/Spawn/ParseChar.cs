﻿using LobotAPI;
using LobotAPI.Globals;

using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System.Text.RegularExpressions;

namespace LobotDLL.Packets.Spawn
{
    public class ParseChar : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            try
            {
                CharData player = new CharData();
                byte inventoryContainsJobEquipment = 0;
                player.Scale = p.ReadUInt8(); // Volume/Height
                player.Level = p.ReadUInt8(); // Rank
                p.ReadUInt8(); // Icons
                p.ReadUInt8(); // Unknown
                p.ReadUInt8(); // Max Slots
                player.Inventory.ItemCount = p.ReadUInt8();
                for (byte i = 0; i < player.Inventory.ItemCount; i++)
                {
                    Equipment equip = new Equipment(new Item());
                    equip.RefItemID = p.ReadUInt32();
                    string itemLongID = PK2DataGlobal.Items[equip.RefItemID].LongId;
                    if (Regex.IsMatch(itemLongID, @"^ITEM_(([0-9A-Z]+|ROC|FORT)_)?(EU|CH).+"))  //(itemLongID.StartsWith("ITEM_CH") || itemLongID.StartsWith("ITEM_EU") || itemLongID.StartsWith("ITEM_FORT") || itemLongID.StartsWith("ITEM_ROC_CH") || itemLongID.StartsWith("ITEM_ROC_EU"))
                    {
                        equip.OptLevel = p.ReadUInt8(); // Item Plus
                    }
                    if (itemLongID.Contains("_TRADE_TRADER_") || itemLongID.Contains("_TRADE_HUNTER_") || itemLongID.Contains("_TRADE_THIEF_"))
                    {
                        inventoryContainsJobEquipment = 1;
                    }

                    player.Inventory.Items.Add(equip);
                }
                player.Inventory.AvatarInventory.Size = p.ReadUInt8(); // Max Avatars Slot
                player.Inventory.AvatarInventory.ItemCount = p.ReadUInt8();
                for (byte i = 0; i < player.Inventory.AvatarInventory.ItemCount; i++)
                {
                    p.ReadUInt32(); // Avatar ID
                    p.ReadUInt8(); // Avatar Plus
                }
                int mask = p.ReadUInt8();
                if (mask == 1)
                {
                    uint maskID = p.ReadUInt32();
                    string maskLongID = PK2DataGlobal.NPCs[maskID].LongId;
                    if (maskLongID.StartsWith("CHAR"))
                    {
                        p.ReadUInt8();
                        byte count = p.ReadUInt8();
                        for (int i = 0; i < count; i++)
                        {
                            p.ReadUInt32();
                        }
                    }
                }

                player.UniqueID = p.ReadUInt32();

                player.XSec = p.ReadUInt8();
                player.YSec = p.ReadUInt8();
                player.XOffset = p.ReadSingle();
                player.ZOffset = p.ReadSingle();
                player.YOffset = p.ReadSingle();

                p.ReadUInt16(); // Position
                byte move = p.ReadUInt8(); // Moving
                player.MovementType = p.ReadUInt8(); // Running

                if (move == 1)
                {
                    player.DestXSec = p.ReadUInt8();
                    player.DestYSec = p.ReadUInt8();
                    if (player.DestYSec == 0x80)
                    {
                        player.DestX = (ushort)(p.ReadUInt16() - p.ReadUInt16());
                        player.DestZ = (ushort)(p.ReadUInt16() - p.ReadUInt16());
                        player.DestY = (ushort)(p.ReadUInt16() - p.ReadUInt16());
                    }
                    else
                    {
                        player.DestX = p.ReadUInt16();
                        player.DestZ = p.ReadUInt16();
                        player.DestY = p.ReadUInt16();
                    }
                }
                else
                {
                    player.SkyClickFlag = p.ReadUInt8(); // No Destination
                    player.Angle = p.ReadUInt16(); // Angle
                }

                //p.ReadUInt8(); // unknown from old sro version
                player.IsAlive = p.ReadUInt8() == 0x01; // Alive
                player.DebuffState = (DebuffStatus)p.ReadUInt8(); // -> Check for != 0
                player.MotionState = p.ReadUInt8(); // (0 = None, 2 = Walking, 3 = Running, 4 = Sitting)
                player.Status = (ObjectStatus)p.ReadUInt8(); //(0 = None,2 = ??@GrowthPet, 3 = Invincible, 4 = Invisible)
                player.WalkSpeed = p.ReadSingle();
                player.RunSpeed = p.ReadSingle();
                player.HwanSpeed = p.ReadSingle();

                byte active_buffs = p.ReadUInt8(); // Buffs count
                for (int a = 0; a < active_buffs; a++)
                {
                    uint skillID = p.ReadUInt32();
                    string skillLongID = PK2DataGlobal.Skills[skillID].LongId;
                    p.ReadUInt32(); // Temp ID
                    if (skillLongID.StartsWith("SKILL_EU_CLERIC_RECOVERYA_GROUP") || skillLongID.StartsWith("SKILL_EU_BARD_BATTLAA_GUARD") || skillLongID.StartsWith("SKILL_EU_BARD_DANCEA") || skillLongID.StartsWith("SKILL_EU_BARD_SPEEDUPA_HITRATE"))
                    {
                        p.ReadUInt8();
                    }
                }
                player.Name = p.ReadAscii();

                player.JobType = p.ReadUInt8(); // (0 = None, 1 = Trader, 2 = Tief, 3 = Hunter)
                player.JobLevel = p.ReadUInt8(); // Job level
                player.PVPState = p.ReadUInt8();
                player.TransportFlag = p.ReadUInt8();
                player.InCombat = p.ReadUInt8();

                if (player.TransportFlag == 1)
                {
                    player.TransportUniqueID = p.ReadUInt32();
                }

                player.ScrollMode = p.ReadUInt8(); //0 = None, 1 = Return Scroll, 2 = Bandit Return Scroll
                player.InteractionMode = p.ReadUInt8(); //0 = None 2 = P2P, 4 = P2N_TALK, 6 = OPNMKT_DEAL
                p.ReadUInt8();

                player.GuildName = p.ReadAscii();

                if (inventoryContainsJobEquipment == 0)
                {
                    player.GuildID = p.ReadUInt32();
                    player.GuildGrantName = p.ReadAscii();
                    player.GuildLastCrest = p.ReadUInt32();
                    player.UnionID = p.ReadUInt32();
                    player.UnionLastCrest = p.ReadUInt32();
                    player.GuildIsFriendly = p.ReadUInt8();
                    player.GuildSeigeAuthority = p.ReadUInt8();
                }

                if (player.InteractionMode == 4) // Stall
                {
                    string stallName = p.ReadAscii();  // old version p.ReadAscii(12000); // Stall Name UTF-32 bit
                    uint stallRefID = p.ReadUInt32(); // Unknown
                }
                byte equipmentCooldown = p.ReadUInt8();             // 00 
                player.PVPFlag = p.ReadUInt8(); // and PK Flag (0xFF)

                SpawnHandler.AddToBotList(player.UniqueID, player, SpawnListsGlobal.BotLists.Players);
                OnParsed(new SpawnArgs(player, SpawnListsGlobal.BotLists.Players));
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0:x} - {1}\n{2}", p.Opcode, ex.Message, ex.StackTrace));
                Logger.Log(LogLevel.HANDLER, string.Format("{0:x} - {1}\n{2}", p.Opcode, ex.Message, ex.StackTrace));
                throw;
            }
        }
    }
}
