﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Spawn
{
    public class GroupSpawnBeginHandler : Handler
    {
        public static uint SpawnType = 0x01;
        public static ushort RemainingSpawns = 0;

        public override ushort ServerOpcode => 0x3017;

        public GroupSpawnBeginHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            SpawnType = p.ReadUInt8();
            RemainingSpawns = p.ReadUInt16();
            return null;
        }
    }
}
