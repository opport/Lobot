﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System.Windows;

namespace LobotDLL.Packets.Spawn
{
    public class ParseItem : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            ItemDrop drop = new ItemDrop();
            drop.RefID = refID;
            drop.Name = PK2DataGlobal.Items[refID].Name;

            string type = PK2DataGlobal.Items[refID].LongId;
            if (type.StartsWith("ITEM_ETC_GOLD"))
            {
                drop.Amount = p.ReadUInt32(); // Amount
            }
            if (type.StartsWith("ITEM_QSP") || type.StartsWith("ITEM_ETC_E090825") || type.StartsWith("ITEM_QNO") || type == "ITEM_TRADE_SPECIAL_BOX")
            {
                drop.Name = p.ReadAscii(); // Name
            }
            if (type.StartsWith("ITEM_CH") || type.StartsWith("ITEM_EU"))
            {
                drop.Plus = p.ReadUInt8(); // Plus
            }
            drop.UniqueID = p.ReadUInt32(); // ID
            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();
            float x = ScriptUtils.GetXCoord(p.ReadSingle(), xSector);
            p.ReadSingle(); //Z
            float y = ScriptUtils.GetYCoord(p.ReadSingle(), ySector);
            p.ReadUInt16(); // Angle

            byte hasOwner = p.ReadUInt8();
            if (hasOwner == 1)
            {
                drop.OwnerID = p.ReadUInt32(); // FF FF FF FF == free for all
                if (drop.OwnerID == 0xFFFFFFFF) // Owner ID
                {
                    //TODO add to pending pickup list MainData.Drops.DropWaiting = true;
                }
            }
            drop.Rarity = p.ReadUInt8(); // Item Type -> 0 = normal , 1 = blue, 2= sox

            if (p.Opcode == 0x3015)
            {
                byte dropSource = p.ReadUInt8(); // 6 = seems to be self/char
                uint dropperUniqueID = p.ReadUInt32();
            }

            drop.Position = new Point(x, y);
            SpawnHandler.AddToBotList(drop.UniqueID, drop, SpawnListsGlobal.BotLists.Drops);
            OnParsed(new SpawnArgs(drop, SpawnListsGlobal.BotLists.Drops));
        }

    }
}
