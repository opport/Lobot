﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Spawn
{
    // Cast Harmony Therapy circle
    //[S -> C]
    //[3015]
    //FF FF FF FF                                       uint.MaxValue -> Buffcircle
    //54 00                                             unkUShort0 84
    //38 06 00 00                                       RefSkillID 1592
    //4C B4 AD 00                                       UniqueID 11383884
    //8A 5C xSector ySector
    //52 87 34 41 A3 2D 9D 41 50 DC 3B 44 00 00         x 4  z 4  y 4  angle 2
    //01                                                unkn
    class ParseSpecialZone : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            p.ReadUInt16(); // unknUShort
            uint refSkillID = p.ReadUInt32();
            uint uniqueID = p.ReadUInt32();

            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();

            float xOffset = p.ReadSingle();
            float zOffset = p.ReadSingle();
            float yOffset = p.ReadSingle();
            ushort angle = p.ReadUInt16();

            if (p.Opcode == 0x3015)
            {
                byte unkn = p.ReadUInt8();
            }
        }
    }
}
