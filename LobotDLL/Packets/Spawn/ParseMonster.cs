﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Windows;

namespace LobotDLL.Packets.Spawn
{
    public class ParseMonster : AbstractSpawnParseStrategy
    {
        public ParseMonster() { }

        public override void Parse(Packet p, uint refID)
        {
            Monster m = new Monster();

            m.RefID = refID;
            m.UniqueID = p.ReadUInt32(); // unique ID
            byte xSector = p.ReadUInt8();
            byte ySector = p.ReadUInt8();
            int x = ScriptUtils.GetXCoord(p.ReadSingle(), xSector);
            p.ReadSingle(); // z
            int y = ScriptUtils.GetYCoord(p.ReadSingle(), ySector);
            p.ReadUInt16(); // Angle
            byte moving = p.ReadUInt8(); // Moving
            p.ReadUInt8(); // Running
            if (moving == 1)
            {
                xSector = p.ReadUInt8();
                ySector = p.ReadUInt8();
                if (ySector == 0x80)
                {
                    x = p.ReadUInt16() - p.ReadUInt16();
                    p.ReadUInt16(); //z
                    p.ReadUInt16(); // z
                    y = p.ReadUInt16() - p.ReadUInt16();
                }
                else
                {
                    x = ScriptUtils.GetXCoord(p.ReadUInt16(), xSector);
                    p.ReadUInt16(); // z
                    y = ScriptUtils.GetYCoord(p.ReadUInt16(), ySector);
                }
            }
            else
            {
                p.ReadUInt8(); // MovementSource 0 = Spinning, 1 = Sky-/Key-walking
                p.ReadUInt16(); // MovementAngle
            }

            float distance = Math.Abs(x - Bot.CharData.DestX) + Math.Abs((y - Bot.CharData.DestY));

            m.IsAlive = p.ReadUInt8() == 1; // Alive
            p.ReadUInt8(); // Unknown
            p.ReadUInt8(); // Motion state 0 = None, 2 = Walking, 3 = Running, 4 = Sitting
            p.ReadUInt8(); // Status, Zerk Active
            p.ReadSingle(); // Walk Speed
            p.ReadSingle(); // Run Speed
            p.ReadSingle(); // Zerk Speed
            byte buffs = p.ReadUInt8(); // Buffs Count On Mobs
            for (byte i = 0; i < buffs; i++)
            {
                Buff buff = new Buff(p.ReadUInt32());
                m.Buffs.Add(buff); // Skill ID
                p.ReadUInt32(); // Skill Duration

                if (buff.Pk2Entry.IsTransferableBuff()) // buff.RefSkillParam is 1701213281)
                {
                    //1701213281 -> atfe -> "auto transfer effect" like Recovery Division
                    buff.IsCreator = p.ReadUInt8();
                }
            }
            p.ReadUInt8(); // Talk flag
            p.ReadUInt8(); // Rarity
            p.ReadUInt8(); // Appearance
            byte type = p.ReadUInt8();
            m.Type = (MonsterType)type;

            if (m.IsAlive)
            {
                m.RefID = refID;
                //Monsters.Distance.Add(dist);

                if (PK2DataGlobal.NPCs.TryGetValue(m.RefID, out LobotAPI.PK2.Pk2NPC npc))
                {
                    m.MaxHP = npc.HP;
                    m.CurrentHP = (uint)m.MaxHP;
                }

                m.Position = new Point(x, y);

                SpawnHandler.AddToBotList(m.UniqueID, m, SpawnListsGlobal.BotLists.MonsterSpawns);
                OnParsed(new SpawnArgs(m, SpawnListsGlobal.BotLists.MonsterSpawns));
            }
        }
    }
}
