﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Spawn
{
    class ParseOther : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            string type = PK2DataGlobal.NPCs[refID].LongId;
            if (type == "INS_QUEST_TELEPORT")
            {
                Monster m = new Monster();
                m.RefID = refID;
                m.UniqueID = p.ReadUInt32(); // MOB ID
                p.ReadUInt8();
                p.ReadUInt8();
                p.ReadSingle();
                p.ReadSingle();
                p.ReadSingle();
                p.ReadUInt16(); // Angle
                p.ReadUInt8(); // Unknwon
                p.ReadUInt8(); // Unknwon
                p.ReadUInt16(); // Unknwon
                p.ReadAscii();
                p.ReadUInt32();

                //AddToBotList(m.ID, m, ObjectListsGlobal.BotLists.TeleportersNearby);
                OnParsed(new SpawnArgs(m, SpawnListsGlobal.BotLists.None));
            }
        }

    }
}
