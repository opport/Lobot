﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Spawn
{
    /// <summary>
    //   uint	4	RefObjID
    //      if(ITEM_QNO)
    //      {
    //          ushort	2	Owner.Name.Length
    //          string* Owner.Name

    //      }
    //      else if(EQUIPMENT)
    //  {
    //      byte	1	OptLevel
    //  }	
    //      uint	4	UniqueID
    //   ushort	2	RegionID
    //   float	4	XOffset
    //   float	4	ZOffset
    //   float	4	YOffset
    //   ushort	2	Angle
    //  if(!ITEM_QNO)
    //  {
    //	    byte	1	hasOwner
    //	    if(hasOwner)
    //	    {
    //		    uint	4	Owner.UserJID(FF FF FF FF = Me)
    //	    }
    //  }
    //  byte	1	isBlue

    //  if(Opcode == 0x3015)
    //  {
    //	    byte	1	Source	
    //	    switch(Source)
    //	    {
    //		    case 5: //Dropped by Monster
    //			    uint	4	Dropper.UniqueID
    //		        break;

    //		    case 6: //Dropped by Player
    //			    uint	4	Dropper.UniqueID
    //		    break;
    //	    }
    //  }

    //spawn strong straw item (x 6348,y 906)
    //[S -> C][3015]
    //61 24 00 00                                       item 9313
    //03 00                                             ownernameLength 3
    //48 61 7A ownerName = Haz
    //6C 9F F5 02                                       itemID
    //A8 60                                             xsector ysector
    //47 50 EB 42 65 FF 14 40 EF A5 AD 44 EE 82         x,z,y coords 4bytes each, angle 2 bytes
    //01                                                has owner? 0 no 1 yes
    //A9 18 00 00                                       6313
    //00                                                isBlue?
    //05                                                DropSource
    //00 00 00 00                                       DropSourceUniqueID

    // Drop cotton hat 
    //[S -> C][3015]
    //17 03 00 00                                       item 791
    //03                                                ................
    //89 D5 49 0A                                       ..I.............
    //A8 60                                             .`..............
    //CE 13 93 43 C9 3C 0E 41 A8 EE B7 44 14 60         ...C.<.A...D.`..
    //01                                                has owner? 0 no 1 yes
    //FF FF FF FF                                       Owner.UserJID (FF FF FF FF = Me)
    //01                                                isBlue?
    //06                                                DropSource (5 Monster, 6 Player)
    //88 D5 49 0A                                       DropSourceUniqueID

    // Monster Spawn Old Weasel
    //[S -> C][3015]
    //90 07 00 00                                       refID 1936 - 1936,MOB_CH_GYO_CLON,Old Weasel
    //FE 65 00 00                                       objectID
    //AA 61                                             xsector ysector
    //5F C7 84 44 60 96 B0 41 00 78 AA 3F 65 3F         x,z,y coords 4bytes each, angle 2 bytes
    //01                                                movementType 1 => walk
    //01                                                flag ???
    //AA 61                                             xSector ySector
    //29 04                                             x
    //16 00                                             z
    //BC 00                                             y
    //01                                                flag00
    //00                                                flag01
    //03                                                flag02
    //00                                                flag03
    //00 00 40 41                                       ..@A............
    //00 00 5C 42                                       ..\B............
    //00 00 C8 42                                       ...B............
    //00                                                ................
    //02                                                ................
    //01                                                ...............
    //05                                                ................
    //00                                                ...............
    //04                                                ................
    /// </summary>
    public class SingleSpawnHandler : SpawnHandler
    {
        public override ushort ServerOpcode => 0x3015;

        public SingleSpawnHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            GroupSpawnBeginHandler.RemainingSpawns++;
            Spawn(p, 0);
            return null;
        }
    }
}
