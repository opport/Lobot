﻿using LobotAPI;

using LobotAPI.Packets;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Spawn
{
    //                numberOfSpawns 2
    //8D 07 00 00                                       refID-- > mangyang
    //58 4F 00 00                                       objectID
    //A5 60                                             xSector ySector
    //A2 3B CC 44 51 79 01 43 EC 8F 5E 44 70 D4         4bytes(x, z, y, ???) 2 bytes angle
    //01                                                movementType 1 => walk
    //00                                                flag ???
    //A5 60                                             xSector ySector
    //61 06                                             x
    //81 00                                             z
    //7A 03                                             y
    //01                                                flag00
    //00                                                flag01
    //00                                                flag02
    //00                                                flag03
    //00 00 00 41 ???
    //00 00 B0 41 ???
    //00 00 C8 42 ???
    //00................
    //02................                                // unkn00
    //01................
    //05................
    //00................

    // Spawning npc - jangan beggar
    // [S -> C][3019]
    //01                                                spawnflag
    //01 00                                             numberOfSpawns
    //F7 07 00 00                                       refID = 2039
    //13 01 00 00                                       objectID(for npc list)
    //A7 61                                             xSector ySector
    //E1 42 AD 44 1C A2 82 41 B8 EE 07 44 B5 40         .B.D...A...D.@..
    //00                                                flag01 0
    //01                                                flag02 1
    //00                                                flag03 0
    //B5 40                                             flag 04 1205
    //01                                                flag05 1
    //00                                                flag06 0
    //00                                                flag07 0
    //00                                                flag08 0
    //00 00 00 00                                       walkSpeed float
    //00 00 00 00                                       runSpeed float
    //00 00 C8 42                                       zerkSpeed float
    //00                                                flag12 0
    //02                                                flag13 2        // unkn00
    //01                                                flag14 1
    //02                                                flag15 2
    public class GroupSpawnHandler : SpawnHandler
    {
        public override ushort ServerOpcode => 0x3019;

        public GroupSpawnHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            int spawns = GroupSpawnBeginHandler.RemainingSpawns;

            if (GroupSpawnBeginHandler.SpawnType == 1) // (spawntype == 1)  spawn
            {
                for (int i = 0; i < spawns; i++) // numberOfObjects; i++)
                {
                    Spawn(p, i);
                }
            }
            else if (GroupSpawnBeginHandler.SpawnType == 2) //(spawntype == 2) despawn
            {
                for (int i = 0; i < spawns; i++) // numberOfObjects; i++)
                {
                    try
                    {
                        uint objectID = p.ReadUInt32();
                        Despawn(objectID);
                    }
                    catch (Exception e)
                    {
                        Logger.Log(LogLevel.HANDLER, string.Format("Despawn: {0}\n{1}", e.Message, e.StackTrace));
                    }
                }
            }

            return null;
        }
    }
}
