﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using LobotDLL.Packets.Pet;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;
using System.Linq;

namespace LobotDLL.Packets.Spawn
{
    /// <summary>
    /// The abstract parentclass to the GroupSpawn- and SingleSpawnHandler class.
    /// </summary>
    public abstract class SpawnHandler : Handler
    {
        public static event EventHandler<DespawnArgs> Despawned = delegate { };

        protected SpawnHandler() { }

        public static void AddToBotList(uint uniqueID, object o, SpawnListsGlobal.BotLists listNumber)
        {
            switch (listNumber)
            {
                case SpawnListsGlobal.BotLists.None:
                    return;
                case SpawnListsGlobal.BotLists.Drops:
                    if (o is ItemDrop && !SpawnListsGlobal.Drops.Any(kvp => kvp.Key == uniqueID))
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        SpawnListsGlobal.Drops[uniqueID] = o as ItemDrop;
                    }
                    break;
                case SpawnListsGlobal.BotLists.MonsterSpawns:
                    if (o is Monster && !SpawnListsGlobal.MonsterSpawns.Any(kvp => kvp.Key == uniqueID))
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        SpawnListsGlobal.MonsterSpawns[uniqueID] = o as Monster;
                    }
                    break;
                case SpawnListsGlobal.BotLists.NPCs:
                    if (o is Monster && !SpawnListsGlobal.NPCSpawns.Any(kvp => kvp.Key == uniqueID))
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        SpawnListsGlobal.NPCSpawns[uniqueID] = o as Monster;
                    }
                    break;
                case SpawnListsGlobal.BotLists.Portals:
                    if (o is Portal && !SpawnListsGlobal.Portals.Any(kvp => kvp.Key == uniqueID))
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        SpawnListsGlobal.Portals[uniqueID] = o as Portal;
                    }
                    break;
                case SpawnListsGlobal.BotLists.Players:
                    if (o is CharData && !SpawnListsGlobal.PlayerList.Any(player => player.RefID == uniqueID))
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        SpawnListsGlobal.PlayerList.Add(o as CharData);
                    }
                    break;
                case SpawnListsGlobal.BotLists.Pets:
                    if (o is LobotAPI.Pet /*&& !ObjectListsGlobal.Pets.Any(player => player.RefObjID == uniqueID)*/)
                    {
                        SpawnListsGlobal.AllSpawnEntries[uniqueID] = listNumber;
                        //ObjectListsGlobal.Pets.Add(o as CharData);
                        //if ((o as LobotAPI.Pet).OwnerID == Bot.CharData?.UniqueID)
                        //{
                        //    switch ((o as LobotAPI.Pet).Pk2Entry.Type)
                        //    {
                        //        case LobotAPI.PK2.Pk2NPCType.PetAbility:
                        //            Bot.GrowthPet = o as GrowthPet;
                        //            break;
                        //        case LobotAPI.PK2.Pk2NPCType.PetPickup:
                        //            Bot.PickupPet = o as LobotAPI.Pet;
                        //            break;
                        //        case LobotAPI.PK2.Pk2NPCType.PetVehicle:
                        //        case LobotAPI.PK2.Pk2NPCType.PetTransport:
                        //        default:
                        //            Bot.Transport = o as TransportPet;
                        //            break;
                        //    }
                        //}
                    }
                    break;
                default:
                    break;
            }
        }

        protected static void Spawn(Packet p, int errorResetIndex)
        {
            uint refID = p.ReadUInt32();

            string type;

            if (PK2DataGlobal.NPCs.TryGetValue(refID, out LobotAPI.PK2.Pk2NPC npc))
            {
                type = npc.LongId;
            }
            else if (PK2DataGlobal.Items.TryGetValue(refID, out LobotAPI.PK2.Pk2Item item))
            {
                type = item.LongId;
            }
            else if (PK2DataGlobal.Teleporters.TryGetValue(refID, out LobotAPI.PK2.Pk2Teleporter portal))
            {
                type = "PORTAL"; // changed from simply taking portal.LongId;
            }
            else if (refID == uint.MaxValue) //EVENT_ZONE (Traps, Buffzones, ...)
            {
                new ParseSpecialZone().Parse(p, refID);
                return;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0:x} - RefID {1:x} not found", p.Opcode, refID));
                Logger.Log(LogLevel.HANDLER, string.Format("{0:x} - RefID {1:x} not found", p.Opcode, refID));
                Logger.Log(LogLevel.HANDLER, string.Format("gw_local_send_buffers {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
                for (int j = errorResetIndex; j < GroupSpawnBeginHandler.RemainingSpawns; j++)
                {
                    ReduceRemainingSpawnsCounter(); // In case of error, don't handle but reset up for next split packet
                }

                return;
            }

            if (type.StartsWith("ITEM_"))
            {
                new ParseItem().Parse(p, refID);
            }
            else if (type.Contains("CHAR_"))
            {
                new ParseChar().Parse(p, refID);
            }
            else if (type.Contains("PORTAL")) // "_GATE" too broad, can be found in normal NPCs
            {
                new ParsePortal().Parse(p, refID);
            }
            else if (type.Contains("COS_"))
            {
                new ParsePet().Parse(p, refID);
            }
            else if (type.Contains("MOB_"))
            {
                new ParseMonster().Parse(p, refID);
            }
            else if (type.Contains("NPC_"))
            {
                new ParseNPC().Parse(p, refID);
            }
            else
            {
                new ParseOther().Parse(p, refID);
            }

            ReduceRemainingSpawnsCounter();
        }

        protected static void Despawn(uint objectID)
        {
            if (SpawnListsGlobal.AllSpawnEntries.TryGetValue(objectID, out SpawnListsGlobal.BotLists listNumber))
            {
                if (listNumber == SpawnListsGlobal.BotLists.Pets && objectID == Bot.GrowthPet?.UniqueID)
                {
                    AutoPotions.HGPTimer.Stop();
                    PetUpdateHandler.HGPChanged -= AutoPotions.HandleHGPChanged;
                }
                SpawnListsGlobal.RemoveFromBotList(objectID, listNumber);
                ReduceRemainingSpawnsCounter();
                OnDespawned(new DespawnArgs(objectID, listNumber));
            }
        }

        protected static void ReduceRemainingSpawnsCounter()
        {
            //Reduce remaining spawns/despawns in 3017 handler
            if (GroupSpawnBeginHandler.RemainingSpawns > 0)
            {
                GroupSpawnBeginHandler.RemainingSpawns--;
            }
            else
            {
                GroupSpawnBeginHandler.RemainingSpawns = 0;
            }
        }

        private static void OnDespawned(DespawnArgs e)
        {
            EventHandler<DespawnArgs> handler = Despawned;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}