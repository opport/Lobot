﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using static LobotAPI.Globals.SpawnListsGlobal;

namespace LobotDLL.Packets.Spawn
{
    /// <summary>
    //[S -> C][3019]
    //01                                                spawnFlag(1 = Spawn)
    //01 00                                             Quantity 1
    //70 59 00 00                                       refID 22896,STORE_TQ_SNAKEGATE,Gate of Viper
    //0F 00 00 00                                       objectID
    //A8 62                                             xSector, ySector
    //00 80 E5 44 00 00 A0 41 00 00 DC 43 00 00         x,z,y coordinates
    //01                                                ................
    //00                                                ................
    //00                                                ................
    //01                                                ................
    //00 00 00 00                                       ................
    //00 00 00 00                                       ................
    /// </summary>
    public class ParsePortal : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            Portal portal = new Portal();
            portal.RefID = refID;

            if (PK2DataGlobal.Teleporters.TryGetValue(refID, out LobotAPI.PK2.Pk2Teleporter data))
            {
                portal.PK2Data = data;
            }

            portal.UniqueID = p.ReadUInt32();
            byte xSec = p.ReadUInt8(); // xSector
            byte ySec = p.ReadUInt8(); // ySector
            float x = p.ReadSingle(); // x
            float z = p.ReadSingle(); // z
            float y = p.ReadSingle(); // y
            p.ReadUInt16(); // angle
            p.ReadUInt8(); // unk0
            byte unk1 = p.ReadUInt8(); // unk1
            p.ReadUInt8(); // unk2
            byte unk3 = p.ReadUInt8(); // unk3

            if (unk3 == 1) // Normal teleporter
            {
                p.ReadUInt32(); // unkUInt0
                p.ReadUInt32(); // unkUInt1
            }
            else if (unk3 == 6) // Dimension hole
            {
                //p.ReadUInt16(); // OwnerNameLength
                p.ReadAscii(); // OwnerName
                p.ReadUInt32(); // OwnerUniqueID
            }

            if (unk1 == 1) // Store event zone default
            {
                p.ReadUInt32(); // unkUInt2 
                p.ReadUInt8(); // unkn4
            }

            portal.Position = new System.Windows.Point(ScriptUtils.GetXCoord(x, xSec), ScriptUtils.GetXCoord(y, xSec));
            SpawnHandler.AddToBotList(portal.UniqueID, portal, BotLists.Portals);
            OnParsed(new SpawnArgs(portal, SpawnListsGlobal.BotLists.Portals));
        }
    }
}
