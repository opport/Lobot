﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Spawn
{
    // Spawn (2F 81 5E 06)
    //[S -> C][3015]
    //6F 1C 00 00                                       o...............
    //2F 81 5E 06                                       /.^.............
    //89 5C                                             .\..............
    //B2 80 8D 44 F9 B8 F5 BF 0C 95 C7 41 D0 6F         ...D.......A.o..
    //01                                                ................
    //2E 51 00 00                                       .Q..............
    //00                                                ................
    //05                                                ................
    //2E 81 5E 06                                       ..^.............

    // Free to pick
    //[S -> C][304D]
    //2F 81 5E 06                                       /.^.............
    public class DropChangeOwnerHandler : Handler
    {
        public static event EventHandler<SpawnArgs> DropOwnerChanged = delegate { };

        private static void OnDropOwnerChanged(SpawnArgs e)
        {
            EventHandler<SpawnArgs> handler = DropOwnerChanged;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public DropChangeOwnerHandler() { }

        public override ushort ServerOpcode => 0x304D;

        protected override ICommand ParsePacket(Packet p)
        {
            uint uniqueID = p.ReadUInt32();
            if (SpawnListsGlobal.TryGetSpawnObject(uniqueID, out ISpawnType spawn)
                && spawn is ItemDrop)
            {
                (spawn as ItemDrop).OwnerID = 0xFFFFFFFF;
                OnDropOwnerChanged(new SpawnArgs(spawn, SpawnListsGlobal.BotLists.Drops));
            }
            return null;
        }
    }
}
