﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Packets.Pet;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System.Collections.Generic;
using System.Windows;

namespace LobotDLL.Packets.Spawn
{
    public class ParsePet : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            LobotAPI.Pet pet = new LobotAPI.Pet();
            pet.RefID = refID;

            pet.UniqueID = p.ReadUInt32(); // PET ID

            // Use old data if pet is being relocated by the server after getting stuck
            if (pet.UniqueID == Bot.GrowthPet.UniqueID)
            {
                pet = new GrowthPet(Bot.GrowthPet);
            }

            byte xsec = p.ReadUInt8();
            byte ysec = p.ReadUInt8();
            float xcoord = p.ReadSingle();
            p.ReadSingle();
            float ycoord = p.ReadSingle();
            ushort angle = p.ReadUInt16(); // Position
            pet.Position = new Point(ScriptUtils.GetXCoord(xcoord, xsec), ScriptUtils.GetYCoord(ycoord, ysec));

            byte move = p.ReadUInt8(); // Moving
            p.ReadUInt8(); // Running

            if (move == 1)
            {
                xsec = p.ReadUInt8();
                ysec = p.ReadUInt8();
                if (ysec == 0x80)
                {
                    xcoord = p.ReadUInt16() - p.ReadUInt16();
                    p.ReadUInt16();
                    p.ReadUInt16();
                    ycoord = p.ReadUInt16() - p.ReadUInt16();
                }
                else
                {
                    xcoord = p.ReadUInt16();
                    p.ReadUInt16();
                    ycoord = p.ReadUInt16();
                }
            }
            else
            {
                p.ReadUInt8(); // Unknown
                p.ReadUInt16(); // Unknwon
            }
            //p.ReadUInt8(); spare byte from old version
            pet.IsAlive = p.ReadUInt8() == 1 ? true : false;
            p.ReadUInt8();
            p.ReadUInt8();
            p.ReadUInt8();
            p.ReadSingle(); // Walk speed
            float speed = p.ReadSingle(); // Run speed
            p.ReadSingle(); // Zerk Speed
            //Pets.Speed.Add(speed); 

            byte buffCount = p.ReadUInt8(); // 
            for (int i = 0; i < buffCount; i++)
            {
                Buff buff = new Buff(p.ReadUInt32());
                buff.BuffDuration = p.ReadUInt32(); //Buff.Duration
                if (buff.Pk2Entry.IsTransferableBuff()) //buff.RefSkillParam is 1701213281)
                {
                    //1701213281 -> atfe -> "auto transfer effect" like Recovery Division
                    buff.IsCreator = p.ReadUInt8();
                }
            }
            byte talkFlag = p.ReadUInt8(); // Buffcount
            if (talkFlag == 2)
            {
                byte talkOptionCount = p.ReadUInt8();
                List<byte> talkOptions = new List<byte>();
                for (int i = 0; i < talkOptionCount; i++)
                {
                    talkOptions.Add(p.ReadUInt8());
                }
            }

            Pk2NPCType type = PK2DataGlobal.NPCs[refID].Type;
            if (type == Pk2NPCType.PetVehicle)
            {
                //Do Nothing
            }
            else if (type == Pk2NPCType.PetPickup)
            {
                pet.CustomName = p.ReadAscii(); //Pet Name
                pet.OwnerName = p.ReadAscii(); //Owner Name
                p.ReadUInt8(); //Unknown ( Always ?? 4 )
                pet.OwnerID = p.ReadUInt32(); //Looks like Pet Owner ID

                if (pet.OwnerID == Bot.CharData.UniqueID)
                {
                    Bot.PickupPet = pet;
                }
            }
            else if (type == Pk2NPCType.PetAbility)
            {
                pet.CustomName = p.ReadAscii(); //Pet Name
                pet.OwnerName = p.ReadAscii(); //Owner Name

                byte jobType = p.ReadUInt8(); //Unknown ( Always ?? 4 )
                byte murderFlag = p.ReadUInt8(); //0 = White, 1 = Purple, 2 = Red

                pet.OwnerID = p.ReadUInt32(); //Looks like Pet Owner ID

                // Prepare for the worst with Packet copy
                //Packet backupPacket = new Packet(p);
                //try
                //{
                //    byte b1 = p.ReadUInt8();
                //    byte b2 = p.ReadUInt8();
                //    byte b3 = p.ReadUInt8();
                //    byte b4 = p.ReadUInt8();
                //    if (b1 == 255 && b2 == 255 & b3 == 255 && b4 == 255)
                //    {
                //        p.ReadUInt16();
                //    }
                //    else
                //    {
                //        p = backupPacket;
                //    }
                //}
                //catch { }
                if (pet.OwnerID == Bot.CharData.UniqueID)
                {
                    Bot.GrowthPet.SetFromPet(pet);
                    AutoPotions.HGPTimer.Start();
                    PetUpdateHandler.HGPChanged += AutoPotions.HandleHGPChanged;
                    pet = new GrowthPet(pet);
                }
            }
            else if (type == Pk2NPCType.PetTransport || type == Pk2NPCType.PetTransportMall)//type.Contains("COS_T"))
            {
                pet.OwnerName = p.ReadAscii(); //Owner Name
                byte jobType = p.ReadUInt8();

                if (jobType != 4)
                {
                    byte murderFlag = p.ReadUInt8(); //0 = White, 1 = Purple, 2 = Red
                }

                if (type == Pk2NPCType.PetGuildCH || type == Pk2NPCType.PetGuildEU)
                {
                    uint ownerRefID = p.ReadUInt32();
                }

                pet.OwnerID = p.ReadUInt32();

                if (pet.OwnerID == Bot.CharData.UniqueID)
                {
                    Bot.Transport.SetFromPet(pet);
                    pet = new TransportPet(pet);
                }
            }
            //else if (type.StartsWith("TRADE_COS_QUEST_TRADE")) // TODO item with type does not exist on some servers
            //{
            //    pet.OwnerName = p.ReadAscii(1200); //Owner Name
            //    p.ReadUInt16(); //Unknwon
            //    pet.OwnerID = p.ReadUInt32(); //Owner ID
            //}
            else
            {
                pet.OwnerName = p.ReadAscii(1200); //Owner Name
                p.ReadUInt8();
                p.ReadUInt8();
                p.ReadUInt32();
            }

            pet.Position = new System.Windows.Point(ScriptUtils.GetXCoord(xcoord, xsec), ScriptUtils.GetYCoord(ycoord, ysec));
            SpawnHandler.AddToBotList(pet.UniqueID, pet, SpawnListsGlobal.BotLists.Pets);
            OnParsed(new SpawnArgs(pet, SpawnListsGlobal.BotLists.Pets));
        }
    }
}
