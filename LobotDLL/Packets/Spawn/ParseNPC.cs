﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Spawn
{
    public class ParseNPC : AbstractSpawnParseStrategy
    {
        public override void Parse(Packet p, uint refID)
        {
            uint objectID = p.ReadUInt32();
            string type = PK2DataGlobal.NPCs[refID].LongId;
            byte xsec = p.ReadUInt8();
            byte ysec = p.ReadUInt8();
            float xcoord = p.ReadSingle();
            p.ReadSingle();
            float ycoord = p.ReadSingle();

            p.ReadUInt16(); // Position
            byte move = p.ReadUInt8(); // Moving
            p.ReadUInt8(); // Running

            if (move == 1)
            {
                xsec = p.ReadUInt8();
                ysec = p.ReadUInt8();
                if (ysec == 0x80)
                {
                    xcoord = p.ReadUInt16() - p.ReadUInt16();
                    p.ReadUInt16();
                    p.ReadUInt16();
                    ycoord = p.ReadUInt16() - p.ReadUInt16();
                }
                else
                {
                    xcoord = p.ReadUInt16();
                    p.ReadUInt16();
                    ycoord = p.ReadUInt16();
                }
            }
            else
            {
                p.ReadUInt8(); // Unknown
                p.ReadUInt16(); // Unknown
            }

            p.ReadUInt8(); // Alive
            //p.ReadUInt8();
            p.ReadUInt8(); // Unknown
            p.ReadUInt8(); // Unknown
            p.ReadUInt8(); // Zerk Active
            p.ReadSingle(); // Walk Speed
            p.ReadSingle(); // Run Speed
            p.ReadSingle(); // Zerk Speed

            p.ReadUInt8(); // buffs

            byte talkFlag = p.ReadUInt8();
            if (talkFlag != 0)
            {
                byte talkOptionsCount = p.ReadUInt8();
                for (byte i = 0; i < talkOptionsCount; i++)
                {
                    p.ReadUInt8();
                }
            }

            Monster m = new Monster();
            m.UniqueID = objectID;
            m.RefID = refID;
            m.CurrentHP = (uint)PK2DataGlobal.NPCs[refID].HP;
            m.Position = new System.Windows.Point(ScriptUtils.GetXCoord(xcoord, xsec), ScriptUtils.GetYCoord(ycoord, ysec));

            SpawnHandler.AddToBotList(m.UniqueID, m, SpawnListsGlobal.BotLists.NPCs);
            OnParsed(new SpawnArgs(m, SpawnListsGlobal.BotLists.NPCs));
        }
    }
}
