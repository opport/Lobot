﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    //drop ume copper ring
    //[C -> S][7034]
    //07                                                movementType
    //24                                                fromSlot

    //[S -> C][B034]
    //01                                                successFlag
    //07                                                movementType
    //24                                                fromSlot
    public class MoveDrop : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.Drop;

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.Inventory.Remove(p.FromSlot);
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.ToSlot);
            return p.Packet;
        }
    }
}
