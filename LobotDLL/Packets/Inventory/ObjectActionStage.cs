﻿namespace LobotDLL.Packets.Inventory
{
    public enum ObjectActionStage : byte
    {
        Start = 0x01,
        End = 0x02
    }
}
