﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    // update size
    //[S -> C][3092]
    //01                                                success flag
    //37                                                size 55
    public class InventoryUpdateSizeHandler : Handler
    {
        public override ushort ServerOpcode => 0x3092;

        public InventoryUpdateSizeHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x1)
            {
                Bot.CharData.Inventory.Size = p.ReadUInt8();
            }
            return null;
        }
    }
}
