﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    public class MovePetToInventory : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.PetToInventory;
        public static event EventHandler<ItemMoveArgs> ItemMoved = delegate { };

        private void OnItemMoved(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemMoved;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.Inventory[p.ToSlot] = CharInfoGlobal.PetInventory[p.FromSlot];
            CharInfoGlobal.PetInventory.Remove(p.FromSlot);
            OnItemMoved(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            p.Packet.WriteUInt32(Bot.PickupPet.RefID);
            return p.Packet;
        }
    }
}
