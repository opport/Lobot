﻿using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    /// <summary>
    /// This packet is used to update the bot inventory for townscript item buys.
    /// The client does not expect any Item changes (B034), so this packet is sent to the client to tell it that there is an incoming change.
    /// This allows simulating an item being picked up
    /// 
    //[C -> S][7074]
    //01                                                ................
    //02                                                ................
    //01                                                ................
    //D6 3A F8 01                                       .:..............

    // Fake item pickup starts here
    //[S -> C] [B074]
    //01                                                object stage = 1 starting
    //01                                                success 1

    // This packet can be sent to the client to update the inventory
    //[S -> C] [B034]
    //01                                                ................
    //06                                                ................
    //32                                                2...............
    //00 00 00 00                                       ................
    //0D 00 00 00                                       ................
    //20 00                                             ................

    //[S -> C] [B074]
    //02                                                object stage = 2 ending
    //00                                                ................

    /// </summary>
    public class FakeItemSuccess : Command
    {
        // 
        private ObjectActionStage Stage;

        public override ushort Opcode => 0x7034;

        public FakeItemSuccess(ObjectActionStage stage = ObjectActionStage.Start)
        {
            Stage = stage;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);

            if (Stage == ObjectActionStage.Start)
            {
                p.WriteUInt8(0x01);
                p.WriteUInt8(0x01);
            }
            else
            {
                p.WriteUInt8(0x02);
                p.WriteUInt8(0x00);
            }
            return p;
        }
    }
}
