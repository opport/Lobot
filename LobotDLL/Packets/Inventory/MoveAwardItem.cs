﻿using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;

namespace LobotDLL.Packets.Inventory
{
    //item move
    //[S -> C][B034]
    //01                                                success
    //0E                                                movementType 14 -> item create spawn
    //27                                                target
    //00                                                fromSlot?
    //00 00 00 00                                       unknownFlag(owner? => 0 == self)
    //68 5E 00 00                                       reward item 24168 (Beginner scroll of experience(150%))
    //03 00                                             amount 3
    public class MoveAwardItem : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.AwardItem;

        public void HandleMove(ItemMoveParams p)
        {
            Item item = new Item();
            item.Slot = p.Packet.ReadUInt8();
            byte fromSlot = p.Packet.ReadUInt8();

            ParseNewItem(ref item, p);
        }
    }
}
