﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.PK2;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    /// <summary>
    // Use X-Large HP Potion in itemSlot 1
    //[C -> S][704C]
    //0D                                                itemSlot(13)
    //EC 08                                             unknown(2284) https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4396959-0x704c-item-type-calculation-vsro.html

    //[S -> C] [B04C]
    //01                                                successFlag
    //0D                                                itemSlot(13)
    //E7 03                                             stackAmount(999)
    //EC 08                                             unknown(2284)

    // using mppot
    //[C -> S][704C]
    //2A                                                *...............
    //EC 10                                             mppot type ec 236 main type & 10 item type

    //[S->C][B04C]
    //01                                                ................
    //2A*...............
    //08 00                                             ................
    //EC 10                                             ................

    // hp pot
    //[C -> S] [704C]
    //28                                                (...............
    //EC 08                                             hppot type ec 236 main type & 08 item type

    //[S->C][B04C]
    //01                                                ................
    //28                                                (...............
    //0E 00                                             ................
    //EC 08                                             ................

    // vigour
    //[C -> S] [704C]
    //2C                                                ,...............
    //EC 18                                             vigour type ec 236 main type & 18 item type

    //[S->C][B04C]
    //01                                                ................
    //2C                                                ,...............
    //15 00                                             ................
    //EC 18                                             ................

    // universal pill
    //[C -> S] [704C]
    //2B                                                +...............
    //6C 31                                             universal pill type 6c & 31

    //[S->C][B04C]
    //01                                                ................
    //2B                                                +...............
    //36 00                                             6...............
    //6C 31                                             universal pill type 6c & 31
    /// </summary>
    public class UseItem : BidirectionalPacket
    {
        private readonly byte ItemSlot;
        private readonly UseItemType Type;
        private readonly uint TargetID;
        private readonly byte TargetSlot;

        public override ushort Opcode => 0x704C;
        public override ushort ServerOpcode => 0xB04C;

        public static event EventHandler<ItemUseArgs> ItemUsed = delegate { };

        protected void OnItemUsed(ItemUseArgs e)
        {
            EventHandler<ItemUseArgs> handler = ItemUsed;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public UseItem() { }

        public UseItem(byte itemSlot, UseItemType type = UseItemType.Normal, uint targetID = 0, byte targetSlot = 0)
        {
            ItemSlot = itemSlot;
            Type = type;
            TargetID = targetID;
            TargetSlot = targetSlot;
        }

        /// <summary>
        /// Calculate the item-use-type using the item's cashItem/Item mall value, the value 3, and the item's3-hex pk2Itemtype.
        /// E.g. for an hp pot the value would be 2284, using
        /// 1	4	ITEM_ETC_HP_POTION_01	HP ?? ??	xxx	SN_ITEM_ETC_HP_POTION_01	SN_ITEM_ETC_HP_POTION_01_TT_DESC	0	0	3	3	1	1	180000	3	0	1	1	1	255	3	1	0	0	1	0	60	0	0	0	1	21	-1	0	-1	0	-1	0	-1	0	-1	0	0	0	0	0	0	0	100	0	0	0	xxx	item\etc\drop_ch_bag.bsr	item\etc\hp_potion_01.ddj	xxx	xxx	1000	2	0	0	1	0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0	0	0	0	0	0	0	0	0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	0.0	120	HP???	0	HP???(%)	0	MP???	0	MP???(%)	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	-1	xxx	0	0
        /// the important part starts after the 7th column:	0	0	3	3	1	1
        /// with 0(cashItem)    0(Bionic)    3(type1--isItem (always 3))    3(type2--{1 = equip; 2 = pets; 3 = etc}) 1(type3--itemtype)  1(type4--itemsubtype)
        /// 
        ///result should be EC 08 -> 8EC ->  2284
        ///CashItem(0) + type1(3*4) + type2(3*32) + type3(1*128) + type4(1* 2048)
        ///= 0 + 12 + 96 + 128 + 2048
        ///= 2248
        /// </summary>
        /// <param name="cashItem"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static ushort CalculateItemType(int cashItem, Pk2ItemType type)
        {
            uint type1 = 3;
            uint type2 = (uint)type >> 8;
            uint type3 = ((uint)type & 0x0F0) >> 4;
            uint type4 = (uint)type & 0x00F;

            // itemType = CashItem + (TypeID1 * 4) + (TypeID2 * 32) + (TypeID3 * 128) + (TypeID4 * 2048)
            return (ushort)(cashItem + (type1 * 4) + (type2 * 32) + (type3 * 128) + (type4 * 2048));
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            p.WriteUInt8(ItemSlot);

            if (CharInfoGlobal.Inventory.TryGetValue(ItemSlot, out LobotAPI.Structs.Item item))
            {
                p.WriteUInt16(CalculateItemType(item.IsMallItem ? 1 : 0, item.Type));
            }

            switch (Type)
            {
                case UseItemType.Normal:
                    break;
                case UseItemType.UseOnPet:
                    if (Bot.Transport?.UniqueID == TargetID || Bot.GrowthPet?.UniqueID == TargetID)
                    {
                        p.WriteUInt32(TargetID);
                    }
                    break;
                case UseItemType.UseOnItem:
                    if (CharInfoGlobal.Inventory.TryGetValue(TargetSlot, out Item value))
                    {
                        p.WriteUInt8(TargetSlot);
                    }
                    break;
                default:
                    break;
            }

            return p;
        }

        //[S -> C][B04C]
        //01                                                successFlag
        //26                                                slot
        //00 00                                             newAmount
        //EC 08                                             itemUseType
        protected override ICommand ParsePacket(Packet p)
        {
            byte success = p.ReadUInt8();

            if (success == 0x01)
            {
                byte slot = p.ReadUInt8();
                ushort newAmount = p.ReadUInt16();

                // Reduce stack count of consumables (ITEM_ETC) or remove them entirely from the inventory, if their stack count reaches 0.
                if (CharInfoGlobal.Inventory.TryGetValue(slot, out Item item)
                    && item.Pk2Item.IsConsumable())
                {
                    item.StackCount--;

                    if (item.StackCount < 1)
                    {
                        CharInfoGlobal.Inventory.Remove(item.Slot);
                    }

                    OnItemUsed(new ItemUseArgs(slot, item));
                }
            }

            return null;
        }
    }
}
