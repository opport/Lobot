﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;
using System.Linq;

namespace LobotDLL.Packets.Inventory
{
    // Buy 2 cotton wristlets from protector trader jangan (Miss Jang)
    //[C -> S][7034]
    //08                                                movementType 8 buy
    //02                                                itemTab 2
    //0C                                                itemSlot 12
    //02 00                                             quantity
    //5D 00 00 00                                       npcID

    //[S -> C][B034]
    //01                                                successFlag
    //08                                                movementType 8 buy
    //02                                                itemTab 2
    //0C                                                itemTab 12
    //02                                                quantity 2 (for unstackable items like Equipment) 
    //26                                                toSlot
    //27                                                toSlot
    //02 00                                             stackAmount 2
    //00 00 00 00                                       recipientNPCID ? 0 => self
    //00 00 00 00                                       recipientNPCID ? 0 => self

    // Buy herb from jangan herbalist (Yangyun)
    //[C -> S][7034] // 0x7034, CLIENT_ITEM_MOVE
    //08                                                movementType 8 buy
    //00                                                itemTab 0
    //01                                                itemSlot 1
    //03 00                                             amount 3
    //02 01 00 00                                       npcID

    //[S -> C][B034]
    //01                                                flag
    //08                                                movementType 8 buy
    //00                                                itemTab 0
    //01                                                itemSlot 1
    //01                                                quantity
    //25                                                toSlot
    //03 00                                             stackAmount 3
    //00 00 00 00                                       recipientNPCID ? 0 => self
    public class MoveBuy : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.Buy;

        public static event EventHandler<ItemMoveArgs> ItemBought = delegate { };

        private void OnItemBought(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemBought;
            if (handler != null)
            {
                handler(null, e);
            }
        }
        public static Item FindItem(uint selectedNPC, byte tab, byte slot)
        {
            if (PK2DataGlobal.Shops.TryGetValue((int)selectedNPC, out Shop shop))
            {
                uint itemID = shop.ShopItemEntries.Values.FirstOrDefault(i => i.TabNumber == tab && i.SlotNumber == slot).ItemID;

                if (itemID != 0x00)
                {
                    LobotAPI.PK2.Pk2Item item = PK2DataGlobal.Items[itemID];
                    return new Item(item);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(string.Format($"Item tab {tab} slot {slot} not found in npc {shop.Pk2Entry.Name} - {shop.NPCID.ToString("X")}"));
                    Logger.Log(LogLevel.SCRIPT, string.Format($"Item tab {tab} slot {slot} not found in npc {shop.Pk2Entry.Name} - {shop.NPCID.ToString("X")}"));

                    return default(Item);
                }

            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format($"Shop {selectedNPC} not found"));
                Logger.Log(LogLevel.SCRIPT, $"Shop {selectedNPC} not found");
                return null;
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            byte tab = p.Packet.ReadUInt8();
            byte slot = p.Packet.ReadUInt8();
            Item item = FindItem(Bot.SelectedNPC.RefID, tab, slot) ?? new Item(); // not found should give dummy item to prevent crash
            byte quantity = p.Packet.ReadUInt8();

            byte[] toSlot = new byte[quantity];

            for (int i = 0; i < quantity; i++)
            {
                toSlot[i] = p.Packet.ReadUInt8();
            }

            item.StackCount = p.Packet.ReadUInt8();

            for (int i = 0; i < quantity; i++)
            {
                uint recipientID = p.Packet.ReadUInt32(); //unknown ? self?

                if (recipientID == 0x0)
                {
                    CharInfoGlobal.Inventory[toSlot[i]] = item;
                    OnItemBought(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
                }
            }
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            base.MoveItem(p);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}
