﻿using System;

namespace LobotDLL.Packets.Inventory
{
    public class MoveItemErrorArgs : EventArgs
    {
        public MoveItemError ErrorType;

        public MoveItemErrorArgs(MoveItemError errorTypes)
        {
            ErrorType = errorTypes;
        }
    }
}
