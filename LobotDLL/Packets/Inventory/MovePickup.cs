﻿using LobotAPI;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    public class MovePickUp : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        // pickup sassi charm
        //[S -> C] [B034]
        //01                                                successFlag
        //06                                                movementType (6 = add to inventory after pickup)
        //26                                                slot
        //00 00 00 00                                       unknownFlag (owner? => 0 == self)
        //BF 1B 00 00                                       RefID (7103 sassi charm)
        //01 00                                             amount

        // pickup sosun copper bow +7
        //[S -> C] [B034]
        //01                                                successFlag
        //06                                                movementType
        //27                                                slot
        //00 00 00 00                                       unknownFlag (owner?)
        //40 10 00 00                                       RefID
        //07                                                enhancement +7
        //00 00 00 00 00 00 00 00                           attributes
        //28 23 00 00                                       durability
        //02                                                blue (str 4 int 4)
        //44 00 00 00 04 00 00 00                           68 4
        //4A 00 00 00 04 00 00 00                           74 4
        //01                                                ................
        //00                                                ................
        //02                                                ................
        //00                                                ................

        // pickup gold
        // target gold
        //[C -> S] [7074]
        //01                                                ................
        //02                                                ................
        //01                                                ................
        //80 70 88 01                                       .p..............

        //[S->C][B074]
        //01                                                successFlag
        //01                                                ................

        //[S -> C] [B034]
        //01                                                successFlag
        //06                                                movementType pickup
        //FE                                                slot 254 = gold
        //19 00 00 00                                       amount 25

        private const byte GOLDFLAG = 254;

        public override ItemMovementType Type => ItemMovementType.PickUp;

        public static event EventHandler<ItemMoveArgs> ItemPickedUp = delegate { };
        private void OnItemPickedUp(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemPickedUp;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            Item item = new Item();
            item.Slot = p.Packet.ReadUInt8();

            if (item.Slot == GOLDFLAG)
            {
                uint amount = p.Packet.ReadUInt32();
                Bot.CharData.RemainGold += amount;
                return;
            }

            ParseNewItem(ref item, p);
            OnItemPickedUp(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            // Pickup is taken care of by the interaction opcodes 7074/B074
            return null;
        }
    }
}
