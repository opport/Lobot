﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    // unequip 
    //[C -> S][7034]
    //23                                                movementType
    //01                                                fromSlot
    //27                                                toSlot

    //[S -> C][B034]
    //01                                                ................
    //23                                                #...............
    //01                                                ................
    //24                                                $...............
    //00 00                                             ................
    //00                                                ................
    public class MoveUnequipAvatarItem : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.UnequipAvatarItem;

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.Inventory[p.ToSlot] = CharInfoGlobal.AvatarInventory[p.FromSlot];
            CharInfoGlobal.AvatarInventory.Remove(p.FromSlot);
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            return p.Packet;
        }
    }
}
