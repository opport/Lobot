﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    //move item from storage slot 5 to slot 2
    //[C -> S][7034]
    //01                                                movementOption
    //05                                                fromSlot
    //02                                                toSlot
    //04 00                                             amount
    //B1 01 00 00                                       selectednpcID

    //[S->C][B034]
    //01                                                successFlag
    //01                                                movementOption
    //05                                                fromSlot
    //02                                                toSlot
    //04 00                                             amount
    public class MoveStorageToStorage : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.StorageToStorage;

        public static event EventHandler<ItemMoveArgs> ItemMoved = delegate { };

        private void OnItemMoved(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemMoved;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            if (CharInfoGlobal.Storage.TryGetValue(p.FromSlot, out Item fromItem))
            {
                // Using the subtype constructors that take an item as an argument helps maintain the item subtypes.
                // Simply creating items of the type parentclass "Item" would discard subtype-specific variables until the character teleports again and the subtypes are assigned again.
                Item refFrom = fromItem.Clone() as Item;

                if (CharInfoGlobal.Storage.TryGetValue(p.ToSlot, out Item toItem))
                {
                    Item refTo = toItem.Clone() as Item;

                    if (fromItem.RefItemID == toItem.RefItemID) // stack
                    {
                        Stack(CharInfoGlobal.Storage, refFrom, refTo, p.Amount);
                    }
                    else // switch
                    {
                        Switch(CharInfoGlobal.Storage, refFrom, refTo);
                    }
                }
                else
                {
                    if (p.Amount < CharInfoGlobal.Storage[p.FromSlot].StackCount)
                    {
                        Split(CharInfoGlobal.Storage, refFrom, p.ToSlot, p.Amount);
                    }
                    else
                    {
                        Move(CharInfoGlobal.Storage, p.FromSlot, p.ToSlot);
                    }
                }

                OnItemMoved(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
            }
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            base.MoveItem(p);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}