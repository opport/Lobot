﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Commands.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    // update inventory stats after disjoint alchemy material
    //[S -> C][3040]
    //29                                                updateSlot 42 (slot with void rondo)
    //08                                                updateType 8 (update?)
    //A1 00                                             newStackCount 161

    // update pet scroll to alive after using grass of life
    //[C -> S][704C]
    //18                                                useSlot
    //EC 30                                             type (grass of life)
    //16                                                targetSlot

    //[S -> C][B04C]
    //01                                                success
    //18                                                useSlot
    //01 00                                             amount
    //EC 30                                             type (grass of life)

    //[S -> C][3040]
    //16                                                petscroll slot
    //40                                                update type (pet ?)
    //03                                                status alive
    public class UpdateInventory : Handler
    {
        public static event EventHandler<InventoryUpdateArgs> InventoryUpdated = delegate { };
        public static event EventHandler<InventoryUpdateArgs> PetScrollUpdated = delegate { };

        protected void OnInventoryUpdated(InventoryUpdateArgs e)
        {
            EventHandler<InventoryUpdateArgs> handler = InventoryUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private void OnPetScrollUpdated(InventoryUpdateArgs e)
        {
            EventHandler<InventoryUpdateArgs> handler = PetScrollUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public UpdateInventory() { }

        public override ushort ServerOpcode => 0x3040;

        protected override ICommand ParsePacket(Packet p)
        {
            byte slotToUpdate = p.ReadUInt8();
            byte updateType = p.ReadUInt8(); // 8 => change stacks?

            InventoryUpdateArgs e = new InventoryUpdateArgs(slotToUpdate, updateType);

            switch (updateType)
            {
                case 8:
                    CharInfoGlobal.Inventory[slotToUpdate].StackCount = p.ReadUInt16();
                    break;
                case 0x40:
                    if (CharInfoGlobal.Inventory.TryGetValue(slotToUpdate, out Item item) && item is PetScroll)
                    {
                        byte updateStatus = p.ReadUInt8();
                        (item as PetScroll).Status = (PetStatus)updateStatus; // 1 = Unsummoned, 2 = Summoned, 3 = Alive, 4 = Dead
                        e.NewStatus = updateStatus;
                        OnPetScrollUpdated(e);
                    }
                    break;
                default:
                    break;
            }

            OnInventoryUpdated(e);

            return null;
        }
    }
}
