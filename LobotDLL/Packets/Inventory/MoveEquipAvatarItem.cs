﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{

    // equip avatar dress
    //[C -> S][7034]
    //24                                                movementType
    //23                                                fromSlot
    //01                                                toSlot

    //[S -> C][B034]
    //01                                                successFlag
    //24                                                movementType
    //24                                                from
    //01                                                toSlot
    //00 00                                             amount
    //00                                                flag
    public class MoveEquipAvatarItem : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.EquipAvatarItem;

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.AvatarInventory[p.ToSlot] = CharInfoGlobal.Inventory[p.FromSlot];
            CharInfoGlobal.Inventory.Remove(p.FromSlot);
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            return p.Packet;

        }
    }
}
