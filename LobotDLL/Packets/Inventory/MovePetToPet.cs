﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    public class MovePetToPet : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.PetToPet;

        public static event EventHandler<ItemMoveArgs> ItemStacked = delegate { };
        public static event EventHandler<ItemMoveArgs> ItemSwitched = delegate { };
        public static event EventHandler<ItemMoveArgs> ItemMoved = delegate { };

        private void OnItemStacked(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemStacked;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private void OnItemSwitched(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemSwitched;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private void OnItemMoved(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemMoved;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            if (CharInfoGlobal.PetInventory.TryGetValue(p.FromSlot, out Item fromItem))
            {
                // Using the subtype constructors that take an item as an argument helps maintain the item subtypes.
                // Simply creating items of the type parentclass "Item" would discard subtype-specific variables until the character teleports again and the subtypes are assigned again.
                Item refFrom = fromItem.Clone() as Item;

                if (CharInfoGlobal.PetInventory.TryGetValue(p.ToSlot, out Item toItem))
                {
                    Item refTo = toItem.Clone() as Item;

                    if (fromItem.RefItemID == toItem.RefItemID) // stack
                    {
                        Stack(CharInfoGlobal.PetInventory, refFrom, refTo, p.Amount);
                        OnItemStacked(new ItemMoveArgs(refFrom.Slot, refTo.Slot, Type));
                    }
                    else // switch
                    {
                        Switch(CharInfoGlobal.PetInventory, refFrom, refTo);
                        OnItemSwitched(new ItemMoveArgs(refTo.Slot, refFrom.Slot, Type));
                    }
                }
                else
                {
                    if (p.Amount < CharInfoGlobal.PetInventory[p.FromSlot].StackCount)
                    {
                        Split(CharInfoGlobal.PetInventory, refFrom, p.ToSlot, p.Amount);
                    }
                    else
                    {
                        Move(CharInfoGlobal.PetInventory, p.FromSlot, p.ToSlot);
                        OnItemMoved(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
                    }
                }
            }
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            base.MoveItem(p);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}
