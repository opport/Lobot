﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using System;

namespace LobotDLL.Packets.Inventory
{
    //[C -> S][7034]                                    Command -> switch
    //00                                                movementType 0 = inventory
    //0D                                                currentSlot 13
    //1D                                                nextSlot 29
    //E8 03                                             amount 1000

    //[S -> C][B034]                                    Response -> sucessful switch
    //01                                                successFlag
    //00                                                movementType
    //0D                                                fromSlot 13
    //1D                                                toSlot 29
    //E8 03                                             amount 1000
    //00                                                successFlag
    public class MoveInventoryToInventory : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.InventoryToInventory;

        public static event EventHandler<ItemMoveArgs> ItemStacked = delegate { };
        public static event EventHandler<ItemMoveArgs> ItemSwitched = delegate { };
        public static event EventHandler<ItemMoveArgs> ItemMoved = delegate { };

        private void OnItemStacked(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemStacked;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private void OnItemSwitched(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemSwitched;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private void OnItemMoved(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemMoved;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            if (CharInfoGlobal.Inventory.TryGetValue(p.FromSlot, out Item fromItem))
            {
                // Using the subtype constructors that take an item as an argument helps maintain the item subtypes.
                // Simply creating items of the type parentclass "Item" would discard subtype-specific variables until the character teleports again and the subtypes are assigned again. 
                Item refFrom = fromItem.Clone() as Item;

                if (CharInfoGlobal.Inventory.TryGetValue(p.ToSlot, out Item toItem))
                {
                    Item refTo = toItem.Clone() as Item;

                    if (fromItem.RefItemID == toItem.RefItemID) // stack
                    {
                        Stack(CharInfoGlobal.Inventory, refFrom, refTo, p.Amount);
                        OnItemStacked(new ItemMoveArgs(refFrom.Slot, refTo.Slot, Type));
                    }
                    else // switch
                    {
                        Switch(CharInfoGlobal.Inventory, refFrom, refTo);
                        OnItemSwitched(new ItemMoveArgs(refTo.Slot, refFrom.Slot, Type));
                    }
                }
                else
                {
                    // p.Amount == 0 usually means switching items in the secondary weapon slot
                    if (p.Amount > 0 && p.Amount < CharInfoGlobal.Inventory[p.FromSlot].StackCount)
                    {
                        Split(CharInfoGlobal.Inventory, refFrom, p.ToSlot, p.Amount);
                    }
                    else
                    {
                        Move(CharInfoGlobal.Inventory, p.FromSlot, p.ToSlot);
                        OnItemMoved(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
                    }
                }
            }
        }
    }
}