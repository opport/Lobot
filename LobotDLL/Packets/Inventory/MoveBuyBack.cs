﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    // Buyback
    //[C -> S][7168]
    //02 01 00 00                                       npcID
    //00                                                fromSlot(BuybackList)

    //gold update
    //[S -> C] [304E]
    //01                                                successFlag
    //CC 0D 00 00 00 00 00 00                           finalAmount 3532
    //01                                                endFlag

    //[S->C][B034]
    //01                                                successFlag
    //22                                                movementType = 34 buyback
    //24                                                toSlot
    //00                                                fromSlot(BuybackList)
    //01 00                                             amount
    public class MoveBuyBack : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.BuyBack;

        public void HandleMove(ItemMoveParams p)
        {
            byte toSlot = p.Packet.ReadUInt8();
            byte fromSlot = p.Packet.ReadUInt8();
            Tuple<byte, Item> reboughtItem = new Tuple<byte, Item>(toSlot, CharInfoGlobal.BuyBackList[fromSlot]);
            CharInfoGlobal.BuyBackList.RemoveAt(fromSlot);

            CharInfoGlobal.Inventory[reboughtItem.Item1] = reboughtItem.Item2;
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            return p.Packet;
        }
    }
}
