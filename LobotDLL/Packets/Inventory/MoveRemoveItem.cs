﻿using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;

namespace LobotDLL.Packets.Inventory
{
    //remove last hp herb after use
    //[S -> C][B034]
    //01                                                success
    //0F                                                movementType 15 -> remove
    //2A                                                slot 42
    //02                                                hasNextItem? 2 -> no because deleted
    public class MoveRemoveItem : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.RemoveItem;

        public void HandleMove(ItemMoveParams p)
        {
            byte slot = p.Packet.ReadUInt8();
            CharInfoGlobal.Inventory.Remove(slot);
            byte removeType = p.Packet.ReadUInt8();

            if (removeType == 2)
            {
                // 2 consumed after use
            }
            else if (removeType == 4)
            {
                // alchemy (like removing elixir or lucky powder after use)
            }
        }
    }
}
