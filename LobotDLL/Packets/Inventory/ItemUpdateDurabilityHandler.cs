﻿using LobotAPI.Globals;
using LobotAPI.Packets;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    /// <summary>
    //[S -> C][3052]
    //03                                                slot = 3 Gauntlet
    //2F 00 00 00                                       durability = 47
    /// </summary>
    public class ItemUpdateDurabilityHandler : Handler
    {
        public static event EventHandler<DurabilityUpdateArgs> DurabilityUpdated = delegate { };

        private static void OnDurabilityUpdated(DurabilityUpdateArgs e)
        {
            EventHandler<DurabilityUpdateArgs> handler = DurabilityUpdated;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public override ushort ServerOpcode => 0x3052;

        public ItemUpdateDurabilityHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            byte slot = p.ReadUInt8();
            if (CharInfoGlobal.Inventory.TryGetValue(slot, out Item value))
            {
                OnDurabilityUpdated(new DurabilityUpdateArgs(value));
            }

            return null;
        }
    }
}
