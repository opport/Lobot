﻿namespace LobotDLL.Packets.Inventory
{
    public enum MoveItemError : short
    {
        CannotFind = 0x0003,
        /// <summary>
        /// Character is not yet interacting with shop NPC
        /// </summary>
        ShopNotOpen = 0x0D,
        /// <summary>
        /// Item cannot be picked because enough items have been collected for the quest.
        /// </summary>
        QuestFinished = 0x0074,
        /// <summary>
        /// No space left in inventory
        /// </summary>
        InventoryFull = 0x1807,
    }
}
