﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    // Buyback
    //[C -> S][7168]
    //02 01 00 00                                       npcID
    //00                                                fromSlot(BuybackList)

    //gold update
    //[S -> C] [304E]
    //01                                                successFlag
    //CC 0D 00 00 00 00 00 00                           finalAmount 3532
    //01                                                endFlag

    //[S->C][B034]
    //01                                                successFlag
    //22                                                movementType = 34 buyback
    //24                                                toSlot
    //00                                                fromSlot(BuybackList)
    //01 00                                             amount
    public class ItemBuyBack : Command
    {
        byte FromSlot;

        public override ushort Opcode => 0x7168;

        public ItemBuyBack(byte fromSlot)
        {
            FromSlot = fromSlot;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, false);
            p.WriteUInt32(Bot.SelectedNPC);
            p.WriteUInt8(FromSlot);
            return p;
        }
    }
}
