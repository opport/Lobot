﻿using LobotAPI;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    //[C -> S][7034]
    //0A                                                movementType
    //19 00 00 00 00 00 00 00                           amount 25

    //[S -> C][B034]
    //01                                                successFlag
    //0A                                                ItemMovementType 10 = drop gold
    //19 00 00 00 00 00 00 00                           amount 25
    public class MoveDropGold : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.DropGold;

        public void HandleMove(ItemMoveParams p)
        {
            Bot.CharData.RemainGold -= p.Packet.ReadUInt64();
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt64(p.Amount);
            return p.Packet;
        }
    }
}
