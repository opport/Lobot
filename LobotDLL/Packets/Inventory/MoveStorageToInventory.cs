﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.Inventory
{
    //move 4 mp recovery herbs from storage slot index 1 to inventory slot 35  
    //[C -> S][7034]
    //03                                                moveOption 3 = storage to inventory
    //01                                                fromSlot
    //23                                                toSlot(selected by client, can end up on different slot if occupied)
    //B1 01 00 00                                       selectedNPCID

    //[S->C][B034]
    //01                                                successFlag
    //03                                                moveOption
    //01                                                fromSlot
    //24                                                toSlot(slot 35 was occupied)
    public class MoveStorageToInventory : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.StorageToInventory;

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.Inventory[p.ToSlot] = CharInfoGlobal.Storage[p.FromSlot];
            CharInfoGlobal.Storage.Remove(p.FromSlot);
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}