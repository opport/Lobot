﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    public class MoveSell : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        private const int MAX_BUYBACK_ITEMS = 5;

        public override ItemMovementType Type => ItemMovementType.Sell;

        public static event EventHandler<ItemMoveArgs> ItemSold = delegate { };

        private void OnItemSold(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemSold;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        //[C -> S] [7034]
        //09                                                moveOption
        //26                                                fromSlot
        //01 00                                             amount
        //9E 01 00 00                                       npcID

        //[S -> C] [B034]
        //01                                                successFlag
        //09                                                moveOption 9 = sell
        //26                                                fromSlot
        //01 00                                             amount
        //9E 01 00 00                                       npcID
        //01                                                amount of items in buyback list
        public void HandleMove(ItemMoveParams p)
        {
            if (CharInfoGlobal.Inventory.TryGetValue(p.FromSlot, out Item fromItem))
            {
                if (p.Amount == fromItem.StackCount)
                {
                    CharInfoGlobal.Inventory.Remove(fromItem.Slot);
                }
                else
                {
                    CharInfoGlobal.Inventory[fromItem.Slot].StackCount -= p.Amount;
                }

                int previousBuyBackCount = CharInfoGlobal.BuyBackList.Count;
                byte buyBackCount = p.Packet.ReadUInt8(); // amount of items in buyback list

                if (previousBuyBackCount == MAX_BUYBACK_ITEMS)
                {
                    CharInfoGlobal.BuyBackList.RemoveAt(0);
                }
                CharInfoGlobal.BuyBackList.Add(fromItem);

                OnItemSold(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
            }
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt16(p.Amount);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}
