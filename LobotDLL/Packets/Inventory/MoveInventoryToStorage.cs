﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Packets.Inventory;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    //move 4 mp recovery herbs from inventory slot 36 to storage slot index 1 
    //[C -> S] [7034]
    //02                                                moveOption 2 = inventory to storage
    //24                                                fromSlot
    //01                                                toSlot (result can vary based on vacancy)
    //B1 01 00 00                                       selectedNPC

    //[S -> C] [B034]
    //01                                                successFlag
    //02                                                moveOption
    //24                                                fromSlot
    //01                                                toSlot
    public class MoveInventoryToStorage : AbstractItemMoveStrategy, IItemMoveHandleStrategy
    {
        public override ItemMovementType Type => ItemMovementType.InventoryToStorage;

        public static event EventHandler<ItemMoveArgs> ItemStored = delegate { };

        private void OnItemStored(ItemMoveArgs e)
        {
            EventHandler<ItemMoveArgs> handler = ItemStored;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public void HandleMove(ItemMoveParams p)
        {
            CharInfoGlobal.Storage[p.ToSlot] = CharInfoGlobal.Inventory[p.FromSlot];
            CharInfoGlobal.Inventory.Remove(p.FromSlot);
            OnItemStored(new ItemMoveArgs(p.FromSlot, p.ToSlot, Type));
        }

        public override Packet MoveItem(ItemMoveParams p)
        {
            p.Packet.WriteUInt8(Type);
            p.Packet.WriteUInt8(p.FromSlot);
            p.Packet.WriteUInt8(p.ToSlot);
            p.Packet.WriteUInt32(Bot.SelectedNPC.UniqueID);
            return p.Packet;
        }
    }
}
