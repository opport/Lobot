﻿using LobotAPI.Packets;
using LobotAPI.Packets.Inventory;
using LobotAPI.Structs;
using SilkroadSecurityApi;
using System;

namespace LobotDLL.Packets.Inventory
{
    // move to storage
    //[C -> S][7034]
    //01                                                movementOption
    //05                                                fromSlot
    //02                                                toSlot
    //04 00                                             amount
    //B1 01 00 00                                       selectednpcID
    public class MoveItem : BidirectionalPacket
    {
        ItemMovementType MovementOption = 0x00;
        byte FromSlot;
        byte ToSloT;
        ushort Amount;
        uint SelectedNPCID;

        private static Comparison<Tuple<byte, Item>> Comparison = new Comparison<Tuple<byte, Item>>((i1, i2) => { return i1.Item1 - i2.Item1; });
        public override ushort Opcode => 0x7034;
        public override ushort ServerOpcode => 0xB034;

        public static event EventHandler<MoveItemErrorArgs> ItemMoveError = delegate { };

        private void OnItemMoveError(MoveItemErrorArgs e)
        {
            EventHandler<MoveItemErrorArgs> handler = ItemMoveError;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        public MoveItem() { }

        public MoveItem(ItemMovementType movementOption, byte fromSlot, byte toSloT, ushort amount = 0, uint uniqueNPCID = 0x00)
        {
            MovementOption = movementOption;
            FromSlot = fromSlot;
            ToSloT = toSloT;
            Amount = amount;
            SelectedNPCID = uniqueNPCID;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode, true);
            ItemMoveParams parameters = new ItemMoveParams(FromSlot, ToSloT, Amount, p);

            // add npcID if movement has to do with another NPC
            switch (MovementOption)
            {
                case ItemMovementType.InventoryToInventory:
                    return new MoveInventoryToInventory().MoveItem(parameters);
                case ItemMovementType.StorageToStorage:
                    return new MoveStorageToStorage().MoveItem(parameters);
                case ItemMovementType.InventoryToStorage:
                    return new MoveInventoryToStorage().MoveItem(parameters);
                case ItemMovementType.PickUp: //// Pickup command is taken care of by the interaction opcodes 7074/B074
                case ItemMovementType.Drop:
                    return new MoveDrop().MoveItem(parameters);
                case ItemMovementType.StorageToInventory:
                    return new MoveStorageToInventory().MoveItem(parameters);
                case ItemMovementType.DropGold:
                    return new MoveDropGold().MoveItem(parameters);
                case ItemMovementType.Buy:
                    return new MoveBuy().MoveItem(parameters);
                case ItemMovementType.Sell:
                    return new MoveSell().MoveItem(parameters);
                case ItemMovementType.PetToPet:
                    break;
                case ItemMovementType.PetToInventory:
                    break;
                case ItemMovementType.InventoryToPet:
                    break;
                case ItemMovementType.BuyBack:
                    return new MoveBuyBack().MoveItem(parameters);
                case ItemMovementType.UnequipAvatarItem:
                    return new MoveUnequipAvatarItem().MoveItem(parameters);
                case ItemMovementType.EquipAvatarItem:
                    return new MoveEquipAvatarItem().MoveItem(parameters);
                default:
                    break;
            }
            return p;
        }


        protected override ICommand ParsePacket(Packet p)
        {
            byte successFlag = p.ReadUInt8();

            if (successFlag == 0x01)
            {
                byte type = p.ReadUInt8();

                switch ((ItemMovementType)type)
                {
                    case ItemMovementType.InventoryToInventory:
                        //Loop takes itemswitch into account (switch bow+arrow with shield)
                        new MoveInventoryToInventory().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8(), p.ReadUInt16()));

                        successFlag = p.ReadUInt8();
                        if (successFlag == 0x01)
                        {
                            type = p.ReadUInt8();
                            if ((ItemMovementType)type == ItemMovementType.InventoryToInventory)
                            {
                                new MoveInventoryToInventory().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8(), p.ReadUInt16()));
                            }
                        }
                        break;
                    case ItemMovementType.StorageToStorage:
                        new MoveStorageToStorage().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8(), p.ReadUInt16()));
                        break;
                    case ItemMovementType.InventoryToStorage:
                        new MoveInventoryToStorage().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8()));
                        break;
                    case ItemMovementType.StorageToInventory:
                        new MoveStorageToInventory().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8()));
                        break;
                    case ItemMovementType.PickUp:
                        new MovePickUp().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.Drop:
                        new MoveDrop().HandleMove(new ItemMoveParams(p.ReadUInt8()));
                        break;
                    case ItemMovementType.Buy:
                        new MoveBuy().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.Sell:
                        new MoveSell().HandleMove(new ItemMoveParams(p.ReadUInt8(), 0, p.ReadUInt16(), p));
                        break;
                    case ItemMovementType.DropGold:
                        new MoveDropGold().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.AwardItem:
                        new MoveAwardItem().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.RemoveItem:
                        new MoveRemoveItem().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.PetToPet:
                        break;
                    case ItemMovementType.PetToInventory:
                        break;
                    case ItemMovementType.InventoryToPet:
                        break;
                    case ItemMovementType.BuyBack:
                        new MoveBuyBack().HandleMove(new ItemMoveParams(0, 0, 0, p));
                        break;
                    case ItemMovementType.UnequipAvatarItem:
                        new MoveUnequipAvatarItem().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8()));
                        break;
                    case ItemMovementType.EquipAvatarItem:
                        new MoveEquipAvatarItem().HandleMove(new ItemMoveParams(p.ReadUInt8(), p.ReadUInt8()));
                        break;
                    default:
                        break;
                }
            }
            else if (successFlag == 0x02)
            {
                ushort error = p.ReadUInt16(); // errorcode
                OnItemMoveError(new MoveItemErrorArgs((MoveItemError)error));
            }

            return null;
        }
    }
}
