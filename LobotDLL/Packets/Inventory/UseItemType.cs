﻿namespace LobotDLL.Packets.Inventory
{
    public enum UseItemType
    {
        Normal,
        UseOnPet,
        UseOnItem
    }
}
