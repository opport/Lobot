﻿using LobotAPI;
using LobotAPI.Packets;
using SilkroadSecurityApi;

namespace LobotDLL.Packets.ItemMall
{
    //[S -> C][3153]
    //23 00 00 00                                       ipcoins 35
    //0A 00 00 00                                       silk/action points 10
    //00 00 00 00                                       soul tickets 0
    public class SilkUpdateHandler : Handler
    {
        public override ushort ServerOpcode => 0x3153;

        public SilkUpdateHandler() { }

        protected override ICommand ParsePacket(Packet p)
        {
            uint ipcoins = p.ReadUInt32();
            uint silk = p.ReadUInt32();
            uint soulTickets = p.ReadUInt32();

            Bot.ItemMallInfo = new LobotAPI.Structs.ItemMallInfo(ipcoins, silk, soulTickets);

            return null;
        }
    }
}
