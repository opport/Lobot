﻿using LobotAPI.Commands;
using SilkroadSecurityApi;

namespace LobotDLL.Commands.Chat
{
    //public static void BuildPrivateMessage(string Text, string Getter)
    //{
    //    if (Getter == "[BOT]System")
    //        Text = "global " + Text;

    //    SilkroadSecurityApi.Packet p = new SilkroadSecurityApi.Packet(0x7025);
    //    p.WriteUInt8(0x02); // for private
    //    p.WriteUInt8(Framework.Global.Global.chatCounter);
    //    p.WriteAscii(Getter);
    //    p.WriteAscii(Text);
    //    Framework.Proxy.Main.ag_remote_security.Send(p);
    //}
    public class ChatMessage : Command
    {
        private string Message = "";
        private ChatType Type = ChatType.All;

        public ChatMessage() : base(0x7025) { }

        protected ChatMessage(string message, ChatType type) : base(0x7025)
        {
            Message = message;
            Type = type;
        }

        protected override Packet AddPayload()
        {
            Packet p = new Packet(Opcode);
            p.WriteAscii(Message);
            return p;
        }
    }
}
