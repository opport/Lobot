﻿using LobotAPI;
using LobotAPI.Captcha;
using LobotAPI.Connection;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Map;
using LobotAPI.Packets;
using LobotAPI.Proxy;
using LobotAPI.StateMachine;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.StateMachine;
using SilkroadSecurityApi;
using System;
using System.Threading.Tasks;

namespace LobotDLL.Connection
{
    public class Proxy : IProxy
    {
        //!!! CrossThreaded !!!
        public delegate void LogEventHandler(string text);
        public event LogEventHandler Log;

        private Server GatewaySocket;

        private bool gatewayConnected;
        public bool GatewayConnected { get { return gatewayConnected; } set { gatewayConnected = value; } }

        // Server Fields

        private Server AgentSocket;

        private bool agentConnected;
        public bool AgentConnected { get { return agentConnected; } set { agentConnected = value; } }

        // Client Fields

        private Client ClientSocket;

        private byte clientLocal;
        public byte ClientLocal { get { return clientLocal; } set { clientLocal = value; } }

        private bool clientConnected;
        public bool ClientConnected { get { return clientConnected; } }

        private ushort localGWPort;
        public ushort LocalGWPort { get { return localGWPort; } set { localGWPort = value; } }

        private bool IsClientless;
        private bool ShouldSwitchClient;
        private bool ShouldConnectToAgent;

        private bool IsClientWaitingForData;
        private bool IsClientWatingForFinish;

        // 0x6102/0x6103 logindata storage

        private uint SessionID;
        private string Username;
        private string Password;
        private ushort ServerID;

        private static Proxy instance;

        private Proxy()
        {
            CharMove.BotMovementRegistered += Bot.CharMovementRegistered;
            // This might update too often if the character keeps switching between the same regions
            //CharMove.BotMovementRegistered += MapUtils.HandleMovementRegistered;

            Bot.CharData.CharPositionChanged += MapUtils.HandleMovementRegistered;
            CharStopMovementHandler.CharStopMovement += Bot.HandleCharStopMovement;

            GatewaySocket = new Server();
            GatewaySocket.HandleConnected += GatewaySocketConnected;
            GatewaySocket.HandleDisconnected += GatewaySocketDisconnected;
            GatewaySocket.HandleKicked += GatewaySocketKicked;
            GatewaySocket.HandlePacketReceived += GatewaySocketPacketReceived;

            AgentSocket = new Server();
            AgentSocket.HandleConnected += AgentSocketConnected;
            AgentSocket.HandleDisconnected += AgentSocketDisconnected;
            AgentSocket.HandleKicked += AgentSocketKicked;
            AgentSocket.HandlePacketReceived += AgentSocketPacketReceived;

            ClientSocket = new Client();
            ClientSocket.HandleConnected += ClientSocketConnected;
            ClientSocket.HandleDisconnected += ClientSocketDisconnected;
            ClientSocket.HandlePacketReceived += ClientSocketPacketReceived;
        }

        /// <summary>
        /// Singleton constructor.
        /// </summary>
        public static Proxy Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Proxy();
                }
                return instance;
            }
        }

        private void AutoReconnect()
        {
            if (Bot.Status == BotStatus.Botting)
            {
                BotStateMachine.Instance.Stop();
            }

            if (LoginSettings.AutoConnect)
            {
                System.Diagnostics.Debug.WriteLine("----Waiting to reconnect-----");
                Logger.Log(LogLevel.NETWORK, "----Waiting to reconnect-----");

                //TODO use loader or clientless to restart
            }
        }

        private static bool IsHandshakePacket(ushort opcode)
        {
            return (!Handling.HandlingFunctions.ContainsKey(opcode) || opcode == 0x5000 || opcode == 0x9000 || opcode == 0x2001 || opcode == 0x2002);
        }

        public async Task SendCommandGW(int delay, ICommand c)
        {
            await Task.Delay(delay).ContinueWith(t => SendCommandGW(c));
        }

        public async Task SendCommandAG(int delay, ICommand c)
        {
            await Task.Delay(delay).ContinueWith(t => SendCommandAG(c));
        }

        public void SendCommandGW(ICommand c)
        {
            if (GatewaySocket != null)
            {
                Packet p = c.Execute();
                Logger.Log(LogLevel.NETWORK, string.Format("Sending {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
                GatewaySocket?.Send(p);
            }
        }

        public void SendCommandAG(ICommand c)
        {
            if (AgentSocket != null)
            {
                Packet p = c.Execute();
                Logger.Log(LogLevel.NETWORK, string.Format("Sending {0:x} - {1} - {2} - {3}", p.Opcode, p.Encrypted.ToString(), p.Massive.ToString(), String.Join(" ", Array.ConvertAll(p.GetBytes(), x => x.ToString("X2")))));
                AgentSocket?.Send(p);
            }
        }

        private void ProcessPacket(Packet p)
        {
            //Process -> Packet
            ICommand response = Handling.HandlePacket(p);
            if (response != null)
            {
                SendCommandGW(50, response).ConfigureAwait(false);
            }
        }

        public void PerformClientless(string gwIP, ushort gwPort, uint clientVersion, byte clientLocal)
        {
            NetworkGlobal.GatewayIP = gwIP;
            NetworkGlobal.ProxyGatewayPort = gwPort;
            LoginSettings.Version = clientVersion;
            this.clientLocal = clientLocal;

            IsClientless = true; //FLAG CLIENTLESS

            GatewaySocket.Connect(NetworkGlobal.PROXY_ADDRESS, NetworkGlobal.ProxyGatewayPort);
        }

        internal void Connect(string gwIP, ushort gwPort, uint clientVersion, byte clientLocal)
        {
            NetworkGlobal.GatewayIP = gwIP;
            NetworkGlobal.ProxyGatewayPort = gwPort;
            LoginSettings.Version = clientVersion;
            this.clientLocal = clientLocal;

            IsClientless = false; //FLAG CLIENTLESS

            GatewaySocket.Connect(NetworkGlobal.PROXY_ADDRESS, NetworkGlobal.ProxyGatewayPort);
        }

        public void SendLogin(string username, string password, Shard Server)
        {
            //[C -> S][6102][22 bytes][Enc]
            //16 //Local
            //06 00 //UsernameLenght
            //64 61 78 74 65 72 //Username
            //09 00 //PasswordLenght
            //31 33 62 69 74 74 65 32 34 //Password
            //40 00  //ShardID                     

            Username = username;
            Password = password;
            ServerID = Server.ID;

            Packet p = new Packet(0x6102, true);
            p.WriteUInt8(clientLocal);
            p.WriteAscii(Username);
            p.WriteAscii(Password);
            p.WriteUInt16(ServerID);
            p.Lock();

            GatewaySocket.Send(p);
        }

        public void SendCaptcha(string captcha)
        {
            Packet p = new Packet(0x6323);
            p.WriteAscii(captcha);
            p.Lock();
            GatewaySocket.Send(p);
        }

        public void Listen()
        {
            ushort testPort = NetworkGlobal.ProxyGatewayPort;
            bool validPort = false;
            System.Net.Sockets.Socket socket = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
            do
            {
                try
                {
                    socket.Bind(new System.Net.IPEndPoint(System.Net.IPAddress.Loopback, testPort));
                    localGWPort = testPort;
                    validPort = true;
                }
                catch { testPort++; }

            } while (!validPort);
            socket.Close();
            socket = null;

            ClientSocket.Listen(localGWPort);
        }
        void GatewaySocketPacketReceived(Packet p)
        {
            if (IsClientless)
            {
                if (!ShouldSwitchClient)  //Clientless connection ps should end up here
                {
                    // Request Patches

                    if (p.Opcode == 0x2001)
                    {
                        //Generate the Patchverification
                        Packet response = new Packet(0x6100, true);
                        response.WriteUInt8(clientLocal);
                        response.WriteAscii("SR_Client"); //ServiceName
                        response.WriteUInt32(LoginSettings.Version);

                        GatewaySocket.Send(response);
                    }

                    // Request the Serverlist.

                    if (p.Opcode == 0xA100)
                    {
                        byte errorCode = p.ReadUInt8();
                        if (errorCode == 1) //Success
                        {
                            Packet response = new Packet(0x6101, true);
                            GatewaySocket.Send(response);
                        }
                        else
                        {
                            Console.WriteLine("There is an update or you are using an invalid silkroad version.");
                        }
                    }

                    // Reconnect to AgentServer on successful login

                    if (p.Opcode == 0xA102)
                    {
                        byte result = p.ReadUInt8();
                        if (result == 1)
                        {
                            SessionID = p.ReadUInt32();
                            NetworkGlobal.GatewayIP = p.ReadAscii();
                            NetworkGlobal.AgentPort = p.ReadUInt16();
                            //doAgentServerConnect = true;                        

                            GatewaySocket.Disconnect();
                            AgentSocket.Connect(NetworkGlobal.AgentIP, NetworkGlobal.AgentPort);
                        }
                    }

                    // ServerList

                    if (p.Opcode == 0xA101)
                    {

                        //GlobalServer
                        bool nextGlobalServer = p.ReadUInt8() == 0x01;
                        do
                        {
                            p.ReadUInt8(); //GlobalOperationID
                            p.ReadAscii(); //GlobalOperationName

                            nextGlobalServer = p.ReadUInt8() == 0x01;
                        } while (nextGlobalServer);

                        //ShardList
                        //LoginSettings.ShardList = new List<Objects.Shard>();
                        bool nextShard = p.ReadUInt8() == 0x01;
                        do
                        {
                            //LoginSettings.ShardList.Add(new Shard(p));
                            nextShard = p.ReadUInt8() == 0x01;
                        } while (nextShard);

                        //Program.main.cbServer.Items.AddRange(LoginSettings.ShardList.ToArray());
                        //Program.main.cbServer.SelectedIndex = 0;
                    }

                    //ImageCode Challenge
                    if (p.Opcode == 0x2322)
                    {
                        var captcha = CaptchaProcessor.BytesToImage(p.GetBytes());
                        CaptchaProcessor.BytesToImage(CaptchaProcessor.CurrentCaptcha.compressedData);
                        //Program.main.picCaptcha.Image = Bitmap.FromFile(Environment.CurrentDirectory + "\\captcha.bmp");
                        //Program.main.groupImageCode.Enabled = true;
                    }

                    //ImageCode Response
                    if (p.Opcode == 0xA323)
                    {
                        var result = p.ReadUInt8();
                        if (result != 1)
                        {
                            //Program.main.groupImageCode.Enabled = true;
                        }
                    }
                } //Else? Else would be clientless and switching client, you could implement Gateway Clientless->Client function, but thats kinda useless.       
            }
            else //Proxy Mode - Forwarding Packets between Client and Server
            {
                // Redirect client to local AgentListener

                if (p.Opcode == 0xA102)
                {
                    byte result = p.ReadUInt8();
                    if (result == 1)
                    {
                        SessionID = p.ReadUInt32();
                        NetworkGlobal.AgentIP = p.ReadAscii();
                        NetworkGlobal.AgentPort = p.ReadUInt16();

                        //Create fake response for Client to redirect to localIP/localPort
                        Packet response = new Packet(0xA102, true);
                        response.WriteUInt8(result);
                        response.WriteUInt32(SessionID);
                        response.WriteAscii(System.Net.IPAddress.Loopback.ToString());
                        response.WriteUInt16(localGWPort);
                        response.Lock();
                        ShouldConnectToAgent = true;
                        p = response;
                    }
                    else
                    {
                        // TODO enable login
                        //Program.main.groupLogin.Enabled = true;
                    }
                }
                ClientSocket.Send(p);
                ProcessPacket(p);
            }
        }

        void GatewaySocketKicked()
        {
            gatewayConnected = false;

            if (!agentConnected)
            {
                Bot.Status = BotStatus.Disconnected;
            }

            GatewaySocket.Disconnect();
            ClientSocket.Disconnect();

            Log?.Invoke("Gateway kicked");
        }

        void GatewaySocketDisconnected()
        {
            gatewayConnected = false;

            Log?.Invoke("Gateway disconnected");
        }

        void GatewaySocketConnected(string ip, ushort port)
        {
            gatewayConnected = true;

            Log?.Invoke(string.Format("GatewaySocket connected to {0}:{1}", ip, port));

            ///!!! CrossThreading !!!
            // TODO enable login
            //Program.main.groupLogin.Enabled = true;
        }

        // Agent-EventHandlers

        void AgentSocketPacketReceived(Packet p)
        {
            if (IsClientless)
            {
                if (!ShouldSwitchClient) //Normal Clientless connection should end up here
                {
                    // Login

                    if (p.Opcode == 0x6005)
                    {
                        Packet response = new Packet(0x6103, true);
                        response.WriteUInt32(SessionID);
                        response.WriteAscii(Username);
                        response.WriteAscii(Password);
                        response.WriteUInt8(clientLocal);

                        Random random = new Random();
                        byte[] MAC = new byte[6];
                        random.NextBytes(MAC);
                        response.WriteUInt8Array(MAC);
                        response.Lock();

                        AgentSocket.Send(response);
                    }

                    if (p.Opcode == 0xA103)
                    {
                        var sucess = p.ReadUInt8();
                        if (sucess == 1)
                        {
                            Packet response = new Packet(0x7007);
                            response.WriteUInt8(2);
                            response.Lock();

                            AgentSocket.Send(response);
                        }
                        else
                        {
                            ///!!! CrossThreading !!!
                            // TODO enable login
                            //Program.main.groupLogin.Enabled = true;
                        }
                    }

                    // CharacterSelection

                    if (p.Opcode == 0xB007)
                    {
                        byte type = p.ReadUInt8();
                        if (type == 2)
                        {
                            byte sucess = p.ReadUInt8();
                            if (sucess == 1)
                            {
                                // Enable character selection
                                //Program.main.groupCharacterSelection.Enabled = true;
                                //LoginSettings.CharacterList = new List<Character>();

                                byte characterCount = p.ReadUInt8();
                                for (byte i = 0; i < characterCount; i++)
                                {
                                    //LoginSettings.CharacterList.Add(new Character(p));
                                }

                                ///!!! CrossThreading !!!
                                //Program.main.cbCharacter.Items.Clear();
                                //Program.main.cbCharacter.Items.AddRange(LoginSettings.CharacterList.ToArray());
                            }
                        }
                    }

                    // Ingame

                    if (p.Opcode == 0x3020)
                    {
                        Packet response = new Packet(0x3012);
                        AgentSocket.Send(response);
                        Packet response2 = new Packet(0x750E); //CLIENT_REQUEST_WEATHER
                        AgentSocket.Send(response2);
                    }

                    if (p.Opcode == 0x34A6) //End CharacterData
                    {
                        ///!!! CrossThreading !!!
                        //Program.main.groupClientlessClient.Enabled = true;
                        //Program.main.lblStatus.Text = "Clientless connection established. Please use a ReturnScroll";
                    }

                    if (p.Opcode == 0xB04C)
                    {
                        byte sucess = p.ReadUInt8();
                        if (sucess == 1)
                        {
                            ///!!! CrossThreading !!!
                            //Program.main.lblStatus.Text = "Waiting for Return Scroll";
                            //Program.main.ReturnScrollStarted();
                        }
                        else
                        {
                            ///!!! CrossThreading !!!
                            //Program.main.lblStatus.Text = "ReturnScroll faild, please check Slot (NO INSTANT-RETURN-SCROLLS SIPPORTED YET)";
                        }
                    }
                }
                else //For SwitchingService
                {
                    // Ingame

                    //Teleport Request
                    if (p.Opcode == 0x30D2)
                    {

                        //Wait for Client
                        //!!! Create Callback here, because this would freeze the thread resulting in connection lost when taking too long!!!
                        while (IsClientWaitingForData == false && IsClientWatingForFinish == false)
                        {
                            System.Threading.Thread.Sleep(1);
                        }

                        //Client is ready
                        if (IsClientWaitingForData)
                        {
                            //Accept Teleport
                            Packet respone = new Packet(0x34B6);
                            AgentSocket.Send(respone);

                            IsClientWatingForFinish = true;

                            //!!! CrossThreading!!!
                            //Program.main.lblStatus.Text = "Waiting for Teleport to finish";
                        }
                    }

                    //Incoming CharacterData
                    if (p.Opcode == 0x34A5)
                    {
                        if (IsClientWatingForFinish)
                        {
                            ShouldSwitchClient = false;
                            IsClientless = false;

                            //!!! CrossThreading!!!
                            //Program.main.lblStatus.Text = "Sucessfully switched";
                        }
                    }
                }
            }
            else //Proxy Mode - Forwarding Packets between Client and Server
            {
                ProcessPacket(p);

                if (ShoppingState.ShouldBeFaked(p))
                {
                    Packet[] fakePackets = ShoppingState.FakeItemMovement(p);

                    foreach (Packet fp in fakePackets)
                    {
                        ClientSocket?.Send(fp);
                    }
                }
                else if (!ShoppingState.ShouldFilter(p)) //Only send packet if not filtered
                {
                    ClientSocket?.Send(p);
                }
            }
        }

        void AgentSocketKicked()
        {
            agentConnected = false;
            Bot.Status = BotStatus.Disconnected;
            AgentSocket.Disconnect();
            ClientSocket.Disconnect();
            Log?.Invoke("Agent kicked");
        }

        void AgentSocketDisconnected()
        {
            agentConnected = false;
            Bot.Status = BotStatus.Disconnected;
            Log?.Invoke("Agent disconnected");
        }

        void AgentSocketConnected(string ip, ushort port)
        {
            agentConnected = true;
            Log?.Invoke(string.Format("AgentServer connected to {0}:{1}", ip, port));
        }

        byte agentLoginFixCounter;

        void ClientSocketPacketReceived(Packet p)
        {
            //For ClientlessSwitcher
            if (ShouldSwitchClient)
            {
                // Fake Client

                // 0x2001
                if (p.Opcode == 0x2001)
                {

                    //[S -> C][2001][16 bytes]
                    //0D 00 47 61 74 65 77 61 79 53 65 72 76 65 72 00   ..GatewayServer.
                    Packet response = new Packet(0x2001);
                    if (!ShouldConnectToAgent)
                    {
                        response.WriteAscii("GatewayServer");
                    }
                    else
                    {
                        response.WriteAscii("AgentServer");
                        ShouldConnectToAgent = false;
                    }
                    response.WriteUInt8(0); //Client-Connection
                    response.Lock();
                    ClientSocket.Send(response);

                    //S->P:2005 Data:01 00 01 BA 02 05 00 00 00 02
                    response = new Packet(0x2005, false, true);
                    response.WriteUInt8(0x01);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x01);
                    response.WriteUInt8(0xBA);
                    response.WriteUInt8(0x02);
                    response.WriteUInt8(0x05);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x02);
                    response.Lock();
                    ClientSocket.Send(response);

                    //S->P:6005 Data:03 00 02 00 02
                    response = new Packet(0x6005, false, true);
                    response.WriteUInt8(0x03);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x02);
                    response.WriteUInt8(0x00);
                    response.WriteUInt8(0x02);
                    response.Lock();
                    ClientSocket.Send(response);
                }

                if (p.Opcode == 0x6100)
                {
                    byte local = p.ReadUInt8();
                    string client = p.ReadAscii();
                    uint version = p.ReadUInt32();

                    //S->P:A100 Data:01
                    Packet response = new Packet(0xA100, false, true);

                    if (local != clientLocal)
                    {
                        response.WriteUInt8(0x02); //Faild
                        response.WriteUInt8(0x01); //Faild to connect to server.(C4)                   
                    }
                    else if (client != "SR_Client")
                    {
                        response.WriteUInt8(0x02); //Faild
                        response.WriteUInt8(0x03); //Faild to connect to server.(C4)                 
                    }
                    else if (version != LoginSettings.Version)
                    {
                        response.WriteUInt8(0x02); //Faild
                        response.WriteUInt8(0x02); //Update - Missing bytes but still trigger update message on Client, launcher will crash :/
                    }
                    else
                    {
                        response.WriteUInt8(0x01); //Sucess
                    }

                    response.Lock();
                    ClientSocket.Send(response);
                }

                if (p.Opcode == 0x6101 && ShouldConnectToAgent == false)
                {
                    Packet response = new Packet(0xA102);
                    response.WriteUInt8(0x01); //Sucess
                    response.WriteUInt32(uint.MaxValue); //SessionID
                    response.WriteAscii("127.0.0.1"); //NetworkGlobal.Server_Address
                    response.WriteUInt16(localGWPort);
                    response.Lock();

                    ShouldConnectToAgent = true;
                    ClientSocket.Send(response);
                }

                if (p.Opcode == 0x6103)
                {
                    //FF FF FF FF 00 00 00 00 16 00 00 9D 53 84 00
                    uint sessionID = p.ReadUInt32();
                    string username = p.ReadAscii();
                    string password = p.ReadAscii();
                    byte local = p.ReadUInt8();
                    //byte[] mac = p.ReadUInt8Array(6); //No need

                    Packet response = new Packet(0xA103);
                    if (sessionID != uint.MaxValue)
                    {
                        response.WriteUInt8(0x02);
                        response.WriteUInt8(0x02);
                    }
                    else if (username != "")
                    {
                        response.WriteUInt8(0x02);
                        response.WriteUInt8(0x02);
                    }
                    else if (password != "")
                    {
                        response.WriteUInt8(0x02);
                        response.WriteUInt8(0x02);
                    }
                    else if (local != clientLocal)
                    {
                        response.WriteUInt8(0x02);
                        response.WriteUInt8(0x02);
                    }
                    else
                    {
                        response.WriteUInt8(0x01); //Sucess
                    }
                    response.Lock();
                    ClientSocket.Send(response);
                }

                if (p.Opcode == 0x7007)
                {
                    byte type = p.ReadUInt8();
                    if (type == 0x02)
                    {
                        Packet responseEndCS = new Packet(0xB001);
                        responseEndCS.WriteUInt8(0x01);

                        Packet responseInitLoad = new Packet(0x34A5);

                        ClientSocket.Send(responseEndCS);
                        ClientSocket.Send(responseInitLoad);
                        IsClientWaitingForData = true;
                    }
                }

            }
            else
            {
                //Not sure why but after clientless->client the clients preferes to send 0x6103 twice.
                if (p.Opcode == 0x6103)
                {
                    if (agentLoginFixCounter > 0)
                    {
                        return;
                    }
                    agentLoginFixCounter++;

                    ////Encrypted
                    //4   uint Token //from LOGIN_RESPONSE
                    //2   ushort Username.Length
                    //* string  Username
                    //2   ushort Password.Length
                    //* string  Password
                    //1   byte Content.ID
                    //6   byte[] MAC-Address
                    uint token = p.ReadUInt32();
                    string username = p.ReadAscii();
                    string password = p.ReadAscii();
                    byte locale = p.ReadUInt8();
                    byte[] macAddress = p.ReadUInt8Array(6);

                    // Spoof packet if it is invalid
                    if (string.IsNullOrEmpty(username))
                    {
                        Packet spoof = new Packet(0x6103, true);
                        spoof.WriteUInt32(token);
                        spoof.WriteAscii(LoginSettings.Username);
                        spoof.WriteAscii(LoginSettings.Password);
                        spoof.WriteUInt8(locale);
                        // write random 6 octet mac address
                        Random mac = new Random();
                        spoof.WriteUInt16(0x0000); // 2 empty
                        spoof.WriteUInt8Array(macAddress);

                        p = spoof;
                    }
                }

                if (p.Opcode == 0x6102)
                {
                    agentLoginFixCounter = 0;
                }

                if (agentConnected)
                {
                    AgentSocket.Send(p);
                }
                else
                {
                    GatewaySocket.Send(p);
                }
            }
        }

        void ClientSocketDisconnected()
        {
            clientConnected = false;

            if (!agentConnected)
            {
                Bot.Status = BotStatus.Disconnected;
                GatewaySocket.Disconnect();
                agentLoginFixCounter = 0;
            }
        }

        void ClientSocketConnected()
        {
            clientConnected = true;

            //If a clients connects while having a clientless connection enable clientless switch
            if (IsClientless)
            {
                ShouldSwitchClient = true;

                IsClientWaitingForData = false;
                IsClientWatingForFinish = false;
            }
            else
            {
                if (ShouldConnectToAgent)
                {
                    if (gatewayConnected)
                        GatewaySocket.Disconnect();

                    ShouldConnectToAgent = false;
                    IsClientWaitingForData = false;
                    IsClientWatingForFinish = false;

                    AgentSocket.Connect(NetworkGlobal.AgentIP, NetworkGlobal.AgentPort);
                }
                else
                {
                    if (agentConnected)
                        AgentSocket.Disconnect();
                    GatewaySocket.Connect(NetworkGlobal.GatewayIP, NetworkGlobal.ProxyGatewayPort);
                }
            }
        }

        internal void SendCharacterSelection(Character character)
        {
            Packet p = new Packet(0x7001);
            p.WriteAscii(character.Name);
            p.Lock();
            AgentSocket.Send(p);
        }

        internal void SendUseReturnScroll()
        {
            Packet p = new Packet(0x704C, true);
            p.WriteUInt8(0x0D); //Slot: 13 (First)
            p.WriteUInt8(0xEC);
            p.WriteUInt8(0x09);
            p.Lock();
            AgentSocket.Send(p);
        }
    }

}
