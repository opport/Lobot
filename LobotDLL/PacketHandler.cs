﻿using LobotAPI.Packets;
using LobotDLL.Packets;
using System.Collections.Generic;

namespace LobotDLL
{
    /// <summary>
    /// Class in charge of executing commands based on the packets received.
    /// </summary>
    class PacketHandler
    {
        /// <summary>
        /// Dictionary containing commands that can be handled. The bot adds or removes commands based on options ticked.
        /// </summary>
        private Dictionary<ushort, Command> commandList = new Dictionary<ushort, Command>
        {
            //Login
            //{ 0xA101 , new Login(Bot.Username, Bot.Password) },
            //{ 0x2322 , new LoginCaptcha() },
            //{ 0xA102, new LoginCharSelect() },
            //{ 0xB007, new  },
            // Party

            // Stalling

            // Scripting

            // Exchange
            { 0x3085 , new CharExchangeAccept() }
        };

        public void AddCommand(ushort opcode, Command command)
        {
            commandList.Add(opcode, command);
        }
        public void RemoveCommand(ushort opcode)
        {
            if (commandList.ContainsKey(opcode))
            {
                commandList.Remove(opcode);
            }
        }

        /// <summary>
        /// Main handling function
        /// </summary>
        /// <param name="opcode">Opcode to be handled</param>
        public void ExecuteCommand(ushort opcode)
        {
            if (commandList.TryGetValue(opcode, out Command command))
            {
                command.Execute();
            }
        }
    }
}
