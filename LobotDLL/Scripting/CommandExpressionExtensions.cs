﻿using LobotAPI;
using LobotAPI.Globals;
using LobotAPI.Globals.Settings;
using LobotAPI.Packets;
using LobotAPI.Packets.Char;
using LobotAPI.Scripting;
using LobotAPI.Structs;
using LobotDLL.Connection;
using LobotDLL.Packets;
using LobotDLL.Packets.Char;
using LobotDLL.Packets.Inventory;
using LobotDLL.Packets.Pet;
using LobotDLL.StateMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace LobotDLL.Scripting
{
    public static class CommandExpressionExtensions
    {
        private static Random Rand = new Random();
        public static event EventHandler SkippedCommand = delegate { };
        public static event EventHandler<CharMoveArgs> ExecutedMoveCommand = delegate { };
        public static event EventHandler<DelayArgs> ExecutedDelayCommand = delegate { };
        public static event EventHandler ExecutedTeleportCommand = delegate { };
        public static event EventHandler<ShoppingArgs> ExecutedShoppingCommand = delegate { };
        public static event EventHandler ExecutedSetAreaCommand = delegate { };

        /// <summary>
        /// List of currently implemented functions. Metaexpressions match their commandName to the string to Execute the matching function.
        /// </summary>
        private static Dictionary<string, Action<string[]>> CommandList = new Dictionary<string, Action<string[]>>{
            { "go", s =>  Proxy.Instance.SendCommandAG(GoCommand(s))},
            { "wait", s => WaitCommand(s) },
            { "teleport", s => TeleportCommand(s) },
            { "quest", s =>  throw new NotImplementedException()},
            { "store", s => OpenShop(BotState.Store) },
            { "stable", s => OpenShop(BotState.Stable) },
            { "repair", s => OpenShop(BotState.Smith)},
            { "potions", s => OpenShop(BotState.Potions) },
            { "special", s => OpenShop(BotState.Special) },
            { "mount", s => Mount()},
            { "dismount", s =>  Dismount()},
            { "setArea1", s =>  SetArea(AreaType.AreaOne, s)},
            { "setArea2", s =>  SetArea(AreaType.AreaTwo, s)}
        };

        /// <summary>
        /// Function to generate randomness for the next walking point, if an argument is given.
        /// </summary>
        /// <param name="variance">Maximum deviation from walk point</param>
        /// <returns>Point to be added to next walkpoint for variance</returns>
        private static Point GetRandomScriptJitter(int variance)
        {
            return new Point(Rand.Next(-variance, variance), Rand.Next(-variance, variance));
        }

        private static ICommand GoCommand(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }
            else if (args.Length < 2)
            {
                throw new ArgumentException(nameof(args));
            }

            int x = int.Parse(args[0]);
            int y = int.Parse(args[1]);
            Point jitter = args.Length == 3 ? GetRandomScriptJitter(int.Parse(args[2])) : new Point();

            Console.WriteLine("go {0}, {1}", args[0], args[1]);

            // send the correct typ, depending on whether a transport pet is being used
            ICommand movementPacket;
            Point target = new Point(x + jitter.X, y + jitter.Y);
            if (Bot.CharData.TransportFlag == 0x01)
            {
                movementPacket = new PetAction(target);
            }
            else
            {
                movementPacket = new CharMove(target);
            }

            return movementPacket;
        }

        private static void WaitCommand(string[] args)
        {
            if (args.Length == 1)
            {
                OnExecutedDelayCommand(new DelayArgs(int.Parse(args[0])));
            }
            else
            {
                OnExecutedDelayCommand(new DelayArgs());
            }
        }

        private static void TeleportCommand(string[] args)
        {
            if (args.Length == 2)
            {
                uint npcID = uint.Parse(args[0]);

                if (SpawnListsGlobal.Portals.Any(tuple => tuple.Key == npcID && tuple.Value.PK2Data.TeleporterData.TeleportLinks.Contains(uint.Parse(args[1]))))
                {
                    uint teleportOption = uint.Parse(args[1]);
                    Proxy.Instance.SendCommandAG(50, new NPCTeleport(npcID, teleportOption)).ConfigureAwait(false);
                    OnExecutedTeleportCommand(EventArgs.Empty);
                }
            }
            else
            {
                //reverse return NotImplementedException yet implemented
                throw new NotImplementedException();
            }
        }

        private static Monster GetShopFromSpawnList(string signature)
        {
            return SpawnListsGlobal.NPCSpawns.FirstOrDefault(kvp => kvp.Value.LongID.Contains(signature)).Value;
        }

        private static void OpenShop(NPCInteractionState state)
        {
            try
            {
                OnExecutedShoppingCommand(new ShoppingArgs(GetShopFromSpawnList(state.NPCSignature), state));
            }
            catch (ArgumentNullException)
            {
                Logger.Log(LobotAPI.LogLevel.SCRIPT, $"Shop <{state.GetType().Name}> not found in area {Bot.CharData.Position}. Continuing script");
                OnSkippedCommand(EventArgs.Empty);
            }
        }

        private static void Mount()
        {
            Item item = CharInfoGlobal.Inventory.FirstOrDefault(tuple => tuple.Value.Type == LobotAPI.PK2.Pk2ItemType.Vehicle).Value;

            if (item != null)
            {
                Proxy.Instance.SendCommandAG(new UseItem(item.Slot)); // TODO mount best horse
            }
        }

        private static void Dismount()
        {
            if (Bot.CharData.TransportFlag == 0x01)
            {
                Proxy.Instance.SendCommandAG(new PetDismount());
            }
        }

        private static void SetArea(AreaType type, string[] args)
        {
            int x = int.Parse(args[0]);
            int y = int.Parse(args[1]);
            int radius = int.Parse(args[2]);

            if (type == AreaType.AreaOne)
            {
                Settings.Instance.Training.TrainingArea1.X = x;
                Settings.Instance.Training.TrainingArea1.Y = y;
                Settings.Instance.Training.TrainingArea1.Radius = radius;
            }
            else
            {
                Settings.Instance.Training.TrainingArea2.X = x;
                Settings.Instance.Training.TrainingArea2.Y = y;
                Settings.Instance.Training.TrainingArea2.Radius = radius;
            }

            OnExecutedSetAreaCommand(EventArgs.Empty);
        }

        /// <summary>
        /// Invoke method in expression
        /// </summary>
        /// <param name="expr"></param>
        public static void Execute(this CommandExpression expr)
        {
            if (expr == null)
            {
                throw new ArgumentNullException(nameof(expr));
            }
            else
            {
                CommandList[expr.CommandName].Invoke(expr.Arguments);
            }
        }

        private static void OnSkippedCommand(EventArgs e)
        {
            EventHandler handler = SkippedCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnExecutedDelayCommand(DelayArgs e)
        {
            EventHandler<DelayArgs> handler = ExecutedDelayCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnExecutedMoveCommand(CharMoveArgs e)
        {
            EventHandler<CharMoveArgs> handler = ExecutedMoveCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnExecutedTeleportCommand(EventArgs e)
        {
            EventHandler handler = ExecutedTeleportCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnExecutedShoppingCommand(ShoppingArgs e)
        {
            EventHandler<ShoppingArgs> handler = ExecutedShoppingCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        private static void OnExecutedSetAreaCommand(EventArgs e)
        {
            EventHandler handler = ExecutedSetAreaCommand;
            if (handler != null)
            {
                handler(null, e);
            }
        }
    }
}