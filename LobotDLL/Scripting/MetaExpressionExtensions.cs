﻿using LobotAPI;
using LobotAPI.Scripting;
using LobotDLL.Packets;
using System;
using System.Collections.Generic;

namespace LobotDLL.Scripting
{
    public static class MetaExpressionExtensions
    {
        /// <summary>
        /// List of currently implemented functions. Metaexpressions match their commandName to the string to Execute the matching function.
        /// </summary>
        private static Dictionary<string, Action<string,string>> MetaExpressionList = new Dictionary<string, Action<string, string>>{
            { "StartingTown", (s1,s2) => { throw new NotImplementedException(); } },
            { "MonsterLevel", (s1,s2) => { throw new NotImplementedException(); } },
            { "MonsterDifficulty", (s1,s2) => { throw new NotImplementedException(); } },
            { "ScriptDifficult", (s1,s2) => { throw new NotImplementedException(); } },
            { "ScriptDifficulty", (s1,s2) => { throw new NotImplementedException(); } },
            { "NextScript", (s1,s2) => { throw new NotImplementedException(); } }
        };

        /// <summary>
        /// Invoke method in expression
        /// </summary>
        /// <param name="expr"></param>
        public static void Execute(this MetaExpression expr)
        {
            if (expr == null)
            {
                throw new ArgumentNullException(nameof(expr));
            }
            else
            {
                MetaExpressionList[expr.ExpressionName].Invoke(expr.Argument1, expr.Argument2);
            }
        }
    }
}